#!/bin/bash

[ ${#Year} -eq 0 ] && [ ${#1} -eq 0 ] && echo "error Year" && exit


set -x

#------------------------------------------------------------------

Dir=$HOME/MAR/out/${Reg}$Dom/${Dom}${Run}/$Year

Nbr_files=12                            # Monthly
   Region="set region/i=10:51/j=16:93"  # 35km

File=MAR_${Reg}${Dom}${Run}_${Year}.nc


if [ -d $Dir ] && [ ! -f $Pwd/in/${Dom}${Run}/${File} ] ; then

 cd $Dir

 nbr=0
 gunzip -f   ICE*.nc.gz &>/dev/null
 for file in ICE*.nc ; do
  nbr=$(( $nbr + 1 ))
 done 

 if [ $nbr -eq $Nbr_files ] ; then

 #sleep 60 ; gunzip -f ICE*.nc.gz &>/dev/null # if there is a SCP transfert at this moment

  nbr=0 ; day=0

  for file in ICE*.nc ; do

    nbr=$(( $nbr + 1 )) 

    tmp=`ncdump -h $file | head -10 | grep UNLIMITED`
    tmp=${tmp##*\(} ; tmp=${tmp%\ currently*}
  
    if [ $nbr -eq 1 ] ; then
ferret -server &> /dev/null <<EOF
use $file ; $Region
save/clobber/file="$Pwd/in/${Dom}${Run}/${File}" lon,lat,sh,srf,msk
quit
EOF
    fi
                      nbr2=$nbr
    [ $nbr -le 9 ] && nbr2="0$nbr"

    echo "use $file ; $Region" >  script.jnl

    echo "let SMB        = MBSF+MBRR-MBRU2-MBE">> script.jnl
    echo "let SMB${nbr2} = SMB[d=1,l=@sum]"   >> script.jnl
    echo "let RU${nbr2}  = MBRU[d=1,l=@sum]"  >> script.jnl
    echo "let ST${nbr2}3 = if(msk gt 0) then ST2[d=1,l=@ave] else ST2[d=1,k=2,l=@ave]" >> script.jnl
    echo "let ST${nbr2}  = if(srf ge 3) then ST${nbr2}3 else TT[d=1,k=3,l=@ave]"     >> script.jnl

    echo "save/append/file=\"$Pwd/in/${Dom}${Run}/${File}\"/k=1 SMB${nbr2},ST${nbr2},RU${nbr2}" >> script.jnl
    echo "quit"  >> script.jnl

    ferret -server -script  script.jnl &> /dev/null
    rm -f                   script.jnl

  done

 else
  echo "Not enough ICE*.nc files in $Dir" 
 fi
fi

#--------------------------------------------------------------------------

cd $Pwd/f90

file=interpol_MAR_to_GISM.dat


#if [   -f $Pwd/in/${Dom}${Run}/${File} ]                       && \
#   [   -f $Pwd/in/GRISLI/GR4MR${Dom}${Run}_class01_01.nc ]           && \
#   [ ! -f $Pwd/out/${Dom}${Run}/MAR4GRISLI_${Dom}${Run}_${Year}.nc ] ; then
#
# ts=`ncdump -h  $Pwd/in/GRISLI/GR4MR${Dom}${Run}_class01_01.nc` 
# ts=${ts#*\(} ; ts=${ts%\ currently*}
#
# if [ $ts -ge $Ts2 ] ; then 
#  echo "$Year  "                                                  > $file
#  echo "../in/${Dom}${Run}/${File}"                              >> $file
#  echo "../in/GRISLI/GR4MR${Dom}${Run}_class01_01.nc"            >> $file
#  echo "../out/${Dom}${Run}/MAR4GRISLI_${Dom}${Run}_${Year}.nc"  >> $file
#  echo "$Ts1 $Ts2"                                               >> $file
#
#  ./interpol_MAR_to_GRISLI.exe
#
#   if [ ! -f $Pwd/out/${Dom}${Run}/MAR4GRISLI_${Dom}${Run}_${Year}.nc ] ; then
#    rm -f $Pwd/in/${Dom}${Run}/${File}
#   fi 
#
# fi
#
#fi

#-------------------------------------------------------------------------------------

if [ $Year -le 2019 ] ; then
  echo "$Year  "                                                  > $file
  echo "../in/${Dom}${Run}/${File}"                              >> $file
  echo "../in/GISM/GR4MR${Dom}${Run}_class01_01.nc"            >> $file
  echo "../out/${Dom}${Run}/MAR4GISM_${Dom}${Run}_${Year}.nc"  >> $file
  echo "1 1"                                                     >> $file
  ./interpol_MAR_to_GISM.exe &
 sleep 2
fi

#-------------------------------------------------------------------------------------
