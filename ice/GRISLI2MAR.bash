#!/bin/bash

#set  -x

cd $Pwd/f90

if [   -f $Pwd/in/GRISLI/GR4MR${Dom}${Run}_class01_06.nc ]                       && \
   [   -f $Pwd/in/${Dom}${Run}/MAR_${Reg}${Dom}${Run}_${Year}.nc ]               && \
   [ ! -f $Pwd/out/${Dom}${Run}/GRISLI4MAR_${Dom}${Run}_$(( $Year + 1 ))0101.cdf ] ; then

 ts=`ncdump -h  $Pwd/in/GRISLI/GR4MR${Dom}${Run}_class01_06.nc` 
 ts=${ts#*\(} ; ts=${ts%\ currently*}
 
 if [ $ts -ge $Ts3 ] ; then 

  sleep 30 # to give time to GRILSLI to "finish" the Netcdf file

  file=interpol_GRISLI_to_MAR.dat

  echo "$Year  "                                                                     > $file
  echo "../in/${Dom}${Run}/MAR_${Reg}${Dom}${Run}_${Year}.nc"                       >> $file
  echo "../in/GRISLI/GR4MR${Dom}${Run}_class01_06.nc"                               >> $file
  echo "../out/${Dom}${Run}/GRISLI4MAR_${Dom}${Run}_$(( $Year + 1 ))0101.cdf"       >> $file
  echo "$Ts1 $Ts3"                                                                  >> $file
  ./interpol_GRISLI_to_MAR.exe

 fi
fi
