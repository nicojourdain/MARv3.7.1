#!/bin/bash
export  Run=65

export  Dom=m
export  Reg=GR

export  Pwd="$HOME/MAR/ice"

cd f90

file=interpol_MAR_to_GRISLI.dat

for Year in `seq 2101 2150` ; do
 export Year
 File=MAR_${Reg}${Dom}${Run}_${Year}.nc
  echo "$Year  "                                                  > $file
  echo "../in/${Dom}${Run}/${File}"                              >> $file
  echo "../in/GRISLI/GR4MR${Dom}66_class01_01.nc"            >> $file
  echo "../out/${Dom}${Run}/MAR4GRISLI_${Dom}${Run}_${Year}.nc"  >> $file
  echo "1 1"                                                     >> $file
  ./interpol_MAR_to_GRISLI.exe &
 sleep 1
done
