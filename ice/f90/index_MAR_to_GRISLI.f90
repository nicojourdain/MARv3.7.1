program interpol_25MAR_to_5Bam

implicit none

integer    ,parameter :: nbr_var=2

include '/usr/include/netcdf.inc'
include 'param.inc'

integer :: ncid,NCcode,var_ID,ID__nc,ll

integer       :: index(nbr_var),yb,ts1,ts2
character*1   :: dom  
character*4   :: varn1(nbr_var),varn2(nbr_var),YYYYc,MMc
character*100 :: desc(nbr_var),units(nbr_var),forcing,fnamNC1,fnamNC2,fnamNC3,tit_NC

real          :: dimval(1000),tmp1(mx,my),tmp2(mx,my),tmp

!--------------------------------------------------------------------------------

open(unit=10,status="old",file="interpol_GRISLI_to_MAR.dat")
read(10,*) yb
read(10,'(a100)') fnamNC2
read(10,'(a100)') fnamNC1
read(10,'(a100)') fnamNC3
read(10,*) ts1,ts2
close(10)

if (ts1>ts2) then
 print *,"ERROR ts1>ts2" ; stop
endif

print *,"interpol_GRISLI_to_MAR",yb,ts1,ts2

!--------------------------------------------------------------------------------

	!----------------------
	!longitudes - latitudes
	!----------------------

write(YYYYc,'(i4)') yb

call CF_READ2D(fnamNC1,'lon',1,bx,by,1,b_lon)
call CF_READ2D(fnamNC1,'lat',1,bx,by,1,b_lat)


call CF_READ2D(fnamNC1,'H',max(1,  ts1-1),bx,by,1,b_tmp1)
call CF_READ2D(fnamNC1,'H',        ts1   ,bx,by,1,b_tmp2)
call CF_READ2D(fnamNC1,'H',min(ts2,ts1+1),bx,by,1,b_tmp3)

 b_tmp1=(b_tmp1+b_tmp2+b_tmp3)/3.

call CF_READ2D(fnamNC1,'H',ts2,bx,by,1,b_tmp2)

do i=1,bx ; do j=1,by
                                          b_dmsk(i,j)= 0
 if(b_tmp1(i,j) >=10.and.b_tmp2(i,j)< 10) b_dmsk(i,j)=-1 
 if(b_tmp1(i,j) < 10.and.b_tmp2(i,j)>=10) b_dmsk(i,j)=+1 

!  if(b_tmp1(i,j) >1.and.b_tmp2(i,j)==0) b_dmsk(i,j)=-1 
!  if(b_tmp1(i,j)==0.and.b_tmp2(i,j) >1) b_dmsk(i,j)=+1 
! WARNING BEFORE 2089 !!!!!

enddo     ; enddo


b_dsh(:,:)=b_tmp2(:,:)-b_tmp1(:,:)

do i=1,bx ; do j=1,by
                   b_msk(i,j)=0
 if(b_tmp1(i,j)>1) b_msk(i,j)=1
enddo ; enddo

!--------------------------------------------------------------------------------

fnamNC1='../fx/MAR25km.nc'

call CF_READ2D(fnamNC1,'LON',1,mx,my,1,m_lon)
call CF_READ2D(fnamNC1,'LAT',1,mx,my,1,m_lat)
call CF_READ2D(fnamNC1,'SRF',1,mx,my,1,m_srf) ! sh  at itexpe=0 
call CF_READ2D(fnamNC1,'BED',1,mx,my,1,m_bed) ! bed at itexpe=0 
call CF_READ2D(fnamNC1,'SRF',1,mx,my,1, m_sh) ! sh  at itexpe=0 
call CF_READ2D(fnamNC1,'ICE',1,mx,my,1,m_msk) ! sh  at itexpe=0 

m_msk  = m_msk*100.
m_msk2 = 0

do i=3,mx-2 ; do j=3,my-2
                         m_msk2(i,j)=1
 do k=-1,1 ; do l=-1,1
  if(m_msk(i+k,j+l)<=50) m_msk2(i,j)=0 ! ablation zone 
 enddo     ; enddo

enddo     ; enddo


!!call CF_READ2D(fnamNC2,'MSK',1,mx,my,1,m_MSK) ! last year MAR ice msk
!!call CF_READ2D(fnamNC2,'SH',1,mx,my,1,  m_sh) ! last year MAR sh

!--------------------------------------------------------------------------------

open(unit=12,name="index_GRISLI_to_MAR.dat",status="old",form='unformatted')

 do k=1,mx ; do l=1,my
  read(12) x,y,(bi(k,l,m),m=1,bb),(bj(k,l,m),m=1,bb),(b_dist(k,l,m),m=1,bb)

  do m=1,bb
   b_dist(k,l,m)=max(1.,b_dist(k,l,m))
  enddo

 enddo ; enddo

close(12)

!--------------------------------------------------------------------------------


call CF_INI_FILE(fnamNC3,"GRISLI outputs in "//YYYYc)

call CF_CREATE_DIM("time","-",1,dimval)

DO i=1,mx
 dimval(i)= (i-27)*m_reso
ENDDO
call CF_CREATE_DIM("x","km",mx,dimval)

DO j=1,my
 dimval(j)= (j-42)* m_reso
ENDDO
call CF_CREATE_DIM("y","km",my,dimval)

call CF_CREATE_VAR("LAT","Latitude"       ,"degree","-","x","y","-" )
call CF_CREATE_VAR("LON","Longitude"       ,"degree","-","x","y","-" )
call CF_CREATE_VAR("BED","Bedrock"        ,"m","time","x","y","-" )
call CF_CREATE_VAR("MSK","Mask"           ,"m","time","x","y","-" )
call CF_CREATE_VAR("SH","Surface height" ,"m","time","x","y","-" )
call CF_CREATE_VAR("newSH","New Surface height" ,"m","time","x","y","-" )
call CF_CREATE_VAR("newMSK","New ICE mask"       ,"-","time","x","y","-" )

call CF_CREATE_FILE(fnamNC3)

call CF_write (fnamNC3, 'LAT'	, 1  , mx   , my, 1 , m_lat)
call CF_write (fnamNC3, 'LON'	, 1  , mx   , my, 1 , m_lon)
call CF_write (fnamNC3, 'BED'	, 1  , mx   , my, 1 , m_bed)
call CF_write (fnamNC3, 'MSK'	, 1  , mx   , my, 1 , m_msk)
call CF_write (fnamNC3, 'SH'	, 1  , mx   , my, 1 , m_sh)

!--------------------------------------------------------------------------------

call interpolation2(b_msk ,tmp2)

call interpolation2(b_dmsk,tmp1) ; tmp1=tmp1*100.

do i=2,mx-1 ; do j=2,my-1
 if(tmp1(i,j)==0.and.tmp2(i,j)==0.and.m_msk(i,j)>0) then
  do k=-1,1 ; do l=-1,1
   if(m_sh(i,j)<m_sh(i+k,j+l)) tmp1(i,j)=min(tmp1(i,j),tmp1(i+k,j+l))
  enddo     ; enddo 
 endif
enddo     ; enddo


do i=1,mx ; do j=1,my

 tmp1(i,j)=m_msk(i,j)+tmp1(i,j)
 tmp1(i,j)=min(99.999,max(0.001,tmp1(i,j)))

 if(m_msk(i,j)==    0.) tmp1(i,j)=m_msk(i,j)
 if(m_msk(i,j)==  100.) tmp1(i,j)=m_msk(i,j)

enddo     ; enddo

call CF_write (fnamNC3, 'newMSK'	, 1  , mx   , my, 1 , tmp1)

!--------------------------------------------------------------------------------

call interpolation2(b_dsh,tmp1)

do i=3,mx-2 ; do j=3,my-2
 if(tmp1(i,j)==0.and.tmp2(i,j)==0.and.m_msk(i,j)>0) then
  do k=-2,2 ; do l=-2,2
   if(m_sh(i,j)<m_sh(i+k,j+l)) tmp1(i,j)=min(tmp1(i,j),tmp1(i+k,j+l))
  enddo     ; enddo 
 endif
enddo     ; enddo

do i=1,mx ; do j=1,my

 tmp1(i,j)=     m_sh(i,j)+tmp1(i,j)  
 tmp1(i,j)=max(m_bed(i,j),tmp1(i,j))

!if(m_msk2(i,j)==0)  tmp1(i,j)=min(m_sh(i,j),tmp1(i,j)) ! only thinning in the current ice sheet margin

 if(m_msk(i,j)== 0.) tmp1(i,j)= m_sh(i,j)

enddo     ; enddo

!---------------------------------------------------
! filtering

do m=1,1000

 cpt=0 ; tmp=0
 do i=2,mx-1 ; do j=2,my-1

  if(m_msk(i,j)<90.) then

    cpt=0
    if(tmp1(i+1,j  )<tmp1(i,j)) cpt=cpt+1
    if(tmp1(i-1,j  )<tmp1(i,j)) cpt=cpt+1
    if(tmp1(i  ,j+1)<tmp1(i,j)) cpt=cpt+1
    if(tmp1(i  ,j-1)<tmp1(i,j)) cpt=cpt+1

    tmp2(i,j)=min(tmp1(i+1,j),tmp1(i-1,j),tmp1(i,j+1),tmp1(i,j-1))

    if(cpt==0.and.tmp2(i,j)>0) then
      print *,"SH Filtering",i,j,tmp1(i,j),m_msk(i,j)
      tmp1(i,j)=tmp2(i,j)+2
      tmp=tmp+1
    endif
  endif
 enddo     ; enddo

 if (tmp==0) goto 1000

enddo

1000 continue

!tmp1=(tmp1+m_sh)/2.

call CF_write (fnamNC3, 'newSH'	, 1  , mx   , my, 1 , tmp1)

!--------------------------------------------------------------------------------

end program


