program index_MAR_to_GRISLI

implicit none

include '/usr/include/netcdf.inc'
include 'param.inc'

integer :: ncid,NCcode,var_ID,ID__nc,nbr_var,ii,jj
character*90 :: fnamNC,tit_NC


!--------------------------------------------------------------------------------

fnamNC='../fx/MAR35km.nc'


call CF_READ2D(fnamNC,'LON',1,mx,my,1,m_lon)
call CF_READ2D(fnamNC,'LAT',1,mx,my,1,m_lat)
call CF_READ2D(fnamNC,'SRF',1,mx,my,1,m_srf) ; m_sh=m_srf
call CF_READ2D(fnamNC,'ICE',1,mx,my,1,m_MSK)


!--------------------------------------------------------------------------------

fnamNC='../fx/GISM5km.nc'


call CF_READ2D(fnamNC,'LON',1,bx,by,1,b_lon)
call CF_READ2D(fnamNC,'LAT',1,bx,by,1,b_lat)
call CF_READ2D(fnamNC,'SURFACE',1,bx,by,1,b_srf)
call CF_READ2D(fnamNC,'ICEMASK',1,bx,by,1,b_msk)

fnamNC='../fx/GISM5km_newtopo.nc'
call CF_READ2D(fnamNC,'zm',1,bx,by,1,b_srf)

!--------------------------------------------------------------------------------

	!------------------------------------
	!transformation des lon/lat en radian
	!------------------------------------

 m_lon = m_lon * pi/180.
 m_lat = m_lat * pi/180.

 b_lon = b_lon * pi/180.
 b_lat = b_lat * pi/180.


!--------------------------------------------------------------------------------

! only MAR ice pixels

 mi=0 ; mj=0 ; m_dist=9999

 do k=1,bx ; do l=1,by
 
  do n=1,mm ; call nearst_pixel1(k,l,n,0.) ; enddo   

  if(m_dist(k,l,1)<m_reso) then 
   write(6,'(2i4,100f6.1)') k,l,(m_dist(k,l,m),m=1,mm)
  else
  !write(6,'(2i4,100f6.1)') k,l,(m_dist(k,l,m),m=1,mm)
  endif   

 enddo ; enddo


 open(unit=12,name="index_MAR_to_GISM.dat1",status="replace",form='unformatted')
 do k=1,bx ; do l=1,by
  write(12) k,l,(mi(k,l,m),m=1,mm),(mj(k,l,m),m=1,mm),(m_dist(k,l,m),m=1,mm)
 enddo ; enddo
 close(12)

!--------------------------------------------------------------------------------

! all ice/tundra/see pixels

 mi=0 ; mj=0 ; m_dist=9999

 do k=1,bx ; do l=1,by
 
  do n=1,mm ; call nearst_pixel1(k,l,n,-1.) ; enddo   

  write(6,'(2i4,100f6.1)') k,l,(m_dist(k,l,m),m=1,mm)

 enddo ; enddo

 open(unit=12,name="index_MAR_to_GISM.dat2",status="replace",form='unformatted')
 do k=1,bx ; do l=1,by
  write(12) k,l,(mi(k,l,m),m=1,mm),(mj(k,l,m),m=1,mm),(m_dist(k,l,m),m=1,mm)
 enddo ; enddo
 close(12)

!--------------------------------------------------------------------------------

end program

