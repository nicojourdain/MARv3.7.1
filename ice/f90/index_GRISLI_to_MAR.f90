program index_GRISLI_to_MAR

implicit none

include '/usr/include/netcdf.inc'
include 'param.inc'

integer :: ncid,NCcode,var_ID,ID__nc,nbr_var,ii,jj
character*90 :: fnamNC,tit_NC


!--------------------------------------------------------------------------------

fnamNC='../fx/MAR25km.nc'


call CF_READ2D(fnamNC,'LON',1,mx,my,1,m_lon)
call CF_READ2D(fnamNC,'LAT',1,mx,my,1,m_lat)
call CF_READ2D(fnamNC,'SRF',1,mx,my,1,m_srf) ;  m_sh=m_srf
call CF_READ2D(fnamNC,'ICE',1,mx,my,1,m_MSK)
call CF_READ2D(fnamNC,'BED',1,mx,my,1,m_bed)


!--------------------------------------------------------------------------------

fnamNC='../fx/bamber_1to5km.cdf'


call CF_READ2D(fnamNC,'LON',1,bx,by,1,b_lon)
call CF_READ2D(fnamNC,'LAT',1,bx,by,1,b_lat)
call CF_READ2D(fnamNC,'SRF',1,bx,by,1,b_srf)
call CF_READ2D(fnamNC,'MSK',1,bx,by,1,b_msk)

!--------------------------------------------------------------------------------

	!------------------------------------
	!transformation des lon/lat en radian
	!------------------------------------

 m_lon = m_lon * pi/180.
 m_lat = m_lat * pi/180.

 b_lon = b_lon * pi/180.
 b_lat = b_lat * pi/180.


!--------------------------------------------------------------------------------

! only MAR ice pixels

 bi=0 ; bj=0 ; b_dist=9999

 ii=1 ; jj=1 ! previous index

 do k=1,mx ; do l=1,my
 
  do n=1,bb ; call nearst_pixel2(k,l,ii,jj) ; enddo   

   write(6,'(2i4,100f7.1)') k,l,(b_dist(k,l,m),m=1,bb)
 enddo ; enddo


 open(unit=12,name="index_GRISLI_to_MAR.dat",status="replace",form='unformatted')
 do k=1,mx ; do l=1,my
  write(12) k,l,(bi(k,l,m),m=1,bb),(bj(k,l,m),m=1,bb),(b_dist(k,l,m),m=1,bb)
 enddo ; enddo
 close(12)

!--------------------------------------------------------------------------------

end program

