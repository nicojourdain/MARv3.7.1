
cd f90

for YYYY in `seq 1976 2005` ; do

 file=`ls -1 ~/MAR/out/GRm/ICE*${YYYY}*.nc`

 echo "$YYYY"  > interpol_MAR_to_GRISLI.dat
 echo "$file" >> interpol_MAR_to_GRISLI.dat
 echo "neant" >> interpol_MAR_to_GRISLI.dat
 echo "$HOME/MARv3.6-MIROC5-$YYYY.nc" >> interpol_MAR_to_GRISLI.dat
 echo "1 1" >> interpol_MAR_to_GRISLI.dat 
  
 ./interpol_MAR_to_GRISLI_yearly.exe
done

