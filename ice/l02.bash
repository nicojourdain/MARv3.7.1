#!/bin/bash

#set -x

export  Run=02

export  Dom=l
export  Reg=GR

export  Pwd="$HOME/MAR/ice"

export  Year=2019
export  YearBegin=2000
export  Ts1=51

while [ 1 -eq 1  ] ; do

 export Ts2=$(( $Ts1 + $Year - $YearBegin    ))
 export Ts3=$(( $Ts1 + $Year - $YearBegin +1 ))

 ./MAR2GRISLI.bash

 ./GRISLI2MAR.bash

 [ -f $Pwd/out/${Dom}${Run}/GRISLI4MAR_${Dom}${Run}_$(( $Year + 1 ))0101.cdf ] && export Year=$(( $Year +1 ))
 [ $Year -eq 2201 ] && exit

  sleep 60
  date

done
