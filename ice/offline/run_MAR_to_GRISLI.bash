#!/bin/bash


#for y in `seq 1970 2169` ; do
#echo "$y"  > interpol_MAR_to_GRISLI.dat
#echo "51 $(( 51 + $y - 2000))">> interpol_MAR_to_GRISLI.dat
#echo "1 1" >> interpol_MAR_to_GRISLI.dat
#echo "l21">> interpol_MAR_to_GRISLI.dat
#./interpol_MAR_to_GRISLI.exe &
#sleep 1
#done
#exit


set -x

run=$1
yearbegin=2019
ts1=19 # reference year 2019 until 2060

year=2020

[ ${#1} -ne 3 ] && echo "runame XYY not specified" && exit


[ $run == "m67" ] && dir=3
[ $run == "m68" ] && dir=4
[ $run == "m69" ] && dir=5
[ $run == "m70" ] && dir=6
[ $run == "m71" ] && dir=7
[ $run == "m72" ] && dir=8
[ $run == "m73" ] && dir=9

sleep 2

while [ $year -le 2150 ] ; do

 [ $year -ge 2060 ] && ts1=7 && yearbegin=2006   #WARNING WARNING BUG BUG BUG

 [ ! -f         /climato_tmp1/fettweis/GRISLI/RESULTATS/${run}/GR4MR${run}_class01_06.nc ] && exit

 ts2=`ncdump -h /climato_tmp1/fettweis/GRISLI/RESULTATS/${run}/GR4MR${run}_class01_06.nc | grep "time ="`
 ts2=${ts2#*\(} ; ts2=${ts2%\ *}

 if [ $(( $year - $yearbegin)) -le $(( $ts2 - $ts1 )) ] ; then
  
   sleep 10 

   ts2=$(( $ts1 + $year - $yearbegin ))

   while [ -f interpol_MAR_to_GRISLI.dat ] ; do
    sleep $(( $RANDOM % 5 ))
   done

   echo "$year"     > interpol_MAR_to_GRISLI.dat
   echo "$ts1 $ts2">> interpol_MAR_to_GRISLI.dat
   echo "$run"     >> interpol_MAR_to_GRISLI.dat

   cat   interpol_MAR_to_GRISLI.dat

   [ ! -f out/${run}/MAR4GRISLI_${run}_${year}.nc ] &&  ./interpol_MAR_to_GRISLI.exe${run}

   rm -f interpol_MAR_to_GRISLI.dat

   if [ -f out/${run}/MAR4GRISLI_${run}_${year}.nc ] ; then  
    year=$(( $year + 1 ))
   else
    sleep $(( $RANDOM % 10 ))
   fi

 fi

 sleep 10

done

wait
