!--------------------------------------------------------------------------------

function distance(lon2,lat2,lon1,lat1)	

implicit none

include 'param.inc'

 real :: lon1,lat1
 real :: lon2,lat2

 distance = acos(sin(lat1) * sin(lat2) + &
	    cos(lat1) * cos(lat2) * cos(lon2 - lon1)) * R

end function


!--------------------------------------------------------------------------------


subroutine nearst_pixel1(k,l,cpt,nn)

implicit none

include 'param.inc'

real nn,dd,cpt2


 if(cpt == 1) then
  do i=1,mx ; do j=1,my

   if(m_MSK(i,j) > nn) then

    dd = distance(m_lon(i,j),m_lat(i,j),b_lon(k,l),b_lat(k,l))

    if(dd >= 0. .and. dd < m_dist(k,l,1)) then
       m_dist(k,l,1) = dd
       mi(k,l,1)     = i
       mj(k,l,1)     = j
    endif   
   endif

  enddo ; enddo
 endif

 if(cpt >= 2) then
  do i=max(1,mi(k,l,cpt-1)-30),min(mi(k,l,cpt-1)+30,mx)
  do j=max(1,mj(k,l,cpt-1)-30),min(mj(k,l,cpt-1)+30,my)

   if(m_MSK(i,j) > nn) then
    dd = distance(m_lon(i,j),m_lat(i,j),b_lon(k,l),b_lat(k,l))

    if(dd >= m_dist(k,l,cpt-1) .and. dd < m_dist(k,l,cpt) .and. &
      (i /= mi(k,l,cpt-1) .or. j /= mj(k,l,cpt-1))        .and. &
      (i /= mi(k,l,max(1,cpt-2)) .or. j /= mj(k,l,max(1,cpt-2))) ) then
       m_dist(k,l,cpt) = dd
       mi(k,l,cpt)     = i
       mj(k,l,cpt)     = j
    endif

   endif

  enddo ; enddo
 endif

end subroutine


!--------------------------------------------------------------------------------


subroutine nearst_pixel2(k,l,ii,jj)

implicit none

include 'param.inc'

integer ii,jj

real dd

 do i=max(1,ii-30),min(ii+30,bx) ; do j=max(1,jj-30),min(jj+30,by)
   dd = distance(m_lon(k,l),m_lat(k,l),b_lon(i,j),b_lat(i,j))
   if(dd <=sqrt(2.*(m_reso/2.)**2) .and. dd < b_dist(k,l,1)) then
      b_dist(k,l,1) = dd
      bi(k,l,1)     = i
      bj(k,l,1)     = j
   endif      
 enddo ; enddo


 if(b_dist(k,l,1)>9000) then

 do i=1,bx ; do j=1,by
   dd = distance(m_lon(k,l),m_lat(k,l),b_lon(i,j),b_lat(i,j))
   if(dd <=sqrt(2.*(m_reso/2.)**2) .and. dd < b_dist(k,l,1)) then
      b_dist(k,l,1) = dd
      bi(k,l,1)     = i
      bj(k,l,1)     = j
   endif      
 enddo ; enddo

 endif

 cpt=1 
 if(b_dist(k,l,1)<9000) then

 do i=max(1,bi(k,l,1)-30),min(bi(k,l,1)+30,bx)
 do j=max(1,bj(k,l,1)-30),min(bj(k,l,1)+30,by)

    dd = distance(m_lon(k,l),m_lat(k,l),b_lon(i,j),b_lat(i,j))

    if(dd <=sqrt(2.*(m_reso/2.)**2).and. &
      (i /= bi(k,l,1).or. j /= bj(k,l,1))) then
                  cpt  =cpt+1
                  cpt  = min(bb,cpt)
       b_dist(k,l,cpt) = dd
       bi(k,l,cpt)     = i
       bj(k,l,cpt)     = j
    endif

 enddo ; enddo

 endif

 ii=bi(k,l,1) ; jj=bj(k,l,1)

end subroutine

!========================================================================

function ave(ts,dd)

implicit none
real            corr,cov,std,ave
integer         dd,i
real            ts(dd)

ave=0

do i=1,dd
 ave=ave+ts(i)/real(dd)
enddo

end function


!========================================================================

function std(ts,dd)

implicit none
real            corr,cov,std,ave
integer         dd,i
real            ts(dd)

std=0

do i=1,dd
 std=std+(ts(i)-ave(ts,dd))**2/real(dd)
enddo

std=max(1e-6,sqrt(std))

end function


!========================================================================

function cov(ts1,ts2,dd)

implicit none
real            corr,cov,std,ave
integer         dd,i
real            ts1(dd),ts2(dd)

cov=0

do i=1,dd
 cov=cov+(ts1(i)-ave(ts1,dd))*(ts2(i)-ave(ts2,dd))/real(dd)
enddo

end function

!========================================================================

function corr(ts1,ts2,dd)

implicit none
integer         dd,i
real            ts1(dd),ts2(dd)
real            corr,cov,std,ave

corr=cov(ts1,ts2,dd)/(std(ts1,dd)*std(ts2,dd))

end function

!========================================================================

function max_ts(ts,dd)

implicit none
real            max_ts
integer         dd,i
real            ts(dd)

max_ts=-99999999

do i=1,dd
 max_ts=max(max_ts,ts(i))
enddo

end function

!========================================================================

function min_ts(ts,dd)

implicit none
real            min_ts
integer         dd,i
real            ts(dd)

min_ts=99999999

do i=1,dd
 min_ts=min(min_ts,ts(i))
enddo

end function

!========================================================================

function percentile(ts0,dd,per)

implicit none

integer i,j,per,dd,dim

real ts0(dd),percentile
real ts1(dd),ts2

ts1=ts0

dim=min(dd,max(1,per*dd/100))

if(per<0 .or. per> 100) stop
 
do i=1,dim
 do j=i+1,dd
  if(ts1(j)>ts1(i)) then
    ts2   =ts1(i)
    ts1(i)=ts1(j)
    ts1(j)=ts2
  endif
 enddo
enddo

percentile=ts1(dim)

end function

!========================================================================

subroutine interpolation1(m_var,b_var)

implicit none

include 'param.inc'


 real :: m_var(mx,my)
 real :: b_var(bx,by)

 integer :: ii(mm),jj(mm)
 real    :: msk(mm)
 real*8  :: vv,dd
 
 do k=1,bx ; do l=1,by

  if((mi(k,l,1) == 999 .and. mj(k,l,1) == 999)) then

   b_var(k,l) = -1e30

  else

   do m=1,mm
    ii(m)=mi(k,l,m)
    jj(m)=mj(k,l,m)
   enddo

   msk=1

   vv = 0 ; dd=0

  !do m=1,min(9,mm) ! BUG BUG 2015/03/10
   do m=1,4
    vv = vv + (1/(m_dist(k,l,m)**P1)) * msk(m) * m_var(ii(m),jj(m))
    dd = dd + (1/(m_dist(k,l,m)**P1)) * msk(m)
   enddo
 
   b_var(k,l) = vv / max(0.00000001d0,dd)

   if(m_dist(k,l,1) == 0.) b_var(k,l) = m_var(mi(k,l,1),mj(k,l,1))

  endif

 enddo ; enddo

end subroutine


!========================================================================

subroutine interpolation2(b_var,m_var)

implicit none

include 'param.inc'


 real :: m_var(mx,my)
 real :: b_var(bx,by)

 integer :: ii(bb),jj(bb)
 real*8  :: vv,dd
 
 do k=1,mx ; do l=1,my

  if(b_dist(k,l,1)>=m_reso) then

   m_var(k,l) = 0

  else

   do m=1,bb
    ii(m)=bi(k,l,m)
    jj(m)=bj(k,l,m)
   enddo
    
   vv = 0 ; dd=0

   do m=1,bb
    if(b_dist(k,l,m)<=m_reso.and.ii(m)>0.and.jj(m)>0) then
     vv = vv + (1./(b_dist(k,l,m)**P2)) * b_var(ii(m),jj(m))
     dd = dd + (1./(b_dist(k,l,m)**P2))
    endif
   enddo
 
   m_var(k,l) = vv / max(0.00000001d0,dd)

  endif

 enddo ; enddo

end subroutine


!--------------------------------------------------------------------------------------

subroutine interpolation1a_with_corrections(m_var,b_var)

 implicit none

 include 'param.inc'

 integer,parameter :: nbr=mm*50

 real :: m_var(mx,my)
 real :: b_var(bx,by),b_var1(bx,by),b_var2(bx,by)

 integer :: ii(mm),jj(mm),ll,o
 real    :: msk(mm),ts1(nbr),ts2(nbr),reg_a,reg_b,in_max,in_min
 real    :: vv,dd

 do k=1,bx ; do l=1,by

  if((mi(k,l,1) == 999 .and. mj(k,l,1) == 999)) then

   b_var(k,l) = -1e30

  else
   
   ts1=0 ; ts2=0

   do m=1,mm
    ii(m)=mi(k,l,m)
    jj(m)=mj(k,l,m)
   enddo

   msk=1

   do m=1,mm
    if(m_msk(ii(m),jj(m))<=0) msk(m)=0
   enddo

   vv = 0 ; dd=0

   do m=1,4
    vv = vv + (1./(m_dist(k,l,m)**P1)) * msk(m) * m_var(ii(m),jj(m))
    dd = dd + (1./(m_dist(k,l,m)**P1)) * msk(m)
   enddo
 
   b_var1(k,l) = vv / max(0.00000001d0,dd)
   b_var(k,l)  = b_var1(k,l) 

   in_max=-30000
   in_min= 30000

   n=0 ; o=0
   do m=1,min(mm,9)
    if(msk(m)>0) then
     n=n+1 ; o=o+1

     in_max=max(in_max,m_var(ii(m),jj(m)))
     in_min=min(in_min,m_var(ii(m),jj(m)))

     ts1(n)=m_sh(ii(m),jj(m))
     ts2(n)=m_var(ii(m),jj(m))
                                                 ll=0
     if(abs((b_sh(k,l)-m_sh(ii(m),jj(m)))<=250)) ll=19
     do i=1,ll*msk(m)
      n=n+1
      ts1(n)=m_sh(ii(m),jj(m))
      ts2(n)=m_var(ii(m),jj(m))
      enddo
     endif
   enddo

   n=min(nbr,n)

   reg_a=cov(ts1,ts2,n)/(std(ts1,n))**2
   reg_b=ave(ts2,n) - reg_a * ave(ts1,n)

   b_var2(k,l)=reg_a*b_sh(k,l)+reg_b

   if(in_max>0) then 
    b_var2(k,l)=min(b_var2(k,l),1.5*in_max)
   else
    b_var2(k,l)=min(b_var2(k,l),0.5*in_max)
   endif

   if(in_min<0) then 
    b_var2(k,l)=max(b_var2(k,l),1.5*in_min)
   else
    b_var2(k,l)=max(b_var2(k,l),0.5*in_min)
   endif

   if(o>=1) b_var(k,l) = b_var2(k,l)

  endif

 enddo ; enddo

end subroutine


!--------------------------------------------------------------------------------------

subroutine interpolation1b_with_corrections(m_var,b_var,type_var)

 implicit none

 include 'param.inc'

 integer,parameter :: nbr=mm*50

 real :: m_var(mx,my)
 real :: b_var(bx,by),b_var1(bx,by),b_var2(bx,by),b_var3(bx,by)

 integer :: ii(mm),jj(mm),ll,o,type_var
 real    :: msk(mm),ts1(nbr),ts2(nbr),reg_a,reg_b
 real    :: vv,dd,in_max(bx,by),in_min(bx,by)

 in_max=-30000
 in_min= 30000

 do k=1,bx ; do l=1,by

  if((mi(k,l,1) == 999 .and. mj(k,l,1) == 999)) then

   b_var(k,l) = -1e30

  else
   
   ts1=0 ; ts2=0 ; msk=1

   do m=1,mm

    ii(m)=mi(k,l,m)
    jj(m)=mj(k,l,m)

    if(m_msk(ii(m),jj(m))<=0) msk(m)=0

    if(msk(m)>0) then
     in_max(k,l)=max(in_max(k,l),m_var(ii(m),jj(m)))
     in_min(k,l)=min(in_min(k,l),m_var(ii(m),jj(m)))
    endif

   enddo

   vv = 0 ; dd=0

   do m=1,4
    vv = vv + (1./(m_dist(k,l,m)**P1)) * msk(m) * m_var(ii(m),jj(m))
    dd = dd + (1./(m_dist(k,l,m)**P1)) * msk(m)
   enddo
 
   b_var1(k,l) = vv / max(0.00000001d0,dd)
   b_var(k,l)  = b_var1(k,l) 

   n=0
   do m=1,min(mm,9)
    do o=m,min(mm,9)
     if(msk(m)>0.and.msk(o)>0.and.abs(m_sh(ii(m),jj(m))-m_sh(ii(o),jj(o)))>=20.)then
          n  = n+1
      ts1(n) =  m_sh(ii(m),jj(m)) -  m_sh(ii(o),jj(o))
      ts2(n) = m_var(ii(m),jj(m)) - m_var(ii(o),jj(o))
     endif
    enddo
   enddo

   n=min(mm,n)

   reg_a = cov(ts1,ts2,n)/(std(ts1,n))**2

   if(n<=4)        reg_a=0
   if(type_var==1) reg_a=min(6.  ,max(-2.  ,reg_a))
   if(type_var==2) reg_a=min(0.00,max(-0.03,reg_a))

   b_var2(k,l) = reg_a

  endif

 enddo ; enddo


 !b_var =b_var1
 b_var3=b_var2

 do n=1,3
 do k=2,bx-1 ; do l=2,by-2
  vv=0 ; dd=0
  do m=-1,1 ; do o=-1,1 
   dd = dd + b_var2(k+m,l+o) / max(1.,sqrt(abs(b_sh(k,l)-b_sh(k+m,l+o))))
   vv = vv + 1.              / max(1.,sqrt(abs(b_sh(k,l)-b_sh(k+m,l+o))))
  enddo     ; enddo
  b_var3(k,l) = dd / vv
 enddo ; enddo
 b_var2=b_var3
 enddo

 do k=1,bx ; do l=1,by
  if( b_srfmar(k,l)>-500.and.abs(b_sh(k,l)-b_srfmar(k,l))>20) then
   b_var(k,l) = b_var1(k,l) + b_var3(k,l) * (b_sh(k,l)-b_srfmar(k,l))

  if(in_max(k,l)>0) then 
    b_var(k,l)=min(b_var(k,l),1.2*in_max(k,l))
   else
    b_var(k,l)=min(b_var(k,l),0.8*in_max(k,l))
   endif

   if(in_min(k,l)<0) then 
    b_var(k,l)=max(b_var(k,l),1.2*in_min(k,l))
   else
    b_var(k,l)=max(b_var(k,l),0.8*in_min(k,l))
   endif
   
  endif

 enddo ; enddo

end subroutine

