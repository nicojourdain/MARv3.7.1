rm -f msk.dat
touch msk.dat
for year in `seq 1969 2169` ; do

file=interpol_GRISLI_to_MAR.dat

echo "$year" > $file
echo "../in/l02/MAR_GRl02_$year.nc" >> $file
echo "../in/GRISLI/GR4MRl02_class01_01.nc" >> $file
echo "../GRISLI4MAR_l02_$(( $year + 1 )).cdf" >> $file
echo "51 $(( 72 + $year - 2020 ))" >> $file

./a.out

done

