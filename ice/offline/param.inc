!------------------
!general parameters
!------------------

real,parameter :: pi   = 3.1415
real,parameter :: R    = 6371.
real,parameter :: epsi = 10.E-1

integer :: i,j,k,l,x,y,m1,m,n

integer :: cpt

real    :: distance,dist,cov,ave,std,corr,max_ts,min_ts

!------------------
!parameters at 25km
!------------------

integer,parameter :: mx     = 60
integer,parameter :: my     = 110
real,parameter    :: m_reso = 25

!------------------
!parameters at 30km
!------------------

!integer,parameter :: mx     = 51
!integer,parameter :: my     = 93
!real,parameter    :: m_reso = 30


!------------------
!parameters at 35km
!------------------

!integer,parameter :: mx     = 42
!integer,parameter :: my     = 78
!real,parameter    :: m_reso = 35

real :: m_lon(mx,my),m_xx(mx,my),m_yy(mx,my),m_lat(mx,my)

real :: m_dx(mx,my)		
real :: m_dy(mx,my)		

real ::  m_SH(mx,my),m_SOL(mx,my),m_SRF(mx,my)		
real :: m_MSK(mx,my),m_bed(mx,my),m_MSK2(mx,my)		


	common/param_MAR/m_lon,m_lat,m_dx,m_dy,m_bed,		&
			 m_SH,m_SOL,m_SRF,m_MSK



!-------------------------------
!parameters indices MAR - Bam5km
!-------------------------------

integer,parameter :: bx     = 301
integer,parameter :: by     = 561
real,parameter    :: b_reso = 5 !km

!-------------------------------
!parameters indices MAR - GISM5km
!-------------------------------

!integer,parameter :: bx     = 329
!integer,parameter :: by     = 561
!real,parameter    :: b_reso = 5 !km


real,parameter :: P1=2.

! nbr of MAR pixels for each bamber pixel
integer,parameter :: mm     = 9
integer :: mi(bx,by,mm),mj(bx,by,mm)
real    :: m_dist(bx,by,mm)

	common/index_MAR/mi,mj,m_dist


real,parameter :: P2=1./2. 

! nbr of bamber pixels for each MAR pixel
integer,parameter :: bb     = abs(real(2*(m_reso/real(b_reso))**2))
                               
integer :: bi(mx,my,bb),bj(mx,my,bb)
real    :: b_dist(mx,my,bb)

	common/index_Bam5km/bi,bj,b_dist

!-----------------
!parameters Bamber
!-----------------


real*4 :: b_lon(bx,by)
real*4 :: b_lat(bx,by)

real :: b_tmp1(bx,by),b_tmp2(bx,by),b_tmp3(bx,by)

real :: b_SH(bx,by),b_dSH(bx,by),b_dmsk(bx,by)
real :: b_SRF(bx,by),b_bed(bx,by),b_thick(bx,by),b_msk(bx,by),b_msk2(bx,by)			
real :: b_srfmar(bx,by),b_mskmar(bx,by),b_latmar(bx,by),b_lonmar(bx,by)

	common/bamber/b_SRF,b_SH,b_lon,b_lat,b_bed,b_thick,b_msk,b_dSH,b_srfmar



