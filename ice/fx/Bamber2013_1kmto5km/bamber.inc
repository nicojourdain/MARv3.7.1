!--------------------------------------------------------
!	Configuration of parameters for interpolation    
!	of the Bamber's topography and the Box's mask	
!	onto the NESTOR grid at different resolutions	
!--------------------------------------------------------
!--------------------------------------------------------
!	Parameters have to be activated according	
!	to the spatial resolution of the NESTOR		
!--------------------------------------------------------


!---------------------------------------
!Configuration of the general parameters
!---------------------------------------

 real,parameter    :: pi   = 3.1415
 real,parameter    :: R    = 6371.
 real,parameter    :: epsi = 10.E-6

 integer :: i,j,k,l,x,y,h
 integer :: cpt,tmp

 real    :: distance
 real	 :: dist
 real    :: dist_min

 real    :: tmp1,tmp2,tmp3

 real	 :: P = 1./4.			!power of the distance

	common/param_gen/	P	


!----
!25km
!-----------------------------------------------------------------------
!Configuration for interpolation onto the NESTOR grid at 25km resolution
!-----------------------------------------------------------------------

  integer,parameter :: mx    = 301
  integer,parameter :: my    = 561
  integer,parameter :: mmx   =  1 ! [mmx,mx-mmx+1]
  integer,parameter :: mmy   =  1 ! [mmy,my-mmy+1]

  real,parameter    :: range = 5 ! km

  integer,parameter :: mh    = 1.1 * range * range

!---------------------------------------------------
!Configuration of the general parameters from NESTOR
!---------------------------------------------------

 real :: m_lon (mx,my),m_lon_tmp(mx,my)
 real :: m_lat (mx,my),m_lat_tmp(mx,my)
 real :: m_bed (mx,my),m_thick(mx,my),m_srf(mx,my)

 real :: m_dist(mx,my)			!distance

 real :: m_sh  (mx,my)			!topography from NESTOR
 real :: m_sol (mx,my)			!mask from NESTOR
 real :: m_gism (mx,my)			!mask from GISM05

 real :: m_msk (mx,my)			!binary mask from NESTOR

	common/param_NESTOR/	m_lon,m_lat,m_dist		, &
				m_sh,m_sol,m_msk


!---------------------------------------------------------------
!Configuration of the parameters for the interpolated topography
!---------------------------------------------------------------

 integer :: bm_cpt   (mx,my)		!number of points to be interpolated
 integer :: bm_i     (mx,my,mh)
 integer :: bm_j     (mx,my,mh)		!indices i and j
 real    :: bm_dist  (mx,my,mh)		!distance

 integer :: B25_cpt  (mx,my)
 integer :: B25_i    (mx,my,mh)
 integer :: B25_j    (mx,my,mh)
 real    :: B25_dist (mx,my,mh)

 integer :: indice   (mh)			
 real    :: tmp_min  (mh)	

 real    :: m_srf_Bam (mx,my)		!Bamber topography interpolated onto the NESTOR grid
 real	 :: m_bed_Bam(mx,my)		!interpolated bed

 real	 :: m_msk_Bam(mx,my)		!interpolated sea-land mask
 real	 :: m_ice_Bam(mx,my)		!interpolated ice mask

 real    :: m_sol_tmp(mx,my)
 real    :: m_sol_Bam(mx,my)		!interpolated and corrected sea-land mask

	common/param_Bam_NEST/	bm_cpt,bm_i,bm_j,bm_dist	, &
				B25_cpt,B25_i,B25_j,B25_dist	, &
				indice,tmp_min,m_srf_Bam        , &
				m_msk_Bam,m_sol_tmp,m_sol_Bam



!---------------------------------------------------
!Configuration of the Bamber's topography parameters
!---------------------------------------------------

 integer,parameter :: bx = 1501
 integer,parameter :: by = 2801

 real :: b_lon    (bx,by)
 real :: b_lat    (bx,by)

 real :: b_tmp    (bx,by)

 real :: b_dist   (bx,by)		!distance 1km Bamber to NESTOR

 real :: b_srf    (bx,by)		!surface height
 real :: b_bed    (bx,by)		!bedrock height
 real :: b_thick  (bx,by)		!ice sheet thickness

 real :: b_msk    (bx,by)		!sea-land mask
 real :: b_ice    (bx,by)		!ice mask

	common/param_Bamber/	b_lon,b_lat	, &
				b_dist,b_srf,b_bed,b_thick	, &
				b_msk,b_ice






















