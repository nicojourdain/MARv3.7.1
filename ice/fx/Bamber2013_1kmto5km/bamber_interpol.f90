program interpol_Bamber1_to_Bamber5

implicit none

include '/usr/include/netcdf.inc'
include 'bamber.inc'


integer      :: ncid,NCcode,var_ID,ID__nc,nbr_var
character*200:: fnamNC,tit_NC
real         :: dimval(500)


integer,parameter :: nx    =  mx*2
integer,parameter :: ny    =  my*2

real   d1,d2,area(mx,my),lon1,lon2,lat1,lat2
real*8   dxinv2,slopx,slopy,slopTV(mx,my),slopGE(mx,my)
!--------------------------------------------------------------------------------

! NESTOR

 open(unit=10,name="bamber_lon.dat",status="old")
 read(10,*) m_lon_tmp
 close(10)

 open(unit=11,name="bamber_lat.dat",status="old")
 read(11,*) m_lat_tmp
 close(11)

  do i=1,mx ; do j=1,my
   m_lon(i,j) = m_lon_tmp(i,(my-j+1))
   m_lat(i,j) = m_lat_tmp(i,(my-j+1))
  enddo ; enddo

 open(unit=12,name="bamber_srf.dat",status="old")
 read(12,*) m_srf
 close(12)

 open(unit=13,name="bamber_bed.dat",status="old")
 read(13,*) m_bed
 close(13)

 open(unit=14,name="bamber_thick.dat",status="old")
 read(14,*) m_thick
 close(14)

 do i=1,bx ; do j=1,by
  if (m_srf  (i,j)<=0) m_msk(i,j)=0
  if (m_srf  (i,j) >0) m_msk(i,j)=1
  if (m_thick(i,j) >0) m_msk(i,j)=2
 enddo ; enddo

!--------------------------------------------------------------------------------

! Bamber

fnamNC='Greenland_bedrock_topography_and_geometry_JGriggs.nc2'


call CF_READ2D(fnamNC,'LON',1,bx,by,1,b_lon)
call CF_READ2D(fnamNC,'LAT',1,bx,by,1,b_lat)

call CF_READ2D(fnamNC,'BED',1,bx,by,1,b_bed)
call CF_READ2D(fnamNC,'SRF',1,bx,by,1,b_srf)

call CF_READ2D(fnamNC,'ICE',1,bx,by,1,b_ice)
call CF_READ2D(fnamNC,'MSK',1,bx,by,1,b_msk)


!--------------------------------------------------------------------------------

 B25_cpt=0

 !WARNING x-1

 open(unit=210,name="bamber_index_1to5.dat",status="old",form='unformatted')
 do j=mmy,my-mmy+1 ; do i=mmx,mx-mmx+1
   read(210) x,y,B25_cpt(x,y),(B25_i(x,y,h),h=1,mh), &
	    (B25_j(x,y,h),h=1,mh),(B25_dist(x,y,h),h=1,mh)
   
   if(x /= i) then
    print *,"ERROR 1",x,i
    stop
   endif

   if(y /= j) then
    print *,"ERROR 2",y,j
    stop
   endif

!   if(i==20.and.j==69) then
!    print *,m_lon(i,j),m_lat(i,j)
!    print *,"---------------------------------"
!    do h=1,B25_cpt(x,y)
!     print *,B25_i(x,y,h),B25_j(x,y,h),b_lon(B25_i(x,y,h),B25_j(x,y,h)),b_lat(B25_i(x,y,h),B25_j(x,y,h)),B25_dist(x,y,h)
!    enddo
!    stop
!   endif

  enddo ; enddo
 close(210)


!--------------------------------------------------------------------------------
 
! interpolation

 do k=1,bx ; do l=1,by
  if(b_srf(k,l) < 0.) b_srf(k,l) = 0.
 enddo ; enddo

 call interpolation(m_sh,b_srf,m_srf_Bam,P)

 call interpolation(m_sh,b_bed,m_bed_Bam,P)

!--------------------------------------------------------------------------------


 do k=1,bx ; do l=1,by
                                                         b_tmp(k,l) =  0
  if(b_msk(k,l) ==1.or.b_msk(k,l) ==2.or.b_msk(k,l) ==4) b_tmp(k,l) =  1.
 enddo ; enddo

 call interpolation(m_msk,b_tmp,m_msk_Bam,P)

 do k=1,bx ; do l=1,by
                                          b_tmp(k,l) = 0
  if(b_msk(k,l) == 2..or.b_msk(k,l) == 4) b_tmp(k,l) = 1.
 enddo ; enddo

 call interpolation(0*m_msk,b_tmp,m_ice_Bam,P)

!--------------------------------------------------------------------------------

d1=0
d2=0
area=0

do i=2,mx-2 ; do j=2,my-2

  d1=0 ; d2=0

  do x=-1,1,2
   lon1=m_lon(i,  j)* pi/180.
   lon2=m_lon(i+x,j)* pi/180
   lat1=m_lat(i,  j)* pi/180
   lat2=m_lat(i+x,j)* pi/180
   d1=d1+distance(lon2,lat2,lon1,lat1)/2.
  enddo

  do x=-1,1,2
   lon1=m_lon(i,j)* pi/180.
   lon2=m_lon(i,j+x)* pi/180
   lat1=m_lat(i,j)* pi/180
   lat2=m_lat(i,j+x)* pi/180
   d2=d2+distance(lon2,lat2,lon1,lat1)/2.
  enddo

  area(i,j)=d1*d2

enddo       ; enddo

!--------------------------------------------------------------------------------
 
fnamNC="bamber_1to5km.cdf"

call CF_INI_FILE(fnamNC,fnamNC)

call CF_CREATE_DIM("time","-",1,1)

DO i=1,mx
 dimval(i)= i
ENDDO
call CF_CREATE_DIM("x","km",mx,dimval)


DO j=1,my
 dimval(j)= j
ENDDO
call CF_CREATE_DIM("y","km",my,dimval)

call CF_CREATE_VAR("LON","Longitude","degrees","-","x","y","-" )
call CF_CREATE_VAR("LAT","Latitude" ,"degrees","-","x","y","-" )
call CF_CREATE_VAR("MSK","Bamber (2013) Soil Mask"      ,"-","-","x","y","-" )
call CF_CREATE_VAR("ICE","Bamber (2013) Ice Mask"       ,"-","-","x","y","-" )
call CF_CREATE_VAR("SRF","Bamber (2013) Surface height" ,"m","-","x","y","-" )
call CF_CREATE_VAR("SLO","Bamber (2013) Slope" ,"-","-","x","y","-" )
call CF_CREATE_VAR("BED","Bedrock height" ,"m","-","x","y","-" )

call CF_CREATE_FILE(fnamNC)

call CF_write (fnamNC, 'LON '   , 1  , mx   , my, 1 , m_lon)
call CF_write (fnamNC, 'LAT '   , 1  , mx   , my, 1 , m_lat)
call CF_write (fnamNC, 'MSK'    , 1  , mx   , my, 1 , m_msk_Bam)
call CF_write (fnamNC, 'ICE'    , 1  , mx   , my, 1 , m_ice_Bam)
call CF_write (fnamNC, 'SRF'    , 1  , mx   , my, 1 , m_srf_Bam)
call CF_write (fnamNC, 'BED'    , 1  , mx   , my, 1 , m_bed_Bam)
call CF_write (fnamNC, 'AREA'   , 1  , mx   , my, 1 , area)

dxinv2=1./(2*5000.)

do i=2,mx-1
do j=2,my-2
  slopx      =(m_srf_Bam(i+1,j)-m_srf_Bam(i-1,j))*dxinv2
  slopy      =(m_srf_Bam(i,j+1)-m_srf_Bam(i,j-1))*dxinv2

  slopTV(i,j)= sqrt(slopx *slopx + slopy *slopy)
  slopGE(i,j)= atan(slopTV(i,j))

enddo
enddo

call CF_write (fnamNC, 'SLO'    , 1  , mx   , my, 1 , real(slopGE))


!--------------------------------------------------------------------------------

end program


!--------------------------------------------------------------------------------

function distance(lon2,lat2,lon1,lat1)	

implicit none

include 'bamber.inc'

 real :: lon1,lat1
 real :: lon2,lat2
 real :: dlat,dlon,a,c

! distance = acos(sin(lat1) * sin(lat2) + &
!	    cos(lat1) * cos(lat2) * cos(lon2 - lon1)) * R

    dlat = (lat2-lat1)
    dlon = (lon2-lon1)
       a =  sin(dLat/2.) * sin(dLat/2.) + cos(lat1) * cos(lat2) * sin(dLon/2) * sin(dLon/2)
       c =  2.d0 * atan2(sqrt(a), sqrt(1-a))
distance =  R * c

end function


!--------------------------------------------------------------------------------------

subroutine interpolation(m_var,b_var,new_var,exp)

implicit none

include 'bamber.inc'


 real    :: m_var   (mx,my)
 real    :: b_var   (bx,by)
 real    :: new_var (mx,my)

 real	 :: numerat (mx,my)			!numerateur de l interpolation
 real    :: denomin (mx,my)			!denominateur de l interpolation
 real	 :: inv_dist(mx,my)

 real    :: exp					!puissance de la distance


 cpt      = 0.
 dist     = 0.
 inv_dist = 0.
 numerat  = 0.
 denomin  = 0.

 do i=1,mx ; do j=1,my

  cpt = B25_cpt(i,j)

  if(cpt == 0) then

   new_var(i,j) = m_var(i,j)

  else

   do h=1,cpt

    x    = B25_i   (i,j,h)
    y    = B25_j   (i,j,h)
    dist = B25_dist(i,j,h)

    if(dist < 1.) dist = 1.

    inv_dist(i,j) = 1./(dist**exp)
    numerat (i,j) = numerat(i,j) + (inv_dist(i,j) * b_var(x,y))
    denomin (i,j) = denomin(i,j) +  inv_dist(i,j)

   enddo

   new_var(i,j) = numerat(i,j)/denomin(i,j)

  !if(new_var(i,j) < 0.) new_var(i,j) = 0.

  endif

 enddo ; enddo

end subroutine


!--------------------------------------------------------------------------------------

!filtering binary mask after interpolation

subroutine filt_bin_mask(mask,new_mask)

implicit none

include 'bamber.inc'


 real :: mask    (mx,my)
 real :: new_mask(mx,my)


 new_mask = mask

 do i=1,mx,mx-1 ; do j=2,my-1

  cpt = 0

  do l=j-1,j+1
   cpt = cpt + new_mask(i,l)
  enddo

  if(cpt == 2  .and. new_mask(i,j) == 0) new_mask(i,j) = 1

  if(cpt == 1  .and. new_mask(i,j) == 1) new_mask(i,j) = 0

 enddo ; enddo

 do i=2,mx-1 ; do j=2,my-1

  cpt = 0

  do k=i-1,i+1 ; do l=j-1,j+1
   cpt = cpt + new_mask(k,l)
  enddo ; enddo

  if(cpt >= 8  .and. new_mask(i,j) == 0) new_mask(i,j) = 1

 !if(cpt <= 1  .and. new_mask(i,j) == 1) new_mask(i,j) = 0

 enddo ; enddo


end subroutine

!--------------------------------------------------------------------------------------



