cd d46

for file in *.f ; do
 diff $file ../d45/$file &> $file.diff

 size=`du -sk $file.diff | awk '{print $1}'`
 [ $size -eq 0 ] && rm -f $file.diff

done
