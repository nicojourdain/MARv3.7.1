#!/bin/bash

YYYY=1978
sim=52
for dir in d0* d1* d2* d3* ; do

 ln -s $dir d$sim
 YYYY=$(( $YYYY + 1 ))
 sim=$(( $sim + 1 ))

done

