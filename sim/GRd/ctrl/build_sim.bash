#!/bin/bash

YYYY=1980
sim=2
set -x
for dir in d* ; do

                  sim2=$sim
 [ $sim -le 9 ] && sim2="0$sim"

 #rm -rf     d$sim2
 #cp -r $dir d$sim2
 
 cd d$sim2
  MAR -again ./MAR.ctr* d$sim2 $YYYY 01 a
 cd ..

 YYYY=$(( $YYYY + 1 ))
 sim=$(( $sim + 1 ))

done

