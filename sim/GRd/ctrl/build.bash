#!/bin/bash

Dom=d

pwd=$PWD

rm -f */MARdt.ctr
cp MARdt-1989.ctr d19/MARdt.ctr
cp MARdt-1989.ctr d69/MARdt.ctr

cp MARdt-2006.ctr d36/MARdt.ctr
cp MARdt-2006.ctr d86/MARdt.ctr

for YYYY in `seq 1979 2015` ; do

 YY=$((  $YYYY - 1900 - 70 ))

 [ $YY -le 9 ] && YY="0$YY"

 sim=$Dom$YY
 
 cd $pwd/$sim

MAR -again ./MAR.ctr* $sim $YYYY 01 a


 cd $pwd

done
exit
for YYYY in `seq 1979 2015` ; do

 YY=$((  $YYYY - 1900 - 20 ))

 [ $YY -le 9 ] && YY="0$YY"

 sim=$Dom$YY

 cd $pwd/$sim

  MAR -again ./MAR.ctr* $sim $YYYY 01 a

 cd $pwd

done

