 
 
      block data SISVAT_dat
 
C +------------------------------------------------------------------------+
C | MAR          SISVAT_dat                                12-05-2010  MAR |
C |   SubRoutine SISVAT_dat contains the constants of the                  |
C |              Soil/Ice Snow Vegetation Atmosphere Transfer Scheme       |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
      include "MAR_SV.inc"
 
 
C +..BEGIN    "MARdSV.inc"
C +--SISVAT Global Variables
C +  =======================
 
      logical       INI_SV               ! Initialisation Switch
      common/SwchSV/INI_SV               !
      real          eps_21               ! Arbitrary  very small value
      common/GendSV/eps_21               !
 
 
C +--Snow
C +  ----
 
      integer       istdSV(5)            ! Snow History
      common/SnidSV/istdSV               !
 
      real          Cn_dSV               ! Snow Heat Capacity          [J/kg/K]
      real          SMndSV               ! Minimum Thickness of new Layers
      real          G1_dSV               ! Conversion 0/99-->0/1
      real          DDcdSV,DFcdSV,DScdSV ! Snow Grains Optical Diameter [1e-4m]
      real                        ADSdSV ! Snow Grains Actual  Diameter [1e-4m]
      real          So1dSV,So2dSV,So3dSV ! Total Solar Irradiance Fractions [-]
      real          aI1dSV,aI2dSV,aI3dSV ! Bare Ice Albedo                  [-]
      real          ws0dSV               ! Irreducible Water Saturation in Snow
      real          roCdSV               ! Pore Hole Close OFF Density  [kg/m3]
      real          roSdSV               ! Pure snow Density  [kg/m3]
      real          ru_dSV               ! Surficial Water Scale Factor [kg/m2]
 
      common/Sn_dSV/Cn_dSV,
     .              SMndSV,
     .              G1_dSV,
     .              DDcdSV,DFcdSV,DScdSV,
     .                            ADSdSV,
     .              So1dSV,So2dSV,So3dSV,
     .              aI1dSV,aI2dSV,aI3dSV,
     .              ws0dSV,
     .              roCdSV,roSdSV,
     .              ru_dSV
 
 
C +--Ice
C +  ---
 
      real          CdidSV
      common/IcedSV/CdidSV
 
 
C +--Vegetation
C +  ----------
 
      integer                nvgt
      parameter             (nvgt=12)
      real          DH_dSV(0:nvgt)       ! Displacement            Height   [m]
      real          Z0mdSV(0:nvgt)       ! Roughness  Length for Momentum   [m]
      real          StodSV(0:nvgt)       ! Minimum    Stomatal Resistance [s/m]
      real          rbtdSV(0:nvgt)       ! Roots Fraction Beta Coefficient  [-]
      real          PR_dSV(0:nvgt)       ! Internal Plant      Resistance   [s]
      real          pscdSV               ! Critical Leaf Water Potential    [m]
      real          StxdSV               ! maXimum  Stomatal   Resistance [s/m]
      real          LAIdSV               ! maximum  LAI
      common/Vg_dSV/DH_dSV,Z0mdSV,StodSV,rbtdSV,PR_dSV
     .             ,pscdSV,StxdSV,LAIdSV
 
 
C +--Soil
C +  ----
 
      real          rcwdSV               ! Density * Water Specific Heat
      real          dz_dSV(-nsol:0)      ! Vertical  Discretization
      real          zz_dSV               ! Soil      Thickness
      common/SozdSV/rcwdSV,dz_dSV,zz_dSV !
 
      integer                nsot
      parameter             (nsot=12)
      real          etadSV(0:nsot)       ! Water Content at Saturation  [m3/m3]
      real          psidSV(0:nsot)       ! Water Succion at Saturation      [m]
      real          Ks_dSV(0:nsot)       ! Hydraulic Conductivity
                                         !               at Saturation    [m/s]
      real          bCHdSV(0:nsot)       ! Clapp-Hornberger Coefficient b   [-]
      common/So_dSV/etadSV,psidSV,Ks_dSV,bCHdSV
 
 
C +--Water Bodies
C +  ------------
 
      real          vK_dSV               ! Diffusivity in Water          [m2/s]
      real          TSIdSV               ! Sea-Ice Fraction: SST Scale      [K]
      common/Wa_dSV/vK_dSV,TSIdSV        !
 
 
C +..END      "MARdSV.inc"
 
 
C +--Internal Variables
C +  ==================
 
      integer               ivg,iso
      common/SISVAT_dat_Loc/ivg,iso
 
 
C +--General DATA
C +  ============
 
      data          INI_SV /  .false./   ! Initialisation  Switch
      data          eps_21 /   1.e-21/   ! Arbitrary  very small value
 
 
C +--Snow
C +  ----
 
      data          Cn_dSV /2105.    /   ! Snow Heat Capacity          [J/kg/K]
                                         ! Loth et al. 1993, JGR 98 D6
                                         ! 2.2.2 2e para p.10453
      data          SMndSV /   1.00  /   ! New Snow Layer Min.Thickn. [mm w.e.]
      data          G1_dSV /  99.    /   ! Conversion 0/99-->0/1
                                         ! Sphericity/Dendricity
                                         ! Optical Diameter of:
      data          DDcdSV /   1.    /   ! Dendritic     Crystals    [0.0001 m]
      data          DFcdSV /   4.    /   ! Young Faceted Crystals    [0.0001 m]
      data          DScdSV /   3.    /   ! Small         Crystals    [0.0001 m]
                                         ! Actual  Diameter of:
      data          ADSdSV /   4.    /   ! Small         Crystals    [0.0001 m]
      data          istdSV /1,2,3,4,5/   ! Snow  History:
                                         ! 1:             faceted cristal
                                         ! 2: liq.watr/no faceted cristal befor
                                         ! 3: liq.watr/   faceted cristal befor
 
C +--Fractions of total Solar Irradiance  in 3 spectral Intervals
C +  (see Feagle and Businger 1981, Int.Geoph.Ser. 25, p.215-222)
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
c     data          So1dSV /   0.606 /   !                 0.3--0.8mim Interval
c     data          So2dSV /   0.301 /   !                 0.8--1.5mim Interval
c     data          So3dSV /   0.093 /   !                 1.5--2.8mim Interval
cXF   data          So1dSV /   0.580 /   ! Tuning ETH camp 0.3--0.8mim Interval
cXF   data          So2dSV /   0.320 /   ! Tuning ETH camp 0.8--1.5mim Interval
cXF   data          So3dSV /   0.100 /   ! Tuning ETH camp 1.5--2.8mim Interval
      data          So1dSV /   0.6 /   !                   0.3--0.8mim Interval
      data          So2dSV /   0.3 /   !                   0.8--1.5mim Interval
      data          So3dSV /   0.1 /   !                   1.5--2.8mim Interval
c #AC data          aI1dSV /   0.50  /   ! Minimum bare ICE albedo
      data          aI1dSV /   0.35  /   ! Minimum bare ICE albedo
      data          aI1dSV /   0.50  /   ! Minimum bare ICE albedo
      data          aI2dSV /   0.55  /   ! Maximum bare ICE albedo
      data          aI3dSV /   0.70  /   ! ICE lense albedo at roCdSV kg/m3
                                         ! and minimum pure snow albedo
      data          ru_dSV /     200./   ! Surficial Water Scale Factor [kg/m2]
 
 
C +--Water in Snow
C +  -------------
 
      data          ws0dSV /   0.1  /    ! Irreducible Water Saturation in Snow
C +                                      ! 0.07      : Coleou et al., 1998, A.Gl
                                         ! 0.08-0.15 : Greuell & Konzelmann (199
cXF
      data          roSdSV / 550.    /   ! Max pure snow Density [kg/m3]
      data          roCdSV / 830.    /   ! Pore Hole Close OFF Density  [kg/m3]
C +                                      ! 800: Greuell & Konzelmann (1994), Glo
C +                                      ! 830: Harper et al. (2012), Nature
cXF
 
C +--Ice
C +  ---
 
      data          CdidSV /   2.1   /   ! Conductivity of pure  Ice    [W/m/K]
 
 
C +--Vegetation                (SVAT Classification)
C +  -----------------------------------------------
 
 
      data     (DH_dSV(ivg),                             ! Displac.  Height [m]
     .                 Z0mdSV(ivg),                      ! Roughness L./Mom [m]
     .                        StodSV(ivg),               ! Min.Stom.Resis.[s/m]
     .                               PR_dSV(ivg),        ! Plant Resistance [s]
     .                                      rbtdSV(ivg), ! Root beta coeff. [-]
     .                                              ivg=0,nvgt)
     .         /0.00,  0.01,5000.,   0.0,   0.000,       !  0 NO     VEGETATION
     .          0.07,  0.01,  50.,   0.5e9, 0.961,       !  1 CROPS      LOW
     .          0.21,  0.03,  50.,   0.5e9, 0.961,       !  2 CROPS      MEDIUM
     .          0.70,  0.10,  50.,   0.5e9, 0.961,       !  3 CROPS      HIGH
     .          0.07,  0.01,  50.,   0.5e9, 0.943,       !  4 GRASS      LOW
     .          0.21,  0.03,  50.,   0.5e9, 0.964,       !  5 GRASS      MEDIUM
     .          0.70,  0.10,  50.,   0.5e9, 0.972,       !  6 GRASS      HIGH
     .          1.40,  0.20,  10.,   1.0e9, 0.968,       !  7 BROADLEAF  LOW
     .          5.60,  0.80,  10.,   1.0e9, 0.962,       !  8 BROADLEAF  MEDIUM
     .         14.00,  2.00,  10.,   1.0e9, 0.962,       !  9 BROADLEAF  HIGH
     .          1.40,  0.20,  10.,   1.0e9, 0.971,       ! 10 NEEDLELEAF LOW
     .          5.60,  0.80,  10.,   1.0e9, 0.976,       ! 11 NEEDLELEAF MEDIUM
     .         14.00,  2.00,  10.,   1.0e9, 0.976/       ! 12 NEEDLELEAF HIGH
 
cXF  + see the table in VgOptP !!!!
 
      data      pscdSV / 250.  /                         ! Crit.Leaf Water Pot.
      data      StxdSV /5000.  /                         ! MAX Stomatal Resist.
      data      LAIdSV /   4.  /                         ! MAX LAI
 
 
C +--Soil
C +  ----
 
      data          rcwdSV /4.180e+6/                    ! Water: Density
C +                                                      !      * Spec.Heat
C +--Soil Vertical Discretization
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      data         (dz_dSV(iso),iso=-4,0)
     .                     /0.72,0.20,0.060,0.019,0.001/ ! Layer's Thickness
 
C +--Soil Hydraulic Parameters (USDA Classification)
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      data         (etadSV(iso),
     .                        psidSV(iso),
     .                                  Ks_dSV(iso),
     .                                            bCHdSV(iso),
     .                                                   iso=0,nsot)
     .             / 1.000,    1.000,   0.0e00,     1.00, !  0 WATER
     .               0.395,    0.121, 176.0e-6,     4.05, !  1 SAND
     .               0.410,    0.090, 156.3e-6,     4.38, !  2 LOAMY      SAND
     .               0.435,    0.218,  34.1e-6,     4.90, !  3 SANDY      LOAM
     .               0.485,    0.786,   7.2e-6,     5.30, !  4 SILT       LOAM
     .               0.451,    0.478,   7.0e-6,     5.39, !  5            LOAM
     .               0.420,    0.299,   6.3e-6,     7.12, !  6 SANDY CLAY LOAM
     .               0.477,    0.356,   1.7e-6,     7.75, !  7 SILTY CLAY LOAM
     .               0.476,    0.630,   2.5e-6,     8.52, !  8       CLAY LOAM
     .               0.426,    0.153,   2.2e-6,    10.40, !  9 SANDY CLAY
     .               0.492,    0.490,   1.0e-6,    10.40, ! 10 SILTY CLAY
     .               0.482,    0.405,   1.3e-6,    11.40, ! 11       CLAY
     .               0.001,    0.001,   0.0e00,     0.02/ ! 12       ICE
C +!!!Revoir les donnees pour la glace
 
 
C +--Water Bodies
C +  ------------
 
      data          vK_dSV /1000.    /   ! Diffusivity in Water          [m2/s]
      data          TSIdSV /   0.50  /   ! Sea-Ice Fraction: SST Scale      [K]
 
      end
