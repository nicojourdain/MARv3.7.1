      subroutine INIsic(ihamr_sic,nhamr_sic,newsicINI)
 
C +------------------------------------------------------------------------+
C | MAR INPUT    Sea-Ice                                   03-03-2004  MAR |
C |   SubRoutine INIsic is used to initialize MAR Sea-Ice    Fractions     |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:  ihamr_sic: Time Digital Filter Status                        |
C |   ^^^^^   nhamr_sic: Time Digital Filter Set Up                        |
C |                                                                        |
C |   OUTPUT: newsicINI: (0,1) ==> (NO new sic , new sic)                  |
C |   ^^^^^^^                                                              |
C |                                                                        |
C |   OUTPUT: sicsIB: Current                     Sea-Ice    Fraction      |
C |   ^^^^^^^ sic1sI: Previous Nesting Time Step  Sea-Ice    Fraction      |
C |           sic2sI: Next     Nesting Time Step  Sea-Ice    Fraction      |
C |           tim1sI,tim2sI:   Times  n, n+1  of  Sea-Ice    Fraction      |
C |                                                                        |
C |   CAUTION: It is assumed that tim1sI and tim2sI do not change when the |
C |   ^^^^^^^^ Variables are reassigned after the dynamical Initialization |
C |            (Reassignation => itexpe := nham => timar := timar-nham*dt) |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
 
      include 'MAR_SL.inc'
      include 'MAR_SV.inc'
      include 'MAR_TV.inc'
      include 'MARsIB.inc'
 
      integer  ihamr_sic,nhamr_sic
      integer  newsicINI
 
 
C +--Local  Variables
C +  ================
 
cXF
      integer*8  itisIB
      real       rate
 
 
C +--Current Time
C +  ============
 
          itisIB=ou2sGE(iyrrGE,mmarGE,jdarGE,jhurGE,minuGE,jsecGE)
c #HF.          + (ihamr_sic+nhamr_sic)                           * idt
 
 
C +--Reinitialization of the Sea-Ice Fraction
C +  ----------------------------------------
 
      IF (iterun.eq.0)                                            THEN
          jdh_sI = 1
          iyr_sI = iyrrGE
          mma_sI = mmarGE
          jda_sI = jdarGE
          jhu_sI = jhurGE
          tim1sI = itisIB
          tim2sI = itisIB
       DO j=1,my
       DO i=1,mx
         sic1sI(i,j) = sicsIB(i,j)
         sic2sI(i,j) = sicsIB(i,j)
       END DO
       END DO
 
      END IF
 
 
C +--New sIB
C +  =======
 
      IF (itisIB.gt.   tim2sI)                                    THEN
 
          tim1sI =     tim2sI
 
          write(6,6001)jda_sI,labmGE(mma_sI),iyr_sI,
     .                 jhu_sI,                      tim1sI,
     .                 jdarGE,labmGE(mmarGE),iyrrGE,
     .                 jhurGE,minuGE,        jsecGE,itisIB
 6001     format(/, '  1st sIB /',i3,'-',a3,'-',i4,i3,' ',2x,'/',2x,
     .              '   t =',i12,'s A.P.',
     .           /, '  Current /',i3,'-',a3,'-',i4,i3,':',i2,':',i2,
     .              '   t =',i12)
C +
       IF (jdh_sI.eq.0)jdh_sI = -1
       open (unit=11,status='old',form='unformatted',file='MARsic.DAT')
       rewind     11
 11    CONTINUE
       IF (jdh_sI.le.0)                                       GO TO 10
 
 
C +--sIB at nesting time step n
C +  --------------------------
 
       DO j=1,my
       DO i=1,mx
         sic1sI(i,j) = sic2sI(i,j)
         sic2sI(i,j) = 0.d0
       END DO
       END DO
 
 
C +--sIB at nesting time step n+1
C +  ----------------------------
 
       read      (11) iyr_sI,mma_sI,jda_sI,jhu_sI,jdh_sI
       read      (11) sic2sI
 
          tim2sI=ou2sGE(iyr_sI,mma_sI,jda_sI,jhu_sI,0,0)
 
       IF(itisIB.gt.tim2sI)                                   GO TO 11
 
          write(6,6002)jda_sI,labmGE(mma_sI),iyr_sI,
     .                 jhu_sI,jdh_sI,               tim2sI
 6002     format(   '  2nd sIB /',i3,'-',a3,'-',i4,i3,' ',2x,'/(',i1,
     .              ')  t =',i12)
 
 10    CONTINUE
       close(unit=11)
 
      ELSE
c #WR     write(6,6003)jdarGE,labmGE(mmarGE),iyrrGE,
c #WR.                 jhurGE,minuGE,        jsecGE,itisIB
 6003     format(   '  Current /',i3,'-',a3,'-',i4,i3,':',i2,':',i2,
     .              '   t =',i12,'s A.P.')
      END IF
 
 
C +--Time Interpolation
C +  ==================
 
      IF            (itisIB.le.tim2sI  .and.   tim1sI.lt.tim2sI)  THEN
 
        rate = float(itisIB  - tim1sI) / float(tim2sI  - tim1sI)
        DO j=1,my
        DO i=1,mx
c #AO       sic1sI(i,j)=sicsIB(i,j)
c #AO       sic2sI(i,j)=sicsIB(i,j)
            sicsIB(i,j)=sic1sI(i,j) +
     .     (sic2sI(i,j)-sic1sI(i,j))*rate
        END DO
        END DO
 
        newsicINI = 1
 
      ELSE
        newsicINI = 0
      END IF
 
      return
      end
