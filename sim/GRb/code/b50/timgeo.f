 
 
      subroutine TIMgeo
 
C +------------------------------------------------------------------------+
C | MAR TIME                                                1-11-2001  MAR |
C |   SubRoutine TIMgeo computes Current Universal and Local Times         |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |    INPUT (via common block)                                            |
C |    ^^^^^  itexpe: Experiment Iteration Counter                         |
C |           itizGE(mx,my): Time Zone                                     |
C |                                                                        |
C |   OUTPUT (via common block)                                            |
C |   ^^^^^^  iyrrGE       : year                                          |
C |           mmarGE       : Month                                         |
C |           jdarGE       : Day                                           |
C |           jhurGE       : Universal Time                      (Hour UT) |
C |           jhlrGE(mx,my): Local     Time                      (Hour LT) |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
 
 
C +--Local  Variables
C +  ================
cXF
      integer   njmo,leap
      integer*8 jdarGE8,jhurGE8,minuGE8,jsecGE8
CXF
 
C +--Universal Time
C +  ==============
 
          iyrrGE = iyr0GE
          mmarGE = mma0GE
          jdarGE8= jda0GE
          jhurGE8= jhu0GE
          minuGE8=          0
          jsecGE8= itexpe*idt
 240  CONTINUE
      if (jsecGE8.lt.  60)                                    GO TO 241
          jsecGE8= jsecGE8- 60
          minuGE8= minuGE8+ 1
      go to 240
 241  CONTINUE
      if (minuGE8.lt.  60)                                    GO TO 242
          minuGE8= minuGE8- 60
          jhurGE8= jhurGE8+ 1
      go to 241
 242  CONTINUE
      if (jhurGE8.lt.  24)                                    GO TO 243
          jhurGE8= jhurGE8- 24
          jdarGE8= jdarGE8+ 1
      go to 242
 243  CONTINUE
cXF
                                                        leap=1
          if(mod(iyrrGE,100)==0.and.mod(iyrrGE,400)/=0) leap=0
cXF
 
          njmo   =          njmoGE(mmarGE)
     .                     +njmbGE(mmarGE)*max(0,1-mod(iyrrGE,4))
     .                     *leap
 
      if (jdarGE8.le.       njmo)                             GO TO 244
          jdarGE8= jdarGE8- njmo
          mmarGE = mmarGE + 1
      if (mmarGE .le.  12)                                    GO TO 243
          mmarGE = mmarGE - 12
          iyrrGE = iyrrGE + 1
      go to 243
 244  CONTINUE
 
 
C +--Local     Time
C +  ==============
 
      DO j=1,my
      DO i=1,mx
                               jhlrGE(i,j) = jhurGE8 + itizGE(i,j)
        IF (jhlrGE(i,j).ge.24) jhlrGE(i,j) = jhlrGE(i,j) - 24
        IF (jhlrGE(i,j).lt. 0) jhlrGE(i,j) = jhlrGE(i,j) + 24
      END DO
      END DO
 
      jdarGE = jdarGE8
      jhurGE = jhurGE8
      minuGE = minuGE8
      jsecGE = jsecGE8
 
      return
      end
