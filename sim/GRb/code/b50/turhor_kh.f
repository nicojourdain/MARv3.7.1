 
 
      subroutine TURhor_kh
 
C +------------------------------------------------------------------------+
C | MAR TURBULENCE HORIZONTAL                              30-12-2000  MAR |
C |   SubRoutine TURhor_kh computes the Horizontal Diffusion Coefficient   |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT  (via common block)                                            |
C |   ^^^^^   uairDY(i,j,k): Horizontal Wind Speed (x-Direction)     (m/s) |
C |           vairDY(i,j,k): Horizontal Wind Speed (y-Direction)     (m/s) |
C |                                                                        |
C |   OUTPUT (via common block)                                            |
C |   ^^^^^^   TUkhx(i,j,k): Horizont.Diff.Coefficient (i+1/2,j,k)  (m2/s) |
C |            TUkhy(i,j,k): Horizont.Diff.Coefficient (i,j+1/2,k)  (m2/s) |
C |                                                                        |
C |   REFER.: Tag et al., JAM 18, 1429--1441, 1979                         |
C |   ^^^^^^^                                                              |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_DY.inc'
 
      include 'MAR_TU.inc'
 
      include 'MAR_WK.inc'
 
 
C +--Reset of the Horizontal Diffusion Coefficient
C +  =============================================
 
      DO k=1,mz
      DO j=1,my
      DO i=1,mx
        TUkhx(i,j,k) = 0.0
        TUkhy(i,j,k) = 0.0
      END DO
      END DO
      END DO
 
 
C +--2D Model Version
C +  ================
 
      IF (mmy.eq.1)                                               THEN
 
           j=1
        DO k=1,mz
        DO i=1,mx
          WKxza(i,k)=uairDY(ip1(i),j,k)-uairDY(i,j,k)
          WKxzb(i,k)=vairDY(ip1(i),j,k)-vairDY(i,j,k)
        END DO
        END DO
 
        DO k=1,mz
          DO i=1,mx
            TUkhx( i,j,k) = TUkhff*dx
     .          *(sqrt(0.5*(WKxzb(i,k)  *WKxzb(i,k) )
     .                     +WKxza(i,k)  *WKxza(i,k)  ))
            WKxza( i,  k) = 0.0
            WKxzb( i,  k) = 0.0
          END DO
            TUkhx(mx,j,k) = 0.0
        END DO
 
 
C +--Upper Absorbing Layer
C +  ---------------------
 
        IF (TUkhmx.gt.0.0)                                        THEN
          DO k=1,mzabso
          DO j=1,my
          DO i=1,mx
            TUkhx(i,j,k) = TUkhx(i,j,k) + TUspon(k)
          END DO
          END DO
          END DO
        END IF
 
 
C +--3D Model Version
C +  ================
 
      ELSE
 
!$OMP PARALLEL DO private(i,j,k)
        DO k=1,mz
 
 
C +--x Direction
C +  -----------
 
          DO i=1,mx
          DO j=jp11,my1
           WKxyz1(i,j,k) =
     .               vairDY(ip1(i),    j ,k)-vairDY(i,    j ,k)
           WKxyz3(i,j,k) =
     .               uairDY(ip1(i),    j ,k)-uairDY(i,    j ,k)
          ENDDO
          ENDDO
 
          DO j=jp11,my1
          DO i=1,mx
           WKxyz2(i,j,k) =
     .               uairDY(    i ,jp1(j),k)-uairDY(i,jm1(j),k)
           WKxyz4(i,j,k) =
     .               vairDY(    i ,jp1(j),k)-vairDY(i,jm1(j),k)
           WKxyz5(i,j,k) =
     .               vairDY(    i ,jp1(j),k)
           WKxyz6(i,j,k) =
     .               vairDY(    i ,jm1(j),k)
          ENDDO
          ENDDO
 
          DO i=1,mx
          DO j=jp11,my1
            TUkhx(i,j,k) = TUkhff *dx *(sqrt(
     .         0.5*((WKxyz1(    i ,    j ,k)
     .       + 0.5* (WKxyz2(    i ,    j ,k)))**2)
     .       +      (WKxyz3(    i ,    j ,k)) **2
     .       +(0.25*(WKxyz4(    i ,    j ,k)
     .              +WKxyz5(ip1(i),    j ,k)
     .              -WKxyz6(ip1(i),    j ,k)))**2))
 
 ! The 3 previous Loops Stand for the following non-vectorized Loop:
 !          TUkhx(i,j,k) = TUkhff *dx *(sqrt(
 !   .         0.5*((vairDY(ip1(i),    j ,k)-vairDY(i,    j ,k)
 !   .       + 0.5* (uairDY(i     ,jp1(j),k)-uairDY(i,jm1(j),k)))**2)
 !   .       +      (uairDY(ip1(i),    j ,k)-uairDY(i,    j ,k)) **2
 !   .       +(0.25*(vairDY(i     ,jp1(j),k)-vairDY(i,jm1(j),k)
 !   .              +vairDY(ip1(i),jp1(j),k)
 !   .              -vairDY(ip1(i),jm1(j),k)))**2))
          END DO
          END DO
 
 
C +--y Direction
C +  -----------
 
          DO j=1,my
          DO i=ip11,mx1
           WKxyz1(i,j,k) =
     .               vairDY(    i ,jp1(j),k)-vairDY(    i ,j,k)
           WKxyz3(i,j,k) =
     .               uairDY(    i ,jp1(j),k)-uairDY(    i ,j,k)
          ENDDO
          ENDDO
 
          DO i=ip11,mx1
          DO j=1,my
           WKxyz2(i,j,k) =
     .               uairDY(ip1(i),    j ,k)-uairDY(im1(i),j,k)
           WKxyz4(i,j,k) =
     .               vairDY(ip1(i),    j ,k)-vairDY(im1(i),j,k)
           WKxyz5(i,j,k) =
     .               vairDY(ip1(i),    j ,k)
           WKxyz6(i,j,k) =
     .               vairDY(im1(i),    j ,k)
          ENDDO
          ENDDO
 
          DO j=1,my
          DO i=ip11,mx1
            TUkhy(i,j,k) = TUkhff *dx *(sqrt(
     .         0.5*((WKxyz1(    i ,    j ,k)
     .       + 0.5* (WKxyz2(    i ,    j ,k)))**2)
     .       +      (WKxyz3(    i ,    j ,k)) **2
     .       +(0.25*(WKxyz4(    i ,    j ,k)
     .              +WKxyz5(    i ,jp1(j),k)
     .              -WKxyz6(    i ,jp1(j),k)))**2))
 
 ! The 3 previous Loops Stand for the following non-vectorized Loop:
 !          TUkhy(i,j,k) = TUkhff *dx *(sqrt(
 !   .         0.5*((vairDY(    i ,jp1(j),k)-vairDY(    i ,j,k)
 !   .       + 0.5* (uairDY(ip1(i),    j ,k)-uairDY(im1(i),j,k)))**2)
 !   .       +      (uairDY(    i ,jp1(j),k)-uairDY(    i ,j,k)) **2
 !   .       +(0.25*(vairDY(ip1(i),    j ,k)-vairDY(im1(i),j,k)
 !   .              +vairDY(ip1(i),jp1(j),k)
 !   .              -vairDY(im1(i),jp1(j),k)))**2))
          ENDDO
          ENDDO
 
          DO i=1,mx
          DO j=jp11,my1
            WKxyz1(i,j,k) = 0.
            WKxyz2(i,j,k) = 0.
            WKxyz3(i,j,k) = 0.
            WKxyz4(i,j,k) = 0.
            WKxyz5(i,j,k) = 0.
            WKxyz6(i,j,k) = 0.
          END DO
          END DO
 
        END DO
!$OMP END PARALLEL DO
 
C +--Upper Absorbing Layer
C +  ---------------------
 
        IF (TUkhmx.gt.0.0)                                        THEN
 
          DO k=1,mzabso
          DO j=1,my
          DO i=1,mx
            TUkhx(i,j,k) = TUkhx(i,j,k) + TUspon(k)
            TUkhy(i,j,k) = TUkhy(i,j,k) + TUspon(k)
          END DO
          END DO
          END DO
 
        END IF
 
      END IF
 
 
C +--Lateral Boundary Values
C +  -----------------------
 
c #OB IF (mmx.gt.1)                                               THEN
c #OB   DO k=1,mz
c #OB   DO j=1,my
c #OB     TUkhx( 1, j,k) = 0.0
c #OB     TUkhx(mx, j,k) = 0.0
c #OB     TUkhy( 1, j,k) = 0.0
c #OB     TUkhy(mx, j,k) = 0.0
c #OB   END DO
c #OB   END DO
c #OB END IF
 
c #OB IF (mmy.gt.1)                                               THEN
c #OB   DO k=1,mz
c #OB   DO i=1,mx
c #OB     TUkhx( i, 1,k) = 0.0
c #OB     TUkhx( i,my,k) = 0.0
c #OB     TUkhy( i, 1,k) = 0.0
c #OB     TUkhy( i,my,k) = 0.0
c #OB   END DO
c #OB   END DO
c #OB END IF
 
 
      return
      end
