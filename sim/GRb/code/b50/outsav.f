 
 
      subroutine OUTsav
 
C +------------------------------------------------------------------------+
C | MAR OUTPUT                                        Mon 23-May-2011  MAR |
C |   SubRoutine OUTsav is used to save the main Model Variables           |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
 
      include 'MAR_DY.inc'
c #NH include 'MAR_NH.inc'
 
      include 'MAR_LB.inc'
      include 'MAR_UB.inc'
      include 'MARsIB.inc'
 
      include 'MAR_TE.inc'
      include 'MAR_TU.inc'
 
      include 'MAR_RA.inc'
 
      include 'MAR_HY.inc'
      include 'MAR_CA.inc'
      include 'MAR_PB.inc'
c #TC include 'MAR_TC.inc'
 
      include 'MAR_SL.inc'
c #PO include 'MAR_PO.inc'
      include 'MAR_SV.inc'
c #sn include 'MAR_SN.inc'
      include 'MAR_BS.inc'
 
      include 'MAR_IO.inc'
 
 
C +--    Hydrostatic Dynamics
C +  ========================
C +
          open (unit=11,status='unknown',form='unformatted',
     .                                   file='MARdyn.DAT')
          rewind     11
          write     (11) itexpe,jdh_LB
          write     (11) iyrrGE,mmarGE,jdarGE,jhurGE
C +...    Time    Parameters
 
          write     (11) imez,jmez
          write     (11) GElat0,GElon0
C +...    Spatial Parameters
 
          write     (11) sigma,ptopDY,dx,dy
C +...    Discretisation
 
          write     (11) uairDY
          write     (11) vairDY
          write     (11) pktaDY
          write     (11) pstDY
          write     (11) qvDY
C +...    Dynamics
 
          write     (11) sh
          write     (11) pstDY1
 
          write     (11) iyr_LB,mma_LB,jda_LB,jhu_LB,jdh_LB
          write     (11) vaxgLB,vaxdLB,vayiLB,vaysLB
C +...    Lateral Boundary Conditions
 
          write     (11) sst_LB
 
          write     (11) uairUB,vairUB,pktaUB
C +...    Upper   Sponge   Reference State
 
          write     (11) pstDYn
          write     (11) RAd_ir
          write     (11) IRsoil
          write     (11)  virDY
          write     (11) tim1LB,v1xgLB,v1xdLB,v1yiLB,v1ysLB
          write     (11) tim2LB,v2xgLB,v2xdLB,v2yiLB,v2ysLB
          write     (11) sst1LB,sst2LB
          write     (11) ua1_UB,ua2_UB
          write     (11) va1_UB,va2_UB
          write     (11) pkt1UB,pkt2UB
C +
          IF (my.EQ.1)                                            THEN
            write   (11) ugeoDY
            write   (11) vgeoDY
          END IF
C +
          close(unit=11)
C +
C +
C +--Non-Hydrostatic Dynamics
C +  ========================
C +
c #NH     open (unit=11,status='unknown',form='unformatted',
c #NH.                                   file='MARonh.DAT')
c #NH     rewind     11
c #NH     write     (11) itexpe
c #NH     write     (11) iyrrGE,mmarGE,jdarGE,jhurGE
C +...    Time    Parameters
C +
c #NH     write     (11) ua0_NH
c #NH     write     (11) va0_NH
c #NH     write     (11) wa0_NH
c #NH     write     (11) wairNH
c #NH     write     (11) pairNH
C +...    Dynamics
C +
c #NH     close(unit=11)
 
 
C +--Mass Flux convective Scheme
C +  ===========================
 
        IF (convec)                                               THEN
          open (unit=11,status='unknown',form='unformatted',
     .                                   file='MARcva.DAT')
          rewind     11
          write     (11) itexpe
          write     (11) iyrrGE,mmarGE,jdarGE,jhurGE
C +...    Time    Parameters
 
          write     (11) adj_CA
          write     (11) int_CA
          write     (11) dpktCA
          write     (11) dqv_CA
          write     (11) dqw_CA
          write     (11) dqi_CA
          write     (11) drr_CA
          write     (11) dss_CA
          write     (11) dsn_CA
          write     (11) rainCA
          write     (11) snowCA
          write     (11) tau_CA
 
          write     (11) Kstep1
          write     (11) K_CbT1
          write     (11) K_CbB1
          write     (11) P_CH_0
          write     (11) PdCH_1
          write     (11) PdTa_1
          write     (11) PdQa_1
          write     (11) PdQw_1
          write     (11) PdQi_1
          write     (11) Pdrr_1
          write     (11) Pdss_1
          write     (11) PuMF_1
          write     (11) PdMF_1
          write     (11) Pfrr_1
          write     (11) Pfss_1
          write     (11) Pcape1
 
        END IF
 
 
C +--Microphysics
C +  ============
 
        IF (micphy)                                               THEN
 
          open (unit=11,status='unknown',form='unformatted',
     .                                   file='MARcld.DAT')
          rewind     11
          write     (11) itexpe
          write     (11) iyrrGE,mmarGE,jdarGE,jhurGE
 
          write     (11) turnHY
          write     (11) ccniHY
          write     (11) qiHY
          write     (11) qsHY
! #qg     write     (11) qgHY
          write     (11) qwHY
          write     (11) qrHY
          write     (11) rainHY,rai0HY
          write     (11) snowHY,sno0HY,sfa0HY
          write     (11) crysHY
          write     (11) rainCA
c #BS     write     (11) uss_HY
 
          close(unit=11)
 
        END IF
 
 
C +--Atmospheric Tracers
C +  ===================
 
c #TC     open (unit=11,status='unknown',form='unformatted',
c #TC.                                   file='MARtca.DAT')
c #TC     rewind     11
c #TC     write     (11) itexpe
c #TC     write     (11) iyrrGE,mmarGE,jdarGE,jhurGE
c #TC     write     (11) dt_ODE,dt2ODE,nt_ODE,jt_ODE
C +
c #TC     write     (11) qxTC
c #TC     write     (11) qsTC
c #TC     write     (11) uqTC
C +
c #TC     close(unit=11)
C +
C +
C +--Polynya Model
C +  =============
C +
c #PO   IF (polmod)                                               THEN
C +
c #PO     open (unit=11,status='unknown',form='unformatted',
c #PO.                                   file='MARpol.DAT')
c #PO     rewind     11
c #PO     write     (11) itexpe
c #PO     write     (11) iyrrGE,mmarGE,jdarGE,jhurGE
C +
c #PO     write     (11) isolSL
c #PO     write     (11) iPO1,iPO2,jPO1,jPO2,iPO3,iPO4,jPO3,jPO4
c #PO     write     (11) hfraPO,vgriPO,uocnPO,vocnPO,swsaPO,focnPO
c #PO     write     (11) silfPO,hicePO,aicePO,uicePO,vicePO,dtPO
C +
c #PO     close(unit=11)
C +
c #PO   END IF
C +
C +
C +--Snow Model
C +  ==========
C +
        IF (snomod.AND..NOT.VSISVAT)                              THEN
C +
          open (unit=11,status='unknown',form='unformatted',
     .                                   file='MARsno.DAT')
          rewind     11
c #sn     write     (11) itexpe,ntSNo ,ntwaSN
c #sn     write     (11)        dtSNo ,dtwaSN
c #sn     write     (11) iyrrGE,mmarGE,jdarGE,jhurGE
C +
c #sn     write     (11) agSNow
c #sn     write     (11) tiSNow
c #sn     write     (11) waSNow
c #sn     write     (11) roSNow
c #sn     write     (11) dzSNow
c #sn     write     (11) g1SNow
c #sn     write     (11) g2SNow
c #sn     write     (11) nhSNow
c #sn     write     (11) nsSNow
c #sn     write     (11) niSNow
c #sn     write     (11) smbalSN0
c #sn     write     (11) znSNow0
c #sn     write     (11) maskSN
c #sn     write     (11) slopSN
c #sn     write     (11) waSNru
c #BS     write     (11) FacRBS,FacSBS,FacTBS,FacUBS,
c #BS.                   g1__BS,g2__BS,sheaBS
C +
          close(unit=11)
C +
        END IF
 
 
C +--Soil Model
C +  ==========
 
          open (unit=11,status='unknown',form='unformatted',
     .                                   file='MARsol.DAT')
          rewind     11
          write     (11) itexpe
          write     (11) iyrrGE,mmarGE,jdarGE,jhurGE
 
          write     (11) nSLsrf
          write     (11) SLsrfl
          write     (11) TairSL
          write     (11) tsrfSL
          write     (11) alb0SL,eps0SL
          write     (11) SaltSL
          write     (11) ro_SL0
          write     (11) ro_SL
          write     (11) d1_SL
          write     (11) t2_SL
          write     (11) w2_SL,wg_SL
          write     (11) roseSL
          write     (11) qvapSL
          write     (11) hsnoSL
          write     (11) hmelSL
 
          write     (11) SLuusl,SL_z0
          write     (11) SLutsl,SL_r0
 
          write     (11) pktaSL
          write     (11) sicsIB
          write     (11) sic1sI,sic2sI
          write     (11) albeSL
          write     (11) SLuus ,SLuts
          write     (11) SLuqs ,SLuqsl
          write     (11) duusSL
          write     (11) dutsSL
          write     (11) cdmSL ,cdhSL
          write     (11) V_0aSL
          write     (11) dT0aSL
c #AM     write     (11) u_0aSL
c #AT     write     (11) uT0aSL
c #AS     write     (11) us0aSL
c #VX     write     (11) WV__SL
          write     (11) SLlmo ,SLlmol
c #BV     write     (11)  virSL
 
          close(unit=11)
 
 
C +--SVAT Model
C +  ==========
 
        IF (vegmod)                                               THEN
 
C +       ***********
          call SVAsav('writ')
C +       ***********
 
        END IF
 
 
C +--Turbulence
C +  ==========
C +
          open (unit=11,status='unknown',form='unformatted',
     .                                   file='MARtur.DAT')
          rewind     11
          write     (11) itexpe
          write     (11) iyrrGE,mmarGE,jdarGE,jhurGE
C +
          write     (11) ect_TE
          write     (11) eps_TE
          write     (11) tranTE
C +...    TURBULENT KINETIC ENERGY (TKE) and DISSIPATION (e)
C +
          write     (11) TUkvm
          write     (11) TUkvh
C +...    TURBULENT DIFFUSION COEFFICIENT
C +
          close(unit=11)
C +
      return
      end
