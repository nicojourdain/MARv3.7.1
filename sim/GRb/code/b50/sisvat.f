 
 
      subroutine SISVAT
 
C +------------------------------------------------------------------------+
C | MAR          SISVAT                               Mon 23-Jan-2012  MAR |
C |   SubRoutine SISVAT contains the fortran 77 code of the                |
C |              Soil/Ice Snow Vegetation Atmosphere Transfer Scheme       |
C |                                                                        |
C +------------------------------------------------------------------------+
C |   PARAMETERS:  klonv: Total Number of columns =                        |
C |   ^^^^^^^^^^        = Total Number of continental     grid boxes       |
C |                     X       Number of Mosaic Cell per grid box         |
C |                                                                        |
C |   INPUT:   daHost   : Date Host Model                                  |
C |   ^^^^^                                                                |
C |                                                                        |
C |   INPUT:   LSmask   : 1:          Land       MASK                      |
C |   ^^^^^               0:          Sea        MASK                      |
C |            ivgtSV   = 0,...,12:   Vegetation Type                      |
C |            isotSV   = 0,...,12:   Soil       Type                      |
C |                       0:          Water,          Liquid (Sea, Lake)   |
C |                      12:          Water, Solid           (Ice)         |
C |                                                                        |
C |   INPUT:   coszSV   : Cosine of the Sun Zenithal Distance          [-] |
C |   ^^^^^    sol_SV   : Surface Downward  Solar      Radiation    [W/m2] |
C |            IRd_SV   : Surface Downward  Longwave   Radiation    [W/m2] |
C |            drr_SV   : Rain  Intensity                        [kg/m2/s] |
C |            dsn_SV   : Snow  Intensity                      [mm w.e./s] |
C |            dsnbSV   : Snow  Intensity,  Drift Fraction             [-] |
C |            dbs_SV   : Drift Amount                           [mm w.e.] |
C |            za__SV   : Surface Boundary Layer (SBL) Height          [m] |
C |            VV__SV   :(SBL Top)   Wind Velocity                   [m/s] |
C |            VV10SV   : 10-m       Wind Velocity                   [m/s] |
C |            TaT_SV   : SBL Top    Temperature                       [K] |
C |            rhT_SV   : SBL Top    Air  Density                  [kg/m3] |
C |            QaT_SV   : SBL Top    Specific  Humidity            [kg/kg] |
C |            qsnoSV   : SBL Mean   Snow      Content             [kg/kg] |
C |            LAI0SV   : Leaf Area  Index                             [-] |
C |            glf0SV   : Green Leaf Fraction                          [-] |
C |            alb0SV   : Soil Basic Albedo                            [-] |
C |            slopSV   : Surface    Slope                             [-] |
C |            dt__SV   : Time  Step                                   [s] |
C |                                                                        |
C |   INPUT /  isnoSV   = total Nb of Ice/Snow Layers                      |
C |   OUTPUT:  ispiSV   = 0,...,nsno: Uppermost Superimposed Ice Layer     |
C |   ^^^^^^   iiceSV   = total Nb of Ice      Layers                      |
C |            istoSV   = 0,...,5 :   Snow     History (see istdSV data)   |
C |                                                                        |
C |   INPUT /  alb_SV   : Surface-Canopy Albedo                        [-] |
C |   OUTPUT:  emi_SV   : Surface-Canopy Emissivity                    [-] |
C |   ^^^^^^   IRs_SV   : Soil           IR Flux  (negative)        [W/m2] |
C |            LMO_SV   : Monin-Obukhov               Scale            [m] |
C |            us__SV   : Friction          Velocity                 [m/s] |
C |            uts_SV   : Temperature       Turbulent Scale          [m/s] |
C |            uqs_SV   : Specific Humidity Velocity                 [m/s] |
C |            uss_SV   : Blowing Snow      Turbulent Scale          [m/s] |
C |            usthSV   : Blowing Snow      Erosion   Threshold      [m/s] |
C |            Z0m_SV   : Momentum     Roughness Length                [m] |
C |            Z0mmSV   : Momentum     Roughness Length (time mean)    [m] |
C |            Z0mnSV   : Momentum     Roughness Length (instantaneous)[m] |
C |            Z0SaSV   : Sastrugi     Roughness Length                [m] |
C |            Z0e_SV   : Erosion Snow Roughness Length                [m] |
C |            Z0emSV   : Erosion Snow Roughness Length (time mean)    [m] |
C |            Z0enSV   : Erosion Snow Roughness Length (instantaneous)[m] |
C |            Z0roSV   : Subgrid Topo Roughness Length                [m] |
C |            Z0h_SV   : Heat         Roughness Length                [m] |
C |            snCaSV   : Canopy   Snow     Thickness            [mm w.e.] |
C |            rrCaSV   : Canopy   Water    Content                [kg/m2] |
C |            psivSV   : Leaf     Water    Potential                  [m] |
C |            TvegSV   : Canopy   Temperature                         [K] |
C |            TsisSV   : Soil/Ice Temperatures (layers -nsol,-nsol+1,..,0)|
C |                     & Snow     Temperatures (layers  1,2,...,nsno) [K] |
C |            ro__SV   : Soil/Snow Volumic Mass                   [kg/m3] |
C |            eta_SV   : Soil/Snow Water   Content                [m3/m3] |
C |            G1snSV   : snow dendricity/sphericity                       |
C |            G2snSV   : snow sphericity/grain size                       |
C |            dzsnSV   : Snow Layer        Thickness                  [m] |
C |            agsnSV   : Snow       Age                             [day] |
C |            BufsSV   : Snow Buffer Layer              [kg/m2] .OR. [mm] |
C |            BrosSV   : Snow Buffer Layer Density      [kg/m3]           |
C |            BG1sSV   : Snow Buffer Layer Dendricity / Sphericity    [-] |
C |            BG2sSV   : Snow Buffer Layer Sphericity / Size [-] [0.1 mm] |
C |            rusnSV   : Surficial   Water              [kg/m2] .OR. [mm] |
C |                                                                        |
C |   OUTPUT:  no__SV   : OUTPUT file Unit Number                      [-] |
C |   ^^^^^^   i___SV   : OUTPUT point   i Coordinate                  [-] |
C |            j___SV   : OUTPUT point   j Coordinate                  [-] |
C |            n___SV   : OUTPUT point   n Coordinate                  [-] |
C |            lwriSV   : OUTPUT point vec Index                       [-] |
C |                                                                        |
C |   OUTPUT:  IRu_SV   : Upward     IR Flux (+, upw., effective)      [K] |
C |   ^^^^^^   hSalSV   : Saltating Layer Height                       [m] |
C |            qSalSV   : Saltating Snow  Concentration            [kg/kg] |
C |            RnofSV   : RunOFF Intensity                       [kg/m2/s] |
C |                                                                        |
C |   Internal Variables:                                                  |
C |   ^^^^^^^^^^^^^^^^^^                                                   |
C |            NLaysv   = New            Snow Layer Switch             [-] |
C |            albisv   : Snow/Ice/Water/Soil Integrated Albedo        [-] |
C |            SoCasv   : Absorbed Solar Radiation by Canopy (Normaliz)[-] |
C |            SoSosv   : Absorbed Solar Radiation by Surfac.(Normaliz)[-] |
C |            tau_sv   : Fraction of Radiation transmitted by Canopy  [-] |
C |            TBr_sv   : Brightness Temperature                       [K] |
C |            IRupsv   : Upward     IR Flux (-, upw.)              [W/m2] |
C |            IRv_sv   : Vegetation IR Flux                        [W/m2] |
C |            rrMxsv   : Canopy Maximum Intercepted Rain          [kg/m2] |
C |            Sigmsv   : Canopy Ventilation Factor                    [-] |
C |            ram_sv   : Aerodynamic Resistance for Momentum        [s/m] |
C |            rah_sv   : Aerodynamic Resistance for Heat            [s/m] |
C |            HSv_sv   : Vegetation Sensible Heat Flux             [W/m2] |
C |            HLv_sv   : Vegetation Latent   Heat Flux             [W/m2] |
C |            Rootsv   : Root Water Pump                        [kg/m2/s] |
C |            Evp_sv   : Evaporation                              [kg/m2] |
C |            EvT_sv   : Evapotranspiration                       [kg/m2] |
C |            HSs_sv   : Surface    Sensible Heat Flux + => absorb.[W/m2] |
C |            HLs_sv   : Surface    Latent   Heat Flux + => absorb.[W/m2] |
C |            Lx_H2O   : Latent Heat of Vaporization/Sublimation   [J/kg] |
C |            Tsrfsv   : Surface    Temperature                       [K] |
C |            LAI_sv   : Leaf Area  Index (snow included)             [-] |
C |            LAIesv   : Leaf Area  Index (effective / transpiration) [-] |
C |            glf_sv   : Green Leaf Fraction of NOT fallen Leaves     [-] |
C |            sEX_sv   : Verticaly Integrated Extinction Coefficient  [-] |
C |            LSdzsv   : Vertical   Discretization Factor             [-] |
C |                     =    1. Soil                                       |
C |                     = 1000. Ocean                                      |
C |            z_snsv   : Snow Pack  Thickness                         [m] |
C |            zzsnsv   : Snow Pack  Thickness                         [m] |
C |            albssv   : Soil       Albedo                            [-] |
C |            Evg_sv   : Soil+Vegetation Emissivity                   [-] |
C |            Eso_sv   : Soil+Snow       Emissivity                   [-] |
C |            psi_sv   : Soil       Water    Potential                [m] |
C |            Khydsv   : Soil   Hydraulic    Conductivity           [m/s] |
C |                                                                        |
C |            ETVg_d   : VegetationEnergy Power         Forcing    [W/m2] |
C |            ETSo_0   : Snow/Soil Energy Power, before Forcing    [W/m2] |
C |            ETSo_1   : Snow/Soil Energy Power, after  Forcing    [W/m2] |
C |            ETSo_d   : Snow/Soil Energy Power         Forcing    [W/m2] |
C |            EqSn_0   : Snow      Energy, before Phase Change     [J/m2] |
C |            EqSn_1   : Snow      Energy, after  Phase Change     [J/m2] |
C |            EqSn_d   : Snow      Energy,       net    Forcing    [J/m2] |
C |            Enrsvd   : SVAT      Energy Power         Forcing    [W/m2] |
C |            Enrbal   : SVAT      Energy Balance                  [W/m2] |
C |            Wats_0   : Soil Water,  before Forcing                 [mm] |
C |            Wats_1   : Soil Water,  after  Forcing                 [mm] |
C |            Wats_d   : Soil Water          Forcing                 [mm] |
C |            SIWm_0   : Snow        initial Mass               [mm w.e.] |
C |            SIWm_1   : Snow        final   Mass               [mm w.e.] |
C |            SIWa_i   : Snow Atmos. initial Forcing            [mm w.e.] |
C |            SIWa_f   : Snow Atmos. final   Forcing(noConsumed)[mm w.e.] |
C |            SIWe_i   : SnowErosion initial Forcing            [mm w.e.] |
C |            SIWe_f   : SnowErosion final   Forcing(noConsumed)[mm w.e.] |
C |            SIsubl   : Snow sublimed/deposed  Mass            [mm w.e.] |
C |            SImelt   : Snow Melted            Mass            [mm w.e.] |
C |            SIrnof   : Surficial Water + Run OFF Change       [mm w.e.] |
C |            SIvAcr   : Sea-Ice    vertical Acretion           [mm w.e.] |
C |            Watsvd   : SVAT Water          Forcing                 [mm] |
C |            Watbal   : SVAT Water  Balance                       [W/m2] |
C |                                                                        |
C |            dsn_Ca,snCa_n :     Snow Contribution to the Canopy[m w.e.] |
C |            drr_Ca,rrCa_n,drip: Rain Contribution to the Canopy [kg/m2] |
C |            vk2      : Square of Von Karman Constant                [-] |
C |            sqrCm0   : Factor of   Neutral Drag Coeffic.Momentum  [s/m] |
C |            sqrCh0   : Factor of   Neutral Drag Coeffic.Heat      [s/m] |
C |            EmiVeg   : Vegetation    Emissivity                     [-] |
C |            EmiSol   : Soil          Emissivity                     [-] |
C |            EmiSno   : Snow          Emissivity                     [-] |
C |            EmiWat   : Water         Emissivity                     [-] |
C |            Z0mSea   :          Sea  Roughness Length               [m] |
C |            Z0mLnd   :          Land Roughness Length               [m] |
C |            sqrrZ0   : u*t/u*                                           |
C |            f_eff    : Marticorena & B. 1995 JGR (20)                   |
C |            A_Fact   : Fundamental * Roughness                          |
C |            Z0mBSn   :         BSnow Roughness Length               [m] |
C |            Z0mBS0   : Mimimum BSnow Roughness Length (blown* )     [m] |
C |            Z0m_Sn   :          Snow Roughness Length (surface)     [m] |
C |            Z0m_S0   : Mimimum  Snow Roughness Length               [m] |
C |            Z0m_S1   : Maximum  Snow Roughness Length               [m] |
C |            Z0_GIM   : Minimum GIMEX Roughness Length               [m] |
C |            Z0_ICE   : Sea Ice ISW   Roughness Length               [m] |
C |                                                                        |
C | # TUNING PARAMETERS :                                                  |
C | # OPTIONS: #BS: Wind Dependant Roughness Length of Snow                |
C | # ^^^^^^^  #ZS: Wind Dependant Roughness Length of Sea                 |
C | #          #FL: Dead Leaves are assumed to been fallen                 |
C | #          #RS: Z0h = Z0m / 100 over the ocean                         |
C | #          #US: u*   computed from aerodynamic resistance              |
C | #          #WV: OUTPUT                                                 |
C | #          #WR: OUTPUT    for Verification                             |
C | #          #SR: Variable      Tracing                                  |
C | #          #CP: Col de Porte  Turbulence        Parameterization       |
C | #          #GL: Greenland                       Parameterization       |
C |                                                                        |
C |                                                                        |
C |   TUNING PARAMETER:                                                    |
C |   ^^^^^^^^^^^^^^^^                                                     |
C |            z0soil   : Soil Surface averaged Bumps Height (see _qSo)[m] |
C |                                                                        |
C |                                                                        |
C |   Preprocessing  Option: SISVAT IO (not always a standard preprocess.) |
C |   ^^^^^^^^^^^^^^^^^^^^^  ^^^^^^^^^                                     |
C |   FILE                 |      CONTENT                                  |
C |   ~~~~~~~~~~~~~~~~~~~~~+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ |
C | # SISVAT_iii_jjj_n     | #E0: OUTPUT on ASCII  File (SISVAT Variables) |
C | #                      |      Energy Budg. Verif.: Soil+(Sea-Ice)+Snow |
C | #                      |(#E0  MUST BE PREPROCESSED BEFORE #e1 & #e2 !) |
C | # SISVAT_iii_jjj_n     | #m0: OUTPUT/Verification: H2O    Conservation |
C |                        |                                               |
C | # stdout               | #s0: OUTPUT of Snow Buffer Layer              |
C |                        |      unit  6, SubRoutine  SISVAT     **ONLY** |
C | # stdout               | #sb: OUTPUT of Snow Erosion                   |
C |                        |      unit  6, SubRoutine  SISVAT_BSn **ONLY** |
C | # stdout               | #sz: OUTPUT of Roughness Length & Drag Coeff. |
C |                        |      unit  6, SubRoutine  SISVAT     **ONLY** |
C | # stdout               | #wz: OUTPUT of Roughness Length (Blown Snow)  |
C |                        |      unit  6, SubRoutines SISVAT, PHY_SISVAT  |
C |                                                                        |
C |   SUGGESTIONS of MODIFICATIONS: see lines beginning with "C +!!!"      |
C |   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                         |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include  "MARCTR.inc"
      include  "MARphy.inc"
      include  "MAR_SV.inc"
      include  "MARlSV.inc"
      include  "MARdSV.inc"
      include  "MAR0SV.inc"
      include  "MARxSV.inc"
c #CP include  "MARdCP.inc"
 
 
C +--Internal Variables
C +  ==================
 
C +--Non Local
C +  ---------
 
      include  "MARySV.inc"
!$OMP threadprivate(/xSISVAT_I/,/xSISVAT_R/,/ySISVAT_I/,/ySISVAT_R/)
 
      real      TBr_sv(klonv)                 ! Brightness Temperature
      real      IRdwsv(klonv)                 ! DOWNward   IR Flux
      real      IRupsv(klonv)                 ! UPward     IR Flux
      real      d_Bufs,Bufs_N                 ! Buffer Snow Layer Increment
      real      Buf_ro,Bros_N                 ! Buffer Snow Layer Density
      real      BufPro                        ! Buffer Snow Layer Density
      real      Buf_G1,BG1__N                 ! Buffer Snow Layer Dendr/Sphe[-]
      real      Buf_G2,BG2__N                 ! Buffer Snow Layer Spher/Size[-]
      real      Bdzssv(klonv)                 ! Buffer Snow Layer Thickness
      real      z_snsv(klonv)                 ! Snow-Ice, current Thickness
 
C +--Energy         Budget
C +  ~~~~~~~~~~~~~~~~~~~~~
! #e1 real      ETVg_d(klonv)                 ! VegetationPower, Forcing
! #e1 real      ETSo_0(klonv)                 ! Soil/Snow Power, before Forcing
! #e1 real      ETSo_1(klonv)                 ! Soil/Snow Power, after  Forcing
! #e1 real      ETSo_d(klonv)                 ! Soil/Snow Power, Forcing
! #e1 real      EqSn_0(klonv)                 ! Snow Energy, befor Phase Change
! #e1 real      EqSn_1(klonv)                 ! Snow Energy, after Phase Change
! #e1 real      EqSn_d(klonv)                 ! Energy in Excess
 
C +--Water   (Mass) Budget
C +  ~~~~~~~~~~~~~~~~~~~~~
! #m0 real      Wats_0(klonv)                 ! Soil Water,  before Forcing
! #m0 real      Wats_1(klonv)                 ! Soil Water,  after  Forcing
! #m0 real      Wats_d(klonv)                 ! Soil Water,         Forcing
 
C +--Snow/Ice(Mass) Budget
C +  ~~~~~~~~~~~~~~~~~~~~~
! #m1 real      SIsubl(klonv)                 ! Snow Sublimed/Deposed Mass
! #m1 real      SImelt(klonv)                 ! Snow Melted           Mass
! #m1 real      SIrnof(klonv)                 ! Local Surficial Water + Run OFF
 
C +--Sea-Ice (Mass) Budget
C +  ~~~~~~~~~~~~~~~~~~~~~
! #m2 real      SIvAcr(klonv)                 ! Sea-Ice      Vertical Acretion
 
 
C +--Local
C +  -----
 
c #MT logical      Garrat                     ! SBL     Scheme    Switch
      character* 1 SepLab                     ! OUTPUT ASCII File Labels
      character* 6 FilLab                     !
      character*16 FilNam                     !
      common/SISVAT_loc_abc/SepLab,FilLab     !
 
      integer               noUNIT            ! OUTPUT File  Unit Number
      integer               nwUNIT            ! OUTPUT File  Unit Number (New)
      common/SISVAT_loc_num/nwUNIT            !
 
      integer   iwr
      integer   ikl   ,isn   ,isl   ,ist      !
      integer   ist__s,ist__w                 ! Soil/Water Body Identifier
      integer   growth                        ! Seasonal               Mask
      integer   LISmsk                        ! Land+Ice / Open    Sea Mask
      integer   LSnMsk                        ! Snow-Ice / No Snow-Ice Mask
      integer   IceMsk,IcIndx(klonv)          !      Ice / No      Ice Mask
      integer   SnoMsk                        ! Snow     / No Snow     Mask
 
      real      drr_Ca,rrCa_n,drip            ! Rain Contribution to the Canopy
      real      dsn_Ca,snCa_n,FallOK(klonv)   ! Snow Contribution to the Canopy
      real      roSMin,roSn_1,roSn_2,roSn_3   ! Fallen Snow Density (PAHAUT)
      real      roSMax                        ! Max. Fallen Snow Density
      real      Dendr1,Dendr2,Dendr3          ! Fallen Snow Dendric.(GIRAUD)
      real      Spher1,Spher2,Spher3,Spher4   ! Fallen Snow Spheric.(GIRAUD)
      real      Polair                        ! Polar  Snow Switch
      real      PorSno,Por_BS,Salt_f,PorRef   !
c #sw real      PorVol,rWater                 !
c #sw real      rusNEW,rdzNEW,etaNEW          !
      real      ro_new                        !
      real      TaPole                        ! Maximum     Polar Temperature
      real      T__Min                        ! Minimum realistic Temperature
 
      real      EmiVeg                        ! Emissivity of Vegetation
      real      EmiSol                        ! Emissivity of       Soil
      real      EmiSno                        ! Emissivity of            Snow
      real      EmiWat                        ! Emissivity of a Water Area
      real      vk2                           ! Square of Von Karman Constant
      real      u2star                        !(u*)**2
c #WR real      u_star                        ! Friction Velocity (aer.resist.)
      real      fallen                        ! Fallen   Leaves         Switch
      real      Z0mSea,Z0hSea                 !          Sea  Roughness Length
      real      Z0mLnd                        !          Land Roughness Length
      real      sqrrZ0                        ! u*t/u*
      real      f_eff                         ! Marticorena & B. 1995 JGR (20)
      real      A_Fact                        ! Fundamental * Roughness
      real      Z0m_nu                        ! Smooth R Snow Roughness Length
      real      Z0mBSn                        !         BSnow Roughness Length
      real      Z0mBS0                        ! Mimimum BSnow Roughness Length
      real      Z0m_S0                        ! Mimimum  Snow Roughness Length
      real      Z0m_S1                        ! Maximum  Snow Roughness Length
c #SZ real      Z0Sa_N                        ! Regime   Snow Roughness Length
c #SZ real      Z0SaSi                        ! 1.IF Rgm Snow Roughness Length
      real      Z0_GIM                        ! Mimimum GIMEX Roughness Length
      real      Z0_ICE                        ! Sea-Ice ISW   Roughness Length
      real      Z0m_Sn,Z0m_90                 ! Snow  Surface Roughness Length
      real      SnoWat                        ! Snow Layer    Switch
c #RN real      rstar,alors                   !
c #RN real      rstar0,rstar1,rstar2          !
      real      SameOK                        ! 1. => Same Type of Grains
      real      G1same                        ! Averaged G1,  same Grains
      real      G2same                        ! Averaged G2,  same Grains
      real      typ__1                        ! 1. => Lay1 Type: Dendritic
      real      zroNEW                        ! dz X ro, if fresh Snow
      real      G1_NEW                        ! G1,      if fresh Snow
      real      G2_NEW                        ! G2,      if fresh Snow
      real      zroOLD                        ! dz X ro, if old   Snow
      real      G1_OLD                        ! G1,      if old   Snow
      real      G2_OLD                        ! G2,      if old   Snow
      real      SizNEW                        ! Size,    if fresh Snow
      real      SphNEW                        ! Spheric.,if fresh Snow
      real      SizOLD                        ! Size,    if old   Snow
      real      SphOLD                        ! Spheric.,if old   Snow
      real      Siz_av                        ! Averaged    Grain Size
      real      Sph_av                        ! Averaged    Grain Spher.
      real      Den_av                        ! Averaged    Grain Dendr.
      real      DendOK                        ! 1. => Average is  Dendr.
      real      G1diff                        ! Averaged G1, diff. Grains
      real      G2diff                        ! Averaged G2, diff. Grains
      real      G1                            ! Averaged G1
      real      G2                            ! Averaged G2
      real      param                         ! Polynomial   fit z0=f(T)
      real      Z0_obs                        ! Fit Z0_obs=f(T) (m)
      real      tamin                         ! min T of linear fit (K)
      real      tamax                         ! max T of linear fit (K)
      real      coefa,coefb,coefc,coefd       ! Coefs for z0=f(T)
      real      ta1,ta2,ta3                   ! Air temperature thresholds
      real      z01,z02,z03                   ! z0 thresholds
      real      tt_c,vv_c                     ! Critical param.
      real      tt_tmp,vv_tmp,vv_virt         ! Temporary variables
      logical   density_kotlyakov             ! .true. if Kotlyakov 1961
                                              ! else density from obs.
 
C +--Energy       Budget
C +  ~~~~~~~~~~~~~~~~~~~
! #e1 integer             noEBal              ! Energy Imbalances Counter
! #e1 common/SISVAT__EBal/noEBal              !
! #e1 real        Enrsvd(klonv)               ! Soil+Vegetat  Power  Forcing
! #e1 real        EnsBal                      ! Soil+Snow   , Power  Balance
! #e1 real        EnvBal                      !      Vegetat, Power  Balance
 
C +--Water (Mass) Budget
C +  ~~~~~~~~~~~~~~~~~~~
! #m0 integer             noWBal              ! Water  Imbalances Counter
! #m0 common/SISVAT__WBal/noWBal              !
! #m0 real        Watsv0(klonv)               ! Soil+Vegetat, before Forcing
! #m0 real        Watsvd(klonv)               ! Soil+Vegetat  Water  Forcing
! #m0 real        Watbal                      ! Soil+Vegetat, Water  Balance
 
C +--Snow  (Mass) Budget
C +  ~~~~~~~~~~~~~~~~~~~
! #m1 integer             noSBal              ! Water  Imbalances Counter
! #m1 common/SISVAT__SBal/noSBal              !
! #m1 real        SIWm_0(klonv),SIWm_1(klonv) ! Snow Initial/Final        Mass
! #m1 real        SIWa_i(klonv),SIWa_f(klonv) ! Snow Initial/Final ATM Forcing
! #m1 real        SIWe_i(klonv),SIWe_f(klonv) ! Snow Initial/Final BLS Forcing
! #m1 real        SnoBal                      ! Snow Pack     Mass   Balance
 
 
C +--Internal DATA
C +  =============
 
c #MT data      Garrat /.true. /              ! SBL     Scheme    Switch
      data      T__Min / 200.00/              ! Minimum realistic Temperature
c #AC data      TaPole / 283.15/              ! Maximum Polar     Temp.
      data      TaPole / 268.15/              ! Maximum Polar     Temperature
c #EU data      roSMin /  25.  /              ! Minimum Snow   Density
c #BS data      roSMin / 200.  /              ! Minimum Snow   Density
c #AC data      roSMin / 200.  /              ! Minimum Snow   Density
      data      roSMin / 200.  /              ! Minimum Snow   Density
cXF
c #EU data      roSMax / 250.  /              ! Max Fresh Snow Density
      data      roSMax / 400.  /              ! Max Fresh Snow Density
      data      tt_c   / -2.0  /              ! Critical Temp. (degC)
C +...DATA      tt_c=-2.  => rho->quickly to rho(vv_c) when T->-inf
      data      vv_c   / 14.3  /              ! Critical Wind speed (m/s)
C +...DATA      vv_c=14.3 => rho->300 when T->-inf
      data      roSn_1 / 109.  /              ! Fall.Sno.Density, Indep. Param.
      data      roSn_2 /   6.  /              ! Fall.Sno.Density, Temper.Param.
      data      roSn_3 /  26.  /              ! Fall.Sno.Density, Wind   Param.
      data      Dendr1 /  17.12/              ! Fall.Sno.Dendric.,Wind 1/Param.
      data      Dendr2 / 128.  /              ! Fall.Sno.Dendric.,Wind 2/Param.
      data      Dendr3 / -20.  /              ! Fall.Sno.Dendric.,Indep. Param.
      data      Spher1 /   7.87/              ! Fall.Sno.Spheric.,Wind 1/Param.
      data      Spher2 /  38.  /              ! Fall.Sno.Spheric.,Wind 2/Param.
      data      Spher3 /  50.  /              ! Fall.Sno.Spheric.,Wind 3/Param.
      data      Spher4 /  90.  /              ! Fall.Sno.Spheric.,Indep. Param.
      data      EmiSol /   0.94/              ! Emissivity of Soil
      data      EmiVeg /   0.98/              ! Emissivity of Vegetation
      data      EmiWat /   0.99/              ! Emissivity of a Water Area
      data      EmiSno /   0.99/              ! Emissivity of Snow
C +...DATA      Emissivities                  ! Pielke, 1984, pp. 383,409
 
      data      fallen /   0.    /            ! Fallen  Leaves         Switch
      data      Z0mBS0 /   0.5e-6/            ! MINimum Snow Roughness Length
                                              ! for Momentum if Blowing Snow
                                              ! Gall?e et al. 2001 BLM 99 (19)
      data      Z0m_S0/    0.00005/           ! MINimum Snow Roughness Length
c #MG data      Z0m_S0/    0.00200/           ! MINimum Snow Roughness Length
                                              ! MegaDunes    included
      data      Z0m_S1/    0.030  /           ! MAXimum Snow Roughness Length
                                              !        (Sastrugis)
      data      Z0_GIM/    0.0013/            ! Ice Min Z0 = 0.0013 m (Broeke)
C +                                           ! Old Ice Z0 = 0.0500 m (Bruce)
c +                                           !              0.0500 m (Smeets)
C +                                           !              0.1200 m (Broeke)
      data      Z0_ICE/    0.0010/            ! Sea-Ice Z0 = 0.0010 m (Andreas)
c +                                           !    (Ice Station Weddel -- ISW)
      vk2    =  vonkar  *  vonkar             ! Square of Von Karman Constant
c #FL fallen =             1.                 ! Fallen  Leaves         Switch
 
 
C +..BEGIN.main.
C +--SISVAT Forcing VERIFICATION
C +  ===========================
 
      IF (.not.iniOUT)                                              THEN
               iniOUT = .true.
        IF (IRs_SV(1).gt.-epsi)
     .  write(6,600)
 600    format(/,'### SISVAT ERROR, Soil IR Upward  not defined ###',
     .         /,'###               Initialize and Store IRs_SV ###')
 
 
! OUTPUT
! ======
 
                    FilLab              ='SISVAT'
                    SepLab              ='_'
                    nwUNIT              = 51
      END IF
 
c #E0 DO ikl=1,klonv
c #E0   IF   (lwriSV(ikl).ne.0.AND.no__SV(lwriSV(ikl)).eq.0)        THEN
c #E0                nwUNIT              = nwUNIT+1
c #E0                no__SV(lwriSV(ikl)) = nwUNIT
c #E0      write(FilNam,'(a6,a1,2(i3.3,a1),i1)')
c #E0.           FilLab,SepLab,i___SV(lwriSV(ikl)),
c #E0.                  SepLab,j___SV(lwriSV(ikl)),
c #E0.                  SepLab,n___SV(lwriSV(ikl))
c #E0      open(unit=nwUNIT,status='unknown',file=FilNam)
c #E0      rewind    nwUNIT
c #E0   END IF
c #E0 END DO
 
c #E0 DO ikl=1,klonv
c #E0   IF (lwriSV(ikl).ne.0)                                       THEN
c #E0           noUNIT=no__SV(lwriSV(ikl))
c #E0     write(noUNIT,5000) daHost,i___SV(lwriSV(ikl)),
c #E0.                              j___SV(lwriSV(ikl)),
c #E0.                              n___SV(lwriSV(ikl)),
c #E0.                                     Z0m_SV(ikl) ,
c #E0.                                     albisv(ikl)
 5000     format(
     .       /,              a18,'|           Grid Point ',2i4,
     .                                           ' (',i2,')',
     .         '    | Z0m =',f12.6,' | Albedo = ',f6.3,' |',
     .       /,' -------+',7('---------+'),2('--------+'))
c #E0   END IF
c #E0 END DO
 
 
C +--"Soil" Humidity of Water Bodies
C +  ===============================
 
      DO ikl=1,klonv
          ist    =      isotSV(ikl)                       ! Soil Type
          ist__s =  min(ist, 1)                           ! 1 => Soil
          ist__w =  1 - ist__s                            ! 1 => Water Body
        DO isl=-nsol,0
          eta_SV(ikl,isl) = eta_SV(ikl,isl) * ist__s      ! Soil
     .                    + etadSV(ist)     * ist__w      ! Water Body
        END DO
 
 
C +--Vertical Discretization Factor
C +  ==============================
 
          LSdzsv(ikl)     =                   ist__s      ! Soil
     .                    + OcndSV          * ist__w      ! Water Body
      END DO
 
 
C +--Vegetation Temperature Limits
C +  =============================
 
          DO ikl=1,klonv
            TvegSV(ikl) = max(TvegSV(ikl),T__Min)         ! T__Min = 200.K
 
 
C +--LAI Assignation and Fallen Leaves Correction (#FL)
C +  ==================================================
 
            LAI0SV(ikl) =     LAI0SV(ikl)*min(1,ivgtSV(ikl)) ! NO LAI if
C +                                                          ! no vegetation
            glf_sv(ikl) =     glf0SV(ikl)
c #FL       glf_sv(ikl) =     1.
            LAI_sv(ikl) =     LAI0SV(ikl)
c #FL.               *        glf0SV(ikl)
          END DO
 
 
C +--LAI in Presence of Snow
C +  =======================
 
C +       ASSUMPTION: LAI decreases   when Snow Thickness increases,
C +       ^^^^^^^^^^      becoming  0 when Snow Thickn. = Displac.Height
          DO ikl=1,klonv
            LAI_sv(ikl) =     LAI_sv(ikl)
     .               * (1.0 - zzsnsv(       ikl, isnoSV(ikl))
     .                      /(DH_dSV(ivgtSV(ikl))+epsi)      )
            LAI_sv(ikl) = max(LAI_sv(ikl),zero)
            LAI_sv(ikl) = min(LAI_sv(ikl),argmax)
          END DO
 
 
C +--Interception of Rain by the Canopy
C +  ==================================
 
! Vegetation Forcing
! ------------------
 
! #m0     DO ikl=1,klonv
! #m0       Watsv0(ikl) =      rrCaSV(ikl)           ! Canopy Water Cont.
! #m0       Watsvd(ikl) =      drr_SV(ikl)           ! Precipitation
! #m0     END DO
 
 
C +--New Canopy Water Content
C +  ------------------------
 
          DO ikl=1,klonv
            rrMxsv(ikl) = 0.2*max( epsi,LAI_sv(ikl)) ! Precip. Max. Intercept.
            Sigmsv(ikl) = 1.0-exp(-demi*LAI_sv(ikl)) ! Canopy Ventilation Coe.
C +                                                  ! (DR97, eqn 3.6)
            drr_Ca      = drr_SV(ikl)  *Sigmsv(ikl)  ! Intercepted Rain
     .                                 *dt__SV       !
            rrCa_n      = rrCaSV(ikl)  +drr_Ca       ! New Canopy Water Contnt
                                                     ! (DR97, eqn 3.28)
            drip        = rrCa_n       -rrMxsv(ikl)  ! Water  Drip
            drip        =      max(zero,drip)        !
            rrCa_n      = rrCa_n       -drip         !
            drr_SV(ikl) = drr_SV(ikl) +(rrCaSV(ikl)  ! Update Rain  Contribut.
     .                                 -rrCa_n     ) !
     .                                 /dt__SV       !
            rrCaSV(ikl) = rrCa_n                     ! Upd.Canopy Water Contnt
 
 
C +--Interception of Snow by the Canopy
C +  ==================================
 
            dsn_Ca      = dsn_SV(ikl)  *Sigmsv(ikl)  ! Intercepted Snow
     .                                 *dt__SV       !
            snCa_n      = snCaSV(ikl)  +dsn_Ca       ! New Canopy Snow Thickn.
            drip        = snCa_n       -rrMxsv(ikl)  !
            drip        =      max(zero,drip)        !
            snCa_n      = snCa_n       -drip         !
            dsn_SV(ikl) = dsn_SV(ikl) +(snCaSV(ikl)  ! Update Snow  Contribut.
     .                                 -snCa_n     ) !
     .                                 /dt__SV       !
            snCaSV(ikl) = snCa_n                     ! Upd.Canopy Snow Thickn.
          END DO
 
 
C +--Snow Fall from the Canopy
C +  =========================
 
C +       ASSUMPTION: snow fall from the canopy,
C +       ^^^^^^^^^^  when the temperature of the vegetation is positive
C +             (.OR. when snow over the canopy is saturated  with water)
 
          DO ikl=1,klonv
            FallOK(ikl) =  max(zero,sign(unun,TvegSV(ikl)-TfSnow+epsi))
     .                  *  max(zero,sign(unun,snCaSV(ikl)       -epsi))
            dsn_SV(ikl) =      dsn_SV(ikl)   +snCaSV(ikl)*FallOK(ikl)
     .                                       /dt__SV
            snCaSV(ikl) =      snCaSV(ikl) * (1.         -FallOK(ikl))
 
 
C +--Blowing Particles Threshold Friction velocity
C +  =============================================
 
c #AE       usthSV(ikl) =                     1.0e+2
          END DO
 
 
C +--Contribution of Snow to the Surface Snow Pack
C +  =============================================
 
      IF (SnoMod)                                                 THEN
 
 
! Snow Initial Mass (below the Canopy) and Forcing
! ------------------------------------------------
 
! #m1   DO ikl=1,klonv
! #m1     SIWa_i(ikl) =(drr_SV(ikl) + dsn_SV(ikl))   *dt__SV         ![mm w.e.]
! #m1     SIWe_i(ikl) = dbs_SV(ikl)                                  !
! #m1     SIWm_0(ikl) = BufsSV(ikl) + HFraSV(ikl)    *ro_Ice         !
! #m1   DO isn=1,nsno                                                !
! #m1     SIWm_0(ikl) = SIWm_0(ikl) + dzsnSV(ikl,isn)*ro__SV(ikl,isn)!
! #m1   END DO                                                       !
! #m1   END DO                                                       !
 
 
C +--Blowing Snow
C +  ------------
 
        IF (BloMod) then
         if (klonv.eq.1) then
          if(isnoSV(1).ge.1) then
C +                       **********
                     call SISVAT_BSn
          endif
         else
                     call SISVAT_BSn
C +                       **********
         endif
        ENDIF
 
C +                       **********
! #ve                call SISVAT_wEq('_BSn  ',1)
C +                       **********
 
 
C +--Sea Ice
C +  -------
 
C +          **********
        call SISVAT_SIc
! #m2.                 (SIvAcr)
C +          **********
 
C +          **********
! #ve   call SISVAT_wEq('_SIc  ',0)
C +          **********
 
 
C +--Buffer Layer
C +  ------------
 
          DO ikl=1,klonv
            BufsSV(ikl) =      BufsSV(ikl)              !     [mm w.e.]
            d_Bufs      =  max(dsn_SV(ikl) *dt__SV,0.)  ! i.e., [kg/m2]
            dsn_SV(ikl) =      0.                       !
            Bufs_N      =      BufsSV(ikl) +d_Bufs      !
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! OUTPUT           for Buffer G1, G2 variables
! #s0       IF (ii__SV(ikl).EQ.iwr_SV .AND. jj__SV(ikl).EQ. jwr_SV .AND.
! #s0.          nn__SV(ikl).EQ.nwr_SV)
! #s0.      write(6,6601)      BufsSV(ikl) ,d_Bufs,Bufs_N
 6601       format(/,'Buffer *: ',3e15.6)
 
C +--Snow Density
C +  ^^^^^^^^^^^^
            Polair      =      zero
c #NP       Polair      =  max(zero,                    !
c #NP.                         sign(unun,TaPole         !
c #NP.                                  -TaT_SV(ikl)))  !
            Polair      =  max(zero,                    !
     .                         sign(unun,TaPole         !
     .                                  -TaT_SV(ikl)))  !
            Buf_ro      =  max( rosMin,                 ! Fallen Snow Density
     .      roSn_1+roSn_2*     (TaT_SV(ikl)-TfSnow)     ! [kg/m3]
     .            +roSn_3*sqrt( VV10SV(ikl)))           ! Pahaut    (CEN)
c #NP       BufPro      =  max( rosMin,                 ! Fallen Snow Density
c #NP.         104. *sqrt( max( VV10SV(ikl)-6.0,0.0)))  ! Kotlyakov (1961)
 
            density_kotlyakov = .true.
c #AC       density_kotlyakov = .false.
C + ...     Fallen Snow Density, Adapted for Antarctica
            if (density_kotlyakov) then
                tt_tmp = TaT_SV(ikl)-TfSnow
                vv_tmp = VV10SV(ikl)
C + ...         [ A compromise between
C + ...           Kotlyakov (1961) and Lenaerts (2012, JGR, Part1) ]
                if (tt_tmp.ge.-10) then
                  BufPro   =  max( rosMin,
     .            104. *sqrt( max( vv_tmp-6.0,0.0))) ! Kotlyakov (1961)
                else
                  vv_virt = (tt_c*vv_tmp+vv_c*(tt_tmp+10))
     .                     /(tt_c+tt_tmp+10)
                  BufPro  = 104. *sqrt( max( vv_virt-6.0,0.0))
                endif
            else
C + ...         [ density derived from observations of the first 50cm of
C + ...           snow - cf. Rajashree Datta - and multiplied by 0.8 ]
C + ...           C. Agosta, 2016-09
                BufPro = 149.2 + 6.84*VV10SV(ikl) + 0.48*Tsrfsv(ikl)
            endif
 
            Bros_N      = (1. - Polair) *   Buf_ro      ! Temperate Snow
     .                        + Polair  *   BufPro      ! Polar     Snow
 
cXF !!!!
            Bros_N = max( 10.,max(rosMin,  Bros_N))
            Bros_N = min(500.,min(rosMax-1,Bros_N)) ! for dz_min in SISVAT_zSn
cXF !!!!
 
!    Instantaneous Density of deposited blown Snow (de Montmollin, 1978)
!    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #BS       PorSno =      1.0d00     -  blsno
c #BS.                               /  ro_Ice
c #BS       Salt_f =      usthSV(ikl)/  max(epsi,   us__SV(ikl))
c #BS       Salt_f =  min(Salt_f     ,  unun)
c #BS       PorRef =      PorSno     /  max(epsi,1.-PorSno)
c #BS.               +log(Salt_f)
c #BS       Por_BS =      PorRef     /          (1.+PorRef)
c #BS       ro_new =      ro_Ice     *          (1.-Por_BS)
c #BS       ro_new =  max(ro_new     ,  blsno)
c #BS       Bros_N      = Bros_N     * (1.0-dsnbSV(ikl))
c #BS.                  + ro_new     *      dsnbSV(ikl)
 
!    Instantaneous Density IF deposited blown Snow (Melted* from Canopy)
!    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Bros_N      = Bros_N     * (1.0-FallOK(ikl))!
     .                  + 300.       *      FallOK(ikl) !
 
!    Time averaged Density of deposited blown Snow
!    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            BrosSV(ikl) =(Bros_N     *      d_Bufs      !
     .                   +BrosSV(ikl)*      BufsSV(ikl))!
     .                   /         max(epsi,Bufs_N)     !
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! OUTPUT           for Buffer G1, G2 variables
! #s0       IF (ii__SV(ikl).EQ.iwr_SV .AND. jj__SV(ikl).EQ. jwr_SV .AND.
! #s0.          nn__SV(ikl).EQ.nwr_SV)
! #s0.      write(6,6602) Buf_ro,Bros_N,BrosSV(ikl),dsnbSV(ikl)
 6602       format('rho    *: ',3e15.6,'    dsnbSV: ',e15.6)
 
C +-- S.Falling Snow Properties (computed as in SISVAT_zAg)
C +     ^^^^^^^^^^^^^^^^^^^^^^^
            Buf_G1      =  max(-G1_dSV,                 ! Temperate Snow
     .               min(Dendr1*VV__SV(ikl)-Dendr2,     !     Dendricity
     .                   Dendr3                   ))    !
            Buf_G2      =  min( Spher4,                 ! Temperate Snow
     .               max(Spher1*VV__SV(ikl)+Spher2,     !     Sphericity
     .                   Spher3                   ))    !
            Buf_G1      = (1. - Polair) *   Buf_G1      ! Temperate Snow
     .                        + Polair  *   G1_dSV      ! Polar     Snow
            Buf_G2      = (1. - Polair) *   Buf_G2      ! Temperate Snow
     .                        + Polair  *   ADSdSV      ! Polar     Snow
                G1      =                   Buf_G1      ! NO  Blown Snow
                G2      =                   Buf_G2      ! NO  Blown Snow
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! OUTPUT           for Buffer G1, G2 variables
! #s0       IF (ii__SV(ikl).EQ.iwr_SV .AND. jj__SV(ikl).EQ. jwr_SV .AND.
! #s0.          nn__SV(ikl).EQ.nwr_SV)
! #s0.      write(6,6603)       BG1sSV(ikl),BG2sSV(ikl)
 6603       format('G1,G2  *: ',3e15.6)
 
!     S.1. Meme  Type  de Neige  / same Grain Type
!          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
c #BS       SameOK  =  max(zero,
c #BS.                     sign(unun,    Buf_G1             *G1_dSV
c #BS.                                 - eps_21                    ))
c #BS       G1same  = ((1.0-dsnbSV(ikl))*Buf_G1+dsnbSV(ikl) *G1_dSV)
c #BS       G2same  = ((1.0-dsnbSV(ikl))*Buf_G2+dsnbSV(ikl) *ADSdSV)
!           Blowing Snow Properties:                         G1_dSV, ADSdSV
 
!     S.2. Types differents / differents Types
!          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
c #BS       typ__1  =  max(zero,sign(unun,epsi-Buf_G1))   ! =1.=> Dendritic
c #BS       zroNEW  =     typ__1  *(1.0-dsnbSV(ikl))      ! fract.Dendr.Lay.
c #BS.              + (1.-typ__1) *     dsnbSV(ikl)       !
c #BS       G1_NEW  =     typ__1  *Buf_G1                 ! G1 of Dendr.Lay.
c #BS.              + (1.-typ__1) *G1_dSV                 !
c #BS       G2_NEW  =     typ__1  *Buf_G2                 ! G2 of Dendr.Lay.
c #BS.              + (1.-typ__1) *ADSdSV                 !
c #BS       zroOLD  = (1.-typ__1) *(1.0-dsnbSV(ikl))      ! fract.Spher.Lay.
c #BS.              +     typ__1  *     dsnbSV(ikl)       !
c #BS       G1_OLD  = (1.-typ__1) *Buf_G1                 ! G1 of Spher.Lay.
c #BS.              +     typ__1  *G1_dSV                 !
c #BS       G2_OLD  = (1.-typ__1) *Buf_G2                 ! G2 of Spher.Lay.
c #BS.              +     typ__1  *ADSdSV                 !
c #BS       SizNEW  =    -G1_NEW  *DDcdSV/G1_dSV          ! Size  Dendr.Lay.
c #BS.               +(1.+G1_NEW         /G1_dSV)         !
c #BS.                  *(G2_NEW  *DScdSV/G1_dSV          !
c #BS.               +(1.-G2_NEW         /G1_dSV)*DFcdSV) !
c #BS       SphNEW  =     G2_NEW         /G1_dSV          ! Spher.Dendr.Lay.
c #BS       SizOLD  =     G2_OLD                          ! Size  Spher.Lay.
c #BS       SphOLD  =     G1_OLD         /G1_dSV          ! Spher.Spher.Lay.
c #BS       Siz_av =     (zroNEW*SizNEW+zroOLD*SizOLD)    ! Averaged Size
c #BS       Sph_av = min( zroNEW*SphNEW+zroOLD*SphOLD     !
c #BS.                   ,  unun)                         ! Averaged Sphericity
c #BS       Den_av = min((Siz_av -(    Sph_av *DScdSV     !
c #BS.                            +(1.-Sph_av)*DFcdSV))   !
c #BS.                 / (DDcdSV -(    Sph_av *DScdSV     !
c #BS.                            +(1.-Sph_av)*DFcdSV))   !
c #BS.                   ,  unun)                         !
c #BS       DendOK  = max(zero,                           !
c #BS.                    sign(unun,     Sph_av *DScdSV   ! Small   Grains
c #BS.                              +(1.-Sph_av)*DFcdSV   ! Faceted Grains
c #BS.                              -    Siz_av        )) !
C +...      REMARQUE: le  type moyen (dendritique ou non) depend
C +         ^^^^^^^^  de la  comparaison avec le diametre optique
C +                   d'une neige recente de   dendricite nulle
C +...      REMARK:   the mean type  (dendritic   or not) depends
C +         ^^^^^^    on the comparaison with the optical diameter
C +                   of a recent snow    having zero dendricity
 
c #BS       G1diff  =(   -DendOK *Den_av
c #BS.               +(1.-DendOK)*Sph_av) *G1_dSV
c #BS       G2diff  =     DendOK *Sph_av  *G1_dSV
c #BS.               +(1.-DendOK)*Siz_av
c #BS       G1      =     SameOK *G1same
c #BS.               +(1.-SameOK)*G1diff
c #BS       G2      =     SameOK *G2same
c #BS.               +(1.-SameOK)*G2diff
 
            BG1__N      =((1. - FallOK(ikl))*   G1      !
     .                        + FallOK(ikl) *   99.)    ! Melted *  from Canopy
     .                  *       d_Bufs/max(epsi,d_Bufs) !
            BG2__N      =((1. - FallOK(ikl))*   G2      !
     .                        + FallOK(ikl) *   30.)    ! Melted *  from Canopy
     .                  *       d_Bufs/max(epsi,d_Bufs) !
 
C +-- S.Buffer  Snow Properties (computed as in SISVAT_zAg)
C +     ^^^^^^^^^^^^^^^^^^^^^^^
            Buf_G1      =       BG1__N                  ! Falling   Snow
            Buf_G2      =       BG2__N                  ! Falling   Snow
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! OUTPUT           for Buffer G1, G2 variables
! #s0       IF (ii__SV(ikl).EQ.iwr_SV .AND. jj__SV(ikl).EQ. jwr_SV .AND.
! #s0.          nn__SV(ikl).EQ.nwr_SV)
! #s0.      write(6,6604)      Buf_G1      ,Buf_G2         ,FallOK(ikl)
! #s0.                                                     ,TvegSV(ikl)
 6604       format('G1,G2 F*: ',3e15.6,'    T__Veg: ',e15.6)
 
!     S.1. Meme  Type  de Neige  / same Grain Type
!          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            SameOK  =  max(zero,
     .                     sign(unun,    Buf_G1 *BG1sSV(ikl)
     .                                 - eps_21                    ))
            G1same  = (d_Bufs*Buf_G1+BufsSV(ikl)*BG1sSV(ikl))
     .                     /max(epsi,Bufs_N)
            G2same  = (d_Bufs*Buf_G2+BufsSV(ikl)*BG2sSV(ikl))
     .                     /max(epsi,Bufs_N)
 
!     S.2. Types differents / differents Types
!          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            typ__1  =  max(zero,sign(unun,epsi-Buf_G1))   ! =1.=> Dendritic
            zroNEW  =(    typ__1  *d_Bufs                 ! fract.Dendr.Lay.
     .              + (1.-typ__1) *BufsSV(ikl))           !
     .                   /max(epsi,Bufs_N)                !
            G1_NEW  =     typ__1  *Buf_G1                 ! G1 of Dendr.Lay.
     .              + (1.-typ__1) *BG1sSV(ikl)            !
            G2_NEW  =     typ__1  *Buf_G2                 ! G2 of Dendr.Lay.
     .              + (1.-typ__1) *BG2sSV(ikl)            !
            zroOLD  =((1.-typ__1) *d_Bufs                 ! fract.Spher.Lay.
     .              +     typ__1  *BufsSV(ikl))           !
     .                   /max(epsi,Bufs_N)                !
            G1_OLD  = (1.-typ__1) *Buf_G1                 ! G1 of Spher.Lay.
     .              +     typ__1  *BG1sSV(ikl)            !
            G2_OLD  = (1.-typ__1) *Buf_G2                 ! G2 of Spher.Lay.
     .              +     typ__1  *BG2sSV(ikl)            !
            SizNEW  =    -G1_NEW  *DDcdSV/G1_dSV          ! Size  Dendr.Lay.
     .               +(1.+G1_NEW         /G1_dSV)         !
     .                  *(G2_NEW  *DScdSV/G1_dSV          !
     .               +(1.-G2_NEW         /G1_dSV)*DFcdSV) !
            SphNEW  =     G2_NEW         /G1_dSV          ! Spher.Dendr.Lay.
            SizOLD  =     G2_OLD                          ! Size  Spher.Lay.
            SphOLD  =     G1_OLD         /G1_dSV          ! Spher.Spher.Lay.
            Siz_av  =   ( zroNEW  *SizNEW+zroOLD*SizOLD)  ! Averaged Size
            Sph_av = min( zroNEW  *SphNEW+zroOLD*SphOLD   !
     .                  ,   unun                       )  ! Averaged Sphericity
            Den_av = min((Siz_av  - (    Sph_av *DScdSV   !
     .                              +(1.-Sph_av)*DFcdSV)) !
     .                 / (DDcdSV  - (    Sph_av *DScdSV   !
     .                              +(1.-Sph_av)*DFcdSV)) !
     .                  ,   unun                         )!
            DendOK  = max(zero,                           !
     .                    sign(unun,     Sph_av *DScdSV   ! Small   Grains
     .                              +(1.-Sph_av)*DFcdSV   ! Faceted Grains
     .                              -    Siz_av        )) !
C +...      REMARQUE: le  type moyen (dendritique ou non) depend
C +         ^^^^^^^^  de la  comparaison avec le diametre optique
C +                   d'une neige recente de   dendricite nulle
C +...      REMARK:   the mean type  (dendritic   or not) depends
C +         ^^^^^^    on the comparaison with the optical diameter
C +                   of a recent snow    having zero dendricity
 
            G1diff  =(   -DendOK *Den_av
     .               +(1.-DendOK)*Sph_av) *G1_dSV
            G2diff  =     DendOK *Sph_av  *G1_dSV
     .               +(1.-DendOK)*Siz_av
            G1      =     SameOK *G1same
     .               +(1.-SameOK)*G1diff
            G2      =     SameOK *G2same
     .               +(1.-SameOK)*G2diff
 
            BG1sSV(ikl) =                       G1      !
     .                  *       Bufs_N/max(epsi,Bufs_N) !
            BG2sSV(ikl) =                       G2      !
     .                  *       Bufs_N/max(epsi,Bufs_N) !
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! OUTPUT           for Buffer G1, G2 variables
! #s0       IF (ii__SV(ikl).EQ.iwr_SV .AND. jj__SV(ikl).EQ. jwr_SV .AND.
! #s0.          nn__SV(ikl).EQ.nwr_SV)
! #s0.      write(6,6605) Buf_G1     ,typ__1
! #s0.                   ,DendOK     ,Den_av     ,Sph_av     ,Siz_av
! #s0.                   ,G1same     ,G1diff     ,G1
 6605       format('B1,Typ  : ',2e15.6,11x,'OK,Den,Sph,Siz: ',4e15.6
     .          ,/,'          ',30x   ,11x,'sam,dif,G1    : ',3e15.6)
 
C +--Update of Buffer Layer Content & Decision about creating a new snow layer
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            BufsSV(ikl) =       Bufs_N                  !     [mm w.e.]
            NLaysv(ikl) = min(unun,                     !
     .                    max(zero,                     ! Allows to create
     .                        sign(unun,BufsSV(ikl)     ! a new snow Layer
     .                                 -SMndSV     ))   ! if Buffer > SMndSV
     .                   *max(zero,                     ! Except if * Erosion
     .                        sign(unun,0.50            ! dominates
     .                                 -dsnbSV(ikl)))   !
     .                   +max(zero,                     ! Allows to create
     .                        sign(unun,BufsSV(ikl)     ! a new snow Layer
     .                                 -SMndSV*3.00)))  ! is Buffer > SMndSV*3
 
            Bdzssv(ikl) = 1.e-3*BufsSV(ikl)*ro_Wat      ! [mm w.e.] -> [m w.e.]
     .                            /max(epsi,BrosSV(ikl))!& [m w.e.] -> [m]
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! OUTPUT           for Buffer G1, G2 variables
! #s0       IF (ii__SV(ikl).EQ.iwr_SV .AND. jj__SV(ikl).EQ. jwr_SV .AND.
! #s0.          nn__SV(ikl).EQ.nwr_SV)
! #s0.      write(6,6606) BG1sSV(ikl),BG2sSV(ikl)
! #s0.                   ,NLaysv(ikl),BdzsSV(ikl)
 6606       format('G1,G2 N*: ',2e15.6,i15,e27.6)
 
          END DO
 
 
C +--Snow Pack Discretization
C +  ========================
 
cXF
      if (klonv.eq.1) then
       if(isnoSV(1).ge.1.or.NLaysv(1).ge.1) then
C +          **********
        call SISVAT_zSn
C +          **********
       endif
      else
C +          **********
        call SISVAT_zSn
C +          **********
      endif
 
C +          **********
! #ve   call SISVAT_wEq('_zSn  ',0)
C +          **********
 
c #EF   IF          (isnoSV(1) .GT. 0)
c #EF.  write(6,6004)isnoSV(1),    dsn_SV(1) *dt__SV + BufsSV(1),
c #EF.              (dzsnSV(1,isn)*ro__SV(1,isn),isn=1,isnoSV(1))
 6004   format(i3,'  dsn+Buf=',f6.2,6x,'z dz *ro =',10f6.2,
     .                                       (/,35x,10f6.2))
 
 
C +--Add a new Snow Layer
C +  ====================
 
          DO ikl=1,klonv
c #EC       IF (NLaysv(ikl).gt.0)
c #EC.      write(6,6005) isnoSV(ikl),1.e3*Bdzssv(ikl),Brossv(ikl),
c #EC.                                     BG1ssv(ikl),BG2ssv(ikl)
 6005       format(i3,' dz     = ',f6.3,3x,' ro     = ',f6.1,3x,
     .                ' G1     = ',f6.3,3x,' G2     = ',f6.1)
C +
            isnoSV(ikl)     = isnoSV(ikl)         +NLaysv(ikl)
            isn             = isnoSV(ikl)
            dzsnSV(ikl,isn) = dzsnSV(ikl,isn) * (1-NLaysv(ikl))
     .                      + Bdzssv(ikl)     *    NLaysv(ikl)
            TsisSV(ikl,isn) = TsisSV(ikl,isn) * (1-NLaysv(ikl))
     .                  + min(TaT_SV(ikl),TfSnow) *NLaysv(ikl)
            ro__SV(ikl,isn) = ro__SV(ikl,isn) * (1-NLaysv(ikl))
     .                      + Brossv(ikl)     *    NLaysv(ikl)
            eta_SV(ikl,isn) = eta_SV(ikl,isn) * (1-NLaysv(ikl))   ! + 0.
            agsnSV(ikl,isn) = agsnSV(ikl,isn) * (1-NLaysv(ikl))   ! + 0.
            G1snSV(ikl,isn) = G1snSV(ikl,isn) * (1-NLaysv(ikl))
     .                      + BG1ssv(ikl)     *    NLaysv(ikl)
            G2snSV(ikl,isn) = G2snSV(ikl,isn) * (1-NLaysv(ikl))
     .                      + BG2ssv(ikl)     *    NLaysv(ikl)
            istoSV(ikl,isn) = istoSV(ikl,isn) * (1-NLaysv(ikl))
     .   + max(zero,sign(unun,TaT_SV(ikl)
     .                       -TfSnow-eps_21)) *    istdSV(2)
     .                                        *    NLaysv(ikl)
            BufsSV(ikl)     = BufsSV(ikl)     * (1-NLaysv(ikl))
            NLaysv(ikl)     = 0
          END DO
 
 
C +--Snow Pack Thickness
C +  -------------------
 
          DO ikl=1,klonv
            z_snsv(ikl)     = 0.0
          END DO
        DO   isn=1,nsno
          DO ikl=1,klonv
            z_snsv(ikl)     = z_snsv(ikl) + dzsnSV(ikl,isn)
            zzsnsv(ikl,isn) = z_snsv(ikl)
          END DO
        END DO
 
 
C +--Diffusion of Surficial Water in the Snow Pack
C +  ---------------------------------------------
 
c #sw     DO isn=1,nsno
c #sw     DO ikl=1,klonv
c #sw       PorVol      = 1.     - ro__SV(ikl,isn) /  ro_Ice          !
c #sw       PorVol      =      max(PorVol            ,zero  )         !
c #sw       rWater      = ws0dSV * PorVol     *ro_Wat*dzsnSV(ikl,isn)
c #sw.                  * max(zero,
c #sw.                   sign(unun,rusnSV(ikl)/ro_Wat-zzsnsv(ikl,isn)
c #sw.                                               +dzsnSV(ikl,isn)))
c #sw       rusNEW      =      max(rusnSV(ikl)-rWater,zero  )
c #sw       rWater      =          rusnSV(ikl)-rusNEW
c #sw       rdzNEW          =      rWater
c #sw.                           + ro__SV(ikl,isn) * dzsnSV(ikl,isn)
c #sw       etaNEW          =      rWater / max(epsi,rdzNEW)
c #sw       rusnSV(ikl) =          rusNEW
c #sw       ro__SV(ikl,isn) =      rdzNEW / max(epsi,dzsnSV(ikl,isn))
c #sw       eta_SV(ikl,isn) =      eta_SV(ikl,isn)  +etaNEW
c #sw     ENDDO
c #sw     ENDDO
 
      END IF
 
c #EF   IF          (isnoSV(1) .GT. 0)
c #EF.  write(6,6006)isnoSV(1),    dsn_SV(1) *dt__SV + BufsSV(1),
c #EF.              (dzsnSV(1,isn)*ro__SV(1,isn),isn=1,isnoSV(1))
 6006   format(i3,'  dsn+Buf=',f6.2,6x,'* dz *ro =',10f6.2,
     .                                       (/,35x,10f6.2))
 
 
C +--Blowing Dust
C +  ============
 
c #BD   IF (BloMod)                                               THEN
 
C +       ***************
c #BD     call SISVAT_BDu
C +       ***************
 
c #BD   END IF
 
 
 
C +--Soil      Albedo: Soil Humidity Correction
C +  ==========================================
 
C +...    REFERENCE: McCumber and Pielke (1981), Pielke (1984)
C +       ^^^^^^^^^
          DO ikl=1,klonv
            albssv(ikl) =
     .      alb0SV(ikl) *(1.0-min(demi,eta_SV(       ikl,0)
     .                                /etadSV(isotSV(ikl))))**0.5
cXF
C +...      REMARK: Albedo of Water Surfaces (isotSV=0):
C +         ^^^^^^  alb0SV := 2  X  effective value, while
C +                 eta_SV :=          etadSV
          END DO
 
 
C +--Snow Pack Optical Properties
C +  ============================
 
      IF (SnoMod)                                                 THEN
 
C +          ******
        call SnOptP
C +          ******
 
      ELSE
        DO ikl=1,klonv
          sEX_sv(ikl,1) = 1.0
          sEX_sv(ikl,0) = 0.0
          albisv(ikl)   = albssv(ikl)
        END DO
      END IF
 
C +          **********
! #ve   call SISVAT_wEq('SnOptP',0)
C +          **********
 
 
C +--Solar Radiation Absorption and Effective Leaf Area Index
C +  ========================================================
 
C +          ******
        call VgOptP
C +          ******
 
 
C +--Surface-Canopy Emissivity
C +  =========================
 
        DO ikl=1,klonv
            LSnMsk     =    min( iun,isnoSV(ikl))
            tau_sv(ikl)=    exp(    -LAI_sv(ikl))          ! Veg Transmit.Frac.
            Evg_sv(ikl)=  EmiVeg*(1-LSnMsk)+EmiSno*LSnMsk  ! Veg+Sno Emissivity
            Eso_sv(ikl)=  EmiSol*(1-LSnMsk)+EmiSno*LSnMsk  ! Sol+Sno Emissivity
            emi_SV(ikl)=
     .   (((EmiSol*     tau_sv(ikl)
     .     +EmiVeg*(1.0-tau_sv(ikl))) *LSmask(ikl))
     .    + EmiWat                 *(1-LSmask(ikl)))*(1-LSnMsk)
     .   +  EmiSno                                     *LSnMsk
        END DO
 
 
C +--Soil/Vegetation Forcing/ Upward IR (INPUT, from previous time step)
C +  ===================================================================
 
        DO ikl=1,klonv
! #e1     Enrsvd(ikl) =    - IRs_SV(ikl)
          IRupsv(ikl) =      IRs_SV(ikl) *     tau_sv(ikl) ! Upward   IR
        END DO
 
 
C +--Turbulence
C +  ==========
 
C +--Latent Heat of Vaporization/Sublimation
C +  ---------------------------------------
 
        DO ikl=1,klonv
          SnoWat      =                     min(isnoSV(ikl),0)
          Lx_H2O(ikl) =
     .    (1.-SnoWat) * Lv_H2O
     .  +     SnoWat  *(Ls_H2O * (1.-eta_SV(ikl,isnoSV(ikl)))
     .                 +Lv_H2O *     eta_SV(ikl,isnoSV(ikl)) )
        END DO
 
 
C +--Roughness Length for Momentum
C +  -----------------------------
 
C +--Land+Sea-Ice / Ice-free Sea Mask
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        DO ikl=1,klonv
          IcIndx(ikl) = 0
        ENDDO
        DO isn=1,nsno
        DO ikl=1,klonv
          IcIndx(ikl) = max(IcIndx(ikl),
     .                      isn*max(0,
     .                              sign(1,
     .                                   int(ro__SV(ikl,isn)-900.))))
        ENDDO
        ENDDO
 
        DO ikl=1,klonv
          LISmsk    =     min(iiceSV(ikl),1     )
          LISmsk    =     max(LSmask(ikl),LISmsk)
          IceMsk    =     max(0,sign(1   ,IcIndx(ikl)-1)  )
          SnoMsk    = max(min(isnoSV(ikl)-iiceSV(ikl),1),0)
 
C +--Sea  Roughness Length
C +  ^^^^^^^^^^^^^^^^^^^^^
          Z0mSea =       0.0002
          Z0hSea =       0.000049
 
c #zs     Z0mSea =       0.0185*us__SV(ikl)*us__SV(ikl) ! Doyle MWR 130
c #zs.                         *grvinv                  ! p.3088 2e col
 
          Z0mSea =       0.016 *us__SV(ikl)*us__SV(ikl) ! Wang  MWR 129
     .                         *grvinv                  ! p.1377 (21)
     .           +       0.11  *akmol                   !
     .                         /  max(epsi ,us__SV(ikl))!
 
c #zs     Z0mSea =       0.0185*us__SV(ikl)*us__SV(ikl) ! Wang  MWR 129
c #zs.                         *grvinv                  ! p.1377 (21)
c #zs.           +       0.135 *akmol                   !   (adapted)
c #zs.                         /  max(epsi ,us__SV(ikl))!
 
          Z0hSea =   max(0.000049,                      ! Wang  MWR 129
     .                   0.20  *akmol                   ! p.1377 (22)
     .                         /  max(epsi ,us__SV(ikl)))
 
          Z0mSea =   max(Z0mSea,epsi)                   !
 
C +--Land Roughness Length, Snow Contribution excluded
C +  ^^^^^^^^^^^^^^^^^^^^^^ Ice  Contribution included
C +                         ^^^^^^^^^^^^^^^^^^^^^^^^^^
C +--If vegetation Seasonal Cycle described by  LAI     :
          growth      =min(max(0,7-ivgtSV(ikl)),1)
          Z0mLnd      =     Z0mdSV(ivgtSV(ikl))*LAI_sv(ikl)*growth
     .                                         /LAIdSV
     .                +     Z0mdSV(ivgtSV(ikl))*         (1-growth)
 
C +--If vegetation Seasonal Cycle described by  GLF only:
          Z0mLnd      =
     .             fallen * Z0mLnd
     .        +(1.-fallen)* Z0mdSV(ivgtSV(ikl))*glf_sv(ikl)*growth
     .                 +    Z0mdSV(ivgtSV(ikl))*         (1-growth)
 
C +--Land Roughness Length, Influence of the Masking by Snow
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          Z0mLnd      =max( Z0mLnd   ,
     .                      Z0mdSV(0)*(iun-IceMsk)
     .                     +Z0_ICE   *     IceMsk )
          Z0mLnd      =     Z0mLnd
     .                    -(zzsnsv(ikl,    isnoSV(ikl))
     .                     -zzsnsv(ikl,max(IcIndx(ikl),0)))/7.
          Z0mLnd      =max( Z0mLnd    ,    5.e-5  )  ! Min set := Z0 on *
C +...    Roughness  disappears under Snow
C +       Assumption Height/Roughness Length =  7 is used
 
C +--Z0 Smooth Regime over Snow (Andreas 1995, CRREL Report 95-16, p. 8)
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^
          Z0m_nu =       5.e-5 ! z0s~(10-d)*exp(-vonkar/sqrt(1.1e-03))
 
C +--Z0 Saltat.Regime over Snow (Gallee  et al., 2001, BLM 99 (19) p.11)
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^
          u2star =       us__SV(ikl) *us__SV(ikl)
          Z0mBSn =       u2star      *0.536e-3   -  61.8e-6
          Z0mBSn =   max(Z0mBS0      ,Z0mBSn)
 
C +--Z0 Smooth + Saltat. Regime
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^
          Z0enSV(ikl) =  Z0m_nu
     .                +  Z0mBSn
 
C +--Rough   Snow Surface Roughness Length (Typical Value)
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
c #tz     Z0m_Sn =    0.250e-3 ! Andreas 1995, CRREL Report 95-16, fig.1&p.2
                               ! z0r~(10-d)*exp(-vonkar/sqrt(1.5e-03))-5.e-5
          Z0m_Sn =    2.000e-3 ! Calibration    of MAR
          Z0m_Sn =    1.000e-3 ! Exemple Tuning in RACMO
          Z0m_Sn =    0.500e-3 ! Exemple Tuning in MAR
 
C +--Rough   Snow Surface Roughness Length (Variable Sastrugi Height)
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          A_Fact      =  1.0000        ! Andreas et al., 2004, p.4
                                       ! ams.confex.com/ams/pdfpapers/68601.pdf
 
! Parameterization of z0 dependance on Temperature (C. Amory, 2015)
! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! Z0=f(T) deduced from observations, Adelie Land, dec2012-dec2013
          coefa = 0.1862
          coefb = -55.7718
          ta1 = 255.
          ta2 = 273.15
          ta3 = 273.15+3
          z01 = exp(coefa*ta1 + coefb) ! ~0.25 mm
          z02 = exp(coefa*ta2 + coefb) ! ~7 mm
          z03 = z01
          coefc = log(z03/z02)/(ta3-ta2)
          coefd = log(z03)-coefc*ta3
          if (TaT_SV(ikl) .lt. ta1) then
            Z0_obs = z01
          else if (TaT_SV(ikl).ge.ta1 .and. TaT_SV(ikl).lt.ta2) then
            Z0_obs = exp(coefa*TaT_SV(ikl) + coefb)
          else if (TaT_SV(ikl).ge.ta2 .and. TaT_SV(ikl).lt.ta3) then
            ! if st > 0, melting induce smooth surface
            Z0_obs = exp(coefc*TaT_SV(ikl) + coefd)
          else
            Z0_obs = z03
          endif
 
cCA       Snow roughness lenght deduced from observations
cCA       (parametrization if no Blowing Snow)
cCA       ----------------------------------- C. Agosta 09-2016 -----
cCA       Substract Z0enSV(ikl) because re-added later in Z0mnSV(ikl)
          Z0m_Sn = Z0_obs - Z0enSV(ikl)
cCA       -----------------------------------------------------------
 
          param = Z0_obs/1. ! param(s) | 1.(m/s)=TUNING
 
c #SZ     Z0Sa_N =                   (us__SV(ikl) -0.2)*param   ! 0.0001=TUNING
c #SZ.           * max(zero,sign(unun,TfSnow-eps9
c #SZ.                               -TsisSV(ikl , isnoSV(ikl))))
!!#SZ     Z0SaSi = max(zero,sign(unun,Z0Sa_N                  ))! 1 if erosion
c #SZ     Z0SaSi = max(zero,sign(unun,zero  -eps9 -uss_SV(ikl)))!
c #SZ     Z0Sa_N = max(zero,          Z0Sa_N)
c #SZ     Z0SaSV(ikl) =
c #SZ.             max(Z0SaSV(ikl)   ,Z0SaSV(ikl)
c #SZ.               + Z0SaSi*(Z0Sa_N-Z0SaSV(ikl))*exp(-dt__SV/43200.))
c #SZ.               -            min(dz0_SV(ikl) ,     Z0SaSV(ikl))
 
c #SZ     A_Fact      =               Z0SaSV(ikl) *  5.0/0.15   ! A=5 if h~10cm
C +...    CAUTION: The influence of the sastrugi direction is not yet included
 
c #SZ     Z0m_Sn =                    Z0SaSV(ikl)               !
c #SZ.                              - Z0m_nu                    !
 
C +--Z0 Saltat.Regime over Snow (Shao & Lin, 1999, BLM 91 (46)  p.222)
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^
          sqrrZ0 =       usthSV(ikl)/max( us__SV(ikl),0.001)
          sqrrZ0 =                   min( sqrrZ0     ,0.999)
          Z0mBSn =       0.55 *0.55 *exp(-sqrrZ0     *sqrrZ0)
     .                  *us__SV(ikl)*     us__SV(ikl)*grvinv*0.5
 
C +--Z0 Smooth + Saltat. Regime (Shao & Lin, 1999, BLM 91 (46)  p.222)
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^
          Z0enSV(ikl) = (Z0m_nu     **    sqrrZ0 )
     .                * (Z0mBSn     **(1.-sqrrZ0))
          Z0enSV(ikl) =  max(Z0enSV(ikl), Z0m_nu)
 
C +--Z0 Smooth Regime over Snow (Andreas etAl., 2004
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^  ams.confex.com/ams/pdfpapers/68601.pdf)
          Z0m_nu = 0.135*akmol  / max(us__SV(ikl) , epsi)
 
C +--Z0 Saltat.Regime over Snow (Andreas etAl., 2004
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^  ams.confex.com/ams/pdfpapers/68601.pdf)
          Z0mBSn = 0.035*u2star      *grvinv
 
C +--Z0 Smooth + Saltat. Regime (Andreas etAl., 2004
!    (      used by Erosion)     ams.confex.com/ams/pdfpapers/68601.pdf)
!    ^^^^^^^^^^^^^^^^^^^^^^^^^^
          Z0enSV(ikl) =  Z0m_nu
     .                +  Z0mBSn
 
C +--Z0 Rough  Regime over Snow (Andreas etAl., 2004
C +  (.NOT. used by Erosion)     ams.confex.com/ams/pdfpapers/68601.pdf)
!    ^^^^^^^^^^^^^^^^^^^^^^^^^^
!!#ZA     u2star =      (us__SV(ikl) -0.1800)     / 0.1
!!#ZA     Z0m_Sn =A_Fact*Z0mBSn *exp(-u2star*u2star)
          Z0m_90 =(10.-0.025*VVs_SV(ikl)/5.)
     .            *exp(-0.4/sqrt(.00275+.00001*max(0.,VVs_SV(ikl)-5.)))
          Z0m_Sn =           DDs_SV(ikl)* Z0m_90 / 45.
     .         - DDs_SV(ikl)*DDs_SV(ikl)* Z0m_90 /(90.*90.)
 
C +--Z0  (Erosion)    over Snow (instantaneous or time average)
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^
          Z0e_SV(ikl) =  Z0enSV(ikl)
          Z0e_SV(ikl) =  Z0emSV(ikl)
 
C +--Momentum  Roughness Length
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^                              ! Contribution of
          Z0mnSV(ikl) =  Z0mLnd                              ! Vegetation Form
     .                + (Z0m_Sn                              ! Sastrugi   Form
     .                +  Z0enSV(ikl))   *SnoMsk              ! Snow    Erosion
 
C +--Mom. Roughness Length, Discrimination among Ice/Land  and Ice-Free Ocean
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          Z0mnSV(ikl) =  Z0mnSV(ikl)    *LISmsk              ! Ice and  Land
     .                  +Z0mSea      *(1-LISmsk)             ! Ice-Free Ocean
c #OR.                  +Z0roSV(ikl)                         ! Subgrid  Topogr.
 
C +--GIS  Roughness Length
C +  ^^^^^^^^^^^^^^^^^^^^^
          Z0mnSV(ikl) =
     .      (1-LSmask(ikl)) *     Z0mnSV(ikl)
     .    +    LSmask(ikl)  * max(Z0mnSV(ikl),max(Z0_GIM,
     .                                            Z0_GIM+
     .      (0.0032-Z0_GIM)*(ro__SV(ikl,isnoSV(ikl))-600.)   !
     .                     /(920.00                 -600.))) !
 
C +--Mom. Roughness Length, Instantaneous OR Box Moving Average in Time
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          Z0m_SV(ikl) =  Z0mnSV(ikl)                         ! Z0mnSV  instant.
          Z0m_SV(ikl) =  Z0mmSV(ikl)                         ! Z0mnSV  Average
 
C +--Corrected Threshold Friction Velocity before Erosion    ! Marticorena and
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^    ! Bergametti 1995
c #BS     Z0e_SV(ikl) =   min(Z0m_SV(ikl),Z0e_SV(ikl))       !
c #MB     f_eff=    log(0.35*(0.1        /Z0e_SV(ikl))**0.8) ! JGR 100
c #MB     f_eff=1.-(log(      Z0m_SV(ikl)/Z0e_SV(ikl)      ))! (20) p. 16420
c #MB.            /(max(      f_eff      ,epsi             ))! p.16426 2nd ?
c #MB     f_eff=    max(      f_eff      ,epsi              )! CONTROL
c #MB     f_eff=1.0   -(1.0 - f_eff)     /5.00               ! TUNING
c #MB     f_eff=    min(      f_eff      ,1.00              )!
c #MB     usthSV(ikl) =       usthSV(ikl)/f_eff              !
 
 
C +--Roughness Length for Scalars
C +  ----------------------------
 
          Z0hnSV(ikl) =     Z0mnSV(ikl)/  7.4
c #SH     Z0hnSV(ikl) =     Z0mnSV(ikl)/100.0
C +                         Z0h = Z0m  /100.0   over the Sahel
C +                                            (Taylor & Clark, QJRMS 127,p864)
 
c #RN     rstar       =     Z0mnSV(ikl) * us__SV(ikl) / akmol
c #RN     rstar       = max(epsi,min(rstar,thous))
c #RN     alors       =          log(rstar)
c #RN     rstar0      = 1.250e0 * max(zero,sign(unun,0.135e0 - rstar))
c #RN.                +(1.      - max(zero,sign(unun,0.135e0 - rstar)))
c #RN.                *(0.149e0 * max(zero,sign(unun,2.500e0 - rstar))
c #RN.                + 0.317e0
c #RN.                *(1.      - max(zero,sign(unun,2.500e0 - rstar))))
c #RN     rstar1      = 0.      * max(zero,sign(unun,0.135e0 - rstar))
c #RN.                +(1.      - max(zero,sign(unun,0.135e0 - rstar)))
c #RN.                *(-0.55e0 * max(zero,sign(unun,2.500e0 - rstar))
c #RN.                - 0.565
c #RN.                *(1.      - max(zero,sign(unun,2.500e0 - rstar))))
c #RN     rstar2      = 0.      * max(zero,sign(unun,0.135e0 - rstar))
c #RN.                +(1.      - max(zero,sign(unun,0.135e0 - rstar)))
c #RN.                *(0.      * max(zero,sign(unun,2.500e0 - rstar))
c #RN.                - 0.183
c #RN.                *(unun    - max(zero,sign(unun,2.500e0 - rstar))))
 
cXF    #RN does not work over bare ice
cXF    MAR is then too warm and not enough melt
 
c #RN     if(ro__SV(ikl,isnoSV(ikl))>50
c #RN.  .and.ro__SV(ikl,isnoSV(ikl))<roSdSV)then
 
c #RN     Z0hnSV(ikl) = max(zero
c #RN.                , sign(unun,zzsnsv(ikl,isnoSV(ikl))-epsi))
c #RN.                * exp(rstar0+rstar1*alors+rstar2*alors*alors)
c #RN.                * 0.001e0 + Z0hnSV(ikl) * ( 1. - max(zero
c #RN.                , sign(unun,zzsnsv(ikl,isnoSV(ikl))-epsi)))
 
c #RN     endif
 
          Z0hnSV(ikl) =     Z0hSea             *(1-LISmsk) ! Ice-free Ocean
     .                +     Z0hnSV(ikl)        *   LISmsk  ! Ice and  Land
 
          Z0h_SV(ikl) =     Z0hnSV(ikl)
          Z0h_SV(ikl) =     Z0hmSV(ikl)
 
 
C +--Contributions of the Roughness Lenghths to the neutral Drag Coefficient
C +  -----------------------------------------------------------------------
 
c #MT     Z0m_SV(ikl) = max(2.0e-6     ,Z0m_SV(ikl)) ! Min Z0_m (Garrat Scheme)
          Z0m_SV(ikl) = min(Z0m_SV(ikl),za__SV(ikl)*0.3333)
          sqrCm0(ikl) = log(za__SV(ikl)/Z0m_SV(ikl))
          sqrCh0(ikl) = log(za__SV(ikl)/Z0h_SV(ikl))
 
! #wz     IF (ikl.EQ.1) write(6,6661) dsn_SV(ikl),us__SV(ikl),Z0SaSi
! #wz.                        ,Z0Sa_N,Z0SaSV(ikl),Z0m_Sn,Z0m_SV(ikl)
 6661     format(7f9.6)
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! OUTPUT           of Roughness Length and Drag Coefficients
! #sz     IF (ii__SV(ikl).EQ.iwr_SV .AND. jj__SV(ikl).EQ. jwr_SV .AND.
! #sz.        nn__SV(ikl).EQ.nwr_SV)
! #sz.    write(6,6600)  za__SV(ikl) , Z0m_SV(ikl)
! #sz.                  ,sqrCm0(ikl) , za__SV(ikl)/Z0m_SV(ikl)
! #sz.                  ,Z0SaSV(ikl) , Z0h_SV(ikl)
! #sz.                  ,sqrCh0(ikl) , za__SV(ikl)/Z0h_SV(ikl)
 6600     format(/,' ** SISVAT     *0  '
     .            ,'  za__SV  = ',e12.4,'  Z0m_SV  = ',e12.4
     .            ,'  sqrCm0  = ',e12.4,'  Za/Z0m  = ',e12.4
     .          ,/,'                   '
     .            ,'  Z0SaSV  = ',e12.4,'  Z0h_SV  = ',e12.4
     .            ,'  sqrCh0  = ',e12.4,'  Za/Z0h  = ',e12.4)
 
 
C +--Vertical Stability Correction
C +  -----------------------------
 
C +--Surface/Canopy Temperature
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^
          Tsrfsv(ikl) = Sigmsv(ikl) * TvegSV(ikl)
     .          + (1. - Sigmsv(ikl))* TsisSV(ikl,isnoSV(ikl))
        END DO
 
C +--Aerodynamic Resistance
C +  ^^^^^^^^^^^^^^^^^^^^^^
c #CP   IF            (SnoMod.AND.ColPrt)                           THEN
 
C +              **********
c #CP       call ColPrt_SBL
C +              **********
 
c #CP   ELSE
c #MT    IF           (Garrat)                                      THEN
 
C +              **********
c #MT       call SISVAT_SBL
C +              **********
 
c #MT    ELSE
 
C +              **********
            call SISVATeSBL
C +              **********
 
c #MT    END IF
c #CP   END IF
 
 
C +--Friction Velocity
C +  -----------------
 
c #US   DO ikl=1,klonv
c #WR     u_star      = sqrt(VV__SV(ikl)/ram_sv(ikl))
c #WR     write(6,*)  u_star,us__SV(ikl)
c #US     us__SV(ikl) = sqrt(VV__SV(ikl)/ram_sv(ikl))
c #US   END DO
 
 
! Canopy Energy Balance
! =====================
 
C +          **********
        call SISVAT_TVg
! #e1.                 (ETVg_d)
C +          **********
 
 
C +--Surface/Canopy Temperature
C +  ==========================
 
        DO ikl=1,klonv
          Tsrfsv(ikl) = Sigmsv(ikl) * TvegSV(ikl)
     .          + (1. - Sigmsv(ikl))* TsisSV(ikl,isnoSV(ikl))
        END DO
 
 
! Soil   Energy Balance
! =====================
 
C +          **********
        call SISVAT_TSo
! #e1.                 (ETSo_0,ETSo_1,ETSo_d)
C +          **********
 
C +          **********
! #ve   call SISVAT_wEq('_TSo  ',0)
C +          **********
 
 
C +--Canopy Water  Balance
C +  =====================
 
C +--Soil Water     Potential
C +  ------------------------
 
      DO   isl=-nsol,0
        DO ikl=1,klonv
          ist             =     isotSV(ikl)        ! Soil Type
          psi_sv(ikl,isl) =     psidSV(ist)        ! DR97, Eqn.(3.34)
     .  *(etadSV(ist) /max(epsi,eta_SV(ikl,isl)))  !
     .  **bCHdSV(ist)                              !
 
 
C +--Soil Hydraulic Conductivity
C +  ---------------------------
 
          Khydsv(ikl,isl) =    s2__SV(ist)         ! DR97, Eqn.(3.35)
     .  *(eta_SV(ikl,isl)**(2.*bCHdSV(ist)+3.))    !
        END DO
      END DO
 
C +          **********
        call SISVAT_qVg
C +          **********
 
 
! Vegetation Forcing
! ------------------
 
! #m0   DO ikl=1,klonv
! #m0     Watsvd(ikl) =     (Watsvd(ikl)           ! Canopy Precip. IN
! #m0.                      -drr_SV(ikl)           ! Canopy Precip. OUT
! #m0.                      -Evp_sv(ikl))* dt__SV  ! Canopy Water Evap.
! #m0   END DO
 
 
C +--Melting / Refreezing in the Snow Pack
C +  =====================================
 
      IF (SnoMod)                                                 THEN
 
C +          **********
        call SISVAT_qSn
     .                 (
! #e1.                  EqSn_0,EqSn_1,EqSn_d
! #m1.                 ,SIsubl,SImelt,SIrnof
     .                 )
C +          **********
 
C +          **********
! #ve   call SISVAT_wEq('_qSn  ',0)
C +          **********
 
c #EF   IF          (isnoSV(1) .GT. 0)
c #EF.  write(6,6007)isnoSV(1),    dsn_SV(1) *dt__SV + BufsSV(1),
c #EF.              (dzsnSV(1,isn)*ro__SV(1,isn),isn=1,isnoSV(1))
 6007   format(i3,'  dsn+Buf=',f6.2,6x,'q dz *ro =',10f6.2,
     .                                       (/,35x,10f6.2))
 
 
C +--Snow Pack Thickness
C +  -------------------
 
          DO ikl=1,klonv
            z_snsv(ikl)     = 0.0
          END DO
        DO   isn=1,nsno
          DO ikl=1,klonv
            z_snsv(ikl)     = z_snsv(ikl) + dzsnSV(ikl,isn)
            zzsnsv(ikl,isn) = z_snsv(ikl)
          END DO
        END DO
 
 
C +--Energy in Excess is added to the first Soil Layer
C +  -------------------------------------------------
 
        DO ikl=1,klonv
            z_snsv(ikl)   = max(zero,
     .                          sign(unun,epsi-z_snsv(ikl)))
            TsisSV(ikl,0) = TsisSV(ikl,0)    + EExcsv(ikl)
     .                                       /(rocsSV(isotSV(ikl))
     .                                        +rcwdSV*eta_SV(ikl,0))
            EExcsv(ikl)   = 0.
        END DO
 
 
! Snow Final   Mass (below the Canopy) and Forcing
! ------------------------------------------------
 
! #m1   DO ikl=1,klonv
! #m1     SIWa_f(ikl) =(drr_SV(ikl) + dsn_SV(ikl))   *dt__SV         ![mm w.e.]
! #m1     SIWe_f(ikl) = dbs_SV(ikl)                                  !
! #m1     SIWm_1(ikl) = BufsSV(ikl) + HFraSV(ikl)    *ro_Ice         !
! #m1   DO isn=1,nsno                                                !
! #m1     SIWm_1(ikl) = SIWm_1(ikl) + dzsnSV(ikl,isn)*ro__SV(ikl,isn)!
! #m1   END DO                                                       !
! #m1   END DO                                                       !
 
      END IF
 
 
! Soil   Water  Balance
! =====================
 
C +          **********
        call SISVAT_qSo
! #m0.                 (Wats_0,Wats_1,Wats_d
C +          **********
 
 
C +--Surface/Canopy Fluxes
C +  =====================
 
        DO ikl=1,klonv
          IRdwsv(ikl)=tau_sv(ikl) *IRd_SV(ikl)*Eso_sv(ikl) ! Downward IR
     .          +(1.0-tau_sv(ikl))*IRd_SV(ikl)*Evg_sv(ikl) !
          IRupsv(ikl) =      IRupsv(ikl)                   ! Upward   IR
     .                + 0.5 *IRv_sv(ikl) * (1.-tau_sv(ikl))!
          IRu_SV(ikl) =     -IRupsv(ikl)                   ! Upward   IR
     .                      +IRd_SV(ikl)                   ! (effective)
     .                      -IRdwsv(ikl)                   ! (positive)
          TBr_sv(ikl) =sqrt(sqrt(IRu_SV(ikl)/stefan))      ! Brightness
!                                                          ! Temperature
          uts_SV(ikl) =     (HSv_sv(ikl) +HSs_sv(ikl))     ! u*T*
     .                     /(rhT_SV(ikl) *Cp)              !
          uqs_SV(ikl) =     (HLv_sv(ikl) +HLs_sv(ikl))     ! u*q*
     .                     /(rhT_SV(ikl) *Lv_H2O)          !
 
C +--Surface/Canopy Temperature
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^
          Tsrfsv(ikl) = Sigmsv(ikl) * TvegSV(ikl)
     .          + (1. - Sigmsv(ikl))* TsisSV(ikl,isnoSV(ikl))
        END DO
 
 
C +--Snow Pack Properties (sphericity, dendricity, size)
C +  ===================================================
 
      IF (SnoMod)                                                 THEN
cXF
      if (klonv.eq.1) then
       if(isnoSV(1).ge.1) then
C +          **********
        call SISVAT_GSn
C +          **********
       endif
      else
C +          **********
        call SISVAT_GSn
C +          **********
      endif
 
C +          **********
! #ve   call SISVAT_wEq('_GSn  ',0)
C +          **********
 
 
C +--Surficial Water Freezing, including that of a Water Surface (isotSV=0)
C +  ======================================================================
 
 
      END IF
 
 
C +--OUTPUT
C +  ======
 
c #E0   DO ikl=1,klonv
c #E0   IF (lwriSV(ikl).ne.0)                                     THEN
c #E0           noUNIT =  no__SV(lwriSV(ikl))
c #E0     write(noUNIT,5001)
c #E0.       (SoSosv(ikl)+SoCasv(ikl))*sol_SV(ikl),
c #E0.        IRdwsv(ikl),IRu_SV(ikl),
c #E0.        HSv_sv(ikl)+HSs_sv(ikl),
c #E0.        HLv_sv(ikl)+HLs_sv(ikl), TaT_SV(ikl),
c #E0.        dsn_SV(ikl)*3.6e3,       drr_SV(ikl)*3.6e3,
c #E0.        SoSosv(ikl)             *sol_SV(ikl),
c #E0.                    IRv_sv(ikl) *0.5,
c #E0.        HSv_sv(ikl),HLv_sv(ikl), TvegSV(ikl),
c #E0.                    SoCasv(ikl) *sol_SV(ikl),
c #E0.        HSs_sv(ikl),HLs_sv(ikl), TsisSV(ikl,isnoSV(ikl))
 5001     format(
     .         '        |Net Solar| IR Down | IR Up   | HS/Dwn=+|',
     .          ' HL/Dwn=+| Temper. |         |  Snow  |  Rain  |',
     .       /,'        | [W/m2]  | [W/m2]  | [W/m2]  | [W/m2]  |',
     .          ' [W/m2]  | [K]     |         | [mm/h] | [mm/h] |',
     .       /,' -------+',7('---------+'),2('--------+'),
     .       /,' SISVAT |',f8.1,' |',f8.1,' |',f8.1,' |',f8.1,' |',
     .                     f8.1,' |A',f7.2,' |', 8x ,' |',2(f7.2,' |'),
     .       /,' Canopy |',f8.1,' |', 8x ,' |',f8.1,' |',f8.1,' |',
     .                     f8.1,' |',f8.2,' |', 8x ,' |',2( 7x ,' |')
     .       /,' Soil   |',f8.1,' |', 8x ,' |', 8x ,' |',f8.1,' |',
     .                     f8.1,' |',f8.2,' |', 8x ,' |',2( 7x ,' |'))
 
 
C +--Energy Budget
C +  -------------
 
! #e1     Enrsvd(ikl) = Enrsvd(ikl)                            ! Up Surf. IR
! #e1.                + IRs_SV(ikl)                            ! Offset
! #e1.        + (      (SoSosv(ikl)                            ! Net   Solar
! #e1.                 +SoCasv(ikl)) *sol_SV(ikl)              !
! #e1.          +                     IRdwsv(ikl)              ! Downward IR
! #e1.          +                     IRupsv(ikl)              ! Upward   IR
! #e1.          +                     HSv_sv(ikl)+HSs_sv(ikl)  ! Sensible
! #e1.          +                     HLv_sv(ikl)+HLs_sv(ikl)) ! Latent
 
! #e1     write(noUNIT,5002)           Enrsvd(ikl),
! #e1.                   ETSo_0(ikl),  ETSo_d(ikl),
! #e1.                   ETSo_0(ikl)+  ETSo_d(ikl), ETSo_1(ikl),
! #e1.                   EqSn_0(ikl)                            /dt__SV,
! #e1.                   EqSn_d(ikl)                            /dt__SV,
! #e1.                  (EqSn_1(ikl)-  EqSn_0(ikl)- EqSn_d(ikl))/dt__SV,
! #e1.                   EqSn_1(ikl)                            /dt__SV
 5002     format(
     .           ' -----------------+-------------------+', !
     .            '-----------------+-+-----------------+', !
     .          '-------------------+',                     !
     .         /,' SOIL/SNOW/VEGET. |                   |', !
     .            ' Power,  Forcing |                   |', ! Enrsvd
     .          '                   |',                     !
c #WR.         /,' -----------------+-------------------+', !
c #WR.            '-----------------+-------------------+', !
c #WR.          '-------------------+',                     !
     .         /,'                  |',   11x ,'        |', !
     .                f9.2,' [W/m2] |',   11x ,'        |', ! Enrsvd
     .                11x ,'        |',                     !
     .         /,' -----------------+-------------------+', !
     .            '-----------------+-------------------+', !
     .          '-------------------+',                     !
     .         /,' SOIL/SNOW  (TSo) | Energy/dt, Time 0 |', !        ETSo_0
     .            ' Power,  Forcing |   Sum Tim.0+Forc. |', ! ETSo_d/ETSo_0+d
     .          ' Energy/dt, Time 1 |',                     ! ETSo_1
c #WR.         /,' -----------------+-------------------+', !
c #WR.            '-----------------+-------------------+', !
c #WR.          '-------------------+',                     !
     .         /,'                  |',  f11.2,' [W/m2] |', !        ETSo_0
     .                f9.2,' [W/m2] |',  f11.2,' [W/m2] |', ! ETSo_d/ETSo_0+d
     .               f11.2,' [W/m2] |',                     ! ETSo_1
     .         /,' -----------------+-------------------+', !
     .            '-----------------+-------------------+', !
     .          '-------------------+',                     !
     .         /,'      SNOW  (qSn) | Energy/dt, Time 0 |', ! EqSn_0/dt
     .            ' Power,  Excess  |   D(Tim.1-0-Forc.)|', ! EqSn_d/dt, 1-0-d
     .          ' Energy/dt, Time 1 |',                     ! EqSn_1/dt
c #WR.         /,' -----------------+-------------------+', !
c #WR.            '-----------------+-------------------+', !
c #WR.          '-------------------+',                     !
     .         /,'                  |',  f12.2, '[W/m2] |', ! EqSn_0/dt
     .                f9.2,' [W/m2] |',  f11.2,' [W/m2] |', ! EqSn_d/dt, 1-0-d
     .               f12.2, '[W/m2] | ',                    ! EqSn_1/dt
     .         /,' -----------------+-------------------+', !
     .            '-----------------+-------------------+', !
     .          '-------------------+')                     !
 
! #e1             EnsBal = ETSo_1(ikl)-(ETSo_0(ikl)+Enrsvd(ikl))
! #e1             EnvBal = Enrsvd(ikl)- ETVg_d(ikl)
! #e1     IF (abs(EnsBal).gt.5.e-1)
! #e2.    .OR.lwriSV(ikl).eq.    2)
! #e1.                                                              THEN
! #e1       write(6,6001) daHost,i___SV(lwriSV(ikl)),
! #e1.                           j___SV(lwriSV(ikl)),
! #e1.                           n___SV(lwriSV(ikl)),
! #e1.                           ETSo_1(ikl),ETSo_0(ikl),ETSo_d(ikl),
! #e1.                           ETSo_1(ikl)-ETSo_0(ikl)-ETSo_d(ikl),
! #e1.                           Enrsvd(ikl),ETVg_d(ikl),ETSo_d(ikl),
! #e1.                           Enrsvd(ikl)-ETVg_d(ikl)-ETSo_d(ikl)
 6001       format(a18,3i4,' (EB1'           ,f15.6,
     .                ')  - [(EB0           ',f15.6,')',
     .               /,55x,'+(ATM->Snow/Soil',f15.6,')] ',
     .                     '= EBAL'          ,f15.6,' [W/m2]',
     .               /,55x,' (ATM->SISVAT'   ,f18.6,
     .               /,55x,'- Veg. ImBal.',   f18.6,')  ',
     .               /,55x,'- ATM->SnoSol',   f18.6,')  ',
     .                     '= ????'          ,f15.6,' [W/m2]')
! #e1           noEBal = noEBal + 1
! #e2           noEBal = noEBal - 1
! #e1       IF (noEBal.GE.       10) stop 'TOO MUCH ENERGY IMBALANCES'
! #e1     END IF
 
 
C +--Snow   Budget [mm w.e.]
C +  -----------------------
 
! #m1     write(noUNIT,5010)
! #m1.             SIWm_0(ikl),  SIWa_i(ikl)-SIWa_f(ikl)
! #m1.            ,SIWm_0(ikl)+  SIWa_i(ikl)-SIWa_f(ikl)
! #m1.                          +SIWe_i(ikl)-SIWe_f(ikl)
! #m1.                          +SIsubl(ikl)
! #m1.                          -SImelt(ikl)
! #m1.                          -SIrnof(ikl)
! #m2.                          +SIvAcr(ikl)
! #m1.            ,SIWm_1(ikl),  SIWe_i(ikl)-SIWe_f(ikl)
! #m1.            ,              SIsubl(ikl)
! #m1.            ,             -SImelt(ikl)
! #m1.            ,             -SIrnof(ikl)
! #m2.            ,              SIvAcr(ikl)
 5010     format(' SNOW             |   Snow,   Time 0  |',
     .            ' Snow,   Forcing |           Sum     |',
     .          '   Snow,   Time 1  |',
c #WR.         /,' -----------------+-------------------+',
c #WR.            '-----------------+-------------------+',
c #WR.          '-------------------+',
     .         /,'                  |',    f13.3,' [mm] |',
     .           ' A',  f9.3,' [mm] |',    f13.3,' [mm] |',
     .                 f13.3,' [mm] |',
     .         /,'                  |',     13x ,'      |',
     .           ' E',  f9.3,' [mm] |',     13x ,'      |',
     .                  13x ,'      |',
     .         /,'                  |',     13x ,'      |',
     .           ' S',  f9.3,' [mm] |',     13x ,'      |',
     .                  13x ,'      |',
     .         /,'                  |',     13x ,'      |',
     .           '(M',  f9.3,' [mm])|  (included in A)  |',
     .                  13x ,'      |',
     .         /,'                  |',     13x ,'      |',
     .           ' R',  f9.3,' [mm] |',     13x ,'      |',
     .                  13x ,'      |',
! #m2.         /,'                  |',     13x ,'      |',
! #m2.           ' O',  f9.3,' [mm] |',     13x ,'      |',
! #m2.                  13x ,'      |',
     .         /,' -----------------+-------------------+',
     .            '-----------------+-------------------+',
     .          '-------------------+')
! #m1             SnoBal = SIWm_1(ikl)-(SIWm_0(ikl)
! #m1.                                 +SIWa_i(ikl)-SIWa_f(ikl)
! #m1.                                 +SIWe_i(ikl)-SIWe_f(ikl))
! #m1.                                 -SIsubl(ikl)
! #m1.                                 +SIrnof(ikl)
! #m2.                                 -SIvAcr(ikl)
! #m1     IF (abs(SnoBal).gt.epsi)                                  THEN
! #m1       write(6,6010) daHost,i___SV(lwriSV(ikl)),
! #m1.                           j___SV(lwriSV(ikl)),
! #m1.                           n___SV(lwriSV(ikl)),
! #m1.                           SIWm_1(ikl),SIWm_0(ikl),
! #m1.                           SIWa_i(ikl),SIWa_f(ikl),
! #m1.                           SIWe_i(ikl),SIWe_f(ikl),
! #m1.                           SIsubl(ikl),SImelt(ikl),
! #m2.                           SIrnof(ikl),SIvAcr(ikl),
! #m1.                           SnoBal
 6010       format(a18,3i4,' (MB1'        ,f12.6,
     .                 ') - [(MB0        ',f12.6,        15x,')',
     .               /,51x,'+(ATM Forcing',f12.6,' - ',f12.6,')',
     .               /,51x,'+(BLS Forcing',f12.6,' - ',f12.6,')',
     .               /,51x,'-(Depo/Sublim',f12.6,        15x,')',
     .               /,51x,' !Melting    ',f12.6,'  included in A!',
     .               /,51x,'+(Run  OFF   ',f12.6,        15x,')',
! #m2.               /,51x,'-(Sea-Ice Acr',f12.6,        15x,')',
     .               /,29x,'= *BAL'       ,f12.6,      ' [mm w.e.]')
! #m1           noSBal = noSBal + 1
! #m1       IF (noSBal.GE.       10) stop 'TOO MUCH SNOW MASS IMBALANCE'
! #m1     END IF
 
 
C +--Water  Budget
C +  -------------
 
! #m0     Watsv0(ikl) =  Watsv0(ikl)                    ! Canopy Water Cont.
! #m0.                 + Wats_0(ikl)                    ! Soil   Water Cont.
! #m0     Watsvd(ikl) =  Watsvd(ikl)                    ! Canopy Forcing
! #m0.                 + Wats_d(ikl)                    ! Soil   Forcing
 
! #m0     write(noUNIT,5003)
! #m0.                   Wats_0(ikl),  Wats_d(ikl),
! #m0.                   Wats_0(ikl)+  Wats_d(ikl),    Wats_1(ikl),
! #m0.                   Watsv0(ikl),  Watsvd(ikl),
! #m0.                   Watsv0(ikl)+  Watsvd(ikl),    Wats_1(ikl)
! #m0.                                                +rrCaSV(ikl)
 5003     format(' SOIL/SNOW  (qSo) |   Water,  Time 0  |',
     .            ' Water,  Forcing |           Sum     |',
     .          '   Water,  Time 1  |',
c #WR.         /,' -----------------+-------------------+',
c #WR.            '-----------------+-------------------+',
c #WR.          '-------------------+',
     .         /,'                  |',    f13.3,' [mm] |',
     .                 f11.3,' [mm] |',    f13.3,' [mm] |',
     .                 f13.3,' [mm] |',
     .         /,' -----------------+-------------------+',
     .            '-----------------+-------------------+',
     .          '-------------------+',
     .         /,' SOIL/SNOW/VEGET. |   Water,  Time 0  |',
     .            ' Water,  Forcing |           Sum     |',
     .          '   Water,  Time 1  |',
c #WR.         /,' -----------------+-------------------+',
c #WR.            '-----------------+-------------------+
c #WR.          '-------------------+',
     .         /,'                  |',    f13.3,' [mm] |',
     .                 f11.3,' [mm] |',    f13.3,' [mm] |',
     .                 f13.3,' [mm] |',
     .         /,' -----------------+-------------------+',
     .            '-----------------+-------------------+',
     .          '-------------------+')
 
! #m0             WatBal = Wats_1(ikl)+rrCaSV(ikl)
! #m0.                   -(Watsv0(ikl)+Watsvd(ikl))
! #m0     IF (abs(WatBal).gt.epsi)                                  THEN
! #m0       write(6,6002) daHost,i___SV(lwriSV(ikl)),
! #m0.                           j___SV(lwriSV(ikl)),
! #m0.                           n___SV(lwriSV(ikl)),
! #m0.                           Wats_1(ikl),rrCaSV(ikl),
! #m0.                           Watsv0(ikl),Watsvd(ikl),WatBal,
! #m0.                           Wats_1(ikl),
! #m0.                           Wats_0(ikl),Wats_d(ikl),
! #m0.               Wats_1(ikl)-Wats_0(ikl)-Wats_d(ikl)
 6002       format(30x,' NEW Soil Water',3x,' Canopy   Water',3x,
     .                 ' OLD SVAT Water',4x,' FRC SVAT Water',
     .           /,a18,3i4,f15.6,' + ' ,f15.6,' - ' ,f15.6,
     .                           ' -  ',f15.6,'    ', 15x ,'    ',
     .      /,31x,'= ',f12.6,' [mm] (Water Balance)',
     .           /,30x,' NEW Soil Water',3x,'               ',3x,
     .                 ' OLD Soil Water',4x,' FRC Soil Water',
     .           /,30x,f15.6,'   ' , 15x ,' - ' ,f15.6,
     .                       ' -  ',f15.6,'    ', 15x ,'    ',
     .      /,31x,'= ',f12.6,' [mm] (3 terms SUM)')
! #m0           noWBal = noWBal + 1
! #m0       IF (noWBal.GE.       10) stop 'TOO MUCH WATER  IMBALANCES'
! #m0     END IF
 
 
C +--Water/Temperature Profiles
C +  --------------------------
 
c #E0       write(noUNIT,5004)
 5004       format(' -----+--------+--+-----+--------+----+---+',
     .  '--------+----+---+--------+------+-+--------+--------+',
     .           /,'    n |     z  |     dz |     ro |    eta |',
     .  '     T  |     G1 |     G2 | Extinc |        | HISTORY|',
     .           /,'      |    [m] |    [m] | [kg/m3]| [m3/m3]|',
     .  '    [K] |    [-] |    [-] |    [-] |        |   [-]  |',
     .           /,' -----+--------+--------+--------+--------+',
     .  '--------+--------+--------+--------+--------+--------+')
c #E0       write(noUNIT,5005) rusnSV(ikl),albisv(ikl)
 5005       format('      |        |        |        |W',f6.3,' |',
     .  '        |        |        |A',f6.3,' |        |        |')
c #E0       write(noUNIT,5015)
c #E0.                    (isn,zzsnsv(ikl,isn),dzsnSV(ikl,isn),
c #E0.                         ro__SV(ikl,isn),eta_SV(ikl,isn),
c #E0.                         TsisSV(ikl,isn),
c #E0.                         G1snSV(ikl,isn),G2snSV(ikl,isn),
c #E0.                         sEX_sv(ikl,isn),istoSV(ikl,isn),
c #E0.                     isn=isnoSV(ikl),1,-1)
 5015       format((i5,' |',2(f7.3,' |'),               f7.1,' |',
     .           f7.3,' |' ,  f7.2,' |', 2(f7.1,' |'),  f7.3,' |',
     .            7x ,' |' ,  i5,'   |'                          ))
c #E0       write(noUNIT,5006)
 5006       format(' -----+--------+--------+--------+--------+',
     .  '--------+--------+--------+--------+--------+--------+')
c #E0       write(noUNIT,5007) TBr_sv(ikl),
c #E0.                         TvegSV(ikl),rrCaSV(ikl)*1.e3,
c #E0.                         EvT_sv(ikl)*86.4e3
 5007       format(' Brgh |',4(8x,'|'),  f7.2,' | [micm] |',4(8x,'|'),
     .           /,' VEGE |',4(8x,'|'),2(f7.2,' |'),        2(8x,'|'),
     .                                   f7.3,' |',           8x,'|' )
c #E0       write(noUNIT,5014)
 5014       format(' -----+--------+--------+--------+--------+',
     .  '--------+--------+--------+--------+--------+--------+',
     .           /,'    n |        |     dz |        |    eta |',
     .  '     T  |        |        |        | Root W.| W.Flow |',
     .           /,'      |        |    [m] |        | [m3/m3]|',
     .  '    [K] |        |        |        | [mm/d] | [mm/h] |',
     .           /,' -----+--------+--------+--------+--------+',
     .  '--------+--------+--------+--------+--------+--------+')
 
c #E0       write(noUNIT,5008)
c #E0.                    (isl,    LSdzsv(ikl)*dz_dSV(    isl),
c #E0.                                         eta_SV(ikl,isl),
c #E0.                                         TsisSV(ikl,isl),
c #E0.                                  86.4e3*Rootsv(ikl,isl),
c #E0.                                   3.6e3*Khydsv(ikl,isl),
c #E0.                     isl=0,-nsol,-1)
 5008       format((i5,' |',   7x ,' |' ,  f7.3,' |' ,   7x ,' |',
     .           f7.3,' |' ,  f7.2,' |', 2( 7x ,' |'),   7x ,' |',
     .                                     f7.3,' |' ,  f7.2,' |'))
c #E0       write(noUNIT,5006)
c #E0       write(noUNIT,5009) RnofSV(ikl)* 3.6e3
 5009       format('      |',9(8x,'|'),f7.3,' |')
c #E0       write(noUNIT,5006)
c #E0   END IF
c #E0   END DO
 
C +..END  .main.
      return
      end
