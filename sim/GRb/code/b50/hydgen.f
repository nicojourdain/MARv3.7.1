 
 
      subroutine HYDgen
 
C +------------------------------------------------------------------------+
C | MAR HYDROLOGIC CYCLE                               Fri 23-10-2009  MAR |
C |   SubRoutine HYDgen contains .main. of the EXPLICIT HYDROLOGICAL CYCLE |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT / OUTPUT: qvDY(mx,my,mz) : air   specific humidity     (kg/kg) |
C |   ^^^^^^^^^^^^^^^ qwHY(mx,my,mz) : cloud drops                 (kg/kg) |
C |                   qrHY(mx,my,mz) : rain  drops                 (kg/kg) |
C |                   qiHY(mx,my,mz) : ice   crystals concentration(kg/kg) |
C |                   qsHY(mx,my,mz) : snow  flakes                (kg/kg) |
C |                 rainHY(mx,my)    : rain  Precipitation             (m) |
C |                 snowHY(mx,my)    : snow  Precipitation         (m w.e) |
C |                 crysHY(mx,my)    : ice   Precipitation         (m w.e) |
C |                 hlatHY(mx,my,mz) : Latent Heat Release           (K/s) |
C |                                                                        |
C |   OUTPUT:                                                              |
C |   ^^^^^^^                                                              |
C |                                                                        |
C |   REFER. : 1) Ntezimana, unpubl.thes.LLN, 115 pp,     1993             |
C |   ^^^^^^^^ 2) Lin et al.       JCAM   22, 1065--1092, 1983             |
C |               (very similar, except that graupels are represented)     |
C |            3) Emde and Kahlig, An.Geo. 7,  405-- 414, 1989             |
C |                                                                        |
C | # OPTIONS: #WH  Additional Output (Each Process  is detailed)          |
C | # ^^^^^^^^ #EW  Additional Output (Energy and Water Conservation)      |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
 
      include 'MAR_DY.inc'
      include 'MAR_HY.inc'
      include 'MAR_CA.inc'
 
      include 'MAR_2D.inc'
 
      include 'MAR_TE.inc'
      include 'MAR_TU.inc'
      include 'MAR_SL.inc'
 
      include 'MAR_WK.inc'
 
      include 'MAR_IO.inc'
      include 'MAR_SV.inc'
      include 'MAR_TV.inc'
c #EW include 'MAR_EW.inc'
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! #wx integer                iSV_v1,jSV_v1,nSV_v1,kSV_v1,lSV_v1
! #wx common  /SISVAT_EV/    iSV_v1,jSV_v1,nSV_v1,kSV_v1,lSV_v1
 
      character*3 vecthy
      integer  klhyOK,io    ,io___1,io___5,iklon,itPhys
      integer  il_mmc,il__mm,i___mm,j___mm,il_mez
c #wH integer         i_fvv(klon),j_fvv(klon),klfvv,i0fvv,j0fvv,k0fvv
c #wH common/DebuggHy/i_fvv      ,j_fvv      ,klfvv,i0fvv,j0fvv,k0fvv
 
      real     qs99  ,hrelmx,erosmx,facLHR
      real     uq1   ,uq2   ,vq1   ,vq2   ,sq1   ,sq2
      real     uInFlw,uOutFl,pp    ,pkt0  ,ta_MAR
      real     vInFlw,vOutFl
 
C +--DATA
C +  ====
 
      data i___mm/0/
      data j___mm/0/
      data qs99  /0.99e+0/
 
c #wH i0fvv = 26                     ! i ccordinate (detailled output)
c #wH j0fvv = 17                     ! j ccordinate (detailled output)
c #wH k0fvv = 24                     ! k ccordinate (detailled output)
 
 
C +--Set UP Verification
C +  ===================
 
c #wH write(6,6020) itexpe,jdarGE,mmarGE,iyrrGE,jhurGE,minuGE,jsecGE
 6020 format(/,'Clouds Microphysics',2i6,'-',i2,'-',i4,i6,'h',i2,':',i2)
 
      IF (itexpe.eq.0)                                            THEN
 
                  klhyOK = mx2*my2
                  klhyOK =       1
            IF   (klon.ne. klhyOK)                                THEN
              IF (klon.gt.1)                                      THEN
                  vecthy = 'NON'
              ELSE
                  vecthy = '   '
              END IF
                write(6,6000) klon,klhyOK,vecthy
 6000           format(/,'++++++++ klon (MARdim.inc) =',i6,' .NE.',i6,
     .                       ' ++++++++++++++',
     .                 /,'++++++++ NOT adapted to a ',a3,' vectorized ',
     .                   'code ++++++++++++++',
     .                 /,'++++++++ BAD SET UP of #hy or klon parameter',
     .                   '  ++++++++++++++',
     .                 /,'     ==> !?%@&* Emergency EXIT in HYDgen'    )
                STOP
            END IF
 
 
C +--Cloud Microphysics Initialization
C +  =================================
 
         DO j=1,my
         DO i=1,mx
           rai0HY(i,j)   = 0.
           rainHY(i,j)   = 0.
           sno0HY(i,j)   = 0.
           sfa0HY(i,j)   = 0.
           snowHY(i,j)   = 0.
           crysHY(i,j)   = 0.
         END DO
         END DO
         DO k=1,mz
         DO j=1,my
         DO i=1,mx
           ccniHY(i,j,k) = 0.
             qiHY(i,j,k) = 0.
             qsHY(i,j,k) = 0.
             qwHY(i,j,k) = 0.
             qrHY(i,j,k) = 0.
         END DO
         END DO
         END DO
 
      END IF
 
cXF (not cumulated precip because in real*4) !!!!
      IF (iterun.eq.0)                                            THEN
         DO j=1,my
         DO i=1,mx
           rai0HY(i,j)   = 0.
           rainHY(i,j)   = 0.
           rainCA(i,j)   = 0.
           sno0HY(i,j)   = 0.
           sfa0HY(i,j)   = 0.
           snowHY(i,j)   = 0.
           snowCA(i,j)   = 0.
           crysHY(i,j)   = 0.
           runoTV(i,j)   = 0.
           draiTV(i,j)   = 0.
           evapTV(i,j)   = 0.
         END DO
         END DO
      END IF
cXF
C +--Cloud Microphysics OFF ==> Reset of the Air Relative Humidity
C +  =============================================================
 
      IF (jhaRUN.lt.tim_HY)                                       THEN
C +...Hydrological cycle is inhibited until jhaRUN = tim_HY
 
        DO k=1,mz
        DO j=1,my
        DO i=1,mx
          tairDY(i,j,k)=pktaDY(i,j,k) *pkDY(i,j,k)
        END DO
        END DO
        END DO
 
C +     ***********
        call qsat3D
C +     ***********
 
        DO k=1,mz
        DO j=1,my
        DO i=1,mx
          qvDY(i,j,k)=qvsiDY(i,j,k)*min(qs99,qvDY(i,j,k)/qvsiDY(i,j,k))
        END DO
        END DO
        END DO
 
        turnHY = .true.
 
      END IF
 
 
C +--Decide to set ON Cloud Microphysics  if Air Relative Humidity > Crit.
C +  =====================================================================
 
      IF (.not.turnHY)                                            THEN
c #BS      erosmx=-1.0
 
C +        ***********
           call qsat3D
C +        ***********
 
        hrelmx= 0.0
        DO j=1,my
        DO i=1,mx
        DO k=1,mz
           hrelmx=max(hrelmx,qvDY(i,j,k)/qvsiDY(i,j,k))
           hrelmx=max(hrelmx,qvDY(i,j,k)/qvswDY(i,j,k))
 
c #BS      erosmx=max(erosmx,SLuusl(i,j,1)-SaltSL(i,j))
        END DO
        END DO
        END DO
 
           if        (hrelmx.gt.rhcrHY)
     .     turnHY = .true.
 
c #BS      if        (erosmx.gt.0.0)
c #BS.     turnHY = .true.
 
      END IF
 
 
C +--Hydrological Cycle Initialization
C +  =================================
 
      IF  (turnHY)                                                THEN
 
 
C +----From 3D to 2D arrays
C +    --------------------
 
        DO io=1,5
             ioutIO(io)=igrdIO(io)+mx*(jgrdIO(io)-1)
        END DO
c #WH   IF (il__mm.eq.0)                                            THEN
c #WH       i___mm = imez
c #WH       j___mm = jmez
c #WH       il__mm = imez + (jmez-1)*mx2
c #WH   END IF
c #EW       il_mez = imez + (jmez-1)*mx2
C +
             iklon  =         0
 
 
!$OMP PARALLEL DO
!$OMP.private(i,j,k,iklon,
!$OMP.        io___1,io___5,il__mm,i___mm,j___mm,il_mez,
!$OMP.     ccni2D,ccnw2D,cfra2D,crys2D,
!$OMP.      dqi2D, dqw2D,ect_2D,enr01D,
!$OMP.     enr11D,enr21D,gplv2D,
!$OMP.     gpmi2D,hlat2D,jhlr2D,mphy2D,
!$OMP.       pk2D,pkta2D,prec2D, pst2D,
!$OMP.     pst2Dn,  qg2D,  qi2D,  qr2D,
!$OMP.       qs2D,  qv2D,qvsi2D,qvsw2D,
!$OMP.       qw2D,rain2D,rolv2D,snoh2D,
!$OMP.     snow2D,tair2D,tsrf2D,TUkv2D,
!$OMP.     uair2D,vair2D,wair2D,wat01D,
!$OMP.     wat11D,wat21D,watf1D)
!$OMP.schedule(dynamic)
        DO   j = jp11,my1
        DO   i = ip11,mx1
              WKxy1(i,j)    =     0.
              WKxy2(i,j)    =     0.
              WKxy3(i,j)    =     0.
              WKxy4(i,j)    =     0.
              WKxy5(i,j)    =     0.
              WKxy6(i,j)    =     0.
c        END DO
c        END DO
 
c        DO   j = jp11,my1
c        DO   i = ip11,mx1
             iklon  = iklon + 1
             iklon  =         1
 
c #wH         i_fvv(iklon)  =       i
c #wH         j_fvv(iklon)  =         j
 
          DO k = 1,klev
               pk2D(iklon,k)=  pkDY(i,j,k)
             pkta2D(iklon,k)=pktaDY(i,j,k)
             tair2D(iklon,k)=tairDY(i,j,k)
             uair2D(iklon,k)=uairDY(i,j,k)
             vair2D(iklon,k)=vairDY(i,j,k)
             wair2D(iklon,k)=wairDY(i,j,k)*.01+sqrt(2.*ect_TE(i,j,k)/3.)
             rolv2D(iklon,k)=rolvDY(i,j,k)
! #kk          qv2D(iklon,k)=
! #kk.                     max(qvDY(i,j,k),epsq)
               qv2D(iklon,k)=
     .                     max(qvDY(i,j,k),epsi)
               qw2D(iklon,k)=  qwHY(i,j,k)
               qr2D(iklon,k)=  qrHY(i,j,k)
               qi2D(iklon,k)=  qiHY(i,j,k)
               qs2D(iklon,k)=
     .                     max(qsHY(i,j,k),zero)
! #qg          qg2D(iklon,k)=  qgHY(i,j,k)
             cfra2D(iklon,k)=cfraHY(i,j,k)
             ccnw2D(iklon,k)=ccnwHY(i,j,k)
             ccni2D(iklon,k)=ccniHY(i,j,k)
              dqi2D(iklon,k)=0.
              dqw2D(iklon,k)=0.
             hlat2D(iklon,k)=hlatHY(i,j,k)
             ect_2D(iklon,k)=ect_TE(i,j,k)
             TUkv2D(iklon,k)=TUkvh (i,j,k)
          END DO
          DO k = 1,klev+1
             gplv2D(iklon,k)=gplvDY(i,j,k)
             gpmi2D(iklon,k)=gpmiDY(i,j,k)
             qvsw2D(iklon,k)=qvswDY(i,j,k)
             qvsi2D(iklon,k)=qvsiDY(i,j,k)
          END DO
 
             pst2D (iklon)  =pstDY (i,j)
             pst2Dn(iklon)  =pstDYn(i,j)
             rain2D(iklon)  =rainHY(i,j)
             snow2D(iklon)  =snowHY(i,j)
             crys2D(iklon)  =crysHY(i,j)
             prec2D(iklon)  =precSL(i,j)
             snoh2D(iklon)  =snohSL(i,j)
             tsrf2D(iklon)  =TairSL(i,j)
c #EW        wat01D(iklon)  =wat0EW(i,j)
c #EW        wat11D(iklon)  =wat1EW(i,j)
c #EW        wat21D(iklon)  =wat2EW(i,j)
c #EW        watf1D(iklon)  =watfEW(i,j)
c #EW        enr01D(iklon)  =enr0EW(i,j)
c #EW        enr11D(iklon)  =enr1EW(i,j)
c #EW        enr21D(iklon)  =enr2EW(i,j)
c #EW        mphy2D(iklon)  =mphyEW(i,j)
             jhlr2D(iklon)  =jhlrGE(i,j)
 
 
C +----Call Cloud Microphysics, in case of NO vectorization
C +    ----------------------------------------------------
 
                   io___1 =  0
                   io___5 =  0
             DO    io  =  1,5
               IF (ioutIO(io).eq.iklon)                           THEN
                   io___1 =  io
                   io___5 =  io
               END IF
             END DO
               IF (i.eq.i___mm .and.j.eq.j___mm )                 THEN
                   il__mm = 1
               ELSE
                   il__mm = 0
               END IF
               IF (i.eq.imez   .and.j.eq.jmez)                    THEN
                   il_mez= 1
               ELSE
                   il_mez= 0
               END IF
 
           DO      itPhys=1,max(1,ntHyd)
C +        ***********
           call HYDmic(io___1,io___5,il__mm,i___mm,j___mm,il_mez,
     .     ccni2D,ccnw2D,cfra2D,crys2D,
     .      dqi2D, dqw2D,ect_2D,enr01D,
     .     enr11D,enr21D,gplv2D,
     .     gpmi2D,hlat2D,jhlr2D,mphy2D,
     .       pk2D,pkta2D,prec2D, pst2D,
     .     pst2Dn,  qg2D,  qi2D,  qr2D,
     .       qs2D,  qv2D,qvsi2D,qvsw2D,
     .       qw2D,rain2D,rolv2D,snoh2D,
     .     snow2D,tair2D,tsrf2D,TUkv2D,
     .     uair2D,vair2D,wair2D,wat01D,
     .     wat11D,wat21D,watf1D)
C +        ***********
           ENDDO
 
C +----From 2D to 3D arrays,    in case of NO vectorization
C +    ----------------------------------------------------
 
          DO k=1,klev
             pktaDY(i,j,k)=pkta2D(iklon,k)
             tairDY(i,j,k)=tair2D(iklon,k)
             rolvDY(i,j,k)=rolv2D(iklon,k)
             qvsiDY(i,j,k)=qvsi2D(iklon,k)
             qvswDY(i,j,k)=qvsw2D(iklon,k)
               qvDY(i,j,k)=  qv2D(iklon,k)
               qwHY(i,j,k)=  qw2D(iklon,k)
               qrHY(i,j,k)=  qr2D(iklon,k)
               qiHY(i,j,k)=  qi2D(iklon,k)
               qsHY(i,j,k)=  qs2D(iklon,k)
! #qg          qgHY(i,j,k)=  qg2D(iklon,k)
             cfraHY(i,j,k)=cfra2D(iklon,k)
             ccnwHY(i,j,k)=ccnw2D(iklon,k)
             ccniHY(i,j,k)=ccni2D(iklon,k)
              dqiHY(i,j,k)= dqi2D(iklon,k)*dsigm1(k)*pstDYn(i,j)
              dqwHY(i,j,k)= dqw2D(iklon,k)*dsigm1(k)*pstDYn(i,j)
             hlatHY(i,j,k)=hlat2D(iklon,k)
          END DO
 
             rainHY(i,j)  =rain2D(iklon)
             snowHY(i,j)  =snow2D(iklon)
             crysHY(i,j)  =crys2D(iklon)
             precSL(i,j)  =prec2D(iklon)
             snohSL(i,j)  =snoh2D(iklon)
c #ew        wat0EW(i,j)  =wat01D(iklon)
c #ew        wat1EW(i,j)  =wat11D(iklon)
c #ew        wat2EW(i,j)  =wat21D(iklon)
c #ew        watfEW(i,j)  =watf1D(iklon)
c #ew        enr0EW(i,j)  =enr01D(iklon)
c #ew        enr1EW(i,j)  =enr11D(iklon)
c #ew        enr2EW(i,j)  =enr21D(iklon)
c #ew        mphyEW(i,j)  =mphy2D(iklon)
        END DO
        END DO
!$OMP END PARALLEL DO
 
C +----Call Cloud Microphysics, in case of    vectorization
C +    ----------------------------------------------------
 
        IF (klon.gt.1)                                            THEN
 
            io___1  =  1
            io___5  =  5
 
C +        ***********
           call HYDmic(io___1,io___5,il__mm,i___mm,j___mm,il_mez)
C +        ***********
 
c #WH         il_mmc = il__mm
c #WH         j___mm =      0
 1000     CONTINUE
c #WH         il_mmc = il_mmc-(my2-jp11+1)
c #WH         j___mm = j___mm  + 1
c #WH     IF (il_mmc.gt.0)                                  GO TO 1000
c #WH         i___mm = il_mmc+(my2-jp11+1) + ip11
 
 
C +----From 2D to 3D arrays,    in case of    vectorization
C +    ----------------------------------------------------
 
             iklon  =         0
          DO j = jp11,my1
          DO i = ip11,mx1
             iklon  = iklon + 1
 
            DO k=1,klev
             pktaDY(i,j,k)=pkta2D(iklon,k)
             tairDY(i,j,k)=tair2D(iklon,k)
             rolvDY(i,j,k)=rolv2D(iklon,k)
             qvsiDY(i,j,k)=qvsi2D(iklon,k)
             qvswDY(i,j,k)=qvsw2D(iklon,k)
               qvDY(i,j,k)=  qv2D(iklon,k)
               qwHY(i,j,k)=  qw2D(iklon,k)
               qrHY(i,j,k)=  qr2D(iklon,k)
               qiHY(i,j,k)=  qi2D(iklon,k)
               qsHY(i,j,k)=  qs2D(iklon,k)
! #qg          qgHY(i,j,k)=  qg2D(iklon,k)
             cfraHY(i,j,k)=cfra2D(iklon,k)
             ccnwHY(i,j,k)=ccnw2D(iklon,k)
             ccniHY(i,j,k)=ccni2D(iklon,k)
              dqiHY(i,j,k)= dqi2D(iklon,k)*dsigm1(k)*pstDYn(i,j)
              dqwHY(i,j,k)= dqw2D(iklon,k)*dsigm1(k)*pstDYn(i,j)
             hlatHY(i,j,k)=hlat2D(iklon,k)
            END DO
 
             rainHY(i,j)  =rain2D(iklon)
             snowHY(i,j)  =snow2D(iklon)
             crysHY(i,j)  =crys2D(iklon)
             precSL(i,j)  =prec2D(iklon)
             snohSL(i,j)  =snoh2D(iklon)
c #EW        wat0EW(i,j)  =wat01D(iklon)
c #EW        wat1EW(i,j)  =wat11D(iklon)
c #EW        wat2EW(i,j)  =wat21D(iklon)
c #EW        watfEW(i,j)  =watf1D(iklon)
c #EW        enr0EW(i,j)  =enr01D(iklon)
c #EW        enr1EW(i,j)  =enr11D(iklon)
c #EW        enr2EW(i,j)  =enr21D(iklon)
c #EW        mphyEW(i,j)  =mphy2D(iklon)
          END DO
          END DO
 
        END IF
 
 
C +--Isotopes Proxies: Diagnostics
C +  =============================
 
              icntHY      = icntHY     + 1
!$OMP PARALLEL DO private(i,j,k,facLHR)
        DO   j = jp11,my1
        DO   i = ip11,mx1
            DO k=2,klev
              WKxy1(i,j)  =               WKxy1(i,j)
     .                    +dsigm1(k)*max(hlatHY(i,j,k),0.)
              WKxy2(i,j)  =               WKxy2(i,j)
     .                    -dsigm1(k)*min(hlatHY(i,j,k),0.)
              WKxy3(i,j)  =               WKxy3(i,j)
     .                    +dsigm1(k)*max(hlatHY(i,j,k),0.)*tairDY(i,j,k)
              WKxy4(i,j)  =               WKxy4(i,j)
     .                    -dsigm1(k)*min(hlatHY(i,j,k),0.)*tairDY(i,j,k)
              WKxy5(i,j)  =               WKxy5(i,j)
     .                    +dsigm1(k)*max(hlatHY(i,j,k),0.)*gplvDY(i,j,k)
              WKxy6(i,j)  =               WKxy6(i,j)
     .                    -dsigm1(k)*min(hlatHY(i,j,k),0.)*gplvDY(i,j,k)
            END DO
              facLHR      =(cp /Ls_H2O)*pstDYn(i,j)  *1.e3*grvinv*dt
             Hcd_HY(i,j)  =Hcd_HY(i,j)  + WKxy1(i,j)      * facLHR
             Hsb_HY(i,j)  =Hsb_HY(i,j)  + WKxy2(i,j)      * facLHR
             Tcd_HY(i,j)  =Tcd_HY(i,j)  + WKxy3(i,j)      * facLHR
             Tsb_HY(i,j)  =Tsb_HY(i,j)  + WKxy4(i,j)      * facLHR
             zcd_HY(i,j)  =zcd_HY(i,j)  + WKxy5(i,j)      * facLHR
             zsb_HY(i,j)  =zsb_HY(i,j)  + WKxy6(i,j)      * facLHR
              WKxy1(i,j)  =     0.
              WKxy2(i,j)  =     0.
              WKxy3(i,j)  =     0.
              WKxy4(i,j)  =     0.
              WKxy5(i,j)  =     0.
              WKxy6(i,j)  =     0.
        END DO
        END DO
!$OMP END PARALLEL DO
 
      END IF
 
 
C +--Hydrological Cycle Lateral Boundary Conditions
C +  ==============================================
 
      IF  (mmx.gt.1)                                              THEN
        DO k=mzhyd,mz
        DO j=1,my
            WKxyz1( 1,j,k) =   max(0.,sign(1.,uairDY( 1,j,k)))  ! u_In_Flow
            WKxyz2( 1,j,k) =               1.-WKxyz1( 1,j,k)    ! u_OutFlow
            WKxyz3( 1,j,k) = (sigma(k)*pstDY( 1,j)+ptopDY)**cap ! pp
            WKxyz4( 1,j,k) =                  pktaDY( 1,j,k)
 
            WKxyz1(mx,j,k) =   max(0.,sign(1.,uairDY(mx,j,k)))
            WKxyz2(mx,j,k) =               1.-WKxyz1(mx,j,k)
            WKxyz3(mx,j,k) = (sigma(k)*pstDY(mx,j)+ptopDY)**cap
            WKxyz4(mx,j,k) =                  pktaDY(mx,j,k)
        ENDDO
        ENDDO
 
        DO k=mzhyd,mz
        DO j=1,my
              qwHY( 1,j,k) =    qwHY( 1,j,k) *WKxyz2( 1,j,k)
              qiHY( 1,j,k) =    qiHY( 1,j,k) *WKxyz2( 1,j,k)
              qrHY( 1,j,k) =    qrHY( 1,j,k) *WKxyz2( 1,j,k)
              qsHY( 1,j,k) =    qsHY( 1,j,k) *WKxyz2( 1,j,k)
            WKxyz5( 1,j,k) =  tairDY( 1,j,k) *WKxyz2( 1,j,k)
 
              qwHY(mx,j,k) =    qwHY(mx,j,k) *WKxyz1(mx,j,k)
              qiHY(mx,j,k) =    qiHY(mx,j,k) *WKxyz1(mx,j,k)
              qrHY(mx,j,k) =    qrHY(mx,j,k) *WKxyz1(mx,j,k)
              qsHY(mx,j,k) =    qsHY(mx,j,k) *WKxyz1(mx,j,k)
            WKxyz5(mx,j,k) =  tairDY(mx,j,k) *WKxyz1(mx,j,k)
        ENDDO
        ENDDO
 
        DO k=mzhyd,mz
        DO j=1,my
            pktaDY( 1,j,k) =  pktaDY( 1,j,k) *WKxyz1( 1,j,k)
     .                     +  WKxyz5( 1,j,k) /WKxyz3( 1,j,k)
 
            pktaDY(mx,j,k) =  pktaDY(mx,j,k) *WKxyz2(mx,j,k)
     .                     +  WKxyz5(mx,j,k) /WKxyz3(mx,j,k)
        ENDDO
        ENDDO
 
        DO k=mzhyd,mz
        DO j=1,my
            hlatHY( 1,j,k) =  WKxyz5( 1,j,k)
     .                  *(1.0-WKxyz4( 1,j,k) /pktaDY( 1,j,k)) /dt
 
            hlatHY(mx,j,k) =  WKxyz5(mx,j,k)
     .                  *(1.0-WKxyz4(mx,j,k) /pktaDY(mx,j,k)) /dt
        ENDDO
        ENDDO
 
        DO k=mzhyd,mz
        DO j=1,my
            WKxyz1( 1,j,k) = 0.
            WKxyz2( 1,j,k) = 0.
            WKxyz3( 1,j,k) = 0.
            WKxyz4( 1,j,k) = 0.
            WKxyz5( 1,j,k) = 0.
            WKxyz1(mx,j,k) = 0.
            WKxyz2(mx,j,k) = 0.
            WKxyz3(mx,j,k) = 0.
            WKxyz4(mx,j,k) = 0.
            WKxyz5(mx,j,k) = 0.
        ENDDO
        ENDDO
      END IF
 
      IF (mmy.gt.1)                                               THEN
        DO k=mzhyd,mz
        DO i=1,mx
            WKxyz1(i, 1,k) =   max(0.,sign(1.,vairDY(i, 1,k)))  ! v_In_Flow
            WKxyz2(i, 1,k) =               1.-WKxyz1(i, 1,k)    ! v_OutFlow
            WKxyz3(i, 1,k) = (sigma(k)*pstDY(i, 1)+ptopDY)**cap ! pp
            WKxyz4(i, 1,k) =                  pktaDY(i, 1,k)
 
            WKxyz1(i,my,k) =   max(0.,sign(1.,vairDY(i,my,k)))
            WKxyz2(i,my,k) =               1.-WKxyz1(i,my,k)
            WKxyz3(i,my,k) = (sigma(k)*pstDY(i,my)+ptopDY)**cap
            WKxyz4(i,my,k) =                  pktaDY(i,my,k)
        ENDDO
        ENDDO
 
        DO k=mzhyd,mz
        DO i=1,mx
              qwHY(i, 1,k) =    qwHY(i, 1,k) *WKxyz2(i, 1,k)
              qiHY(i, 1,k) =    qiHY(i, 1,k) *WKxyz2(i, 1,k)
              qrHY(i, 1,k) =    qrHY(i, 1,k) *WKxyz2(i, 1,k)
              qsHY(i, 1,k) =    qsHY(i, 1,k) *WKxyz2(i, 1,k)
            WKxyz5(i, 1,k) =  tairDY(i, 1,k) *WKxyz2(i, 1,k)
 
              qwHY(i,my,k) =    qwHY(i,my,k) *WKxyz1(i,my,k)
              qiHY(i,my,k) =    qiHY(i,my,k) *WKxyz1(i,my,k)
              qrHY(i,my,k) =    qrHY(i,my,k) *WKxyz1(i,my,k)
              qsHY(i,my,k) =    qsHY(i,my,k) *WKxyz1(i,my,k)
            WKxyz5(i,my,k) =  tairDY(i,my,k) *WKxyz1(i,my,k)
        ENDDO
        ENDDO
 
        DO k=mzhyd,mz
        DO i=1,mx
            pktaDY(i, 1,k) =  pktaDY(i, 1,k) *WKxyz1(i, 1,k)
     .                     +  WKxyz5(i, 1,k) /WKxyz3(i, 1,k)
 
            pktaDY(i,my,k) =  pktaDY(i,my,k) *WKxyz2(i,my,k)
     .                     +  WKxyz5(i,my,k) /WKxyz3(i,my,k)
        ENDDO
        ENDDO
 
        DO k=mzhyd,mz
        DO i=1,mx
            hlatHY(i, 1,k) =  WKxyz5(i, 1,k)
     .                  *(1.0-WKxyz4(i, 1,k) /pktaDY(i, 1,k)) /dt
 
            hlatHY(i,my,k) =  WKxyz5(i,my,k)
     .                  *(1.0-WKxyz4(i,my,k) /pktaDY(i,my,k)) /dt
        ENDDO
        ENDDO
 
        DO k=mzhyd,mz
        DO i=1,mx
            WKxyz1(i, 1,k) = 0.
            WKxyz2(i, 1,k) = 0.
            WKxyz3(i, 1,k) = 0.
            WKxyz4(i, 1,k) = 0.
            WKxyz5(i, 1,k) = 0.
            WKxyz1(i,my,k) = 0.
            WKxyz2(i,my,k) = 0.
            WKxyz3(i,my,k) = 0.
            WKxyz4(i,my,k) = 0.
            WKxyz5(i,my,k) = 0.
        ENDDO
        ENDDO
      END IF
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! #wx IF (lSV_v1.GT.0  .AND.  lSV_v1.LE.2)                          THEN
! #wx   write(6,6010)   (qsHY(iSV_v1,jSV_v1,k)*1.e3,k=mz,mz-4,-1)
 6010   format(10x,'After HYDmic   : q   [g/kg] =',5f9.6)
! #wx END IF
 
      return
      end
