 
 
      subroutine SISVAT_BSn
 
C +------------------------------------------------------------------------+
C | MAR          SISVAT_BSn                           Wed  2-Sep-2011  MAR |
C |   SubRoutine SISVAT_BSn treats Snow Erosion and Deposition             |
C |                                                                        |
C |                                                                        |
C |   Preprocessing  Option: SISVAT IO (not always a standard preprocess.) |
C |   ^^^^^^^^^^^^^^^^^^^^^  ^^^^^^^^^                                     |
C |   FILE                 |      CONTENT                                  |
C |   ~~~~~~~~~~~~~~~~~~~~~+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ |
C | # stdout               | #sb: OUTPUT of Snow Erosion                   |
C |                        |      unit  6, SubRoutine  SISVAT_BSn **ONLY** |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--General Variables
C +  =================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MAR_SV.inc'
      include 'MARdSV.inc'
 
      include 'MARxSV.inc'
      include 'MARySV.inc'
 
C +--Local Variables
C +  ===============
 
      logical           BlowIn
      common/llocal_BSn/BlowIn
 
      real              FacSBS,FacUBS        !
      real              Por_BS               ! Snow       Porosity
      real              SheaBS               !
      real              rCd10n               ! GM97:   assumed neutral stabil.
      common/rlocal_BSn/FacSBS,FacUBS,       !
     .                  Por_BS,SheaBS,rCd10n !
 
!$OMP threadprivate(/xSISVAT_I/,/xSISVAT_R/,/ySISVAT_I/,/ySISVAT_R/
!$OMP.             ,/llocal_BSn/,/rlocal_BSn/)
 
      integer  ikl   ,isn   ,isnMAX,is2      !
      integer  Mobilm,Mobiln                 !
      integer  Mobile(klonv)                 !
 
      real     DendOK                        ! Dendricity Switch
      real     SaltOK                        ! Saltation  Switch
      real     MeltOK                        ! Saltation  Switch (Melting Snow)
      real     SnowOK                        ! Pack Top   Switch
      real     SaltM1,SaltM2,SaltMo,SaltMx   ! Saltation  Parameters
      real     ShearX                        ! Arg. Max Shear Stress
      real     SaltSU,Salt_U                 !
      real     ArgFac,Fac_Mo                 !
      real     FacRBS,FacTBS                 !
      real     ArguSi                        !
      real     SaltSI(klonv,nsno)            ! Snow Drift Index
      real     hdrift                        ! Inverse erodibl.Snow Lay.Thickn.
      real     h_mmWE                        ! Eroded Snow Layer Min Thickness
      real     tfv_vk                        ! * Fall Veloc. / Von Karman Cst
      real     sdrift(klonv,nsno)            !
      real     xdrift(klonv)                 !
      real     zdrift(klonv)                 !
      real     tdepos(klonv)                 !
      real     zdepos(klonv,nsno)            !
      real     dbsaux(klonv)                 ! Drift Amount   (Dummy Variable)
      real     dzweqo,dzweqn,bsno_x          !
      real                   hsno_x          !
      real     PorSno,Salt_f,PorRef,ro_new   !
      real     MIN_Mo                        ! Minimum Mobility Fresh Fallen *
      real     AgBlow                        ! Snow Mobility    Time  Scale
      real     snofOK                        ! Threshd Snow Fall
 
      integer  isagr1(klonv)                 ! 1st     Layer History
      integer  isagr2(klonv)                 ! 2nd     Layer History
 
      real     WEagre(klonv)                 ! Snow Water Equivalent Thickness
      real     Agrege(klonv)                 ! 1. when Agregation constrained
      real     dzagr1(klonv)                 ! 1st     Layer Thickness
      real     dzagr2(klonv)                 ! 2nd     Layer Thickness
      real     T_agr1(klonv)                 ! 1st     Layer Temperature
      real     T_agr2(klonv)                 ! 2nd     Layer Temperature
      real     roagr1(klonv)                 ! 1st     Layer Density
      real     roagr2(klonv)                 ! 2nd     Layer Density
      real     etagr1(klonv)                 ! 1st     Layer Water Content
      real     etagr2(klonv)                 ! 2nd     Layer Water Content
      real     G1agr1(klonv)                 ! 1st     Layer Dendricity/Spher.
      real     G1agr2(klonv)                 ! 2nd     Layer Dendricity/Spher.
      real     G2agr1(klonv)                 ! 1st     Layer Sphericity/Size
      real     G2agr2(klonv)                 ! 2nd     Layer Sphericity/Size
      real     agagr1(klonv)                 ! 1st     Layer Age
      real     agagr2(klonv)                 ! 2nd     Layer Age
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! OUTPUT           for Snow Erosion  Variables
! #sb real     Sno0WE,Sno1WE                 ! Snow Mass before/after Erosion
! #sb real     SnodWE                        ! Snow Mass              Erosion
 
 
C +--DATA
C +  ====
 
      data     AgBlow  / 1.00     /          ! 1 Day (F.Domine, pers.communic.)
      data     SaltMx  /-5.83e-2  /          !
      data     FacRBS  / 2.868    /          !
      data     FacTBS  / 0.085    /          !
      data     hdrift  / 1.00e+1  /          ! Inverse erodibl.Snow Lay.Thickn.
      data     h_mmWE  / 0.01e00  /          ! Eroded Snow Layer Min Thickness
      data     tfv_vk  / 5.10e-1  /          ! tfv (Terminal Fall Veloc. =.216)
C +...                                       ! /vk (Von Karman Constant  =.4  )
C +                                          ! (Wamser & Lykosov,   1995
C +                                          !  Contr.Atm.Phys. 68, p.90)
 
C +--Initialization
C +  ==============
 
      IF (.NOT.BlowIn)                                             THEN
               BlowIn = .true.
               FacSBS =  1.             / FacRBS
               FacUBS =  1.             / FacTBS
               Por_BS =  1.             - blsno /      ro_Ice
               SheaBS =                   Por_BS/(unun-Por_BS)
C +...         SheaBS =  Arg(sqrt(shear = max shear stress in snow)):
C +            shear  =  3.420d00 * exp(-(Por_BS      +Por_BS)
C +  .                                  /(unun        -Por_BS))
C +            SheaBS :  see de Montmollin         (1978),
C +                      These Univ. Sci. Medic. Grenoble, Fig. 1 p. 124
 
                                        ! Parameterization of u*th
               rCd10n      =  1./  26.5 ! was developed from observations made
                                        ! during assumed neutral conditions
 
!$omp MASTER
               write(6,5000)  1./  rCd10n
 5000          format(/,' Blowing Snow Model  Initialization     ',
     .                /,' Vt / u*t =',f8.2,' (Neutral Assumption)',
     .                /,'           ', 8x ,' (Budd assumes  26.5)',/)
!$omp END MASTER
      END IF
 
 
C +--Snow Age (Influence on Snow Erosion Threshold)
C +  ==============================================
 
c #BS DO isn=1,nsno
c #BS DO ikl=1,klonv
c #BS   agsnSV(ikl,isn) = agsnSV(ikl,isn) + dt__SV/86400.
c #BS END DO
c #BS END DO
c #BS DO ikl=1,klonv
c #BS   isn    = max(1 ,        isnoSV(ikl))
c #BS   snofOK = max(0.,sign(1.,dsn_SV(ikl)-epsi))      !  Threshold=1.e-6
c #BS   agsnSV(ikl,isn) =   (1.-snofOK) *agsnSV(ikl,isn)! ~0.1 mm w.e./day
c #BS END DO
      IF (.NOT.BloMod)                                     GO TO 1000
c #AG STOP '?!&~@|@[#@#] --- INCONSISTANT SNOW AGE --- EMERGENCY STOP'
 1000 CONTINUE
 
 
C +--EROSION
C +  =======
 
      DO isn = 1, nsno
      DO ikl = 1,klonv
 
C +--Below the high Snow Density Threshold  (ro__SV < blsno)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        DendOK   =  max(zero,sign(unun,epsi-G1snSV(ikl,isn)  ))  !
        SaltOK   =  min(1   , max(istdSV(2)-istoSV(ikl,isn),0))  !
        MeltOK   =     (unun                                     !
     .             -max(zero,sign(unun,TfSnow-epsi               !
     .                                     -TsisSV(ikl,isn)  ))) ! Melting Snow
     .           *  min(unun,DendOK                              !
     .                  +(1.-DendOK)                             !
     .                      *sign(unun,     G2snSV(ikl,isn)-1.0))! 1.0 for 1mm
        SnowOK   =  min(1   , max(isnoSV(ikl)      +1 -isn ,0))  ! Snow Switch
 
        G1snSV(ikl,isn) =      SnowOK *    G1snSV(ikl,isn)
     .                  + (1.- SnowOK)*min(G1snSV(ikl,isn),G1_dSV)
        G2snSV(ikl,isn) =      SnowOK *    G2snSV(ikl,isn)
     .                  + (1.- SnowOK)*min(G2snSV(ikl,isn),G1_dSV)
 
        SaltOK   =  min(unun , SaltOK +    MeltOK)       * SnowOK
        SaltM1   = -0.750e-2 * G1snSV(ikl,isn)
     .             -0.500e-2 * G2snSV(ikl,isn)+ 0.500e00
C +...  SaltM1   :  Guyomarc'h & Merindol, 1997, Ann. Glac.
C +       CAUTION:  Guyomarc'h & Merindol Dendricity Sign is +
C +       ^^^^^^^^                    MAR Dendricity Sign is -
        SaltM2   = -0.833d-2 * G1snSV(ikl,isn)
     .             -0.583d-2 * G2snSV(ikl,isn)+ 0.833d00
        SaltMo   = (DendOK   * SaltM1 + (1.-DendOK) *     SaltM2       )
 
C +--Increased Mobility of Deposed (blown) Snow (Mann et al., 2000, JGR 105,
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  Fig.2 p.24496 & text below)
        MIN_Mo   =  0.
c #MA   MIN_Mo   =  0.6 * exp(-agsnSV(ikl,isn)                  /AgBlow)
        SaltMo   =                                    max(SaltMo,MIN_Mo)
 
        SaltMo   =  SaltOK   * SaltMo + (1.-SaltOK) * min(SaltMo,SaltMx)
c #TUNE SaltMo   =  SaltOK   * SaltMo - (1.-SaltOK) *     0.9500
        SaltMo   =         max(SaltMo ,  epsi-unun)
 
        SaltSU   =     (1.00d0+SaltMo)     *FacSBS
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! OUTPUT           for Snow Erosion  Variables
! #sb   Salt_U   =        -log(SaltSU)     *FacUBS
! #sb   IF (ii__SV(ikl).EQ.iwr_SV.AND.jj__SV(ikl).EQ.jwr_SV        .AND.
! #sb.      nn__SV(ikl).EQ.nwr_SV.AND.isn        .EQ.isnoSV(ikl))
! #sb.    write(6,6010) itexpe
! #sb.                 ,isnoSV(ikl),G1snSV(ikl,isn)/G1_dSV
! #sb.                             ,G2snSV(ikl,isn)/G1_dSV
! #sb.                             ,ro__SV(ikl,isn),agsnSV(ikl,isn)
! #sb.                             ,SaltM1, SaltM2, SaltMo, Salt_U
! #sb.                             ,us__SV(ikl)   / rCd10n
 6010     format(/,'SISVAT_BSn',i6
     .           ,6x,i3,2x,'G1         =',f6.3,'   G2         =',f7.3
     .           ,      '   ro [kg/m3] =',f9.3,'   Age* [Day] =',f9.3
     .           ,   /,27x,'SaltM1     =',f6.3,'   SaltM2     =',f7.3
     .           ,      '   Mobility I.=',f9.3,'   Vt   [m/s] =',f9.3
     .           ,   /,27x,'            ', 6x ,'               ', 7x
     .           ,      '               ', 9x ,'   Vn10 [m/s] =',f9.3)
 
C +--Above the high Snow Density Threshold  (ro__SV > blsno)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        Por_BS      =  1.000       - ro__SV(ikl,isn)     /ro_Ice
        ShearX      =                Por_BS/max(epsi,unun-Por_BS)
C +...  ShearX ==> Arg(sqrt(shear)) with shear = max shear stress in snow:
C +     shear       =  3.420d00 * exp(-(Por_BS      +Por_BS)
C +  .                                /max(epsi,unun-Por_BS))
C +                    see de Montmollin         (1978),
C +                    These Univ. Sci. Medic. Grenoble, Fig. 1 p. 124
 
C +--Influence of Density on Shear Stress if ro__SV > blsno
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ArgFac      =  max(zero  ,SheaBS-ShearX)     !
!       Fac_Mo      =  exp(       ArgFac       )     ! ** NOT ** tuned
        Fac_Mo   =     exp(       ArgFac       )     ! = 1 if ro__SV < blsno
                                                     ! < 1 if ro__SV > blsno
C +--Snow Drift Index
C +  ~~~~~~~~~~~~~~~~
        SaltSU      =  max(epsi  ,    SaltSU)
        SaltSU      =  exp(Fac_Mo*log(SaltSU))
        ArguSi      =     -FacTBS              *us__SV(ikl)/rCd10n
        SaltSI(ikl,isn) = (SaltSU-exp(ArguSi)) *FacRBS
C +...  SaltSI          :  Generalization of the Snow Drift Index of
C +                        Guyomarc'h & Merindol (1997, Ann.Glaciol.)
 
C +--Threshold Friction Velocity
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~
        SnowOK   =  1 -min(1,iabs(isn-isnoSV(ikl)))
        Salt_U   =               -log(SaltSU)  *FacUBS
C +...  Salt_U   :  Guyomarc'h & Merindol, 1997, Ann. Glac.
 
        usthSV(ikl) =     SnowOK *   (Salt_U   *rCd10n)
     .              + (1.-SnowOK)*    usthSV(ikl)
 
c #BA   usthSV(ikl) =     SnowOK *   (Salt_U   /26.5)
c #BA.              + (1.-SnowOK)*    usthSV(ikl)
C +...  Us(U10)     :  Budd et al.            1966, Ant.Res.Ser.9
C +               (see Pomeroy & Gray 1995 NHRI Sci.Rep.7(30)p.62)
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! OUTPUT           for Snow Erosion  Variables
! #sb   IF (ii__SV(ikl).EQ.iwr_SV.AND.jj__SV(ikl).EQ.jwr_SV        .AND.
! #sb.      nn__SV(ikl).EQ.nwr_SV.AND.isn        .EQ.isnoSV(ikl))
! #sb.    write(6,6011)     Fac_Mo,Por_BS,SaltSI(ikl,isn),usthSV(ikl)
 6011     format(      27x,'Fac_Mo     =',f6.3,'   Por_BS     =',f7.3
     .           ,      '   Drift    I.=',f9.3,'   ut*_0[m/s] =',f9.3)
      END DO
      END DO
 
 
C +--Deepest Mobile Snow Layer
C +  -------------------------
 
      DO ikl = 1,klonv
        Mobile(ikl) = nsno+1
      END DO
      DO isn =   nsno ,1,-1
      DO ikl = 1,klonv
        isnMAX      =       max(   1,          isnoSV(ikl)             )
        isnMAX      =       min( isn,          isnMAX                  )
        Mobiln      = isn * max(zero,sign(unun,SaltSI(ikl     ,isnMAX)))
        Mobilm      =   1 - min(1   ,          Mobile(ikl) -1 -Mobiln)
C +...  Mobilm      =   1   ONLY IF   Mobiln = Mobile(ikl) -1 (0 otherwise)
 
        Mobile(ikl) =                 Mobilm * Mobiln
     .              +              (1-Mobilm)* Mobile(ikl)
      END DO
      END DO
 
 
C +--Weighting the Amount of Snow to erode
C +  -------------------------------------
 
      DO ikl = 1,klonv
        zdrift(ikl) = 0.0
        xdrift(ikl) = 0.0
        dbsaux(ikl) = dbs_SV(ikl)
      END DO
 
      DO isn = 1, nsno
      DO ikl = 1,klonv
        zdrift(ikl)     =  zdrift(ikl)
     .            + 0.50 * dzsnSV(ikl,isn) * (3.25  -SaltSI(ikl,isn))
        sdrift(ikl,isn) =  SaltSI(ikl,isn)
     .          *exp(  max(argmin, -zdrift(ikl)     *hdrift     ))
     .          *min(1,max(0     ,  isn +1          -Mobile(ikl)))
     .          *min(1,max(0     ,  isnoSV(ikl)     -isn +1     ))
C +...           Last 2 Lines force sdrift = 0 outside mobile Snow Layers
     .          *      max(zero, sign(unun,         -dbs_SV(ikl)))
C +...           Erosion is allowed only if available Blowing Snow
 
        if(ro__SV(ikl,isn)>700) sdrift(ikl,isn) = 0.
        if(isnoSV(ikl) < 4    ) sdrift(ikl,isn) = 0.
cXF
        xdrift(ikl)     =           sdrift(ikl,isn) +xdrift(ikl)
        zdrift(ikl)     =  zdrift(ikl)
     .            + 0.50 * dzsnSV(ikl,isn) * (3.25  -SaltSI(ikl,isn))
      END DO
      END DO
 
C +--Normalization
C +  ~~~~~~~~~~~~~
      DO isn = 1, nsno
      DO ikl = 1,klonv
        sdrift(ikl,isn) =  sdrift(ikl,isn) /max(epsi,xdrift(ikl))
      END DO
      END DO
 
 
C +--Weighting the Amount of Snow to depose
C +  --------------------------------------
 
      DO ikl = 1,klonv
        zdrift(ikl) = 0.0
        tdepos(ikl) = 0.0
      END DO
 
      DO isn = 1, nsno
      DO ikl = 1,klonv
        zdepos(ikl,isn) =      exp(-zdrift(ikl)   )
     .          *min(1,max(0     ,  isn +1          -Mobile(ikl)))
     .          *min(1,max(0     ,  isnoSV(ikl    ) -isn +1     ))
C +...           Last 2 Lines force zdepos = 0 outside mobile Snow Layers
 
        if(ro__SV(ikl,isn)>700) zdepos(ikl,isn) = 0.
        if(isnoSV(ikl) < 4    ) zdepos(ikl,isn) = 0.
cXF
        tdepos(ikl) = tdepos(ikl) + zdepos(ikl,isn)
        zdrift(ikl) = zdrift(ikl) + dzsnSV(ikl,isn) *ro__SV(ikl,isn)
     .                                              /ro_Wat
      END DO
      END DO
 
C +--Normalization
C +  ~~~~~~~~~~~~~
      DO isn = 1, nsno
      DO ikl = 1,klonv
        zdepos(ikl,isn) = zdepos(ikl,isn) / max(epsi,tdepos(ikl))
      END DO
      END DO
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! OUTPUT           for Snow Erosion  Variables
! #sb DO ikl = 1,klonv
! #sb   IF (ii__SV(ikl).EQ.iwr_SV.AND.jj__SV(ikl).EQ.jwr_SV        .AND.
! #sb.      nn__SV(ikl).EQ.nwr_SV                          )        THEN
! #sb     Sno0WE =   0.
! #sb   DO isn=1,nsno
! #sb     Sno0WE =   Sno0WE
! #sb.           +   dzsnSV(ikl,isn) *ro__SV(ikl,isn)
! #sb   END DO
! #sb     write(6,6005)   Sno0WE                    ,dbs_SV(ikl)
 6005     format(
     .      18x,'MB0',6x,'Sno1WE [mm]=',f9.3,19x,'0  dbs_SV [mm]=',f9.6)
! #sb     SnodWE =   dbs_SV(ikl)
! #sb   END IF
! #sb END DO
 
 
C +--Weighted  Erosion (Erosion amount is distributed       ! dbs_SV decreases
C +  -----------------  over the upper Snow Pack)           ! dzsnSV decreases
 
      DO isn = nsno,2,-1
      DO ikl = 1,klonv
        SnowOK      = min(1,max(isnoSV(ikl)+1-isn ,0))      ! Snow Switch
        dzweqo      = dzsnSV(ikl,isn) *ro__SV(ikl,isn)      ! [kg/m2, mm w.e.]
        bsno_x      = dbsaux(ikl)     *sdrift(ikl,isn)
        dzweqn      = dzweqo          +bsno_x
        dzweqn  = max(dzweqn,          h_mmWE *SnowOK)
        dzweqn  = min(dzweqn,          dzweqo)
cXF
        dbs_SV(ikl) = dbs_SV(ikl)    +(dzweqo -dzweqn)
        dbs_Er(ikl) = dbs_Er(ikl)    +(dzweqo -dzweqn)
        dzsnSV(ikl,isn) =              dzweqn
     .                       /max(epsi,ro__SV(ikl,isn))
        if(dzsnSV(ikl,isn)>0 .and.dzsnSV(ikl,isn)<0.0001)then
        dbs_SV(ikl) = dbs_SV(ikl)+ dzsnSV(ikl,isn)*ro__SV(ikl,isn)
        dbs_Er(ikl) = dbs_Er(ikl)+ dzsnSV(ikl,isn)*ro__SV(ikl,isn)
        dzsnSV(ikl,isn) = 0
        ro__SV(ikl,isn) = 0
        isnoSV(ikl)     = max(0,isnoSV(ikl) - 1)
        endif
 
      END DO
      END DO
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! OUTPUT           for Snow Erosion  Variables
! #sb DO ikl = 1,klonv
! #sb   IF (ii__SV(ikl).EQ. 1 .AND.  jj__SV(ikl) .EQ. 1)            THEN
! #sb     SnodWE =   SnodWE         -dbs_SV(ikl)
! #sb     Sno1WE =   0.
! #sb   DO isn=1,nsno
! #sb     Sno1WE =   Sno1WE
! #sb.           +   dzsnSV(ikl,isn)*ro__SV(ikl,isn)
! #sb   END DO
! #sb     write(6,6006)Sno1WE      , dbs_SV(ikl)
 6006     format(
     .      18x,'MB1',6x,'Sno1WE [mm]=',f9.3,19x,'1  dbs_SV [mm]=',f9.6)
! #sb     write(6,6007)Sno1WE    ,SnodWE   ,Sno0WE,
! #sb.                (Sno1WE    -SnodWE   -Sno0WE)
 6007     format(
     .      18x,'MB ',5x,'(After  [mm]=',f6.0, ')-(Erosion[mm]=', f7.3,
     .                                         ')-(Before [mm]=', f9.3,
     .                                         ')= Budget [mm]=', f9.6)
! #sb   END IF
! #sb END DO
 
 
C +--ACCUMULATION of BLOWN SNOW                             ! dsn_SV decreases
C +  --------------------------                             ! dzsnSV increases
 
        DO ikl = 1,klonv
 
          tdepos(ikl) = dsn_SV(ikl) * dsnbSV(ikl) * dt__SV
          WEagre(ikl) = 0.
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! OUTPUT           for Snow Erosion  Variables
! #sb     IF (ii__SV(ikl).EQ.iwr_SV.AND.jj__SV(ikl).EQ.jwr_SV      .AND.
! #sb.        nn__SV(ikl).EQ.nwr_SV.AND.0          .LT.isnoSV(ikl))
! #sb.        write(6,6003) tdepos(ikl)  ,Mobile(ikl)
 6003         format(/,41x,'tdepos [-] =',f6.3,40x,'Mobil',i3
     .              ,/,27x,'Salt.Index    sdrift'
     .              ,      '    zdepos  ro__snow  ro_bsnow  roN_snow'
     .              ,                '  dz__snow  dz_bsnow  dzN_snow'
     .              ,                '  d___snow'
     .              ,/,27x,'             [kg/m3]   [kg/m3]   [kg/m3]'
     .              ,                '       [m]       [m]       [m]'
     .              ,                '   [kg/m2]')
        END DO
 
      DO isn =     nsno,1,-1
        DO ikl = 1,klonv
          WEagre(ikl) = WEagre(ikl) + ro__SV(ikl,isn)*dzsnSV(ikl,isn)
          isagr1(ikl) = istoSV(ikl,isn)
          isagr2(ikl) = 0.
 
C +--Density of deposited blown Snow
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          ro_new =                    blsno
 
C +--Density of deposited blown Snow (de Montmollin, 1978)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #EM     PorSno =      1.0d00     -  ro__SV(ikl,isn)
c #EM.                             /  ro_Ice
c #EM     Salt_f =      usthSV(ikl)/  max(epsi,   us__SV(ikl))
c #EM     Salt_f =  min(Salt_f     ,  unun)
c #EM     PorRef =      PorSno     /  max(epsi,1.-PorSno)
c #EM.             +log(Salt_f)
c #EM     Por_BS =      PorRef     / (1.0d00 + PorRef)
c #EM     ro_new =      ro_Ice     * (1.0d00 - Por_BS)
c #EM     ro_new =  max(ro_new     ,  blsno )
 
          roagr1(ikl) = ro__SV(ikl,isn)
          roagr2(ikl) = ro_new
ccc       roagr2(ikl) = max(ro_new,roagr1(ikl))
cXF
          hsno_x      = tdepos(ikl)*  zdepos(ikl,isn)
cc        dbs_Ac(ikl) = dbs_Ac(ikl) + hsno_x
 
          dzagr1(ikl) = dzsnSV(ikl,isn)
          dzagr2(ikl) = hsno_x     /  ro_new
C +...    Conversion    [kg/m2, i.e., mm w.e.] -----> [mSnow]
 
          dsn_SV(ikl) = dsn_SV(ikl)-  hsno_x / dt__SV
 
C +--Other Snow Properties
C +  ~~~~~~~~~~~~~~~~~~~~~
          T_agr1(ikl) =    TsisSV(ikl,isn)
          T_agr2(ikl) =min(TfSnow,TaT_SV(ikl))
          etagr1(ikl) =    eta_SV(ikl,isn)
          etagr2(ikl) =    0.0
          G1agr1(ikl) =    G1snSV(ikl,isn)
          G1agr2(ikl) =    G1_dSV
          G2agr1(ikl) =    G2snSV(ikl,isn)
          G2agr2(ikl) =    ADSdSV
c #BY     G2agr2(ikl) =    0.87d0
C +...    Budd et al. 1966, 2~m Average /Table 5 p. 97
 
          agagr1(ikl) =    agsnSV(ikl,isn)
          agagr2(ikl) =    0.
          Agrege(ikl) =    1.
        END DO
 
C +--Agregation
C +  ~~~~~~~~~~
C +     ***************
        call SISVAT_zAg
     .                 (isagr1,isagr2,WEagre
     .                 ,dzagr1,dzagr2,T_agr1,T_agr2
     .                 ,roagr1,roagr2,etagr1,etagr2
     .                 ,G1agr1,G1agr2,G2agr1,G2agr2
     .                 ,agagr1,agagr2,Agrege
     .                 )
C +     ***************
 
        DO ikl = 1,klonv
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! OUTPUT           for Snow Erosion  Variables
! #sb     IF (ii__SV(ikl).EQ.iwr_SV.AND.jj__SV(ikl).EQ.jwr_SV      .AND.
! #sb.        nn__SV(ikl).EQ.nwr_SV.AND.isn        .LE.isnoSV(ikl))
! #sb.        write(6,6004)   isn          ,SaltSI(ikl,isn)
! #sb.                     ,sdrift(ikl,isn),zdepos(ikl,isn)
! #sb.                     ,ro__SV(ikl,isn),roagr2(ikl),roagr1(ikl)
! #sb.                     ,dzsnSV(ikl,isn),dzagr2(ikl),dzagr1(ikl)
! #sb.                     ,dsn_SV(ikl)
 6004         format((27x,i3,f7.2,2f10.6,3f10.3,4f10.6))
 
          istoSV(ikl,isn) = isagr1(ikl)
          dzsnSV(ikl,isn) = dzagr1(ikl)
          TsisSV(ikl,isn) = T_agr1(ikl)
          ro__SV(ikl,isn) = roagr1(ikl)
          eta_SV(ikl,isn) = etagr1(ikl)
          G1snSV(ikl,isn) = G1agr1(ikl)
          G2snSV(ikl,isn) = G2agr1(ikl)
          agsnSV(ikl,isn) = agagr1(ikl)
 
        END DO
 
      END DO
 
c #EF   IF          (isnoSV(1) .GT. 0)
c #EF.  write(6,6008)isnoSV(1),    dsn_SV(1) *dt__SV + BufsSV(1),
c #EF.              (dzsnSV(1,isn)*ro__SV(1,isn),isn=1,isnoSV(1))
 6008   format(i3,'  dsn+Buf=',f6.2,6x,'A dz *ro =',10f6.2,
     .                                       (/,35x,10f6.2))
 
        DO ikl = 1,klonv
          hdrift      =  tdepos(ikl)/dt__SV
          esnbSV(ikl) = (dsnbSV(ikl)-unun)*hdrift/max(dsn_SV(ikl),epsi)
     .                  +dsnbSV(ikl)
          dsnbSV(ikl) =          min(unun,   max(zero,esnbSV(ikl) )   )
!         dsnbSV is now the Blown Snow fraction of precipitating snow
!                will be used for characterizing the Buffer Layer
!               (see update of  Bros_N, G1same, G2same, zroOLD, zroNEW)
        END DO
 
      return
      END
