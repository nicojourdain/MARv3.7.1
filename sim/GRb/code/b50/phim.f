      function phim(zeta_m)
C +
C +------------------------------------------------------------------------+
C | MAR TURBULENCE (ASL)                                   27-09-2001  MAR |
C |   Function phim is the            Universal Function for Momentum      |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C | OPTIONS:Duynkerke,MWR 119,                         324--341, 1991 (#DU |
C | ^^^^^^^ Businger, Workshop on Micrometeorology AMS, 67--100, 1973 (#BU |
C |         Dyer,     BLM 7,                           363--372, 1974 (#DR |
C |         Noihlan,  EERM Internal Note No 171,                 1986 (#NO |
C |                                                                        |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
      include 'MARphy.inc'
C +
      real     phim
      real     zeta_m
      real     r6p1
C +
      data     r6p1/6.25e00/
C +
C +--Stability
C +  ~~~~~~~~~
      IF  (zeta_m.gt.eps9)                                          THEN
           phim=1.0+6.0*zeta_m
C +...     Bintanja 1997, Annals of Glaciology
C +
c #DU      phim=1.0+5.00*zeta_m/exp(0.20*log(unun+r6p1*zeta_m))
c #NO   IF(zeta_m.lt.1.0)                                           THEN
c #DR      phim=1.0+5.00*zeta_m
c #BU      phim=1.0+4.70*zeta_m
c #NO   ELSE
C +
C +--Strong Stability
C +  ~~~~~~~~~~~~~~~~
c #NO      phim=   5.7
c #NO   END IF
      ELSE
C +
C +--Unstability
C +  ~~~~~~~~~~~
           phim=1.0/(1.0-20.0*zeta_m)
c #DR      phim=1.0/(1.0-16.0*zeta_m)
c #BU      phim=1.0/(1.0-15.0*zeta_m)
           phim=sqrt(sqrt(phim))
      END IF
C +
      return
      end
