 
      subroutine DYNfilv3D(f3_fil,e3_fil,k3_fil)
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS FILTER (3-D)                              24-07-2004  MAR |
C |   SubRoutine DYNfilv3D  is used to Filter Horizontal Fields in 3D Code |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:   f3_fil(i,j,k): variable to be filtered (in surface k)       |
C |   ^^^^^    e3_fil(    k): value of the selectivity parameter           |
C |            k3_fil       : vertical dimension of the variable           |
C |                                                                        |
C |   OUTPUT:  f3_fil(i,j,k)                                               |
C |   ^^^^^^^                                                              |
C |                                                                        |
C |   LATERAL BOUNDARIES:                                                  |
C |   ^^^^^^^^^^^^^^^^^^^                                                  |
C |   1. The value    of the variable is fixed at the Boundary             |
C |   2. Filter Selectivity Parameter may be increased near the Boundary   |
C |                                                                (#EP)   |
C |                                                                        |
C |   REFER. : Raymond and Garder, MWR 116, Jan 1988, p209                 |
C |   ^^^^^^^^                                                             |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_WK.inc'
 
      real     f3_fil(mx,my,mz),e3_fil(mz)
      integer  k3_fil
 
 
C +--Local  Variables
C +  ================
 
      real     eps2(   mz)
      real     eps3(   mz)
      real     eps4(   mz)
      real     eps5(   mz)
      real     a1_fil(1:mx,mz) ,b1_fil(1:mx,mz) ,aa_fil(mz)
      real     a2_fil(1:my,mz) ,b2_fil(1:my,mz) ,bb_fil(mz)
 
      integer  i1_f3D,i2_f3D,j1_f3D,j2_f3D,k1_f3D,k2_f3D
 
 
C +--Initialisation
C +  ==============
 
      m  = mx
      m1 = mx1
      m2 = mx2
c #EP m3 = mx-3
c #EP m4 = mx-4
      mn = my
      mn1= my1
      mn2= my2
c #EP mn3= my-3
c #EP mn4= my-4
 
 
      IF    (k3_fil.EQ.1)                                           THEN
             k      =  1
 
 
C +--1st Matrix Initialisation,  1 Level
C +  -----------------------------------
 
          a1_fil( 1,k) = 0.0
          b1_fil( 1,k) = 1.0
          a1_fil(mx,k) = 0.0
          b1_fil(mx,k) = 1.0
 
c #EP     eps5( k) =      e3_fil(k)+e3_fil( k)
c #EP     eps4( k) =        eps5(k)+eps5(k)
c #EP     eps3( k) =        eps4(k)+eps4(k)
c #EP     eps2( k) =        eps3(k)+eps3(k)
C +...    Pour diminution de la sectivite du filtre vers les bords
C +      (augmentation --X 2-- parametre selectivite a chaque pas spatial)
 
          aa_fil(   k) =      1.0-e3_fil(k)
          bb_fil(   k) = 2.0*(1.0+e3_fil(k))
 
        DO i=ip11,mx1
          a1_fil(i, k) = aa_fil(k)
          b1_fil(i, k) = bb_fil(k)
        END DO
 
c #EP     a1_fil(2,k) =      1.0-eps2(k)
c #EP     a1_fil(3,k) =      1.0-eps3(k)
c #EP     a1_fil(4,k) =      1.0-eps4(k)
c #EP     a1_fil(5,k) =      1.0-eps5(k)
c #EP     b1_fil(2,k) = 2.0*(1.0+eps2(k))
c #EP     b1_fil(3,k) = 2.0*(1.0+eps3(k))
c #EP     b1_fil(4,k) = 2.0*(1.0+eps4(k))
c #EP     b1_fil(5,k) = 2.0*(1.0+eps5(k))
 
c #EP     a1_fil(m1,k)= a1_fil(2,k)
c #EP     a1_fil(m2,k)= a1_fil(3,k)
c #EP     a1_fil(m3,k)= a1_fil(4,k)
c #EP     a1_fil(m4,k)= a1_fil(5,k)
c #EP     b1_fil(m1,k)= b1_fil(2,k)
c #EP     b1_fil(m2,k)= b1_fil(3,k)
c #EP     b1_fil(m3,k)= b1_fil(4,k)
c #EP     b1_fil(m4,k)= b1_fil(5,k)
 
 
C +--2th Matrix Initialisation,  1 Level
C +  -----------------------------------
 
          a2_fil( 1,k) = 0.0
          b2_fil( 1,k) = 1.0
          a2_fil(my,k) = 0.0
          b2_fil(my,k) = 1.0
 
        DO j=jp11,my1
          a2_fil(j,k)  = aa_fil(k)
          b2_fil(j,k)  = bb_fil(k)
        END DO
 
c #EP     a2_fil(2,k) = a1_fil(2,k)
c #EP     a2_fil(3,k) = a1_fil(3,k)
c #EP     a2_fil(4,k) = a1_fil(4,k)
c #EP     a2_fil(5,k) = a1_fil(5,k)
c #EP     b2_fil(2,k) = b1_fil(2,k)
c #EP     b2_fil(3,k) = b1_fil(3,k)
c #EP     b2_fil(4,k) = b1_fil(4,k)
c #EP     b2_fil(5,k) = b1_fil(5,k)
 
c #EP     a2_fil(mn1,k) = a1_fil(2,k)
c #EP     a2_fil(mn2,k) = a1_fil(3,k)
c #EP     a2_fil(mn3,k) = a1_fil(4,k)
c #EP     a2_fil(mn4,k) = a1_fil(5,k)
c #EP     b2_fil(mn1,k) = b1_fil(2,k)
c #EP     b2_fil(mn2,k) = b1_fil(3,k)
c #EP     b2_fil(mn3,k) = b1_fil(4,k)
c #EP     b2_fil(mn4,k) = b1_fil(5,k)
 
      ELSE IF (k3_fil.EQ.mz)                                        THEN
 
 
C +--All Matrices Initialisation, mz Levels
C +  --------------------------------------
 
        DO k=1,mz
 
c #EP     eps5( k) =      e3_fil(k)+e3_fil( k)
c #EP     eps4( k) =        eps5(k)+eps5(k)
c #EP     eps3( k) =        eps4(k)+eps4(k)
c #EP     eps2( k) =        eps3(k)+eps3(k)
C +...    Pour diminution de la sectivite du filtre vers les bords
C +      (augmentation --X 2-- parametre selectivite a chaque pas spatial)
 
          aa_fil(   k) =      1.0-e3_fil(k)
          bb_fil(   k) = 2.0*(1.0+e3_fil(k))
        END DO
 
 
C +--1st Matrix   Initialisation, mz Levels
C +  --------------------------------------
 
        DO k=1,mz
         DO i=ip11,mx1
          a1_fil(i, k) = aa_fil(k)
          b1_fil(i, k) = bb_fil(k)
         END DO
        END DO
 
        DO k=1,mz
          a1_fil( 1,k) = 0.0
          b1_fil( 1,k) = 1.0
          a1_fil(mx,k) = 0.0
          b1_fil(mx,k) = 1.0
        END DO
 
c #EP   DO k=1,mz
c #EP     a1_fil( 2,k) =      1.0-eps2(k)
c #EP     a1_fil( 3,k) =      1.0-eps3(k)
c #EP     a1_fil( 4,k) =      1.0-eps4(k)
c #EP     a1_fil( 5,k) =      1.0-eps5(k)
c #EP     b1_fil( 2,k) = 2.0*(1.0+eps2(k))
c #EP     b1_fil( 3,k) = 2.0*(1.0+eps3(k))
c #EP     b1_fil( 4,k) = 2.0*(1.0+eps4(k))
c #EP     b1_fil( 5,k) = 2.0*(1.0+eps5(k))
 
c #EP     a1_fil(m1,k) = a1_fil(2,k)
c #EP     a1_fil(m2,k) = a1_fil(3,k)
c #EP     a1_fil(m3,k) = a1_fil(4,k)
c #EP     a1_fil(m4,k) = a1_fil(5,k)
c #EP     b1_fil(m1,k) = b1_fil(2,k)
c #EP     b1_fil(m2,k) = b1_fil(3,k)
c #EP     b1_fil(m3,k) = b1_fil(4,k)
c #EP     b1_fil(m4,k) = b1_fil(5,k)
c #EP   END DO
 
 
C +--2th Matrix   Initialisation, mz Levels
C +  --------------------------------------
 
        DO k=1,mz
         DO j=jp11,my1
          a2_fil(j,k)  = aa_fil(k)
          b2_fil(j,k)  = bb_fil(k)
         END DO
        END DO
 
        DO k=1,mz
          a2_fil( 1,k) = 0.0
          b2_fil( 1,k) = 1.0
          a2_fil(my,k) = 0.0
          b2_fil(my,k) = 1.0
        END DO
 
c #EP   DO k=1,mz
c #EP     a2_fil(  2,k) = a1_fil(2,k)
c #EP     a2_fil(  3,k) = a1_fil(3,k)
c #EP     a2_fil(  4,k) = a1_fil(4,k)
c #EP     a2_fil(  5,k) = a1_fil(5,k)
c #EP     b2_fil(  2,k) = b1_fil(2,k)
c #EP     b2_fil(  3,k) = b1_fil(3,k)
c #EP     b2_fil(  4,k) = b1_fil(4,k)
c #EP     b2_fil(  5,k) = b1_fil(5,k)
 
c #EP     a2_fil(mn1,k) = a1_fil(2,k)
c #EP     a2_fil(mn2,k) = a1_fil(3,k)
c #EP     a2_fil(mn3,k) = a1_fil(4,k)
c #EP     a2_fil(mn4,k) = a1_fil(5,k)
c #EP     b2_fil(mn1,k) = b1_fil(2,k)
c #EP     b2_fil(mn2,k) = b1_fil(3,k)
c #EP     b2_fil(mn3,k) = b1_fil(4,k)
c #EP     b2_fil(mn4,k) = b1_fil(5,k)
c #EP   END DO
 
      ELSE
        write(6,*) 'k3_fil = ',k3_fil,'! This case is not accounted'
        stop
      END IF
 
 
C +--1st Equations System
C +  ====================
 
C +--Gaussian Elimination Algorithm: Set Up
C +  --------------------------------------
 
      DO k=1,k3_fil
      DO j=1,my
      DO i=1,mx
        WKxyz1(i,j,k) = a1_fil(i,k)
        WKxyz2(i,j,k) = b1_fil(i,k)
        WKxyz3(i,j,k) = a1_fil(i,k)
      END DO
      END DO
      END DO
 
      DO k=1,k3_fil
      DO j=jp11,my1
        WKxyz4(1     ,j ,k) =
     &  f3_fil( 1,jm1(j),k) +2.0*f3_fil( 1,j,k)+f3_fil( 1,jp1(j),k)
        WKxyz4(mx    ,j ,k) =
     &  f3_fil(mx,jm1(j),k) +2.0*f3_fil(mx,j,k)+f3_fil(mx,jp1(j),k)
      END DO
      END DO
 
      DO k=1,k3_fil
      DO i=ip11,mx1
      DO j=   1,my
            WKxyz6(i     ,j     ,k) =
     &      f3_fil(im1(i),j     ,k)+2.0*f3_fil(i     ,j     ,k)
     & +    f3_fil(ip1(i),j     ,k)
      ENDDO
      ENDDO
 
      DO j=jp11,my1
      DO i=ip11,mx1
            WKxyz5(i     ,j     ,k) =   WKxyz6(i     ,jm1(j),k)
            WKxyz7(i     ,j     ,k) =   WKxyz6(i     ,jp1(j),k)
      ENDDO
      ENDDO
 
      DO j=jp11,my1
      DO i=ip11,mx1
            WKxyz4(i     ,j     ,k) =
     .      WKxyz7(i     ,j     ,k)
     .    + WKxyz6(i     ,j     ,k) +   WKxyz6(i     ,j     ,k)
     .    + WKxyz5(i     ,j     ,k)
 
 ! Previous three Loops Stand for the following unvectorized Loop:
 !          WKxyz4(i     ,j     ,k) =
 !   &      f3_fil(im1(i),jp1(j),k)+2.0*f3_fil(i,jp1(j),k)
 !   & +    f3_fil(ip1(i),jp1(j),k)+
 !   &  2.0*f3_fil(im1(i),j     ,k)+4.0*f3_fil(i     ,j     ,k)
 !   & +2.0*f3_fil(ip1(i),j     ,k)+
 !   &      f3_fil(im1(i),jm1(j),k)+2.0*f3_fil(i     ,jm1(j),k)
 !   & +    f3_fil(ip1(i),jm1(j),k)
      END DO
      END DO
      END DO
 
 
C +--Gaussian Elimination Algorithm: F-B Sweep ==> WKxyz7
C +  ----------------------------------------------------
 
      IF (k3_fil.EQ.1)                                              THEN
 
C +        ************
      call MARgx11mx1my
C +        ************
 
      ELSE
 
C +        ************
      call MARgx_1mx1my
C +        ************
 
      ENDIF
 
 
C +--2th Equations System
C +  ====================
 
C +--Gaussian Elimination Algorithm: Set Up
C +  --------------------------------------
 
      DO k=1,k3_fil
      DO j=1,my
      DO i=ip11,mx1
        WKxyz1(i,j,k) = a2_fil(j,k)
        WKxyz2(i,j,k) = b2_fil(j,k)
        WKxyz3(i,j,k) = a2_fil(j,k)
      END DO
      END DO
      END DO
 
      DO k=1,k3_fil
      DO i=ip11,mx1
        WKxyz4(i, 1,k) = f3_fil(i, 1, k)
        WKxyz4(i,my,k) = f3_fil(i,my, k)
      END DO
      END DO
 
      DO k=1,k3_fil
      DO j=jp11,my1
      DO i=ip11,mx1
        WKxyz4(i,j,k) = WKxyz7(i,j,k)
      END DO
      END DO
      END DO
 
      k1_f3D = 1
      k2_f3D = k3_fil
 
 
C +--Gaussian Elimination Algorithm: F-B Sweep ==> WKxyz7
C +  ----------------------------------------------------
 
      IF (k3_fil.EQ.1)                                              THEN
 
C +        ************
      call MARgy11mx1my
C +        ************
 
      ELSE
 
C +        ************
      call MARgy_1mx1my
C +        ************
 
      ENDIF
 
 
C +--Result
C +  ======
 
      DO k=1,k3_fil
      DO j=jp11,my1
      DO i=ip11,mx1
        f3_fil(i,j,k) = WKxyz7(i,j,k)
      END DO
      END DO
      END DO
 
      DO k=1,k3_fil
      DO j=1,my
      DO i=1,mx
        WKxyz1(i,j,k) = 0.0
        WKxyz2(i,j,k) = 0.0
        WKxyz3(i,j,k) = 0.0
        WKxyz4(i,j,k) = 0.0
        WKxyz5(i,j,k) = 0.0
        WKxyz6(i,j,k) = 0.0
        WKxyz7(i,j,k) = 0.0
      END DO
      END DO
      END DO
 
      return
      end
