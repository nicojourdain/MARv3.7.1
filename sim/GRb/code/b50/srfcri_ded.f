      function   SRFcri_ded(kst,knst)
C +
C +------------------------------------------------------------------------+
C | MAR SURFACE                                            01-12-2001  MAR |
C |   Function   SRFcri_ded computes minimal thickness before aggregation  |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT  :   *kst*   -  rang de la strate a examiner.                  |
C |   ^^^^^^^^   *knst*  -  nombre de strates du manteau neigeux.          |
C |                                                                        |
C |   REFER. : (CROCUS & MAR, adapted to MAR at CEN by H.Gallee)           |
C |   ^^^^^^^^                                                             |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +--Global Variables
C +  ================
C +
      real    SRFcri_ded
      real    vdzde1,vdzde2,vdzde3,vdzde4,vdzde5
      integer nvnst8,nvnst9,kst,knst
C +
C +--DATA
C +  ====
C +
      data vdzde1/ 4.00e00/, vdzde2/ 12.00e00/
      data vdzde3/75.00e-3/, vdzde4/  4.00e00/
      data vdzde5/ 2.00e00/
C +
      data nvnst8/ 9      /, nvnst9/  4      /
C +
C +
C +--Snow Layers Aggregation Criterion
C +  =================================
C +
C +--Strate sur le Sol /bottom Layer.
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      if(kst.eq.1)then
        SRFcri_ded=vdzde1
C +
C +--Autres Strates /other Cases.
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~
      elseif(kst.lt.knst-nvnst8)then
         SRFcri_ded=vdzde2-kst*vdzde3
      elseif(kst.lt.knst-nvnst9)then
         SRFcri_ded=vdzde4
      else
         SRFcri_ded=vdzde5
      endif
C +
      return
      end
