 
 
      subroutine DYNadv_hor(ff,fp0,fp1,fu,fv)
C +
C +------------------------------------------------------------------------+
C | MAR DYNAMICS   SLOW                                    27-09-2001  MAR |
C |   SubRoutine DYNadv_hor includes the Horizontal Advection Contribution |
C |              solved by using a Cubic Spline Technique                  |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:   fp0: mass                                                   |
C |   ^^^^^    fu:  advection velocity, x-direction, (e.g., uairDY)        |
C |            fv:  advection velocity, y-direction, (e.g., vairDY)        |
C |                                                                        |
C |            iterun: Iteration            Index                          |
C |            openLB: Zero-Gradient  LBC   Switch                         |
C |            FirstC: First Call at time=t Switch                         |
C |            qqmass: Mass  Conservation   Switch                         |
C |                                                                        |
C |   INPUT &/ ff: advected  variable, which may be:                       |
C |   OUTPUT : uairDY,vairDY, pktaDY, qvDY, qwHY,qrHY, qiHY,ccniHY,qsHY    |
C |   ^^^^^^^^ uairDY  : x-wind speed component                     (m/s)  |
C |            vairDY  : y-wind speed component                     (m/s)  |
C |            pktaDY: Potential Temperature divided by 100.[kPa]**(R/Cp)  |
C |              qvDY: Air specific Humidity                      (kg/kg)  |
C |            ****HY: Hydrometeor  Concentration                 (kg/kg)  |
C |                                                                        |
C |   METHOD : The following Contributions may be taken into account:      |
C |   ^^^^^^^^     du/dt:=-udu/dx -vdu/dy                                  |
C |                dv/dt:=-udv/dx -vdv/dy                                  |
C |                dP/dt:=-udP/dx -vdP/dy  (Potential Temperature)         |
C |                dq/dt:=-udq/dx -vdq/dy  (Water     Species)             |
C |            Correction for Mass Conservation  (qqmass = .true.)         |
C |            is based on the assumption that the meteorological fields   |
C |            at Lateral Boundaries (LB) change only through relaxation   |
C |            of MAR Fields towards Large Scale Meteorological Fields,    |
C |            ==> Total Mass remains constant during "inner" Advection    |
C |                (i1,i2) = (1,mx)   ==>    correction operated at LB's   |
C |                          (   correction slightly inconsistent     )    |
C |                (i1,i2) = (2,mx-1) ==> no correction operated at LB's   |
C |                          (LB relaxation slightly badly conditioned)    |
C |            Inclusion of Mass Flux at the LB's causes a conflict        |
C |                                                      with LBC scheme   |
C |            This is verified by the onset of spurious waves   at LB's   |
C |                                                                        |
C |   REFER. : Alpert, thesis, 1980                                        |
C |   ^^^^^^^^ Pielke, Mesoscale Meteorological Modeling, 297--307, 1984   |
C |           (Seibert and Morariu, JAM, p.118, 1991)                      |
C |                                                                        |
C | # OPTIONS: #MC (Mass   Correction)             performed               |
C | # ^^^^^^^^ #MD (Mass   Difference) Correction  preferred               |
C |                                                                        |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +--Global Variables
C +  ================
C +
      include 'MARCTR.inc'
      include 'MARphy.inc'
C +
      include 'MARdim.inc'
      include 'MARgrd.inc'
C +
      include 'MAR_HY.inc'
      include 'MAR_CU.inc'
C +
      include 'MAR_LB.inc'
      include 'MAR_WK.inc'
C +
      real      fp0(mx,my)
      real      fp1(mx,my)
      real       ff(mx,my,mz)
      real       fu(mx,my,mz)
      real       fv(mx,my,mz)
C +
C +
C +--Local  Variables
C +  ================
C +
      integer iunPos,junPos,iindex,jindex
C +
      integer               i1_adh  ,i2_adh   ,j1_adh  ,j2_adh   ,k_pdim
      parameter            (i1_adh=1,i2_adh=mx,j1_adh=1,j2_adh=my)
      parameter            (k_pdim=mz)
c #QB common/DYNadv_hor_loc/i1_adh  ,i2_adh   ,j1_adh  ,j2_adh
C +
c #MC real                  sumMav
      real                  sumMx
      common/DYNadv_horrloc/sumMx(mz)
C +
      real                    dff
      real                     f0(mx,my,mz)
      real                  sumM0(mz),sumM1(mz)
      real                  sumP0(mz),sumP1(mz)
      real                  sumF0(mz)
      real                 rsum,rsumd,rsumds,rsumda,FlwPos
C +
c #SP logical               log_xx,log_yy
C +
      logical               qqflux
      logical               NestOK
C +
C +
C +--DATA
C +  ====
C +
      data                  qqflux/.false./
      data                  NestOK/.true./
C +
C +
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +
C +
C +--Conservation: Mass
C +  ==================
C +
      IF   (qqmass)                                               THEN
        IF (FirstC)                                               THEN
C +
C +
C +--Interior of the Model Domain
C +  ----------------------------
C +
c #MC     IF (qqflux)                                             THEN
c #MC       DO   k=1,k_pdim
c #MC           sumM0(k)= 0.0
c #MC           sumM1(k)= 0.0
c #MC         DO j=j1_adh,j2_adh
c #MC         DO i=i1_adh,i2_adh
c #MC           sumM0(k)= sumM0(k)+ fp0(i,j)
c #MC           sumM1(k)= sumM1(k)+ fp1(i,j)
c #MC         END DO
c #MC         END DO
c #MC       END DO
c #MC     ELSE
c #MC       DO   k=1,k_pdim
c #MC           sumM0(k)= 0.0
c #MC           sumM1(k)= 0.0
c #MC         DO j=1,my
c #MC         DO i=1,mx
c #MC           sumM0(k)= sumM0(k)+ fp0(i,j)
c #MC           sumM1(k)= sumM1(k)+ fp1(i,j)
c #MC         END DO
c #MC         END DO
c #MC       END DO
c #MC     END IF
C +
C +
C +--Mass Inflow / x-Lateral Boundaries
C +  ----------------------------------
C +
c #MC     IF (mmx.gt.1.and.qqflux)                                THEN
C +
C +--"x-small" Boundary
C +  ~~~~~~~~~~~~~~~~~~
c #MC            i=i1_adh
c #MC       DO   k=     1,k_pdim
c #MC         DO j=j1_adh,j2_adh
c #MC           sumM0(k)= sumM0(k)+ fp0(i,j)*fu(i,j,k)*dtx
c #MC         END DO
C +
C +--"x-large" Boundary
C +  ~~~~~~~~~~~~~~~~~~
c #MC            i=i2_adh
c #MC         DO j=j1_adh,j2_adh
c #MC           sumM0(k)= sumM0(k)- fp0(i,j)*fu(i,j,k)*dtx
c #MC         END DO
c #MC       END DO
c #MC     END IF
C +
C +
C +--Mass Inflow / y-Lateral Boundaries
C +  ----------------------------------
C +
c #MC     IF (mmy.gt.1.and.qqflux)                                THEN
C +
C +--"y-small" Boundary
C +  ~~~~~~~~~~~~~~~~~~
c #MC            j=j1_adh
c #MC       DO   k=     1,k_pdim
c #MC         DO i=i1_adh,i2_adh
c #MC           sumM0(k)= sumM0(k)+ fp0(i,j)*fv(i,j,k)*dtx
c #MC         END DO
C +
C +--"y-large" Boundary
C +  ~~~~~~~~~~~~~~~~~~
c #MC            j=j2_adh
c #MC         DO i=i1_adh,i2_adh
c #MC           sumM0(k)= sumM0(k)- fp0(i,j)*fv(i,j,k)*dtx
c #MC         END DO
c #MC       END DO
c #MC     END IF
c #MC           sumMav  = 0.
            DO   k= 1,k_pdim
                sumMx(k)= 1.
c #MC           sumMx(k)= sumM0(k)/ sumM1(k)
c #MC           sumMav  = sumMav  + sumMx(k) *dsigm1(k)
            END DO
c #MC         DO j=j1_adh,j2_adh
c #MC         DO i=i1_adh,i2_adh
c #MC           fp1(i,j)= fp1(i,j)* sumMav
c #MC         END DO
c #MC         END DO
        END IF
C +
C +
C +--Conservation: Property
C +  ======================
C +
C +--Interior of the Model Domain
C +  ----------------------------
C +
          IF (qqflux)                                             THEN
            DO   k=     1,k_pdim
                sumP0(k) = 0.0
                sumF0(k) = 0.0
              DO j=j1_adh,j2_adh
              DO i=i1_adh,i2_adh
                f0(i,j,k) = ff(i,j,k) *fp0(i,j)
                 sumP0(k) =  sumP0(k) + f0(i,j,k)
              END DO
              END DO
            END DO
          ELSE
            DO   k=     1,k_pdim
                sumP0(k)= 0.0
              DO j=     1,my
              DO i=     1,mx
                f0(i,j,k) = ff(i,j,k) *fp0(i,j)
                 sumP0(k) =  sumP0(k) + f0(i,j,k)
              END DO
              END DO
            END DO
          END IF
C +
C +
C +--Mass Inflow / x-Lateral Boundaries
C +  ----------------------------------
C +
          IF (mmx.gt.1.and.qqflux)                                THEN
            DO   k=     1,k_pdim
                 i=i1_adh
              DO j=j1_adh,j2_adh
                sumF0(k) = sumF0(k)
     .                  + ff(i,j,k)*fp0(i,j)*fu(i,j,k)*dtx
              END DO
                 i=i2_adh
              DO j=j1_adh,j2_adh
                sumF0(k) = sumF0(k)
     .                  - ff(i,j,k)*fp0(i,j)*fu(i,j,k)*dtx
              END DO
            END DO
          END IF
C +
C +
C +--Mass Inflow / y-Lateral Boundaries
C +  ----------------------------------
C +
          IF (mmy.gt.1.and.qqflux)                                THEN
            DO   k=     1,k_pdim
                 j=j1_adh
              DO i=i1_adh,i2_adh
                sumF0(k) = sumF0(k)
     .                  + ff(i,j,k)*fp0(i,j)*fv(i,j,k)*dtx
              END DO
                 j=j2_adh
              DO i=i1_adh,i2_adh
                sumF0(k) = sumF0(k)
     .                  - ff(i,j,k)*fp0(i,j)*fv(i,j,k)*dtx
              END DO
            END DO
          END IF
          IF (             qqflux)                                THEN
            DO   k=     1,k_pdim
                sumP0(k) = sumP0(k) + sumF0(k)
            END DO
          END IF
C +
C +
C +--Positive Definiteness Condition
C +  -------------------------------
C +
            DO   k=     1,k_pdim
                sumP0(k) = max(sumP0(k),zero)
            END DO
C +
      END IF
C +
C +
C +--Time Splitting (Alternate Direction)
C +  ====================================
C +
c #SP log_xx = .false.
c #SP log_yy = .false.
C +
 300  CONTINUE
c #SP IF (mod(itexpe,2).eq.0.and..not.log_yy)                GO TO 301
C +
C +
C +--Advection Contribution following x
C +  ==================================
C +
        IF (mmx.gt.1)                                             THEN
C +
          DO i=1,mx
            DO k=   1,k_pdim
            DO j=jp11,my1
              WKxyz1(i,j,k)=fu(i,j,k) *dtx
            END DO
            END DO
          END DO
C +
C +
C +--First Order Derivative, LBC
C +  ---------------------------
C +
            DO k=   1,k_pdim
            DO j=jp11,my1
              WKxyz2( 1,j,k) = 0.0
              WKxyz2(mx,j,k) = 0.0
            END DO
            END DO
C +
C +
C +--First Order Derivative, Forward  Sweep
C +  --------------------------------------
C +
          DO i=ip11,mx1
            DO k=   1,k_pdim
            DO j=jp11,my1
              WKxyz2(i,j,k)=(3.0*(ff(i+1,j,k)-ff(i-1,j,k))/dx
     .                       -WKxyz2(i-1,j,k)                )/CUspxh(i)
            END DO
            END DO
          END DO
C +
C +
C +--First Order Derivative, Backward Sweep
C +  --------------------------------------
C +
          DO i=mx2,ip11,-1
            DO k=   1,k_pdim
            DO j=jp11,my1
              WKxyz2(i,j,k)=CUspxb(i)*WKxyz2(i+1,j,k)+WKxyz2(i,j,k)
            END DO
            END DO
          END DO
C +
C +
C +--First Order Difference
C +  ----------------------
C +
          DO i=1,mx
            DO k=   1,k_pdim
            DO j=jp11,my1
              WKxyz2(i,j,k)= WKxyz2(i,j,k)*dx
            END DO
            END DO
          END DO
C +
C +
C +--Interpolated Variable
C +  ---------------------
C +
          DO i=1,mx
            DO k=   1,k_pdim
            DO j=jp11,my1
              WKxyz3(i,j,k)= WKxyz1(i,j,k)*WKxyz1(i,j,k)
            END DO
            END DO
          END DO
C +
C +
C +--Direction of Advection
C +  ----------------------
C +
          DO i=1,mx
            DO k=   1,k_pdim
            DO j=jp11,my1
              WKxyz4(i,j,k) =  sign(unun,WKxyz1(i     ,j,k))
              iunPos        =            WKxyz4(i     ,j,k)
              iindex        =   max(   1,i-iunPos)
              iindex        =   min(  mx,  iindex)
              WKxyz5(i,j,k) =            WKxyz2(iindex,j,k)
              WKxyz6(i,j,k) = WKxyz4(i,j,k)*(ff(iindex,j,k)-ff(i,j,k))
            END DO
            END DO
          END DO
C +
C +
C +--Advection
C +  ---------
C +
          DO i=1,mx
            DO k=   1,k_pdim
            DO j=jp11,my1
              WKxyz7(i,j,k)=WKxyz6(i,j,k)+WKxyz6(i,j,k)+WKxyz2(i,j,k)
              WKxyz8(i,j,k)=WKxyz7(i,j,k)+WKxyz6(i,j,k)+WKxyz2(i,j,k)
              WKxyz6(i,j,k)=    ff(i,j,k)-WKxyz1(i,j,k)*WKxyz2(i,j,k)
            END DO
            END DO
          END DO
C +
C +
          DO i=1,mx
            DO k=   1,k_pdim
            DO j=jp11,my1
              WKxyz6(i,j,k)=    ff(i,j,k)- WKxyz1(i,j,k)*WKxyz2(i,j,k)
     .       +WKxyz4(i,j,k)*WKxyz3(i,j,k)*(WKxyz8(i,j,k)+WKxyz5(i,j,k))
     .       -WKxyz1(i,j,k)*WKxyz3(i,j,k)*(WKxyz7(i,j,k)+WKxyz5(i,j,k))
            END DO
            END DO
          END DO
C +
C +
C +--Inflow LBC
C +  ----------
C +
          IF   (qqflux)                                           THEN
C +
C +--Large Scale Contribution over dt (dff, to be implemented)
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            IF (NestOK)                                           THEN
                dff = 0.
C +
C +--Host Model Solution is preferred ("outer" solution if inflow included)
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
              DO k=   1,k_pdim
              DO j=   1,my
                WKxyz6( 1,j,k) =         ff( 1,j,k) + dff
                WKxyz6(mx,j,k) =         ff(mx,j,k) + dff
              END DO
              END DO
C +
C +--MAR        Solution is preferred ("inner" solution if inflow included)
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            ELSE
              DO k=   1,k_pdim
              DO j=   1,my
                FlwPos         = max(zero, sign(unun,WKxyz4( 1,j,k)))
                WKxyz6( 1,j,k) =     WKxyz6( 1,j,k) * (unun - FlwPos)
     .                         +         ff( 1,j,k) *         FlwPos
                FlwPos         = max(zero, sign(unun,WKxyz4(mx,j,k)))
                WKxyz6(mx,j,k) =     WKxyz6(mx,j,k) *         FlwPos
     .                         +         ff(mx,j,k) * (unun - FlwPos)
              END DO
              END DO
            END IF
C +
C +--Host Model Solution is preferred
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          ELSE
              DO k=   1,k_pdim
              DO j=   1,my
                WKxyz6( 1,j,k) =         ff( 1,j,k)
                WKxyz6(mx,j,k) =         ff(mx,j,k)
              END DO
              END DO
          END IF
C +
C +
C +--Finalisation
C +  ------------
C +
          DO i=1,mx
            DO k=   1,k_pdim
            DO j=jp11,my1
                  ff( i,j,k) = WKxyz6(i,j,k)
              WKxyz1( i,j,k) = 0.0
              WKxyz2( i,j,k) = 0.0
              WKxyz3( i,j,k) = 0.0
              WKxyz4( i,j,k) = 0.0
              WKxyz5( i,j,k) = 0.0
              WKxyz6( i,j,k) = 0.0
              WKxyz7( i,j,k) = 0.0
              WKxyz8( i,j,k) = 0.0
            END DO
            END DO
          END DO
        END IF
C +
c #SP log_xx = .true.
C +
C +
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +
C +
C +--Time Splitting (Alternate Direction)
C +  ====================================
C +
 301  CONTINUE
c #SP IF (                            log_yy)                GO TO 302
C +
C +
C +--Advection Contribution following y
C +  ==================================
C +
        IF (mmy.gt.1)                                             THEN
C +
          DO j=1,my
            DO k=   1,k_pdim
            DO i=ip11,mx1
              WKxyz1(i,j,k)=fv(i,j,k) *dtx
            END DO
            END DO
          END DO
C +
C +
C +--First Order Derivative, LBC
C +  ---------------------------
C +
            DO k=   1,k_pdim
            DO i=ip11,mx1
              WKxyz2(1, 1,k) = 0.0
              WKxyz2(1,my,k) = 0.0
            END DO
            END DO
C +
C +
C +--First Order Derivative, Forward  Sweep
C +  --------------------------------------
C +
          DO j=jp11,my1
            DO k=   1,k_pdim
            DO i=ip11,mx1
              WKxyz2(i,j,k)=(3.0*(ff(i,j+1,k)-ff(i,j-1,k))/dx
     .                       -WKxyz2(i,j-1,k)                )/CUspyh(j)
            END DO
            END DO
          END DO
C +
C +
C +--First Order Derivative, Backward Sweep
C +  --------------------------------------
C +
          DO j=my2,jp11,-1
            DO k=   1,k_pdim
            DO i=ip11,mx1
              WKxyz2(i,j,k)=CUspyb(j)*WKxyz2(i,j+1,k)+WKxyz2(i,j,k)
            END DO
            END DO
          END DO
C +
C +
C +--First Order Difference
C +  ----------------------
C +
          DO j=1,my
            DO k=   1,k_pdim
            DO i=ip11,mx1
              WKxyz2(i,j,k)= WKxyz2(i,j,k)*dx
            END DO
            END DO
          END DO
C +
C +
C +--Interpolated Variable
C +  ---------------------
C +
          DO j=1,my
            DO k=   1,k_pdim
            DO i=ip11,mx1
              WKxyz3(i,j,k)= WKxyz1(i,j,k)*WKxyz1(i,j,k)
            END DO
            END DO
          END DO
C +
C +
C +--Direction of Advection
C +  ----------------------
C +
          DO j=1,my
            DO k=   1,k_pdim
            DO i=ip11,mx1
              WKxyz4(i,j,k) =  sign(unun,WKxyz1(i,j     ,k))
              junPos        =            WKxyz4(i,j     ,k)
              jindex        =   max(   1,j-junPos)
              jindex        =   min(  my,  jindex)
              WKxyz5(i,j,k) =            WKxyz2(i,jindex,k)
              WKxyz6(i,j,k) = WKxyz4(i,j,k)*(ff(i,jindex,k)-ff(i,j,k))
            END DO
            END DO
          END DO
C +
C +
C +--Advection
C +  ---------
C +
          DO j=1,my
            DO k=   1,k_pdim
            DO i=ip11,mx1
              WKxyz7(i,j,k)=WKxyz6(i,j,k)+WKxyz6(i,j,k)+WKxyz2(i,j,k)
              WKxyz8(i,j,k)=WKxyz7(i,j,k)+WKxyz6(i,j,k)+WKxyz2(i,j,k)
              WKxyz6(i,j,k)=    ff(i,j,k)-WKxyz1(i,j,k)*WKxyz2(i,j,k)
            END DO
            END DO
          END DO
C +
C +
          DO j=1,my
            DO k=   1,k_pdim
            DO i=ip11,mx1
                  ff(i,j,k)=    ff(i,j,k)- WKxyz1(i,j,k)*WKxyz2(i,j,k)
     .       +WKxyz4(i,j,k)*WKxyz3(i,j,k)*(WKxyz8(i,j,k)+WKxyz5(i,j,k))
     .       -WKxyz1(i,j,k)*WKxyz3(i,j,k)*(WKxyz7(i,j,k)+WKxyz5(i,j,k))
            END DO
            END DO
          END DO
C +
C +
C +--Inflow LBC
C +  ----------
C +
          IF   (qqflux)                                           THEN
C +
C +--Large Scale Contribution over dt (dff, to be implemented)
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            IF (NestOK)                                           THEN
                dff = 0.
C +
C +--Host Model Solution is preferred ("outer" solution if inflow included)
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
              DO k=   1,k_pdim
              DO i=   1,mx
                WKxyz6(i, 1,k) =         ff(i, 1,k)
                WKxyz6(i,my,k) =         ff(i,my,k)
              END DO
              END DO
C +
C +--Nested Model Solution is preferred
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
C +
C +--MAR        Solution is preferred ("inner" solution if inflow included)
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            ELSE
              DO k=   1,k_pdim
              DO i=   1,mx
                FlwPos         = max(zero, sign(unun,WKxyz4(i, 1,k)))
                WKxyz6(i, 1,k) =     WKxyz6(i, 1,k) * (unun - FlwPos)
     .                         +         ff(i, 1,k) *         FlwPos
                FlwPos         = max(zero, sign(unun,WKxyz4(i,my,k)))
                WKxyz6(i,my,k) =     WKxyz6(i,my,k) *         FlwPos
     .                         +         ff(i,my,k) * (unun - FlwPos)
              END DO
              END DO
            END IF
C +
C +--Host Model Solution is preferred
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          ELSE
              DO k=   1,k_pdim
              DO i=   1,mx
                WKxyz6(i, 1,k) =         ff(i, 1,k)
                WKxyz6(i,my,k) =         ff(i,my,k)
              END DO
              END DO
          END IF
C +
C +
C +--Finalisation
C +  ------------
C +
          DO j=1,my
            DO k=   1,k_pdim
            DO i=ip11,mx1
                  ff(i, j,k) = WKxyz6(i,j,k)
              WKxyz1(i, j,k) = 0.0
              WKxyz2(i, j,k) = 0.0
              WKxyz3(i, j,k) = 0.0
              WKxyz4(i, j,k) = 0.0
              WKxyz5(i, j,k) = 0.0
              WKxyz6(i, j,k) = 0.0
              WKxyz7(i, j,k) = 0.0
              WKxyz8(i, j,k) = 0.0
            END DO
            END DO
          END DO
        END IF
C +
c #SP log_yy = .true.
C +
c #SP IF (                       .not.log_xx)                GO TO 300
 302  CONTINUE
C +
C +
C +--Conservation
C +  ============
C +
        IF   (qqmass)                                             THEN
C +
C +
C +--Fluxes at the Lateral Boundaries      through Advection
C +  -------------------------------------------------------
C +
          IF (qqflux)                                             THEN
            DO k=1,k_pdim
                     sumP1(k)     = 0.0
                  DO     j=j1_adh,j2_adh
                  DO     i=i1_adh,i2_adh
                     WKxy1(i,j) = ff(i,j,k) *    fp1(i,j)
                     sumP1(k)   =  sumP1(k) +  WKxy1(i,j)
                  END DO
                  END DO
C +
                    rsumd     = sumP1(k)
c #MD               rsumd     = sumP1(k)-sumP0(k)
                    rsumds    = sign(unun,rsumd)
                    rsumda    =  abs(     rsumd)
                    rsumd     =  max(eps9,rsumda)       *rsumds
                    rsum      = sumP0(k) /rsumd
c #MD               rsum      = sumF0(k) /rsumd
                  DO     j=j1_adh,j2_adh
                  DO     i=i1_adh,i2_adh
                    ff(i,j,k) =     ff(i,j,k)           *rsum
c #MD               ff(i,j,k) =(    f0(i,j,k)
c #MD.                         +(WKxy1(i,j)  -f0(i,j,k))*rsum)
c #MD.                        /    fp1(i,j)
                  END DO
                  END DO
C +
            END DO
C +
                  DO     j=1,my
                  DO     i=1,mx
                    WKxy1(i,j) = 0.0
                  END DO
                  END DO
C +
C +
C +--Fluxes at the Lateral Boundaries only through the nudging Procedure
C +  -------------------------------------------------------------------
C +
          ELSE
C +
            DO k=1,k_pdim
                     sumP1(k) = 0.0
                  DO     j=1,my
                  DO     i=1,mx
                     sumP1(k) = sumP1(k) + ff(i,j,k) * fp1(i,j)
                  END DO
                  END DO
C +
                    rsumd     = sumP1(k) * sumMx(k)
                    rsumds    = sign(unun,rsumd)
                    rsumda    =  abs(     rsumd)
                    rsumd     =  max(eps9,rsumda)    * rsumds
                    rsum      = sumP0(k) /rsumd
                  DO     j=1,my
                  DO     i=1,mx
                    ff(i,j,k) =            ff(i,j,k) * rsum
                  END DO
                  END DO
C +
            END DO
          END IF
C +
        END IF
C +
C +
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +
C +
      return
      end
