 
 
      subroutine GRDstr(xxmar,yymar,GE0lon,GE0lat,GElonM,GElatM,GEtruL)
 
C +------------------------------------------------------------------------+
C | MAR GRID                                               19-11-2004  MAR |
C |   SubRoutine GRDstr computes the Latitudes, Longitudes                 |
C |                              of a MAR Domain Grid Point                |
C |                     assuming Inverse Stereographic Oblique Projection  |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:  xxmar ,yymar : MAR        Coordinates                        |
C |   ^^^^^^  GE0lon,GE0lat: Geographic Coordinates of MAR Domain Center   |
C |                          (3-D): South-North Direction along            |
C |                                 90E, 180E, 270E or 360E Meridians      |
C |                                                                        |
C |   OUTPUT: GElatM       : Latitude  of the MAR grid point      (radian) |
C |   ^^^^^^^ GElonM       : Longitude of the MAR grid point        (hour) |
C |                                                                        |
C |   REFERENCE: F. Pearson, Map projection methods, CRC Press, 1990.      |
C |   ^^^^^^^^^^                                                           |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
 
C +--local  Parameters
C +  =================
 
      real    pidemi,CphiP ,SphiP ,Sphi
      real    dGElon,GElonM,GElatM
      real    GEtruL,GE0lon,GE0lat
      real    denomi,OBLlon,OBLlat
      real    ddista,xxmar ,yymar
      real    costru
 
      pidemi= pi / 2.0
 
      CphiP = cos(degrad*GE0lat)
      SphiP = sin(degrad*GE0lat)
 
      costru= cos(degrad*GEtruL)
 
 
C +--Coordinates relative to a Pole set to the Domain Center
C +  =======================================================
 
 
C +--Relative Longitude -OBLlon (0 <= OBLlon < 2pi)
C +  ----------------------------------------------
 
      if      (xxmar.gt.0.)                      then
         OBLlon = pidemi - atan(yymar/xxmar)
      else if (xxmar.eq.0. .and. yymar.lt.0.)    then
         OBLlon = pi
      else if (xxmar.lt.0.)                      then
         OBLlon = 3.00 *pidemi - atan(yymar/xxmar)
      else if (xxmar.eq.0. .and. yymar.ge.0.)    then
         OBLlon = 0.0
      end if
 
 
C +--Relative  Latitude  OBLlat
C +  --------------------------
 
      ddista = sqrt ( xxmar*xxmar + yymar*yymar )
      OBLlat = 0.50*pi - 2.0*atan(ddista/(earthr*(1.+costru)))
 
 
C +--Coordinates Change (OBLlon,OBLlat) -> (GElonM,GElatM)
C +                   / (rotation, Pearson p.57)
C +  =====================================================
 
 
C +--Latitude (radians)
C +  ------------------
 
      Sphi  = SphiP * sin(OBLlat) + CphiP * cos(OBLlat) * cos(OBLlon)
      GElatM= asin(Sphi)
 
 
C +--Longitude  (hours)
C +  ------------------
 
C +--dGElon = GElonM - GE0lon  (-pi < dGElon <= pi)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      denomi =  CphiP * tan (OBLlat) - SphiP * cos(OBLlon)
 
      if (OBLlon.gt.epsi          .and. OBLlon.lt.(pi-epsi))     then
 
C +--1) OBLlon in trigonometric quadrant 1 or 4 ("right"):
C +     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        dGElon = atan(sin(OBLlon)/denomi)
        if (dGElon.lt.0.0) then
            dGElon = dGElon + pi
C +...      Go to Quadrant 1 by adding       180 degrees
        end if
 
C +--2) OBLlon is in trigonometric quadrant 2or3 ("left "):
C +     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      else if (OBLlon.gt.(pi+epsi).and. OBLlon.lt.(2.0*pi-epsi)) then
 
        dGElon = atan(sin(OBLlon)/denomi)
        if (dGElon.gt.0.0) then
            dGElon = dGElon - pi
C +...      Go to Quadrant 2 by substracting 180 degrees
        end if
 
      else if (OBLlon.le.epsi .or. OBLlon.ge.(2.0*pi-epsi))      then
 
C +--3) OBLlon = 0 -> dGElon = 0 or pi :
C +     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if ((pidemi-OBLlat) .gt. (pidemi-degrad*GE0lat) ) then
C +...    North pole crossed ==> add 180 degrees to Longitude
          dGElon = pi
        else
          dGElon = 0.0
        end if
 
      else if (OBLlon.ge.(pi-epsi) .and. OBLlon.le.(pi+epsi))    then
 
C +--4) OBLlon = pi -> dGElon = 0 or pi :
C +     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if ((pidemi-OBLlat) .gt. (pidemi+degrad*GE0lat) ) then
C +...    South pole crossed ==> add 180 degrees to Longitude
          dGElon = pi
        else
          dGElon = 0.0
        end if
      end if
 
C +--Longitude (hours)
C +  ~~~~~~~~~
      GElonM= (dGElon + GE0lon * degrad) / hourad
 
      return
      end
