 
 
      subroutine LBCnud_000(f_LBC0,iv_nu0,kd_nu0)
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS LBC                                   Thu  3-12-2009  MAR |
C |   SubRoutine LBCnud_000 computes the Lateral Boundary Conditions       |
C |              following the Davies (1976) scheme                        |
C |              assuming  zero Outer Fields                               |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT / OUTPUT : f_LBC0, i.e. w,pairNH, ccniHY, qi,qs,qr,qwHY        |
C |   ^^^^^^^^     for iv_nu0 =            3,      3,  3, 3, 3,   3)       |
C |                    f_LBC0 reevalued on a 5-points width boundary zone  |
C |                                                                        |
C |   INPUT:   iv_nu0: Index of the Variable to relax to Outer Conditions  |
C |   ^^^^^^   kd_nu0: Maximum Value of the k (vertical) Index             |
C |                                                                        |
C |   INPUT (via common block)                                             |
C |   ^^^^^    reaLBC: Input INI: Previous Dyn.Simulation (MAR .or. GCM)   |
C |            rxfact: Lateral Sponge Coefficient         (A89)            |
C |            rxLB,ryLB: Nudging Coefficient                              |
C |            Independant Term  used in the Implicit Scheme               |
C |                                                                        |
C |   REFER. : Davies, QJRMS 102, pp.405--418, 1976  (relation 11 p.409)   |
C |   ^^^^^^^^                                                             |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
!    Global Variables
!    ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_LB.inc'
      include 'MAR_WK.inc'
 
      real     f_LBC0(mx,my,mz)
      integer  iv_nu0,kd_nu0
 
 
!    Local  Variables
!    ================
 
      logical  relaxg
 
      real     fmag0g(6),fmag0d(6),fmag0b(6),fmag0h(6)
      data fmag0g/1.0e0,1.0e0,1.0e0,1.0e0,1.0e0,1.0e0/
      data fmag0d/1.0e0,1.0e0,1.0e0,1.0e0,1.0e0,1.0e0/
      data fmag0b/1.0e0,1.0e0,1.0e0,1.0e0,1.0e0,1.0e0/
      data fmag0h/1.0e0,1.0e0,1.0e0,1.0e0,1.0e0,1.0e0/
C +...     fmag0X:magnification factor (=>nudging selectively modified)
 
c #OG data relaxg/.false./
C +...     relaxg=.false.==> NO nudging  at the left boundary.
 
 
!    x Boundaries
!    ============
 
      IF (mmx.gt.1)                                               THEN
 
c #OG   IF (relaxg)                                               THEN
          DO i=ip11,n6-1
          DO k=  1 ,kd_nu0
          DO j=jp11,my1
              f_LBC0(i,j,k) =  f_LBC0(i,j,k)
     .         /(1.0     +     fmag0g(iv_nu0)*rxLB(i))
          END DO
          END DO
          END DO
c #OG   END IF
 
          DO i=mx-n6+2,mx1
          DO k=  1    ,kd_nu0
          DO j=jp11   ,my1
              f_LBC0(i,j,k) =  f_LBC0(i,j,k)
     .         /(1.0     +     fmag0d(iv_nu0)*rxLB(i))
          END DO
          END DO
          END DO
 
 
!    Zero Gradient at x LBC if fmag0g,d = 0
!    --------------------------------------
 
          DO k=  1 ,kd_nu0
          DO j=jp11,my1
              f_LBC0( 1,j,k) = (1.-fmag0g(iv_nu0))*f_LBC0(ip11,j,k) ! 0-grad.
!    .                       +     fmag0g(iv_nu0) *f_LBC0(   1,j,k) ! 0 at x-LB
              f_LBC0(mx,j,k) = (1.-fmag0d(iv_nu0))*f_LBC0( mx1,j,k) ! 0-grad.
!    .                       +     fmag0d(iv_nu0) *f_LBC0( mx ,j,k) ! 0 at x-LB
          END DO
          END DO
 
 
!    Nudging to zero in in the lateral Sponge
!    ----------------------------------------
 
          DO i=ip11,n6-1
          DO k=  1 ,kd_nu0
          DO j=jp11,my1
            WKxyz1(i  ,j,k)  = f_LBC0(  i,j,k) + rxfact*rxLB(i)
     .    *(f_LBC0(i+1,j,k)  + f_LBC0(i-1,j,k)
     .     -f_LBC0(i  ,j,k)  - f_LBC0(i  ,j,k))
          END DO
          END DO
          END DO
 
          DO i=ip11,n6-1
          DO k=  1 ,kd_nu0
          DO j=jp11,my1
            f_LBC0(i  ,j,k)  = WKxyz1(i  ,j,k)
          END DO
          END DO
          END DO
 
          DO i=mx-n6+2,mx1
          DO k=  1    ,kd_nu0
          DO j=jp11   ,my1
            WKxyz1(i  ,j,k)  = f_LBC0(  i,j,k) + rxfact*rxLB(i)
     .    *(f_LBC0(i+1,j,k)  + f_LBC0(i-1,j,k)
     .     -f_LBC0(i  ,j,k)  - f_LBC0(i  ,j,k))
          END DO
          END DO
          END DO
 
          DO i=mx-n6+2,mx1
          DO k=  1    ,kd_nu0
          DO j=jp11   ,my1
            f_LBC0(i  ,j,k)  = WKxyz1(i  ,j,k)
          END DO
          END DO
          END DO
 
 
!    Zero Gradient at x LBC if fmag0g,d = 0
!    --------------------------------------
 
          DO k=  1 ,kd_nu0
          DO j=jp11,my1
              f_LBC0( 1,j,k) = (1.-fmag0g(iv_nu0))*f_LBC0(ip11,j,k) ! 0-grad.
!    .                       +     fmag0g(iv_nu0) *f_LBC0(   1,j,k) ! 0 at x-LB
              f_LBC0(mx,j,k) = (1.-fmag0d(iv_nu0))*f_LBC0( mx1,j,k) ! 0-grad.
!    .                       +     fmag0d(iv_nu0) *f_LBC0( mx ,j,k) ! 0 at x-LB
          END DO
          END DO
 
      END IF
 
 
!    y Boundaries
!    ============
 
      IF (mmy.gt.1)                                               THEN
 
          DO j=jp11,n6-1
          DO k=  1 ,kd_nu0
          DO i=  1 ,mx
            f_LBC0(i,j,k) = f_LBC0(i,j,k)
     .      /(1.0     +     fmag0b(iv_nu0)*ryLB(j))
          END DO
          END DO
          END DO
 
          DO j=my-n6+2,my1
          DO k=  1    ,kd_nu0
          DO i=  1    ,mx
            f_LBC0(i,j,k) = f_LBC0(i,j,k)
     .      /(1.0     +     fmag0h(iv_nu0)*ryLB(j))
          END DO
          END DO
          END DO
 
 
!    Zero Gradient at y LBC if fmag0b,h = 0
!    --------------------------------------
 
          DO k=  1 ,kd_nu0
          DO i=  1 ,mx
            f_LBC0(i, 1,k) = (1.-fmag0b(iv_nu0))*f_LBC0(i,jp11,k) ! 0-grad.
!    .                     +     fmag0b(iv_nu0) *f_LBC0(i,   1,k) ! 0 at y-LB
            f_LBC0(i,my,k) = (1.-fmag0h(iv_nu0))*f_LBC0(i, my1,k) ! 0-grad.
!    .                     +     fmag0h(iv_nu0) *f_LBC0(i, my ,k) ! 0 at y-LB
          END DO
          END DO
 
 
!    Nudging to zero in in the lateral Sponge
!    ----------------------------------------
 
          DO j=  2 ,n6-1
          DO k=  1 ,kd_nu0
          DO i=ip11,mx1
            WKxyz2(i,j,k) =f_LBC0(  i,j,k) + rxfact*ryLB(j)
     .                  *( f_LBC0(i,j+1,k)  +f_LBC0(i,j-1,k)
     .                   - f_LBC0(i,j  ,k)  -f_LBC0(i,j  ,k))
          END DO
          END DO
          END DO
 
          DO j=  2 ,n6-1
          DO k=  1 ,kd_nu0
          DO i=ip11,mx1
                f_LBC0(i,j,k) = WKxyz2(i,j,k)
          END DO
          END DO
          END DO
 
          DO j=my-n6+2,my1
          DO k=  1    ,kd_nu0
          DO i=ip11   ,mx1
            WKxyz2(i,j,k) =f_LBC0(  i,j,k) + rxfact*ryLB(j)
     .                  *( f_LBC0(i,j+1,k)  +f_LBC0(i,j-1,k)
     .                   - f_LBC0(i,j  ,k)  -f_LBC0(i,j  ,k))
          END DO
          END DO
          END DO
 
          DO j=my-n6+2,my1
          DO k=  1    ,kd_nu0
          DO i=ip11   ,mx1
            f_LBC0( i, j,k) = WKxyz2(i,j,k)
          END DO
          END DO
          END DO
 
 
!    Zero Gradient at y LBC if fmag0b,h = 0
!    --------------------------------------
 
          DO k=  1 ,kd_nu0
          DO j=jp11,my1
            f_LBC0(i, 1,k) = (1.-fmag0b(iv_nu0))*f_LBC0(i,jp11,k) ! 0-grad.
!    .                     +     fmag0b(iv_nu0) *f_LBC0(i,   1,k) ! 0 at y-LB
            f_LBC0(i,my,k) = (1.-fmag0h(iv_nu0))*f_LBC0(i, my1,k) ! 0-grad.
!    .                     +     fmag0h(iv_nu0) *f_LBC0(i, my ,k) ! 0 at y-LB
          END DO
          END DO
 
c #OB     DO k=  1 ,kd_nu0
c #OB       f_LBC0( 1, 1,k) = (f_LBC0( 1,jp11,k)+f_LBC0(ip11, 1,k))*0.5
c #OB       f_LBC0(mx, 1,k) = (f_LBC0(mx,jp11,k)+f_LBC0( mx1, 1,k))*0.5
c #OB       f_LBC0( 1,my,k) = (f_LBC0( 1, my1,k)+f_LBC0(ip11,my,k))*0.5
c #OB       f_LBC0(mx,my,k) = (f_LBC0(mx, my1,k)+f_LBC0( mx1,my,k))*0.5
c #OB     END DO
 
      END IF
 
 
C +--Work Arrays Reset
C +  =================
 
          DO k=1,mz
          DO j=1,my
          DO i=1,mx
            WKxyz1(i,j,k) = 0.0
            WKxyz2(i,j,k) = 0.0
          END DO
          END DO
          END DO
 
      return
      end
