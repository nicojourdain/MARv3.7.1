 
 
      subroutine time_steps
 
C +------------------------------------------------------------------------+
C | MAR INPUT                                            01-11-2016    MAR |
C +------------------------------------------------------------------------+
 
      IMPLICIT NONE
 
      character(8)  :: date_mar
      character(10) :: time_mar
 
      real,parameter :: dt_base=60 ! second
c     lower dt_base is, more stable MAR is but slower MAR is
 
 
      include 'MARdim.inc'
      include 'MAR_GE.inc'
      include 'MARgrd.inc'
      include 'MARphy.inc'
      include 'MARCTR.inc'
 
      ! in summer or full year
 
      ! characteristic time of radCEP (3600s)
      jtRadi2 = max(     900/int(dt),jtRadi2) ! 15 min
      jtRadi2 = min(3  *3600/int(dt),jtRadi2) ! 3h
      jtRadi  = 1*jtRadi2
 
      ! characteristic time of sisvat (60-80s)
      ntPhys  = nint(dt/dt_base)
      if(dt>dt_base) ntPhys=max(2,ntPhys)
 
      ! characteristic time of Hydmic (60-80s)
      ntHyd   = nint(dt/dt_base)
      if(dt>dt_base) ntHyd=max(2,ntHyd)
 
      ! subgrid scale steps (turbulence and convection) (60-80s)
      ntDiff  = nint(dt/dt_base)
      if(dt>dt_base) ntDiff=max(2,ntDiff)
 
!     -----------------------------------------------------------------
 
      ! Arctic
      if(GElatr(1,1)/degrad>70.or.GElatr(mx,my)/degrad>70) then
       if(mmarGE>=9.and.mmarGE<=10) jtRadi = 2*jtRadi2
       if(mmarGE>=3.and.mmarGE<=4 ) jtRadi = 2*jtRadi2
       if(mmarGE>=11.or.mmarGE<=2 ) jtRadi = 3*jtRadi2
      endif
 
!     -----------------------------------------------------------------
 
 
      ! Antarctic
cc#AC if(GElatr(1,1)/degrad<-40.and.GElatr(mx,my)/degrad<-40) then
cc#AC  if(mmarGE>=9.and.mmarGE<=10) jtRadi  = 2*jtRadi2  ! spring
cc#AC  if(mmarGE>=3.and.mmarGE<=4 ) jtRadi  = 2*jtRadi2  ! fall
cc#AC  if(mmarGE>=5.and.mmarGE<=8 ) jtRadi  = 3*jtRadi2  ! winter (no sun)
cc#AC endif
 
!     -----------------------------------------------------------------
 
c#BS  ntPhys = max(2,ntPhys)
 
      ntPhys = min(4,max(1,ntPhys))
      ntHyd  = min(4,max(1,ntHyd))
      ntDiff = min(4,max(1,ntDiff))
 
      dtPhys = dt/real(ntPhys)
      dtHyd  = dt/real(ntHyd)
      dtDiff = dt/real(ntDiff)
 
      dtRadi = dt*jtRadi
 
!     -----------------------------------------------------------------
 
       call date_and_time(DATE=date_mar)
       call date_and_time(TIME=time_mar)
 
       write(6,400) jdarGE,mmarGE,iyrrGE,jhurGE,minuGE,jsecGE
 
  400  format (' MAR  time : ',i2,'/',i2,'/',i4,' ',i2,':',i2,':',i2)
 
       write(6,*) "Real time : "//date_mar(5:6)//"/"
     .      //date_mar(7:8) //"/"//date_mar(1:4)//" "//time_mar(1:2)
     .      //":"//time_mar(3:4)//":"//time_mar(5:6)
 
       write(6,401) dt,dtHyd,dtRadi
  401  format (' Step time : dt=',f5.1,", dtHyd=",f5.1,
     .          ", dtRadi=",f6.0,' s')
 
      end
