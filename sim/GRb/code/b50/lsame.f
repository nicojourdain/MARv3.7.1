      LOGICAL FUNCTION LSAME(CH1,CH2)
C +
C +----------------------------------------+
C |   cfr. Hubert Gallee, 30 octobre 92    |
C +----------------------------------------+
C +
      CHARACTER CH1,CH2
      IF (CH1.EQ.CH2)  THEN
       LSAME = .TRUE.
      ELSE
       LSAME = .FALSE.
      END IF
      RETURN
      END
