      subroutine tlat(tlat_a,tlat_b,tlat_c,tlat_d,tlat_p,tlat_q,nx,n
     .               ,tlat_x)
C +
C +------------------------------------------------------------------------+
C | MAR DYNAMICS FILTER                                    20-09-2001  MAR |
C |   SubRoutine tlat  uses the Gaussian Elimination Algorithm             |
C |    (e.g. Pielke (1984), pp.302--303)                                   |
C |    (needed to solve the implicit scheme developped for filtering)      |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:   tlat_a,tlat_b,tlat_c: tri-diagional matrix coefficients     |
C |   ^^^^^    tlat_d              : tri-diagional matrix independent term |
C |            tlat_p,tlat_q       : working          variables            |
C |            n                   : dimension of the variables            |
C |            ix                  : switch                                |
C |            tlat_x              : variable to solve                     |
C |                                                                        |
C |   OUTPUT:  tlat_x                                                      |
C |   ^^^^^^                                                               |
C +------------------------------------------------------------------------+
C +
      IMPLICIT NONE
C +
      integer  nx,n
      real     tlat_a(nx),tlat_b(nx),tlat_c(nx),tlat_d(nx)
      real     tlat_x(nx),tlat_p(nx),tlat_q(nx)
C +
      integer  k ,l
      integer  ix
C +
      data     ix/0/
C +
C +
C +--Forward  Sweep
C +  ==============
C +
      IF (ix.ne.1)                                                  THEN
          tlat_p(1)= tlat_b(1)
          tlat_q(1)=-tlat_c(1)/tlat_p(1)
        DO k=2,n
          tlat_p(k)= tlat_a(k)*tlat_q(k-1)+tlat_b(k)
          tlat_q(k)=-tlat_c(k)/tlat_p(k)
        END DO
      END IF
C +
          tlat_x(1)= tlat_d(1)/tlat_p(1)
        DO k=2,n
          tlat_x(k)=(tlat_d(k)-tlat_a(k)  *tlat_x(k-1))/tlat_p(k)
        END DO
C +
C +
C +--Backward Sweep
C +  ==============
C +
        DO l=2,n
          k=n-l+1
          tlat_x(k)=tlat_q(k)*tlat_x(k+1)+tlat_x(k)
        END DO
C +
      return
      end
