 
 
      subroutine SISVAT_SIc
! #m2.                     (SIvAcr)
 
C +------------------------------------------------------------------------+
C | MAR          SISVAT_SIc                                26-09-2006  MAR |
C |   SubRoutine SISVAT_SIc treats Sea-Ice and Ocean Latent Heat Exchanges |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:   TaT_SV   : SBL Top    Temperature                       [K] |
C |   ^^^^^    isnoSV   : total Nb of Ice/Snow Layers                  [-] |
C |            LSmask   : Land-Sea   Mask                              [-] |
C |            dsn_SV   : Snow  Intensity                      [mm w.e./s] |
C |                                                                        |
C |   INPUT /  TsisSV   : Snow/Ice/Soil-Water Temperature              [K] |
C |   OUTPUT:  eta_SV   : Soil/Snow Water   Content                [m3/m3] |
C |   ^^^^^^   dzsnSV   : Snow Layer        Thickness                  [m] |
C |                                                                        |
C |   OUTPUT:  HFraSV   : Frazil            Thickness                  [m] |
C |   ^^^^^^                                                               |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--General Variables
C +  =================
 
      include 'MARphy.inc'
 
      include 'MAR_SV.inc'
      include 'MARdSV.inc'
 
 
C +--INPUT/OUTPUT
C +  ------------
 
      include  "MARxSV.inc"
!$OMP threadprivate(/xSISVAT_I/,/xSISVAT_R/)
 
! #m2 real      SIvAcr(klonv)                 ! Sea-Ice      Vertical Acretion
 
 
C +--Local Variables
C +  ===============
 
      integer             ikl   ,isn
      real                OCN_OK
      real                SIceOK
      real                SIcFrz
      real                Twat_n
      real                Crodzw,Lro__I
      common/SISVAT_SIc_R/Crodzw,Lro__I
      logical             SIcINI
      common/SISVAT_SIc_L/SIcINI
 
      real                SalIce              ! Sea-Ice   Salinity
      real                SalWat              ! Sea-Water Salinity
 
 
C +--DATA
C +  ====
 
      data                SalIce /10./        ! Sea-Ice   Salinity
      data                SalWat /35./        ! Sea-Water Salinity
C +...                    Typical Salinities in Terra Nova Bay
C +                       (Bromwich and Kurtz,   1984, JGR, p.3568;
C +                        Cavalieri and Martin, 1985,      p. 248)
 
 
C +--Initialisation
C +  ==============
 
      IF (.NOT.SIcINI)                                              THEN
               SIcINI =  .true.
               Crodzw =  C__Wat*ro_Wat *          dz_dSV(0)   ! [J/m2/K]
               Lro__I =  Lf_H2O*ro_Ice *(1.-1.e-3*SalIce      ! [J/m3]
     .                 -(SalIce/SalWat)*(1.-1.e-3*SalWat) )   !
! #e1          Lro__I =  Lf_H2O*ro_Ice
      END IF
 
 
C +--Snow Fall cools Sea Water
C +  =========================
 
      DO ikl=1,klonv
        OCN_OK        =  (1   -LSmask(ikl)   )                ! Free Ocean
     .             *max(0,1   -isnoSV(ikl)   )                !
c #IA   TsisSV(ikl,0) =        TsisSV(ikl,0)                  ! [K]
c #IA. -OCN_OK*(Cn_dSV*(TfSnow-TaT_SV(ikl)   )                ! [J/kg]
c #IA.         +Lf_H2O*(1.    -eta_SV(ikl,0)))                ! [J/kg]
c #IA.        * dsn_SV(ikl)   *dt__SV          / Crodzw       ! [kg/m2]
 
 
C +--Sea-Ice Formation
C +  =================
 
c #IA   Twat_n      =      max(TsisSV(ikl,0  )  ,tfrwat)      ! [K]
c #IA   SIcFrz      =  (Twat_n-TsisSV(ikl,0  ) )*Crodzw/Lro__I! [m]
c #IA.                              * 0.75
C +***  Hibler (1984), Ocean Heat Flux: 25% of cooling (ANTARCTIC Ocean)
C +    (Hansen and Takahashi Eds)
C +     Geophys. Monogr. 29, M. Ewing Vol. 5, AGU, p. 241
 
 
C +--Frazil  Formation
C +  -----------------
 
c #IA   HFraSV(ikl) =          SIcFrz           *OCN_OK
 
 
C +--Growth of the Sea-Ice First Ice Floe
C +  ------------------------------------
 
c #IA   SIceOK        =  (1   -LSmask(ikl)     )              ! Ice Cover.Ocean
c #IA.             *min(  1   ,isnoSV(ikl)     )              !
c #IA   dzsnSV(ikl,1) =        dzsnSV(ikl,1)                  ! Vertical Acret.
c #IA.                +        SIcFrz           *SIceOK       !
 
 
C +--Diagnostic of the Surface Mass Balance
C +  --------------------------------------
 
! #m2   SIvAcr(ikl) = ro_Ice*SIcFrz     *(OCN_OK+SIceOK)
! #m2.              - dt__SV*dsn_SV(ikl)* OCN_OK
 
 
C +--Water Fluxes Update
C +  -------------------
 
        RnofSV(ikl) =          RnofSV(ikl)
     .              +          dsn_SV(ikl) *     OCN_OK
        dsn_SV(ikl) =          dsn_SV(ikl) * (1.-OCN_OK)
 
      END DO
 
      return
      end
