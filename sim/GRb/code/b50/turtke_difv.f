 
 
      subroutine TURtke_difv(dt_dif,alphaD)
 
C +------------------------------------------------------------------------+
C | MAR TURBULENCE (TKE)                               Fri 11-03-2011  MAR |
C |   SubRoutine TURtke_difv includes TKE Vertical Turbulence Contribution |
C |      to Turbulent Kinetic Energy (ect_TE) and Dissipation (eps_TE)     |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |  INPUT: TUkvm(mx,my,mz): Vertical Turbulent Coeffic.(momentum) [m2/s2] |
C |  ^^^^^^                                                                |
C |                                                                        |
C |  INPUT / OUTPUT: The Vertical Turbulent Fluxes are included for:       |
C |  ^^^^^^^^^^^^^^                                                        |
C |       a) Turbulent Kinetic Energy             ect_TE(mx,my,mz) [m2/s2] |
C |       b) Turbulent Kinetic Energy Dissipation eps_TE(mx,my,mz) [m2/s3] |
C |                                                                        |
C | #OPTIONS: #De: Dirichlet Type Top Boundary Condit. for ect_TE & eps_TE |
C | #^^^^^^^^                                                              |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_DY.inc'
 
c #De include 'MAR_DI.inc'
 
      include 'MAR_TE.inc'
      include 'MAR_TU.inc'
c _PE include 'MARpen.inc'
c #PE include 'MARpen.inc'
 
      include 'MAR_WK.inc'
 
      real     dt_dif
 
 
C +--Local  Variables
C +  ================
 
      integer  k1
      real     sige  ,sigk
      real     sige0 ,sigk0 ,sigek ,alpha ,beta  ,ab    ,alphaD
 
 
C +--DATA
C +  ====
 
      data sige /0.862e0/
C +...Bintanja , 2000, BLM (95),milieu p. 355 : 1/sig_e = 1./1.16 = 0.862
c #PD data sige0/0.420e0/
C +...Duynkerke, 1988, JAS (45), end a. p.868 : 1/sig_e = 1./2.38 = 0.420
c #KI data sige0/0.769e0/
C +...Kitada   , 1987, BLM (41),        p.220 : 1/sig_e = 1./1.30 = 0.769
c #Kl data sige0/0.420e0/
C +...For TKE Closure (Therry and Lacarrere, 1983)
 
      data sigk /1.000e0/
C +...Duynkerke, 1988, JAS (45), end a. p.868 : 1/sig_k  = 1./1.00=1.000
c #KI data sigk0/1.000e0/
C +...Kitada   , 1987, BLM (41),        p.220 : 1/sig_k  = 1./1.00=1.000
c #Kl data sigk0/1.200e0/
C +...Schayes and Thunis, Contribution 60 Inst.Astr.Geoph.(1990) p.6
 
c #PD sige  = sige0
c #KI sige  = sige0
c #Kl sige  = sige0
      sigek = sige / sigk
 
 
C +--Parameters for the Numerical Scheme of Vertical Turbulent Transport
C +  ===================================================================
 
      alpha = alphaD            ! Expliciteness := 0 (positive definite)
      beta  = 1.00-alpha        ! Impliciteness
      ab    = alpha/beta        !
 
 
C +--Work Arrays Reset
C +  =================
 
      DO k=   1,mz
      DO j=   1,my
      DO i=   1,mx
        WKxyz1(i,j,k) =0.0
        WKxyz2(i,j,k) =0.0
        WKxyz3(i,j,k) =0.0
      END DO
      END DO
      END DO
 
 
C +--Vertical Diffusion of Turbulent Kinetic Energy
C +  ==============================================
 
 
C +--Tridiagonal Matrix Coefficients - ect_TE
C +  ----------------------------------------
 
        DO k=mmz2,1,-1
 
        DO j=   1,my
        DO i=   1,mx
          WKxyz1(i,j,k)=-gravi2*(TUkvm (i,j,k)+TUkvm (i,j,k+1)
c _PE.                          +pente3(i,j,k)+pente3(i,j,k+1)
c #PE.                          +pente3(i,j,k)+pente3(i,j,k+1)
     .                          )*0.50*beta*sigk
     .                         * romiDY(i,j,k)*rolvDY(i,j,k)
     .                         /(pstDY2(i,j)  *dsigm1(k) *dsig_1(k))
        END DO
        END DO
 
        DO j=   1,my
        DO i=   1,mx
          WKxyz3(i,j,kp1(k)) = WKxyz1(i,j,k) *dsigm1(k)/dsigm1(kp1(k))
     .                                  /rolvDY(i,j,k)*rolvDY(i,j,k+1)
        END DO
        END DO
 
        END DO
 
        DO k=   1,mmz1
        DO j=   1,my
        DO i=   1,mx
          WKxyz1(i,j,k) =      WKxyz1(i,j,k) * dt_dif
          WKxyz3(i,j,k) =      WKxyz3(i,j,k) * dt_dif
          WKxyz2(i,j,k) = 1.0 -WKxyz3(i,j,k) -WKxyz1(i,j,k)
        END DO
        END DO
        END DO
 
 
C +--Second Member of the Tridiagonal System - ect_TE
C +  ------------------------------------------------
 
        DO j=   1,my
        DO i=   1,mx
          WKxyz4(i,j,1) =
     .    WKxyz1(i,j,1) *ab*(ect_TE(i,j,1)-ect_TE(i,j,kp1(1)))
c #De     WKxyz1(i,j,1) = 0.0
c #De     WKxyz2(i,j,1) = 1.0
c #De     WKxyz4(i,j,1) = ect_DI(i,j)
        END DO
        END DO
 
        DO k=kp1(1),mmz2
        DO j=   1,my
        DO i=   1,mx
          WKxyz4(i,j,k) =
     .    WKxyz1(i,j,k) *ab*(ect_TE(i,j,k)-ect_TE(i,j,kp1(k)))
     .   -WKxyz3(i,j,k) *ab*(ect_TE(i,j,km1(k))-ect_TE(i,j,k))
        END DO
        END DO
        END DO
 
        DO j=   1,my
        DO i=   1,mx
          WKxyz4(i,j,mmz1)=-(alpha*ect_TE(i,j,mmz1)-ect_TE(i,j,mz))
     .                 *gravi2*(TUkvm (i,j,mmz1)+TUkvm (i,j,mmz2)
c _PE.                         +pente3(i,j,mmz1)+pente3(i,j,mmz2)
c #PE.                         +pente3(i,j,mmz1)+pente3(i,j,mmz2)
     .                                                           )*0.50
     .                        * romiDY(i,j,mmz1)*romiDY(i,j,mmz1)
     .                        /(pstDY2(i,j)*dsigm1(mmz1)*dsig_1(mmz1))
     .   -WKxyz3(i,j,mmz1) *ab*(ect_TE(i,j,mmz2)-ect_TE(i,j,mmz1))
        END DO
        END DO
 
 
C +--Tridiagonal Matrix Inversion - ect_TE
C +  -------------------------------------
 
             k1= 1
c #De        k1= 2
        DO k=k1,mz
        DO j=   1,my
        DO i=   1,mx
          WKxyz4(i,j,k)    = WKxyz4(i,j,k) + ect_TE(i,j,k)
        END DO
        END DO
        END DO
 
C +         ************
       call MARgz_1mx1my(1,mmz1)
C +         ************
 
        DO k=1,mmz1
        DO j=   1,my
        DO i=   1,mx
          tranTE(i,j,k) = tranTE(i,j,k) +(WKxyz7(i,j,k)-ect_TE(i,j,k))
     .                                   /dt_dif
          ect_TE(i,j,k) = WKxyz7(i,j,k)
        END DO
        END DO
        END DO
 
 
C +--Vertical Diffusion of Dissipation
C +  =================================
 
 
C +--Update Tridiagonal Matrix Coefficients - eps_TE
C +  -----------------------------------------------
 
        DO k=1,mmz1
        DO j=   1,my
        DO i=   1,mx
          WKxyz1(i,j,k) =       WKxyz1(i,j,k) * sigek
          WKxyz3(i,j,k) =       WKxyz3(i,j,k) * sigek
          WKxyz2(i,j,k) = 1.0 - WKxyz3(i,j,k) - WKxyz1(i,j,k)
        END DO
        END DO
        END DO
 
 
C +--Second Member of the Tridiagonal System - eps_TE
C +  ------------------------------------------------
 
        DO j=   1,my
        DO i=   1,mx
          WKxyz4(i,j,1) =
     .    WKxyz1(i,j,1) *ab*(eps_TE(i,j,1)-eps_TE(i,j,kp1(1)))
c #De     WKxyz1(i,j,1) = 0.0
c #De     WKxyz2(i,j,1) = 1.0
c #De     WKxyz4(i,j,1) = eps_DI(i,j)
        END DO
        END DO
 
        DO k=kp1(1),mmz2
        DO j=   1,my
        DO i=   1,mx
          WKxyz4(i,j,k) =
     .    WKxyz1(i,j,k) *ab*(eps_TE(i,j,k)-eps_TE(i,j,kp1(k)))
     .   -WKxyz3(i,j,k) *ab*(eps_TE(i,j,km1(k))-eps_TE(i,j,k))
        END DO
        END DO
        END DO
 
        DO j=   1,my
        DO i=   1,mx
          WKxyz4(i,j,mmz1)=-(alpha*eps_TE(i,j,mmz1)-eps_TE(i,j,mz))
     .                 *gravi2*(TUkvm (i,j,mmz1)+TUkvm (i,j,mmz2)
c _PE.                         +pente3(i,j,mmz1)+pente3(i,j,mmz2)
c #PE.                         +pente3(i,j,mmz1)+pente3(i,j,mmz2)
     .                                                           )*0.50
     .                        * romiDY(i,j,mmz1)*romiDY(i,j,mmz1)
     .                        /(pstDY2(i,j)*dsigm1(mmz1)*dsig_1(mmz1))
     .   -WKxyz3(i,j,mmz1) *ab*(eps_TE(i,j,mmz2)-eps_TE(i,j,mmz1))
        END DO
        END DO
 
 
C +--Tridiagonal Matrix Inversion - eps_TE
C +  -------------------------------------
 
             k1= 1
c #De        k1= 2
        DO k=k1,mz
        DO j=   1,my
        DO i=   1,mx
          WKxyz4(i,j,k)    = WKxyz4(i,j,k) + eps_TE(i,j,k)
        END DO
        END DO
        END DO
 
C +         ************
       call MARgz_1mx1my(1,mmz1)
C +         ************
 
        DO k=1,mmz1
        DO j=   1,my
        DO i=   1,mx
          eps_TE(i,j,k) = WKxyz7(i,j,k)
        END DO
        END DO
        END DO
 
 
C +--Work Arrays Reset
C +  =================
 
      DO k=1,mz
      DO j=   1,my
      DO i=   1,mx
        WKxyz1(i,j,k) = 0.00
        WKxyz2(i,j,k) = 0.00
        WKxyz3(i,j,k) = 0.00
        WKxyz4(i,j,k) = 0.00
        WKxyz7(i,j,k) = 0.00
      END DO
      END DO
      END DO
 
      return
      end
