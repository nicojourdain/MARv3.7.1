 
 
      subroutine TURtke_advh(dt_dif)
 
C +------------------------------------------------------------------------+
C | MAR TURBULENCE (TKE)                                   14-09-2001  MAR |
C |   SubRoutine TURtke_advh includes TKE Horizontal Advection Contribution|
C |              solved by a 1st Order Accurate in Space Upstream Scheme   |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |  INPUT         :  dt_dif: Local Time Step                          (s) |
C |  ^^^^^^^^^^^^^^                                                        |
C |                                                                        |
C |  INPUT / OUTPUT: The Vertical Turbulent Fluxes are included for:       |
C |  ^^^^^^^^^^^^^^                                                        |
C |       a) Turbulent Kinetic Energy             ect_TE(mx,my,mz) [m2/s2] |
C |       b) Turbulent Kinetic Energy Dissipation eps_TE(mx,my,mz) [m2/s3] |
C |                                                                        |
C |  REMARK : The Advected Variables Lateral Boundary Conditions           |
C |  ^^^^^^^^ are Fixed Implicitely                                        |
C |            1)  inflow => no change                                     |
C |            2) outflow => advected Value                                |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_DY.inc'
      include 'MAR_TE.inc'
 
      include 'MAR_WK.inc'
 
      real     dt_dif
 
 
C +--Local  Variables
C +  ================
 
      real     dti   ,dtxe  ,dtye  ,tran
 
 
C +--Parameters
C +  ==========
 
      dti = 1.0 /dt_dif
      dtxe= dt_dif / dx
      dtye= dt_dif / dy
 
 
C +--Courant Number
C +  ==============
 
      DO   k=1,mmz1
      DO   j=1,my
      DO   i=1,mx
        WKxyz1(i,j,k)=-max(zero,demi*(uairDY(i,j,k)+uairDY(i,j,kp1(k))))
     .               *dtxe
        WKxyz2(i,j,k)=-min(zero,demi*(uairDY(i,j,k)+uairDY(i,j,kp1(k))))
     .               *dtxe
        WKxyz3(i,j,k)=-max(zero,demi*(vairDY(i,j,k)+vairDY(i,j,kp1(k))))
     .               *dtye
        WKxyz4(i,j,k)=-min(zero,demi*(vairDY(i,j,k)+vairDY(i,j,kp1(k))))
     .               *dtye
C +...Velocities V are computed in the layers (i.e. at k+1/2)
C +   Interpolation based on V=0 at the Surface (very approximative)
 
      END DO
      END DO
      END DO
 
 
C +--Advection (x-Direction)
C +  =======================
 
        DO   k=1,mmz1
        DO   i=ip11,mx1
        DO   j=jp11,my1
          tran         =WKxyz1(i,j,k)*(ect_TE(i,j,k)-ect_TE(im1(i),j,k))
     .                 +WKxyz2(i,j,k)*(ect_TE(ip1(i),j,k)-ect_TE(i,j,k))
        tranTE(i,j,k)=tranTE(i,j,k) + tran * dti
        ect_TE(i,j,k)=ect_TE(i,j,k) + tran
        eps_TE(i,j,k)=eps_TE(i,j,k)
     .                 +WKxyz1(i,j,k)*(eps_TE(i,j,k)-eps_TE(im1(i),j,k))
     .                 +WKxyz2(i,j,k)*(eps_TE(ip1(i),j,k)-eps_TE(i,j,k))
        END DO
        END DO
        END DO
 
 
C +--Advection (y-Direction)
C +  =======================
 
      IF (mmy.gt.1)                                               THEN
        DO  k=1,mmz1
        DO  j=jp11,my1
        DO  i=ip11,mx1
        tran         =WKxyz3(i,j,k)*(ect_TE(i,j,k)-ect_TE(i,jm1(j),k))
     .               +WKxyz4(i,j,k)*(ect_TE(i,jp1(j),k)-ect_TE(i,j,k))
        tranTE(i,j,k)=tranTE(i,j,k) + tran * dti
        ect_TE(i,j,k)=ect_TE(i,j,k) + tran
        eps_TE(i,j,k)=eps_TE(i,j,k)
     .               +WKxyz3(i,j,k)*(eps_TE(i,j,k)-eps_TE(i,jm1(j),k))
     .               +WKxyz4(i,j,k)*(eps_TE(i,jp1(j),k)-eps_TE(i,j,k))
        END DO
        END DO
        END DO
      END IF
 
 
C +--Work Arrays Reset
C +  =================
 
      DO k=1,mz
      DO j=1,my
      DO i=1,mx
        WKxyz1(i,j,k) = 0.0
        WKxyz2(i,j,k) = 0.0
        WKxyz3(i,j,k) = 0.0
        WKxyz4(i,j,k) = 0.0
      END DO
      END DO
      END DO
 
      return
      end
