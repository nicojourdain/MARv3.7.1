 
 
      subroutine GRDgeo
 
C +------------------------------------------------------------------------+
C | MAR GRID                                           Tue 17-11-2009  MAR |
C |   SubRoutine GRDgeo computes the Latitudes, Longitudes and             |
C |                              the Time Zone of each Grid Point          |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |    INPUT: abs(maptyp)=0: Polar   Stereogr. Project. (SOUTH HEMISPHERE) |
C |    ^^^^^              1: Oblique Stereogr. Project. (ALL    LATITUDES  |
C |                                                                        |
C |    INPUT (via common block)                                            |
C |    ^^^^^  imez,jmez    : Indices of the MAR Domain Center              |
C |           GEddxx       : (2-D): x-Axis      Direction                  |
C |                          (3-D): South-North Direction along            |
C |                                 90E, 180E, 270E or 360E Meridians      |
C |           GElat0       : Latitude  of (0,0) in  MAR              (deg) |
C |           GElon0       : Longitude of (0,0) in  MAR              (deg) |
C |                                                                        |
C |   OUTPUT (via common block)                                            |
C |   ^^^^^^  GElatr(mx,my): Latitude  of the (x,y) MAR coordinate   (rad) |
C |           GElonh(mx,my): Longitude of the (x,y) MAR coordinate     (h) |
C |           itizGE(mx,my): Time Zone                                     |
C |           fcorDY(mx,my): Coriolis Parameter (may be variable)          |
C |                                                                        |
C |   MODIF.  3 Nov 2009   : Map Scaling Factor SFm_DY computed only       |
C |   ^^^^^                  for a domain which is North/South Pole        |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
 
      include 'MAR_DY.inc'
 
 
C +--Local  Variables
C +  ================
 
      integer         i1_gg ,i2_gg ,id10  ,jd10
      real            GElon ,GElat ,RadLat,clat_s
      real     argrot,cosrot,sinrot
      real            xxmar0,yymar0,x0,y0
      real     ddista,xdista,ydista
 
 
C +--GEOGRAPHIC Coordinates
C +  ======================
 
      IF (.NOT.geoNST)                                            THEN
 
C +--1-D and 2-D Cases
C +  -----------------
 
        IF (mmy.eq.1)                                             THEN
 
            argrot = (GEddxx-90.0)*degrad
            cosrot =  cos(argrot)
            sinrot =  sin(argrot)
 
          DO j=1,my
          DO i=1,mx
            xxmar0 = cosrot*(i-imez)*dx + sinrot*(j-jmez)*dx
            yymar0 = cosrot*(j-jmez)*dx - sinrot*(i-imez)*dx
 
C +         ***********
            call GRDstr(xxmar0,yymar0,GElon0,GElat0,GElon,GElat,GEtrue)
C +         ***********
 
            GElatr(i,j) =  GElat
            GElonh(i,j) =  GElon
 
          END DO
          END DO
 
 
C +--3-D         Cases
C +  -----------------
 
        ELSE
 
C +- ANTARCTICA (Polar   Stereographic Projection is assumed)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #AP     if (maptyp.eq.0)                                        THEN
c #AP        call StereoSouth_inverse(GElon0,GElat0,GEddxx,x0,y0)
c #AP        do j=1,my
c #AP        do i=1,mx
c #AP          xxmar0  = (i-imez)*dx/1000.+x0
c #AP          yymar0  = (j-jmez)*dy/1000.+y0
c #AP
C +                 ***********
c #AP          call StereoSouth(xxmar0,yymar0,GEddxx,GElon,GElat)
C +                 ***********
c #AP
c #AP          GElonh(i,j) =  GElon / 15.
C +   ...      Conversion: degrees->hour
c #AP          GElatr(i,j) =  GElat * degrad
C +   ...      Conversion: rad ->degrees
C +
c #AP        enddo
c #AP        enddo
c #AP     end if
 
          IF (maptyp.eq.0)                                       THEN
              ddista = earthr * 2.0 * tan((45.0+GElat0*0.50)*degrad)
              xdista = ddista       * cos((90.0-GElon0)     *degrad)
              ydista = ddista       * sin((90.0-GElon0)     *degrad)
            DO j=1,my
            DO i=1,mx
              IF (abs(GEddxx- 90.0).lt.epsi)                      THEN
                     xxmar0 = (i-imez)*dx
                     yymar0 = (j-jmez)*dy
              END IF
              IF (abs(GEddxx      ).lt.epsi)                      THEN
                     xxmar0 = (j-jmez)*dy
                     yymar0 =-(i-imez)*dx
              END IF
              IF (abs(GEddxx-270.0).lt.epsi)                      THEN
                     xxmar0 =-(i-imez)*dx
                     yymar0 =-(j-jmez)*dy
              END IF
              IF (abs(GEddxx-180.0).lt.epsi)                      THEN
                     xxmar0 =-(j-jmez)*dy
                     yymar0 = (i-imez)*dx
              END IF
 
                     xxmar0 = xxmar0 + xdista
                     yymar0 = yymar0 + ydista
 
                   ddista      =     sqrt(xxmar0*xxmar0+yymar0*yymar0)
                   GElatr(i,j) =-0.5*pi
     .                           +2.*atan(ddista*0.5   /earthr)
              IF(abs(xxmar0).gt.zero)                             THEN
                     GElonh(i,j) =     atan(yymar0       /xxmar0)
                IF  (xxmar0.lt.zero)
     .               GElonh(i,j) =                GElonh(i,j) + pi
 
                     GElonh(i,j) =   0.50 * pi - GElonh(i,j)
                 IF (GElonh(i,j).gt.        pi)
     .               GElonh(i,j) =  -2.00 * pi + GElonh(i,j)
                 IF (GElonh(i,j).lt.       -pi)
     .               GElonh(i,j) =   2.00 * pi + GElonh(i,j)
 
              ELSE
                IF   (yymar0.gt.zero)                             THEN
                     GElonh(i,j) =   0.00
                ELSE
                     GElonh(i,j) =           pi
                END IF
              END IF
C +   ...     transformation stereographic coordinates (center = South Pole)
C +   ...                 -> spherical     coordinates
 
                     GElonh(i,j) =                GElonh(i,j)   / hourad
C +   ...     Conversion:   radian       -> Hour
 
            END DO
            END DO
 
          END IF
 
C +- OTHERS     (Oblique Stereographic Projection is assumed)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          IF (abs(maptyp).eq.1)                                   THEN
 
            DO j=1,my
            DO i=1,mx
 
             argrot = (GEddxx-90.0)*degrad
             cosrot = cos(argrot)
             sinrot = sin(argrot)
             xxmar0 = cosrot*(i-imez)*dx+sinrot*(j-jmez)*dy
             yymar0 = cosrot*(j-jmez)*dy-sinrot*(i-imez)*dx
 
C +          ***********
             call GRDstr(xxmar0,yymar0,GElon0,GElat0,GElon,GElat,GEtrue)
C +          ***********
 
             GElatr(i,j) =  GElat
             GElonh(i,j) =  GElon
 
            END DO
            END DO
 
          END IF
 
        END IF
 
      END IF
 
 
C +--Sine, Cosine of Latitude
C +  ========================
 
        DO j=1,my
        DO i=1,mx
              clatGE(i,j)  =   cos(GElatr(i,j))
              slatGE(i,j)  =   sin(GElatr(i,j))
        END DO
        END DO
 
 
C +--Scaling Map Factor
C +  ==================
 
        IF (abs(GElat0).GE.90.-epsi)                                THEN
              clat_s    =   1.+sin((90.-    GEtrue)*degrad)
          DO j=1,my
          DO i=1,mx
              SFm_DY(i,j) = clat_s/( 1.+abs(slatGE(i,j)))
          END DO
          END DO
        ELSE
          DO j=1,my
          DO i=1,mx
              SFm_DY(i,j)  =   1.0
          END DO
          END DO
        END IF
 
 
C +--Numerical Equator
C +  ~~~~~~~~~~~~~~~~~
        DO j=1,my
        DO i=1,mx
          IF(abs(GElatr(i,j)).lt.        epsi)                    THEN
                 RadLat       =          epsi
                 slatGE(i,j)  =   sin(RadLat )
                 clatGE(i,j)  =   cos(RadLat )
          END IF
 
C +--Numerical North Pole
C +  ~~~~~~~~~~~~~~~~~~~~
          IF(    GElatr(i,j) .gt.demi*pi-epsi)                    THEN
                 RadLat       =  demi*pi-epsi
cXF              slatGE(i,j)  =   sin(RadLat )
cXF              clatGE(i,j)  =   cos(RadLat )
                  slatGE(i,j) =  1.
                  clatGE(i,j) =  0.
          END IF
 
C +--Numerical South Pole
C +  ~~~~~~~~~~~~~~~~~~~~
          IF(    GElatr(i,j) .lt.epsi-demi*pi)                    THEN
                 RadLat       =  epsi-demi*pi
cXF              slatGE(i,j)  =   sin(RadLat )
cXF              clatGE(i,j)  =   cos(RadLat )
                  slatGE(i,j) = -1.
                  clatGE(i,j) =  0.
          END IF
 
       END DO
       END DO
 
 
C +--Coriolis Parameter
C +  ==================
 
      DO j=1,my
      DO i=1,mx
        fcorDY(i,j) = 2.0*earthv*sin(GElatr(i   ,j   ))
c #CC   fcorDY(i,j) = 2.0*earthv*sin(GElatr(imez,jmez))
C +...  fcorDY      : Coriolis Parameter
 
      END DO
      END DO
 
 
C +--Time Zone
C +  =========
 
      DO j=1,my
      DO i=1,mx
            itizGE(i,j)    =    GElonh(i,j)
        if (itizGE(i,j).gt. 12) itizGE(i,j) = itizGE(i,j)-24
        if (itizGE(i,j).lt.-12) itizGE(i,j) = itizGE(i,j)+24
      END DO
      END DO
 
 
C +--OUTPUT
C +  ======
 
          i1_gg = imez  - 50
          i2_gg = imez  + 50
          i1_gg = max(i1_gg, 1)
          i2_gg = min(i2_gg,mx)
          id10  = 1 + min(mx-1,10)
          jd10  = 1 + min(my-1,10)
 
          write(4,990)(i,i=i1_gg,i2_gg,id10)
 990      format(/,' LATITUDES / LONGITUDES / TOPOGRAPHY:  x ->  y ^ ',
     .           /,' ===================================',/,9x,13i9)
          DO j=my,1,-jd10
            write(4,991)j,(GElatr(i,j)/degrad,i=i1_gg,i2_gg,id10)
 991        format(  i9,11f9.3)
            write(4,992)  (GElonh(i,j)*1.5d+1,i=i1_gg,i2_gg,id10)
 992        format(  9x,11f9.3)
            write(4,993)  (sh    (i,j)*1.0d-3,i=i1_gg,i2_gg,id10)
 993        format(  9x,11f9.3)
            write(4,994)  (itizGE(i,j),       i=i1_gg,i2_gg,id10)
 994        format(  9x,11i9  )
            write(4,995)  (fcorDY(i,j)       ,i=i1_gg,i2_gg,id10)
 995        format(  9x,11f9.6)
          END DO
 
      return
      end
