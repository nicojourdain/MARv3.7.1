 
 
      function qse_0D(tt,sigma,ps,ptop)
 
!--------------------------------------------------------------------------+
!                                                          Tue 30-Jun-2009 |
!     fonction qse_0D computes ECMWF saturation specific humidities        |
!                                                                          |
!--------------------------------------------------------------------------+
 
      IMPLICIT NONE
 
      real     ps    ,sigma ,ptop  ,foeew ,zcor
      real     tt    ,pp    ,qswT
      real     rkbol ,rnavo ,r     ,rmd   ,rmv
      real     rd    ,rv    ,restt ,r2es  ,r3les
      real     r3ies ,r4les ,r4ies ,retv  ,rtt
      real     qse_0D
 
 
      data rkbol  /1.380658e-23 /
      data rnavo  /6.0221367e+23/
      data rmd    /28.9644      /
      data rmv    /18.0153      /
      data restt  /611.21       /
      data r3les  /17.502       /
      data r3ies  /22.587       /
      data r4les  /32.19        /
      data r4ies  /-0.7         /
      data rtt    /273.16       /
 
           r      = rnavo*rkbol
           rd     = 1000.*r/rmd
           rv     = 1000.*r/rmv
           r2es   = restt*rd/rv
           retv   = rv/rd-1
 
           pp     = ps*sigma+ptop
 
 
           foeew  = r2es*exp((r3les*(tt-rtt))/(tt-r4les))
 
           qswT   = foeew/(1000.*pp)
           zcor   = 1./(1.-retv*qswT)
           qswT   = qswT*zcor
 
           qse_0D = qswT
 
 
! FROM ECMWF
! ----------
! #
! #   computes qsat for water , USAGE qsat T P
! #
! set -e
! if [ $1 ] ; then
!    dummy=0
! else
!    echo "computes qsat for water; USAGE: qsat T P " >&2 ; exit 2
! fi
! if [ $2 ] ; then
!    dummy=0
! else
!    echo "computes qsat for water; USAGE: qsat T P " >&2 ; exit 2
! fi
! echo $1 $2 | awk 'BEGIN { \
!                 rkbol=1.380658e-23; rnavo=6.0221367e+23; r=rnavo*rkbol; \
!                 rmd=28.9644; rmv=18.0153; rd=1000.*r/rmd; rv=1000.*r/rmv; \
!                 restt=611.21; r2es=restt*rd/rv; r3les=17.502; r3ies=22.587; \
!                 r4les=32.19; r4ies=-0.7;
!                 retv=rv/rd-1; rtt=273.16}
!                 {t=$1; p=$2; \
!                 foeew=r2es*exp((r3les*(t-rtt))/(t-r4les)) ; \
!                 qs=foeew/p ; zcor=1/(1-retv*qs);qs=qs*zcor ; print qs}'
! #
! # computes satration water vapor pressure over water as in the IFS
! # usage: qsat T P    ! T in K and P in Pa e.g. qsat 293 1e5
! #
 
      RETURN
 
      END
