 
 
      subroutine MARgz_1mx1my(k1,k2)
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS                                           02-07-2004  MAR |
C |   SubRoutine MARgz_1mx1my performs Gaussian Elimination along s-Dir.   |
C |    (e.g. Pielke (1984), pp.302--303)                                   |
C |    (needed to solve the implicit scheme developped for filtering)      |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:                      k1,k2: k Loops Limits                    |
C |   ^^^^^                                                                |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_WK.inc'
 
      integer  k1,k2
 
 
C +--Forward  Sweep
C +  ==============
 
        DO j=1,my
        DO i=1,mx
          WKxyz5(i,j,k1) = WKxyz2(i,j,k1)
          WKxyz6(i,j,k1) =-WKxyz1(i,j,k1) /WKxyz5(i,j,k1)
        END DO
        END DO
      DO   k=kp1(k1),k2
        DO j=1,my
        DO i=1,mx
          WKxyz5(i,j,k) = WKxyz3(i,j,k)   *WKxyz6(i,j,k-1)+WKxyz2(i,j,k)
          WKxyz6(i,j,k) =-WKxyz1(i,j,k)   /WKxyz5(i,j,k)
        END DO
        END DO
      END DO
        DO j=1,my
        DO i=1,mx
          WKxyz7(i,j,k1)= WKxyz4(i,j,k1)  /WKxyz5(i,j,k1)
        END DO
        END DO
      DO   k=kp1(k1),k2
        DO j=1,my
        DO i=1,mx
          WKxyz7(i,j,k) =(WKxyz4(i,j,k)   -WKxyz3(i,j,k)
     .                   *WKxyz7(i,j,k-1))/WKxyz5(i,j,k)
        END DO
        END DO
      END DO
 
 
C +--Backward Sweep
C +  ==============
 
      DO k=km1(k2),k1,-1
        DO j=1,my
        DO i=1,mx
          WKxyz7(i,j,k) =  WKxyz6(i,j,k) *WKxyz7(i,j,k+1) +WKxyz7(i,j,k)
        END DO
        END DO
      END DO
 
      return
      end
