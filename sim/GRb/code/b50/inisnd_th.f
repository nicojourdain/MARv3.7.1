      subroutine INIsnd_th(pij,ptopDY,sigmid,sigma,t_ij,q_ij)
C +
C +------------------------------------------------------------------------+
C | MAR INPUT      ATMOS                                   25-09-2001  MAR |
C |   SubRoutine INIsnd_th initializes                                     |
C |     ATMOSPHERIC TEMPERATURES and SPECIFIC HUMIDITIES vertical profiles |
C |     from sounding data (observations or academic situation)            |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:    pij -> pstDY (i,j) : Model Pressure Thickness              |
C |   ^^^^^     ptopDY               Model Pressure Top                    |
C |             sigmid               Model Layer    Interface Coordinate   |
C |             sigma                Model Level              Coordinate   |
C |                                                                        |
C |   INPUT (via common)                                                   |
C |   ^^^^^     tpSND                Sounding       Potential Temperature  |
C |                                                                        |
C |   OUTPUT:    t_ij       -> tairDY(i,j,1->mz)                           |
C |   ^^^^^^     q_ij       ->   qvDY(i,j,1->mz)                           |
C |                                                                        |
C |   CAUTION:  The Sounding must be in Hydrostatic Balance                |
C |   ^^^^^^^                                                              |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +--Global Variables
C +  ================
C +
      include 'MARphy.inc'
C +
      include 'MARdim.inc'
C +
      include 'MARSND.inc'
C +
      real      pij   ,ptopDY
      real      sigmid(mzz)
      real       sigma(mz)
C +
      real      t_ij(mz,2),q_ij(mz,2)
C +
C +
C +--Local  Variables
C +  ================
C +
      integer   k     ,ksnd  ,lsnd
      real      gra   ,prl   ,pr1   ,pr2
      real      gsnd  ,tt1   ,tt2   ,ttav
      real      gqv   ,qv1   ,qv2   ,qvav
C +
C +
C +--Scheme Initialisation
C +  =====================
C +
           gra     = -gravit / RDryAi
C +
C +--Temperature Vertical Profile
C +  ============================
C +
           k       =  mz
           ksnd    =  1
C +...     ksnd    =  1 (when pSND(mz) -> pSND(0:mz), etc...)
C +
C + - -do until
 100   CONTINUE
           prl     =  pij      *  sigma(k)     +ptopDY
        if(k  .eq.1) then
           pr1     = (pij *0.5 *  sigma(1)     +ptopDY) *10.0
        else
           pr1     = (pij      * sigmid(k)     +ptopDY) *10.0
        end if
           pr2     = (pij      * sigmid(k+1)   +ptopDY) *10.0
C +...     Factor 10 is needed for [cb] --> [mb]
C +
C + - - do until
 110    CONTINUE
        if (pSND(ksnd,nSND).lt.pr2    ) go to 111
           ksnd    =  ksnd + 1
        go to 110
 111    CONTINUE
C + - - end do
C +
           gsnd    = (tpSND(ksnd,nSND)  - tpSND(ksnd-1,nSND))
     .             / ( pSND(ksnd,nSND)  -  pSND(ksnd-1,nSND))
           tt2     =  tpSND(ksnd-1,nSND)+  gsnd*(pr2-pSND(ksnd-1,nSND))
C +
           gqv     = ( qsnd(ksnd,nSND)  -  qSND(ksnd-1,nSND))
     .             / ( pSND(ksnd,nSND)  -  pSND(ksnd-1,nSND))
           qv2     =   qSND(ksnd-1,nSND)+  gqv *(pr2-pSND(ksnd-1,nSND))
C +
        if(pSND(ksnd,nSND).ge.pr1    ) then
           ttav    =  (tt2              + tpSND(ksnd,nSND))
     .              * (pr2              -  pSND(ksnd,nSND))
           qvav    =  (qv2              +  qSND(ksnd,nSND))
     .              * (pr2              -  pSND(ksnd,nSND))
        else
           ttav    =  zero
           qvav    =  zero
        end if
C +
C + - - do until
           lsnd    =         0
 120    CONTINUE
        if(pSND(ksnd,nSND).lt.pr1    ) go to 121
           ksnd    =  ksnd + 1
           lsnd    =         1
        if(pSND(ksnd,nSND).ge.pr1    ) then
           ttav    =  ttav
     .             + (tpSND(ksnd-1,nSND)+ tpSND(ksnd,nSND))
     .              *( pSND(ksnd-1,nSND)-  pSND(ksnd,nSND))
           qvav    =  qvav
     .             + ( qSND(ksnd-1,nSND)+  qSND(ksnd,nSND))
     .              *( pSND(ksnd-1,nSND)-  pSND(ksnd,nSND))
        else
           gsnd    = (tpSND(ksnd,nSND)  - tpSND(ksnd-1,nSND))
     .             / ( pSND(ksnd,nSND)  -  pSND(ksnd-1,nSND))
           tt1     =  tpSND(ksnd-1,nSND)+  gsnd*(pr1-pSND(ksnd-1,nSND))
           ttav    =  ttav
     .             + (tpSND(ksnd-1,nSND)+   tt1)
     .              *( pSND(ksnd-1,nSND)-   pr1)
           ttav    =  ttav * 0.5 / (pr2-pr1)
C +
           gqv     = ( qSND(ksnd,nSND)  -  qSND(ksnd-1,nSND))
     .             / ( pSND(ksnd,nSND)  -  pSND(ksnd-1,nSND))
           qv1     =   qSND(ksnd-1,nSND)+  gqv *(pr1-pSND(ksnd-1,nSND))
           qvav    =  qvav
     .             + ( qSND(ksnd-1,nSND)+   qv1)
     .              *( pSND(ksnd-1,nSND)-   pr1)
           qvav    =  qvav * 0.5 / (pr2-pr1)
        end if
        go to 120
 121    CONTINUE
C + - - end do
C +
        if(lsnd.eq.0) then
           tt1     =  tpSND(ksnd-1,nSND)+  gsnd*(pr1-pSND(ksnd-1,nSND))
           ttav    = (tt2+tt1) * 0.5
C +
           qv1     =   qSND(ksnd-1,nSND)+  gqv *(pr1-pSND(ksnd-1,nSND))
           qvav    = (qv2+qv1) * 0.5
        end if
C +
C +
C +--Interpolated/Integrated Values
C +  ==============================
C +
           t_ij(k,nSND)=  ttav * prl**cap / pcap
           q_ij(k,nSND)=  qvav
C +
C +
C +--Continue Interpolation
C +  ======================
C +
       if (k         .le.1      ) go to 101
           k       =  k    - 1
       go to 100
 101   CONTINUE
C + - -end do
C +
      return
      end
