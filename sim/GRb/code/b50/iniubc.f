      subroutine INIubc(ihamr_ubc,nhamr_ubc,newubcINI)
 
C +------------------------------------------------------------------------+
C | MAR INPUT    Upper Sponge                          Thu 05-11-2009  MAR |
C |   SubRoutine INIubc is used to initialize MAR Upper Sponge Refer.State |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:  ihamr_ubc: Time Digital Filter Status                        |
C |   ^^^^^   nhamr_ubc: Time Digital Filter Set Up                        |
C |                                                                        |
C |   OUTPUT: newubcINI: (0,1) ==> (NO new ubc , new ubc)                  |
C |   ^^^^^^^                                                              |
C |                                                                        |
C |   OUTPUT: uairUB: Current                     x-Wind Speed Component   |
C |   ^^^^^^^ ua1_UB: Previous Nesting Time Step  x-Wind Speed Component   |
C |           ua2_UB: Next     Nesting Time Step  x-Wind Speed Component   |
C |           vairUB, va1_UB, va2_UB, pktaUB, pkt1UB, pkt2UB: idem         |
C |           tim1UB,tim2UB:   Times  n, n+1  of  uairUB, vairUB, pktaUB   |
C |                                                                        |
C |   CAUTION: It is assumed that tim1UB and tim2UB do not change when the |
C |   ^^^^^^^^ Variables are reassigned after the dynamical Initialization |
C |            (Reassignation => itexpe := nham => timar := timar-nham*dt) |
C |                                                                        |
C |   MODIF.   5 Nov 2009   : Map Scaling Factor SFm_DY scales (u,v) at UB |
C |   ^^^^^                   (i.e., ua2_UB, va2_UB are divided by SFm_DY) |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
 
      include 'MAR_DY.inc'
      include 'MAR_UB.inc'
 
      integer  ihamr_ubc,nhamr_ubc
      integer  newubcINI
 
 
C +--Local  Variables
C +  ================
 
      integer*8  itimUB
      real       rate
 
 
C +--Current Time
C +  ============
 
          itimUB=ou2sGE(iyrrGE,mmarGE,jdarGE,jhurGE,minuGE,jsecGE)
c #HF.           + (ihamr_ubc+nhamr_ubc)                           * idt
 
 
C +--Reinitialization of the Upper Sponge Reference State
C +  ----------------------------------------------------
 
      IF (iterun.eq.0)                                            THEN
          jdh_UB = 1
          iyr_UB = iyrrGE
          mma_UB = mmarGE
          jda_UB = jdarGE
          jhu_UB = jhurGE
          tim1UB = itimUB
          tim2UB = itimUB
       DO k=1,mzabso
       DO j=1,my
       DO i=1,mx
         ua1_UB(i,j,k) = uairUB(i,j,k)
         ua2_UB(i,j,k) = uairUB(i,j,k)
         va1_UB(i,j,k) = vairUB(i,j,k)
         va2_UB(i,j,k) = vairUB(i,j,k)
         pkt1UB(i,j,k) = pktaUB(i,j,k)
         pkt2UB(i,j,k) = pktaUB(i,j,k)
       END DO
       END DO
       END DO
 
      END IF
 
 
C +--New UBC
C +  =======
 
      IF (itimUB.gt.   tim2UB)                                    THEN
 
          tim1UB =     tim2UB
 
          write(6,6001)jda_UB,labmGE(mma_UB),iyr_UB,
     .                 jhu_UB,                      tim1UB,
     .                 jdarGE,labmGE(mmarGE),iyrrGE,
     .                 jhurGE,minuGE,        jsecGE,itimUB
 6001     format(/, '  1st UBC /',i3,'-',a3,'-',i4,i3,' ',2x,'/',2x,
     .              '   t =',i12,'s A.P.',
     .           /, '  Current /',i3,'-',a3,'-',i4,i3,':',i2,':',i2,
     .              '   t =',i12)
C +
       IF (jdh_UB.eq.0)jdh_UB = -1
       open (unit=11,status='old',form='unformatted',file='MARubc.DAT')
       rewind     11
 11    CONTINUE
       IF (jdh_UB.le.0)                                       GO TO 10
 
 
C +--UBC at nesting time step n
C +  --------------------------
 
       DO k=1,mzabso
       DO j=1,my
       DO i=1,mx
         ua1_UB(i,j,k) = ua2_UB(i,j,k)
         va1_UB(i,j,k) = va2_UB(i,j,k)
         pkt1UB(i,j,k) = pkt2UB(i,j,k)
         ua2_UB(i,j,k) = 0.d0
         va2_UB(i,j,k) = 0.d0
         pkt2UB(i,j,k) = 0.d0
       END DO
       END DO
       END DO
 
 
C +--UBC at nesting time step n+1
C +  ----------------------------
 
       read      (11) iyr_UB,mma_UB,jda_UB,jhu_UB,jdh_UB
       read      (11) ua2_UB,va2_UB,pkt2UB
 
       tim2UB=ou2sGE(iyr_UB,mma_UB,jda_UB,jhu_UB,0,0)
 
       DO k=1,mzabso
       DO j=1,my
       DO i=1,mx
            ua2_UB(i,j,k)=ua2_UB(i,j,k) / SFm_DY(i,j)
            va2_UB(i,j,k)=va2_UB(i,j,k) / SFm_DY(i,j)
       END DO
       END DO
       END DO
 
       IF(itimUB.gt.tim2UB)                                   GO TO 11
 
          write(6,6002)jda_UB,labmGE(mma_UB),iyr_UB,
     .                 jhu_UB,jdh_UB,               tim2UB
 6002     format(   '  2nd UBC /',i3,'-',a3,'-',i4,i3,' ',2x,'/(',i1,
     .              ')  t =',i12)
 
 10    CONTINUE
       close(unit=11)
 
      ELSE
c #WR     write(6,6003)jdarGE,labmGE(mmarGE),iyrrGE,
c #WR.                 jhurGE,minuGE,        jsecGE,itimUB
 6003     format(   '  Current /',i3,'-',a3,'-',i4,i3,':',i2,':',i2,
     .              '   t =',i12,'s A.P.')
      END IF
 
 
C +--Time Interpolation
C +  ==================
 
      IF            (itimUB.le.tim2UB  .and.   tim1UB.lt.tim2UB)  THEN
 
        rate = float(itimUB  - tim1UB) / float(tim2UB  - tim1UB)
        DO k=1,mzabso
        DO j=1,my
        DO i=1,mx
            uairUB(i,j,k)=ua1_UB(i,j,k) +
     .     (ua2_UB(i,j,k)-ua1_UB(i,j,k))*rate
            vairUB(i,j,k)=va1_UB(i,j,k) +
     .     (va2_UB(i,j,k)-va1_UB(i,j,k))*rate
            pktaUB(i,j,k)=pkt1UB(i,j,k) +
     .     (pkt2UB(i,j,k)-pkt1UB(i,j,k))*rate
        END DO
        END DO
        END DO
 
        newubcINI = 1
 
      ELSE
        newubcINI = 0
      END IF
 
      return
      end
