 
 
      subroutine MARgx_1mx1my
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS                                           05-07-2004  MAR |
C |   SubRoutine MARgx_1mx1my performs Gaussian Elimination along x-Dir    |
C |    (e.g. Pielke (1984), pp.302--303)                                   |
C |    (needed to solve the implicit scheme developped for filtering)      |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:                                                               |
C |   ^^^^^                                                                |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_WK.inc'
 
      integer  ix
 
      data ix /0/
 
 
C +--Forward  Sweep
C +  ==============
 
      IF (ix.ne.1)                                                  THEN
          DO k= 1,mz
          DO j= 1,my
            WKxyz5(1,j,k)= WKxyz2(1,j,k)
            WKxyz6(1,j,k)=-WKxyz1(1,j,k)/WKxyz5(1,j,k)
          END DO
          END DO
        DO   i=ip11,mx
          DO k=   1,mz
          DO j=   1,my
            WKxyz5(i,j,k)= WKxyz3(i,j,k)*WKxyz6(i-1,j,k)+WKxyz2(i,j,k)
            WKxyz6(i,j,k)=-WKxyz1(i,j,k)/WKxyz5(i  ,j,k)
          END DO
          END DO
        END DO
      END IF
 
          DO k= 1,mz
          DO j= 1,my
            WKxyz7(1,j,k)= WKxyz4(1,j,k)/WKxyz5(1,j,k)
          END DO
          END DO
 
        DO   i=ip11,mx
          DO k=   1,mz
          DO j=   1,my
            WKxyz7(i,j,k)=(WKxyz4(i,j,k)-WKxyz3(i,j,k)*WKxyz7(i-1,j,k))
     .                    /WKxyz5(i,j,k)
          END DO
          END DO
        END DO
 
 
C +--Backward Sweep
C +  ==============
 
        DO   i=mx1,1,-1
          DO k=  1,mz
          DO j=  1,my
            WKxyz7(i,j,k)=WKxyz6(i,j,k)*WKxyz7(i+1,j,k)+WKxyz7(i,j,k)
          END DO
          END DO
        END DO
 
      return
      end
