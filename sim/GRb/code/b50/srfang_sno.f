      function SRFang_sno(pst,psro,pgradt)
C +
C +------------------------------------------------------------------------+
C | MAR SURFACE                                            01-12-2001  MAR |
C |   Function SRFang_sno simulates                                        |
C |            growth rates for non dendritic grains with sphericity=0     |
C |            la croissance des grains non dendritiques et anguleux       |
C +------------------------------------------------------------------------+
C |   REFER. : Marbouty, D.     1980, J. Glaciol 26 pp. xxx--xxx           |
C |   ^^^^^^^^ (CROCUS Model, adapted to MAR at CEN by H.Gallee)           |
C |                                                                        |
C |    INPUT : pst    temperature     de                la strate de neige |
C |    ^^^^^^^ psro   masse volumique de                la strate de neige |
C |            pgradt gradient de temperature affectant la strate de neige |
C |                                                                        |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +--General Variables
C +  =================
C +
      include 'MARphy.inc'
C +
      real     SRFang_sno
      real     pst   ,psro  ,pgradt
      real     vtang1,vtang2,vtang3,vtang4,vtang5,vtang6,vtang7
      real     vtang8,vtang9,vtanga,vtangb,vtangc,vrang1,vrang2
      real     vgang1,vgang2,vgang3,vgang4,vgang5,vgang6,vgang7
      real     vgang8,vgang9,vganga,vgangb,vgangc
C +
C +
C +--DATA (Coefficient Fonction fort Gradient Marbouty)
C +  ==================================================
C +
      data  vtang1 /40.0e0/
      data  vtang2 / 6.0e0/
      data  vtang3 /22.0e0/
      data  vtang4 / 0.7e0/
      data  vtang5 / 0.3e0/
      data  vtang6 / 6.0e0/
      data  vtang7 / 1.0e0/
      data  vtang8 / 0.8e0/
      data  vtang9 /16.0e0/
      data  vtanga / 0.2e0/
      data  vtangb / 0.2e0/
      data  vtangc /18.0e0/
C +
      data  vrang1 / 0.40e0/
      data  vrang2 / 0.15e0/
C +
      data  vgang1 / 0.70e0/
      data  vgang2 / 0.25e0/
      data  vgang3 / 0.40e0/
      data  vgang4 / 0.50e0/
      data  vgang5 / 0.10e0/
      data  vgang6 / 0.15e0/
      data  vgang7 / 0.10e0/
      data  vgang8 / 0.55e0/
      data  vgang9 / 0.65e0/
      data  vganga / 0.20e0/
      data  vgangb / 0.85e0/
      data  vgangc / 0.15e0/
C +
C +
C +-- Influence de la Temperature /Temperature Influence
C +   ==================================================
C +
      if      (pst.ge.TfSnow-vtang1)                                then
        if    (pst.ge.TfSnow-vtang2)                                then
          SRFang_sno =vtang4+vtang5*(TfSnow       -pst)/vtang6
        elseif(pst.ge.TfSnow-vtang3)                                then
          SRFang_sno =vtang7-vtang8*(TfSnow-vtang2-pst)/vtang9
        else
          SRFang_sno =vtanga-vtangb*(TfSnow-vtang3-pst)/vtangc
        endif
C +
C +
C +-- Influence de la Masse Volumique /Density Influence
C +   ==================================================
C +
        if  (psro.le.vrang1)                                        then
          if(psro.gt.vrang2)
     .      SRFang_sno = SRFang_sno*(1.d0-(psro-vrang2)/(vrang1-vrang2))
C +
C +
C +-- Influence du Gradient de Temperature /Temperature Gradient Influence
C +   ====================================================================
C +
          if      (pgradt.le.vgang1)                                then
            if    (pgradt.le.vgang2)                                then
             SRFang_sno = SRFang_sno*vgang5*(pgradt-vgang6)
     .                                     /(vgang2-vgang6)
            elseif(pgradt.le.vgang3)                                then
             SRFang_sno = SRFang_sno*vgang7
     .                              +vgang8*(pgradt-vgang2)
     .                                     /(vgang3-vgang2)
            elseif(pgradt.le.vgang4)                                then
             SRFang_sno = SRFang_sno*vgang9
     .                              +vganga*(pgradt-vgang3)
     .                                     /(vgang4-vgang3)
            else
             SRFang_sno = SRFang_sno*vgangb
     .                              +vgangc*(pgradt-vgang4)
     .                                     /(vgang1-vgang4)
            endif
          endif
        else
             SRFang_sno = 0.d0
        endif
      else
             SRFang_sno = 0.d0
      endif
      return
      end
