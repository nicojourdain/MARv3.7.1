      function   SRFcri_agr(kst,knst)
C +
C +------------------------------------------------------------------------+
C | MAR SURFACE                                             01-12-2001 MAR |
C |   Function   SRFcri_agr computes minimal thickness before aggregation  |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT  :   *kst*   -  rang de la strate a examiner.                  |
C |   ^^^^^^^^   *knst*  -  nombre de strates du manteau neigeux.          |
C |                                                                        |
C |   REFER. : (CROCUS & MAR, adapted to MAR at CEN by H.Gallee)           |
C |   ^^^^^^^^                                                             |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +--General Variables
C +  =================
C +
      real    SRFcri_agr
      real    vdzag1,vdzag2,vdzag3,vdzag4,vdzag5,vdzag6,vdzag7,vdzag8
      integer nvnst4,nvnst6,nvnst5,nvnst7,kst,knst
C +
C +
C +--DATA
C +  ====
C +
      data vdzag1/ 1.00e00/, vdzag2/  0.50e00/
      data vdzag3/ 0.30e00/, vdzag4/  1.50e00/
      data vdzag5/ 4.00e00/, vdzag6/  0.05e00/
      data vdzag7/ 1.20e00/, vdzag8/  0.60e00/
C +
      data nvnst4/ 3      /, nvnst5/  2      /
C +...     Limite de nombre de strates internes au manteau neigeux
C +
      data nvnst6/10      /, nvnst7/  5      /
C +
C +
C +--Snow Layers Aggregation Criterion
C +  =================================
C +
C +--Strate sur le sol.
C +  ~~~~~~~~~~~~~~~~~
      if(kst.eq.1)then
        SRFcri_agr=vdzag1
C +
C +--Strate de surface.
C +  ~~~~~~~~~~~~~~~~~
      elseif(kst.eq.knst)then
        SRFcri_agr=vdzag2
C +
C +--Strates internes si au plus nvnst4.
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      elseif(knst.le.nvnst4+2)then
        SRFcri_agr=vdzag3
C +
C +--Strates internes si plus de nvnst4.
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      else
        if(kst.eq.nvnst5)then
          SRFcri_agr=vdzag4
        elseif(kst.lt.knst-nvnst6)then
          SRFcri_agr=vdzag5-kst*vdzag6
        elseif(kst.lt.knst-nvnst7)then
          SRFcri_agr=vdzag7
        else
          SRFcri_agr=vdzag8
        endif
      endif
C +
      return
      end
