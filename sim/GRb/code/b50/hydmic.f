 
 
      subroutine HYDmic(io1,io5,ilmm,imm,jmm,ilmez,
     .     ccni2D,ccnw2D,cfra2D,crys2D,
     .      dqi2D, dqw2D,ect_2D,enr01D,
     .     enr11D,enr21D,gplv2D,
     .     gpmi2D,hlat2D,jhlr2D,mphy2D,
     .       pk2D,pkta2D,prec2D, pst2D,
     .     pst2Dn,  qg2D,  qi2D,  qr2D,
     .       qs2D,  qv2D,qvsi2D,qvsw2D,
     .       qw2D,rain2D,rolv2D,snoh2D,
     .     snow2D,tair2D,tsrf2D,TUkv2D,
     .     uair2D,vair2D,wair2D,wat01D,
     .     wat11D,wat21D,watf1D)
 
!------------------------------------------------------------------------+
! MAR HYDROLOGIC CYCLE                              Wed 10-Feb-2012  MAR |
!   SubRoutine HYDmic computes Cloud Microphysical Processes             |
!                                                                        |
!------------------------------------------------------------------------+
!                                                                        |
!   INPUT / OUTPUT: qv2D(klon,klev): air   specific humidity     (kg/kg) |
!   ^^^^^^^^^^^^^^  qw2D(klon,klev): cloud drops                 (kg/kg) |
!                   qr2D(klon,klev): rain  drops                 (kg/kg) |
!                   qi2D(klon,klev): ice   crystals concentration(kg/kg) |
!                   qs2D(klon,klev): snow  flakes                (kg/kg) |
!   (to be added)   qg2D(klon,klev): graupels                    (kg/kg) |
!                 ccnw2D(klon,klev): cloud droplets number       (Nb/m3) |
!                 ccni2D(klon,klev): ice   crystals number       (Nb/m3) |
!                                                                        |
!                 cfra2D(klon,klev): cloud fraction                      |
!                                                                        |
!                 rain2D(klon)     : rain  Precipitation        (m w.e.) |
!                 snow2D(klon)     : snow  Precipitation        (m w.e.) |
!                 crys2D(klon)     : ice   Precipitation        (m w.e.) |
!                                                                        |
!                 hlat2D(klon,klev): Latent Heat Release           (K/s) |
!                  dqi2D(klon,klev): Ice    Water Formation      (kg/kg) |
!                  dqw2D(klon,klev): Liquid Water Formation      (kg/kg) |
!                 qvsi2D(klon,klev+1): Saturation Specific Humid.(kg/kg) |
!                 qvsw2D(klon,klev+1): Saturation Specific Humid.(kg/kg) |
!                                                                        |
!   REFER. : 1) Ntezimana, unpubl.thes.LLN,          115 pp,     1993    |
!   ^^^^^    2) Lin et al.       JCAM            22, 1065--1092, 1983    |
!               (very similar, except that graupels are represented)     |
!            3) Emde and Kahlig, Annal.Geophys.   7,  405-- 414, 1989    |
!            4) Levkov et al.,   Contr.Atm.Phys. 65,   35--  57, 1992    |
!            5) Meyers et al.,   JAM             31,  708-- 731, 1992    |
!               (Primary Ice-Nucleation Parameterization)                |
!            6) Delobbe and Gallee, BLM          89,   75-- 107  1998    |
!               (Partial Condensation Scheme)                            |
!                                                                        |
!   CAUTION:     Partial Condensation Scheme NOT validated               |
!   ^^^^^^^      for SCu -- Cu Transition                                |
!                erf fonction is erroneous on HP                         |
!                                                                        |
! # OPTIONS: #HM  Hallet-Mossop Theory (for Convective Updraft)          |
! # ^^^^^^^  #hm  idem                 (non vectorized code)             |
! #        ! #qf  Cloud Droplets Heterogeneous Freezing (not included)   |
! #        ! #qg  Graupel Conservation Equation         (to  include)    |
! #          #hb  Snow particles distrib. parameter cnos set to BS value |
! #          #hs  Emde & Kahlig Homogeneous Sublimation (not in Levkov)  |
! #        ! #pp  Emde & Kahlig Ice Crystal Deposition  (not included)   |
!                                                                        |
! #          #VW  Duynkerke et al. 1995, JAS 52, p.2763 Dropplets Fall   |
! #          #LI  Lin  et  al (1983,JCAM 22, p.1076(50) Autoconv. Scheme |
! #          #LO  Liou and Ou (1989, JGR 94, p.8599)    Autoconv. Scheme |
!                                                                        |
! #          #up  Snow Particles: Unrimed Side Planes                    |
! #          #ur  Snow Particles: Aggregates of unrimed radiat. assembl. |
!                                                                        |
! # DEBUG:   #WH  Additional Output (Each Process  is detailed)          |
! # ^^^^^    #WQ  FULL       Output (Each Process  is detailed)          |
! #          #EW  Additional Output (Energy and Water Conservation)      |
!                                                                        |
!   REMARK : the sign '~' indicates that reference must be verified      |
!   ^^^^^^^^                                                             |
!------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
!  Global Variables
!  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
      include 'MAR_IO.inc'
 
      include 'MAR_DY.inc'
      include 'MAR_HY.inc'
c #EW include 'MAR_EW.inc'
 
 
!  Input / Output
!  ==============
 
      include 'MAR_2D.inc'
 
      integer           io1 ,io5 ,ilmm,imm,jmm,ilmez
 
!  Debug Variables
!  ~~~~~~~~~~~~~~~
c #wH integer         i_fvv(klon),j_fvv(klon),klfvv,i0fvv,j0fvv,k0fvv
c #wH common/DebuggHy/i_fvv      ,j_fvv      ,klfvv,i0fvv,j0fvv,k0fvv
c #wH character*70    debugH
c #wH character*10    proc_1,proc_2,proc_3,proc_4
c #wH real            procv1,procv2,procv3,procv4
c #wH integer         kv    ,nl
c #wH real            debugV(16,klev)
 
 
!  Local  Variables
!  ================
 
      integer il,kl,itc,itmx,it,ii,io,ilmmi
 
      real    thir5,eps1,cnor,cnos,cnog,ui50,ri50,
     .        beta,C1_EkM,C2_EkM,tsfo,WatIce,ExpWat,
     .        ExpWa2,qsd0,qi00,qg00,sigmaw,rcrilo,qw00L,
     .        qw00,camart,csud,typww,cc1,cc2,dd0,SSImax
 
      REAL    argerf,erf,xt
      real    signQw,signQr,signQi,signQs,signCi,signVR,
     .        signVS,signHN,Qw0_OK,Qr0_OK,Qi0_OK,Qs0_OK,
     .        Qi0qOK,Ci0cOK,Ci0_OK,vr__OK,vs__OK,qHoNuc,
     .        qwOK,dpv,dqv,qHeNu1,qHeNu2,qHeNu3,qHeNuc,
     .        qicnd1,qisign,qi1_OK,qicnd2,qicnd,qBerge,
     .        a1,a2,am0,qidep,qvdfci,qSubl1,qSubl2,qSubli,
     .        demde,sat,ab1,ab2,amf,pisub,qisub,qMelt1,
     .        qMelt2,qMelt,qxmlt,qimlt,cimlt,qt,tl,pa_hPa,
     .        es_hPa,qsl,dqt,wqt,ww,coefC2,sig2rh,sigqt,
     .        err,alpha,t1,t2,signFR,cfraOK,SCuLim,
     .        qw_new,dqw,signdq,fac_qv,updatw,dpw,signAU,
     .        AutoOK,signFC,ClouOK,praut,qraut,signCC,qiOK,
     .        qid,a1saut,c1saut,xtsaut,qsaut,cnsaut,ex1,psaut,
     .        sign_W,WbyR_w,sign_R,WbyR_r,WbyROK,pracw,
     .        qracw,WbyS_w,sign_S,WbyS_s,WbySOK,qsacw,
     .        sign_T,Fact_R,SnoA,sign_C,CbyS_c,CbyS_T,
     .        CbySOK,efc,psaci,qsaci,cnsaci,CbyR_c,CbyR_r,
     .        CbyR_T,CbyROK,praci,qraci,CbyS_s,cnraci,
     .        piacr,qiacr,qsacr,RbyS_r,RbyS_s,RbySOK,flR,
     .        SbyR_r,SbyR_s,SbyROK,flS,pracs,qracs,qsacrS,
     .        qracsS,Evap_r,EvapOK,sr,sign_Q,Evap_q,
     .        qsacrR,almr,ab,prevp,qrevp,Evap_s,alms,si,
     .        pssub,qssub,dqamx,Depo_s,SnoM_s,SnoM_T,
     .        SnoMOK,qsmlt,xCoef,ACoef,BCoef,Tc,
     .        Freezr,FreezT,FrerOK,psfr,akps,psmlt,
     .        qsfr,Sedi_c,Sedicc,SediOK,vrmx,vsmx,vimx,
     .        dzmn,xtmn,dwat,dsno,qcloud,pp,pkt0,vmmx,vmmi,
     .        connw,qwclou,dmed0,dmedv,dmede,dmed5,waterb,
     .        dmed,dmed2,dw0,dw4,rwbar,signHV,heavi,vwmx
 
      real     dqi,dqi1,dqi2,dqi3,qi0S,cnos2
      real    relhum,argexp,qvs_wi,relCri
 
      logical  Meyers
      logical  LevkovAUTO,LevkovAUTX
      logical  EmdeKa
      logical  fracSC                       ! SCu Fractional Cloudiness Delobbe
      logical  fraCEP                       ! SCu Fractional Cloudiness ECMWF
 
      real    vi(klon,klev)
c #VW real    vw(klon,klev)
      real    vr(klon,klev)
      real    vs(klon,klev)
! #qg real    vh(klon,klev)
      real    psacw(klon,klev),psacr(klon,klev)
 
c #WH real    wihm1(klev),wihm2(klev),wicnd(klev)
c #WH real    widep(klev),wisub(klev),wimlt(klev)
c #WH real    wwevp(klev)
c #WH real    wraut(klev),wsaut(klev)
c #WH real    wracw(klev),wsacw(klev)
c #WH real    wsaci(klev),wraci(klev),wiacr(klev)
c #WH real    wsacr(klev),wracs(klev),wrevp(klev)
c #WH real    wssub(klev),wsmlt(klev),wsfre(klev)
c #WH real    qiold(klev),qwold(klev)
 
      real    aM_Nid,bM_Nid,TM_Nid               ! Meyers et al. 1992 JAM
      real    aM_Nic,bM_Nic,TM_Nic,rad_ww        ! Meyers et al. 1992 JAM
c #HM real    TmnNhm,TmxNhm,w_svrl,SplinJ,SplinP ! Levkov et al. 1992 CAM
 
      real    aa1(31),aa2(31)
 
 
!  DATA
!  ====
 
      data Meyers    /.true./
      data LevkovAUTO/.true./
      data LevkovAUTX/.true./   !  .TRUE. => Levkov      paramet. Bergeron Proc.
!                               ! .FALSE. => Emde&Kahlig paramet. Bergeron Proc.
      data EmdeKa    /.false./
      data fracSC    /.false./
         ! fracSC   = .true.
         !          => Delobbe SCu Fractional Cloudiness Scheme
         !                     may be set up if fracld = .true.
      data fraCEP    /.false./
 
      data thir5 /1.66e0/
!          thir5 = 5 / 3
 
      data eps1  /1.e-01/
 
      data cnor/8.0e06/     ! intercept parameter / rain    distribution
c #LA data cnor/3.0e06/     ! intercept parameter / rain    distribution ! Tun
c #AC data cnos/5.0e07/     ! intercept parameter / snow    distribution
      data cnos/5.0e07/     ! intercept parameter / snow    distribution
      data cnos/3.0e06/     ! intercept parameter / snow    distribution
c #LA data cnos/4.0e06/     ! intercept parameter / snow    distribution ! Tun
      data cnog/4.0e04/     ! intercept parameter / graupel distribution
!                           ! Lin et al.   1983, JCAM 22, p.1068 (1,2 and 3)
 
 
      data cnos2/5.e06/     ! intercept parameter / snow    distribution
 
 
      data ui50/0.1e0/
      data ri50/5.e-5/
      data beta/0.5e0/
 
      data C1_EkM /0.14e-3/ ! Partial Condensation Scheme
      data C2_EkM /9.75e+0/ !
!                           ! Ek and Mahrt 1991, An.Geoph. 9, 716--724
 
      data tsfo/-35.e0/     ! Levkov et al.1992, C.Atm.Ph.65, p.39
!          tsfo: minimum temperature (deg.C)
!                before instantaneous cloud dropplets freezing
!
      data WatIce/273.16e0/
      data ExpWat/5.138e0/
      data ExpWa2/6827.e0/
!          Saturation pressure over Water (Dudhia (1989) JAS)
 
!     data aM_Nid/-0.639 /  !(Meyers et al. 1992, p.713)
!     data bM_Nid/ 0.1296/
cXF
      data aM_Nid/-1.488 /
      data bM_Nid/ 0.0187/  !(Prenni et al. 2007, p.545, BAMS)
      data TM_Nid/-5.    /
!          Deposition and Condensation-Freezing Nucleation Parameters
!                               (Meyers et al. 1992, p.713)
      data aM_Nic/-2.80  /
      data bM_Nic/ 0.262 /
      data TM_Nic/-2.    /
!          Contact Freezing Nucleation                     Parameters
!                               (Meyers et al. 1992, p.713)
c #HM data TmnNhm/-8.    /
c #HM data TmxNhm/-3.    /
c #HM data w_svrl/ 1.    /
!          Hallet-Mossop Theory (see Levkov et al., 1992,
!                                Contr.Atm.Phy. 65, p.40)
      data qsd0/2.00e-4/
!          qsd0: Smallest Diameter of Particles in the snow Class
!          Ref.: Levkov et al. 1992, Contr. Atm. Phys. 65, p.41, para 1
 
      data qi00/0.001e0/
!          qi00: maximum ice   crystals concentration
!                before autoconversion of snow flakes occurs
!          Ref.: Lin et al.      1983, JCAM      22, p.1070 (21)
 
! _hl data qi00/0.0008/
!               0.0008 : compromise when graupels are not included
!          Ref.: Emde and Kahlig 1989, Ann.Geoph. 7, p.408  (18)
 
      data qg00/0.0006e0/
!          qg00: maximum ice   crystals concentration
!                before autoconversion of graupels occurs
!          Ref.: Lin et al.      1983, JCAM      22, p.1074 (37)
 
        data sigmaw/0.27e+0/
!            sigmaw=1/3 ln(1/k), where k=0.8d+0 (dispersion parameter)
!           (Martin et al., 1994, JAS 51, p.1823)
 
c #LO   data rcrilo/10.0e-6/
!            rcrilo:Autoconversion Critical Radius (Liou and Ou, 1989)
 
c #LI data qw00L/0.002e0/
!          qw00L: maximum cloud droplets concentration
!                before autoconversion occurs
!          Ref.: Lin et al.      1983, JCAM      22, p.1076 (50)
 
      data qi0S/0.10e-3/ ! critical  solid water mixing ratio (tuned Dome C)
!                                                             (FacFIk >   1)
!          qi0S=0.30e-3  ! critical  solid water mixing ratio (standard)
      data qw00/0.10e-3/
!          qw00 0.30e-3  ! critical liquid water mixing ratio
!          Reference.    ! Sundqvist (1988) : Physically-Based Modelling and
!                          Simulation of Climate and Climatic Change,
!                          M.E. Schlesinger, Ed., Reidel, 433-461.
 
c #SC data camart/0.8e0/
c #SC data connw /1.2e8/
!          connw : droplets number concentration (m-3)
 
      data csud/1.0e-4/
!          csud : 1/characteristic time scale for autoconversion (SUND)
!          Ref. : Sundqvist (1988)
 
      data typww /18.e-15/
!          typww: Typical Cloud Droplet Weight [Ton] (typ. diam.: 32.5 mim)
!                (used with air Density rolv2D [Ton/m3])
 
      data  cc1/1.200e-04/
      data  cc2/1.569e-12/
      data  dd0/0.15e0/
!           cc1, cc2, dd0: cloud droplets autoconversion parameters
 
 
!     ======================================================================
!     Bergeron Process Data (given by Koenig, 1971, J.A.S. 28,p235) ========
 
      data aa1/0.7939e-07 , 0.7841e-06 , 0.3369e-05 , 0.4336e-05 ,
     .         0.5285e-05 , 0.3728e-05 , 0.1852e-05 , 0.2991e-06 ,
     .         0.4248e-06 , 0.7434e-06 , 0.1812e-05 , 0.4394e-05 ,
     .         0.9145e-05 , 0.1725e-06 , 0.3348e-04 , 0.1725e-04 ,
     .         0.9175e-05 , 0.4412e-05 , 0.2252e-05 , 0.9115e-06 ,
     .         0.4876e-06 , 0.3473e-06 , 0.4758e-06 , 0.6306e-06 ,
     .         0.8573e-06 , 0.7868e-06 , 0.7192e-06 , 0.6513e-06 ,
     .         0.5956e-06 , 0.5333e-06 , 0.4834e-06 /
      data aa2/0.4006e0,  0.4831e0,  0.5320e0,  0.5307e0,  0.5319e0,
     .         0.5249e0,  0.4888e0,  0.3894e0,  0.4047e0,  0.4318e0,
     .         0.4771e0,  0.5183e0,  0.5463e0,  0.5651e0,  0.5813e0,
     .         0.5655e0,  0.5478e0,  0.5203e0,  0.4906e0,  0.4447e0,
     .         0.4126e0,  0.3960e0,  0.4149e0,  0.4320e0,  0.4506e0,
     .         0.4483e0,  0.4460e0,  0.4433e0,  0.4413e0,  0.4382e0,
     .         0.4361e0/
 
!     Bergeron Process Data (given by Koenig, 1971, J.A.S. 28,p235) ========
!     ======================================================================
 
 
!  Upper Limit for specific Humidity
!  =================================
 
! #kk data SSImax  /  900.0   /
      data SSImax  /  101.0   /
!          SSImax: Maximum Sursaturation % ICE (900 ==> RH=1000%)
 
      data relCri  /    1.0   /
c #rc      relCri=0.90+0.08*sqrt(max(0.,100.-dx*0.001)/95.)
 
 
!  Cloud Droplets Autoconversion Threshold
!  =======================================
 
c #LI qw00  = qw00L
 
 
!  For Blown Snow Particles
!  ========================
c #hb cnos  = 0.1d18
C +...        DO NOT USE unless for specific sensivity experiments
c #hb IF (itexpe.eq.0) write(6,6000)
 6000 format(/,' ****************************************************',
     .       /,' * cnos  = 0.1d18 for PURE BLOWING SNOW EXPERIMENTS *',
     .       /,' *             DO not USE  OTHERWISE                *',
     .       /,' ****************************************************',
     .       /)
 
 
!  Update of Temperature
!  =====================
 
       xt=min(dt,dtHyd)
 
        DO kl=mzhyd,klev
        DO il=1,klon
          tair2D(il,kl)= pkta2D(il,kl)*pk2D(il,kl)
 
!  Debug
!  ~~~~~
c #wH     debugH( 1:35) = 'HYDmic: Debugged Variables: Initial'
c #wH     debugH(36:70) = '                                   '
c #wH     proc_1        = 'R.Hum W[%]'
c #wH     procv1        =  0.1*qv2D(il,kl)/(rhcrHY * qvsw2D(il,kl))
c #wH     proc_2        = 'R.Hum I[%]'
c #wH     procv2        =  0.1*qv2D(il,kl)/(rhcrHY * qvsi2D(il,kl))
c #wH     proc_3        = '          '
c #wH     procv3        =  0.
c #wH     proc_4        = '          '
c #wH     procv4        =  0.
c #wh     include 'MAR_HY.Debug'
 
c #wH     DO kv=1,16
c #wH     debugV(kv,kl) =  0.
c #wH     ENDDO
 
 6020                format(/,a70
     .                     ,/,13x,'Tc'
     .                       ,10x,'Qv'
     .                       ,10x,'Qw'
     .                       ,10x,'Qi'
     .                        ,9x,'CLD'
     .                       ,10x,'Qs'
     .                       ,10x,'Qr'
     .                        ,2x,a10
     .                        ,2x,a10
     .                        ,2x,a10
     .                        ,2x,a10)
 6021                format(i3,12f12.6)
 
        END DO
        END DO
 
 
!  Vertical Integrated Energy and Water Content
!  ============================================
 
c #EW   DO il=1,klon
c #EW     enr01D(il ) = 0.0
c #EW     wat01D(il ) = 0.0
 
c #EW   DO kl=1,klev
c #EW     enr01D(il ) = enr01D(il )
c #EW.                +(tair2D(il,kl)
c #EW.                  -(qw2D(il,kl)+qr2D(il,kl))*r_LvCp
c #EW.                  -(qi2D(il,kl)+qs2D(il,kl))*r_LsCp)
c #EW.                  *  dsigm1(kl)
c #EW     wat01D(il ) = wat01D(il )
c #EW.                +  (qv2D(il,kl)
c #EW.                +   qw2D(il,kl)+qr2D(il,kl)
c #EW.                +   qi2D(il,kl)+qs2D(il,kl)        )
c #EW.                  *  dsigm1(kl)
c #EW   END DO
 
c #EW     mphy2D(il ) ='                    '
C +...    mphy2D -->   '12345678901234567890'
 
c #ew     enr01D(il ) = enr01D(il ) * pst2Dn(il ) * grvinv
c #EW     wat01D(il ) = wat01D(il ) * pst2Dn(il ) * grvinv
C +...    wat01D [m]    contains an implicit factor 1.d3 [kPa-->Pa] /ro_Wat
 
c #EW   END DO
 
c #WH   vmmx = 0.0
 
 
!  Set lower limit on Hydrometeor Concentration
!  ============================================
 
        IF (no_vec)                                               THEN
 
          DO kl=mzhyd,klev
          DO il=1,klon
 
            IF (qw2D(il,kl).lt.eps9)                              THEN
                qv2D(il,kl) =  qv2D(il,kl)+qw2D(il,kl)
              tair2D(il,kl) =tair2D(il,kl)-qw2D(il,kl)*r_LvCp
               dqw2D(il,kl) = dqw2D(il,kl)-qw2D(il,kl)
                qw2D(il,kl) =  0.0
            END IF
 
            IF (qr2D(il,kl).lt.eps9)                              THEN
                qv2D(il,kl) =  qv2D(il,kl)+qr2D(il,kl)
              tair2D(il,kl) =tair2D(il,kl)-qr2D(il,kl)*r_LvCp
               dqw2D(il,kl) = dqw2D(il,kl)-qr2D(il,kl)
                qr2D(il,kl) =  0.0
            END IF
 
            IF (qi2D(il,kl).lt.eps9.or.ccni2D(il,kl).lt.unun)     THEN
                qv2D(il,kl) =  qv2D(il,kl)+qi2D(il,kl)
              tair2D(il,kl) =tair2D(il,kl)-qi2D(il,kl)*r_LsCp
               dqi2D(il,kl) = dqi2D(il,kl)-qi2D(il,kl)
                qi2D(il,kl) =  0.0
              ccni2D(il,kl) =  0.0
            END IF
 
            IF (qs2D(il,kl).lt.eps9)                              THEN
                qv2D(il,kl) =  qv2D(il,kl)+qs2D(il,kl)
              tair2D(il,kl) =tair2D(il,kl)-qs2D(il,kl)*r_LsCp
               dqi2D(il,kl) = dqi2D(il,kl)-qs2D(il,kl)
                qs2D(il,kl) =  0.0
            END IF
          END DO
          END DO
 
        ELSE
 
          DO kl=mzhyd,klev
          DO il=1,klon
 
            signQw           = sign(unun,  eps9  -     qw2D(il,kl))
            Qw0_OK           =  max(zero,signQw) *     qw2D(il,kl)
              qw2D(il,kl) =      qw2D(il,kl)  -     Qw0_OK
             dqw2D(il,kl) =     dqw2D(il,kl)  -     Qw0_OK
              qv2D(il,kl) =      qv2D(il,kl)  +     Qw0_OK
            tair2D(il,kl) =    tair2D(il,kl)  -     Qw0_OK*r_LvCp
 
            signQr           = sign(unun,  eps9  -     qr2D(il,kl))
            Qr0_OK           =  max(zero,signQr) *     qr2D(il,kl)
              qr2D(il,kl) =      qr2D(il,kl)  -     Qr0_OK
             dqw2D(il,kl) =     dqw2D(il,kl)  -     Qr0_OK
              qv2D(il,kl) =      qv2D(il,kl)  +     Qr0_OK
            tair2D(il,kl) =    tair2D(il,kl)  -     Qr0_OK*r_LvCp
 
            signQi           = sign(unun,  eps9  -     qi2D(il,kl))
            Qi0qOK           =  max(zero,signQi)
            signCi           = sign(unun,  unun  -   ccni2D(il,kl))
            Ci0cOK           =  max(zero,signCi)
 
            Ci0_OK           =  max(     Ci0cOK  ,   Qi0qOK)
            Qi0_OK           =           Ci0_OK  *     qi2D(il,kl)
 
            ccni2D(il,kl) =    ccni2D(il,kl)  *     Ci0_OK
              qi2D(il,kl) =      qi2D(il,kl)  -     Qi0_OK
             dqi2D(il,kl) =     dqi2D(il,kl)  -     Qi0_OK
              qv2D(il,kl) =      qv2D(il,kl)  +     Qi0_OK
            tair2D(il,kl) =    tair2D(il,kl)  -     Qi0_OK*r_LsCp
 
            signQs           = sign(unun,  eps9  -     qs2D(il,kl))
            Qs0_OK           =  max(zero,signQs) *     qs2D(il,kl)
              qs2D(il,kl) =      qs2D(il,kl)  -     Qs0_OK
             dqi2D(il,kl) =     dqi2D(il,kl)  -     Qs0_OK
              qv2D(il,kl) =      qv2D(il,kl)  +     Qs0_OK
            tair2D(il,kl) =    tair2D(il,kl)  -     Qs0_OK*r_LsCp
 
          END DO
          END DO
 
        END IF
 
 
!  Update of dummy Variables
!  =========================
 
        DO kl=mzhyd,klev
        DO il=1,klon
          W2xyz1(il,kl)= tair2D(il,kl)-TfSnow
          W2xyz2(il,kl)= 1.e-2*exp(-0.6*W2xyz1(il,kl))
!         W2xyz2       : Ice Crystals Number (Fletcher, 1962)
 
          W2xyz3(il,kl)= qr2D(il,kl)
          W2xyz4(il,kl)= qs2D(il,kl)
! #qg     W2xyz0(il,kl)= qg2D(il,kl)
 
c #WH     IF (il.eq.ilmm)                                         THEN
c #WH       qwold(kl) = qw2D(il,kl)
c #WH       qiold(kl) = qi2D(il,kl)
c #WH     END IF
!         old values
 
        END DO
        END DO
 
 
!  Saturation Specific Humidity
!  ============================
 
!       ***********
        call qsat2D(tair2D,pst2D,tsrf2D,qvsi2D,qvsw2D)
!       ***********
 
        DO kl=mzhyd,klev
        DO il=1,klon
 
          W2xyz5(il,kl) = rhcrHY * qvsi2D(il,kl)
!         W2xyz5:  Saturation Specific Humidity over Ice
 
          W2xyz6(il,kl) = sqrt((pst2Dn(il)+ptopDY)
     .                        /(rolv2D(il,kl)*RDryAi
     .                        * tair2D(il,klev)))
 
 
!  Cloud Droplets Fall Velocity (Calcul de la Vitesse Terminale Moyenne)
!  ----------------------------
 
c #VW     IF (qw2D(il,kl).ge.eps9)                               THEN
 
c #VW       ccnw2D(il,kl) = 1.2d+8
C +...      ccnw2D: ASTEX case (Duynkerke et al. 1995, JAS 52, p.2763)
 
c #VW       qwclou        =  qw2D(il,kl) / max(cminHY ,cfra2D(il,kl))
c #VW       dmed0         = 4.5       *sigmaw         *sigmaw
c #VW       dmedv         =12.5       *sigmaw         *sigmaw
c #VW       dmede         = qwclou    *rolv2D(il,kl)
c #VW.                    * 6.0d+0/(pi*ccnw2D(il,kl)*exp(dmed0))
c #VW       dmed5         = exp(thir5*log(dmede))
C +...      dmed          = exp(third*log(dmede))
C +
c #VW       vw(il,kl)  = 1.19d8* pi*ccnw2D(il,kl)*dmed5
c #VW.                    * exp(dmedv)/(24.0*rolv2D(il,kl)*qwclou)
c #VW     ELSE
c #VW       vw(il,kl)  = 0.00
c #VW     END IF
 
 
!  Rain           Fall Velocity
!  ----------------------------
 
            W2xyz7(il,kl) = exp(0.25*log((pi*cnor)
     .                       / (rolv2D(il,kl)*max(eps9,qr2D(il,kl)))))
!           W2xyz7(il,kl) : lambda_r : Marshall-Palmer Distribution Parameter
!                                      for Rain
!                        Note that a simplification occurs
!                        between the 1000. factor of rho, and rho_water=1000.
!           Reference  : Emde and Kahlig 1989, Ann.Geoph. 7, p.407 (3)
!
          IF                          (qr2D(il,kl).gt.eps9)       THEN
 
            signVR      =  sign(unun,  qr2D(il,kl)  - eps9)
            vr__OK      =   max(zero,signVR)
!           vr__OK      =   1.0 if     qr2D(il,kl)  > eps9
!                       =   0.0 otherwise
 
            vr(il,kl)   = vr__OK*392. *W2xyz6(il,kl)
     .                  / exp(0.8 *log(W2xyz7(il,kl)))
!           vr          : Terminal Fall Velocity for Rain
!                                392  = a Gamma[4+b] / 6
!                                where  a = 842.  and b = 0.8
 
          ELSE
            vr(il,kl)=   0.0
          END IF
 
 
!  Snow Fall Velocity
!  ------------------
 
c #cn       cnos          = min(2.e8
c #cn.                         ,cnos2*exp(-.12*min(0.,W2xyz1(il,kl))))
            W2xyz8(il,kl) = exp(0.25*log((0.50*pi*cnos)
     .                       / (rolv2D(il,kl)*max(eps9,qs2D(il,kl)))))
!           W2xyz8(il,kl) : lambda_s : Marshall-Palmer distribution parameter
!                                      for Snow Flakes
!                        Note that a partial simplification occurs
!                        between the 1000. factor of rho, and rho_snow=500.
!           Reference  : Emde and Kahlig 1989, Ann.Geoph.      7,  p.407 (3)
!           (rho_snow)   Levkov et al.   1992, Cont.Atm.Phys. 65(1) p.37 (5)
 
          IF                          (qs2D(il,kl).gt.eps9)       THEN
 
            signVS      =  sign(unun,  qs2D(il,kl)  - eps9)
            vs__OK      =   max(zero,signVS)
!           vs__OK      =   1.0 if     qs2D(il,kl)  > eps9
!                       =   0.0 otherwise
 
            vs(il,kl)   =  vs__OK*2.19 *W2xyz6(il,kl)
     .                   / exp(0.25*log(W2xyz8(il,kl)))
!           vs          : Terminal Fall Velocity for Snow Flakes
!                                 2.19 = c Gamma[4+d] / 6
!                                 where  c = 4.836 = 0.86 *1000.**0.25
!                                              and d = 0.25
!                     (Locatelli and Hobbs, 1974, JGR: table 1 p.2188:
!                      Graupellike Snow Flakes of Hexagonal Type)
 
            vs(il,kl)   =  vs__OK*2976.*W2xyz6(il,kl)
     .                   / exp(0.99*log(W2xyz8(il,kl)))
!                  OR             2976.= c Gamma[4+d] / 6
!                                 where  c = 755.9 = 0.81 *1000.**0.99
!                                              and d = 0.99
!                     (Locatelli and Hobbs, 1974, JGR: table 1 p.2188:
!                      Unrimed Side Planes)
 
c #ur       vs(il,kl)   =  vs__OK*20.06*W2xyz6(il,kl)
c #ur.                   / exp(0.41*log(W2xyz8(il,kl)))
!                  OR             2976.= c Gamma[4+d] / 6
!                                 where  c = 755.9 = 0.69 *1000.**0.41
!                                              and d = 0.41
!                     (Locatelli and Hobbs, 1974, JGR: table 1 p.2188:
!                      Aggregates of unrimed radiating assemblages)
 
          ELSE
            vs(il,kl)   =   0.0
          END IF
 
 
!  Graupel Fall Velocity
!  ---------------------
 
! #qg     IF (qg2D(il,kl).ge.eps9)                                THEN
!         Do'nt forget "#hy" option !
! #qg       W2xyz9(il,kl) =exp(0.250*log((pi*cnog)
! #qg.                    /(rolv2D(il,kl)*max(eps9,qg2D(il,kl)))))
!           W2xyz9(il,kl) : lambda_g : Marshall-Palmer distribution parameter
!                                      for Graupel
!                        Note that a simplification occurs
!                        between the 1000. factor of rho, and rho_ice=1000.
! #qg           vh(il,kl) = 25.1 *W2xyz6(il,kl)
! #qg.             / exp(0.57*log(W2xyz9(il,kl)))
!           vh         : Terminal Fall Velocity for Graupels
!                        25.1 = c Gamma[4+d] / 6
!                        where  c = 4.836 = 1.10 *1000.**0.57 and d = 0.57
!                        (Locatelli and Hobbs, 1974, JGR: table 1 p.2188:
!                         Hexagonal Graupel)
 
! #qg     ELSE
! #qg           vh(il,kl) =   0.0
! #qg       W2xyz9(il,kl) =   0.0
! #qg     END IF
 
        END DO
        END DO
 
 
!===============================================================================
 
!  Microphysical Processes affecting non Precipitating Cloud Particles
!  ===================================================================
 
!  Homogeneous Nucleation by Cloud Dropplets Solidification  ! BFREWI
!  Reference: Emde and Kahlig 1989, Ann.Geoph. 7, p.407 (11) ! Levkov (24) p.40
!  ---------------------------------------------------------
 
          DO kl=mzhyd,klev
          DO il=1,klon
 
c #wH         qHoNuc      =  0.
c #wH         qHeNuc      =  0.
c #wH         qwOK        =  0.
 
            IF                        (W2xyz1(il,kl).lt.tsfo)     THEN
 
              signHN      = -sign(unun,W2xyz1(il,kl)  - tsfo)
              qHoNuc      =   max(zero,signHN)
!             qHoNuc      =   1.0 if   W2xyz1(il,kl)  < tsfo
!                         =   0.0 otherwise
 
c #EW        IF(qHoNuc.gt.epsi)                                   THEN ! ctr
c #EW         mauxEW        =  mphy2D(il )
c #EW         mauxEW(01:01) = 'i'
c #EW         mphy2D(il )   =  mauxEW
c #EW        END IF                                                    ! ctr
 
                qwOK        =   qw2D(il,kl) *                qHoNuc
                qi2D(il,kl) =   qi2D(il,kl) +                qwOK
              ccni2D(il,kl) = ccni2D(il,kl) + rolv2D(il,kl) *qwOK/typww
              tair2D(il,kl) = tair2D(il,kl) + r_LcCp        *qwOK
c #WQ         write(6,*) 'Qihm1',  qw2D(il,kl),
c #WQ.                  ' Qi'   ,  qi2D(il,kl),
c #WQ.                  ' CcnI' ,ccni2D(il,kl),itexpe,il,kl
c #WH         IF (il.eq.ilmm) wihm1(kl) =   qwOK
 
                qw2D(il,kl) = qw2D(il,kl) - qwOK
 
            END IF
 
 
!  Heterogeneous Freezing of Cloud Droplets                  ! BNUFWI
!  Reference: Levkov et al., 1992 (21) p.40                  ! Levkov (21) p.40
!  ----------------------------------------
 
! #qf #hy   IF                        (W2xyz1(il,kl).lt.0.00)     THEN
 
! #qf         signHN      = -sign(unun,W2xyz1(il,kl)  - 0.00)
! #qf         qHeNuc      =   max(zero,signHN)
!             qHeNuc      =   1.0 if   W2xyz1(il,kl)  < 0.00dgC
!                         =   0.0 otherwise
 
! #qf         argexp      = min(max(argmin,-W2xyz1(il,kl)),argmax)
! #qf         qHeNuc      =         qHeNuc*(exp(argexp)   -  1.  )
! #qf.                                     *  qw2D(il,kl) * 100.0 *typww
! #qf         qHeNuc      = min(    qHeNuc ,  qw2D(il,kl)        )
 
! #qf           qi2D(il,kl) =   qi2D(il,kl) +               qHeNuc
! #qf         ccni2D(il,kl) = ccni2D(il,kl) + rolv2D(il,kl)*qHeNuc/typww
! #qf         tair2D(il,kl) = tair2D(il,kl) + r_LcCp       *qHeNuc
! #qf           qw2D(il,kl) = qw2D(il,kl)   -               qHeNuc
 
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Homo+Hetero Nucleation by Droplets '
c #wH         debugH(36:70)   = 'Solidification (BFREWI+BNUFWI)     '
c #wH         proc_1          = 'BFREWI    '
c #wH         procv1          =  qHoNuc
c #wH         proc_2          = 'BNUFWI    '
c #wH         procv2          =  qHeNuc
c #wH         proc_3          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(01,kl)   =  qwOK+qHeNuc
 
! #qf #hy   END IF
 
 
!===============================================================================
 
!  Homogeneous Sublimation                                   ! XXXXXX
!  Reference: Emde and Kahlig 1989, Ann.Geoph. 7, p.407 (12) ! Levkov
!  ---------------------------------------------------------
 
c #EW        IF(qHoNuc.gt.epsi)                                   THEN ! ctr
c #EW          mauxEW        =  mphy2D(il )
c #EW          mauxEW(02:02) = 'I'
c #EW          mphy2D(il )   =  mauxEW
c #EW        END IF                                                    ! ctr
 
c #hs          dpv = (qv2D(il,kl)-W2xyz5(il,kl))
c #hs.             /(1.00+1.733e7*W2xyz5(il,kl)
c #hs.                          /(tair2D(il,kl)*tair2D(il,kl)))
!                         1.733e7=Ls*Ls*0.622/Cpa/Ra with Ls = 2833600 J/kg
 
c #hs          dpv = qHoNuc*max(zero,dpv)
c #hs          dqv =                 dpv
 
c #hs           qi2D(il,kl) =   qi2D(il,kl) +                   dqv
c #hs          dqi2D(il,kl) =  dqi2D(il,kl) +                   dqv
!             ccni2D(il,kl) : NO VARIATION
c #hs           qv2D(il,kl) =   qv2D(il,kl) -                   dqv
c #hs         tair2D(il,kl) = tair2D(il,kl) +       r_LsCp    * dqv
 
!  Full Debug
!  ~~~~~~~~~~
c #WQ         write(6,*) 'Qihm2',   dqv,
c #WQ.                  ' Qi'   ,  qi2D(il,kl),
c #WQ.                  ' CcnI' ,ccni2D(il,kl),itexpe,il,kl
c #WH         if (il.eq.ilmm) wihm2(kl) = dqv
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Emde and Kahlig: Homogeneous Sublim'
c #wH         debugH(36:70)   = 'ation                              '
c #wH         proc_1          = 'dQv   g/kg'
c #wH         procv1          =  dqv
c #wH         proc_2          = '          '
c #wH         procv2          =  0.
c #wH         proc_3          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = 'CCNI/1.e15'
c #wH         procv4          =  ccni2D(il,kl)*1.e-18
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(01,kl)   =  dqv + debugV(01,kl)
 
          END DO
          END DO
 
 
!===============================================================================
 
!  Nucleation  I: Deposition & Condensation-Freezing Nucleat.
!  Source       : Water Vapor                                ! BNUCVI
!  Reference: Meyers et al., 1992, JAM 31, (2.4) p.712       ! Levkov (20) p.40
!  -----------------------------------------------------------
 
        IF (Meyers)                                               THEN
          DO kl=mzhyd,klev
          DO il=1,klon
 
c #wH             qHeNuc        =  0.
c #wH             qicnd1        =  0.
c #wH             dqi1          =  0.
c #wH             dqi2          =  0.
c #wH             dqi3          =  0.
 
            IF   (W2xyz1(il,kl).lt.TM_Nid)                        THEN
                  signHN        = -sign(unun,W2xyz1(il,kl) - TM_Nid)
                  qHeNu1        =   max(zero,signHN)
!                 qHeNu1        =   1.0 if   W2xyz1(il,kl) < TM_Nid
!                               =   0.0 otherwise
 
                  dqv           =                qv2D(il,kl)
     .                                        -W2xyz5(il,kl)    ! Sursaturat.
                  dqv           =          max(zero   , dqv)
 
              IF (dqv.gt.0.)                                      THEN
                  signHN        =  sign(unun,dqv   )
                  qHeNu3        =   max(zero,signHN)
!                 qHeNu3        =   1.0 if     qv2D(il,kl) > W2xyz5(il,kl)
!                               =   0.0 otherwise
 
                  qHeNuc        =   qHeNu1   * qHeNu3
 
                  qicnd1        =  1.0e2 * dqv/W2xyz5(il,kl)    ! Sursaturat.%I
                  qicnd1        =          min(qicnd1       ,SSImax )
                  qicnd1        =  1.0e3 * exp(aM_Nid           ! Meyers et al.
     .                                        +bM_Nid * qicnd1) ! 1992 JAM, 2.4
                  qicnd1        =          max(qicnd1
     .                                        -ccni2D(il,kl),zero  )
     .                                      *  qHeNuc
                  ccni2D(il,kl) =              ccni2D(il,kl)
     .                                      +  qicnd1
                  dqi           =   1.e-15  *  qicnd1
     .                                      /  rolv2D(il,kl)
!                                   1.e-15  =  0.001 * Initial Ice Crystal Mass
                  dqi           =          min(dqi    , dqv)
                   qi2D(il,kl) =                qi2D(il,kl) + dqi
                  dqi2D(il,kl) =               dqi2D(il,kl) + dqi
                   qv2D(il,kl) =                qv2D(il,kl) - dqi
                 tair2D(il,kl) =              tair2D(il,kl) + dqi*r_LsCp
                   dqi1         =                             dqi
 
              END IF
            END IF
 
 
!  Nucleation  I:              Contact     -Freezing Nucleat.
!  Source       : Cloud Dropplets                            ! BSPRWI
!  Reference: Meyers et al., 1992, JAM 31, (2.6) p.713       ! Levkov (20) p.40
!  -----------------------------------------------------------
 
c #wH             qicnd1        =  0.
c #wH             qicnd2        =  0.
c #wH             dqi           =  0.
 
            IF   (  qw2D(il,kl).gt.0.)                            THEN
                  signHN        =  sign(unun,  qw2D(il,kl))
                  qHeNu3        =   max(zero,signHN)
C +...            qHeNu3        =   1.0 if     qw2D(il,kl) > 0.
C +                             =   0.0 otherwise
 
              IF (W2xyz1(il,kl).lt.TM_Nic)                        THEN
                  signHN        = -sign(unun,W2xyz1(il,kl) - TM_Nic)
                  qHeNu2        =   max(zero,signHN)
!                 qHeNu2        =   1.0 if   W2xyz1(il,kl) < TM_Nic
!                               =   0.0 otherwise
 
                  qHeNuc        =   qHeNu1   * qHeNu3
 
                  qicnd1        =   1.e3 *     qHeNuc           ! Contact-Freez
     .                                   * exp(aM_Nic           ! Potent.Nuclei
     .                                        -bM_Nic           ! Meyers et al.
     .                                        *W2xyz1(il,kl))   ! 1992 JAM, 2.6
                  rad_ww        =  (1.e3     * rolv2D(il,kl)    ! Drop.  Radius
     .                                       *   qw2D(il,kl)
     .                                       * .2e-11       ) ** 0.33
!                 .2e-11        =   1. / (1.2e+8         * 1.e3 * 4.19)
!                                         ccnw2D (ASTEX)   ro_w   4 pi /3
                  qicnd2        = 603.2e+3  *  qicnd1 * rad_ww  ! Levkov et al.
     .                                      *  rolv2D(il,kl)    ! 1992 CAM,(23)
!                 603.2e3       =   4.0e-7  *  4 pi   * 1.2e+8  * 1.e3
!                                   DFar                ccnw2D    fact(rolv)
                  ccni2D(il,kl) =              ccni2D(il,kl)
     .                                      +  qicnd2
                  dqi           =   1.e-15  *  qicnd2
     .                                      /  rolv2D(il,kl)
!                                   1.e-15  =  1.0e-3 * Ice Crystal Mass
                  dqi           =          min( qw2D(il,kl) , dqi)
                   qi2D(il,kl)  =               qi2D(il,kl) + dqi
                   qw2D(il,kl)  =               qw2D(il,kl) - dqi
                 tair2D(il,kl)  =             tair2D(il,kl) + dqi*r_LcCp
                   dqi2         =                             dqi
 
              END IF
            END IF
 
 
!  Nucleation II: Hallett-Mossop Ice-Multiplication Process  ! BSPRWI
!  Reference: Levkov et al., 1992, Contr.Atm.Ph.65,(25) p.40 ! Levkov (25) p.40
!  -----------------------------------------------------------
 
c #hm       IF   (W2xyz1(il,kl).lt.TmxNhm.AND.
c #hm.            W2xyz1(il,kl).gt.TmnNhm.AND.
c #hm.            wair2D(il,kl).gt.w_svrl    )                      THEN
c #HM             signHN        = -sign(unun,W2xyz1(il,kl) - TmxNhm)
c #HM             qHeNu1        =   max(zero,signHN)
!                 qHeNu1        =   1.0 if   W2xyz1(il,kl) < TmxNhm
!                               =   0.0 otherwise
 
c #HM             signHN        =  sign(unun,W2xyz1(il,kl) - TmnNhm)
c #HM             qHeNu2        =   max(zero,signHN)
!                 qHeNu2        =   1.0 if   W2xyz1(il,kl) > TmnNhm
!                               =   0.0 otherwise
 
c #HM             signHN        =  sign(unun,wair2D(il,kl) - w_svrl)
c #HM             qHeNu3        =   max(zero,signHN)
!                 qHeNu3        =   1.0 if   wair2D(il,kl) > w_svrl
!                               =   0.0 otherwise
 
c #cn             cnos    = min(2.e8
c #cn.                         ,cnos2*exp(-.12*min(0.,W2xyz1(il,kl))))
c #HM             SplinJ = 1.358e12 *qw2D(il,kl) *cnos
c #HM.                   /        (W2xyz8(il,kl)**.33)
!                 1.358e12=pi   *Gamma(3.5) *g   *ro_s /(3 *Cd  *4.19e-12)
!                        [=3.14 *3.3233625  *9.81*0.1  /(3 *0.6 *4.19e-12)]
c #HM             SplinP = 0.003 * (1. - 0.05 *SplinJ) * qHeNu1 *qHeNu2
c #HM.                                                 * qHeNu3
c #HM             SplinP =      max(zero,      SplinP)
 
c #HM             dqi    =          1.e-15  *  SplinP
c #HM.                                      /  rolv2D(il,kl)
!                                   1.e-15  =  1.0e-3 * Ice Crystal Mass
c #HM             SplinP = (min(1.0,qs2D(il,kl)/max(dqi,eps9))) *SplinP
c #HM             ccni2D(il,kl) =              ccni2D(il,kl)
c #HM.                                      +  SplinP
c #HM             dqi    =      min(qs2D(il,kl),  dqi)
c #HM              qi2D(il,kl)  =   qi2D(il,kl) + dqi
c #HM             dqi2D(il,kl)  =  dqi2D(il,kl) + dqi
c #HM              qs2D(il,kl)  =   qs2D(il,kl) - dqi
c #HM              dqi3         =                 dqi
c #hm       END IF
 
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Meyers: Nucl. I, Depot & Cond-Freez'
c #wH         debugH(36:70)   = 'Nucl. / Freez / Nucl. II / Bergeron'
c #wH         proc_1          = 'dQi1 Meyer'
c #wH         procv1          =  dqi1
c #wH         proc_2          = 'dQi2 Meyer'
c #wH         procv2          =  dqi2
c #wH         proc_3          = 'dQi Ha-Mos'
c #wH         procv3          =  dqi3
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(02,kl)   =  dqi1 + dqi2 + dqi3
 
          END DO
          END DO
 
 
!===============================================================================
 
        ELSE
 
!===============================================================================
 
!  Ice Crystals Nucleation Process between 0.C and -35.C
!  (each crystal has a mass equal or less than 10d-12 kg)
!  Reference: Emde and Kahlig 1989, Ann.Geoph. 7, p.408 (13)
!  ---------------------------------------------------------
 
          DO kl=mzhyd,klev
          DO il=1,klon
 
c #wH         qicnd1        =  0.
c #wH         qicnd2        =  0.
c #wH         qicnd         =  0.
 
            IF                          (W2xyz1(il,kl).gt.tsfo)   THEN
 
              signHN        =  sign(unun,W2xyz1(il,kl)  - tsfo)
              qHeNu1        =   max(zero,signHN)
!             qHeNu1        =   1.0 if   W2xyz1(il,kl)  > tsfo
!                           =   0.0 otherwise
 
            IF                          (W2xyz1(il,kl).lt.0.e0)   THEN
 
              signHN        = -sign(unun,W2xyz1(il,kl)        )
              qHeNu2        =   max(zero,signHN)
!             qHeNu2        =   1.0 if   W2xyz1(il,kl)  < 0.e0
!                           =   0.0 otherwise
 
            IF                            (qv2D(il,kl).gt.W2xyz5(il,kl))
     .                                                            THEN
 
              signHN        =  sign(unun,  qv2D(il,kl)  - W2xyz5(il,kl))
              qHeNu3        =   max(zero,signHN)
!             qHeNu3        =   1.0 if     qv2D(il,kl)  > W2xyz5(il,kl)
!                           =   0.0 otherwise
 
              qHeNuc        =   qHeNu1 * qHeNu2 * qHeNu3
 
c #EW        IF(qHeNuc.gt.epsi)                                   THEN ! ctr
c #EW         mauxEW        =  mphy2D(il )
c #EW         mauxEW(03:03) = 'I'
c #EW         mphy2D(il )   =  mauxEW
c #EW        END IF                                                    ! ctr
 
              qicnd1 = qHeNuc * 1.d-15 * W2xyz2(il,kl) / rolv2D(il,kl)
!             qicnd1 : amount of nucleated ice crystals (first  condition)
 
              qisign = sign(unun,qicnd1-qi2D(il,kl))
              qi1_OK =  max(zero,qisign)
              qicnd1 =           qicnd1*qi1_OK
 
              qicnd2 = (  qv2D(il,kl)-W2xyz5(il,kl))
     .                /(1.0d0+1.733d7*W2xyz5(il,kl)
     .                /(tair2D(il,kl)*tair2D(il,kl)))
              qicnd2 =  qHeNuc*max(zero,qicnd2)
!             qicnd2 : amount of nucleated ice crystals (second condition)
 
              qicnd  = min(qicnd1,qicnd2)
 
               qi2D(il,kl) =   qi2D(il,kl) +                qicnd
              dqi2D(il,kl) =  dqi2D(il,kl) +                qicnd
             ccni2D(il,kl) = ccni2D(il,kl) + rolv2D(il,kl) *qicnd
     .                                                     *1.e15
               qv2D(il,kl) =   qv2D(il,kl) -                qicnd
             tair2D(il,kl) = tair2D(il,kl) + r_LsCp        *qicnd
 
!  Full Debug
!  ~~~~~~~~~~
c #WQ        write(6,*) 'QiCnd', qicnd,
c #WQ.                 ' Qi'   ,  qi2D(il,kl),
c #WQ.                 ' CcnI' ,ccni2D(il,kl),itexpe,il,kl
c #WH        IF (il.eq.ilmm) wicnd(kl) = qicnd
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Emde and Kahlig: Ice Crystals Nucle'
c #wH         debugH(36:70)   = 'ation Process between 0.C and -35.C'
c #wH         proc_1          = 'Qicnd1    '
c #wH         procv1          =  qicnd1
c #wH         proc_2          = 'Qicnd2    '
c #wH         procv2          =  qicnd2
c #wH         proc_3          = 'Qicnd g/kg'
c #wH         procv3          =  qicnd
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(02,kl)   =  qicnd
 
 
            END IF
            END IF
            END IF
 
 
          END DO
          END DO
 
        END IF
 
 
!==============================================================================
 
!  Bergeron Process (water vapor diffusion-deposition on ice crystals)
!  Reference: Koenig          1971, J.A.S.    28, p.235
!             Emde and Kahlig 1989, Ann.Geoph. 7, p.408 (14)
!  ---------------------------------------------------------
 
        IF (.NOT.LevkovAUTX)                                        THEN
 
          DO kl=mzhyd,klev
          DO il=1,klon
 
c #wH         qBerge        =  0.
c #wH         qidep         =  0.
c #wH         qicnd         =  0.
 
            IF                          (  qi2D(il,kl).gt.eps9
     .         .AND.                     W2xyz1(il,kl).lt.0.e0)   THEN
 
              signHN        =  sign(unun,  qi2D(il,kl)  - eps9)
              qBerge        =   max(zero,signHN)
!             qBerge        =   1.0 if     qi2D(il,kl)  > eps9
!                           =   0.0 otherwise
 
              signHN        = -sign(unun,W2xyz1(il,kl)        )
              qHeNuc        =   max(zero,signHN)
!             qHeNuc        =   1.0 if   W2xyz1(il,kl)  < 0.e0
!                           =   0.0 otherwise
 
              qBerge        =   qHeNuc * qBerge
 
c #EW        IF(qBerge.gt.epsi)                                   THEN ! ctr
c #EW         mauxEW        =  mphy2D(il )
c #EW         mauxEW(04:04) = 'i'
c #EW         mphy2D(il )   =  mauxEW
c #EW        END IF                                                    ! ctr
 
              itc   = abs(W2xyz1(il,kl)-unun)
              itc   = min(itc,31)
              itc   = max(itc, 1)
              a1    = aa1(itc)
              a2    = aa2(itc)
 
              am0   =  1.d+3*rolv2D(il,kl)*qi2D(il,kl)/W2xyz2(il,kl)
              amf   = (a1*(1.0-a2)*xt+am0**(1.0-a2))**(1.0/(1.0-a2))
!             amf   :  analytical integration of
!                    (14) p.408 Emde and Kahlig 1989, Ann.Geoph. 7
              qidep = (1.d-3*W2xyz2(il,kl)/rolv2D(il,kl))*(amf-am0)
              qidep =  max(zero,qidep)
 
              qicnd =  max(zero,qw2D(il,kl))
!             qicnd :  to avoid the use of qw2D < 0.
 
              qidep      =  qBerge*min(qicnd,    qidep)
              qi2D(il,kl)=  qi2D(il,kl)         +qidep
!           ccni2D(il,kl):NO VARIATION
 
              qw2D(il,kl)=  qw2D(il,kl)         -qidep
            tair2D(il,kl)=tair2D(il,kl)+r_LcCp  *qidep
 
!  Full Debug
!  ~~~~~~~~~~
c #WQ       write(6,*) 'QiDep', qidep,
c #WQ.                ' Qi'   ,  qi2D(il,kl),
c #WQ.                ' CcnI' ,ccni2D(il,kl),itexpe,il,kl
c #WH       if (il.eq.ilmm) widep(kl)= qidep
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Bergeron Process (water vapor diffu'
c #wH         debugH(36:70)   = 'sion-deposition on ice crystals)   '
c #wH         proc_1          = 'qBerge ICE'
c #wH         procv1          =  qBerge
c #wH         proc_2          = 'Qicnd g/kg'
c #wH         procv2          =  qicnd
c #wH         proc_3          = 'Qidep g/kg'
c #wH         procv3          =  qidep
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(02,kl)   =  qidep + debugV(02,kl)
 
 
            END IF
 
          END DO
          END DO
 
        END IF
 
 
!===============================================================================
 
!  Ice Crystals Sublimation                                  ! BDEPVI
!  Reference: Emde and Kahlig, 1989 p.408 (15)               ! Levkov (27) p.40
!  -------------------------------------------
 
        DO kl=mzhyd,klev
        DO il=1,klon
 
c #wH       qisub           =  0.
 
          IF                          (W2xyz5(il,kl).gt.qv2D(il,kl))
     .                                                            THEN
 
            qvdfci        =            W2xyz5(il,kl) -  qv2D(il,kl)
! #pp       signHN        =  sign(unun,qvdfci)
! #pp       qSubl1        =   max(zero,signHN)
!           qSubl1        =   1.0 if   W2xyz5(il,kl) >  qv2D(il,kl)
!                         =   0.0 otherwise
 
          IF                            (qi2D(il,kl).gt.eps9)     THEN
 
            signHN        =  sign(unun,  qi2D(il,kl)  - eps9)
            qSubl2        =   max(zero,signHN)
!           qSubl2        =   1.0 if     qi2D(il,kl)  > eps9
!                         =   0.0 otherwise
 
            qSubli        =            qSubl2
! #pp.                    *   qSubl1
 
c #EW      IF(qSubli.gt.epsi)                                     THEN ! ctr
c #EW       mauxEW        =  mphy2D(il )
c #EW       mauxEW(05:05) = 'V'
c #EW       mphy2D(il )   =  mauxEW
c #EW      END IF                                                      ! ctr
 
            demde = 1.1d+4
            sat   = qv2D(il,kl) /     W2xyz5(il,kl)
            ab1   = 6.959d+11   /    (tair2D(il,kl)*tair2D(il,kl))
!                   6.959e+11
!                 = [Ls=2833600J/kg] * Ls / [kT=0.025W/m/K] / [Rv=461.J/kg/K]
!                                            kT: Air thermal Conductivity
            ab2   = 1.0d0 / (1.875d-2*rolv2D(il,kl)*W2xyz5(il,kl))
!                            1.875d-5: Water Vapor Diffusivity in Air
            pisub = (1-sat) *4.d0 *demde *W2xyz2(il,kl) /(ab1+ab2)
            qisub = pisub *xt
            qisub = max(qisub, -qv2D(il,kl))  ! H2O deposit.limit = H2O content
            qisub = min(qisub,  qi2D(il,kl))  ! qi  sublim. limit = qi  content
            qisub = min(qisub,  qvdfci     )  ! qi  sublim. limit = Saturation
     .                * qSubli
 
                qi2D(il,kl) =   qi2D(il,kl) -           qisub
               dqi2D(il,kl) =  dqi2D(il,kl) -           qisub
                qv2D(il,kl) =   qv2D(il,kl) +           qisub
              tair2D(il,kl) = tair2D(il,kl) - r_LsCp   *qisub
 
!  Full Debug
!  ~~~~~~~~~~
c #WQ       write(6,*) 'QiSub', qisub,
c #WQ.                ' Qi'   ,  qi2D(il,kl),
c #WQ.                ' CcnI' ,ccni2D(il,kl),itexpe,il,kl
c #WH       IF (il.eq.ilmm) wisub(kl) = qisub
 
          END IF
          END IF
 
C +--Debug
C +  ~~~~~
c #wH         debugH( 1:35)   = 'Emde and Kahlig: Ice Crystals Subli'
c #wH         debugH(36:70)   = 'mation                             '
c #wH         proc_1          = 'Qisub g/kg'
c #wH         procv1          =  qisub
c #wH         proc_2          = 'R.Hum I[%]'
c #wH         procv2          =  0.1 * sat
c #wH         proc_3          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(03,kl)   = -qisub
 
        END DO
        END DO
 
        DO kl=mzhyd,klev
        DO il=1,klon
            IF (qi2D(il,kl).le.0.e0)                              THEN
                qi2D(il,kl) =  0.e0
              ccni2D(il,kl) =  0.e0
            END IF
        END DO
        END DO
 
 
!===============================================================================
 
!  Ice Crystals Instantaneous Melting
!  ----------------------------------
 
        DO kl=mzhyd,klev
        DO il=1,klon
 
c #wH       qimlt           =  0.
c #wH       cimlt           =  0.
 
          IF                          (W2xyz1(il,kl).gt.0.e0)     THEN
 
            signHN        =  sign(unun,W2xyz1(il,kl)        )
            qMelt1        =   max(zero,signHN)
!           qMelt1        =   1.0 if   W2xyz1(il,kl) >  0.e0
!                         =   0.0 otherwise
 
          IF                            (qi2D(il,kl).gt.eps9)     THEN
 
            signHN        =  sign(unun,  qi2D(il,kl) -  eps9)
            qMelt2        =   max(zero,signHN)
!           qMelt2        =   1.0 if     qi2D(il,kl) >  eps9
!                         =   0.0 otherwise
 
            qMelt         =   qMelt1 * qMelt2
 
c #EW      IF(qMelt .gt.epsi)                                     THEN ! ctr
c #EW       mauxEW        =  mphy2D(il )
c #EW       mauxEW(06:06) = 'w'
c #EW       mphy2D(il )   =  mauxEW
c #EW      END IF                                                      ! ctr
 
            qxmlt =        W2xyz1(il,kl) / r_LcCp
            qimlt =    min(  qi2D(il,kl) ,          qxmlt) *qMelt
            cimlt         =ccni2D(il,kl) *          qimlt
     .                  /max(qi2D(il,kl) , eps9)
              qi2D(il,kl) =  qi2D(il,kl) -          qimlt
            ccni2D(il,kl) =ccni2D(il,kl) -          cimlt
              qw2D(il,kl) =  qw2D(il,kl) +          qimlt
            tair2D(il,kl) =tair2D(il,kl) - r_LcCp  *qimlt
 
!  Full Debug
!  ~~~~~~~~~~
c #WQ       write(6,*) 'QiMlt', qimlt,
c #WQ.                ' Qi'   ,  qi2D(il,kl),
c #WQ.                ' CcnI' ,ccni2D(il,kl),itexpe,il,kl
c #WH       IF (il.eq.ilmm) wimlt(kl) = qimlt
 
          END IF
          END IF
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Emde and Kahlig: Ice Crystals Insta'
c #wH         debugH(36:70)   = 'ntaneous Melting                   '
c #wH         proc_1          = 'Qimlt g/kg'
c #wH         procv1          =  qimlt
c #wH         proc_2          = 'cimlt /e15'
c #wH         procv2          =  cimlt*1.e-18
c #wH         proc_3          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(04,kl)   = -qimlt
 
        END DO
        END DO
 
 
!===============================================================================
 
!  Water Vapor Condensation / Evaporation (Fractional Cloudiness)
!  Reference: Laurent Delobbe Thesis (Ek&Mahrt91)
!  --------------------------------------------------------------
 
          DO kl=mzhyd,klev             ! Zeroing needed since
          DO il=1,klon                 ! cfra2D  build  from
            cfra2D(il,kl) =  0.0       ! a maximization process
!           cfra2D: Cloud Fraction     !
 
          END DO
          END DO
 
        IF (fracld.AND.fracSC)                                    THEN
 
          DO kl=mzhyd,klev
          DO il=1,klon
 
c #wH         dqw           =  0.
 
            IF (W2xyz1(il,kl).ge.tsfo)                            THEN
 
c #EW        IF(W2xyz1(il,kl).ge.tsfo)                            THEN ! ctr
c #EW         mauxEW        =  mphy2D(il )
c #EW         mauxEW(07:07) = 'W'
c #EW         mphy2D(il )   =  mauxEW
c #EW        END IF                                                    ! ctr
 
              signHN        =  sign(unun,W2xyz1(il,kl) - tsfo)
              qHeNu1        =   max(zero,signHN)
!             qHeNu1        =   1.0 if   W2xyz1(il,kl) > tsfo
!                           =   0.0 otherwise
 
              qt =   qv2D(il,kl) +              qw2D(il,kl)
!             qt : Total Water Mixing Ratio
 
              tl = tair2D(il,kl) -  r_LvCp    * qw2D(il,kl)
!             tl : Liquid Temperature
 
!  Saturation specific humidity over water,
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ corresponding to liquid temperature
!                               ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
              pa_hPa =(pst2Dn(il)  * sigma(kl) + ptopDY) * 10.0d0
              es_hPa = 6.1078d0 * exp (ExpWat*  log(WatIce     /tl))
     .                          * exp (ExpWa2*(unun/WatIce-unun/tl))
!             Dudhia (1989) JAS, (B1) and (B2) p.3103
!             See also Pielke (1984), p.234 and Stull (1988), p.276
 
              qsl    = .622d0*es_hPa /(pa_hPa - .378d0*es_hPa)
!             Saturation Vapor Specific Concentration over Water
!             (even for temperatures less than freezing point)
 
!  Partial Condensation/Scheme
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~
              dqt = qv2D(il ,MIN(kl+1,klev))-qv2D(il,kl)
     .            + qw2D(il ,MIN(kl+1,klev))-qw2D(il,kl)
              wqt = TUkv2D(il,kl)*dqt
     .           /(gplv2D(il,kl+1)-gplv2D(il,kl))*gravit
 
              ww  = 0.66d+0 * ect_2D(il,kl)
!             ww  : Vertical Velocity Variance
 
              coefC2 = wqt/(sqrt(ww)*qsl)
              sig2rh = C1_EkM + C2_EkM * coefC2 * coefC2
!             sig2rh : Relative Humidity Variance
!                     (Ek and Mahrt, 1991, An. Geoph., 9, 716--724)
 
              sigqt  = sqrt(sig2rh) *qsl
!             sigqt  : Total Water       Variance
 
              argerf = (qt-qsl)/(1.414d+0*sigqt)
              err    = erf(argerf)
 
              cfra2D(il,kl) = 0.5d+0 * (1.d+0 + err)
!             cfra2D: Cloud Fraction
 
              alpha  = 1.d+0/(1.d+0+1.349d7*qsl/(tl*tl))
              t1     = sigqt/sqrt(pi+pi)* exp(-min(argerf*argerf
     .                                            ,argmax       ))
              t2     = cfra2D(il,kl)*(qt-qsl)
 
              signFR = sign(unun,cfra2D(il,kl) - cminHY)
              cfraOK =  max(zero,signFR)
!             cfraOK =   1.0 if  cfra2D(il,kl) > cminHY
!                    =   0.0 otherwise
 
              cfra2D(il,kl) =    cfra2D(il,kl) * cfraOK * qHeNu1
              qw_new        =  alpha * (t1+t2) * cfraOK
!             qw_new        :  Mesh Averaged Liquid Water Mixing Ratio
 
              dqw           =    qw_new   -  qw2D(il,kl)
 
!  Vectorisation of the Atmospheric Water Update
!  ~~~~~~~~~~~~~+-------------------------------------------+
!               |       if (dqw.gt.0.d0)             then   |
!               |        dqw = min(qv2D(il,kl), dqw)        |
!               |       else                                |
!               |        dqw =-min(qw2D(il,kl),-dqw)        |
!               |       end if                              |
!               +-------------------------------------------+
 
              signdq        =    sign(unun,dqw)
              fac_qv        =     max(zero,signdq)
              updatw        =    fac_qv *    qv2D(il,kl)
     .                  + (1.d0 -fac_qv)*    qw2D(il,kl)
! #kk         SCuLim        =        exp(min(0.,300.-tair2D(il,kl))) ! SCu Lim.
              dqw           =    signdq *min(updatw,signdq*dqw)
     .                          *qHeNu1
! #kk.                                             *SCuLim           ! SCu
! #kk         cfra2D(il,kl) =    cfra2D(il,kl)                       !
! #kk.                                             *SCuLim           ! Limitor
 
!  Update of qv2D, qw2D and tair2D
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                qw2D(il,kl) =   qw2D(il,kl) +             dqw
               dqw2D(il,kl) =  dqw2D(il,kl) +             dqw
                qv2D(il,kl) =   qv2D(il,kl) -             dqw
              tair2D(il,kl) = tair2D(il,kl) + r_LvCp    * dqw
 
!  Full Debug
!  ~~~~~~~~~~
c #WQ         write(6,*) 'QwEvp',dqw,itexpe,il,kl
c #WH         if (il.eq.ilmm) wwevp(kl)     = dqw
 
            END IF
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Delobbe: Condensation              '
c #wH         debugH(36:70)   = '                                   '
c #wH         proc_1          = 'dQw   g/kg'
c #wH         procv1          =  dqw
c #wH         proc_2          = '          '
c #wH         procv2          =  0.
c #wH         proc_3          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(05,kl)   =  dqw
 
          END DO
          END DO
 
 
!===============================================================================
 
!  Water Vapor Condensation / Evaporation
!  Reference: Emde and Kahlig 1989, Ann.Geoph. 7, p.407 (7)
!  --------------------------------------------------------
 
        ELSE
 
          DO kl=mzhyd,klev
          DO il=1,klon
 
c #wH         dqw           =  0.
 
            IF (W2xyz1(il,kl).ge.tsfo)                            THEN
 
c #EW        IF(W2xyz1(il,kl).ge.tsfo)                            THEN ! ctr
c #EW         mauxEW        =  mphy2D(il )
c #EW         mauxEW(07:07) = 'W'
c #EW         mphy2D(il )   =  mauxEW
c #EW        END IF                                                    ! ctr
C +
              signHN        =  sign(unun,W2xyz1(il,kl) - tsfo)
              qHeNu1        =   max(zero,signHN)
!             qHeNu1        =   1.0 if   W2xyz1(il,kl) > tsfo
!                           =   0.0 otherwise
 
              dpw = (qv2D(il,kl)  -qvsw2D(il,kl)*rhcrHY)
     .            / (1.0d0+1.349d7*qvsw2D(il,kl)
     .                           /(tair2D(il,kl)*tair2D(il,kl)))
!                          1.349e7=Lv*Lv*0.622/Cpa/Ra with Lv = 2500000 J/kg
 
              dqw = dpw
 
!  Vectorisation of the Atmospheric Water Update
!  ~~~~~~~~~~~~~+-------------------------------------------+
!               |       if (dqw.gt.0.d0)             then   |
!               |        dqw = min(qv2D(il,kl), dqw)        |
!               |       else                                |
!               |        dqw =-min(qw2D(il,kl),-dqw)        |
!               |       end if                              |
!               +-------------------------------------------+
 
              signdq        =    sign(unun,dqw)
              fac_qv        =     max(zero,signdq)
              updatw        =    fac_qv *    qv2D(il,kl)
     .                  + (1.d0 -fac_qv)*    qw2D(il,kl)
              dqw           =    signdq *min(updatw,signdq*dqw)
     .                          *qHeNu1
 
!  Update of qv2D, qw2D and tair2D
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                qw2D(il,kl) =   qw2D(il,kl) +             dqw
               dqw2D(il,kl) =  dqw2D(il,kl) +             dqw
                qv2D(il,kl) =   qv2D(il,kl) -             dqw
              tair2D(il,kl) = tair2D(il,kl) + r_LvCp    * dqw
!            [Ls=2500000J/kg]/[Cp=1004J/kg/K]=2490.04
 
!  Full Debug
!  ~~~~~~~~~~
c #WQ         write(6,*) 'QwEvp',dqw,itexpe,il,kl
c #WH         if (il.eq.ilmm) wwevp(kl) = dqw
 
            END IF
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Emde and Kahlig: Water Vapor Conden'
c #wH         debugH(36:70)   = 'sation / Evaporation               '
c #wH         proc_1          = 'dQw   g/kg'
c #wH         procv1          =  dqw
c #wH         proc_2          = '          '
c #wH         procv2          =  0.
c #wH         proc_3          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(05,kl)   =  dqw
 
          END DO
          END DO
 
        END IF
 
 
!===============================================================================
 
!  Fractional  Cloudiness ! Guess may be computed (Ek&Mahrt91 fracSC=.T.)
!  ====================== ! Final value  computed  below
 
! #sc   IF (fracld.AND..NOT.fracSC)                                 THEN
        IF (fracld)                                                 THEN
         IF(fraCEP) THEN ! ECMWF Large Scale Cloudiness
                         ! ----------------------------
          DO kl=mzhyd,klev
          DO il=1,klon
              cfra2D(il,kl) =             (qi2D(il,kl) +   qw2D(il,kl)
     .                                    +qs2D(il,kl) *  0.33
     .                 * (1.-min(1.,exp((tair2D(il,kl) -258.15)*0.1))))
     .                 / (0.02     *     qvsw2D(il,kl)                )
              cfra2D(il,kl) =min(1.000 , cfra2D(il,kl))
              cfra2D(il,kl) =max(0.001 , cfra2D(il,kl))
     .                     *max(0.,sign(1.,qi2D(il,kl) +   qw2D(il,kl)
     .                                    +qs2D(il,kl) -3.E-9         ))
          END DO
          END DO
         ELSE            ! XU and Randall  1996, JAS 21, p.3099 (4)
                         ! ----------------------------
          DO kl=mzhyd,klev
          DO il=1,klon
              qvs_wi=                                    qvsw2D(il,kl)
c #wi         qvs_wi=max(eps9,((qi2D(il,kl)+qs2D(il,kl))*qvsi2D(il,kl)
c #wi.                         +qw2D(il,kl)             *qvsw2D(il,kl))
c #wi.                /max(eps9,qi2D(il,kl)+qs2D(il,kl) +  qw2D(il,kl)))
              relhum=  min(relCri,      max(qv2D(il,kl) ,qv_MIN)
     .                                                 / qvs_wi)
              argexp=  (  (relCri  -relhum)            * qvs_wi)
     .                         **   0.49
              argexp        =   min(100.* (qi2D(il,kl) +   qw2D(il,kl)
     .                                    +qs2D(il,kl) *  0.33
     .                 * (1.-min(1.,exp((tair2D(il,kl) -258.15)*0.1))))
     .                                /max(eps9        , argexp       )
     .                             ,argmax                             )
 
              cfra2D(il,kl) =      (     relhum       ** 0.25         )
     .                         *   (1.  -   exp(-argexp)              )
          END DO
          END DO
         END IF
 
        ELSE
! #sc   ELSE IF (      .NOT.fracld)                                 THEN
! #sc     IF               (fracSC) stop 'fracSC set up when fracld NOT'
          DO kl=mzhyd,klev
          DO il=1,klon
              qcloud        =              qi2D(il,kl) +   qw2D(il,kl)
            IF                            (qcloud     .gt.eps9)     THEN
 
              signQW        =  sign(unun,  qcloud       - eps9)
              cfra2D(il,kl) =   max(zero,signQW)
!             cfra2D(il,kl) =   1.0 if     qcloud       > eps9
!                           =   0.0 otherwise
 
            END IF
          END DO
          END DO
 
        END IF
 
 
!  Debug
!  ~~~~~
c #wH     DO kl=mzhyd,klev
c #wH     DO il=1,klon
c #wH         debugH( 1:35) = 'Fractional Cloudiness (XU .OR. CEP)'
c #wH         debugH(36:70) = '                                   '
c #wH         proc_1        = '          '
c #wH         procv1        =  0.
c #wH         proc_2        = '          '
c #wH         procv2        =  0.
c #wH         proc_3        = '          '
c #wH         procv3        =  0.
c #wH         proc_4        = '          '
c #wH         procv4        =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     END DO
c #wH     END DO
 
 
!===============================================================================
 
!  Autoconversion Processes (i.e., generation of precipitating particles)
!  ======================================================================
 
!  Cloud Droplets Autoconversion
!  Reference: Lin et al.      1983, JCAM      22, p.1076 (50)
!  ----------------------------------------------------------
 
        DO kl=mzhyd,klev
        DO il=1,klon
 
c #wH       qraut         =   0.0
 
          IF                            (qw2D(il,kl).gt.eps9)     THEN
 
            signAU        =  sign(unun,  qw2D(il,kl)  - eps9)
            AutoOK        =   max(zero,signAU)
!           AutoOK        =   1.0 if     qw2D(il,kl)  > eps9
!                         =   0.0 otherwise
 
          IF                          (cfra2D(il,kl).gt.cminHY)   THEN
 
            signFC        =  sign(unun,cfra2D(il,kl)  - cminHY)
            ClouOK        =   max(zero,signFC)
!           ClouOK        =   1.0 if   cfra2D(il,kl)  > cminHY
!                         =   0.0 otherwise
 
            AutoOK        =  AutoOK  * ClouOK
 
c #EW        IF(AutoOK.gt.epsi)                                   THEN ! ctr
c #EW         mauxEW        =  mphy2D(il )
c #EW         mauxEW(08:08) = 'r'
c #EW         mphy2D(il )   =  mauxEW
c #EW        END IF                                                    ! ctr
 
!  Sundqvist      (1988, Schlesinger, Reidel, p.  433) Autoconversion Scheme
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            dqw    = AutoOK *qw2D(il,kl)/qw00/max(cminHY,cfra2D(il,kl))
            praut  = AutoOK *qw2D(il,kl)*csud*   (1.-exp(-min(dqw*dqw
     .                                                       ,argmax)))
     .                                       /max(cminHY,cfra2D(il,kl))
 
!  Liou and Ou    (1989, JGR  94, p. 8599) Autoconversion Scheme
!  Boucher et al. (1995, JGR 100, p.16395) ~~~~~~~~~~~~~~~~~~~~~
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #LO       ccnw2D(il,kl) = 1.2e+8 ! ASTEX (Duynkerke&al.1995, JAS 52, p.2763)
! #LO       ccnw2D(il,kl) = 1.e+11 !       (polluted air, Rogers&Yau 89, p.90)
 
c #LO       qwclou        =  qw2D(il,kl) / cfra2D(il,kl)
c #LO       dmed0         = 4.5d0        *sigmaw         *sigmaw
c #LO       dmede         = qwclou       *rolv2D(il,kl)
c #LO.                    *  6.d+0/pi    /ccnw2D(il,kl)  /exp(dmed0)
c #LO       dmed          = exp(third*log(dmede))
c #LO       dmed2         =               dmed           *dmed
c #LO       dw0           = 8.0d+0       *sigmaw         *sigmaw
c #LO       dw4           = exp(dw0)     *dmed2          *dmed2
c #LO       rwbar         = 0.5d+0       *sqrt(sqrt(dw4))
 
c #LO       signHV        = sign(unun, rwbar-rcrilo)
c #LO       heavi         =  max(zero,signHV       )
!           heavi         : Heaviside Function
 
c #LO       praut         = AutoOK *cfra2D(il,kl) * heavi *4.09d6 *pi
c #LO.                             *ccnw2D(il,kl) * dw4   *qwclou
 
!  Lin et al.(1983) Autoconversion Scheme
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #LI       dqw       = AutoOK * (qw2D(il,kl)-qw00)
c #LI       praut     =          dqw*dqw*dqw/(cc1*dqw+1000.d0*cc2/dd0)
 
            qraut     = praut * xt
            qraut     = min(qraut,qw2D(il,kl))
            qw2D(il,kl) = qw2D(il,kl) - qraut
            qr2D(il,kl) = qr2D(il,kl) + qraut
 
c #WQ       write(6,*) 'QrAut',qraut,itexpe,il,kl
c #WH       if (il.eq.ilmm) wraut(kl) = qraut
 
          END IF
          END IF
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Lin et al.(1983) Autoconversion Sch'
c #wH         debugH(36:70)   = 'eme                                '
c #wH         proc_1          = 'Qraut g/kg'
c #wH         procv1          =  qraut
c #wH         proc_2          = '          '
c #wH         procv2          =  0.
c #wH         proc_3          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(06,kl)   =  qraut
 
        END DO
        END DO
 
 
!  Conversion from Cloud Ice Crystals to Snow Flakes
!  Reference: Levkov et al.   1992, Contr.Atm.Phys. 65, p.41
!  ---------------------------------------------------------
 
        IF (LevkovAUTO)                                           THEN
 
 
!  Depositional Growth: Ice Crystals  => Snow Flakes     (BDEPIS)
!  Reference: Levkov et al.   1992, Contr.Atm.Phys. 65, p.41 (28)
!  --------------------------------------------------------------
 
         IF(LevkovAUTX)                                           THEN
 
          DO kl=mzhyd,klev
          DO il=1,klon
 
c #wH         qsaut       =   0.0
 
            IF                          (qi2D(il,kl).gt.eps9)     THEN
 
              signAU      =  sign(unun,  qi2D(il,kl)  - eps9)
              AutoOK      =   max(zero,signAU)
!             AutoOK      =   1.0 if     qi2D(il,kl)  > eps9
!                         =   0.0 otherwise
 
            IF                        (ccni2D(il,kl).gt.1.e0)     THEN
 
              signCC      =  sign(unun,ccni2D(il,kl)  - 1.e0)
              ClouOK      =   max(zero,signCC)
!             ClouOK      =   1.0 if   ccni2D(il,kl)  > 1.e0
!                         =   0.0 otherwise
 
              AutoOK      =  AutoOK  * ClouOK
                qiOK      =  AutoOK  *   qi2D(il,kl)
 
!  Pristine Ice Crystals Diameter
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
              qid   = 0.156 *exp(third*log(thous*rolv2D(il,kl)
     .                                *max(eps9 ,  qi2D(il,kl))
     .                                /max(unun ,ccni2D(il,kl))))
!             qid   : Pristine Ice Crystals Diameter
!                     Levkov et al. 1992, Contr. Atm. Phys. 65, (5) p.37
!                     where 6/(pi*ro_I)**1/3 ~ 0.156
 
!  Deposition Time Scale
!  ~~~~~~~~~~~~~~~~~~~~~
              sat   = max(epsq, qv2D(il,kl))   / W2xyz5(il,kl)
c #a1         a1saut= max(eps9, sat-1.)
c #a1.              /(0.702e12/(tair2D(il,kl)*tair2D(il,kl))
!                     0.702e12 ~ 0.701987755e12 = (2.8345e+6)**2/0.0248/461.5
!                                                  Ls_H2O    **2/Ka    /Rw
c #a1.               +1.0     /(2.36e-2 *rolv2D(il,kl)*qv2D(il,kl)*sat))
!                               2.36e-2= 2.36e-5      *1.e3
!                                        Dv
 
              xtsaut= 0.125   *(qsd0*qsd0-qid*qid)
     .              *(0.702e12/(tair2D(il,kl)*tair2D(il,kl))
!                     0.702e12 ~ 0.701987755e12 = (2.8345e+6)**2/0.0248/461.5
!                                                  Ls_H2O    **2/Ka    /Rw
     .               +1.0     /(2.36e-2 *rolv2D(il,kl)
     .                           *max(epsq,qv2D(il,kl))*sat))
!                               2.36e-2= 2.36e-5       *1.e3
!                                        Dv
 
!  Deposition
!  ~~~~~~~~~~
               qsaut =    xt *qiOK*(sat-1.)/xtsaut
               qsaut =   min( qi2D(il,kl)  , qsaut)
               qsaut =   max(-qs2D(il,kl)  , qsaut)
                qi2D(il,kl) = qi2D(il,kl)  - qsaut
                qs2D(il,kl) = qs2D(il,kl)  + qsaut
 
            END IF
            END IF
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Lin et al.(1983) Depositional Growt'
c #wH         debugH(36:70)   = 'h                                  '
c #wH         proc_1          = 'Qsaut g/kg'
c #wH         procv1          =  qsaut
c #wH         proc_2          = '          '
c #wH         procv2          =  0.
c #wH         proc_3          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(07,kl)   =  qsaut
 
          END DO
          END DO
 
         END IF
 
 
!  Ice Crystals Aggregation           => Snow Flakes     (BAGRIS)
!  Reference: Levkov et al.   1992, Contr.Atm.Phys. 65, p.41 (31)
!  --------------------------------------------------------------
 
          DO kl=mzhyd,klev
          DO il=1,klon
 
c #wH          qsaut      =   0.0
c #wH         xtsaut      =   0.0
 
            IF                          (qi2D(il,kl).gt.eps9)     THEN
 
              signAU      =  sign(unun,  qi2D(il,kl)  - eps9)
              AutoOK      =   max(zero,signAU)
!             AutoOK      =   1.0 if     qi2D(il,kl)  > eps9
!                         =   0.0 otherwise
 
            IF                        (ccni2D(il,kl).gt.1.e0)     THEN
 
              signCC      =  sign(unun,ccni2D(il,kl)  - 1.e0)
              ClouOK      =   max(zero,signCC)
!             ClouOK      =   1.0 if   ccni2D(il,kl)  > 1.e0
!                         =   0.0 otherwise
 
              AutoOK      =  AutoOK  * ClouOK
                qiOK      =  AutoOK  *   qi2D(il,kl)
 
c #EW        IF(AutoOK.gt.epsi)                                   THEN
c #EW         mauxEW        =  mphy2D(il )
c #EW         mauxEW(09:09) = 's'
c #EW         mphy2D(il )   =  mauxEW
c #EW        END IF
 
!  Pristine Ice Crystals Diameter
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
              qid   = 0.156 *exp(third*log(thous*rolv2D(il,kl)
     .                                 *max(eps9,  qi2D(il,kl))
     .                                 /max(unun,ccni2D(il,kl))))
!             qid   : Pristine Ice Crystals Diameter
!                     Levkov et al. 1992, Contr. Atm. Phys. 65, (5) p.37
!                     where [6/(pi*ro_I)]**1/3 ~ 0.156
 
!  Time needed for Ice Crystals Diameter to reach Snow Diameter Threshold
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
              c1saut = max(eps9,qiOK)*rolv2D(il,kl) *35.0
     .                *exp(third *log(rolv2D(il,klev)/rolv2D(il,kl)))
 
              xtsaut =-6.d0*log(qid/qsd0) /c1saut
              xtsaut = max(xt,             xtsaut) ! qi fully used if xtsaut<xt
 
!  Time needed for Ice Crystals Diameter to reach Snow Diameter Threshold
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~(ALTERNATE PARAMETERIZATION)~
! #nt         xtsaut =-2.0 * (3.0*log(    qid        /qsd0)
! #nt.                       +    log(max(qi2D(il,kl),eps9))) /c1saut
! #nt         xtsaut = max(eps9,xtsaut)
 
!  Aggregation
!  ~~~~~~~~~~~
               qsaut =     xt*qiOK        /xtsaut
               qsaut =   min( qi2D(il,kl) , qsaut)
               qsaut =   max(-qs2D(il,kl) , qsaut)
                qi2D(il,kl) = qi2D(il,kl) - qsaut
                qs2D(il,kl) = qs2D(il,kl) + qsaut
 
 
!  Decrease of Ice Crystals Number                       (BAGRII)
!  Reference: Levkov et al.   1992, Contr.Atm.Phys. 65, p.41 (34)
!  --------------------------------------------------------------
 
              ccni2D(il,kl) = ccni2D(il,kl) * exp(-0.5*c1saut*xt)
 
c #WQ          write(6,*) 'QsAut', qsaut,
c #WQ.                   ' Qi'   ,  qi2D(il,kl),
c #WQ.                   ' CcnI' ,ccni2D(il,kl),itexpe,il,kl
c #WH         if (il.eq.ilmm) wsaut(kl) = qsaut
 
            END IF
            END IF
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Lin et al.(1983) Ice Crystals Aggre'
c #wH         debugH(36:70)   = 'gation                             '
c #wH         proc_1          = 'xtsaut sec'
c #wH         procv1          =  xtsaut
c #wH         proc_2          = 'Qsaut g/kg'
c #wH         procv2          =  qsaut
c #wH         proc_3          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(07,kl)   =  qsaut + debugV(07,kl)
 
          END DO
          END DO
 
 
!===============================================================================
 
!  Ice Crystals Autoconversion => Snow Flakes
!  Reference: Lin et al.      1983, JCAM      22, p.1070 (21)
!             Lin et al.      1983, JCAM      22, p.1074 (38)
!             Emde and Kahlig 1989, Ann.Geoph. 7, p. 408 (18)
!  ----------------------------------------------------------
 
        ELSE IF (EmdeKa)                                          THEN
 
          DO kl=mzhyd,klev
          DO il=1,klon
 
c #wH            qsaut      =  0.0
c #wH           cnsaut      =  0.0
 
            IF (qi2D(il,kl) .ge. qi00)                            THEN
 
c #EW        IF(qi2D(il,kl) .ge. qi00)                            THEN
c #EW         mauxEW        =  mphy2D(il )
c #EW         mauxEW(09:09) = 's'
c #EW         mphy2D(il )   =  mauxEW
c #EW        END IF
 
               ex1          = 0.025d0* W2xyz1(il,kl)      ! W2 = t?[K]
               psaut        = 0.001d0*(  qi2D(il,kl)-qi00)*exp(ex1)
               qsaut        =     psaut  * xt
               qsaut        = max(qsaut, zero       )
               qsaut        = min(qsaut, qi2D(il,kl))
              cnsaut        = ccni2D(il,kl) * qsaut
     .                       /max(qi00 , qi2D(il,kl))
              ccni2D(il,kl) = ccni2D(il,kl) -cnsaut
                qi2D(il,kl) =   qi2D(il,kl) - qsaut
                qs2D(il,kl) =   qs2D(il,kl) + qsaut
c #WQ         write(6,*) 'QsAut',qsaut,itexpe,il,kl
c #WH         IF (il.eq.ilmm) wsaut(kl)=qsaut
            END IF
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Emde and Kahlig  Ice Crystals Autoc'
c #wH         debugH(36:70)   = 'onversion                          '
c #wH         proc_1          = 'Qsaut g/kg'
c #wH         procv1          =  qsaut
c #wH         proc_2          = 'cnsaut/e15'
c #wH         procv2          =  cnsaut*1.e-18
c #wH         proc_3          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(07,kl)   =  qsaut
 
          END DO
          END DO
 
 
!  Sundqvist      (1988, Schlesinger, Reidel, p.  433) Autoconversion Scheme
!  -------------------------------------------------------------------------
 
        ELSE
 
          DO kl=mzhyd,klev
          DO il=1,klon
 
c #wH            qsaut      =  0.0
c #wH           cnsaut      =  0.0
 
            IF                          (qi2D(il,kl).gt.eps9)     THEN
 
              signAU      =  sign(unun,  qi2D(il,kl)  - eps9)
              AutoOK      =   max(zero,signAU)
!             AutoOK      =   1.0 if     qi2D(il,kl)  > eps9
!                         =   0.0 otherwise
 
              dqi   = AutoOK *qi2D(il,kl)/qi0S
! #mf.                                        /max(cminHY,cfra2D(il,kl))
              psaut = AutoOK *qi2D(il,kl)*csud
     .                       *(1.-exp(-dqi*dqi))
! #mf.                                        *max(cminHY,cfra2D(il,kl))
              qsaut =                       psaut * xt
              qsaut =   min(  qi2D(il,kl) , qsaut)
              qsaut =   max(  zero        , qsaut)
             cnsaut =       ccni2D(il,kl) * qsaut
     .                                  /max(qi2D(il,kl),eps9)
             ccni2D(il,kl) = ccni2D(il,kl) -cnsaut
               qi2D(il,kl) =   qi2D(il,kl) - qsaut
               qs2D(il,kl) =   qs2D(il,kl) + qsaut
 
            END IF
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Sundqvist (1988) Ice Crystals Autoc'
c #wH         debugH(36:70)   = 'onversion                          '
c #wH         proc_1          = 'Qsaut g/kg'
c #wH         procv1          =  qsaut
c #wH         proc_2          = 'cnsaut/e15'
c #wH         procv2          =  cnsaut*1.e-18
c #wH         proc_3          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(07,kl)   =  qsaut
 
          END DO
          END DO
        END IF
 
 
!  Ice Crystals Autoconversion => Graupels
!  ---------------------------------------
 
! #qg   DO kl=mzhyd,klev
! #qg   DO il=1,klon
 
! #qg     IF (qi2D(il,kl) .ge. qg00)                              THEN
 
! #qg       ex1       = 0.090*  W2xyz1(il,kl)
! #qg       pgaut     = 0.001*(   qi2D(il,kl)-qg00)*exp(ex1)
! #qg       qgaut     =     pgaut * xt
! #qg       qgaut     = max(qgaut,zero       )
! #qg       qgaut     = min(qgaut,qi2D(il,kl))
! #qg       qi2D(il,kl) = qi2D(il,kl) - qgaut
! #qg       qg2D(il,kl) = qg2D(il,kl) + qgaut
 
! #qg     END IF
 
! #qg   END DO
! #qg   END DO
 
 
!===============================================================================
 
!  Accretion Processes (i.e. increase in size of precipitating particles
!  ====================      through a collision-coalescence process)===
!                      ==============================================
 
!  Accretion of Cloud Droplets by Rain
!  Reference: Lin et al.      1983, JCAM      22, p.1076 (51)
!             Emde and Kahlig 1989, Ann.Geoph. 7, p. 407 (10)
!  ----------------------------------------------------------
 
        DO kl=mzhyd,klev
        DO il=1,klon
 
c #wH       qracw       =   0.0
 
          IF                          (qw2D(il,kl).gt.eps9)       THEN
 
            sign_W      =  sign(unun,  qw2D(il,kl)  - eps9)
            WbyR_w      =   max(zero,sign_W)
!           WbyR_w      =   1.0 if     qw2D(il,kl)  > eps9
!                       =   0.0 otherwise
 
          IF                        (W2xyz3(il,kl).gt.eps9)       THEN
 
            sign_R      =  sign(unun,W2xyz3(il,kl)  - eps9)
            WbyR_r      =   max(zero,sign_R)
!           WbyR_r      =   1.0 if   W2xyz3(il,kl)  > eps9
!                       =   0.0 otherwise
 
            WbyROK      =  WbyR_w *  WbyR_r
 
c #EW      IF(WbyROK.gt.epsi)                                     THEN ! ctr
c #EW       mauxEW        =  mphy2D(il )
c #EW       mauxEW(10:10) = 'r'
c #EW       mphy2D(il )   =  mauxEW
c #EW      END IF                                                      ! ctr
 
            pracw = 3104.28d0  * cnor * W2xyz6(il,kl)
     .       *qw2D(il,kl)/exp(3.8d0*log(W2xyz7(il,kl)))
!                   3104.28 = a pi Gamma[3+b] / 4
!                      where  a = 842. and b = 0.8
            qracw =     pracw * xt    * WbyROK
            qracw = min(qracw,qw2D(il,kl))
 
            qw2D(il,kl) = qw2D(il,kl) - qracw
            qr2D(il,kl) = qr2D(il,kl) + qracw
 
c #WQ       write(6,*) 'Qracw',qracw,itexpe,il,kl
c #WH       if (il.eq.ilmm) wracw(kl) = qracw
 
          END IF
          END IF
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Lin et al.(1983): Accretion of Clou'
c #wH         debugH(36:70)   = 'd Droplets by Rain                 '
c #wH         proc_1          = 'Qracw g/kg'
c #wH         procv1          =  qracw
c #wH         proc_2          = '          '
c #wH         procv2          =  0.
c #wH         proc_3          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(08,kl)   =  qracw
 
        END DO
        END DO
 
 
!  Accretion of Cloud Droplets by Snow Flakes
!  Reference: Lin et al.      1983, JCAM      22, p.1070 (24)
!  ----------------------------------------------------------
 
        DO kl=mzhyd,klev
        DO il=1,klon
 
c #wH       snoA        =   0.0
 
          IF                          (qw2D(il,kl).gt.eps9)       THEN
 
            sign_W      =  sign(unun,  qw2D(il,kl)  - eps9)
            WbyS_w      =   max(zero,sign_W)
!           WbyS_w      =   1.0 if     qw2D(il,kl)  > eps9
!                       =   0.0 otherwise
 
          IF                        (W2xyz4(il,kl).gt.eps9)       THEN
 
            sign_S      =  sign(unun,W2xyz4(il,kl)  - eps9)
            WbyS_s      =   max(zero,sign_S)
!           WbyS_s      =   1.0 if   W2xyz4(il,kl)  > eps9
!                       =   0.0 otherwise
 
            WbySOK      =  WbyS_w *  WbyS_s
 
c #EW      IF(WbySOK.gt.epsi)                                     THEN ! ctr
c #EW       mauxEW        =  mphy2D(il )
c #EW       mauxEW(11:11) = 's'
c #EW       mphy2D(il )   =  mauxEW
c #EW      END IF                                                      ! ctr
 
c #cn       cnos        = min(2.e8
c #cn.                       ,cnos2*exp(-.12*min(0.,W2xyz1(il,kl))))
            psacw(il,kl)= 9.682d0 * cnos * W2xyz6(il,kl)
     .         *qw2D(il,kl)/exp(3.25d0*log(W2xyz8(il,kl)))
!           psacw         : taken into account in the snow melting process
!                           (if positive temperatures)
!                           9.682 = c pi  Gamma[3+d] / 4
!                           where   c = 4.836 and d = 0.25
!                     (Locatelli and Hobbs, 1974, JGR: table 1 p.2188:
!                      Graupellike Snow Flakes of Hexagonal Type)
 
            psacw(il,kl)= 3517.   * cnos * W2xyz6(il,kl)
     .         *qw2D(il,kl)/exp(3.99d0*log(W2xyz8(il,kl)))
!           psacw         : taken into account in the snow melting process
!                           (if positive temperatures)
!                           3517. = c pi  Gamma[3+d] / 4
!                           where   c = 755.9 and d = 0.99
!                     (Locatelli and Hobbs, 1974, JGR: table 1 p.2188:
!                      Unrimed Side Planes)
 
c #ur       psacw(il,kl)= 27.73   * cnos * W2xyz6(il,kl)
c #ur.         *qw2D(il,kl)/exp(3.41d0*log(W2xyz8(il,kl)))
!           psacw         : taken into account in the snow melting process
!                           (if positive temperatures)
!                           27.73 = c pi  Gamma[3+d] / 4
!                           where   c = 11.718and d = 0.41
!                     (Locatelli and Hobbs, 1974, JGR: table 1 p.2188:
!                      Aggregates of unrimed radiating assemblages)
 
            qsacw       =     psacw(il,kl) *xt * WbySOK
            qsacw       = min(qsacw,qw2D(il,kl))
 
            sign_T      =  sign(unun,tair2D(il,kl) - TfSnow)
            Fact_R      =   max(zero,sign_T)
!           Fact_R      =   1.0 if   tair2D(il,kl) > TfSnow
!                       =   0.0 otherwise
 
              qw2D(il,kl) =   qw2D(il,kl) -                   qsacw
              qr2D(il,kl) =   qr2D(il,kl) +         Fact_R  * qsacw
              SnoA        =                 (1.d0 - Fact_R) * qsacw
              qs2D(il,kl) =   qs2D(il,kl) +                   SnoA
            tair2D(il,kl) = tair2D(il,kl) +  r_LcCp         * SnoA
!           Negative Temperatures => Latent Heat is released by Freezing
 
!  Full Debug
!  ~~~~~~~~~~
c #WQ       write(6,*) 'Qsacw',qsacw,itexpe,il,kl
c #WH       if (il.eq.ilmm) wsacw(kl) = qsacw
 
          END IF
          END IF
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Lin et al.(1983): Accretion of Clou'
c #wH         debugH(36:70)   = 'd Droplets by Snow Particles       '
c #wH         proc_1          = 'Qsacw g/kg'
c #wH         procv1          =  SnoA
c #wH         proc_3          = '          '
c #wH         procv2          =  0.
c #wH         proc_2          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(09,kl)   =  SnoA
 
        END DO
        END DO
 
 
!  Accretion of Cloud Droplets by Graupels (Dry Growth Mode)
!  Reference: Lin et al.      1983, JCAM      22, p.1075 (40)
!             Emde and Kahlig 1989, Ann.Geoph. 7, p. 407 (~20)
!  -----------------------------------------------------------
 
! #qg   DO kl=mzhyd,klev
! #qg   DO il=1,klon
 
! #qg     IF                          (qw2D(il,kl).gt.eps9)       THEN
 
! #qg       sign_W      =  sign(unun,  qw2D(il,kl)  - eps9)
! #qg       WbyG_w      =   max(zero,sign_W)
!           WbyG_w      =   1.0 if     qw2D(il,kl)  > eps9
!                       =   0.0 otherwise
 
! #qg     IF                          (qg2D(il,kl).gt.eps9)       THEN
 
! #qg       sign_G      =  sign(unun,  qg2D(il,kl)  - eps9)
! #qg       WbyG_g      =   max(zero,sign_G)
!           WbyG_g      =   1.0 if     qg2D(il,kl)  > eps9
!                       =   0.0 otherwise
 
! #qg       WbyGOK      =  WbyG_w *  WbyG_g
 
! #qg     IF                        (tair2D(il,kl).lt.TfSnow)     THEN
 
! #qg       sign_T      = -sign(unun,tair2D(il,kl)  - TfSnow)
! #qg       Fact_G      =   max(zero,sign_T)
!           Fact_G      =   1.0 if   tair2D(il,kl)  > TfSnow
!                       =   0.0 otherwise
 
! #qg        pgacw      = ???
! #qg        qgacw      =     pgacw * xt * WbyGOK
! #qg        qgacw      = min(qgacw,qw2D(il,kl))
 
! #qg         qw2D(il,kl) =   qw2D(il,kl) -       qgacw
! #qg         qg2D(il,kl) =   qg2D(il,kl) +       qgacw
! #qg       tair2D(il,kl) = tair2D(il,kl) +r_LcCp  gacw
 
! #qg     END IF
! #qg     END IF
! #qg     END IF
 
! #qg   END DO
! #qg   END DO
 
 
!  Accretion of Cloud Ice      by Snow Particles
!  Reference: Lin et al.      1983, JCAM      22, p.1070 (22)
!  ----------------------------------------------------------
 
        DO kl=mzhyd,klev
        DO il=1,klon
 
c #wH       qsaci       =   0.0
c #wH       cnsaci      =   0.0
 
          IF                          (qi2D(il,kl).gt.eps9)       THEN
 
            sign_C      =  sign(unun,  qi2D(il,kl)  - eps9)
            CbyS_c      =   max(zero,sign_C)
!           CbyS_c      =   1.0 if     qi2D(il,kl)  > eps9
!                       =   0.0 otherwise
 
          IF                        (W2xyz4(il,kl).gt.eps9)       THEN
 
            sign_S      =  sign(unun,W2xyz4(il,kl)  - eps9)
            CbyS_s      =   max(zero,sign_S)
!           CbyS_s      =   1.0 if   W2xyz4(il,kl)  > eps9
!                       =   0.0 otherwise
 
          IF                        (tair2D(il,kl).lt.TfSnow)     THEN
 
            sign_T      = -sign(unun,tair2D(il,kl)  - TfSnow)
            CbyS_T      =   max(zero,sign_T)
!           CbyS_T      =   1.0 if   tair2D(il,kl)  < TfSnow
!                       =   0.0 otherwise
 
            CbySOK      =  CbyS_c *  CbyS_s        * CbyS_T
 
c #EW      IF(CbySOK.gt.epsi)                                     THEN ! ctr
c #EW       mauxEW        =  mphy2D(il )
c #EW       mauxEW(12:12) = 's'
c #EW       mphy2D(il )   =  mauxEW
c #EW      END IF                                                      ! ctr
 
            efc   =  exp(0.025d0*W2xyz1(il,kl))
!           efc   : Collection Efficiency
!                   Lin et al. 1983 JCAM 22 p.1070 (23)
 
c #cn       cnos  = min(2.e8
c #cn.                 ,cnos2*exp(-.12*min(0.,W2xyz1(il,kl))))
            psaci = efc * 9.682d0 * cnos * W2xyz6(il,kl)
     .         *qi2D(il,kl)/exp(3.25d0*log(W2xyz8(il,kl)))
 
            psaci = efc * 3517.d0 * cnos * W2xyz6(il,kl)
     .         *qi2D(il,kl)/exp(3.99d0*log(W2xyz8(il,kl)))
 
c #ur       psaci = efc * 27.73d0 * cnos * W2xyz6(il,kl)
c #ur.         *qi2D(il,kl)/exp(3.41d0*log(W2xyz8(il,kl)))
 
            qsaci =     psaci * xt * CbySOK
            qsaci = min(qsaci,qi2D(il,kl))
 
            cnsaci        = ccni2D(il,kl) * qsaci /max(qi2D(il,kl),eps9)
            ccni2D(il,kl) = ccni2D(il,kl) -cnsaci
              qi2D(il,kl) =   qi2D(il,kl) - qsaci
              qs2D(il,kl) =   qs2D(il,kl) + qsaci
 
c #WQ       write(6,*) 'Qsaci',qsaci,itexpe,il,kl
c #WH       if (il.eq.ilmm) wsaci(kl) = qsaci
 
          END IF
          END IF
          END IF
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Lin et al.(1983): Accretion of Clou'
c #wH         debugH(36:70)   = 'd Ice by Snow Particles            '
c #wH         proc_1          = 'Qsaci g/kg'
c #wH         procv1          =  qsaci
c #wH         proc_2          = 'CNsaci/e15'
c #wH         procv2          =  cnsaci*1.e-18
c #wH         proc_3          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(10,kl)   =  qsaci
 
        END DO
        END DO
 
 
!  Accretion of Cloud Ice      by Graupel (Cloud Ice Sink)
!  Reference: Lin et al.      1983, JCAM      22, p.1075 (41)
!             Emde and Kahlig 1989, Ann.Geoph. 7, p. 407 (~19)
!  -----------------------------------------------------------
 
! #qg   DO kl=mzhyd,klev
! #qg   DO il=1,klon
 
! #qg     IF                          (qw2D(il,kl).gt.eps9)       THEN
 
! #qg       sign_C      =  sign(unun,  qi2D(il,kl)  - eps9)
! #qg       CbyG_c      =   max(zero,sign_C)
!           CbyG_c      =   1.0 if     qi2D(il,kl)  > eps9
!                       =   0.0 otherwise
 
! #qg     IF                          (qg2D(il,kl).gt.eps9)       THEN
 
! #qg       sign_G      =  sign(unun,  qg2D(il,kl)  - eps9)
! #qg       CbyG_g      =   max(zero,sign_G)
!           CbyG_g      =   1.0 if     qg2D(il,kl)  > eps9
!                       =   0.0 otherwise
 
! #qg     IF                        (tair2D(il,kl).lt.TfSnow)     THEN
 
! #qg       sign_T      = -sign(unun,tair2D(il,kl)  - TfSnow)
! #qg       Fact_G      =   max(zero,sign_T)
!           Fact_G      =   1.0 if   tair2D(il,kl)  < TfSnow
!                       =   0.0 otherwise
 
! #qg       CbyGOK      =  CbyG_c *  CbyG_g        * Fact_G
 
! #qg       pgaci = ???
! #qg       qgaci =     pgaci *xt *CbyGOK
! #qg       qgaci = min(qgaci,qi2D(il,kl))
 
! #qg       qi2D(il,kl) = qi2D(il,kl) - qgaci
! #qg       qg2D(il,kl) = qg2D(il,kl) + qgaci
 
! #qg     END IF
! #qg     END IF
! #qg     END IF
 
! #qg   END DO
! #qg   END DO
 
 
!  Accretion of Cloud Ice      by Rain (Cloud Ice Sink)
!  Reference: Lin et al.      1983, JCAM      22, p.1071 (25)
!  ----------------------------------------------------------
 
        DO kl=mzhyd,klev
        DO il=1,klon
 
c #wH       qraci       =   0.0
c #wH       qiacr       =   0.0
 
          IF                          (qi2D(il,kl).gt.eps9)       THEN
 
            sign_C      =  sign(unun,  qi2D(il,kl)  - eps9)
            CbyR_c      =   max(zero,sign_C)
!           CbyR_c      =   1.0 if     qi2D(il,kl)  > eps9
!                       =   0.0 otherwise
 
          IF                        (W2xyz3(il,kl).gt.eps9)       THEN
 
            sign_R      =  sign(unun,W2xyz3(il,kl)  - eps9)
            CbyR_r      =   max(zero,sign_R)
!           CbyR_r      =   1.0 if   W2xyz3(il,kl)  > eps9
!                       =   0.0 otherwise
 
          IF                        (tair2D(il,kl).lt.TfSnow)     THEN
 
            sign_T      = -sign(unun,tair2D(il,kl)  - TfSnow)
            CbyR_T      =   max(zero,sign_T)
!           CbyR_T      =   1.0 if   tair2D(il,kl)  < TfSnow
!                       =   0.0 otherwise
 
            CbyROK      =  CbyR_c *  CbyR_r         * CbyR_T
 
c #EW      IF(CbyROK.gt.epsi)                                     THEN ! ctr
c #EW            mauxEW        =  mphy2D(il )
c #EW        if (mauxEW(13:13).eq.'s'.or.mauxEW(13:13).eq.'A')    then ! ctr
c #EW            mauxEW(13:13) =  'A'
c #EW        else                                                      ! ctr
c #EW            mauxEW(13:13) =  'r'
c #EW        end if                                                    ! ctr
c #EW            mphy2D(il )   =  mauxEW
c #EW      END IF                                                      ! ctr
 
              praci = 3104.28d0  * cnor * W2xyz6(il,kl)
     .         *qi2D(il,kl)/exp(3.8d0*log(W2xyz7(il,kl)))
              qraci =     praci * xt    * CbyROK
              qraci = min(qraci,qi2D(il,kl))
             cnraci         = ccni2D(il,kl)* qraci/max(qi2D(il,kl),eps9)
             ccni2D(il,kl)  = ccni2D(il,kl)-cnraci
               qi2D(il,kl)  =   qi2D(il,kl)- qraci
 
! #qg       IF(qr2D(il,kl) .gt. 1.e-4 )                           then ! ctr
! #qg          qg2D(il,kl) =   qg2D(il,kl) + qraci
!              CAUTION : Graupels Formation is not taken into account  !
!                        This could be a reasonable assumption for Antarctica
 
! #qg       ELSE                                                       ! ctr
              qs2D(il,kl)  =   qs2D(il,kl) + qraci
! #qg       END IF                                                     ! ctr
 
c #WQ       write(6,*) 'Qraci',qraci,itexpe,il,kl
c #WH       if (il.eq.ilmm) wraci(kl) = qraci
 
 
!  Accretion of Rain           by Cloud Ice (Rain Sink)
!  Reference: Lin et al.      1983, JCAM      22, p.1071 (26)
!  ----------------------------------------------------------
 
c #EW      IF(CbyROK.gt.epsi)                                     THEN ! ctr
c #EW           mauxEW        =  mphy2D(il )
c #EW       if (mauxEW(13:13).eq.'r'.or.mauxEW(13:13).eq.'A')     then ! ctr
c #EW           mauxEW(13:13) =  'A'
c #EW       else                                                       ! ctr
c #EW           mauxEW(13:13) =  's'
c #EW       end if                                                     ! ctr
c #EW           mphy2D(il )   =  mauxEW
c #EW      END IF                                                      ! ctr
 
               piacr =     4.1d20 * cnor * W2xyz6(il,kl)
     .          *qi2D(il,kl)/exp(6.8d0*log(W2xyz7(il,kl)))
!                          4.1e20 = a pi**2 rhow/mi Gamma[6+b] / 24
!                          where    a=842., rhow=1000, mi=4.19e-13
!                                                      and b = 0.8
!               Lin et al, 1983, JAM,p1071: mi:Ice Crystal Mass
               qiacr =     piacr * xt    * CbyROK
               qiacr = min(qiacr,qr2D(il,kl))
                qr2D(il,kl) =   qr2D(il,kl) -          qiacr
              tair2D(il,kl) = tair2D(il,kl) + r_LcCp  *qiacr
 
! #qg        if(qr2D(il,kl) .gt. 1.e-4 )                          then ! ctr
! #qg           qg2D(il,kl) =   qg2D(il,kl) +          qiacr
!             CAUTION : Graupels Formation is not taken into account   !
!                       This could be a reasonable assumption for Antarctica
 
! #qg        else                                                      ! ctr
                qs2D(il,kl) =   qs2D(il,kl) +          qiacr
! #qg        end if                                                    ! ctr
 
!  Full Debug
!  ~~~~~~~~~~
c #WQ         write(6,*) 'Qiacr',qiacr,itexpe,il,kl
c #WH         if (il.eq.ilmm) wiacr(kl) = qiacr
 
          END IF
          END IF
          END IF
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Lin et al.(1983): Accretion of Clou'
c #wH         debugH(36:70)   = 'd Ice by Rain                      '
c #wH         proc_1          = 'Qraci g/kg'
c #wH         procv1          =  qraci
c #wH         proc_2          = 'qiacr g/kg'
c #wH         procv2          =  qiacr
c #wH         proc_3          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(11,kl)   =  qiacr
 
        END DO
        END DO
 
 
!  Accretion of Rain           by Snow Flakes
!  Accretion of Snow Flakes    by Rain
!  Reference: Lin et al.      1983, JCAM      22, p.1071 (27)
!             Lin et al.      1983, JCAM      22, p.1071 (28)
!             Emde and Kahlig 1989, Ann.Geoph. 7, p. 408 (~21)
!  -----------------------------------------------------------
 
        DO kl=mzhyd,klev
        DO il=1,klon
 
            psacr(il,kl)=   0.d0
            qsacr       =   0.d0
c #wH       qraci       =   0.0
c #wH       qracsS      =   0.0
c #wH       qsacrR      =   0.0
 
          IF                        (W2xyz3(il,kl).gt.eps9)         THEN
 
            sign_R      =  sign(unun,W2xyz3(il,kl)  - eps9) ! W2xyz3: Qr
            RbyS_r      =   max(zero,sign_R)
!           RbyS_r      =   1.0 if   W2xyz3(il,kl)  > eps9
!                       =   0.0 otherwise
 
          IF                        (W2xyz4(il,kl).gt.eps9)         THEN
 
            sign_S      =  sign(unun,W2xyz4(il,kl)  - eps9) ! W2xyz4: Qs
            RbyS_s      =   max(zero,sign_S)
!           RbyS_s      =   1.0 if   W2xyz4(il,kl)  > eps9
!                       =   0.0 otherwise
 
            RbySOK      =  RbyS_r *  RbyS_s
 
c #EW      IF(CbyROK.gt.epsi)                                       THEN
c #EW       mauxEW        =  mphy2D(il )
c #EW       mauxEW(14:14) = 'A'
c #EW       mphy2D(il )   =  mauxEW
c #EW      END IF
 
!  Accretion of Rain by Snow --> Snow           | W2xyz7 : lambda_r
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~           | W2xyz8 : lambda_s
            flS=(5.0d0/(W2xyz8(il,kl)*W2xyz8(il,kl)*W2xyz7(il,kl))
     .          +2.0d0/(W2xyz8(il,kl)*W2xyz7(il,kl)*W2xyz7(il,kl))
     .          +0.5d0/(W2xyz7(il,kl)*W2xyz7(il,kl)*W2xyz7(il,kl)))
     .  /(W2xyz8(il,kl)*W2xyz8(il,kl)*W2xyz8(il,kl)*W2xyz8(il,kl))
 
c #cn       cnos        = min(2.e8
c #cn.                       ,cnos2*exp(-.12 *min(0.,W2xyz1(il,kl))))
            pracs       = 986.96d-3*(cnor*cnos/rolv2D(il,kl))
     .                             * abs(vr(il,kl)-vs(il,kl))*flS
!                         986.96: pi**2 * rhos
!                        (snow    density assumed equal to  100 kg/m3)
            qracs       =     pracs * xt * RbySOK
            qracs       = min(qracs,qr2D(il,kl))
 
c #WQ       write(6,*) 'Qracs',qracs,itexpe,il,kl
c #WH       if (il.eq.ilmm) wracs(kl) = qracs
 
!  Accretion of Snow by Rain --> Rain
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            sign_R      =  sign(unun,W2xyz3(il,kl)  - 1.e-4)
            SbyR_r      =   max(zero,sign_R)
!           SbyR_r      =   1.0 if   W2xyz3(il,kl)  > 1.e-4
!                       =   0.0 otherwise
 
            sign_S      =  sign(unun,W2xyz4(il,kl)  - 1.e-4)
            SbyR_s      =   max(zero,sign_S)
!           SbyR_s      =   1.0 if   W2xyz4(il,kl)  > 1.e-4
!                       =   0.0 otherwise
 
            SbyROK      =   max(SbyR_r,SbyR_s)
 
           IF (SbyROK.gt.epsi)                                    THEN
            flR=(5.0d0/(W2xyz7(il,kl)*W2xyz7(il,kl)*W2xyz8(il,kl))
     .          +2.0d0/(W2xyz7(il,kl)*W2xyz8(il,kl)*W2xyz8(il,kl))
     .          +0.5d0/(W2xyz8(il,kl)*W2xyz8(il,kl)*W2xyz8(il,kl)))
     .  /(W2xyz7(il,kl)*W2xyz7(il,kl)*W2xyz7(il,kl)*W2xyz7(il,kl))
 
            psacr(il,kl)= 9869.6d-3*(cnor*cnos/rolv2D(il,kl))
     .                             * abs(vr(il,kl)-vs(il,kl))*flR
!                         9869.6: pi**2 * rhow
!                        (water   density assumed equal to 1000 kg/m3)
            qsacr       =     psacr(il,kl) * xt * RbySOK  * SbyROK
            qsacr       = min(qsacr,qs2D(il,kl))
 
c #WQ       write(6,*) 'Qsacr',qsacr,itexpe,il,kl
c #WH       if (il.eq.ilmm) wsacr(kl) = qsacr
           ELSE
            psacr(il,kl) =  0.d0
            qsacr        =  0.d0
           END IF
 
            sign_T      = -sign(unun,tair2D(il,kl)  - TfSnow)
            CbyR_T      =   max(zero,sign_T)
!           CbyR_T      =   1.0 if   tair2D(il,kl)  < TfSnow
!                       =   0.0 otherwise
 
            qracsS      =                     qracs *      CbyR_T
            qsacrR      =                     qsacr *(1.d0-CbyR_T)
 
               qr2D(il,kl)  =   qr2D(il,kl) - qracsS
! #qg       if (W2xyz3(il,kl).lt.1.e-4 .and. W2xyz4(il,kl).lt.1.e-4) then
!              CAUTION  : Graupel Formation is not taken into Account
               qs2D(il,kl)  =   qs2D(il,kl) + qracsS
! #qg       else
! #qg          qs2D(il,kl)  =   qs2D(il,kl) - qracsS
! #qg          qg2D(il,kl)  =   qg2D(il,kl) + qsacrS + qracsS
! #qg       end if
             tair2D(il,kl)  = tair2D(il,kl) + qracsS * r_LcCp
 
               qr2D(il,kl)  =   qr2D(il,kl) + qsacrR
               qs2D(il,kl)  =   qs2D(il,kl) - qsacrR
             tair2D(il,kl)  = tair2D(il,kl) - qsacrR * r_LcCp
 
          END IF
          END IF
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Lin et al.(1983): Accretion of Snow'
c #wH         debugH(36:70)   = '(Rain) by Rain(Snow)               '
c #wH         proc_1          = 'Qracs g/kg'
c #wH         procv1          =  qracsS
c #wH         proc_2          = 'Qsacr g/kg'
c #wH         procv2          =  qsacrR
c #wH         proc_3          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(12,kl)   =  qracsS - qsacrR
 
        END DO
        END DO
 
 
!  Accretion of Snow           by Graupels
!  Reference: Lin et al.      1983, JCAM      22, p.1071 (29)
!  ----------------------------------------------------------
 
! #qg   DO kl=mzhyd,klev
! #qg   DO il=1,klon
 
! #qg     IF                        (W2xyz0(il,kl).gt.eps9)       THEN
 
! #qg       sign_G      =  sign(unun,W2xyz0(il,kl)  - eps9)
! #qg       SbyG_g      =   max(zero,sign_G)
!           SbyG_g      =   1.0 if   W2xyz0(il,kl)  > eps9
!                       =   0.0 otherwise
 
! #qg     IF                        (W2xyz4(il,kl).gt.eps9)       THEN
 
! #qg       sign_S      =  sign(unun,W2xyz4(il,kl)  - eps9)
! #qg       SbyG_s      =   max(zero,sign_S)
!           SbyG_s      =   1.0 if   W2xyz4(il,kl)  > eps9
!                       =   0.0 otherwise
 
! #qg       SbyGOK      =  SbyG_g *  SbyG_s
 
! #qg       efc   =  exp(0.090*W2xyz1(il,kl))
!           efc   : Collection Efficiency
!                   Lin et al. 1983 JCAM 22 p.1072 (30)
 
! #qg       flg=exp(-6.0d0*log(W2xyz8(il,kl))
! #qg.         *(5.0/W2xyz9(il,kl)
! #qg.          +2.0*W2xyz8(il,kl)/(W2xyz9(il,kl)*W2xyz9(il,kl))
! #qg.          +0.5*W2xyz8(il,kl)* W2xyz8(il,kl)
! #qg.               /exp(3.0d0*log(W2xyz9(il,kl))))
c #cn       cnos       = min(2.e8
c #cn.                      ,cnos2*exp(-.12*min(0.,W2xyz1(il,kl))))
! #qg       pgacs      = 986.96d-3*(cnog*cnos/rolv2D(il,kl))
! #qg.                            * abs(vg(il,kl)-vs(il,kl))*flg*efc
!                        986.96: pi**2 * rhog
!                       (graupel densitity assumed equal to snow density)
! #qg       qgacs      =     pgacs * xt       * SbyGOK
! #qg       qgacs      = min(qgacs,qs2D(il,kl))
! #qg       qg2D(il,kl)  = qg2D(il,kl) +        qgacs
! #qg       qs2D(il,kl)  = qs2D(il,kl) -        qgacs
 
! #qg     END IF
! #qg     END IF
 
! #qg   END DO
! #qg   END DO
 
 
!  Accretion of Rain           by Graupels (Dry Growth Mode)
!  Reference: Lin et al.      1983, JCAM      22, p.1075 (42)
!  ----------------------------------------------------------
 
! #qg   DO kl=mzhyd,klev
! #qg   DO il=1,klon
 
! #qg     IF                        (W2xyz0(il,kl).gt.eps9)       THEN
 
! #qg       sign_G      =  sign(unun,W2xyz0(il,kl)  - eps9)
! #qg       RbyG_g      =   max(zero,sign_G)
!           RbyG_g      =   1.0 if   W2xyz0(il,kl)  > eps9
!                       =   0.0 otherwise
 
! #qg     IF                        (W2xyz3(il,kl).gt.eps9)       THEN
 
! #qg       sign_R      =  sign(unun,W2xyz3(il,kl)  - eps9)
! #qg       RbyG_r      =   max(zero,sign_R)
!           RbyG_r      =   1.0 if   W2xyz3(il,kl)  > eps9
!                       =   0.0 otherwise
 
! #qg     IF                        (tair2D(il,kl).lt.TfSnow)     THEN
 
! #qg       sign_T      = -sign(unun,tair2D(il,kl)  - TfSnow)
! #qg       Fact_G      =   max(zero,sign_T)
!           Fact_G      =   1.0 if   tair2D(il,kl)  < TfSnow
!                       =   0.0 otherwise
 
! #qg       RbyGOK      =  RbyG_g *  RbyG_s         * Fact_G
 
! #qg       flg=exp(-6.0d0*log(W2xyz8(il,kl))
! #qg.         *(5.0/W2xyz9(il,kl)
! #qg.          +2.0*W2xyz8(il,kl)/(W2xyz9(il,kl)*W2xyz9(il,kl))
! #qg.          +0.5*W2xyz8(il,kl)* W2xyz8(il,kl)
! #qg.               /exp(3.0d0*log(W2xyz9(il,kl))))
c #cn       cnos         = min(2.e8
c #cn.                        ,cnos2*exp(-.12*min(0.,W2xyz1(il,kl))))
! #qg       pgacr        = 986.96d-3*(cnog*cnos/rolv2D(il,kl))
! #qg.                              * abs(vg(i,kl) - vr(il,kl))*flg
! #qg       qgacr        =     pgacr * xt  *        RbyGOK
! #qg       qgacr        = min(qgacr,qr2D(il,kl))
! #qg         qg2D(il,kl)  =   qg2D(il,kl) +        qgacr
! #qg         qr2D(il,kl)  =   qr2D(il,kl) -        qgacr
! #qg       tair2D(il,kl)  = tair2D(il,kl) + r_LcCp*qgacr
 
! #qg     END IF
! #qg     END IF
! #qg     END IF
 
! #qg   END DO
! #qg   END DO
 
 
!  Graupels Wet Growth Mode
!  Reference: Lin et al.      1983, JCAM      22, p.1075 (43)
!  ----------------------------------------------------------
 
! #qg   ! TO BE ADDED !
 
 
!  Microphysical Processes affecting     Precipitating Cloud Particles
!  ===================================================================
 
 
!  Rain Drops Evaporation                                    ============
!  Reference: Lin et al.      1983, JCAM      22, p.1077 (52)
!  ----------------------------------------------------------
 
        DO kl=mzhyd,klev
        DO il=1,klon
 
c #wH       qrevp       =   0.0
 
          IF                        (W2xyz3(il,kl).gt.eps9)       THEN
!                                    W2xyz3 : old Rain    Concentration
 
            sign_R      =  sign(unun,W2xyz3(il,kl)  - eps9)
            Evap_r      =   max(zero,sign_R)
!           Evap_r      =   1.0 if   W2xyz3(il,kl)  > eps9
!                       =   0.0 otherwise
 
            EvapOK      =  Evap_r
 
c #EW      IF(EvapOK.gt.epsi)                                     THEN ! ctr
c #EW       mauxEW        =  mphy2D(il )
c #EW       mauxEW(15:15) = 'v'
c #EW       mphy2D(il )   =  mauxEW
c #EW      END IF                                                      ! ctr
 
            sr          =  qv2D(il,kl)/(rhcrHY*qvsw2D(il,kl))
!           sr          :  grid scale saturation humidity
 
          IF                        (sr           .lt.unun)       THEN
            sign_Q      = -sign(unun,sr             - unun)
            Evap_q      =   max(zero,sign_Q)
!           Evap_q      =   1.0 if   sr             < unun
!                       =   0.0 otherwise
 
            EvapOK      =  EvapOK                   * Evap_q
 
            almr  = 0.78d0  /(W2xyz7(il,kl) *W2xyz7(il,kl))
     .            + 3940.d0 *           sqrt(W2xyz6(il,kl))
!                   3940.: 0.31 Sc**(1/3) *(a/nu)**(1/2) * Gamma[(b+5)/2]
!                   where       Sc=0.8(Schm.) nu=1.5e-5
!                                            (Air Kinematic Viscosity)
     .                      /exp(2.9d0  *log(W2xyz7(il,kl)))
            ab    = 5.423d11/(tair2D(il,kl) *tair2D(il,kl))
!                   5.423e11
!                 = [Lv=2500000J/kg] * Lv / [kT=0.025W/m/K] / [Rv=461.J/kg/K]
!                                            kT:  Air Thermal Conductivity
     .            + 1.d0       /(1.875d-2 *rolv2D(il,kl) *qvsw2D(il,kl))
 
 
            prevp = 2*pi*(1.d0-sr)*cnor*almr/ab
            qrevp =     prevp * xt
            qrevp = min(qrevp,  qr2D(il,kl))
 
            qrevp = min(qrevp,rhcrHY*qvsw2D(il,kl)-qv2D(il,kl))
!           supersaturation is not allowed to occur
 
            qrevp = max(qrevp,zero)       *           EvapOK
!           condensation    is not allowed to occur
 
              qr2D(il,kl) =   qr2D(il,kl) -           qrevp
             dqw2D(il,kl) =  dqw2D(il,kl) -           qrevp
              qv2D(il,kl) =   qv2D(il,kl) +           qrevp
            tair2D(il,kl) = tair2D(il,kl) - r_LvCp   *qrevp
 
!  Full Debug
!  ~~~~~~~~~~
c #WQ       write(6,*) 'Qrevp',qrevp,itexpe,il,kl
c #WH       if (il.eq.ilmm) wrevp(kl) = qrevp
 
          END IF
          END IF
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Lin et al.(1983): Rain Drops Evapor'
c #wH         debugH(36:70)   = 'ation                              '
c #wH         proc_1          = 'Qrevp g/kg'
c #wH         procv1          =  qrevp
c #wH         proc_2          = 'R.Hum  [%]'
c #wH         procv2          =  sr*0.1
c #wH         proc_3          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(13,kl)   = -qrevp
 
        END DO
        END DO
 
 
!  (Deposition on) Snow Flakes (Sublimation)
!   Reference: Lin et al.      1983, JCAM      22, p.1072 (31)
!   ----------------------------------------------------------
 
c #BS   DO il=1,klon
c #BS     hlat2D(il,1) = 0.d0
c #BS   END DO
 
        DO kl=mzhyd,klev
        DO il=1,klon
 
c #wH       qssub       =   0.0
 
          IF                        (W2xyz4(il,kl).gt.eps9)       THEN
!                                    W2xyz4 : old Snow F. Concentration
 
            sign_S      =  sign(unun,W2xyz4(il,kl)  - eps9)
            Evap_s      =   max(zero,sign_S)
!           Evap_s      =   1.0 if   W2xyz4(il,kl)  > eps9
!                       =   0.0 otherwise
 
c #EW      IF(Evap_s.gt.epsi)                                     THEN ! ctr
c #EW       mauxEW        =  mphy2D(il )
c #EW       mauxEW(16:16) = 'V'
c #EW       mphy2D(il )   =  mauxEW
c #EW      END IF                                                      ! ctr
 
           si    =             qv2D(il,kl)/W2xyz5(il,kl)
 
           alms  = 0.78d0  /(W2xyz8(il,kl)*W2xyz8(il,kl))
     .           +  238.d0 *          sqrt(W2xyz6(il,kl))
!                   238.: 0.31 Sc**(1/3) *(c/nu)**(1/2) * Gamma[(d+5)/2]
!                   where      Sc=0.8(Schm.) nu=1.5e-5
!                                           (Air Kinematic Viscosity)
     .                    /exp(2.625d0*log(W2xyz8(il,kl)))
           ab    = 6.959d11/(tair2D(il,kl)*tair2D(il,kl))
!                  6.959e11
!                = [Ls=2833600J/kg]*Ls /[kT=0.025W/m/K] /[Rv=461.J/kg/K]
!                                        kT: Air Thermal   Conductivity
     .           + 1.d0       /(1.875d-2*rolv2D(il,kl)*W2xyz5(il,kl))
 
c #cn       cnos  = min(2.e8
c #cn.                 ,cnos2*exp(-.12*min(0.,W2xyz1(il,kl))))
            pssub = 2*pi*(1.d0-si)*cnos*alms/(1.d3*rolv2D(il,kl)*ab)
            qssub = pssub * xt
 
            dqamx = W2xyz5(il,kl) - qv2D(il,kl)
 
            sign_S      =  sign(unun,si             - unun)
            Depo_s      =   max(zero,sign_S)
!           Depo_s      =   1.0 if   si             > unun
!                       =   0.0 otherwise
 
            qssub       =   max(qssub             ,dqamx)*       Depo_s
     .              +   min(min(qssub,qs2D(il,kl)),dqamx)*(1.0d0-Depo_s)
!           qssub < 0       ... Deposition
!                 > 0       ... Sublimation
 
            qssub       =       qssub                    *       Evap_s
 
              qs2D(il,kl) =   qs2D(il,kl)-          qssub
             dqi2D(il,kl) =  dqi2D(il,kl)-          qssub
              qv2D(il,kl) =   qv2D(il,kl)+          qssub
            tair2D(il,kl) = tair2D(il,kl)-r_LsCp   *qssub
c #BS       hlat2D(il ,1) = hlat2D(il ,1)+          qssub *rolv2D(il,kl)
c #BS.                    *(gpmi2D(il,kl)-gpmi2D(il,kl+1))*grvinv
!           hlat2D(il ,1) : Vertical Integrated Blowing Snow Sublimation
!                          [m w.e.]
 
!  Full Debug
!  ~~~~~~~~~~
c #WQ       write(6,*) 'Qssub',qssub,itexpe,il,kl
c #WH       if (il.eq.ilmm) wssub(kl) =-qssub
 
          END IF
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Lin et al.(1983): (Deposition on) S'
c #wH         debugH(36:70)   = 'now Particles (Sublimation)        '
c #wH         proc_1          = 'Qssub g/kg'
c #wH         procv1          =  qssub
c #wH         proc_2          = '          '
c #wH         procv2          =  0.
c #wH         proc_3          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(14,kl)   = -qssub
 
        END DO
        END DO
 
 
!  Graupels Sublimation
!  Reference: Lin et al.      1983, JCAM      22, p.1076 (46)
!  ----------------------------------------------------------
 
! #qg   ! TO BE ADDED !
 
 
!  Snow Flakes Melting        PSMLT
!  Reference: Lin et al.      1983, JCAM      22, p.1072 (32)
!  ----------------------------------------------------------
 
        DO kl=mzhyd,klev
        DO il=1,klon
 
c #wH       qsmlt       =   0.0
 
          IF                        (W2xyz4(il,kl).gt.eps9)       THEN
!                                    W2xyz4 : old Snow Fl.Concentration
 
            sign_S      =  sign(unun,W2xyz4(il,kl)  - eps9)
            SnoM_s      =   max(zero,sign_S)
!           SnoM_s      =   1.0 if   W2xyz4(il,kl)  > eps9
!                       =   0.0 otherwise
 
          IF                        (W2xyz1(il,kl).gt.0.e0)       THEN
!                                    W2xyz1 : old Celsius Temperature
 
            sign_T      =  sign(unun,W2xyz1(il,kl)  - 0.e0)
            SnoM_T      =   max(zero,sign_T)
!           SnoM_T      =   1.0 if   W2xyz1(il,kl)  > 0.e0
!                       =   0.0 otherwise
 
            SnoMOK      =  SnoM_s                   * SnoM_T
 
c #EW      IF(SnoMOK.gt.epsi)                                     THEN ! ctr
c #EW       mauxEW        =  mphy2D(il )
c #EW       mauxEW(17:17) = 'r'
c #EW       mphy2D(il )   =  mauxEW
c #EW      END IF                                                      ! ctr
 
            alms  =    0.78    /  (W2xyz8(il,kl) *W2xyz8(il,kl))
     .            +  238.      *             sqrt(W2xyz6(il,kl))
     .                         / exp(2.625d0 *log(W2xyz8(il,kl)))
 
c #cn       cnos  = min(2.e8
c #cn.                 ,cnos2*exp(-.12*min(0.,W2xyz1(il,kl))))
            xCoef = 1.904d-8 *cnos *alms *r_LcCp /rolv2D(il,kl)
!                   1.904e-8: 2 pi / Lc /[1.e3=rho Factor]
 
            ACoef = 0.025d00 *xCoef
     .            +(psacw(il,kl) + psacr(il,kl)) *r_LcCp /78.8d0
!                   78.8    : Lc /[Cpw=4.187e3 J/kg/K]
 
            Bcoef = 62.34d+3 *rolv2D(il,kl) *(qv2D(il,kl)-W2xyz5(il,kl))
!                   62.34   : Ls*[psiv=2.200e-5 m2/s]
!                   46.88   : Lv*[psiv=1.875e-5 m2/s]
     .                       *xCoef
            Bcoef = min(-eps9,Bcoef)
 
            Tc    =    (tair2D(il,kl)-TfSnow-ACoef/Bcoef)*exp(-ACoef*xt)
            qsmlt =    (tair2D(il,kl)-TfSnow-Tc         )/     r_LcCp
            qsmlt = max( qsmlt,0.         )              *SnoMOK
            qsmlt = min( qsmlt,qs2D(il,kl))
 
cXF
 
            alms  = 0.78d0  /(W2xyz8(il,kl)*W2xyz8(il,kl))
     .               +  238.d0 *       sqrt(W2xyz6(il,kl))
     .                     /exp(2.625d0*log(W2xyz8(il,kl)))
            akps  = 0.025d0  *W2xyz1(il,kl)
     .            + 46.875d3 *rolv2D(il,kl) *(qv2D(il,kl)-W2xyz5(il,kl))
C +...              46.875  : Lv*[psiv=1.875e-5m2/s]
 
            psmlt = 1.904d-8*cnos*akps*alms/rolv2D(il,kl)
C +...              1.904e-8: 2 pi / Lc /[1.e3=rho Factor]
     .            -(psacw(il,kl) + psacr(il,kl)) *W2xyz1(il,kl) /78.8d0
C +...                               Lc /[Cpw=4.187e3 J/kg/K]  = 78.8
            qsmlt =     psmlt * xt        *          SnoMOK
            qsmlt = max(qsmlt,zero)
            qsmlt = min(qsmlt,qs2D(il,kl))
c this options increases the conversion of Snowfall to rainfall
 
 
              qs2D(il,kl) =   qs2D(il,kl) -          qsmlt
              qr2D(il,kl) =   qr2D(il,kl) +          qsmlt
            tair2D(il,kl) = tair2D(il,kl) - r_LcCp  *qsmlt
 
!  Full Debug
!  ~~~~~~~~~~
c #WQ       write(6,*) 'Qsmlt',qsmlt,itexpe,il,kl
c #WH       if (il.eq.ilmm) wsmlt(kl) = qsmlt
 
          END IF
          END IF
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Lin et al.(1983): Snow Particles Me'
c #wH         debugH(36:70)   = 'lting                              '
c #wH         proc_1          = 'Qsmlt g/kg'
c #wH         procv1          =  qsmlt
c #wH         proc_2          = '          '
c #wH         procv2          =  0.
c #wH         proc_3          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(15,kl)   = -qsmlt
 
        END DO
        END DO
 
 
!  Graupels    Melting
!  Reference: Lin et al.      1983, JCAM      22, p.1076 (47)
!  ----------------------------------------------------------
 
! #qg   ! TO BE ADDED !
 
 
!  Rain Freezing
!  Reference: Lin et al.      1983, JCAM      22, p.1075 (45)
!  ----------------------------------------------------------
 
!  **CAUTION**: Graupel Formation TO BE ADDED !
 
        DO kl=mzhyd,klev
        DO il=1,klon
 
c #wH       qsfr        =   0.0
 
          IF                        (W2xyz3(il,kl).gt.eps9)       THEN
!                                    W2xyz3 : old Rain    Concentration
 
            sign_R      =  sign(unun,W2xyz3(il,kl)  - eps9)
            Freezr      =   max(zero,sign_R)
!           Freezr      =   1.0 if   W2xyz3(il,kl)  > eps9
!                       =   0.0 otherwise
 
          IF                        (W2xyz1(il,kl).lt.0.e0)       THEN
!                                    W2xyz1 : old Celsius Temperature
 
            sign_T      = -sign(unun,W2xyz1(il,kl)  - 0.e0)
            FreezT      =   max(zero,sign_T)
!           FreezT      =   1.0 if   W2xyz1(il,kl)  < 0.e0
!                       =   0.0 otherwise
 
            FrerOK      =  Freezr                   * FreezT
 
c #EW      IF(FrerOK.gt.epsi)                                     THEN ! ctr
c #EW       mauxEW        =  mphy2D(il )
c #EW       mauxEW(19:19) = 's'
c #EW       mphy2D(il )   =  mauxEW
c #EW      END IF                                                      ! ctr
 
            psfr = 1.974d4 *cnor
     .       /(rolv2D(il,kl)*exp(7.d0 *log(W2xyz7(il,kl))))
     .                     *(exp(-0.66d0  *W2xyz1(il,kl))-1.d0)
            qsfr =     psfr * xt          *        FrerOK
            qsfr = min(qsfr,qr2D(il,kl))
 
              qr2D(il,kl) =   qr2D(il,kl) -          qsfr
              qs2D(il,kl) =   qs2D(il,kl) +          qsfr
!             CAUTION : graupel production is included into snow production
!                       proposed modification in line below.
! #qg         qg2D(il,kl) =   qg2D(il,kl) +          qsfr
            tair2D(il,kl) = tair2D(il,kl) + r_LcCp  *qsfr
 
!  Full Debug
!  ~~~~~~~~~~
c #WQ       write(6,*) 'Qsfre',qsfr,itexpe,il,kl
c #WH       if (il.eq.ilmm) wsfre(kl) = qsfr
 
          END IF
          END IF
 
!  Debug
!  ~~~~~
c #wH         debugH( 1:35)   = 'Lin et al.(1983): Rain Freezing    '
c #wH         debugH(36:70)   = '                                   '
c #wH         proc_1          = 'Qsfr g/kg'
c #wH         procv1          =  qsfr
c #wH         proc_2          = '          '
c #wH         procv2          =  0.
c #wH         proc_3          = '          '
c #wH         procv3          =  0.
c #wH         proc_4          = '          '
c #wH         procv4          =  0.
c #wh         include 'MAR_HY.Debug'
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )
c #wH.        debugV(16,kl)   =  qsfr
 
        END DO
        END DO
 
!  Debug (Summary)
!  ~~~~~~~~~~~~~~~
c #wH   DO kl=mzhyd,klev
c #wH   DO il=1,klon
 
c #wH     IF(i_fvv(il).EQ.i0fvv.AND.j_fvv(il).EQ.j0fvv     )  THEN
c #kl.             kl .EQ.k0fvv                            )  THEN
c #wH          IF (kl .EQ.mzhyd)                              THEN
c #wH          write(6,6022)
 6022          format(/,'HYDmic STATISTICS'
     .                /,'=================')
c #wH          write(6,6026)
 6026          format(  '    T_Air Qv   Qw g/kg  Qi g/kg  CLOUDS % '
     .                 ,              ' Qs g/kg  Qr g/kg'
     .                 ,' Qi+ E.K.'
     .                 ,' Qi+ Mey.'
     .                 ,' Qi- Sub.'
     .                 ,' Qi- Mlt.'
     .                 ,' Qw+ Cds.'
     .                 ,' Qraut r+'
     .                 ,' Qsaut s+'
     .                 ,' Qracw r+')
c #wH          END IF
c #wH          write(6,6023)   kl
c #wH.              ,      tair2D(il,kl)-TfSnow
c #wH.              ,1.e3*   qv2D(il,kl)
c #wH.              ,1.e3*   qw2D(il,kl)
c #wH.              ,1.e3*   qi2D(il,kl)
c #wH.              ,1.e2* cfra2D(il,kl)
c #wH.              ,1.e3*   qs2D(il,kl)
c #wH.              ,1.e3*   qr2D(il,kl)
c #wH.             ,(1.e3* debugV(kv,kl),kv=1,08)
 6023          format(i3,f6.1,f5.2,2f9.6,f9.1,2f9.3,8f9.6)
c #wH          IF (kl .EQ.klev )                              THEN
c #wH          write(6,6026)
c #wH          write(6,*)  ' '
c #wH          write(6,6024)
 6024          format(  8x,'Z [km]'
     .                 ,' RH.w.[%]'
     .                 ,' RH.i.[%]'     ,9x
     .                 ,' Vss cm/s'
     .                 ,' Vrr cm/s'
     .                 ,' Qsacw s+'
     .                 ,' Qsaci s+'
     .                 ,' Qiacr r+'
     .                 ,' Qracs ds'
     .                 ,' Qrevp w-'
     .                 ,' Qssub s-'
     .                 ,' Qsmlt s-'
     .                 ,' Qsfr  s+')
c #wH          DO nl=mzhyd,klev
c #wH          write(6,6025)   nl       ,zsigma(   nl)*1.e-3
c #wH.              ,1.e2*   qv2D(il,nl)/qvsw2D(il,nl)
c #wH.              ,1.e2*   qv2D(il,nl)/qvsi2D(il,nl)
c #wH.              ,1.e2*     vs(il,nl)
c #wH.              ,1.e2*     vr(il,nl)
c #wH.             ,(1.e3* debugV(kv,nl),kv=9,16)
 6025          format(i3,f11.3,    2f9.1,9x,  2f9.1,8f9.6)
c #wH          END DO
c #wH          write(6,6024)
c #wH          write(6,*)  ' '
c #wH          END IF
 
c #wH     END IF
 
c #wH   END DO
c #wH   END DO
 
 
!  Vertical Integrated Energy and Water Content
!  ============================================
 
c #EW   DO       il=1,klon
c #EW     enr11D(il) = 0.0d00
c #EW     wat11D(il) = 0.0d00
 
c #EW     DO     kl=1,klev
c #EW     enr11D(il) = enr11D(il )
c #EW.               +(tair2D(il,kl)
c #EW.                 -(qw2D(il,kl)+qr2D(il,kl)) *r_LvCp
c #EW.                 -(qi2D(il,kl)+qs2D(il,kl)) *r_LsCp)*dsigm1(kl)
c #EW     wat11D(il) = wat11D(il )
c #EW.               +  (qv2D(il,kl)
c #EW.               +   qw2D(il,kl)+qr2D(il,kl)
c #EW.               +   qi2D(il,kl)+qs2D(il,kl)         )*dsigm1(kl)
c #EW     END DO
 
c #ew     enr11D(il) = enr11D(il ) * pst2Dn(il) * grvinv
c #EW     wat11D(il) = wat11D(il ) * pst2Dn(il) * grvinv
C +...    wat11D [m]   contains implicit factor 1.d3 [kPa-->Pa] /ro_Wat
 
c #EW   END DO
 
 
!  Precipitation
!  =============
 
!  Hydrometeors Fall Velocity
!  --------------------------
 
!  Pristine Ice Crystals Diameter and Fall Velocity
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        DO kl=mzhyd,klev
        DO il=1,klon
 
          IF                          (qi2D(il,kl).gt.eps9)       THEN
 
            sign_Q      =  sign(unun,  qi2D(il,kl)  - eps9)
            Sedi_c      =   max(zero,sign_Q)
!           Sedi_c      =   1.0 if     qi2D(il,kl)  > eps9
!                       =   0.0 otherwise
 
          IF                        (ccni2D(il,kl).gt.1.e0)       THEN
 
            signCC      =  sign(unun,ccni2D(il,kl)  - 1.e0)
            Sedicc      =   max(zero,signCC)
!           Sedicc      =   1.0 if   ccni2D(il,kl)  > 1.e0
!                       =   0.0 otherwise
 
            SediOK      =  Sedi_c  * Sedicc
 
            qid   = 0.16d0 *exp(third *log(thous*rolv2D(il,kl)
     .                *max(eps9,qi2D(il,kl))/max(ccni2D(il,kl),unun)))
!           qid   : Pristine Ice Crystals Diameter,
!                   Levkov et al. 1992, Contr. Atm. Phys. 65, (5) p.37
!                   where 6/(pi*ro_I)**1/3 ~ 0.16
 
            vi (il,kl)  =  SediOK * 7.d2*qid
     .         *exp(0.35d0*log(rolv2D(il,klev)  / rolv2D(il,kl)))
!           vi    : Terminal Fall Velocity for Pristine Ice Crystals
!                   Levkov et al. 1992, Contr. Atm. Phys. 65, (4) p.37
          ELSE
            vi (il,kl)  =  0.0d00
 
          END IF
          END IF
 
        END DO
        END DO
 
!  Set Up of the Numerical Scheme
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #VW     vwmx = 0.d0
          vrmx = 0.d0
          vsmx = 0.d0
          vimx = 0.d0
        DO il=1,klon
c #EW     watf1D(il) = 0.d0
!         watf1D     : Water Flux (Atmosphere --> Surface)
 
        END DO
 
!  Snow and Rain Fall Velocity (Correction)
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        DO kl=mzhyd,klev
        DO il=    1,klon
          vi(il,kl) = vi(il,kl) *qi2D(il,kl)/max(qi2D(il,kl),eps9)
          vs(il,kl) = vs(il,kl) *qs2D(il,kl)/max(qs2D(il,kl),eps9)
c #VW     vw(il,kl) = vw(il,kl) *qw2D(il,kl)/max(qw2D(il,kl),eps9)
          vr(il,kl) = vr(il,kl) *qr2D(il,kl)/max(qr2D(il,kl),eps9)
 
          vimx = max(vi(il,kl),vimx)
          vsmx = max(vs(il,kl),vsmx)
c #VW     vwmx = max(vw(il,kl),vwmx)
          vrmx = max(vr(il,kl),vrmx)
 
c #WH     IF (vsmx.gt.vmmx)                                       THEN ! ctr
c #WH       vmmx  = vsmx
c #WH       ilmmi = il
c #WH     END IF                                                       ! ctr
c #WH     IF (vrmx.gt.vmmi)                                       THEN ! ctr
c #WH       vmmi  = vrmx
c #WH       ilmmi = il
c #WH     END IF                                                       ! ctr
 
        END DO
        END DO
 
            dzmn  =          10000.
        DO  il=1,klon
            dzmn  =min(dzmn,(gplv2D(il,mz1)-gplv2D(il,mz))*grvinv)
        END DO
 
 
!  Rain Drops  Precipitation (Implicit Scheme)
!  -------------------------------------------
 
        DO il=     1,klon
          W2xyz8(il,mzhyd-1) = 0.
        ENDDO
 
!  Precipitation Mass & Flux
!  ~~~~~~~~~~~~~~~~~~~~~~~~~
        DO kl= mzhyd,klev
        DO il=     1,klon
          W2xyz1(il,kl) = pst2Dn(il)    *dsigm1(kl)   *grvinv ! Air  Mass
          W2xyz6(il,kl) = xt* vr(il,kl) *rolv2D(il,kl)        ! Flux Fact.
        ENDDO
 
        DO il=     1,klon
          W2xyz5(il,kl) =   qr2D(il,kl) *W2xyz1(il,kl)        ! Rain Mass
     .                  +    0.5        *W2xyz8(il,kl-1)      ! From abov.
          W2xyz7(il,kl) =                                     ! Var. Fact.
     .             min(2.,W2xyz6(il,kl) /W2xyz1(il,kl))       ! Flux Limi.
        ENDDO
 
        DO il=     1,klon
          W2xyz8(il,kl) = W2xyz5(il,kl) *W2xyz7(il,kl)        ! Mass Loss
     .                              /(1.+W2xyz7(il,kl)*0.5)   !
        ENDDO
 
        DO il=     1,klon
          W2xyz5(il,kl) = W2xyz5(il,kl) -W2xyz8(il,kl)
     .                  +    0.5        *W2xyz8(il,kl-1)      ! From abov.
 
!  Cooling from above precipitating flux
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          tair2D(il,kl) =
     .   (tair2D(il,kl)  *W2xyz1(il,kl)
     .   +tair2D(il,kl-1)*W2xyz8(il,kl-1))
     .  /(                W2xyz1(il,kl)
     .   +                W2xyz8(il,kl-1))
        ENDDO
 
        DO il=     1,klon
            qr2D(il,kl) = W2xyz5(il,kl) /W2xyz1(il,kl)
        ENDDO
        ENDDO
 
!  Precipitation reaching the Surface
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        DO il=     1,klon
          dwat          = W2xyz8(il,klev)
!         dwat contains an implicit factor 1.e3[kPa->Pa]/ro_Wat[kg/m2->m w.e.]
 
          rain2D(il )   = rain2D(il ) + dwat
!         rain2D        : snow precipitation height since start of run     [m]
 
c #EW     watf1D(il )   = watf1D(il ) - dwat
 
          prec2D(il )   = prec2D(il ) + dwat
!         prec2D        : rain precipitation height                        [m]
!                         is reset to zero after included in water reservoir
        END DO
          dwat          = 0.0
 
 
!  Droplets    Precipitation
!  -------------------------
 
c #VW   itmx = int(1.0d0 + xt * vwmx / dzmn)
!       normally,  0.5 is sufficient to take into account truncation effect
c #VW   itmx = max(1,itmx)
c #VW   xtmn = xt  / itmx
 
!  Precipitation reaching the Surface
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #VW   DO  it=    1,itmx
c #VW    DO il=    1,klon
c #VW     dwat        = vw(il,klev) *qw2D(il,klev) *rolv2D(il,klev)*xtmn
!         dwat contains an implicit factor 1.d3[kPa->Pa]/ro_Wat[kg/m2->m w.e.]
 
c #VW     rain2D(il ) = rain2D(il ) + dwat
!         rain2D      : rain precipitation height since start of run       [m]
 
c #Vw     watf1D(il ) = watf1D(il ) - dwat
 
c #VW     prec2D(il ) = prec2D(il ) + dwat
!         prec2D      : rain precipitation height                          [m]
!                       is reset to zero after included in water reservoir
!                       (cfr. routine SRFfrm_XXX)
c #VW    END DO
 
!  Precipitation elsewhere
!  ~~~~~~~~~~~~~~~~~~~~~~~
c #VW    DO kl= klev,mzhyd+1,-1
c #VW    DO il=    1,klon
c #VW     W2xyz1(il,kl) = qw2D(il,kl)  *pst2Dn(il) *dsigm1(kl)
c #VW.  + gravit *xtmn  *(qw2D(il,kl-1)*vw(il,kl-1)*rolv2D(il,kl-1)
c #VW.                   -qw2D(il,kl)  *vw(il,kl)  *rolv2D(il,kl)  )
c #VW    END DO
c #VW    END DO
 
c #VW    DO il=    1,klon
c #VW     W2xyz1(il,mzhyd)= qw2D(il,mzhyd)*pst2Dn(il)  *dsigm1(   mzhyd)
c #VW.  - gravit *xtmn    * qw2D(il,mzhyd)*vw(il,mzhyd)*rolv2D(il,mzhyd)
c #VW    END DO
 
c #VW    DO kl=      mzhyd,klev
c #VW    DO il=    1,klon
c #VW       qw2D(il,kl) = W2xyz1(il,kl) / (pst2Dn(il)  *dsigm1(kl))
c #VW    END DO
c #VW    END DO
 
c #VW   END DO
 
 
!  Snow Flakes Precipitation (Implicit Scheme)
!  -------------------------------------------
 
        DO il=     1,klon
          W2xyz8(il,mzhyd-1) = 0.
        ENDDO
 
!  Precipitation Mass & Flux
!  ~~~~~~~~~~~~~~~~~~~~~~~~~
        DO kl= mzhyd,klev
        DO il=     1,klon
          W2xyz1(il,kl) = pst2Dn(il)    *dsigm1(kl)   *grvinv ! Air  Mass
          W2xyz6(il,kl) = xt* vs(il,kl) *rolv2D(il,kl)        ! Flux Fact.
        ENDDO
 
        DO il=     1,klon
          W2xyz5(il,kl) =   qs2D(il,kl) *W2xyz1(il,kl)        ! Snow Mass
     .                  +    0.5        *W2xyz8(il,kl-1)      ! From abov.
          W2xyz7(il,kl) =                                     ! Var. Fact.
     .             min(2.,W2xyz6(il,kl) /W2xyz1(il,kl))       ! Flux Limi.
        ENDDO
 
        DO il=     1,klon
          W2xyz8(il,kl) = W2xyz5(il,kl) *W2xyz7(il,kl)        ! Mass Loss
     .                              /(1.+W2xyz7(il,kl)*0.5)   !
        ENDDO
 
        DO il=     1,klon
          W2xyz5(il,kl) = W2xyz5(il,kl) -W2xyz8(il,kl)
     .                  +    0.5        *W2xyz8(il,kl-1)      ! From abov.
 
!  Cooling from above precipitating flux
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          tair2D(il,kl) =
     .   (tair2D(il,kl)  *W2xyz1(il,kl)
     .   +tair2D(il,kl-1)*W2xyz8(il,kl-1))
     .  /(                W2xyz1(il,kl)
     .   +                W2xyz8(il,kl-1))
        ENDDO
 
        DO il=     1,klon
            qs2D(il,kl) = W2xyz5(il,kl) /W2xyz1(il,kl)
        ENDDO
        ENDDO
 
!  Precipitation reaching the Surface
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        DO il=     1,klon
          dsno          = W2xyz8(il,klev)
!         dsno contains an implicit factor 1.e3[kPa->Pa]/ro_Wat[kg/m2->m w.e.]
 
          snow2D(il )   = snow2D(il ) + dsno
!         snow2D        : snow precipitation height since start of run     [m]
 
c #EW     watf1D(il )   = watf1D(il ) - dsno
 
          snoh2D(il )   = snoh2D(il ) + dsno
!         snoh2D        : snow precipitation height                        [m]
!                         is reset to zero after included in snow cover
!                        (cfr. routine SRFfrm_sno)
        END DO
          dsno          = 0.
 
 
!  Pristine Ice Crystals Precipitation
!  -----------------------------------
 
        itmx = int(1.0d0 + xt * vimx / dzmn)
!       normally,  0.5 is sufficient to take into account truncation effect
        itmx = max(1,itmx)
        xtmn = xt  / itmx
 
!  Precipitation reaching the Surface
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        DO  it=    1,itmx
         DO il=    1,klon
          dsno        = vi(il,klev) *qi2D(il,klev) *rolv2D(il,klev)*xtmn
!         dsno contains an implicit factor 1.d3[kPa->Pa]/ro_Wat[kg/m2->m w.e.]
 
          crys2D(il) = crys2D(il ) + dsno
          snow2D(il) = snow2D(il ) + dsno
!         snow2D     : snow precipitation height since start of run        [m]
 
c #EW     watf1D(il) = watf1D(il ) - dsno
 
          snoh2D(il) = snoh2D(il ) + dsno
!         snoh2D     : snow precipitation height                           [m]
!                    is reset to zero after included in snow cover
!                    (cfr. routine SRFfrm_sno)
         END DO
 
!  Precipitation elsewhere
!  ~~~~~~~~~~~~~~~~~~~~~~~
         DO kl= klev,mzhyd+1,-1
         DO il=    1,klon
          W2xyz1(il,kl) =   qi2D(il,kl)  *pst2Dn(il) *dsigm1(kl)
     .  + gravit *xtmn    *(qi2D(il,kl-1)*vi(il,kl-1)*rolv2D(il,kl-1)
     .                     -qi2D(il,kl)  *vi(il,kl)  *rolv2D(il,kl)  )
          W2xyz5(il,kl) = ccni2D(il,kl)  *pst2Dn(il) *dsigm1(kl)
     .  + gravit *xtmn  *(ccni2D(il,kl-1)*vi(il,kl-1)*rolv2D(il,kl-1)
     .                   -ccni2D(il,kl)  *vi(il,kl)  *rolv2D(il,kl)  )
         END DO
         END DO
 
         DO il=    1,klon
         W2xyz1(il,mzhyd)=  qi2D(il,mzhyd)*pst2Dn(il)  *dsigm1(mzhyd)
     .  -gravit *xtmn    *  qi2D(il,mzhyd)*vi(il,mzhyd)*rolv2D(il,mzhyd)
         W2xyz5(il,mzhyd)=ccni2D(il,mzhyd)*pst2Dn(il)  *dsigm1(mzhyd)
     .  -gravit *xtmn    *ccni2D(il,mzhyd)*vi(il,mzhyd)*rolv2D(il,mzhyd)
         END DO
 
         DO kl =      mzhyd,klev
         DO il=     1,klon
            qi2D(il,kl) = W2xyz1(il,kl) / (pst2Dn(il)  *dsigm1(kl))
          ccni2D(il,kl) = W2xyz5(il,kl) / (pst2Dn(il)  *dsigm1(kl))
         END DO
         END DO
 
        END DO
 
 
!  Fractional  Cloudiness ! Guess may be computed (Ek&Mahrt91 fracSC=.T.)
!  ====================== ! Final value  computed  below
 
! #sc   IF (fracld.AND..NOT.fracSC)                                 THEN
        IF (fracld)                                                 THEN
         IF(fraCEP) THEN ! ECMWF Large Scale Cloudiness
                         ! ----------------------------
          DO kl=mzhyd,klev
          DO il=1,klon
              cfra2D(il,kl) =             (qi2D(il,kl) +   qw2D(il,kl)
     .                                    +qs2D(il,kl) *  0.33
     .                 * (1.-min(1.,exp((tair2D(il,kl) -258.15)*0.1))))
     .                 / (0.02     *     qvsw2D(il,kl)                )
              cfra2D(il,kl) =min(1.000 , cfra2D(il,kl))
              cfra2D(il,kl) =max(0.001 , cfra2D(il,kl))
     .                     *max(0.,sign(1.,qi2D(il,kl) +   qw2D(il,kl)
     .                                    +qs2D(il,kl) -3.E-9         ))
          END DO
          END DO
         ELSE            ! XU and Randall  1996, JAS 21, p.3099 (4)
                         ! ----------------------------
          DO kl=mzhyd,klev
          DO il=1,klon
              qvs_wi=                                    qvsw2D(il,kl)
c #wi         qvs_wi=max(eps9,((qi2D(il,kl)+qs2D(il,kl))*qvsi2D(il,kl)
c #wi.                         +qw2D(il,kl)             *qvsw2D(il,kl))
c #wi.                /max(eps9,qi2D(il,kl)+qs2D(il,kl) +  qw2D(il,kl)))
              relhum=  min(relCri,      max(qv2D(il,kl) ,qv_MIN)
     .                                                 / qvs_wi)
              argexp=  (  (relCri  -relhum)            * qvs_wi)
     .                         **   0.49
              argexp        =   min(100.* (qi2D(il,kl) +   qw2D(il,kl)
     .                                    +qs2D(il,kl) *  0.33
     .                 * (1.-min(1.,exp((tair2D(il,kl) -258.15)*0.1))))
     .                                /max(eps9        , argexp       )
     .                             ,argmax                             )
 
              cfra2D(il,kl) =      (     relhum       ** 0.25         )
     .                         *   (1.  -   exp(-argexp)              )
          END DO
          END DO
         END IF
 
        ELSE
! #sc   ELSE IF (      .NOT.fracld)                                 THEN
          DO kl=mzhyd,klev
          DO il=1,klon
              qcloud        =              qi2D(il,kl) +   qw2D(il,kl)
            IF                            (qcloud     .gt.eps9)     THEN
 
              signQW        =  sign(unun,  qcloud       - eps9)
              cfra2D(il,kl) =   max(zero,signQW)
!             cfra2D(il,kl) =   1.0 if     qcloud       > eps9
!                           =   0.0 otherwise
 
            END IF
          END DO
          END DO
 
        END IF
 
 
!  Vertically Integrated Energy and Water Content
!  ==============================================
 
c #EW   DO il=1,klon
c #EW     enr21D(il) = 0.0d00
c #EW     wat21D(il) = 0.0d00
C +...    Vertical Integrated Energy and Water Content
 
c #EW   DO kl=1,klev
c #EW     enr21D(il) = enr21D(il )
c #EW.               +(tair2D(il,kl)
c #EW.                 -(qw2D(il,kl)+qr2D(il,kl))*r_LvCp
c #EW.                 -(qi2D(il,kl)+qs2D(il,kl))*r_LsCp   ) *dsigm1(kl)
c #EW     wat21D(il) = wat21D(il )
c #EW.               +  (qv2D(il,kl)
c #EW.               +   qw2D(il,kl)+qr2D(il,kl)
c #EW.               +   qi2D(il,kl)+qs2D(il,kl)           ) *dsigm1(kl)
c #EW   END DO
 
c #ew     enr21D(il) = enr21D(il ) * pst2Dn(il) * grvinv
c #EW     wat21D(il) = wat21D(il ) * pst2Dn(il) * grvinv
C +...    wat21D [m]   contains implicit factor 1.d3 [kPa-->Pa] /ro_Wat
 
c #EW   END DO
 
 
!  OUTPUT
!  ======
 
c #WH IF (mod(minuGE,6).eq.0.and.jsecGE.eq.0.and.ilmm.gt.0)       THEN
c #WH   write(6,1030) jhlr2D(ilmm),minuGE,jsecGE,itexpe,imm,jmm
 1030   format(//,i4,'LT',i2,'m',i2,'s (iter.',i6,')  /  Pt.(',2i4,')',
     .          /,'  ==========================================')
c #WH   write(6,1031)(kl,0.1019d0*gplv2D(ilmm,kl),qv2D(ilmm,kl),
c #WH.   1.d3*qiold(kl),1.d3*qi2D(ilmm,kl),
c #WH.   1.d3*wihm1(kl),1.d3*wihm2(kl),1.d3*wicnd(kl),
c #WH.   1.d3*widep(kl),1.d3*wisub(kl),1.d3*wimlt(kl),kl=mzhyd,klev)
 1031   format(/,
     .     '            |  Water Vapor |  Cloud Ice, Time n & n+1',
     .     '   Cloud Ice Nucleation Processes    |',
     .     '   Bergeron   Sublimation   Melting  ',
     .   /,'  k    z[m] |  qv   [g/kg] |  qi_n [g/kg] qi_n+[g/kg]',
     .     ' QiHm1[g/kg] QiHm2[g/kg] QiCnd[g/kg] |',
     .     '  QiDep[g/kg] QiSub[g/kg] QiMlt[q/kg]',
     .   /,'------------+--------------+-------------------------',
     .     '-------------------------------------+',
     .     '-------------------------------------',
     .   /,(i3,f8.1,' | ',f12.6,' | ',2f12.6,3d12.4,' | ',3d12.4))
 
c #WH   write(6,1032)(kl,0.1019d0*gplv2D(ilmm,kl),
c #WH.   1.d3*W2xyz4(ilmm,kl),1.d3*qs2D(ilmm,kl),
c #WH.   1.d3*wsaut(kl),1.d3*wsaci(kl),1.d3*wsacw(kl),
c #WH.   1.d3*wiacr(kl),1.d3*wsacr(kl),1.d3*wssub(kl),vs(ilmm,kl),
c #WH.              kl=mzhyd,klev)
 1032   format(/,
     .     '            |  Snow Flakes, Time n&n+1 Autoconver. |',
     .     '  Accretion Processes ===> Snow Flakes            |',
     .     '  Sublimation | Term.F.Vel',
     .   /,'  k    z[m] |  qs_n [g/kg] qs_n+[g/kg] Qsaut[g/kg] |',
     .     '  Qsaci[g/kg] Qsacw[g/kg] Qiacr[g/kg] Qsacr[g/kg] |',
     .     '  QsSub[g/kg] | vs   [m/s]',
     .   /,'------------+--------------------------------------+',
     .     '--------------------------------------------------+',
     .     '--------------+-----------',
     .   /,(i3,f8.1,' | ',2f12.6,e12.4,' | ',4d12.4,' | ',e12.4,
     .              ' | ',f10.6))
 
c #WH   write(6,1033)(kl,0.1019d0*gplv2D(ilmm,kl),tair2D(ilmm,kl),
c #WH.   1.d3*qwold(kl),1.d3*  qw2D(ilmm,kl),
c #WH.   1.d3*wwevp(kl),1.d2*cfra2D(ilmm,kl),kl=mzhyd,klev)
 1033   format(/,
     .   /,'            | Temperat.|  Cloud Water, Time n&n+1',
     .     ' Condens/Evp | Cloud ',
     .   /,'  k    z[m] | T    [K] |  qw_n [g/kg] qw_n+[g/kg]',
     .     ' QwEvp[g/kg] | Fract.',
     .   /,'------------+----------+-------------------------',
     .     '-------------+-------',
     .   /,(i3,f8.1,' | ',f8.3,' | ',2f12.6,e12.4,' | ',f5.1))
 
c #WH   write(6,1034)(kl,0.1019d0*gplv2D(ilmm,kl),
c #WH.   1.d3*W2xyz3(ilmm,kl),1.d3*qr2D(ilmm,kl),
c #WH.   1.d3*wraut(kl),1.d3*wracw(kl),1.d3*wraci(kl),
c #WH.   1.d3*wracs(kl),1.d3*wrevp(kl),1.d3*wsfre(kl),vr(ilmm,kl),
c #WH.              kl=mzhyd,klev)
 1034   format(/,
     .  /,'            | Rain Drops, Time n&n+1   Autoconver. |',
     .    '  Accretion Processes ===> Rain Drops |',
     .    '  Evaporation  Freezing   | Term.F.Vel',
     .  /,'  k    z[m] |  qr_n [g/kg] qr_n+[g/kg] Qraut[g/kg] |',
     .    '  Qracw[g/kg] Qraci[g/kg] Qracs[g/kg] |',
     .    '  QrEvp[g/kg] QsFre[g/kg] | vr   [m/s]',
     .  /,'------------+--------------------------------------+',
     .    '--------------------------------------+',
     .    '--------------------------+-----------',
     .  /,(i3,f8.1,' | ',2f12.6,e12.4,' | ',3d12.4,' | ',2d12.4,
     .             ' | ',f10.6))
 
c #WH   DO kl=mzhyd,klev
c #WH     wihm1(kl) = 0.d0
c #WH     wihm2(kl) = 0.d0
c #WH     wicnd(kl) = 0.d0
c #WH     widep(kl) = 0.d0
c #WH     wisub(kl) = 0.d0
c #WH     wimlt(kl) = 0.d0
c #WH     wwevp(kl) = 0.d0
c #WH     wraut(kl) = 0.d0
c #WH     wsaut(kl) = 0.d0
c #WH     wracw(kl) = 0.d0
c #WH     wsacw(kl) = 0.d0
c #WH     wsaci(kl) = 0.d0
c #WH     wraci(kl) = 0.d0
c #WH     wiacr(kl) = 0.d0
c #WH     wsacr(kl) = 0.d0
c #WH     wracs(kl) = 0.d0
c #WH     wrevp(kl) = 0.d0
c #WH     wssub(kl) = 0.d0
c #WH     wsmlt(kl) = 0.d0
c #WH     wsfre(kl) = 0.d0
c #WH   END DO
c #WH END IF
 
 
!  Vertical Integrated Energy and Water Content: OUTPUT
!  ====================================================
 
c #EW IF (ilmez.gt.0)                                             THEN
c #EW   waterb = wat21D(ilmez)-wat11D(ilmez)-watf1D(ilmez)
c #EW   write(6,606) itexpe,
c #EW.                     enr01D(ilmez),1.d3*wat01D(ilmez),
c #EW.                     mphy2D(ilmez),
c #EW.                     enr11D(ilmez),1.d3*wat11D(ilmez),
c #EW.                     enr21D(ilmez),1.d3*wat21D(ilmez),
c #EW.                                   1.d3*watf1D(ilmez),
c #EW.                                   1.d3*waterb
 606    format(i9,'  Before mPhy:  E0 =',f12.6,'  W0 = ',f9.6,3x,a20,3x,
     .       /,9x,'  Before Prec:  E1 =',f12.6,'  W1 = ',f9.6,
     .       /,9x,'  After  Prec:  E2 =',f12.6,'  W2 = ',f9.6,
     .                                         '  W Flux =',f9.6,
     .                                         '  Div(W) =',e9.3)
c #EW END IF
 
      IF (jmmMAR.eq.0.and.jssMAR.eq.0)                            THEN
            IO_loc =  IO_gen + 2
        DO  io=io1   ,io5
 
          IF         (io.gt.0)                                    THEN
            il=ioutIO(io)
 
            IF((  itexpe.gt.0.and.jmmMAR.eq.0.and.jssMAR.eq.0.and.
     .          ((IO_loc.ge.4.and.    jhurGE   .eq.0) .or.
     .           (IO_loc.ge.5.and.mod(jhurGE,3).eq.0) .or.
     .           (IO_loc.ge.6)                            )       ).or.
     .            IO_loc.ge.7                                   ) THEN
 
!             ***********
              call TIMcor
!             ***********
 
              write(4,1037) jdplus,mmplus,jhlr2D(il),minuGE,
     .                      igrdIO(io),jgrdIO(io)
 1037         format(/,' Ice-Crystal mPhy ',
     .                   i2,'/',i2,1x,i2,'h',i2,'LT',
     .                 ' -- Grid Point (',i5,',',i5,')',
     .  /,' ==========================================================',
     .         /,'     |  z  [m] | T  [K] | qi[g/kg] |',
     .                ' Ni [m-3] | Ni0[m-3] | vi [m/s] | qs[g/kg] |'
     .         /,'-----+---------+--------+----------+',
     .                '----------+----------+----------+----------+')
              write(4,1038)(kl,gplv2D(il,kl)*grvinv,tair2D(il,kl),
     .                          qi2D(il,kl)*1.d3,
     .                        ccni2D(il,kl),W2xyz2(il,kl),vi(il,kl),
     .                          qs2D(il,kl)*1.d3,kl=mzhyd,klev)
 1038         format((i4,' |' ,  f8.1,' |',f7.2,' |',f9.6,' |',
     .            2(d9.3,' |'),2(f9.6,' |')))
            END IF
 
          END IF
 
        END DO
 
            IO_loc = IO_gen
 
      END IF
 
c #WH ilmm = ilmmi
 
 
!  Latent Heat Release
!  ===================
 
      DO kl=mzhyd,klev
      DO il=1,klon
        pkt0          =  pkta2D(il,kl)
        pkta2D(il,kl) =  tair2D(il,kl)/pk2D(il,kl)
        hlat2D(il,kl) =  tair2D(il,kl)*(1.d0-pkt0/pkta2D(il,kl))
     .                                  /xt
      END DO
      END DO
 
 
!  Limits on Microphysical Variables
!  =================================
 
        DO kl=1,mzhyd
        DO il=1,klon
            qv2D(il,kl)=max(qv2D(il,kl),qv_MIN)
            qv2D(il,kl)=min(qv2D(il,kl),qvsi2D(il,kl))
            qw2D(il,kl)=    zero
            qi2D(il,kl)=    zero
          ccni2D(il,kl)=    zero
            qr2D(il,kl)=    zero
            qs2D(il,kl)=    zero
        END DO
        END DO
 
        DO kl=mzhyd,klev
        DO il=1,klon
            qw2D(il,kl)=max(zero,  qw2D(il,kl))
            qi2D(il,kl)=max(zero,  qi2D(il,kl))
          ccni2D(il,kl)=max(zero,ccni2D(il,kl))
            qr2D(il,kl)=max(zero,  qr2D(il,kl))
            qs2D(il,kl)=max(zero,  qs2D(il,kl))
        END DO
        END DO
 
        DO kl=1,klev
        DO il=1,klon
            W2xyz1(il,kl) = 0.d0
            W2xyz2(il,kl) = 0.d0
            W2xyz3(il,kl) = 0.d0
            W2xyz4(il,kl) = 0.d0
            W2xyz5(il,kl) = 0.d0
            W2xyz6(il,kl) = 0.d0
            W2xyz7(il,kl) = 0.d0
            W2xyz8(il,kl) = 0.d0
        END DO
        END DO
 
        return
        end
