 
 
       subroutine TURtke_gen(dt_dif)
 
C +------------------------------------------------------------------------+
C | MAR TURBULENCE (TKE)                               Mon 25-11-2010  MAR |
C |    SubRoutine TURtke_gen includes 1 1/2 Vertical Turbulence Closures   |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C | METHOD: 1. `Standard' Closure of Turbulence:                           |
C | ^^^^^^^     E - epsilon  , Duynkerke,           JAS 45, 865--880, 1988 |
C |  .OR.   2.  E - epsilon  , Huang and Raman,     BLM 55, 381--407, 1991 |
C |  .OR.   3.  TKE          , Therry et Lacarrere, BLM 25,  63-- 88, 1983 |
C |                                                                        |
C | INPUT  : itexpe: Nb of iterations                                      |
C | ^^^^^^^^ dt_dif: Local Time Step                                   (s) |
C |          explIO: Experiment Label                                  (s) |
C |                                                                        |
C | INPUT / OUTPUT: The Vertical Turbulent Fluxes are included for:        |
C | ^^^^^^^^^^^^^^                                                         |
C |   a) Turbulent Kinetic Energy             ect_TE(mx,my,mz)     (m2/s2) |
C |   b) Turbulent Kinetic Energy Dissipation eps_TE(mx,my,mz)     (m2/s3) |
C |                                                                        |
C | OUTPUT :  TUkvm(i,j,k): vertical diffusion coeff. for momentum  (m2/s) |
C | ^^^^^^^^  TUkvh(i,j,k): vertical diffusion coeff. for heat...   (m2/s) |
C |          zi__TE(i,j)  : inversion height                           (m) |
C |                                                                        |
C | OPTIONS: #De: Dirichlet Type Top Boundary Condit. for ect_TE & eps_TE  |
C | ^^^^^^^^ #WE: Output on MAR.TKE (unit 29)                              |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
 
      include 'MAR_DY.inc'
 
c #De include 'MAR_DI.inc'
 
      include 'MAR_TE.inc'
      include 'MAR_TU.inc'
 
      include 'MAR_HY.inc'
 
      include 'MAR_SL.inc'
 
      include 'MAR_WK.inc'
      include 'MAR_IO.inc'
 
      real     dt_dif
 
 
C +--Local  Variables
C +  ================
 
      logical                 log_KA
      integer                 locTKE,mz__KA
      common /TURtke_gen_loc/ locTKE,mz__KA
 
      integer i5    ,n5    ,kect  ,ke
c #WE integer m30   ,m31
 
      real    aalme(mx,mz)
      real    aalmk(mx,mz)
      real    flott(mx,mz)
 
      real    epse  ,epsd  ,ectmin,turmx ,dkect
      real    sqrcmu,sqrcmu0
      real    cmu   ,c1ep  ,c2ep  ,rc1c2 ,betahr,voninv
      real    cmu0  ,c1ep0 ,c2ep0
      real    ectcrt,akz   ,alme  ,almk  ,pousse
      real    hrel  ,base
      real    epslme,ectnew,produc,factur,zeta  ,stab_s,phimm
      real    edtnum,edtden
 
      real    phim  ,dzlev
      real    sature,sgnLMO,absLMO,se
      real    alamb ,Ri_Sat,fRiMin
      real    fac_Ri,vuzvun,Kz_vun
      real    kz_max,kz_mix,kz2mix,KvhMax(mx,my,mz)
 
 
C +--DATA
C +  ====
 
      data epse  /0.00000100/
      data epsd  /0.00000001/
 
      data ectmin/0.00010000/
C +...     ectmin:Minimum SBL    turbulent kinetic   energy
 
      data cmu /0.090/
C +...Bintanja , 2000, BLM (95),milieu p. 355
c #PD data cmu0/0.033/
C +...Duynkerke, 1988, JAS (45),  haut p. 868
c #KI data cmu0/0.090/
C +...Kitada   , 1987, BLM (41),  haut  p.220
 
      data sqrcmu /3.333/
C +...Bintanja , 2000, BLM (95),milieu p. 355
c #PD data sqrcmu0/5.500/
C +...Duynkerke, 1988, JAS (45),  (19) p. 869 :(c_mu)^1/2=(0.033)^1/2=5.50
c #KI data sqrcmu0/3.333/
C +...Kitada   , 1987, BLM (41),        p.220 :(c_mu)^1/2=(0.090)^1/2=3.333
c #Kl data sqrcmu0/4.000/
C +...Schayes and Thunis, 1990, Contrib. 60 Inst.Astr.Geoph. p.8
 
      data c1ep /1.46/
C +...Duynkerke, 1988, JAS (45),milieu p. 868
C +...Bintanja , 2000, BLM (95),milieu p. 355
c #KI data c1ep0/1.44/
C +...Kitada   , 1987, BLM (41),  haut  p.220
 
      data c2ep /1.92/
C +...Bintanja , 2000, BLM (95),milieu p. 355
c #PD data c2ep0/1.83/
C +...Duynkerke, 1988, JAS (45),milieu p. 868
c #KI data c2ep0/1.92/
C +...Kitada   , 1987, BLM (41),  haut  p.220
 
c #HR data betahr/2.00/
C +...Huang and Raman, 1991, BLM (55), p.386 and (A22) p.405
 
      data  alamb/0.1    /! 1 / 10 m
      data Ri_Sat/2.0    /! Mahalov&al., 2004, GRL
      data fRiMin/0.00925/! 1/(1+4.7*Ri_Sat)**2                    Louis  1979
c #82 data fRiMin/0.07970/! 1/(1+10 *Ri_Sat/(1+Ri_Sat))            Louis& 1982
 
c #PD cmu    = cmu0
c #KI cmu    = cmu0
c #PD sqrcmu = sqrcmu0
c #KI sqrcmu = sqrcmu0
c #Kl sqrcmu = sqrcmu0
c #KI c1ep   = c1ep0
c #PD c2ep   = c2ep0
c #KI c2ep   = c2ep0
 
      rc1c2  = c1ep / c2ep
 
 
C +--Parameters
C +  ==========
 
      voninv=1./vonkar
 
      n5    =         5
      n5    = min(mx,n5)
 
 
C +--Initialisation
C +  ==============
 
       IF (.NOT. ini_KA_TE)                                         THEN
                 ini_KA_TE = .true.
             mz__KA=mz1
 11      CONTINUE
         IF (zsigma(mz__KA).GT.  5..OR.mz__KA.LE.1)             GO TO 10
             mz__KA=mz__KA-1
                                                                GO TO 11
 10      CONTINUE
             write(6,1000)      mz  -  mz__KA
 1000        format(/,' TKE: Moving Average until mz -',i2,' Level',/)
       END IF
 
 
       IF (locTKE.eq.0.and.itexpe.le.1)                             THEN
 
c #Kl        log_KA = .false.
             log_KA = .true.
c #Kl    IF (log_KA)                                                THEN
c #Kl        write(6,6000)
 6000        format('!?%@&* Conflicting Use of #KA and  #Kl',
     .                ' ==> Emergency EXIT in TURtke_gen',6x,
     .                ' ### DO NOT PRE-PROCESS #KA with #Kl ###')
c #Kl        STOP
c #Kl    END IF
 
 
C +--Minimum Vertical Turbulent Diffusion Coefficient (ARPS.4.0 Users Guide,
C +  ------------------------------------------------  fin para 6.3.4 p.143)
 
         DO k=1,mz1
           dzlev         =               zsigma(k)-zsigma(k+1)
           TUmin (    k) =                      akmol
c #ARPS    TUmin (    k) = min(0.15,     epsi * dzlev * dzlev )
         END DO
            k=  mz
           dzlev         =               zsigma(k)
           TUmin (    k) =                      akmol
c #ARPS    TUmin (    k) = min(0.15,     epsi * dzlev * dzlev )
 
      ENDIF
 
C +--Initial E,e
C +  -----------
 
!$OMP PARALLEL DO
!$OMP. private(i,j,k,hrel,sature,base,sgnLMO,absLMO,dkect,
!$OMP. akz,alme,almk,pousse,epslme,ectnew,produc,factur,
!$OMP. se,ke,edtden,edtnum,kz2mix,kz_max,kz_mix,
!$OMP. Kz_vun,fac_Ri,vuzvun)
         DO j=1,my
       IF (locTKE.eq.0.and.itexpe.le.1)                             THEN
         DO k=1,mz
         DO i=1,mx
           ect_TE(i,j,k) = epse
           eps_TE(i,j,k) = epsd
C +...     These initial values of TKE and epsilon correspond to K ~ Kmol
 
           TUkvm (i,j,k) = akmol
           TUkvh (i,j,k) = akmol
         END DO
         END DO
c        END DO
 
c        DO j=1,my
         DO i=1,mx
                 zi__TE(i,j) =  max(gpmiDY(i,j,mz)* grvinv-sh(i,j),
     .                              zi__TE(i,j))
         END DO
c        END DO
 
 
C +--Verification: TKE must be Positive Definite
C +  ===========================================
 
       ELSE
c        DO j=1,my
         DO k=1,mz
         DO i=1,mx
           ect_TE(i,j,k)=max(epse,ect_TE(i,j,k))
           eps_TE(i,j,k)=max(epsd,eps_TE(i,j,k))
         END DO
         END DO
c        END DO
 
 
C +--Inversion Height
C +  ================
 
c        DO j=1,my
         DO i=1,mx
           WKxy1(i,j) = 0.05*max(max(ect_TE(i,j,mmz1),
     .                               ect_TE(i,j,mz  )),ectmin)
           WKxy2(i,j) = 1.
         ENDDO
cc#vK    ENDDO
 
         DO k=1,mz
cc#vK    DO j=1,my
         DO i=1,mx
           IF   (ect_TE(i,j,k) .lt. WKxy1(i,j))                     THEN
                  WKxy2(i,j) =  min(mz1,k)
           END IF
         ENDDO
         ENDDO
cc#vK    ENDDO
 
cc#vK    DO j=1,my
         DO i=1,mx
             k =  WKxy2(i,j)
             IF (ect_TE(i,j,k+1).lt.ectmin)                         THEN
                  WKxy1(i,j) =      gpmiDY(i,j,mz)* grvinv
     .                            -     sh(i,j)
             ELSE
                  dkect      =      ect_TE(i,j,k)  -ect_TE(i,j,k+1)
                  WKxy1(i,j) =  (   gpmiDY(i,j,k+2)
     .                            +(gpmiDY(i,j,k+1)-gpmiDY(i,j,k+2))
     .                            *(WKxy1(i,j)     -ect_TE(i,j,k+1))
     .             /(sign(unun ,    dkect                           )
     .               *max(eps9 ,abs(dkect                          )))
     .                            - gplvDY(i,j,mzz)
     .                          )                 * grvinv
             END IF
         ENDDO
cc#vK    ENDDO
 
cc#vK    DO j=1,my
         DO i=1,mx
                 zi__TE(i,j) =  min( WKxy1(i,j),
     .                              gpmiDY(i,j,1) * grvinv-sh(i,j))
         ENDDO
cc#vK    ENDDO
 
cc#vK    DO j=1,my
         DO i=1,mx
                 zi__TE(i,j) =  max(gpmiDY(i,j,mz)* grvinv-sh(i,j),
     .                              zi__TE(i,j))
                  WKxy1(i,j) =  0.
                  WKxy2(i,j) =  0.
 
c #WR      IF   (zi__TE(i,j).gt.10000.)                           THEN
c #WR            kect = k
c #WR            kect = min(mz-3,kect)
c #WR            kect = max(   5,kect)
c #WR      write(6,6001) i,j,k,kect,(           ke ,ke=kect+3,kect-4,-1)
c #WR.           ,zi__TE(i,j),      (ect_TE(i,j,ke),ke=kect+3,kect-4,-1)
 6001      format('zi / TKE',2(i6,i4),8i10,/,d10.3,18x,8d10.3)
c #WR      END IF
 
         END DO
c        END DO
       END IF
 
 
C +--TKE Production/Destruction by the Vertical Wind Shear
C +  =====================================================
 
c      DO j=1,my
       DO k=kp1(1),mmz1
       DO i=1,mx
         WKxyz3(i,j,k)= uairDY(i,j,k)- uairDY(i,j,k+1)
         WKxyz4(i,j,k)= vairDY(i,j,k)- vairDY(i,j,k+1)
       END DO
       END DO
C      END DO
 
       DO k=1,mz
c      DO j=1,my
       DO i=1,mx
         WKxyz5(i,j,k)=
     .             gravit/(gplvDY (i,j,k)-gplvDY (i,j,k+1))
C +...   1/dz(k+1/2)
       END DO
c      END DO
       END DO
 
       DO k=kp1(1),mmz1
c      DO j=1,my
       DO i=1,mx
         WKxyz1(i,j,k)=
     .    TUkvm(i,j,k)*(WKxyz3(i,j,k)*WKxyz3(i,j,k)
     .                 +WKxyz4(i,j,k)*WKxyz4(i,j,k))
     .                * WKxyz5(i,j,k)*WKxyz5(i,j,k)
       END DO
c      END DO
       END DO
 
c      DO j=1,my
       DO i=1,mx
         WKxyz1(i,j,mz)=0.0
       END DO
c      END DO
 
 
C +--Buoyancy
C +  ========
 
C +--Reduced (Equivalent) Potential Temperature
C +  ------------------------------------------
 
c      DO j=1,my
       DO i=1,mx
          WKxy5(i,j)   = pktaDY(i,j,mzz)
c #AV.       * (1.0+0.715*virSL(i,j    ))
     .       *exp(Lv_H2O*qvapSL(i,j)  /(cp*TairSL(i,j))  )
       END DO
c      END DO
 
       DO k=1,mz
c      DO j=1,my
       DO i=1,mx
         WKxyz2(i,j,k) = pktaDY(i,j,k)
c #AV.       * (1.0+0.715*virDY(i,j,k))
     .         *exp(Lv_H2O*qvDY(i,j,k)/(cp*tairDY(i,j,k)))
       END DO
c      END DO
       END DO
 
 
C +--Buoyancy Coefficients
C +  ---------------------
 
       DO k=1,mz
cc#HY    DO j=1,my
         DO i=1,mx
 
             hrel       = 0.50*(  qvDY(i,j,k)     /qvswDY(i,j,k) +
     .                            qvDY(i,j,kp1(k))/qvswDY(i,j,k))
             WKxy3(i,j) = 0.50*(qvswDY(i,j,k)+qvswDY(i,j,k+1))
             WKxy4(i,j) = 0.50*(tairDY(i,j,k)+tairDY(i,j,kp1(k)))
 
             sature     = max(0.,sign(1.,hrel+eps12-1.))
             base       =        WKxy3(i,j)*Lv_H2O/(RDryAi*WKxy4(i,j))
 
C +--Vectorization of the unsaturated (H<1) and saturated cases (H=1.)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
             WKxy1(i,j) =                                               !
     .            1.-sature                                             ! H<1.
     .              +sature*(1.0+base)                                  ! H=1.
     .                     /(1.0+base*0.605*Lv_H2O/(cp    *WKxy4(i,j))) !
C +...       C_thq (Duynkerke & Driedonks 1987 JAS 44(1), Table 1 p.47) !
 
             WKxy2(i,j) =                                               !
     .           (1.-sature)              *(Lv_H2O/(cp    *WKxy4(i,j))  ! H<1.
     .                                     - 0.605                    ) !
     .              +sature                                             ! H=1.
C +...       C_q_w (Duynkerke and Driedonks 1987)                       !
C +                with (1-Ra/Rv)/(Ra/Rv) =  0.605 [Ra=287.J/kg/K;      !
C +                                                 Rv=461.J/kg/K]      !
 
C +--Unsaturated Case
C +  ~~~~~~~~~~~~~~~~
C +        IF (hrel.lt.1.0)                                       THEN
C +          WKxy1(i,j) = 1.0
C +          WKxy2(i,j) =                    Lv_H2O/(cp*WKxy4(i,j))
C +  .                                     - 0.605
 
C +--Saturated   Case
C +  ~~~~~~~~~~~~~~~~
C +        ELSE
C +          base       =      WKxy3(i,j)*Lv_H2O/(RDryAi*WKxy4(i,j))
C +          WKxy1(i,j) = (1.0+base)
C +  .                   /(1.0+base*0.605*Lv_H2O/(cp    *WKxy4(i,j)))
C +          WKxy2(i,j) =  1.0
C +        END IF
 
         END DO
cc#HY    END DO
 
 
C +--Buoyancy
C +  --------
 
         IF (k.le.mmz1)                                           THEN
c          DO j=1,my
           DO i=1,mx
             WKxyz3(i,j,k)= gravit
     .      *WKxyz5(i,j,k) *( (WKxyz2(i,j,k)-WKxyz2(i,j,k+1)) *2.0
     .                       /(WKxyz2(i,j,k)+WKxyz2(i,j,k+1))
     .                       *  WKxy1(i,j)
     .                      -   WKxy2(i,j)
     .                       *(  qvDY(i,j,k)-  qvDY(i,j,kp1(k))
     .                          +qwHY(i,j,k)-  qwHY(i,j,kp1(k))
     .                          +qrHY(i,j,k)-  qrHY(i,j,kp1(k))
     .                          +qiHY(i,j,k)-  qiHY(i,j,kp1(k))
     .                          +qsHY(i,j,k)-  qsHY(i,j,kp1(k)))
     .                                                          )
C +...      (g/theta)                  X(dtheta/dz) :
C +          Buoyancy Parameter beta X grad.vert.temp.pot. en k+1/2
           END DO
c          END DO
 
         ELSE
c          DO j=1,my
           DO i=1,mx
             WKxyz3(i,j,k)= gravit
     .      *WKxyz5(i,j,k) *( (WKxyz2(i,j,k)- WKxy5(i,j))     *2.0
     .                       /(WKxyz2(i,j,k)+ WKxy5(i,j))
     .                       *  WKxy1(i,j)
     .                                                          )
C +...      (g/theta)                  X(dtheta/dz) :
C +          Buoyancy Parameter beta X grad.vert.temp.pot. en k+1/2
           END DO
c          END DO
         END IF
       END DO
 
 
C +--Length Scales Parameters (Therry and Lacarrere 1983 Model)
C +  ========================
 
c      DO j=1,my
       DO i=1,mx
         WKxy3 (i,j  )= 1.0/zi__TE(i,j)
 
         WKxy1 (i,j  )=15.0* WKxy3(i,j)
C +...   Ce1/hi
         WKxy2 (i,j  )= 5.0* WKxy3(i,j)
C +...   Ce1/hi
         WKxy4 (i,j  )=11.0* WKxy3(i,j)
C +...   Ck2/hi
       END DO
c      END DO
 
c      DO j=1,my
       DO i=1,mx
         sgnLMO   = sign(unun,SLlmo(i,j))
         absLMO   =  abs(     SLlmo(i,j))
         SLlmo(i,j) =         sgnLMO      * max(absLMO,eps12)
         WKxy3(i,j) = -min(0.,sgnLMO)
     .          /(1.0 -min(0.,SLlmo(i,j))      /zi__TE(i,j))
 
C +--Replacement of:
C +      IF (SLlmo(i,j).lt.0.0)                                   THEN
C +          SLlmo(i,j) =        min(SLlmo(i,j),-eps12)
C +          WKxy3(i,j) =  1.0/(1.d0-SLlmo(i,j)/zi__TE(i,j))
C +      ELSE
C +          SLlmo(i,j) =        max(SLlmo(i,j), eps12)
C +          WKxy3(i,j) =  0.0
C +      END IF
C +...  m2
c      END DO
       END DO
 
       DO k=kp1(1),mz
c      DO j=1,my
       DO i=1,mx
         WKxyz4(i,j,k) = sqrt(max(zero,WKxyz3(i,j,k))/ect_TE(i,j,k))
C +...   1/ls
       END DO
c      END DO
       END DO
 
 
C +--Dissipation Length
C +  ------------------
 
       DO k=kp1(1),mz
c      DO j=1,my
       DO i=1,mx
         akz=voninv*gravit/(gpmiDY(i,j,k+1)-gplvDY(i,j,mzz))
C +...   1/kz(i,j,k+1/2)
C +
         alme=akz+WKxy1(i,j)
     .    -(akz+WKxy2(i,j))*WKxy3(i,j)/(1.+5.0e-3*zi__TE(i,j)*akz)
     .     +1.5*WKxyz4(i,j,k)
C +...   alme=1/Dissipation Length (Therry and Lacarrere, 1983 BLM 25 p.75)
 
C +--Mixing Length
C +  -------------
 
         almk=akz+WKxy1(i,j)
     .    -(akz+WKxy4(i,j))*WKxy3(i,j)/(1.+2.5e-3*zi__TE(i,j)*akz)
     .     +3.0*WKxyz4(i,j,k)
C +...   almk=1/Mixing Length      (Therry and Lacarrere, 1983 BLM 25 p.78)
 
 
C +--Contribution of Vertical Wind Shear + Buoyancy + Dissipation to TKE
C +  ===================================================================
 
         pousse=-TUkvh(i,j,k) *   WKxyz3(i,j,k)
 
         epslme= eps_TE(i,j,k)
c #Kl    epslme= 0.125*alme*sqrt(ect_TE(i,j,k))*ect_TE(i,j,k)
         ectnew= ect_TE(i,j,k) *
     .          (ect_TE(i,j,k)+dt_dif*(WKxyz1(i,j,k)+max(zero,pousse)))
     .         /(ect_TE(i,j,k)+dt_dif*(             -min(zero,pousse)
     .                                               +epslme       ))
C +...   Numerical Scheme : cfr. E. Deleersnijder, 1992 (thesis) pp.59-61
 
 
C +--Contribution of Vertical Wind Shear + Buoyancy to epsilon
C +  =========================================================
 
c #HR                                                tranTE(i,j,k)=zero
         produc = WKxyz1(i,j,k)+max(pousse,zero)+max(tranTE(i,j,k),zero)
 
c #KI    produc = WKxyz1(i,j,k)+max(zero,pousse)
C +...   based on standard values of Kitada, 1987, BLM 41, p.220
 
c #BH    produc = WKxyz1(i,j,k)+max(zero,pousse) * 1.80
c #BH.                         -min(zero,pousse) * 1.15
C +...   based on          values of Betts et Haroutunian, 1983
C +      can be used by replacing strings `c #KI' (except the previous one)
C +                                   and `c #BH' by blanks
C +                            (cfr. Kitada, 1987, BLM 41, p.220):
C +      buoyancy > 0 (unstability) => (1-ce3) X buoyancy = 1.8  X buoyancy
C +      buoyancy < 0 (  stability) => (1-ce3) X buoyancy =-1.15 X buoyancy
 
         factur = eps_TE(i,j,k) /ect_TE(i,j,k)
         eps_TE(i,j,k) =
     .            eps_TE(i,j,k)
     .          *(eps_TE(i,j,k) +dt_dif *factur *c1ep *produc)
     .          /(eps_TE(i,j,k) +dt_dif *factur *c2ep *eps_TE(i,j,k))
C +...   Numerical Scheme : cfr. E. Deleersnijder, 1992 (thesis) pp.59-61
 
c #Kl    eps_TE(i,j,k)=epslme
 
 
C +--New TKE Value
C +  =============
 
         ect_TE(i,j,k)=ectnew
 
 
C +--Dissipation Lengths Variables are Assigned for Output Purpose
C +  =============================================================
 
         WKxyz1(i,j,k)=alme
         WKxyz2(i,j,k)=almk
       END DO
c      END DO
       END DO
 
 
C +--OUTPUT Preparation
C +  ==================
 
c       IF((  itexpe.gt.0.and.jmmMAR.eq.0.and.jssMAR.eq.0.and.
c     .     ((IO_loc.ge.2.and.    jhurGE   .eq.0) .or.
c     .      (IO_loc.ge.2.and.mod(jhurGE,3).eq.0) .or.
c     .      (IO_loc.ge.3)                            )       ).or.
c     .       IO_loc.ge.7                                          ) THEN
c
c         DO i5 = 1,n5
c             aalme(i5,1)  =0.0
c             aalmk(i5,1)  =0.0
c             flott(i5,1)  =0.0
c         DO k = kp1(1),mz
c           IF (WKxyz1(igrdIO(i5),jgrdIO(i5),k).gt.0.0)              THEN
c             aalme(i5,k) = 1.0/WKxyz1(igrdIO(i5),jgrdIO(i5),k)
c           ELSE
c             aalme(i5,k) = 0.0
c           END IF
cC +...       Le
c           IF (WKxyz2(igrdIO(i5),jgrdIO(i5),k).gt.0.d0)             THEN
c             aalmk(i5,k) = 1.0/WKxyz2(igrdIO(i5),jgrdIO(i5),k)
c           ELSE
c             aalmk(i5,k) = 0.0
c           END IF
cC +...       Lk
c             flott(i5,k)  = - WKxyz3(igrdIO(i5),jgrdIO(i5),k)
cC +...       proportional to the buoyant force (e.g. Demoor, 1978, p.47)
c         END DO
c         END DO
c       END IF
 
 
C +--Lower Boundary Conditions
C +  =========================
 
       DO i=1,mx
c      DO j=1,my
          WKxy1(i,j)    =  SLuus(i,j)      * SLuus(i,j)            ! -> TKE SBC
          WKxy3(i,j)    =(gplvDY(i,j,mz)   -gplvDY(i,j,mzz))*grvinv!    z_SBL
cc#vL  ENDDO
       ENDDO
 
       DO i=1,mx
cc#vL  DO j=1,my
          WKxy2(i,j)    =  WKxy1(i,j)      * SLuus(i,j)            ! -> e   SBC
         ect_TE(i,j,mz) =  WKxy1(i,j)      *sqrcmu                 !    TKE SBC
 
          WKxy4(i,j)    =  WKxy3(i,j)      / SLlmo(i,j)            !    zeta
          WKxy5(i,j)    =  max(0.0,sign(unun,SLlmo(i,j)))          !
cc#vL  ENDDO
       ENDDO
 
       DO i=1,mx
cc#vL  DO j=1,my
         eps_TE(i,j,mz) =  WKxy2(i,j)
     .            *(      (WKxy5(i,j) *(1.+A_Turb*    WKxy4(i,j))  ! phim Stab.
     .               +(1.0-WKxy5(i,j))/(1.-20.*min(0.,WKxy4(i,j))))! phim Inst.
     .            *voninv /WKxy3(i,j)
     .            -voninv /SLlmo(i,j))
C +...   Duynkerke, 1988, JAS (45), (19) p. 869
 
c #KI    eps_TE(i,j,mz) =  WKxy2(i,j)
c #KI.            *voninv /WKxy3(i,j)
cc#vL  ENDDO
       ENDDO
 
 
C +--When TKE Closure is Used, TKE is Modified near the Lower Boundary
C +  -----------------------------------------------------------------
 
       DO i=1,mx
cc#vL  DO j=1,my
         se = max(0.,sign(unun,ect_TE(i,j,mmz2)-ect_TE(i,j,mmz1)))
         ke =                             mmz1 -    se
         ect_TE(i,j,mmz1)=     ect_TE(i,j,ke  )
         eps_TE(i,j,mmz1)=     eps_TE(i,j,ke  )
C +...   Schayes and Thunis, 1990, Contrib. 60 Inst.Astr.Geoph. p.8, 1.4.4.
 
! #KC    ect_TE(i,j,mmz1)=     ect_TE(i,j,mz  )
! #KC    eps_TE(i,j,mmz1)=     eps_TE(i,j,mz  )
 
 
C +--Upper Boundary Conditions
C +  =========================
 
         ect_TE(i,j,1) = ect_TE(i,j,kp1(1))
c #De    ect_TE(i,j,1) = ect_TE(i,j,    mz)*rolvDY(i,j,mz)/rolvDY(i,j,1)
c #De    ect_DI(i,j)   = ect_TE(i,j,     1)
 
         eps_TE(i,j,1) = eps_TE(i,j,kp1(1))
c #De    eps_TE(i,j,1) = eps_TE(i,j,    mz)*rolvDY(i,j,mz)/rolvDY(i,j,1)
c #De    eps_DI(i,j)   = eps_TE(i,j,     1)
C +
c      END DO
       END DO
 
 
C +--TKE-e Vertical Moving Average
C +  =============================
 
       DO k=mz__KA,mz1
cc#KA  DO j=1,my
       DO i=1,mx
          WKxyz7(i,j,k)=(dsigm1(kp1(k))*ect_TE(i,j,kp1(k))
     .                  +dsigm1(    k )*ect_TE(i,j,    k )*2.
     .                  +dsigm1(km1(k))*ect_TE(i,j,km1(k))   )
     .                 /(dsigm1(kp1(k))
     .                  +dsigm1(    k )                   *2.
     .                  +dsigm1(km1(k))                      )
          WKxyz8(i,j,k)=(dsigm1(kp1(k))*eps_TE(i,j,kp1(k))
     .                  +dsigm1(    k )*eps_TE(i,j,    k )*2.
     .                  +dsigm1(km1(k))*eps_TE(i,j,km1(k))   )
     .                 /(dsigm1(kp1(k))
     .                  +dsigm1(    k )                   *2.
     .                  +dsigm1(km1(k))                      )
       END DO
cc#KA  END DO
       END DO
 
       DO k=mz__KA,mz1
cc#KA  DO j=1,my
       DO i=1,mx
          ect_TE(i,j,k)= WKxyz7(i,j,k)
          eps_TE(i,j,k)= WKxyz8(i,j,k)
       END DO
cc#KA  END DO
       END DO
 
 
C +--Verification: TKE must be Positive Definite
C +  ===========================================
 
       DO k=1,mz
c      DO j=1,my
       DO i=1,mx
         ect_TE(i,j,k)=max(epse,ect_TE(i,j,k))
         eps_TE(i,j,k)=max(epsd,eps_TE(i,j,k))
       END DO
c      END DO
       END DO
 
 
C +--Minimum Energy Dissipation Time
C +  ===============================
 
cc#HR  DO j=1,my
c #HR  DO i=1,mx
c #HR      edtnum      = 0.0
c #HR      edtden      = 0.0
c #HR    DO k=1,mz
c #HR      edtnum      = edtnum + ect_TE(i,j,k)*dsig_1(k)
c #HR      edtden      = edtden + eps_TE(i,j,k)*dsig_1(k)
c #HR    END DO
 
c #HR    IF (edtden.gt.0.0)                                       THEN
c #HR      edt_TE(i,j) = betahr*edtnum/edtden
c #HR    ELSE
c #HR      edt_TE(i,j) = 1.e-8
C +...     edt_TE set to an arbitrary small value
c #HR    END IF
c #HR  END DO
cc#HR  END DO
 
 
C +--Turbulent Diffusion Coefficients
C +  ================================
 
       IF (locTKE.gt.0 .or.itexpe.gt.1)                           THEN
 
 
C +--Richardson Number (contributors)
C +  -----------------
 
c        DO j=1,my
         DO i=1,mx
           TU_Pra(i,j,mz)  =   1.
         END DO
c        END DO
 
         DO k=kp1(1),mz1
c        DO j=1,my
         DO i=1,mx
           WKxyz6(i,j,k) =  0.0
c #RI      WKxyz6(i,j,k) =  0.5*(pktadY(i,j,k)-pktadY(i,j,k+1))*pcap
c #RI.                   /      (tairDY(i,j,k)+tairDY(i,j,k+1))
c #RI      WKxyz7(i,j,k) =  max((uairDY(i,j,k)-uairDY(i,j,k+1))**2
c #RI.                         +(vairDY(i,j,k)-vairDY(i,j,k+1))**2,epsi)
         END DO
cc#vK    END DO
         END DO
 
 
C +--Richardson Number
C +  -----------------
 
         DO k=kp1(1),mz1
cc#vK    DO j=1,my
         DO i=1,mx
           WKxyz8(i,j,k) =  0.0
c #RI      WKxyz8(i,j,k) = (gravit/WKxyz5(i,j,k))         ! g * dz     (k+1/2)
c #RI.                            *WKxyz6(i,j,k)          ! d(theta)/T (k+1/2)
c #RI.                            /WKxyz7(i,j,k)          ! d|V|
         END DO
cc#vK    END DO
         END DO
 
 
C +--Diffusion Coefficient for Heat
C +  ------------------------------
 
         DO k=kp1(1),mz
cc#vK    DO j=1,my
         DO i=1,mx
           TUkvh(i,j,k)=
     .            cmu * ect_TE(i,j,k)* ect_TE(i,j,k)/(eps_TE(i,j,k)
c #HR.                                +ect_TE(i,j,k)/ edt_TE(i,j)
     .                                                             )
C +...     nu_t =c_mu X ECT          X ECT          / eps
c #Kl      TUkvh(i,j,k)=  0.50 * sqrt( ect_TE(i,j,k))/WKxyz2(i,j,k)
 
           kz_max= vonkar *    (gplvDY(i,j,k+1)-gplvDY(i,j,mzz))*grvinv
           kz_mix= kz_max /    (1.             +kz_max          *0.1   )
           kz2mix= kz_mix *     kz_mix
           KvhMax(i,j,k) = max(   5000.        ,   100.
     .           * kz2mix *abs(( ssvSL(i,j,k)  - ssvSL(i,j,kp1(k)))
     .                         *WKxyz5(i,j,k)                      ))
           TUkvh(i,j,k)=   min( KvhMax(i,j,k)  , TUkvh(i,j,    k ))
           TUkvh(i,j,k)=   max( akmol          , TUkvh(i,j,    k ))
         END DO
cc#vK    END DO
         END DO
 
 
C +--Prandtl Number (Sukoriansky et al., 2005,
C +  --------------  BLM 117: 231-257, Eq.15, 19, 20 & Fig.2)
 
         DO k=kp1(1),mz1
cc#vK    DO j=1,my
         DO i=1,mx
c #RI      fac_Ri= 5.0 *     max(WKxyz8(i,j,k), epsi)
c #RI      vuzvun= 0.4 *(1.-(fac_Ri-1./fac_Ri)/(fac_Ri+1./fac_Ri)) + 0.2
c #RI      fac_Ri= 4.2 *     max(WKxyz8(i,j,k), epsi)
c #RI      Kz_vun= 0.7 *(1.-(fac_Ri-1./fac_Ri)/(fac_Ri+1./fac_Ri))
           TU_Pra(i,j,k) =                             1.
c #RI      TU_Pra(i,j,k) =   max(0.     ,sign(1.  , TUkvh(i,j,k)-0.20))
c #RI.                   -   min(0.     ,sign(1.  , TUkvh(i,j,k)-0.20))
c #RI.                   *   min(vuzvun / max(epsi,Kz_vun),     20.00)
         END DO
c        END DO
         END DO
 
 
C +--Diffusion Coefficient for Momentum
C +  ----------------------------------
 
         DO k=kp1(1),mz
c        DO j=1,my
         DO i=1,mx
           TUkvm(i,j,k)=                  TUkvh (i,j,k)
C +...     cfr Machiels, 1992, TFE (FSA/UCL) (3.21) p.21
 
c #Kl      TUkvm(i,j,k)=  0.7           * TUkvh (i,j,k)
 
c #RI      TUkvm(i,j,k)=  TU_Pra(i,j,k) * TUkvh (i,j,k)
         END DO
c        END DO
         END DO
 
       END IF
 
 
C +--Lateral Boundary Conditions
C +  ===========================
 
c #OB  IF (openLB)                                                THEN
c #OB    IF (mmx.gt.1)                                            THEN
c #OB      DO k=1,mz
c #OB      DO j=jp11,my1
c #OB        ect_TE( 1,j,k) = ect_TE(ip11,j,k)
c #OB        ect_TE(mx,j,k) = ect_TE(mx1,j,k)
c #OB        eps_TE( 1,j,k) = eps_TE(ip11,j,k)
c #OB        eps_TE(mx,j,k) = eps_TE(mx1,j,k)
c #OB         TUkvm( 1,j,k) =  TUkvm(ip11,j,k)
c #OB         TUkvm(mx,j,k) =  TUkvm(mx1,j,k)
c #OB         TUkvh( 1,j,k) =  TUkvh(ip11,j,k)
c #OB         TUkvh(mx,j,k) =  TUkvh(mx1,j,k)
c #OB      END DO
c #OB      END DO
c #OB    END IF
 
c #OB    IF (mmy.gt.1)                                            THEN
c #OB      DO k=1,mz
c #OB      DO i=ip11,mx1
c #OB        ect_TE(i, 1,k) = ect_TE(i,jp11,k)
c #OB        ect_TE(i,my,k) = ect_TE(i,my1,k)
c #OB        eps_TE(i, 1,k) = eps_TE(i,jp11,k)
c #OB        eps_TE(i,my,k) = eps_TE(i,my1,k)
c #OB         TUkvm(i, 1,k) =  TUkvm(i,jp11,k)
c #OB         TUkvm(i,my,k) =  TUkvm(i,my1,k)
c #OB         TUkvh(i, 1,k) =  TUkvh(i,jp11,k)
c #OB         TUkvh(i,my,k) =  TUkvh(i,my1,k)
c #OB      END DO
c #OB      END DO
c #OB    END IF
C +... Lateral Boundary Values of Kzm and Kzh are used
C +    during the Initialisation Procedure in 1-D Zone
 
c #OB  END IF
 
 
C +--Hourly Output on Listing
C +  ========================
 
c       IF((  itexpe.gt.0.and.jmmMAR.eq.0.and.jssMAR.eq.0.and.
c     .     ((IO_loc.ge.2.and.    jhurGE   .eq.0) .or.
c     .      (IO_loc.ge.2.and.mod(jhurGE,3).eq.0) .or.
c     .      (IO_loc.ge.3)                            )       ).or.
c     .       IO_loc.ge.7                                          ) THEN
c
c         DO i5=1,n5
c
c           DO k=1,mz
c             WKxza(i5,k) = 6.6d-1
c     .                   * (ect_TE(igrdIO(i5),jgrdIO(i5),k)**1.50)
c     .                    /(eps_TE(igrdIO(i5),jgrdIO(i5),k)
cC +...            0.066  = cmu(Duynkerke) * 2
cc #HR.                     +ect_TE(igrdIO(i5),jgrdIO(i5),k)
cc #HR.                     /edt_TE(igrdIO(i5),jgrdIO(i5))
c     .                                                )
c           END DO
c
c             write(4 ,61)explIO,igrdIO(i5),jgrdIO(i5),
c     .            jdarGE,jhlrGE(igrdIO(i5),jgrdIO(i5)),
c     .            jmmMAR,jssMAR,dt_dif,
c     .                    xxkm(igrdIO(i5)),
c     .                   SLlmo(igrdIO(i5),jgrdIO(i5)),
c     .                  zi__TE(igrdIO(i5),jgrdIO(i5))
cc #WE      IF (mod(jhaMAR,12).eq.0)
cc #WE.       write(25,61)explIO,igrdIO(i5),jgrdIO(i5),
cc #WE.            jdarGE,jhlrGE(igrdIO(i5),jgrdIO(i5)),
cc #WE.            jmmMAR,jssMAR,dt_dif,
cc #WE.                    xxkm(igrdIO(i5)),
cc #WE.                   SLlmo(igrdIO(i5),jgrdIO(i5)),
cc #WE.                  zi__TE(igrdIO(i5),jgrdIO(i5))
c 61          format(/,' EXP.',a3,' / Pt. (',i3,',',i3,')',
c     .                      ' / Turbulence Characteristics on',
c     .           i5,'d',i2,'LT',i2,'m',i2,'s - dt=',f6.0,
c     .         /,' ------------------------------------------',
c     .           '-----------------------------------------',
c     .       /,' x =',f5.0,'km',3x,'Lo =',e9.2,'m' ,3x,'zi =',f6.0,'m',
c     .       /,' lev  Altit. Temper.  Pot.T. Wind_u Wind_v   TKE   ',
c     .         ' epsilon Buoyancy     Le    Lk   Le(e) Prandtl     Kvm',
c     .                                               '     Kvh Kvh MAX',
c     .       /,'       [m]     [K]      [K]   [m/s]  [m/s] [m2/s2] ',
c     .         ' [m2/s2]    [s-2]    [m]   [m]  [m]        [-]  [m2/s]',
c     .                                               '  [m2/s]  [m2/s]')
c             write(4 ,62)(k,
c     .              0.1019*gplvDY(igrdIO(i5),jgrdIO(i5),k),
c     .                     tairDY(igrdIO(i5),jgrdIO(i5),k),
c     .              3.7300*pktaDY(igrdIO(i5),jgrdIO(i5),k),
c     .                     uairDY(igrdIO(i5),jgrdIO(i5),k),
c     .                     vairDY(igrdIO(i5),jgrdIO(i5),k),
c     .              0.1019*gpmiDY(igrdIO(i5),jgrdIO(i5),k+1),
c     .                     ect_TE(igrdIO(i5),jgrdIO(i5),k),
c     .                     eps_TE(igrdIO(i5),jgrdIO(i5),k),
c     .              flott(i5,k) ,aalme(i5,k) ,aalmk(i5,k),WKxza(i5,k),
c     .                       TU_Pra(igrdIO(i5),jgrdIO(i5),k),
c     .                        TUkvm(igrdIO(i5),jgrdIO(i5),k),
c     .                        TUkvh(igrdIO(i5),jgrdIO(i5),k),
c     .                       KvhMax(igrdIO(i5),jgrdIO(i5),k),
c     .               k=1,mz)
c             write(4 ,620)
c 620         format(
c     .         ' lev  Altit. Temper.  Pot.T. Wind_u Wind_v   TKE   ',
c     .         ' epsilon Buoyancy     Le    Lk   Le(e) Prandtl     Kvm',
c     .                                               '     Kvh Kvh MAX',
c     .       /,'       [m]     [K]      [K]   [m/s]  [m/s] [m2/s2] ',
c     .         ' [m2/s2]    [s-2]    [m]   [m]  [m]        [-]  [m2/s]',
c     .                                               '  [m2/s]  [m2/s]',
c     .       /,1x)
cc #WE      IF (mod(jhaMAR,12).eq.0)                               THEN
cc #WE        m30=min(mz,30)
cc #WE        write(25,62)(k,
cc #WE.              0.1019*gplvDY(igrdIO(i5),jgrdIO(i5),k),
cc #WE.                     tairDY(igrdIO(i5),jgrdIO(i5),k),
cc #WE.              3.7300*pktaDY(igrdIO(i5),jgrdIO(i5),k),
cc #WE.                     uairDY(igrdIO(i5),jgrdIO(i5),k),
cc #WE.                     vairDY(igrdIO(i5),jgrdIO(i5),k),
cc #WE.              0.1019*gpmiDY(igrdIO(i5),jgrdIO(i5),k+1),
cc #WE.                     ect_TE(igrdIO(i5),jgrdIO(i5),k),
cc #WE.                     eps_TE(igrdIO(i5),jgrdIO(i5),k),
cc #WE.              flott(i5,k) ,aalme(i5,k) ,aalmk(i5,k),WKxza(i5,k),
cc #WE.                     TU_Pra(igrdIO(i5),jgrdIO(i5),k),
cc #WE.                      TUkvm(igrdIO(i5),jgrdIO(i5),k),
cc #WE.                      TUkvh(igrdIO(i5),jgrdIO(i5),k),
cc #WE.                     KvhMax(igrdIO(i5),jgrdIO(i5),k),
cc #WE.               k=1,m30)
cc #WE        write(25,64)
c 64          format(/////)
cc #WE        write(25,61)explIO,jhlrGE(igrdIO(i5),jgrdIO(i5)),
cc #WE.                          minuGE,jsecGE,dt_dif,
cc #WE.                    xxkm(igrdIO(i5)),
cc #WE.                   SLlmo(igrdIO(i5),jgrdIO(i5)),
cc #WE.                  zi__TE(igrdIO(i5),jgrdIO(i5))
cc #WE        m31=min(mz,31)
cc #WE        if (mz.ge.m31)
cc #WE.       write(25,62)(k,
cc #WE.              0.1019*gplvDY(igrdIO(i5),jgrdIO(i5),k),
cc #WE.                     tairDY(igrdIO(i5),jgrdIO(i5),k),
cc #WE.              3.7300*pktaDY(igrdIO(i5),jgrdIO(i5),k),
cc #WE.                     uairDY(igrdIO(i5),jgrdIO(i5),k),
cc #WE.                     vairDY(igrdIO(i5),jgrdIO(i5),k),
cc #WE.              0.1019*gpmiDY(igrdIO(i5),jgrdIO(i5),k+1),
cc #WE.                     ect_TE(igrdIO(i5),jgrdIO(i5),k),
cc #WE.                     eps_TE(igrdIO(i5),jgrdIO(i5),k),
cc #WE.              flott(i5,k) ,aalme(i5,k) ,aalmk(i5,k),WKxza(i5,k),
cc #WE.                     TU_Pra(igrdIO(i5),jgrdIO(i5),k),
cc #WE.                      TUkvm(igrdIO(i5),jgrdIO(i5),k),
cc #WE.                      TUkvh(igrdIO(i5),jgrdIO(i5),k),
cc #WE.                     KvhMax(igrdIO(i5),jgrdIO(i5),k),
cc #WE.               k=m31,mz)
cc #WE      END IF
c 62        format((i4,f8.0,2f8.2,2f7.2,
c     .           /,4x,f8.0,30x,3e9.2,2f6.1,5f8.2))
c           write(4,63) SL_z0(igrdIO(i5),jgrdIO(i5),1),
c     .                TairSL(igrdIO(i5),jgrdIO(i5))  ,
c     .                 SLuus(igrdIO(i5),jgrdIO(i5))
c 63        format( 4x,f8.6, f8.2,8x,f8.3)
c
c           DO k=1,mz
c             WKxza(i5,k) = 0.0
c           END DO
c
c         END DO
c       END IF
 
 
C +--Work Arrays Reset
C +  =================
 
c     DO  j=1,my
      DO  i=1,mx
        WKxy1(i,j) = 0.0
        WKxy2(i,j) = 0.0
        WKxy3(i,j) = 0.0
        WKxy4(i,j) = 0.0
        WKxy5(i,j) = 0.0
      END DO
c     END DO
 
      DO k=1,mz
c     DO j=1,my
      DO i=1,mx
        WKxyz1(i,j,k) = 0.0
        WKxyz2(i,j,k) = 0.0
        WKxyz3(i,j,k) = 0.0
        WKxyz4(i,j,k) = 0.0
        WKxyz5(i,j,k) = 0.0
        WKxyz6(i,j,k) = 0.0
        WKxyz7(i,j,k) = 0.0
        WKxyz8(i,j,k) = 0.0
        tranTE(i,j,k) = 0.0
      END DO
      END DO
      END DO
!$OMP END PARALLEL DO
 
      locTKE = 1
C +...locTKE: turn on TKE Evolution
 
      return
      end
