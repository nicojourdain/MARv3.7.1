 
 
      subroutine DYNwww
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS   DIAGNOSTICS                             16-07-2004  MAR |
C |   SubRoutine DYNwww computes Vertical Wind Speed wairDY (z Coordinate) |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT  (via common block)                                            |
C |   ^^^^^   psigDY(i,j,k): Vertical Wind Speed (Sigma Coordinate system) |
C |                          on Level k                                    |
C |           uairDY(i,j,k): k Sigma Level Wind (x-direction)        [m/s] |
C |           vairDY(i,j,k): k Sigma Level Wind (y-direction)        [m/s] |
C |           gplvDY(i,j,k): k Sigma Level Geopotential            [m2/s2] |
C |           tairDY(i,j,k): k Sigma Level Temperature                 [K] |
C |                                                                        |
C |   OUTPUT (via common block)                                            |
C |   ^^^^^^  wairDY(i,j,k)= -(R T /p) g dSigma /dt + (u dz /dx +v dz /dy) |
C |                                                                 [cm/s] |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_DY.inc'
 
      include 'MAR_WK.inc'
 
 
C +--local   Variables
C +  =================
 
      include 'MARvec.inc'
 
      DO  k=1,mz
 
 
C +--u d(phi)/d(x)
C +  =============
 
        DO i=1,mx
        DO j=1,my
              WKxy1(i,j)  =
     .        fac43 *(gplvDY(ip1(i),j,k)-gplvDY(im1(i),j,k)
     .       -0.125 *(gplvDY(im2(i),j,k)-gplvDY(im2(i),j,k)))*dyinv2
        END DO
        END DO
 
        DO i=1,mx
        DO j=1,my
             WKxyz1(i,j,k)=  WKxy1(i,j) *uairDY(i,j,k)
        END DO
        END DO
 
 
C +--v d(phi)/d(y)
C +  =============
 
        IF (mmy.gt.1)                                             THEN
          DO j=1,my
          DO i=1,mx
              WKxy1(i,j)  =
     .        fac43 *(gplvDY(i,jp1(j),k)-gplvDY(i,jm1(j),k)
     .       -0.125 *(gplvDY(i,jp2(j),k)-gplvDY(i,jm2(j),k)))*dyinv2
          END DO
          END DO
 
          DO i=1,mx
          DO j=1,my
             WKxyz1(i,j,k)= WKxyz1(i,j,k)
     .                    +  WKxy1(i,j) *vairDY(i,j,k)
          END DO
          END DO
        END IF
 
      END DO
 
 
C +--Vertical Wind Speed (cm/s)
C +  ==========================
 
      DO i=1,mx
      DO j=1,my
      DO k=1,mz
        wairDY(i,j,k)=-RDryAi*tairDY(i,j,k)*psigDY(i,j,k)*grvinv
     .               / (pstDY(i,j)*sigma(k)+ptopDY)             *100.
     .               + WKxyz1(i,j,k)                     *grvinv*100.
      END DO
      END DO
      END DO
 
      DO j=1,my
      DO i=1,mx
      DO k=1,mz
         WKxy1(i,j)  = 0.0
        WKxyz1(i,j,k)= 0.0
      END DO
      END DO
      END DO
 
      return
      end
