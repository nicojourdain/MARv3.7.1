 
 
      subroutine ADVbot_4(flu,vec,aa0,aa1,aa2,aa3,aa4,
     .                    cnp,cnm,sip,sim,sid,mmm,logpos)
C +
C +------------------------------------------------------------------------+
C | MAR ADVECTION                                          16-12-2000  MAR |
C |   SubRoutine ADVbot_4   includes the            Advection Contribution |
C |                              for positive definite scalar Variables    |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:   vec                : variable to advect                     |
C |   ^^^^^    aa0,aa1,aa2,aa3,aa4: Work Area                              |
C |            cnp                : Courant Number (x+ Cell)               |
C |            cnm                : Courant Number (x- Cell)               |
C |            sip,sim,sid        : Work Area                              |
C |            mmm                : Dimension of the Variables             |
C |            logpos             : Positive definite Constraint Switch    |
C |                                                                        |
C |   OUTPUT:  flu   : advection fluxes at each cell boundaries            |
C |   ^^^^^^                                                               |
C |                                                                        |
C |   METHOD : The Flux Form of the Equation is solved:                    |
C |   ^^^^^^^^ d(Qp*)/dt + d(uQp*)/dx + d(vQp*)/dy + d(sQp*)/ds = 0        |
C |            The scheme is that of Bott (1989) MWR, 117, 1007-1009       |
C |                              and Bott (1989) MWR, 117, 2635 (Table 1)  |
C |                                                                        |
C | # OPTIONS: STANDARD: Bott Scheme, order 0 (zero)                       |
C | # ^^^^^^^^ #B+ #B2   Bott Scheme, order 2                              |
C | #          #B+ #B4   Bott Scheme, order 4                              |
C |                                                                        |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +--Global Variables
C +  ================
C +
      include 'MARphy.inc'
C +
      integer   mmm,logpos
C +
      real      flu(0:mmm)
      real      vec(0:mmm)
      real      aa0(0:mmm)
      real      aa1(0:mmm)
      real      aa2(0:mmm)
      real      aa3(0:mmm)
      real      aa4(0:mmm)
      real      cnp(0:mmm)
      real      cnm(0:mmm)
      real      sip(0:mmm)
      real      sim(0:mmm)
      real      sid(0:mmm)
C +
C +
C +--Local  Variables
C +  ================
C +
      integer   i     ,im1   ,ip1   ,im2   ,ip2
      real      epsadv,propo1,propo2,propo3,propo4,propo5
      real             prone1,prone2,prone3,prone4,prone5
C +
C +
C +--DATA
C +  ====
C +
      data epsadv/1.e-6/
C +
      DO i=0,mmm
         im1=max(i-1,0)
         ip1=min(i+1,mmm)
         im2=max(i-2,0)
         ip2=min(i+2,mmm)
C +
C +
C +--Approximation of vec
C +  --------------------
C +
C +--Polynomial Fitting, as in Bott (1989) MWR, 117, 2635 (Table 1)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        aa0(i) =
c #B2.          (26.0 *
     .                  vec(i  )
c #B2.                 -vec(ip1)-vec(im1))/24.0
c #B2   aa1(i) = 0.50 *(vec(ip1)-vec(im1))
c #B2   aa2(i) = 0.50 *(vec(ip1)+vec(im1)) - vec(i)
C +...  aa0, aa1, aa2 : Lagrange Polynomial Coefficients
C +                    (see Bott 1989 MWR Table 1 p.2635)
C +
        aa0(i)=( 9.0*vec(ip2) -116.0*vec(ip1) + 2134.0*vec(i)
     .        -116.0*vec(im1) +  9.0*vec(im2))                /1920.0
        aa1(i)=(-5.0*vec(ip2) + 34.0*vec(ip1)
     .         -34.0*vec(im1) +  5.0*vec(im2))                  /48.0
        aa2(i)=(-3.0*vec(ip2) + 36.0*vec(ip1) -   66.0*vec(i)
     .         +36.0*vec(im1) -  3.0*vec(im2))                  /48.0
        aa3(i)=(     vec(ip2) -  2.0*vec(ip1)
     .          +2.0*vec(im1) -      vec(im2))                  /12.0
        aa4(i)=(     vec(ip2) -  4.0*vec(ip1) +    6.0*vec(i)
     .          -4.0*vec(im1) +      vec(im2))                  /24.0
      END DO
C +
C +--Integral (7), Bott 1989 MWR 117 p. 1007
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      DO i=0,mmm
        propo1 = 1.0-2.0*cnp(i)
        propo2 = propo1 *propo1
        propo3 = propo2 *propo1
        propo4 = propo3 *propo1
        propo5 = propo4 *propo1
        sip(i) = aa0(i  )        *     cnp(i)
     .        + (aa1(i  )/  8.0)*(1.0-propo2)
     .        + (aa2(i  )/ 24.0)*(1.0-propo3)
     .        + (aa3(i  )/ 64.0)*(1.0-propo4)
     .        + (aa4(i  )/160.0)*(1.0-propo5)
      END DO
C +
C +--Integral (8), Bott 1989 MWR 117 p. 1008
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      DO i=0,mmm
         ip1=min(i+1,mmm)
        prone1 = 1.00-2.0*cnm(i)
        prone2 = prone1 *prone1
        prone3 = prone2 *prone1
        prone4 = prone3 *prone1
        prone5 = prone4 *prone1
        sim(i) = aa0(ip1)        *     cnm(i)
     .        - (aa1(ip1)/  8.0)*(1.0-prone2)
     .        + (aa2(ip1)/ 24.0)*(1.0-prone3)
     .        - (aa3(ip1)/ 64.0)*(1.0-prone4)
     .        + (aa4(ip1)/160.0)*(1.0-prone5)
      END DO
C +
C +
C +--Positive Definite Constraint, Bott 1989, MWR 117 (14) p.1008
C +  ------------------------------------------------------------
C +
      IF (logpos.eq.1)                                            THEN
C +
C +--Positive Definite Constraint is     applied
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        DO i=0,mmm
          sip(i) = max(sip(i),zero)
          sim(i) = max(sim(i),zero)
          sid(i) = aa0(i) + aa2(i) / 12.0
     .                    + aa4(i) / 80.0
        END DO
C +
        DO i=0,mmm
           im1=max(i-1,0)
          sid(i) = max(sid(i),sip(i)+sim(im1)+epsadv)
        END DO
C +
        DO i=0,mmm
           ip1=min(i+1,mmm)
          flu(i) = sip(i)*vec(i)/sid(i)  -  sim(i)*vec(ip1)/sid(ip1)
        END DO
C +
      ELSE
C +
C +--Positive Definite Constraint is not applied
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        DO i=0,mmm
          flu(i) = sip(i)                -  sim(i)
        END DO
C +
      END IF
C +
      return
      end
