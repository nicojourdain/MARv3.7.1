 
 
      block data HYDdat
 
C +------------------------------------------------------------------------+
C | MAR HYDROLOGIC CYCLE                                   19-10-2006  MAR |
C |   Block Data HYDdat is used to define microphysical constants of   MAR |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARdim.inc'
 
      include 'MAR_HY.inc'
 
      data cminHY/1.0e-3/
C +...     cminHY:Cloud Fraction under which no Autoconversion occurs
 
      end
