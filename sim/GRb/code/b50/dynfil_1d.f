      subroutine DYNfil_1D(f1_fil,e1_fil,k1_fil)
C +
C +------------------------------------------------------------------------+
C | MAR DYNAMICS FILTER (1-D)                              24-09-2001  MAR |
C |   SubRoutine DYNfil_1D  is used to Filter Horizontal Fields in 2D Code |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:  f1_fil   : filtered variable                                 |
C |   ^^^^^   e1_fil   : value of the selectivity parameter                |
C |           k1_fil   : vertical dimension of the problem                 |
C |                                                                        |
C |   OUTPUT:(via common block)                                            |
C |   ^^^^^^  f1_fil                                                       |
C |                                                                        |
C |   LATERAL BOUNDARIES:                                                  |
C |   ^^^^^^^^^^^^^^^^^^^                                                  |
C |   1. The value    of the variable is fixed at the Boundary             |
C |   2. Filter Selectivity Parameter may be increased near the Boundary   |
C |                                                                (#EP)   |
C |                                                                        |
C |   REFER. : Alpert (1981), J.Comput.Phys. 44, pp.212--219               |
C |   ^^^^^^^^                                                             |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +--Global Variables
C +  ================
C +
      include 'MARphy.inc'
      include 'MARdim.inc'
C +
      real     f1_fil(mx,my,mz),e1_fil(mz)
      integer  k1_fil
C +
C +
C +--Local  Variables
C +  ================
C +
      include 'MARvec.inc'
C +
      integer  kindex
      integer  m , i
      integer  m1,m2
c #EP integer  m3,m4
      real     aa,bb
c #EP real     delt2,delt3,delt4
C +
C +
C +--Initialisation of the Gaussian Elimination Algorithm
C +  ====================================================
C +
        m  = mx
        m1 = m-1
        m2 = m-2
c #EP   m3 = m-3
c #EP   m4 = m-4
C +
      DO kindex=1,k1_fil
C +
        vecx1(1) = 0.0
        vecx2(1) = 1.0
        vecx1(m) = 0.0
        vecx2(m) = 1.0
        vecx4(1) = f1_fil(1,1,kindex)
        vecx4(m) = f1_fil(m,1,kindex)
C +
        delta =    e1_fil(    kindex)
C +
c #EP   delt4 =        delta+delta
c #EP   delt3 =        delt4+delt4
c #EP   delt2 =        delt3+delt3
C +..   Filter Selectivity Parameter Increase near the Boundaries
C +
        aa    =     1.0-delta
        bb    =2.0*(1.0+delta)
C +
        DO i=2,m1
          vecx1(i)  = aa
          vecx2(i)  = bb
          vecx4(i)  = f1_fil(i-1,1,kindex)+2.0*f1_fil(i,1,kindex)
     .               +f1_fil(i+1,1,kindex)
        END DO
C +
c #EP     vecx1(2)  =      1.0-delt2
c #EP     vecx1(3)  =      1.0-delt3
c #EP     vecx1(4)  =      1.0-delt4
c #EP     vecx2(2)  = 2.0*(1.0+delt2)
c #EP     vecx2(3)  = 2.0*(1.0+delt3)
c #EP     vecx2(4)  = 2.0*(1.0+delt4)
C +
c #EP     vecx1(m2) = vecx1(2)
c #EP     vecx1(m3) = vecx1(3)
c #EP     vecx1(m4) = vecx1(4)
c #EP     vecx2(m2) = vecx2(2)
c #EP     vecx2(m3) = vecx2(3)
c #EP     vecx2(m4) = vecx2(4)
C +
        DO i=1,m
          vecxa(i)  = f1_fil(i,1,kindex)
          vecx5(i)  = 0.
          vecx6(i)  = 0.
        END DO
C +
C +
C +--Gaussian Elimination Algorithm
C +  ==============================
C +
C +     *********
        call tlat(vecx1,vecx2,vecx1,vecx4,vecx5,vecx6,m ,m ,vecxa)
C +     *********
C +
        DO i=1,m
          f1_fil(i,1,kindex)= vecxa(i)
        END DO
C +
      END DO
C +
      return
      end
