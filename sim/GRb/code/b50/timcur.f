 
 
      subroutine TIMcur
 
C +------------------------------------------------------------------------+
C | MAR TIME                                               18-09-2001  MAR |
C |   SubRoutine TIMcur computes MAR               Time                    |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |    INPUT (via common block)                                            |
C |    ^^^^^  itexpe: Experiment Iteration Counter                         |
C |                                                                        |
C |   OUTPUT (via common block)                                            |
C |   ^^^^^^  jdaMAR: Nb of Days    Since Run Beginning (i.e. itexpe = 0)  |
C |           jhaMAR: Nb of Hours                                          |
C |           jmmMAR: Nb of Minutes                                        |
C |           jssMAR: Nb of Seconds                                        |
C |           jhaRUN: Nb of Hours   Since Run Beginning                    |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
 
C +--MAR Time
C +  ========
 
      jssMAR  =    itexpe*idt
      jmmMAR  =    jssMAR/60
      jhaRUN  =    jmmMAR/60
      jdaMAR  =    jhaRUN/24
 
      jhaMAR  =    jhaRUN -   jdaMAR *24
      jmmMAR  =    jmmMAR -  (jdaMAR *24 + jhaMAR) *60
      jssMAR  =    jssMAR - ((jdaMAR *24 + jhaMAR) *60 + jmmMAR) *60
 
      return
      end
