 
 
      block data SISVAT_CdP
 
C +------------------------------------------------------------------------+
C | MAR          SISVAT_CdP                                14-02-2006  MAR |
C |   SubRoutine SISVAT_CdP contains specific (Col de Porte) constants of  |
C |              Soil/Ice Snow Vegetation Atmosphere Transfer Scheme       |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
      include "MAR_SV.inc"
 
 
C +..BEGIN    "MARdCP.inc"
C +--Col de Porte specific Constants
C +  ===============================
 
      logical         ColPrt                ! Col de Porte Switch
      common/ColPrt_l/ColPrt
 
 
C +--Fractions of total solar irradiances in 3 spectral intervals
C +  ------------------------------------------------------------
 
      real            Dr_1SN,Dr_2SN,Dr_3SN  ! Direct  Radiation
      real            Df_1SN,Df_2SN,Df_3SN  ! Diffuse Radiation, Clear  Sky
      real            Dfc1SN,Dfc2SN,Dfc3SN  ! Diffuse Radiation, Cloudy Sky
      common/ColPrt_r/Dr_1SN,Dr_2SN,Dr_3SN
     .               ,Df_1SN,Df_2SN,Df_3SN
     .               ,Dfc1SN,Dfc2SN,Dfc3SN
 
      real            DirSol,DifSol,TotSol,Clouds
      common/ColPrt_v/DirSol,DifSol,TotSol,Clouds
 
C +..END      "MARdCP.inc"
 
 
C +--DATA
C +  ====
 
c #CP data     ColPrt /.true./
 
      data     Dr_1SN/0.59    /,Dr_2SN/0.31    /,Dr_3SN/0.10    /
      data     Df_1SN/0.95    /,Df_2SN/0.05    /,Df_3SN/0.00    /
      data     Dfc1SN/0.66    /,Dfc2SN/0.27    /,Dfc3SN/0.07    /
C +...         0.3--0.8micr.m   0.8--1.5micr.m   1.5--2.8micr.m
C +            Fractions of total solar irradiance in 3 spectral intervals
C +***        (see Eric Martin Sept. 1996, CROCUS, Subroutine METEO)
 
 
      end
