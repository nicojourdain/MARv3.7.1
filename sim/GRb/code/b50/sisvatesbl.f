 
 
      subroutine SISVATeSBL
 
!--------------------------------------------------------------------------+
!   MAR          SISVATeSBL                           Tue 12-Apr-2011  MAR |
!     SubRoutine SISVATeSBL generates Surface Boundary Layers Properties   |
!--------------------------------------------------------------------------+
!                                                                          |
!     PARAMETERS:  klonv: Total Number of columns                          |
!     ^^^^^^^^^^        = Total Number of continental     grid boxes       |
!                       X       Number of Mosaic Cell per grid box         |
!                                                                          |
!     INPUT:   za__SV   : Surface Boundary Layer (SBL) Height          [m] |
!     ^^^^^    VV__SV   :(SBL Top)   Wind Velocity                   [m/s] |
!              TaT_SV   : SBL Top    Temperature                       [K] |
!              ExnrSV   : Exner      Potential                         [-] |
!              qsnoSV   : SBL Mean   Snow      Content             [kg/kg] |
!              uqs_SV   : Specific   Humidity  Turbulent Flux        [m/s] |
!              usthSV   : Blowing Snow Erosion   Threshold           [m/s] |
!              Z0m_SV   : Momentum     Roughness Length                [m] |
!              Z0h_SV   : Heat         Roughness Length                [m] |
!              Tsrfsv   : Surface    Temperature                       [K] |
!              sqrCm0   : Contribution of Z0m to Neutral Drag Coefficient  |
!              sqrCh0   : Contribution of Z0h to Neutral Drag Coefficient  |
!                                                                          |
!     INPUT /  LMO_SV   : Monin-Obukhov       Scale                    [m] |
!     OUTPUT:  us__SV   : Friction  Velocity                         [m/s] |
!     ^^^^^^   uts_SV   : Temperature         Turbulent Flux       [K.m/s] |
!              uss_SV   : Blowing Snow        Turbulent Flux         [m/s] |
!                                                                          |
!     OUTPUT:  hSalSV   : Saltating Layer Height                       [m] |
!     ^^^^^^   qSalSV   : Saltating Snow  Concentration            [kg/kg] |
!              ram_sv   : Aerodynamic Resistance for Momentum        [s/m] |
!              rah_sv   : Aerodynamic Resistance for Heat            [s/m] |
!                                                                          |
!   # OPTIONS: #BS: Blowing Snow turbulent Fluxes are computed             |
!   # ^^^^^^^  #ss: Additional Output                                      |
!                                                                          |
!--------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
!  Global Variables
!  ================
 
      include  "MARphy.inc"
      include  "MAR_SV.inc"
      include  "MARdSV.inc"
 
      include  "MARxSV.inc"
      include  "MARySV.inc"
 
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! #wx integer                iSV_v1,jSV_v1,nSV_v1,kSV_v1,lSV_v1
! #wx common  /SISVAT_EV/    iSV_v1,jSV_v1,nSV_v1,kSV_v1,lSV_v1
 
!  V,  dT(a-s)    Time Moving Averages
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      integer                       ntaver,nt !
      parameter                    (ntaver= 4)! ntaver defined in MAR_SL.inc
      real             V__mem(klonv,ntaver)   !                   only
      real             VVmmem(klonv)          !
      common/SVeSBLmem/V__mem,VVmmem          !
      real             T__mem(klonv,ntaver)   !
      real             dTmmem(klonv)          !
      common/STeSBLmem/T__mem,dTmmem          !
 
!$OMP threadprivate(/xSISVAT_I/,/xSISVAT_R/,/ySISVAT_I/,/ySISVAT_R/,
!$OMP. /SVeSBLmem/,/STeSBLmem/)
 
!  u*, u*T*, u*s* Time Moving Averages
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #AM real             u__mem(klonv,ntaver)
c #AT real             uT_mem(klonv,ntaver)
c #AS real             us_mem(klonv,ntaver)
c #AM common/S_eSBLmem/u__mem
c #AT.                ,uT_mem
c #AS.                ,us_mem
 
 
!  Internal Variables
!  ==================
 
      integer   ikl   ,icount
c #AE integer   nit   ,iit
      real      VVaSBL(klonv),VVa_OK          ! effective SBL wind speed
      real      dTa_Ts(klonv)                 ! effective SBL Temperature diff.
      real      Theta0                        ! Potential Reference Temperature
      real      LMOmom(klonv)                 ! Monin-Obukhov Scale Momentum
      real      LMOsgn                        ! Monin-Obukhov Scale Sign
      real      LMOabs                        ! Monin-Obukhov Scale Abs.Value
      real      uustar,thstar,qqstar,ssstar,thstarv,thstars,thstara
      real      zetam ,zetah ,zeta_S,zeta_A,zeta0m ,zeta0h
      real      psim_s,xpsimi,psim_i,psim_z
      real      psis_s,psis_z,psis_0
      real      psih_s,xpsihi,psih_i,psih_z
      real      psim_0,psih_0
      real      CDm(klonv)                    ! Drag Coefficient, Momentum
      real      CDs(klonv),rCDs(klonv)        ! Drag Coefficient, Blown **
      real      CDh(klonv)                    ! Drag Coefficient, Scalar
      real      dustar,u0star,uTstar,usstar
      real      sss__F,sss__N,usuth0
c #AE real      dusuth,signus
c #AE real      sss__K,sss__G
c #AE real      us_127,us_227,us_327,us_427,us_527
      real      zetMAX
      real      coef_m,coef_h,stab_s
c #AE real      SblPom
      real      Richar(klonv)                 ! Richardson Number
c #wr real      W_pLMO                        ! Pseudo Obukhov Length  (WRITE)
c #wr real      W_psim                        ! Pseudo psim(z)         (WRITE)
c #w1 real      W_NUs1                        ! Contrib to U* numerat.1(WRITE)
c #w1 real      W_NUs2                        ! Contrib to U* numerat.2(WRITE)
c #w1 real      W_NUs3                        ! Contrib to U* numerat.3(WRITE)
c #w1 real      W_DUs1                        ! Contrib to U* denomin.1(WRITE)
c #w1 real      W_DUs2                        ! Contrib to U* denomin.2(WRITE)
      real      fac_Ri,vuzvun,Kz_vun
 
 
!  Internal DATA
!  =============
 
      data      Theta0/288.0/   ! Potential Reference Temperature
c #ZX data      zetMAX/ 1.e6/   ! Strong Stability Limit
c #zx data      zetMAX/ 1.e0/   ! Strong Stability Limit
C +                             !(Mahalov et al. 2004, GRL  31 2004GL021055)
      data      zetMAX/ 4.28/   ! Strong Stability Limit
C +                             !(King    et al. 1996, JGR 101(7) p.19121)
      data      coef_m/20.  /   ! Stabil.Funct.for Moment.: unstab.coef.
      data      coef_h/15.  /   ! Stabil.Funct.for Heat:    unstab.coef.
c #AE data      SblPom/ 1.27/   ! Lower Boundary Height Parameter
C +                             ! for Suspension
C +                             ! Pommeroy, Gray and Landine 1993,
C +                             ! J. Hydrology, 144(8) p.169
c #AE data      nit   / 5   /   ! us(is0,uth) recursivity: Nb Iterations
 
 
!  Effective SBL variables
!  =======================
 
      DO ikl=1,klonv
        VVaSBL(ikl)   = VV__SV(ikl)
        VVaSBL(ikl)   = VVmmem(ikl)
        dTa_Ts(ikl)   = TaT_SV(ikl)-Tsrfsv(ikl)
        dTa_Ts(ikl)   = dTmmem(ikl)
      ENDDO
 
 
!  Convergence Criterion
!  =====================
 
      icount = 0
 
 1    CONTINUE
      icount = icount + 1
      dustar = 0.
 
        DO ikl=1,klonv
 
          u0star      = us__SV(ikl)
 
!  u*, u*T*, u*s* Time Moving Averages
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #AM     u0star      = 0.0
c #AT     uTstar      = 0.0
c #AS     usstar      = 0.0
c #AM     DO nt=1,ntaver
c #AM     u0star      = u0star + u__mem(ikl,nt)
c #AT     uTstar      = uTstar + uT_mem(ikl,nt)
c #AS     usstar      = usstar + us_mem(ikl,nt)
c #AM     ENDDO
c #AM     u0star      = u0star / ntaver
c #AM     us__SV(ikl) = u0star
c #AT     uts_SV(ikl) = uTstar / ntaver
c #AS     uss_SV(ikl) = usstar / ntaver
 
 
!  Turbulent Scales from previous Time Step
!  ----------------------------------------
 
          u0star      =      max(epsi,u0star)      ! Friction Velocity     u*
          uustar      = u0star      * u0star       ! Friction Velocity^2  uu*
          thstar      = uts_SV(ikl) / u0star       ! Temperature       theta*
          qqstar      = uqs_SV(ikl) / u0star       ! Specific Humidity    qq*
          ssstar      = uss_SV(ikl) / u0star       ! Blown    Snow        ss*
 
 
!  Monin-Obukhov Stability Parameter for Momentum
!  ----------------------------------------------
 
!  Pseudo Virtual Temperature Turbulent Scale thetav*
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          thstarv     = thstar      + Theta0      *(0.608*qqstar)
     .                             /(1.+0.608*QaT_SV(ikl)-qsnoSV(ikl))
          thstars     =     sign(unun,thstarv)
          thstara     =      abs(     thstarv)
          thstarv     =      max(epsi,thstara)*thstars
 
!  Pseudo Obukhov Length Scale        (Gall?e et al., 2001 BLM 99, (A2) p.17)
!  Full   Obukhov Length Scale        (when Blowing * is ##NOT## switched ON)
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          LMO_SV(ikl) = Theta0      * max(epsi,uustar)
     .                /(vonkar      * gravit  *thstarv)
c #wr     W_pLMO      = LMO_SV(ikl)
 
          zetah       = za__SV(ikl) / LMO_SV(ikl)
          zetam       =           min(zetMAX,zetah)! Strong Stability Limit
                                                   !(Mahalov et al. 2004
                                                   ! GRL 31 2004GL021055)
          LMOmom(ikl) = za__SV(ikl) /(max(epsi,abs(zetam))
     .                              *sign(unun,    zetam ))
          zeta0m      = Z0m_SV(ikl) / LMOmom(ikl)
          zeta0h      = Z0h_SV(ikl) / LMO_SV(ikl)
 
!  Momentum Pseudo Stability Function (Gall?e et al. 2001, BLM 99, (11) p. 7)
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          stab_s      =  max(zero,sign(unun,zetam))
 
          psim_s      =  -A_Turb *zetam
          xpsimi      = sqrt(sqrt(unun-coef_m*min(zero,zetam)))
          psim_i      =   2. *log(demi*(unun+xpsimi))
     .                       +log(demi*(unun+xpsimi*xpsimi))
     .                   -2.*atan(xpsimi)   +demi*pi
          psim_z      =    stab_s*psim_s+(1.-stab_s)*psim_i
c #wr     W_psim      =           psim_z
 
          psim_s      =  -A_Turb *zeta0m
          xpsimi      = sqrt(sqrt(unun-coef_m*min(zero,zeta0m)))
          psim_i      =   2. *log(demi*(unun+xpsimi))
     .                       +log(demi*(unun+xpsimi*xpsimi))
     .                   -2.*atan(xpsimi)   +demi*pi
          psim_0      =    stab_s*psim_s+(1.-stab_s)*psim_i
 
!  Virtual Temperature Turbulent Scale thetav*    (ss* impact included   )
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ needed for new ss*)
c #AE     thstarv     = thstar      + Theta0      *(0.608*qqstar
c #AE.                                                   -ssstar
c #AE.                                             )
c #AE.                             /(1.+0.608*QaT_SV(ikl)-qsnoSV(ikl))
c #AE     thstars     =     sign(unun,thstarv)
c #AE     thstara     =      abs(     thstarv)
c #AE     thstarv     =      max(epsi,thstara)    *thstars
 
!  Full   Obukhov Length Scale        (Gall?e et al. 2001, BLM 99, (A1) p.16)
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #AE     LMO_SV(ikl) = Theta0      * us__SV(ikl)* us__SV(ikl)
c #AE.                /(vonkar      * gravit     * thstarv)
C +
c #AE     zetah       = za__SV(ikl) / LMO_SV(ikl)
c #AE     zetam       =           min(zetMAX,zetah)! Strong Stability Limit
                                                   !(Mahalov et al. 2004
                                                   ! GRL 31 2004GL021055)
c #AE     LMOmom(ikl) = za__SV(ikl) /(max(epsi,abs(zetam))
c #AE.                              *sign(unun,    zetam ))
c #AE     zeta0m      = Z0m_SV(ikl) / LMOmom(ikl)
 
!  Snow Erosion    Stability Function (Gall?e et al. 2001, BLM 99, (11) p. 7)
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #AE     stab_s      =  max(zero,sign(unun,zetam))
 
c #AE     psis_s      =  -AsTurb *zetam
c #AE     xpsimi      = sqrt(sqrt(unun-coef_m*min(zero,zetam)))
c #AE     psim_i      =   2. *log(demi*(unun+xpsimi))
c #AE.                       +log(demi*(unun+xpsimi*xpsimi))
c #AE.                   -2.*atan(xpsimi)   +demi*pi
c #AE     psis_z      =    stab_s*psis_s+(1.-stab_s)*psim_i
 
c #AE     psis_s      =  -AsTurb *zeta0m
c #AE     xpsimi      = sqrt(sqrt(unun-coef_m*min(zero,zeta0m)))
c #AE     psim_i      =   2. *log(demi*(unun+xpsimi))
c #AE.                       +log(demi*(unun+xpsimi*xpsimi))
c #AE.                   -2.*atan(xpsimi)   +demi*pi
c #AE     psis_0      =    stab_s*psis_s+(1.-stab_s)*psim_i
 
!  Square Roots of the Drag Coefficient for Snow Erosion Turbulent Flux
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #AE     rCDmSV(ikl) = vonkar/(sqrCm0(ikl)-psim_z+psim_0)
 
c #ss     IF (ii__SV(ikl).EQ.iwr_SV.AND.jj__SV(ikl).EQ.jwr_SV      .AND.
c #ss.        nn__SV(ikl).EQ.nwr_SV                          )
c #ss.    write(6,6600)  Z0m_SV(ikl) , psim_z
c #ss.                  ,LMO_SV(ikl) , uustar
c #ss.                  ,sqrCm0(ikl) , psim_0
c #ss.                  ,LMOmom(ikl) , thstarv
 6600     format(/,' ** SISVATeSBL *0  '
     .            ,'  Z0m_SV  = ',e12.4,'  psim_z  = ',e12.4
     .            ,'  LMO_SV  = ',e12.4,'  uustar  = ',e12.4
     .          ,/,'                   '
     .            ,'  sqrCm0  = ',e12.4,'  psim_0  = ',e12.4
     .            ,'  LMOmom  = ',e12.4,'  thstarv = ',e12.4)
 
 
!  Momentum            Turbulent Scale  u*
!  ---------------------------------------
 
!  Momentum            Turbulent Scale  u*          in case of NO Blow. Snow
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          VVa_OK      =  max(0.000001,       VVaSBL(ikl))
          sss__N      =  vonkar      *       VVa_OK
          sss__F      = (sqrCm0(ikl) - psim_z + psim_0)
          usuth0      =  sss__N /sss__F                ! u* if NO Blow. Snow
 
!  Momentum            Turbulent Scale  u*          in case of    Blow. Snow
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #AE     sss__G      =  0.27417     * gravit
 
!  ______________               _____
!  Newton-Raphson (! Iteration, BEGIN)
!  ~~~~~~~~~~~~~~               ~~~~~
c #AE     DO iit=1,nit
c #AE     sss__K      =  gravit      * r_Turb * A_Turb *za__SV(ikl)
c #AE.                                     *rCDmSV(ikl)*rCDmSV(ikl)
c #AE.                           /(1.+0.608*QaT_SV(ikl)-qsnoSV(ikl))
c #AE     us_127      =  exp(    SblPom *log(us__SV(ikl)))
c #AE     us_227      =  us_127         *    us__SV(ikl)
c #AE     us_327      =  us_227         *    us__SV(ikl)
c #AE     us_427      =  us_327         *    us__SV(ikl)
c #AE     us_527      =  us_427         *    us__SV(ikl)
 
c #AE     us__SV(ikl) =  us__SV(ikl)
c #AE.    - (  us_527     *sss__F     /sss__N
c #AE.      -  us_427
c #AE.      -  us_227     *qsnoSV(ikl)*sss__K
c #AE.      + (us__SV(ikl)*us__SV(ikl)-usthSV(ikl)*usthSV(ikl))/sss__G)
c #AE.     /(  us_427*5.27*sss__F     /sss__N
c #AE.      -  us_327*4.27
c #AE.      -  us_127*2.27*qsnoSV(ikl)*sss__K
c #AE.      +  us__SV(ikl)*2.0                                 /sss__G)
 
c #AE     us__SV(ikl)= min(us__SV(ikl),usuth0)
c #AE     us__SV(ikl)= max(us__SV(ikl),epsi  )
c #AE     rCDmSV(ikl)=     us__SV(ikl)/VVa_OK
! #AE     sss__F     =     vonkar     /rCDmSV(ikl)
c #AE     ENDDO
!  ______________               ___
!  Newton-Raphson (! Iteration, END  )
!  ~~~~~~~~~~~~~~               ~~~
 
c #AE     us_127      =  exp(    SblPom *log(us__SV(ikl)))
c #AE     us_227      =  us_127         *    us__SV(ikl)
 
!  Momentum            Turbulent Scale  u*: 0-Limit in case of no Blow. Snow
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #AE     dusuth      =  us__SV(ikl) - usthSV(ikl)       ! u* - uth*
c #AE     signus      =  max(sign(unun,dusuth),zero)     ! 1 <=> u* - uth* > 0
          us__SV(ikl) =                                  !
c #AE.                   us__SV(ikl)  *signus  +         ! u* (_BS)
     .                   usuth0                          ! u* (nBS)
c #AE.                            *(1.-signus)           !
 
 
!  Blowing Snow        Turbulent Scale ss*
!  ---------------------------------------
 
c #AE     hSalSV(ikl) = 8.436e-2  *exp(SblPom  *log(us__SV(ikl)))
c #AE     qSalSV(ikl) = (us__SV(ikl) * us__SV(ikl)
c #AE.                  -usthSV(ikl) * usthSV(ikl))*signus
c #AE.                / (sss__G      * us_227     )
 
c #ae     qSalSV(ikl) = (us__SV(ikl) * us__SV(ikl)
c #ae.                  -usthSV(ikl) * usthSV(ikl))
c #ae.                  *signus      * us__SV(ikl) *3.25
c #ae.                 /(hSalSV(ikl) * gravit           )
 
c #AE     ssstar      =  rCDmSV(ikl) *(qsnoSV(ikl) -qSalSV(ikl))
c #AE.                 * r_Turb
 
c #AE     uss_SV(ikl) =  min(zero    , us__SV(ikl) *ssstar)
c #BS     uss_SV(ikl) =  max(-0.002  , uss_SV(ikl)        )
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #ss     IF (ii__SV(ikl).EQ.iwr_SV.AND.jj__SV(ikl).EQ.jwr_SV      .AND.
c #ss.        nn__SV(ikl).EQ.nwr_SV                          )
c #ss.    write(6,6610)  usuth0      , us__SV(ikl)
c #ss.                  ,qsnoSV(ikl) , uss_SV(ikl)
c #ss.                  ,usthSV(ikl) , LMO_SV(ikl)
c #ss.                  ,qSalSV(ikl) , VVa_OK
 6610     format(/,' ** SISVATeSBL *1  '
     .            ,'  u*(nBS) = ',e12.4,'  u*(_BS) = ',e12.4
     .            ,'  Qs      = ',e12.4,'  u*Qs*   = ',e12.4
     .          ,/,'                   '
     .            ,'  u*(_th) = ',e12.4,'  LMO     = ',e12.4
     .            ,'  QSalt   = ',e12.4,'  VVa     = ',e12.4)
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! #wx IF       (ikl .EQ. kSV_v1   .AND.   lSV_v1 .GT. 0
! #wx.                            .AND.   lSV_v1 .LE. 2)
! #wx.    write(6,6000)  daHost     ,     icount     ,
! #wx.                   us__SV(ikl),1.e3*hSalSV(ikl),
! #wx.              1.e3*Z0m_SV(ikl),
! #wx.              1.e3*qsnoSV(ikl),1.e3*qSalSV(ikl)
! #wx.                  ,usthSV(ikl),     us__SV(ikl)-usthSV(ikl),
! #wx.              1.e3*ssstar     ,1.e3*us__SV(ikl)*ssstar
 6000     format(a18,i3,6x,'u*   [m/s] =',f6.3,'   hSalt[mm]='  ,e9.3,
     .                  '   Z0m   [mm] =',f9.3,'   q   [g/kg] =',f9.3,
     .               /,91x,                    '   qSa [g/kg] =',f9.3,
     .               /,27x, 'ut*[m/s]='  ,e9.3,'   u*-ut*   ='  ,e9.3,
     .                  '   s*  [g/kg] =',f9.3,'   us* [mm/s] =',f9.3)
 
 
!  Virtual Temperature Turbulent Scale thetav*    (ss* impact included)
!  --------------------------------------------------------------------
 
c #AE     thstarv     = thstar      + Theta0      *(0.608*qqstar
c #AE.                                                   -ssstar
c #AE.                                             )
c #AE.                             /(1.+0.608*QaT_SV(ikl)-qsnoSV(ikl))
c #AE     thstars     =     sign(unun,thstarv)
c #AE     thstara     =      abs(     thstarv)
c #AE     thstarv     =      max(epsi,thstara)    *thstars
 
 
!  Full   Obukhov Length Scale (Gall?e et al., 2001, BLM 99, (A1) p.16)
!  --------------------------------------------------------------------
 
c #AE     LMO_SV(ikl) = Theta0      * us__SV(ikl)* us__SV(ikl)
c #AE.                /(vonkar      * gravit     * thstarv)
 
c #AE     zetah       = za__SV(ikl) / LMO_SV(ikl)
c #AE     zetam       =           min(zetMAX,zetah)! Strong Stability Limit
                                                   !(Mahalov et al. 2004
                                                   ! GRL 31 2004GL021055)
c #AE     LMOmom(ikl) = za__SV(ikl) /(max(epsi,abs(zetam))
c #AE.                              *sign(unun,    zetam ))
c #AE     zeta0m      = Z0m_SV(ikl) / LMOmom(ikl)
c #AE     zeta0h      = Z0h_SV(ikl) / LMO_SV(ikl)
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! #wx IF       (ikl .EQ. kSV_v1   .AND.   lSV_v1 .GT. 0
! #wx.                            .AND.   lSV_v1 .LE. 2)
! #wx.    write(6,6001)  LMO_SV(ikl)    ,    zetah
 6001     format(18x,9x,'LMO  [m]=',f9.1,'   zetah[-] =',f9.3)
 
 
!  Turbulent Scales
!  ----------------
 
!  Momentum Stability Function (Gall?e et al., 2001, BLM 99, (11) p. 7)
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          stab_s      =  max(zero,sign(unun,zetam))
 
          psim_s      =  -A_Turb *zetam
          xpsimi      = sqrt(sqrt(unun-coef_m*min(zero,zetam)))
          psim_i      =   2. *log(demi*(unun+xpsimi))
     .                       +log(demi*(unun+xpsimi*xpsimi))
     .                   -2.*atan(xpsimi)   +demi*pi
          psim_z      =    stab_s*psim_s+(1.-stab_s)*psim_i
 
          psim_s      =  -A_Turb *zeta0m
          xpsimi      = sqrt(sqrt(unun-coef_m*min(zero,zeta0m)))
          psim_i      =   2. *log(demi*(unun+xpsimi))
     .                       +log(demi*(unun+xpsimi*xpsimi))
     .                   -2.*atan(xpsimi)   +demi*pi
          psim_0      =    stab_s*psim_s+(1.-stab_s)*psim_i
 
!  Heat     Stability Function (Gall?e et al., 2001, BLM 99, (11) p. 7)
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          stab_s      =  max(zero,sign(unun,zetah))
 
          psih_s      =  -AhTurb *zetah
          xpsihi      = sqrt(sqrt(unun-coef_h*min(zero,zetah)))
          psih_i      =   2. *log(demi*(unun+xpsihi))
          psih_z      =    stab_s*psih_s+(1.-stab_s)*psih_i
 
          psih_s      =  -AhTurb *zeta0h
          xpsihi      = sqrt(sqrt(unun-coef_h*min(zero,zeta0h)))
          psih_i      =   2. *log(demi*(unun+xpsihi))
          psih_0      =    stab_s*psih_s+(1.-stab_s)*psih_i
 
!  Square Roots of the Drag Coefficients
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          rCDhSV(ikl) = vonkar*(ExnrSV(ikl)/pcap)
     .                        /(sqrCh0(ikl)-psih_z+psih_0)
          rCDmSV(ikl) = vonkar/(sqrCm0(ikl)-psim_z+psim_0)
 
!  Drag Coefficients
!  ~~~~~~~~~~~~~~~~~
          CDh(ikl)    = rCDmSV(ikl) * rCDhSV(ikl)
          CDm(ikl)    = rCDmSV(ikl) * rCDmSV(ikl)
 
!  Real    Temperature Turbulent Scale theta*
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          thstar      = rCDhSV(ikl) * dTa_Ts(ikl)
     .                              *(pcap       /ExnrSV(ikl))
          uts_SV(ikl) = us__SV(ikl) * thstar
 
 
!  Convergence Criterion
!  =====================
 
          dustar      = max(dustar,abs(us__SV(ikl)-u0star))
 
!  u*, u*T*, u*s* Time Moving Averages
!  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #AM     DO nt=   1,ntaver-1
c #AM     u__mem(ikl,nt    ) = u__mem(ikl,nt+1)
c #AT     uT_mem(ikl,nt    ) = uT_mem(ikl,nt+1)
c #AS     us_mem(ikl,nt    ) = us_mem(ikl,nt+1)
c #AM     ENDDO
c #AM     u__mem(ikl,ntaver) = us__SV(ikl)
c #AT     uT_mem(ikl,ntaver) = uts_SV(ikl)
c #AS     us_mem(ikl,ntaver) = uss_SV(ikl)
 
!  OUTPUT for Verification (general)
!  ~~~~~~~~~~~~~~~~~~~~~~~
c #wr    IF (icount     .EQ.1  )                                    THEN
c #wr     write(6,6004)
 6004     format(122('-'))
c #wr    IF (mod(VVaSBL(ikl),4.).LT.0.1)                            THEN
c #wr     write(6,6003)
 6003     format('   V  Ta-Ts  Z0      It'
     .   ,' du*     u*    sss__F   CD       Qss       Qs*     '
     .   ,' PseudOL Full-OL zetam   zetah   psim_z  psih_z')
c #wr     write(6,6004)
c #wr    END IF
c #wr    END IF
c #wr     write(6,6002) VVaSBL(ikl),dTa_Ts(ikl),Z0m_SV(ikl),icount
c #wr.                 ,dustar     ,us__SV(ikl),sss__F
c #wr.                 ,   CDm(ikl),qSalSV(ikl),ssstar
c #wr.                 ,W_pLMO     ,LMO_SV(ikl)
c #wr.                 ,zetam      ,zetah      ,W_psim     ,psih_z
 6002     format(2f6.1,f8.4,i3,f9.6,f6.3,f9.3,3f9.6,2f8.2,2f8.4,2f8.2)
 
!  OUTPUT for Verification (u*_AE)
!  ~~~~~~~~~~~~~~~~~~~~~~~
c #w1    IF (icount     .EQ.1  )                                    THEN
c #w1     write(6,6014)
 6014     format(100('-'))
c #w1    IF (mod(VVaSBL(ikl),4.).LT.0.1)                            THEN
c #w1     write(6,6013)
 6013     format('   V  Ta-Ts  Z0      It'
     .   ,' du*     u*    sss__F   W_NUs1   W_NUs2   W_NUs3      '
     .   ,' W_DUs1     W_DUs2 ')
c #w1     write(6,6014)
c #w1    END IF
c #w1    END IF
c #w1     write(6,6012) VVaSBL(ikl),dTa_Ts(ikl),Z0m_SV(ikl),icount
c #w1.                 ,dustar     ,us__SV(ikl),sss__F
c #w1.                 ,W_NUs1     ,W_NUs2     ,W_NUs3
c #w1.                 ,W_DUs1     ,W_DUs2
 6012     format(2f6.1,f8.4,i3,f9.6,f6.3,f9.3,3f9.3,2f12.3)
 
        END DO
 
c #IX IF (                     icount.lt. 3)                     GO TO 1
!     IF (dustar.gt.0.0001.AND.icount.lt. 6)                     GO TO 1
 
 
c #AM   DO ikl=1,klonv
c #AM     u0star      =      0.0
c #AT     uTstar      =      0.0
c #AS     usstar      =      0.0
c #AM     DO nt=1,ntaver
c #AM     u0star      = u0star + u__mem(ikl,nt)
c #AT     uTstar      = uTstar + uT_mem(ikl,nt)
c #AS     usstar      = usstar + us_mem(ikl,nt)
c #AM     ENDDO
c #AM     us__SV(ikl) = u0star / ntaver
c #AT     uts_SV(ikl) = uTstar / ntaver
c #AS     uss_SV(ikl) = usstar / ntaver
c #AM   END DO
 
 
!  Aerodynamic Resistances
!  -----------------------
 
        DO ikl=1,klonv
          ram_sv(ikl) = 1./(CDm(ikl)*max(VVaSBL(ikl),epsi))
          rah_sv(ikl) = 1./(CDh(ikl)*max(VVaSBL(ikl),epsi))
        END DO
 
 
      return
      end
