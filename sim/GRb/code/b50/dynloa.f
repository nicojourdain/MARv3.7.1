 
 
      subroutine DYNloa
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS                                      Mon 11-Apr-2011  MAR |
C |   SubRoutine DYNloa computes the Air Loading due to the Precipitation  |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT   (via common block)                                           |
C |   ^^^^^   qvDY(mx,my,mz) : Air Specific Humidity               [kg/kg] |
C |           qwHY(mx,my,mz) : Cloud Droplets     Concentration    [kg/kg] |
C |           qiHY(mx,my,mz) : Cloud ice Crystals Concentration    [kg/kg] |
C |           qrHY(mx,my,mz) : Rain  Drops        Concentration    [kg/kg] |
C |           qsHY(mx,my,mz) : Snow  Flakes       Concentration    [kg/kg] |
C |                                                                        |
C |   OUTPUT  (via common block)                                           |
C |   ^^^^^^ virDY(mx,my,mz) : Air Loading by Water Vapor & Hydrometeors   |
C |          virSL(mx,my)    : Air Loading by Water Vapor & Hydrometeors   |
C |                            (in the Surface Layer)                      |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_DY.inc'
 
      include 'MAR_HY.inc'
c #TC include 'MAR_TC.inc'
 
      include 'MAR_SL.inc'
 
 
C +--Local  Variables
C +  ================
 
c #BV include 'MARvec.inc'
c #BV integer  kdim
 
c #BV real     deltav
 
 
C +--DATA
C +  ====
 
c #BV data    deltav/0.20/
 
 
C +--Air Loading including Specific Humidity (Level 0)
C +  =================================================
 
      DO k=1,mz
      DO j=1,my
      DO i=1,mx
        virDY( i,j,k) = qvDY(i,j,k)
      END DO
      END DO
      END DO
 
 
C +--Air Loading including all Hydrometeors  (Level 1)
C +  =================================================
 
      DO k=1,mz
      DO j=1,my
      DO i=1,mx
        virDY( i,j,k) = qvDY(i,j,k)
     .   -1.64*min(demi,qwHY(i,j,k)+qiHY(i,j,k)+qrHY(i,j,k)+qsHY(i,j,k)
c #DV.                 +qxTC(i,j,k,1)
     .                                                                 )
      END DO
      END DO
      END DO
 
 
C +--Air Loading including Correction Factor (Level 2)
C +  =================================================
 
      DO k=1,mz
      DO j=1,my
      DO i=1,mx
        virDY( i,j,k) = 0.850 * virDY( i,j,k)
      END DO
      END DO
      END DO
 
 
C +--Lateral Boundary Conditions
C +  ===========================
 
      DO   k=1,mz
        DO j=1,my
          virDY( 1,j,k) = virDY(ip11,j,k)
          virDY(mx,j,k) = virDY(mx1,j,k)
        END DO
 
        DO i=1,mx
          virDY(i, 1,k) = virDY(i,jp11,k)
          virDY(i,my,k) = virDY(i,my1,k)
        END DO
 
      END DO
 
 
C +--Air Loading for SBL Parameterization
C +  ====================================
 
c #BV    dumeps(    1)  = deltav
c #BV DO j=1,my
c #BV DO i=1,mx
c #BV    dumy3D(i,j,1)  = fracSL*0.715*virDY(i,j,mz)
c #BV.             + (1.0-fracSL)     *virSL(i,j)
c #BV END DO
c #BV END DO
c #BV    kdim           = 1
 
 
C +--Filtering
C +  ---------
 
c #BV IF   (mmx.gt.1)                                             THEN
c #BV   IF (mmy.eq.1)                                             THEN
 
C +            *********
c #BV     call DYNfil_1D(dumy3D,dumeps,kdim)
C +            *********
 
c #BV   ELSE
 
c #bv     IF (no_vec)                                             THEN
 
c #bv               if(openmp) then
C +                      **********
c #bv               call DYNfil_3D_mp (dumy3D,dumeps,kdim)
C +                      **********
c #bv               else
C +                      **********
c #bv               call DYNfil_3D (dumy3D,dumeps,kdim)
C +                      **********
c #bv               endif
 
c #bv     ELSE
 
C +              *********
c #BV       call DYNfilv3D (dumy3D,dumeps,kdim)
C +              *********
 
c #bv     END IF
 
c #BV   END IF
c #BV END IF
 
c #BV DO j=1,my
c #BV DO i=1,mx
c #BV     virSL(i,j)   = dumy3D(i,j,1)
c #BV END DO
c #BV END DO
 
 
C +--NO Loading included in case of Mountain Waves Tests
C +  ===================================================
 
c #OM DO k=1,mz
c #OM DO j=1,my
c #OM DO i=1,mx
c #OM     virDY(i,j,k) = 0.0d+0
c #OM END DO
c #OM END DO
c #OM END DO
 
      return
      end
