 
 
 
      subroutine FILatmo
 
C +------------------------------------------------------------------------+
C | MAR Filtering                                        19-01-2014-XF MAR |
C +------------------------------------------------------------------------+
 
      IMPLICIT NONE
 
C +
C +--General Variables
C +  =================
 
      include 'MARphy.inc'
      include 'MARCTR.inc'
      include 'MAR_SV.inc'
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
      include 'MAR_DY.inc'
      include 'MAR_LB.inc'
      include 'MAR_SL.inc'
      include 'MAR_SN.inc'
      include 'MAR_BS.inc'
      include 'MAR_IO.inc'
      include 'MAR_TV.inc'
      include 'MARsSN.inc'
      include 'MAR_IB.inc'
      include 'MARdSV.inc'
 
      real min_tt_sea,min_tt_land,diff_max
 
c #AC data min_tt_sea /-80./ !degree C
c #AC data min_tt_land/-90./ !degree C
 
      data min_tt_sea /-70./ !degree C
      data min_tt_land/-75./ !degree C
      data diff_max   / 35./ !degree C
 
      real           :: min_tt,tt,diff,ww,w
      real           :: pk,ua,va,wa,gp,ps
 
      integer        :: n,ii,jj
 
      if(iterun<=1) then
 
       write(6,11) min_tt_sea,min_tt_land
11     format('XF WARNING: FILatmo min Temp. over  sea=',f5.0,
     .                           ' min Temp. over land=',f5.0)
 
      else
 
!$OMP PARALLEL DO default(shared)
!$OMP. private(i,j,k,min_tt,diff,pk,ua,va,wa,gp,ps,ww,ii,jj)
!$OMP. schedule(dynamic)
       do k=mzabso,mz
 
        do i=2,mx-1 ; do j=2,my-1
 
         if(isolSL(i,j)<=2) then
          min_tt=min_tt_sea
         else
          min_tt=min_tt_land
         endif
 
         if(isnan(pktaDY(i,j,k)).or.isnan(uairDY(i,j,k)).or.
     .      isnan(tairDY(i,j,k)).or.isnan(vairDY(i,j,k)))then
          print *,"STOP in filatmo.f: NaN on pixel(i,j,k)",i,j,k
          print *,tairDY(i,j,k),uairDY(i,j,k),vairDY(i,j,k)
          stop
         endif
 
         diff=0
 
         if (tairdy(i,j,k)-273.15<min(-30.,min_tt+30.)) then
          diff=max(diff,abs(tairdy(i,j,k)-tairdy(i-1,j  ,k)))
          diff=max(diff,abs(tairdy(i,j,k)-tairdy(i+1,j  ,k)))
          diff=max(diff,abs(tairdy(i,j,k)-tairdy(i  ,j-1,k)))
          diff=max(diff,abs(tairdy(i,j,k)-tairdy(i  ,j+1,k)))
         endif
 
         if (tairdy(i,j,k)-273.15<min_tt.or.diff>diff_max.or.
     .       isnan(pktaDY(i,j,k)))then
 
          pk=0;ua=0;va=0;wa=0;gp=0;ps=0;ww=0
 
          do ii=-1,1 ; do jj=-1,1
 
                                w=1
            if(ii==0.or. jj==0) w=2
            if(ii==0.and.jj==0) w=0
 
            if (tairdy(i+ii,j+jj,k)-273.15>min_tt-20.and.
     .          .not.isnan(pktaDY(i+ii,j+jj,k))) then
             pk=pk+w*pktaDY(i+ii,j+jj,k)
c            ua=ua+w*uairDY(i+ii,j+jj,k)
c            va=va+w*vairDY(i+ii,j+jj,k)
c            wa=wa+w*wairDY(i+ii,j+jj,k)
c            gp=gp+w*gplvDY(i+ii,j+jj,k)
c            ps=ps+w* pstDY(i+ii,j+jj)
             ww=ww+w
            endif
 
          enddo ; enddo
 
          if (ww>2) then
           pktaDY(i,j,k)=pk/ww
c          uairDY(i,j,k)=ua/ww
c          vairDY(i,j,k)=va/ww
c          wairDY(i,j,k)=wa/ww
c          gplvDY(i,j,k)=gp/ww
c           pstDY(i,j  )=ps/ww
 
           tt            = -273.15 + pktaDY(i,j,k)
     .                   * ((pstDY(i,j)*sigma(k)+ptopDY)**cap)
!$OMP      CRITICAL
           write(6,12) iyrrGE,mmarGE,jdarGE,jhurGE,minuGE,
     .                 i,j,k,tairdy(i,j,k)-273.15,tt
   12      format('ERROR FILatmo',
     .             i5,4i3,' for (',i3,','i3,',',i2,')',f6.0,'=>',f6.0)
           write(6,*)
!$OMP      END CRITICAL
          endif
         endif
 
       enddo     ; enddo
 
       enddo
!$OMP END PARALLEL DO
 
      endif
 
      end
