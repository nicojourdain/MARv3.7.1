      function TeqCVA(T_Draf,ExDraf,QvDraf,GlacDg)
C +
C +------------------------------------------------------------------------+
C | MAR CONVECTION                                         25-11-1999  MAR |
C |                                                                        |
C |   Function TeqCVA computes the Equivalent Potential Temperture         |
C |                                                                        |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +--Global Variables
C +  ================
C +
      include 'MARphy.inc'
C +
      real     TeqCVA,T_Draf,ExDraf,QvDraf,GlacDg
C +
C +
C +--Local  Variables
C +  ================
C +
      real     h2oLCp,h2oSCp,h2oMCp,argexp
      logical  GlaCld
C +
      data h2oLCp / 2490.04e+0 /
C +      = Lv_H2O [Latent Heat of Vaporisation for Water (2.500d+6 J/kg)]
C +      / Cp     [Air Specific Heat                     (1.004d+3 J/kg/K)]
C +
      data h2oSCp / 2822.31e+0 /
C +      = Ls_H2O [Latent Heat of Vaporisation for Water (2.8336+6 J/kg)]
C +      / Cp     [Air Specific Heat                     (1.004d+3 J/kg/K)]
C +
c #CG data GlaCld / .true.     /
C +
C +
C +--Equivalent Potential Temperature
C +  ================================
C +
c #CG IF      (GlaCld)                                            THEN
c #CG          h2oMCp=h2oSCp*GlacDg+h2oLCp*(1.0d+0 -GlacDg)
c #CG          argexp=1.0273*h2oMCp*(1.0+0.810*QvDraf)
c #CG ELSE
               argexp=       h2oLCp
c #CG END IF
C +
               TeqCVA=T_Draf/ExDraf
     .                        *exp(QvDraf*argexp/T_Draf)
C +
      return
      end
