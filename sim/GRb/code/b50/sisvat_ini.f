 
 
      subroutine SISVAT_ini
 
C +------------------------------------------------------------------------+
C | MAR          SISVAT_ini                            Sun 31-10-2010  MAR |
C |   SubRoutine SISVAT_ini generates non time dependant SISVAT parameters |
C +------------------------------------------------------------------------+
C |   PARAMETERS:  klonv: Total Number of columns =                        |
C |   ^^^^^^^^^^        = Total Number of continental     grid boxes       |
C |                     X       Number of Mosaic Cell per grid box         |
C |                                                                        |
C |   INPUT:   dt__SV   : Time  Step                                   [s] |
C |   ^^^^^    dz_dSV   : Layer Thickness                              [m] |
C |                                                                        |
C |   OUTPUT:  RF__SV   : Root Fraction in Layer isl                   [-] |
C |   ^^^^^^   rocsSV   : Soil Contrib. to (ro c)_s exclud.Water  [J/kg/K] |
C |            etamSV   : Soil Minimum Humidity                    [m3/m3] |
C |                      (based on a prescribed Soil Relative Humidity)    |
C |            s1__SV   : Factor of eta**( b+2) in Hydraul.Diffusiv.       |
C |            s2__SV   : Factor of eta**( b+2) in Hydraul.Conduct.        |
C |            aKdtSV   : KHyd: Piecewise Linear Profile:  a * dt    [m]   |
C |            bKdtSV   : KHyd: Piecewise Linear Profile:  b * dt    [m/s] |
C |            dzsnSV(0): Soil first Layer Thickness                   [m] |
C |            dzmiSV   : Distance between two contiguous levels       [m] |
C |            dz78SV   : 7/8 (Layer Thickness)                        [m] |
C |            dz34SV   : 3/4 (Layer Thickness)                        [m] |
C |            dz_8SV   : 1/8 (Layer Thickness)                        [m] |
C |            dzAvSV   : 1/8  dz_(i-1) + 3/4 dz_(i) + 1/8 dz_(i+1)    [m] |
C |            dtz_SV   : dt/dz                                      [s/m] |
C |            OcndSV   : Swab Ocean / Soil Ratio                      [-] |
C |            Implic   : Implicit Parameter  (0.5:  Crank-Nicholson)      |
C |            Explic   : Explicit Parameter = 1.0 - Implic                |
C |                                                                        |
C | # OPTIONS: #ER: Richards Equation is not smoothed                      |
C | # ^^^^^^^  #kd: De Ridder   Discretization                             |
C | #          #SH: Hapex-Sahel Values                                     |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include  "MARphy.inc"
      include  "MAR_SV.inc"
      include  "MARdSV.inc"
      include  "MAR0SV.inc"
 
 
C +--INPUT / OUTPUT
C +  --------------
 
      include  "MARxSV.inc"
!$OMP threadprivate(/xSISVAT_I/,/xSISVAT_R/)
 
C +--Internal Variables
C +  ==================
 
      integer   ivt   ,ist   ,ivg   ,ikl   ,isl   ,isn   ,ikh
      integer   misl_2,nisl_2
      real      zDepth
      real      d__eta,eta__1,eta__2,Khyd_1,Khyd_2
      real      RHsMin                          ! Min.Soil Relative Humidity
      real      PsiMax                          ! Max.Soil Water    Potential
      real      a_Khyd,b_Khyd                   ! Piecewis.Water Conductivity
c #WR real      Khyd_x,Khyd_y
 
 
C +--DATA
C +  ====
 
      data      RHsMin/0.001/                   ! Min.Soil Relative Humidity
 
 
C +--Non Time Dependant SISVAT parameters
C +  ====================================
 
C +--Soil Discretization
C +  -------------------
 
C +--Numerical Scheme Parameters
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^
        Implic = 0.75                           ! 0.5  <==> Crank-Nicholson
        Explic = 1.00 - Implic                  !
 
C +--Soil/Snow Layers Indices
C +  ^^^^^^^^^^^^^^^^^^^^^^^^
      DO  isl=-nsol,0
        islpSV(isl) =           isl+1
        islpSV(isl) = min(      islpSV(isl),0)
        islmSV(isl) =           isl-1
        islmSV(isl) = max(-nsol,islmSV(isl))
      END DO
 
      DO  isn=1,nsno
        isnpSV(isn) =           isn+1
        isnpSV(isn) = min(      isnpSV(isn),nsno)
      END DO
 
C +--Soil      Layers Thicknesses
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
c #kd IF (nsol.gt.4)                                              THEN
c #kd   DO isl=-5,-nsol,-1
c #kd     dz_dSV(isl)=   1.
c #kd   END DO
c #kd END IF
 
      IF (nsol.ne.4)                                              THEN
        DO isl= 0,-nsol,-1
          misl_2 =     -mod(isl,2)
          nisl_2 =         -isl/2
          dz_dSV(isl)=(((1-misl_2) * 0.001
     .                  +  misl_2  * 0.003) * 10**(nisl_2)) * 4.
C +...    dz_dSV(0)  =         Hapex-Sahel Calibration:       4 mm
 
        END DO
          dz_dSV(0)  =               0.001
          dz_dSV(-1) = dz_dSV(-1)  - dz_dSV(0)          + 0.004
      END IF
 
        zz_dSV      = 0.
      DO  isl=-nsol,0
        dzmiSV(isl) = 0.500*(dz_dSV(isl)        +dz_dSV(islmSV(isl)))
        dziiSV(isl) = 0.500* dz_dSV(isl)        /dzmiSV(isl)
        dzi_SV(isl) = 0.500* dz_dSV(islmSV(isl))/dzmiSV(isl)
        dtz_SV(isl) =        dt__SV             /dz_dSV(isl)
        dz78SV(isl) = 0.875* dz_dSV(isl)
        dz34SV(isl) = 0.750* dz_dSV(isl)
        dz_8SV(isl) = 0.125* dz_dSV(isl)
        dzAvSV(isl) = 0.125* dz_dSV(islmSV(isl))
     .              + 0.750* dz_dSV(isl)
     .              + 0.125* dz_dSV(islpSV(isl))
c #ER   dz78SV(isl) =        dz_dSV(isl)
c #ER   dz34SV(isl) =        dz_dSV(isl)
c #ER   dz_8SV(isl) = 0.
c #ER   dzAvSV(isl) =        dz_dSV(isl)
        zz_dSV      = zz_dSV+dz_dSV(isl)
      END DO
      DO ikl=1,klonv
        dzsnSV(ikl,0) =      dz_dSV(0)
      END DO
 
C +--Conversion to a 50 m Swab Ocean Discretization
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        OcndSV = 0.
      DO isl=-nsol,0
        OcndSV = OcndSV +dz_dSV(isl)
      END DO
        OcndSV = 50.    /OcndSV
 
 
C +--Secondary Vegetation Parameters
C +  -------------------------------
 
C +--Minimum Stomatal Resistance (Hapex Sahel Data)
C +  (Taylor et al. 1997, J.Hydrol 188-189, p.1047)
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
c #SH DO ivg=1,3                       !
c #SH   StodSV(ivg) = 210.             ! Millet
c #SH END DO                           !
c #SH   StodSV(  4) = 120.             ! Sparse Tiger Bush
c #SH DO ivg=5,6                       !
c #SH   StodSV(ivg) =  80.             ! Dense  Tiger Bush
c #SH END DO                           !
c #SH   StodSV(  7) =  80.             ! Low    Trees (Fallow)
c #SH   StodSV( 10) =  80.             !
 
C +--Minimum Stomatal Resistance (Tropical Forest)
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
c #SH   StodSV(  8) =  60.             ! Medium Trees
c #SH   StodSV( 11) =  60.             !
c #SH   StodSV(  9) =  40.             ! High   Trees
c #SH   StodSV( 12) =  40.             !
 
C +--Root Fraction
C +  ^^^^^^^^^^^^^
C +  * GENERAL REFERENCE
C +    Jackson et al., 1996: A global analysis of root distributions for
C +    terrestrial biomes. In Oecologia, 108, 389-411.
 
C +  * ROOT PROFILE
C +    The cumulative root fraction Y is given by
C +        Y = 1 - beta**d   with d    the depth (in cm),
C +                               beta a coefficient (vegetation dependent).
 
C +  * BETA VALUES (for 11 world biomes)
C +  1  boreal forest                0.943
C +  2  crops                        0.961
C +  3  desert                       0.975
C +  4  sclerophyllous shrubs        0.964
C +  5  temperate coniferous forest  0.976
C +  6  temperate deciduous forest   0.966
C +  7  temperate grassland          0.943
C +  8  tropical deciduous forest    0.961
C +  9  tropical evergreen forest    0.962
C +  10 tropical grassland savanna   0.972
C +  11 tundra                       0.914
 
C +  * ADVISED BETA VALUES FOR MAR
C +    (see 'block data SISVAT_dat', variable rbtdSV)
C +
C +    SVAT veg. type         default      West Africa
C +    0  barren soil         0.000        0.000
C +    1  crops low           0.961 (2)    0.961 (2)
C +    2  crops medium        0.961 (2)    0.961 (2)
C +    3  crops high          0.961 (2)    0.961 (2)
C +    4  grass low           0.943 (7)    0.943 (7)
C +    5  grass medium        0.943 (7)    0.964 (4)
C +    6  grass high          0.943 (7)    0.972 (10)
C +    7  broadleaf low       0.966 (6)    0.968 (4,10)
C +    8  broadleaf medium    0.966 (6)    0.962 (8,9)
C +    9  broadleaf high      0.966 (6)    0.962 (8,9)
C +    10 needleleaf low      0.976 (5)    0.971 (5,6)
C +    11 needleleaf medium   0.976 (5)    0.976 (5)
C +    12 needleleaf high     0.976 (5)    0.976 (5)
 
C +    Numbers between brackets refer to Jackson's biomes. For more details
C +    about some choices, see the correspondance between the IGBP and SVAT
C +    vegetation classes (i.e. in NESTOR).
 
C +  * WARNING
C +    Most of the roots are located in the first 2 m of soil. The root
C +    fraction per layer depends on the definition of the soil layer
C +    thickness. It will get wrong if a thick layer is defined around 2 m
C +    deep.
 
      write(*,'(/a)') 'ROOT PROFILES (Jackson, 1996) :'
 
      DO ivt = 0, nvgt
        zDepth = 0.
        DO isl = 0, -nsol, -1
          IF (ivt .ne. 0) THEN
            RF__SV(ivt,isl) =  rbtdSV(ivt)**zDepth *
     .                         (1. - rbtdSV(ivt)**(dz_dSV(isl)*100) )
            zDepth = zDepth + dz_dSV(isl)*100  !in cm
          ELSE
            RF__SV(ivt,isl) = 0.
          END IF
        END DO
        write(*,'(a,i2,a,i3,a,99f10.5:)')
     .       '  RF__SV(', ivt, ',', -nsol, ':0) =', RF__SV(ivt,:)
      END DO
      write(6,6600)
 6600 format(
     .  '  NOTE: If root fraction is not close to 0  around 2 m deep,',
     ./,'        Then you should redefine the soil layer thicknesses.',
     ./,'        See the code for more details.')
 
 
C +--Secondary Soil       Parameters
C +  -------------------------------
 
      DO  ist=0,nsot
         rocsSV(ist)=(1.0-etadSV(ist))*1.2E+6  ! Soil Contrib. to (ro c)_s
         s1__SV(ist)=     bCHdSV(ist)          ! Factor of (eta)**(b+2)
     .  *psidSV(ist)     *Ks_dSV(ist)          !    in DR97, Eqn.(3.36)
     . /(etadSV(ist)**(   bCHdSV(ist)+3.))     !
         s2__SV(ist)=     Ks_dSV(ist)          ! Factor of (eta)**(2b+3)
     . /(etadSV(ist)**(2.*bCHdSV(ist)+3.))     !    in DR97, Eqn.(3.35)
 
C +--Soil Minimum Humidity (from a prescribed minimum relative Humidity)
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         Psimax = -(log(RHsMin))/7.2E-5        ! DR97, Eqn 3.15 Inversion
         etamSV(ist) =  etadSV(ist)
     .         *(PsiMax/psidSV(ist))**(-min(10.,1./bCHdSV(ist)))
      END DO
         etamSV(12)  =  0.
 
C +--Piecewise Hydraulic Conductivity Profiles
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      DO   ist=0,nsot
 
c #WR     write(6,6000)
 6000     format(' Type |    etaSat | No |    eta__1 |    eta__2 |',
     .           '    Khyd_1 |    Khyd_x |    Khyd_2 |    Khyd_y |'
     .         /,' -----+-----------+----+-----------+-----------+',
     .           '-----------+-----------+-----------+-----------+')
 
          d__eta          =  etadSV(ist)/nkhy
          eta__1          =  0.
          eta__2          =  d__eta
        DO ikh=0,nkhy
          Khyd_1          =  s2__SV(ist)             ! DR97, Eqn.(3.35)
     .  *(eta__1      **(2. *bCHdSV(ist)+3.))        !
          Khyd_2          =  s2__SV(ist)             !
     .  *(eta__2      **(2. *bCHdSV(ist)+3.))        !
 
          a_Khyd          = (Khyd_2-Khyd_1)/d__eta   !
          b_Khyd          =  Khyd_1-a_Khyd *eta__1   !
c #WR     Khyd_x          =  a_Khyd*eta__1 +b_Khyd   !
c #WR     Khyd_y          =  a_Khyd*eta__2 +b_Khyd   !
          aKdtSV(ist,ikh) =  a_Khyd       * dt__SV   !
          bKdtSV(ist,ikh) =  b_Khyd       * dt__SV   !
 
c #WR     write(6,6001) ist,etadSV(ist),ikh,eta__1,
c #WR.          eta__2,Khyd_1,Khyd_x,Khyd_2,Khyd_y
 6001     format(i5,' |',e10.2,' |',i3,' |',
     .                 6(e10.2,' |'))
 
          eta__1          = eta__1  + d__eta
          eta__2          = eta__2  + d__eta
        END DO
      END DO
 
      return
      end
