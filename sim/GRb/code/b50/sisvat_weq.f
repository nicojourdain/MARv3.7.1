 
 
      subroutine SISVAT_wEq( labWEq ,istart)
 
C +------------------------------------------------------------------------+
C | MAR          SISVAT_wEq                                22-09-2001  MAR |
C |   SubRoutine SISVAT_wEq computes the Snow/Ice  Water  Equivalent       |
C |                                                                        |
C |                                                                        |
C |   Preprocessing  Option: SISVAT IO (not always a standard preprocess.) |
C |   ^^^^^^^^^^^^^^^^^^^^^  ^^^^^^^^^                                     |
C |   FILE                 |      CONTENT                                  |
C |   ~~~~~~~~~~~~~~~~~~~~~+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ |
C | # SISVAT_wEq.ve        | #ve: OUTPUT/Verification: Snow/Ice Water Eqv. |
C |                        |      unit 45, SubRoutine  SISVAT_wEq **ONLY** |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include  "MARphy.inc"
      include  "MAR_SV.inc"
      include  "MARxSV.inc"
!$OMP threadprivate(/xSISVAT_I/,/xSISVAT_R/)
 
 
      character*6         labWEq
      integer             istart
 
      logical             logWEq
      common/SISVAT_wEq_L/logWEq
 
 
C +--Local  Variables
C +  ================
 
      integer  ikl   ,isn
      real     SnoWEQ,IceWEQ
 
 
C +--Switch Initialization
C +  =====================
 
      IF (.NOT.logWEq)                                              THEN
               logWEq = .true.
               open(unit=45,status='unknown',file='SISVAT_wEq.ve')
               rewind    45
      END IF
 
 
C +--Snow Water Equivalent
C +  =====================
 
           ikl   = 1
      IF          (isnoSV(ikl).gt.iiceSV(ikl))                      THEN
 
          SnoWEQ = 0.
        DO isn   = iiceSV(ikl)+1 ,isnoSV(ikl)
          SnoWEQ = SnoWEQ       + ro__SV(ikl,isn) * dzsnSV(ikl,isn)
        END DO
 
      END IF
 
 
C +--Ice  Water Equivalent
C +  =====================
 
      IF        (iiceSV(1).gt.0)                                    THEN
 
          IceWEQ = 0.
        DO isn   =             1 ,iiceSV(ikl)
          IceWEQ = IceWEQ       + ro__SV(ikl,isn) * dzsnSV(ikl,isn)
        END DO
 
      END IF
 
 
C +--OUTPUT
C +  ======
 
      IF (istart.eq.1)                                              THEN
        write(45,45)dahost,i___SV(lwriSV(1)),j___SV(lwriSV(1)),
     .              n___SV(lwriSV(1))
 45     format(a18,10('-'),'Pt.',3i4,60('-'))
      END IF
 
      write(45,450) labWEq,IceWEQ,iiceSV(ikl),SnoWEQ
     .                    ,IceWEQ+SnoWEQ,isnoSV(ikl)
     .                                  ,drr_SV(ikl)*dt__SV
     .                                  ,dsn_SV(ikl)*dt__SV
     .                                  ,BufsSV(ikl)
 450  format(a6,3x,'  I+S =',f11.4,'(',i2,') +',f11.4,' =',
     .                       f11.4,'(',i2,')',
     .             '  drr =', f7.4,
     .             '  dsn =', f7.4,
     .             '  Buf =', f7.4)
 
      return
      end
