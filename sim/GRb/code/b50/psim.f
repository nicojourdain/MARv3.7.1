 
 
      function psim(zetaIm)
C +
C +------------------------------------------------------------------------+
C | MAR TURBULENCE (ASL)                                    9-10-2001  MAR |
C |   Function psim is the Integrated Universal Function for Momentum      |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C | OPTIONS:Duynkerke,MWR 119,                         324--341, 1991 (#DU |
C | ^^^^^^^ Businger, Workshop on Micrometeorology AMS, 67--100, 1973 (#BU |
C |         Dyer,     BLM 7,                           363--372, 1974 (#DR |
C |         Noihlan,  EERM Internal Note No 171,                 1986 (#NO |
C |                                                                        |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
      include 'MARphy.inc'
C +
      real     psim
      real     zetaIm
      real     r6p1  ,beta  ,x
C +
      data     r6p1/6.25e0/
c #DR data     beta/5.00e0/
c #BU data     beta/4.70e0/
C +
C +--Stability
C +  ~~~~~~~~~
      IF  (zetaIm.gt.0.0)                                          THEN
           psim=-6.0*zetaIm
C +...     Bintanja 1997, Annals of Glaciology
C +
c #DU      psim=-exp(0.8d0*log(unun+r6p1*zetaIm))
c #NO   IF(zetaIm.lt.1.d0)                                          THEN
c #DR      psim=-5.0*zetaIm
c #BU      psim=-4.7*zetaIm
c #NO   ELSE
C +
C +--Strong Stability
C +  ~~~~~~~~~~~~~~~~
c #NO      psim=-beta*(1.0+log(zetaIm))
c #NO   END IF
      ELSE
C +
C +--Unstability
C +  ~~~~~~~~~~~
           x   = sqrt(sqrt(1.0-20.0*zetaIm))
c #DR      x   = sqrt(sqrt(1.0-16.0*zetaIm))
c #BU      x   = sqrt(sqrt(1.0-15.0*zetaIm))
           psim= 2.0*log(demi*(unun+x)) +log(demi*(unun+x*x))
     .          -2.0*atan(x) +demi*pi
      END IF
C +
      return
      end
