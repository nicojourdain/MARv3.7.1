      function phih(zeta_h)
C +
C +------------------------------------------------------------------------+
C | MAR TURBULENCE (ASL)                                   27-09-2001  MAR |
C |   Function phih is the            Universal Function for Heat          |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C | OPTIONS:Duynkerke,MWR 119,                         324--341, 1991 (#DU |
C | ^^^^^^^ Businger, Workshop on Micrometeorology AMS, 67--100, 1973 (#BU |
C |         Dyer,     BLM 7,                           363--372, 1974 (#DR |
C |         Noihlan,  EERM Internal Note No 171,                 1986 (#NO |
C |                                                                        |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
      include 'MARphy.inc'
C +
      real     phih
      real     zeta_h
      real     r9p3
C +
      data     r9p3/9.375e0/
C +
C +--Stability
C +  ~~~~~~~~~
      IF  (zeta_h.gt.eps9)                                          THEN
           phih=1.0+6.0*zeta_h
C +...     Bintanja 1997, Annals of Glaciology
C +
c #DU      phih=1.0+7.5*zeta_h/exp(0.20*log(unun+r9p3*zeta_h))
c #NO   IF(zeta_h.lt.1.0)                                           THEN
c #DR      phih=1.0+5.0*zeta_h
c #BU      phih=.74+4.7*zeta_h
c #NO   ELSE
C +
C +--Strong Stability
C +  ~~~~~~~~~~~~~~~~
c #NO      phih=    5.44
c #NO   END IF
      ELSE
C +
C +--Unstability
C +  ~~~~~~~~~~~
           phih=1.0/sqrt(1.0-15.*zeta_h)
c #DR      phih=1.0/sqrt(1.0-16.*zeta_h)
c #BU      phih=.74/sqrt(1.0- 9.*zeta_h)
      END IF
C +
      return
      end
