 
 
      subroutine DYNqqm(specQt,eval_s,eval_p)
 
!--------------------------------------------------------------------------+
!   MAR DYNAMICS   SLOW                                Wed 11-12-2009  MAR |
!                                                                          |
!     subroutine DYNqqm restaures Water Mass                               |
!                                                                          |
!--------------------------------------------------------------------------+
 
      IMPLICIT  NONE
 
 
!  Global Variables
!  ================
 
      include  "MARphy.inc"
      include  "MARdim.inc"
      include  "MARgrd.inc"
      include  "MAR_GE.inc"
      include  "MAR_DY.inc"
      include  "MAR_FI.inc"
      include  "MARqqm.inc"
      include  "MAR_WK.inc"
 
      logical           RetroD
      logical           Set_MM
      logical           logqqm
      common/DYNqqm_log/logqqm
      real              specQt(mx,my,mz)
      character*3       eval_s
      character*6       eval_p
 
 
!  Local  Variables
!  ================
 
      character*6       eval0p
      common/DYNqqm_cha/eval0p
 
      real              fac_mm    ,summmm    ,sum_mm    ,countr
      real              sumnew    ,qqnmin(mz),qqnmax(mz)
      real              sumbak(mz),qqxmin(mz),qqxmax(mz)
     .                 ,qqnFil(mx,my,mz)
      common/DYNqqm_rea/sumbak    ,qqxmin    ,qqxmax
     .                 ,qqnFil
 
      data              RetroD/.false./
      data              Set_MM/.false./
      data              logqqm/.false./
 
 
!  Conservation Constraint Boundary
!  ================================
 
      IF (.not.logqqm)                                              THEN
               logqqm=.true.
        write(6,6) lb
 6      format(/,'*** DYNqqm: lb =',i3,' ***',/,'    ******',/)
      END IF
 
 
!  Retro-diffusion (Before Process)
!  ================================
 
      IF (RetroD)                                                   THEN
        DO   k=1,mz
        DO   j=1,my
        DO   i=1,mx
          WKxyz1(i,j,k) =           (FIsloQ       /FIslou)
     .   *FIkhmn*(specQt(i-1,j,k)-4.*specQt(i,j,k)+specQt(i+1,j,k)
     .           +specQt(i,j-1,k)                 +specQt(i,j+1,k))
     .   *dtx/dx
        END DO
        END DO
        END DO
        DO   k=1,mz
        DO   j=1,my
        DO   i=1,mx
          specQt(i,j,k) =            specQt(i,j,k)-WKxyz1(i,j,k)
          WKxyz1(i,j,k) =         0.
        END DO
        END DO
        END DO
      END IF
 
 
!  Mass Evaluation (Before Process)
!  ================================
 
      IF   (eval_s.EQ.'BAK')                                        THEN
            eval0p    = eval_p
        DO   k=1,mz
            qqxmax(k) = 0.0
            qqxmin(k) = 1.e20
          DO j=1,my
          DO i=1,mx
            qqnFil(i,j,k) =         specQt(i,j,k)   *pstDYn(i,j)
     .                            /(SFm_DY(i,j)     *SFm_DY(i,j))
            qqxmax(k) =         max(qqxmax(k)       ,qqnFil(i,j,k))
            qqxmin(k) =         min(qqxmin(k)       ,qqnFil(i,j,k))
          END DO
          END DO
        END DO
 
       IF  (FIBord)                                                 THEN
        DO   k=1,mz
            sumbak(k) = 0.0
          DO j=lgy,ldy
          DO i=lgx,ldx
            sumbak(k) =             sumbak(k)       +qqnFil(i,j,k)
          END DO
          END DO
        END DO
 
        IF (eval_p(1:3).EQ.'FIL')                                   THEN
 
          DO k=1,mz
          DO j=lgy ,ldy
            sumbak(k) = sumbak(k) + dtx
     .           *(FIkhmn*FacFIk  *(specQt(lgx1,j,k)*pstDYn(lgx1,j)
     .                             -specQt(lgx ,j,k)*pstDYn(lgx ,j)
     .                             +specQt(ldx1,j,k)*pstDYn(ldx1,j)
     .                             -specQt(ldx ,j,k)*pstDYn(ldx ,j)))/dx
          ENDDO
 
          DO i=lgx ,ldx
            sumbak(k) = sumbak(k) + dtx
     .           *(FIkhmn*FacFIk  *(specQt(i,lgy1,k)*pstDYn(i,lgy1)
     .                             -specQt(i,lgy ,k)*pstDYn(i,lgy )
     .                             +specQt(i,ldy1,k)*pstDYn(i,ldy1)
     .                             -specQt(i,ldy ,k)*pstDYn(i,ldy )))/dx
          ENDDO
          ENDDO
 
        END IF
 
       ELSE
 
          DO k=1,mz
            sumbak(k) = 0.0
          DO j=1,my
          DO i=1,mx
            sumbak(k) = sumbak(k) + qqnFil(i,j,k)
          END DO
          END DO
          END DO
 
       END IF
 
 
!  Mass Reset      (After  Process)
!  ================================
 
      ELSE IF (eval_s.EQ.'SET' )                                    THEN
        IF    (eval_p.NE.eval0p)                                    THEN
          write(6,6010) eval_p,eval0p
 6010     format('Problem in Mass Reset, Process',a7,' .NE. ',a6)
          STOP
        END IF
 
        DO   k=1,mz
            qqnmax(k) = 0.0
            qqnmin(k) = 1.e20
          DO j=1,my
          DO i=1,mx
            WKxyz1(i,j,k) =         specQt(i,j,k)   *pstDYn(i,j)
     .                            /(SFm_DY(i,j)     *SFm_DY(i,j))
          END DO
          END DO
        END DO
 
c #VQ       summmm =  0.0
        IF (FIBord)                                                 THEN
          DO k=1,mz
            sumnew  = 0.0
          DO j=lgy,ldy
          DO i=lgx,ldx
            sumnew = WKxyz1(i,j,k)               +           sumnew
          END DO
          END DO
            sumnew =                                max(eps9,sumnew)
            countr =                         (ldx-lgx+1)*(ldy-lgy+1)
            fac_mm =                               sumbak(k)/sumnew
c #VQ       summmm = summmm       +dsigm1(k)     * fac_mm
          DO j=1,my
          DO i=1,mx
            specQt(i,j,k) =        specQt(i,j,k) * fac_mm
            WKxyz1(i,j,k) =        WKxyz1(i,j,k) * fac_mm
            qqnmax(k) =        max(WKxyz1(i,j,k) , qqnmax(k))
            qqnmin(k) =        min(WKxyz1(i,j,k) , qqnmin(k))
          END DO
          END DO
            sumbak(k) = sumbak(k) / countr
          END DO
        ELSE
          DO k=1,mz
            sumnew  = 0.0
          DO j=1,my
          DO i=1,mx
            sumnew = WKxyz1(i,j,k)               +           sumnew
          END DO
          END DO
            sumnew =                                max(eps9,sumnew)
            countr =                                 mx *        my
            fac_mm =                               sumbak(k)/sumnew
c #VQ       summmm = summmm       +dsigm1(k)     * fac_mm
          DO j=1,my
          DO i=1,mx
            specQt(i,j,k) =        specQt(i,j,k) * fac_mm
            WKxyz1(i,j,k) =        WKxyz1(i,j,k) * fac_mm
            qqnmax(k) =        max(WKxyz1(i,j,k) , qqnmax(k))
            qqnmin(k) =        min(WKxyz1(i,j,k) , qqnmin(k))
          END DO
          END DO
            sumbak(k) = sumbak(k) / countr
          END DO
        ENDIF
 
 
!  Maximorum/Minimorum RESET (water vapor only)
!  ============================================
 
        IF (eval_p.EQ.'FIL_Qv'.AND.Set_MM)                          THEN
c #VQ       sum_mm = 0.
          DO k=1,mz
            fac_mm =     (qqxmax(k)-sumbak(k))
     .         /(max(epsi,qqnmax(k)-sumbak(k)))
            fac_mm = min((sumbak(k)- sigma(k) *103.5*epsq)
     .                  /(sumbak(k)-qqnmin(k)),fac_mm)
            fac_mm = max( 1.0                 ,fac_mm)
 
c #VQ       sum_mm = sum_mm       + dsigm1(k) *fac_mm
 
          DO j=1,my
          DO i=1,mx
            WKxyz1(i,j,k)=sumbak(k)+fac_mm*(WKxyz1(i,j,k)-sumbak(k))
            specQt(i,j,k)=WKxyz1(i,j,k)   * SFm_DY(i,j)  *SFm_DY(i,j)
     .                                    / pstDYn(i,j)
          END DO
          END DO
          END DO
 
 
!  Output of Statistics
!  ====================
 
c #VQ         IF (mod(jhurGE,3).EQ.0.AND.minuGE.EQ.0.AND.jsecGE.EQ.0)
c #VQ.          write(24,240)
 240            format(21x, 'RESTORE MASS ... EXTREMA')
c #VQ           write(24,241)jdarGE,labmGE(mmarGE),iyrrGE
c #VQ.                      ,jhurGE,minuGE,jsecGE
c #VQ.                      ,summmm,sum_mm
 241            format(i3,'-',a3,'-',i4,i3,'h',i2,':',i2,2f12.6)
 
        END IF
 
 
!  Work Array(s) reset
!  ===================
 
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
            WKxyz1(i,j,k) = 0.
          END DO
          END DO
        END DO
 
      ELSE
          write(6,6020) eval_s
 6020     format('Problem in Mass Reset, Type   ',a4)
          STOP
      END if
 
      return
      end
