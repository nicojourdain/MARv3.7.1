 
 
      subroutine INIphy
 
C +------------------------------------------------------------------------+
C | MAR INPUT      ATMOS                              Thu 21-Jul-2011  MAR |
C |   SubRoutine INIphy Initializes coupling Variables  between            |
C |                                 MAR Surface and     Atmosphere         |
C |                     Calls       MAR Surface Model initializing Routines|
C |                     Initializes MAR Cloud   Microphysical      Scheme  |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |    INPUT:      itexpe: Experiment Iteration Counter                    |
C |    ^^^^^       micphy: Cloud Microphysics   Switch                     |
C |                fxlead: Lead (in Sea ice)    Fraction                   |
C |                polmod: Polynya Model        Switch                     |
C |                snomod: Snow    Model        Switch                     |
C |                reaVAR: Previous OR Large Scale Variables Switch        |
C |                reaLBC: LBC:        Large Scale Variables Switch        |
C |                                                                        |
C |    INPUT (via common block)                                            |
C |    ^^^^^       pstDY (mx,my)    : Atmosphere        Thickness    [kPa] |
C |    (itexpe=0): TairSL(mx,my)    : Surface Air       Temperature    [K] |
C |                                  (Sounding Extrapolated to the Surface |
C |                                   - dtagSL                            )|
C |    (itexpe=1): pktaDY(mx,my,mzz): Reduced Potential Temperature        |
C |                sh    (mx,my)    : Surface           Elevation      [m] |
C |                                                                        |
C |   OUTPUT (via common block)                                            |
C |   ^^^^^^       tsrfSL(mx,my,mw) : Surface           Temperature    [K] |
C |    (itexpe=1): TairSL(mx,my)    : Surface Air       Temperature    [K] |
C |               (CAUTION: CRUDE REINITIALISATION                         |
C |                         BE CAREFULL WHEN USING PRESCRIBED TEMPERATURE) |
C |                SLsrfl(mx,my,mw) : Surface Type Area                    |
C |    (itexpe=0): pktaDY(mx,my,mzz): Reduced Potential Temperature        |
C |                gplvDY(mx,my,mzz): Geopotential                 [m2/s2] |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_DY.inc'
 
      include 'MAR_LB.inc'
 
      include 'MAR_HY.inc'
      include 'MAR_CA.inc'
c #BS include 'MAR_BS.inc'
 
      include 'MAR_SL.inc'
c #AO include 'MAR_AO.inc'     !cpl
      include 'MAR_SV.inc'
      include 'MAR_TV.inc'
 
 
C +--Local  Variables
C +  ================
 
      integer n
      real    tsrf  ,rowat2
 
 
C +--First Guess Initialization
C +  ==========================
 
      IF (.not.reaVAR)                                            THEN
 
 
C +--Surface Geopotential
C +  --------------------
 
        DO j=1,my
        DO i=1,mx
          gplvDY(i,j,mzz) = sh(i,j)     *   gravit
        END DO
        END DO
 
 
C +--Grid BOXES Area     (First Guess)
C +  ---------------------------------
 
        DO j=1,my
        DO i=1,mx
          SLsrfl(i,j,1) = 1.0
          SLsrfl(i,j,2) = 0.0
        END DO
        END DO
 
 
C +--Surface Temperature (First Guess)
C +  ---------------------------------
 
        DO n=1,mw
        DO j=1,my
        DO i=1,mx
            tsrfSL(i,j,n) =     TairSL(i,j)
        END DO
        END DO
        END DO
 
        DO j=1,my
        DO i=1,mx
 
          go to (101,102,103,104,105) isolSL(i,j)
 101      CONTINUE
          IF   (reaLBC)                                           THEN
                tsrfSL(i,j,1) =       sst_LB(i,j)
          ELSE
                tsrfSL(i,j,1) =   max(sst_SL       ,tfrwat)
          END IF
          go to 106
 102      CONTINUE
c #OM           tsrf          =       tsrfSL(i,j,1)
                tsrfSL(i,j,1) =   min(tsrfSL(i,j,1),TfSnow)
c #OM           tsrfSL(i,j,1) =       tsrf
                tsrfSL(i,j,2) =       sst_SL
          go to 106
 103      CONTINUE
                tsrfSL(i,j,1) =   min(tsrfSL(i,j,1),TfSnow)
          go to 106
 104      CONTINUE
          go to 106
 105      CONTINUE
          go to 106
 
 106      CONTINUE
 
        END DO
        END DO
 
      END IF
 
 
C +--Polynya Model
C +  =============
 
 
C +--Polynya Model Initialisation
C +  ----------------------------
 
C +   ***************
c #PO call SRFini_pol
C +   ***************
 
 
C +--Snow    Model
C +  =============
 
              rowat2     = ro_Wat**1.88
      DO j=1,my
      DO i=1,mx
              ro_SL0(i,j)=(d1_SL(i,j) / sqrt(csnow*cdice*cs2SL/rowat2))
     .                  **(1.0/1.44)
              ro_SL(i,j) = ro_SL0(i,j)
              SaltSL(i,j)= 1.e2
C +...        SaltSL: Here an impossible Value
C +                  (Preclude Saltation)
      END DO
      END DO
 
      IF   (.not.snomod)                                          THEN
 
        DO j=1,my
        DO i=1,mx
          IF (isolSL(i,j).ge.2.and.tsrfSL(i,j,1).lt.TfSnow)       THEN
               ro_SL(i,j) = 0.00
              SaltSL(i,j) = 0.38
C +...        SaltSL: Threshold Friction Velocity for Blowing Snow
C +                  (Budd et al., 1966, Byrd Snow Project)
          END IF
        END DO
        END DO
 
      ELSE
 
C +                       ***************
c #sn   IF (.NOT.VSISVAT) call SRFini_sno
C +                       ***************	
 
      END IF
 
 
C +--Soil Model
C +  ==========
 
      DO j=1,my
      DO i=1,mx
          roseSL(i,j) =      0.0
          hmelSL(i,j) =      0.0
      END DO
      END DO
 
      DO j=1,my
      DO i=1,mx
      GO TO (51,51,53,54,51) isolSL(i,j)
 51   CONTINUE
           t2_SL(i,j) =      tsrfSL(i,j,1)
      GO TO 59
 53   CONTINUE
        if (.not.snomod) then
           t2_SL(i,j) =      tsrfSL(i,j,1)
        end if
      GO TO 59
 54   CONTINUE
           t2_SL(i,j) =      tsrfSL(i,j,1)
           w2_SL(i,j) =      w20SL
           wg_SL(i,j) =      wg0SL
           wk_SL(i,j) =      wk0SL
           wx_SL(i,j) =      wx0SL
      GO TO 59
 59   CONTINUE
      END DO
      END DO
 
 
C +--Grid BOXES Area     (Update)
C +  ============================
 
      IF (VSISVAT)                                                THEN
        DO j=1,my
        DO i=1,mx
              nSLsrf(i,j)   =                         nvx
        END DO
        END DO
        DO n=1,nvx
        DO j=1,my
        DO i=1,mx
              SLsrfl(i,j,n) =         ifraTV(i,j,n)
              SLsrfl(i,j,n) =         SLsrfl(i,j,n) * 0.01
        END DO
        END DO
        END DO
      ELSE
        DO j=1,my
        DO i=1,mx
          IF (isolSL(i,j).le.4)                                   THEN
              nSLsrf(i,j)   = max(iun,nSLsrf(i,j)  )
              SLsrfl(i,j,1) =     1. -SLsrfl(i,j,2)
          ELSE
              nSLsrf(i,j)   =     1
              SLsrfl(i,j,1) =     1.
              nSLsrf(i,j)   =                         nvx
            DO n=1,nvx
              SLsrfl(i,j,n) =         ifraTV(i,j,n)
              SLsrfl(i,j,n) =         SLsrfl(i,j,n) * 0.01
            END DO
          END IF
        END DO
        END DO
      END IF
 
 
C +--Surface Temperature (Update)
C +  ============================
 
      DO j=1,my
      DO i=1,mx
          TairSL(i,j)     = 0.
        DO n=1,mw
          TairSL(i,j)     = TairSL(i,j)
     .   +SLsrfl(i,j,n)   * tsrfSL(i,j,n)
        END DO
          pktaDY(i,j,mzz) = TairSL(i,j)
     .                   / ((pstDY(i,j)+ptopDY)**cap)
      END DO
      END DO
 
 
C +--Microphysics
C +  ============
 
      IF (micphy)                                                 THEN
          turnHY = .false.
 
          DO k=1,mz
          DO j=1,my
          DO i=1,mx
              qiHY(i,j,k) = 0.0
              qsHY(i,j,k) = 0.0
! #qg         qgHY(i,j,k) = 0.0
              qwHY(i,j,k) = 0.0
              qrHY(i,j,k) = 0.0
            hlatHY(i,j,k) = 0.0
          END DO
          END DO
          END DO
 
          DO j=1,my
          DO i=1,mx
            rainHY(i,j)   = 0.0
            snowHY(i,j)   = 0.0
            crysHY(i,j)   = 0.0
            rainCA(i,j)   = 0.0
            snowCA(i,j)   = 0.0
          END DO
          END DO
 
      END IF
 
      return
      end
