 
 
      subroutine VgOptP
 
C +------------------------------------------------------------------------+
C | MAR/SISVAT   VgOptP                                     8-04-2009  MAR |
C |   SubRoutine VgOptP computes the Canopy    optical Properties          |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   PARAMETERS:  klonv: Total Number of columns =                        |
C |   ^^^^^^^^^^        = Total Number of continental     Grid Boxes       |
C |                     X       Number of Mosaic Cell per Grid Box         |
C |                                                                        |
C |   INPUT:   ivgtSV   = 0,...,12:   Vegetation Type                      |
C |   ^^^^^               0:          Water, Solid or Liquid               |
C |                                                                        |
C |   INPUT:   coszSV   : Cosine of the Sun Zenithal Distance          [-] |
C |   ^^^^^    sol_SV   : Surface Downward  Solar   Radiation       [W/m2] |
C |            snCaSV   : Canopy     Snow      Thickness         [mm w.e.] |
C |                                                                        |
C |            LAI_sv   : Leaf Area  Index      (snow included)        [-] |
C |            glf_sv   : Green Leaf Fraction of NOT fallen Leaves     [-] |
C |            albisv   : Snow/Ice/Water/Soil Integrated Albedo        [-] |
C |                                                                        |
C |   OUTPUT:  alb_SV   : Surface-Canopy Albedo                        [-] |
C |   ^^^^^^   SoCasv   : Absorbed Solar Radiation by Canopy (Normaliz)[-] |
C |            SoSosv   : Absorbed Solar Radiation by Surfac (Normaliz)[-] |
C |            LAIesv   : Effective Leaf Area  Index for Transpiration [-] |
C |                                                                        |
C |   Internal Variables: Normalized Values:                               |
C |   ^^^^^^^^^^^^^^^^^^                                                   |
C |            u0_Vis   : Upward   Visible Radiation at Top Canopy     [-] |
C |            absg_V   : Absorbed Visible Radiation by the Ground     [-] |
C |            absv_V   : Absorbed Visible Radiation by the Canopy     [-] |
C |            u0_nIR   : Upward   Near IR Radiation at Top Canopy     [-] |
C |            absgnI   : Absorbed Near IR Radiation by the Ground     [-] |
C |            absv_V   : Absorbed Near IR Radiation by the Canopy     [-] |
C |                                                                        |
C |   REFERENCE:   De Ridder, 1997, unpublished thesis, chapter 2 (DR97,2) |
C |   ^^^^^^^^^                                                            |
C |                                                                        |
C |   ASSUMPTIONS: Leaf Inclination Index chi_l (eqn2.49 DR97) set to zero |
C |   ^^^^^^^^^^^                         for all vegetation types         |
C |                Radiation Fluxes are normalized                         |
C |                      with respect to incoming solar radiation (=I0+D0) |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include  "MARphy.inc"
      include  "MAR_SV.inc"
 
      include  "MARxSV.inc"
      include  "MARySV.inc"
!$OMP threadprivate(/xSISVAT_I/,/xSISVAT_R/,/ySISVAT_I/,/ySISVAT_R/)
 
C +--Internal Variables
C +  ==================
 
 
      integer   ikl   ,kri
 
      real      exdRad,k_drad,k___sv(klonv)
      real      e_prad,e1pRad
      real      zv_fac,zv1fac,deadLF
      real      T_Rad0,A_Rad0,A0__sv(klonv)
      real                    r0_Rad,t0_Rad,nu_Rad
      real      Tr_Rad,Re_Rad,r__Rad,t__Rad,t1_Rad
      real      arggam, gamma,gamasv(klonv),gammaL
      real      denSig,Sig__c,Sigcsv(klonv)
      real      DDifH1,DDifC1,C1__sv(klonv)
      real      DDifH2,DDifC2,C2__sv(klonv)
      real      denS_s,denS_a,den_c1,DDif_L
      real      u0_Vis,absg_V,absv_V
      real      u0_nIR,absgnI,absvnI
      real      argexg,argexk,criLAI(klonv)
      real      residu,d_DDif,dDDifs,dDDifa
 
 
C +--Internal DATA
C +  =============
 
      integer            nvgt
      parameter         (nvgt=12)
      real      reVisL(0:nvgt)      ! Reflectivity  / Visible / Live Leaves
      real      renIRL(0:nvgt)      ! Reflectivity  / Near IR / Live Leaves
      real      trVisL(0:nvgt)      ! Transmitivity / Visible / Live Leaves
      real      trnIRL(0:nvgt)      ! Transmitivity / Near IR / Live Leaves
      real      reVisD(0:nvgt)      ! Reflectivity  / Visible / Dead Leaves
      real      renIRD(0:nvgt)      ! Reflectivity  / Near IR / Dead Leaves
      real      trVisD(0:nvgt)      ! Transmitivity / Visible / Dead Leaves
      real      trnIRD(0:nvgt)      ! Transmitivity / Near IR / Dead Leaves
 
      real      reVisS              ! Reflectivity  / Visible / Canopy Snow
      real      renIRS              ! Reflectivity  / Near IR / Canopy Snow
      real      trVisS              ! Transmitivity / Visible / Canopy Snow
      real      trnIRS              ! Transmitivity / Near IR / Canopy Snow
 
      real      snCaMx              ! Canopy Snow Thickness for having Snow
                                    ! Snow Reflectivity and Transmitivity
      real      CriStR              ! Critical Radiation Stomatal Resistance
      real      alb_SV_old
 
      integer            ivg
 
      DATA (reVisL(ivg),renIRL(ivg),trVisL(ivg),trnIRL(ivg),
     .      reVisD(ivg),renIRD(ivg),trVisD(ivg),trnIRD(ivg),ivg=0,nvgt)
 
C +   reVisL renIRL trVisL trnIRL reVisD renIRD trVisD trnIRD  SVAT     CLASSES
C +   ------ ------ ------ ------ ------ ------ ------ ------+ ----------------
     ./0.11,  0.58,  0.07,  0.25,  0.36,  0.58,  0.22,  0.38,!  0 NO VEGETATION
     . 0.11,  0.58,  0.07,  0.25,  0.36,  0.58,  0.22,  0.38,!  1 CROPS LOW
     . 0.11,  0.58,  0.07,  0.25,  0.36,  0.58,  0.22,  0.38,!  2 CROPS MEDIUM
     . 0.11,  0.58,  0.07,  0.25,  0.36,  0.58,  0.22,  0.38,!  3 CROPS HIGH
     . 0.11,  0.58,  0.07,  0.25,  0.36,  0.58,  0.22,  0.38,!  4 GRASS LOW
     . 0.11,  0.58,  0.07,  0.25,  0.36,  0.58,  0.22,  0.38,!  5 GRASS MEDIUM
     . 0.11,  0.58,  0.07,  0.25,  0.36,  0.58,  0.22,  0.38,!  6 GRASS HIGH
     . 0.10,  0.45,  0.05,  0.25,  0.16,  0.39,  0.01,  0.01,!  7 BROADL LOW
     . 0.10,  0.45,  0.05,  0.25,  0.16,  0.39,  0.01,  0.01,!  8 BROADL MEDIUM
     . 0.10,  0.45,  0.05,  0.25,  0.16,  0.39,  0.01,  0.01,!  9 BROADL HIGH
     . 0.07,  0.35,  0.05,  0.10,  0.10,  0.39,  0.01,  0.01,! 10 NEEDLE LOW
     . 0.07,  0.35,  0.05,  0.10,  0.10,  0.39,  0.01,  0.01,! 11 NEEDLE MEDIUM
     . 0.07,  0.35,  0.05,  0.10,  0.10,  0.39,  0.01,  0.01/! 12 NEEDLE HIGH
 
      DATA
     .reVisS,renIRS,trVisS,trnIRS
C +   ------ ------ ------ ------+
     ./0.85,  0.85,  0.00,  0.00/!
C +   REMARK: Possible Refinement by taking actual Surface Snow Reflectivities
C +   ^^^^^^
 
      DATA      snCaMx /0.5/
 
      DATA      CriStR /25./
 
 
C +--General Parameters, Solar Radiation Absorption
C +  ==============================================
 
      DO ikl=1,klonv
 
c #sv   IF (ifraSV(ikl).GT.0)                                     THEN
 
            k_dRad = 0.5 /max(coszSV(ikl),epsi) ! absorbed irradiance fraction
            e_pRad = 2.5   *  coszSV(ikl)       ! exponential argument,
C +                                             ! V/nIR radiation partitioning,
C +                                             ! DR97, 2, eqn (2.53) & (2.54)
            exdRad =    exp(-k_dRad*LAI_sv(ikl))! exponential, Irradi. Absorpt.
            e1pRad = 1.-exp(-e_pRad)            ! exponential, V/nIR Rad. Part.
 
            ivg    =                ivgtSV(ikl) ! Vegetation Type
            zv_fac =    min( snCaSV(ikl)/snCaMx ! Contribution of Snow to Leaf
     .                      ,  unun)            ! Reflectivity and Transmissiv.
            zv1fac = 1.     -       zv_fac      !
            deadLF = 1.     -       glf_sv(ikl) ! Dead Leaf Fraction
 
 
C +--Visible Part of the Solar Radiation Spectrum (V,   0.4--0.7mi.m)
C +  ================================================================
 
            A_Rad0 =      0.25 + 0.697 * e1pRad ! Absorbed    Vis. Radiation
            T_Rad0 = 1. - A_Rad0                ! Transmitted Vis  Radiation
 
C +--Reflectivity, Transmissivity
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            Re_Rad = glf_sv(ikl) *ReVisL(ivg)
     .             + deadLF      *ReVisD(ivg)
            Tr_Rad = glf_sv(ikl) *TrVisL(ivg)
     .             + deadLF      *TrVisD(ivg)
 
C +--Adaptation to Snow
C +  ^^^^^^^^^^^^^^^^^^
            Re_Rad = zv1fac      *Re_Rad      + zv_fac *reVisS
            Tr_Rad = zv1fac      *Tr_Rad      + zv_fac *trVisS
 
C +--Scattering /DR97, 2, eqn (2.26) and (2.27)        ! Diffuse  Radiation:
C +  ^^^^^^^^^^                                        ! ^^^^^^^^^^^^^^^^^^
            r__Rad = (2. *Re_Rad +     Tr_Rad) / 3.    ! Upw.  Scatter.Fract.
            t__Rad = (    Re_Rad + 2. *Tr_Rad) / 3.    ! Downw.Scatter.Fract.
 
            t1_Rad =  1. -t__Rad                       !
            arggam =      t1_Rad*t1_Rad-r__Rad*r__Rad  !
            arggam =  max(arggam,zero)                 !
            gamma  = sqrt(arggam)                      ! eqn (2.39)
            gammaL =  min( gamma*LAI_sv(ikl),40.0)     !
            DDifH1 =  exp( gammaL           )          ! Downw.Diffus.Solut.1
            DDifH2 =  exp(-gammaL           )          ! Downw.Diffus.Solut.2
C +         REMARK:  These 2 contributions are zero in case of 0 Reflectivity
C +         ^^^^^^
 
C +--Scattering /DR97, 2, eqn (2.19) and (2.20)        ! Direct   Radiation:
C +  ^^^^^^^^^^                                        ! ^^^^^^^^^^^^^^^^^^
            r0_Rad = 0.5 *((Re_Rad+Tr_Rad) *k_dRad     ! Upw.  Scatter.Fract.
     .                    +(Re_Rad-Tr_Rad) /    3.)    !
            t0_Rad = 0.5 *((Re_Rad+Tr_Rad) *k_dRad     ! Downw.Scatter.Fract.
     .                    -(Re_Rad-Tr_Rad) /    3.)    !
 
            nu_Rad = t1_Rad-r__Rad*albisv(ikl)         ! nu coeff., eqn 2.43
            den_c1 =  gamma*(DDifH1+DDifH2)            ! eqn (2.43) Denomin.
     .              +nu_Rad*(DDifH1-DDifH2)            !(Constant for DDifH1)
 
            denSig =  gamma*gamma - k_dRad*k_dRad      ! eqn (2.40) Denomin.
            denS_s = sign(unun,denSig)                 !
            denS_a =  abs(     denSig)                 !
            denSig =  max(epsi,denS_a) * denS_s        !
            Sig__c = (r__Rad* r0_Rad                   ! sigma_c, eqn (2.40)
     .               +t0_Rad*(k_dRad+t1_Rad)) / denSig !
 
            DDifC1 = ((gamma-nu_Rad)*(T_Rad0-Sig__c*A_Rad0)*DDifH2
     .             +((k_dRad-nu_Rad)* Sig__c
     .               +t0_Rad+r__Rad * albisv(ikl)) *A_Rad0 *exdRad)
     .           /max(den_c1,epsi)
            DDifC2 =  T_Rad0        - DDifC1-Sig__c*A_Rad0
 
C +--Visible Diffuse Fluxes
C +  ^^^^^^^^^^^^^^^^^^^^^^
            DDif_L =  DDifC1*DDifH1 + DDifC2*DDifH2    ! DOWNward,
     .             +  Sig__c*A_Rad0 *exdRad            ! Canopy Basis
            u0_Vis = ((gamma+t1_Rad)*DDifC1            ! UPward
     .               -(gamma-t1_Rad)*DDifC2            ! Canopy Top
     .             -((k_dRad-t1_Rad)*Sig__c            !
     .               +t0_Rad               )*A_Rad0)   !
     .          / max(r__Rad,epsi)                     !
            u0_Vis = min(0.99,max(epsi,u0_Vis))        ! ERROR
            absg_V = (1.-albisv(ikl))*(A_Rad0*exdRad   ! Ground Absorption
     .                                +DDif_L       )  !
            absv_V = (1.-u0_Vis     )- absg_V          ! Veget. Absorption
 
C +--Parameters for Computing Effective LAI for Transpiration
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            gamasv(ikl) = gamma
            C1__sv(ikl) = DDifC1
            C2__sv(ikl) = DDifC2
            Sigcsv(ikl) = Sig__c
            k___sv(ikl) = k_dRad
            A0__sv(ikl) = A_Rad0
 
 
C +--Near-IR Part of the Solar Radiation Spectrum (nIR, 0.7--2.8mi.m)
C +  ================================================================
 
            A_Rad0 =      0.80 + 0.185 * e1pRad ! Absorbed    nIR. Radiation
            T_Rad0 = 1. - A_Rad0                ! Transmitted nIR  Radiation
 
C +--Reflectivity, Transmissivity
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            Re_Rad = glf_sv(ikl) *RenIRL(ivg)
     .             + deadLF      *RenIRD(ivg)
            Tr_Rad = glf_sv(ikl) *TrnIRL(ivg)
     .             + deadLF      *TrnIRD(ivg)
 
C +--Adaptation to Snow
C +  ^^^^^^^^^^^^^^^^^^
            Re_Rad = zv1fac      *Re_Rad      + zv_fac *renIRS
            Tr_Rad = zv1fac      *Tr_Rad      + zv_fac *trnIRS
 
C +--Scattering /DR97, 2, eqn (2.26) and (2.27)        ! Diffuse  Radiation:
C +  ^^^^^^^^^^                                        ! ^^^^^^^^^^^^^^^^^^
            r__Rad = (2. *Re_Rad +     Tr_Rad) / 3.    ! Upw.  Scatter.Fract.
            t__Rad = (    Re_Rad + 2. *Tr_Rad) / 3.    ! Downw.Scatter.Fract.
 
            t1_Rad =  1. -t__Rad                       !
            arggam =      t1_Rad*t1_Rad-r__Rad*r__Rad  !
            arggam =  max(arggam,zero)                 !
            gamma  = sqrt(arggam)                      ! eqn (2.39)
            DDifH1 =  exp( gamma*LAI_sv(ikl))          ! Downw.Diffus.Solut.1
            DDifH2 =  exp(-gamma*LAI_sv(ikl))          ! Downw.Diffus.Solut.2
C +         REMARK:  These 2 contributions are zero in case of 0 Reflectivity
C +         ^^^^^^
 
C +--Scattering /DR97, 2, eqn (2.19) and (2.20)        ! Direct   Radiation:
C +  ^^^^^^^^^^                                        ! ^^^^^^^^^^^^^^^^^^
            r0_Rad = 0.5 *((Re_Rad+Tr_Rad) *k_dRad     ! Upw.  Scatter.Fract.
     .                    +(Re_Rad-Tr_Rad) /    3.)    !
            t0_Rad = 0.5 *((Re_Rad+Tr_Rad) *k_dRad     ! Downw.Scatter.Fract.
     .                    -(Re_Rad-Tr_Rad) /    3.)    !
 
            nu_Rad = t1_Rad-r__Rad*albisv(ikl)         ! nu coeff., eqn 2.43
            den_c1 =  gamma*(DDifH1+DDifH2)            ! eqn (2.43) Denomin.
     .              +nu_Rad*(DDifH1-DDifH2)            !(Constant for DDifH1)
 
            denSig =  gamma*gamma - k_dRad*k_dRad      ! eqn (2.40) Denomin.
            denS_s = sign(unun,denSig)                 !
            denS_a =  abs(     denSig)                 !
            denSig =  max(epsi,denS_a) * denS_s        !
            Sig__c = (r__Rad* r0_Rad                   ! sigma_c, eqn (2.40)
     .               +t0_Rad*(k_dRad+t1_Rad)) / denSig !
 
            DDifC1 = ((gamma-nu_Rad)*(T_Rad0-Sig__c*A_Rad0)*DDifH2
     .             +((k_dRad-nu_Rad)* Sig__c
     .               +t0_Rad+r__Rad * albisv(ikl)) *A_Rad0 *exdRad)
     .           /max(den_c1,epsi)
            DDifC2 =  T_Rad0        - DDifC1-Sig__c*A_Rad0
 
C +--Near IR Diffuse Fluxes
C +  ^^^^^^^^^^^^^^^^^^^^^^
            DDif_L =  DDifC1*DDifH1 + DDifC2*DDifH2    ! DOWNward,
     .             +  Sig__c*A_Rad0 *exdRad            ! Canopy Basis
            u0_nIR = ((gamma+t1_Rad)*DDifC1            ! UPward
     .               -(gamma-t1_Rad)*DDifC2            ! Canopy Top
     .             -((k_dRad-t1_Rad)*Sig__c            !
     .               +t0_Rad               )*A_Rad0)   !
     .          / max(r__Rad,epsi)                     !
            u0_nIR = min(0.99,max(epsi,u0_nIR))        ! ERROR
            absgnI = (1.-albisv(ikl))*(A_Rad0*exdRad   ! Ground Absorption
     .                                +DDif_L       )  !
            absvnI = (1.-u0_nIR     )- absgnI          ! Veget. Absorption
 
 
C +--Surface-Canopy Albedo and Normalized Solar Radiation Absorption
C +  ===============================================================
 
            alb_SV_old  = (u0_Vis+u0_nIR)*0.5d0
            alb_SV(ikl) = (u0_Vis+u0_nIR)*0.5d0
 
cXF vegetation albedo too low
            if(alb_SV_old<0.3.and.alb_SV_old>0.01
     .         .and.ivgtSV(ikl)>0)then
             alb_SV(ikl)=alb_SV_old+(0.3-alb_SV_old)/5.
             u0_nIR     =u0_nIR*alb_SV(ikl)/alb_SV_old
             u0_Vis     =u0_Vis*alb_SV(ikl)/alb_SV_old
	     absvnI     = (1.-u0_nIR     )- absgnI
             absv_V     = (1.-u0_Vis     )- absg_V
            else
             alb_SV(ikl)= (u0_Vis+u0_nIR)*0.5d0
            endif
cXF
            SoCasv(ikl) = (absv_V+absvnI)*0.5d0
            SoSosv(ikl) = (absg_V+absgnI)*0.5d0
 
c #sv   END IF
 
      END DO
 
 
C +--Effective LAI for Transpiration
C +  ===============================
 
        DO ikl=1,klonv
              criLAI(ikl) = 2.                  ! LAI for which D0_Vis > 20W/m2
C +                                             ! DR97, 2, eqn (2.57)
        END DO
 
      DO   kri=1,10
        DO ikl=1,klonv
 
c #sv     IF (ifraSV(ikl).GT.0)                                   THEN
              argexg      =  min(criLAI(ikl)*gamasv(ikl),     argmax)
              argexk      =  min(criLAI(ikl)*k___sv(ikl),     argmax)
              residu      =      C1__sv(ikl)            *exp( argexg)
     .                          +C2__sv(ikl)            *exp(-argexg)
     .                          +A0__sv(ikl)*gamasv(ikl)*exp(-argexk)
     .                          -CriStR /max(sol_SV(ikl),       epsi)
 
              d_DDif      =      C1__sv(ikl)*gamasv(ikl)*exp( argexg)
     .                          -C2__sv(ikl)*gamasv(ikl)*exp(-argexg)
     .                          -A0__sv(ikl)*k___sv(ikl)*exp(-argexk)
              dDDifs      = sign(unun,d_DDif)
              dDDifa      =  abs(     d_DDif)
              d_DDif      =  max(epsi,dDDifa) * dDDifs
 
              criLAI(ikl) =      criLAI(ikl)-residu/d_DDif
              criLAI(ikl) =  max(criLAI(ikl),zero       )
              criLAI(ikl) =  min(criLAI(ikl),LAI_sv(ikl))
c #sv     END IF
 
        END DO
      END DO
 
        DO ikl=1,klonv
              LAIesv(ikl) = criLAI(ikl) +(exp(-k___sv(ikl)*criLAI(ikl))
     .                                   -exp(-k___sv(ikl)*LAI_sv(ikl)))
     .                                  /      k___sv(ikl)
        END DO
 
      return
      end
