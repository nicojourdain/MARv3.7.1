 
 
 
      subroutine MARgau_x(i1,i2,j1,j2,k1,k2)
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS                                           17-12-2000  MAR |
C |   SubRoutine MARgau_x performs Gaussian Elimination Algorithm along x  |
C |    (e.g. Pielke (1984), pp.302--303)                                   |
C |    (needed to solve the implicit scheme developped for filtering)      |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:              i1,i2,j1,j2,k1,k2: i,j,k Loops Limits            |
C |   ^^^^^                                                                |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_WK.inc'
 
      integer i1,i2,j1,j2,k1,k2,ix
 
      data ix /0/
 
 
C +--Forward  Sweep
C +  ==============
 
      IF (ix.ne.1)                                                THEN
          DO k=k1,k2
          DO j=j1,j2
            WKxyz5(i1,j,k)= WKxyz2(i1,j,k)
            WKxyz6(i1,j,k)=-WKxyz1(i1,j,k)/WKxyz5(i1,j,k)
          END DO
          END DO
        DO i=ip1(i1),i2
          DO k=k1,k2
          DO j=j1,j2
            WKxyz5(i,j,k)= WKxyz3(i,j,k)*WKxyz6(i-1,j,k)+WKxyz2(i,j,k)
            WKxyz6(i,j,k)=-WKxyz1(i,j,k)/WKxyz5(i  ,j,k)
          END DO
          END DO
        END DO
      END IF
 
          DO k=k1,k2
          DO j=j1,j2
            WKxyz7(i1,j,k)= WKxyz4(i1,j,k)/WKxyz5(i1,j,k)
          END DO
          END DO
 
        DO i=ip1(i1),i2
          DO k=k1,k2
          DO j=j1,j2
            WKxyz7(i,j,k)=(WKxyz4(i,j,k)-WKxyz3(i,j,k)*WKxyz7(i-1,j,k))
     .                    /WKxyz5(i,j,k)
          END DO
          END DO
        END DO
 
 
C +--Backward Sweep
C +  ==============
 
        DO i=im1(i2),i1,-1
          DO k=k1,k2
          DO j=j1,j2
            WKxyz7(i,j,k)=WKxyz6(i,j,k)*WKxyz7(i+1,j,k)+WKxyz7(i,j,k)
          END DO
          END DO
        END DO
 
      return
      end
