 
 
      subroutine DYNgpo
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS   FAST                                    16-12-2000  MAR |
C |   SubRoutine DYNgpo contains the Integration of Hydrostatic Relation   |
C |                          and the Computation of Real Temperature t [K] |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT   (via common block)                                           |
C |   ^^^^^   pktaDY(mx,my,mzz): Reduced Potential Temperature             |
C |           gplvDY(mx,my,mzz): Surface Geopotential (i.e. for k=mzz)     |
C |            virDY(mx,my,mz) : Air Loading by water vapor & hydrometeors |
C |                                                                        |
C |   OUTPUT  (via common block)                                           |
C |   ^^^^^^    pkDY(mx,my,mz) : Exner Potential                           |
C |           tairDY(mx,my,mz) : Temperature                           [K] |
C |           gplvDY(mx,my,mzz):    Geopotential                   [m2/s2] |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_DY.inc'
 
      include 'MAR_WK.inc'
 
 
C +--Local  Variables
C +  ================
 
      real    ab
 
 
C +--EXNER Potential and Temperature
C +  ===============================
 
      ab=0.5*(1.0-sigmid(mz))/(1.0-sigmid(mmz1))
 
      DO k=1,mz
      DO j=1,my
      DO i=1,mx
          pkDY(i,j,k) = exp(cap *log(pstDYn(i,j)*sigma(k)+ptopDY))
        tairDY(i,j,k) = pktaDY(i,j,k) *pkDY(i,j,k)
        WKxyz1(i,j,k) = cp            *pkDY(i,j,k)
      END DO
      END DO
      END DO
 
      DO k=1,mmz1
      DO j=1,my
      DO i=1,mx
        WKxyz2(i,j,k) = cp            *pkDY(i,j,kp1(k))
      END DO
      END DO
      END DO
 
      DO j=1,my
      DO i=1,mx
        WKxyz2(i,j,mz)= cp *
     .                  exp(cap *log(pstDYn(i,j)         +ptopDY))
      END DO
      END DO
 
 
C +--Integration of the Hydrostatic Equation / Work Arrays Reset
C +  ===========================================================
 
      DO j=1,my
      DO i=1,mx
        gplvDY(i,j,mz)=gplvDY(i,j,mzz) +(WKxyz2(i,j,mz)-WKxyz1(i,j,mz))
     .      *((1.0+ab)*pktaDY(i,j,mz)  *(1.0+virDY(i,j,mz ))
     .            -ab *pktaDY(i,j,mmz1)*(1.0+virDY(i,j,mmz1)))
 
        WKxyz1(i,j,mz)=   0.0
        WKxyz2(i,j,mz)=   0.0
      END DO
      END DO
 
      DO  k=mmz1,1,-1
      DO  j=1,my
      DO  i=1,mx
        gplvDY(i,j,k)=gplvDY(i,j,kp1(k))+(WKxyz2(i,j,k )-WKxyz1(i,j,k ))
     .              *(pktaDY(i,j,k)     *(1.0+virDY(i,j,k))
     .               +pktaDY(i,j,kp1(k))*(1.0+virDY(i,j,kp1(k))))*0.5
 
        WKxyz1(i,j,k) =   0.0
        WKxyz2(i,j,k) =   0.0
      END DO
      END DO
      END DO
 
      return
      end
