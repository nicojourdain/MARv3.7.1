 
 
      subroutine relrea
 
C +------------------------------------------------------------------------+
C |   SubRoutine relrea Computes Machine Precision                         |
C |      using a LAPACK auxiliary routine                                  |
C |   (Originating from CONVEX -- Ruud VAN DER PAS)                        |
C +------------------------------------------------------------------------+
 
      include 'MARCTR.inc'
 
      character *1 cc
cXF
c Pour ifort 9.1
c     eps   = SLAMCH(cc)
      eps   = 0.1E-06
      cc    ='S'
c     sfmin = SLAMCH(cc)
      sfmin = 0.1E-36
      cc    ='U'
c     rrmin = SLAMCH(cc)
      rrmin = 0.1E-36
      cc    ='O'
c     rrmax = SLAMCH(cc)
      rrmax = 0.1E+38
cXF
      write(6,16) eps,sfmin,rrmin,rrmax
 16   format(' Precision relative  : ',e12.4,
     .     /,' Plus petit nombre   : ',e12.4,
     .     /,' Underflow Threshold = ',e12.4,
     .     /,' Overflow  Threshold = ',e12.4)
 
      return
      end
