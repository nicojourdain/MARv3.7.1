 
 
      subroutine SISVAT_TVg
! #e1.                     (ETVg_d)
 
C +------------------------------------------------------------------------+
C | MAR          SISVAT_TVg                                13-09-2003  MAR |
C |   SubRoutine SISVAT_TVg computes the Canopy Energy Balance             |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   PARAMETERS:  klonv: Total Number of columns =                        |
C |   ^^^^^^^^^^        = Total Number of continental     grid boxes       |
C |                     X       Number of Mosaic Cell per grid box         |
C |                                                                        |
C |   INPUT:   ivgtSV   = 0,...,12:   Vegetation Type                      |
C |   ^^^^^               0:          Water, Solid or Liquid               |
C |            isnoSV   = total Nb of Ice/Snow Layers                      |
C |                                                                        |
C |   INPUT:   sol_SV   : Downward Solar Radiation                  [W/m2] |
C |   ^^^^^    IRd_SV   : Surface  Downward Longwave Radiation      [W/m2] |
C |            TaT_SV   : SBL Top  Temperature                         [K] |
C |            rhT_SV   : SBL Top  Air  Density                    [kg/m3] |
C |            QaT_SV   : SBL Top  Specific  Humidity              [kg/kg] |
C |            psivSV   : Leaf     Water     Potential                 [m] |
C |            IRs_SV   : Soil     IR Flux  (previous time step)    [W/m2] |
C |            dt__SV   : Time     Step                                [s] |
C |                                                                        |
C |            SoCasv   : Absorbed Solar Radiation by Canopy (Normaliz)[-] |
C |            tau_sv   : Fraction of Radiation transmitted by Canopy  [-] |
C |            Evg_sv   : Soil+Vegetation Emissivity                   [-] |
C |            Eso_sv   : Soil+Snow       Emissivity                   [-] |
C |            rah_sv   : Aerodynamic Resistance for Heat            [s/m] |
C |            Sigmsv   : Canopy Ventilation Factor                    [-] |
C |            LAI_sv   : Leaf Area  Index                             [-] |
C |            LAIesv   : Leaf Area  Index (effective / transpiration) [-] |
C |            glf_sv   : Green Leaf Fraction of NOT fallen Leaves     [-] |
C |            rrMxsv   : Canopy Maximum Intercepted Rain          [kg/m2] |
C |                                                                        |
C |   INPUT /  TvegSV   : Canopy   Temperature                         [K] |
C |   OUTPUT:  rrCaSV   : Canopy     Water     Content             [kg/m2] |
C |   ^^^^^^                                                               |
C |                                                                        |
C |   OUTPUT:  IRv_sv   : Vegetation IR Flux                        [W/m2] |
C |   ^^^^^^   HSv_sv   : Sensible Heat Flux                        [W/m2] |
C |            HLv_sv   : Latent   Heat Flux                        [W/m2] |
C |            Evp_sv   : Evaporation                              [kg/m2] |
C |            EvT_sv   : Evapotranspiration                       [kg/m2] |
C |            ETVg_d   : Vegetation  Energy Power Forcing          [W/m2] |
C |                                                                        |
C |   Internal Variables:                                                  |
C |   ^^^^^^^^^^^^^^^^^^                                                   |
C |                                                                        |
C |   METHOD: The Newton-Raphson Scheme is preferable                      |
C |   ^^^^^^  when computing over a long time step the heat content        |
C |           of a medium having a very small or zero heat capacity.       |
C |           This is to handle strong non linearities arising             |
C |           in conjunction with rapid temperature variations.            |
C |                                                                        |
C | # OPTIONS: #NN: Newton-Raphson Increment not added in last Iteration   |
C | # ^^^^^^^                                                              |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include  "MARphy.inc"
      include  "MAR_SV.inc"
      include  "MARdSV.inc"
 
      include  "MARxSV.inc"
      include  "MARySV.inc"
!$OMP threadprivate(/xSISVAT_I/,/xSISVAT_R/,/ySISVAT_I/,/ySISVAT_R/)
 
C +--OUTPUT
C +  ------
 
! #e1 real      ETVg_d(klonv)                 ! VegetationPower, Forcing
 
C +--OUTPUT for Stand Alone NetCDF File
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #NC real          SOsoKL(klonv)             ! Absorbed Solar Radiation
c #NC real          IRsoKL(klonv)             ! Absorbed IR    Radiation
c #NC real          HSsoKL(klonv)             ! Absorbed Sensible Heat Flux
c #NC real          HLsoKL(klonv)             ! Absorbed Latent   Heat Flux
c #NC real          HLs_KL(klonv)             ! Evaporation
c #NC real          HLv_KL(klonv)             ! Transpiration
c #NC common/DumpNC/SOsoKL,IRsoKL
c #NC.             ,HSsoKL,HLsoKL
c #NC.             ,HLs_KL,HLv_KL
 
 
C +--Internal Variables
C +  ==================
 
      integer   ikl                           ! Grid Point Index
      integer   nitmax,nit                    ! Iterations Counter
      real      d_Tveg                        ! Canopy Temperat. Increment
      real      dTvMAX                        ! Canopy Temperat. Increment MAX
      real      dHvdTv                        ! Derivativ.of Canopy Energ.Budg.
      real      Hv_Tv0                        ! Imbalance of Canopy Energ.Budg.
      real      Hv_MAX                        ! MAX Imbal.of Canopy Energ.Budg.
      real      Hv_MIN                        ! MIN Imbal.of Canopy Energ.Budg.
      real      Hswich                        ! Newton-Raphson         Switch
      real      Tveg_0(klonv)                 ! Canopy Temperature, Previous t
      real      tau_Ca                        ! Canopy IR Radiation Absorption
      real      IR_net                        ! InfraRed  NET(t)
      real      dIRdTv(klonv)                 ! InfraRed  NET(t), Derivative(t)
      real      dHSdTv(klonv)                 ! Sensible Heat FL. Derivative(t)
      real      dHLdTv(klonv)                 ! Latent   Heat FL. Derivative(t)
c #HC real      dHCdTv(klonv)                 !          Heat     Storage
      real      EvFrac                        ! Condensat./Transpirat. Switch
      real      SnoMsk                        ! Canopy Snow            Switch
      real      den_qs,arg_qs,qsatvg          ! Canopy Saturat. Spec. Humidity
      real      dqs_dT                        ! d(qsatvg)/dTv
      real      FacEvp,FacEvT,Fac_Ev          ! Evapo(transpi)ration Factor
      real      dEvpdT(klonv),dEvTdT(klonv)   ! Evapo(transpi)ration Derivative
      real      F_Stom                        ! Funct.  (Leaf Water Potential)
      real      R0Stom                        ! Minimum Stomatal Resistance
      real      R_Stom                        !         Stomatal Resistance
      real      LAI_OK                        ! 1. ==>  Leaves   exist
      real      rrCaOK,snCaOK,dEvpOK          ! Positive Definiteness Correct.
 
 
C +--Internal DATA
C +  =============
 
      data      nitmax /   5   /              ! Maximum  Iterations    Number
      data      dTvMAX /   5.  /              ! Canopy Temperat. Increment MAX
      data      Hv_MIN /   0.1 /              ! MIN Imbal. of Surf.Energy Budg.
      data      SnoMsk /   0.0 /              ! Canopy Snow Switch    (Default)
 
 
C +--Newton-Raphson Scheme
C +  =====================
 
      nit    = 0
  101 CONTINUE
      nit    = nit + 1
      HV_MAX = 0.
 
 
C +--Temperature of the Previous Time Step
C +  -------------------------------------
 
        DO ikl=1,klonv
          Tveg_0(ikl) = TvegSV(ikl)
 
 
C +--IR    Radiation Absorption
C +  --------------------------
 
          tau_Ca = 1.  - tau_sv(ikl)                        ! Canopy Absorption
          IRv_sv(ikl) = -2.0   *Evg_sv(ikl)  *stefan        !
     .                         *TvegSV(ikl)  *TvegSV(ikl)   ! Downward IR (OUT)
     .                         *TvegSV(ikl)  *TvegSV(ikl)   ! + Upward IR (OUT)
          dIRdTv(ikl) =
     .     -Evg_sv(ikl)*                                    !
     .                8.*stefan*TvegSV(ikl)  *TvegSV(ikl)   ! Downward IR (OUT)
     .                         *TvegSV(ikl)                 ! + Upward IR (OUT)
          IR_net =       tau_Ca                             !
     .    *(Evg_sv(ikl)* IRd_SV(ikl)                        ! Downward IR (IN)
     .     -             IRs_SV(ikl)                        !   Upward IR (IN)
     .     +             IRv_sv(ikl))                       !          IR (OUT)
 
 
C +--Sensible Heat Flux
C +  ------------------
 
          dHSdTv(ikl) = rhT_SV(ikl)* Sigmsv(ikl) *Cp        ! Derivative, t(n)
     .                / rah_sv(ikl)                         !
          HSv_sv(ikl) = dHSdTv(ikl)                         ! Value,      t(n)
     .                *(TaT_SV(ikl)-TvegSV(ikl))            !
 
 
C +--Latent   Heat Flux
C +  ------------------
 
C +--Canopy Saturation Specific Humidity
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          den_qs =         TvegSV(ikl)- 35.8
          arg_qs = 17.27 *(TvegSV(ikl)-273.16)/den_qs
          qsatvg = .0038 * exp(arg_qs)
          dqs_dT = qsatvg* 4099.2    /(den_qs *den_qs)
 
C +--Canopy Stomatal Resistance
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^
          R0Stom = min(         StodSV(ivgtSV(ikl))
     .                /max(epsi,glf_sv(       ikl)),StxdSV)  ! Min Stomatal R.
          F_Stom = pscdSV / max(pscdSV-psivSV(ikl) ,epsi)    ! F(Leaf Wat.Pot.)
C +                                                          ! DR97, eqn. 3.22
          R_Stom =(R0Stom / max(LAIesv(ikl), R0Stom/StxdSV)) ! Can.Stomatal R.
     .           * F_Stom                                    ! DR97, eqn. 3.21
 
C +--Evaporation / Evapotranspiration
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          SnoMsk = max(zero, sign(unun,snCaSV(ikl)-eps_21))  !
          EvFrac = max(zero, sign(unun,QaT_SV(ikl)-qsatvg))  ! Condensation/
          EvFrac = EvFrac                                    ! Transpiration
     .       + (1.-EvFrac)*((1-SnoMsk)*         rrCaSV(ikl)  !        Switch
     .                                         /rrMxsv(ikl)  !
     .                      +  SnoMsk *min(unun,snCaSV(ikl)  !
     .                                         /rrMxsv(ikl)))!
          Fac_Ev = rhT_SV(ikl) *Sigmsv(ikl)                  ! Idem,  Factor
          FacEvp = Fac_Ev      *EvFrac     / rah_sv(ikl)     !
          Evp_sv(ikl) = FacEvp*(qsatvg     - QaT_SV(ikl))    ! Evaporation
          dEvpdT(ikl) = FacEvp* dqs_dT                       ! Evp Derivative
          FacEvt = Fac_Ev * (1.-EvFrac)    /(rah_sv(ikl)     !
     .                              +R_Stom *Sigmsv(ikl))    !
          EvT_sv(ikl) = FacEvt*(qsatvg     - QaT_SV(ikl))    ! EvapoTranspir.
          dEvTdT(ikl) = FacEvt* dqs_dT                       ! EvT Derivative
          HLv_sv(ikl) =-Lv_H2O*(Evp_sv(ikl)+ EvT_sv(ikl))    ! Latent   Heat
     .                 -Lf_H2O* Evp_sv(ikl)* SnoMsk          !(Subli.Contrib.)
          dHLdTv(ikl) = Lv_H2O*(dEvpdT(ikl)+ dEvTdT(ikl))    !
     .                 +Lf_H2O* dEvpdT(ikl)* SnoMsk          !
c #HC     dHCdTv(ikl) = Cn_dSV* snCaSV(ikl)/ dt__SV          ! Heat  Storage
 
 
C +--Imbalance  of the Canopy  Energy Budget
C +  ---------------------------------------
 
          LAI_OK = max(zero,                                 ! NO Budget if
     .                 sign(unun, LAI_sv(ikl)-eps_21))       ! no Leaves
          Hv_Tv0 =    (  SoCasv(ikl)         *sol_SV(ikl)    ! Absorbed Solar
     .                 + IR_net                              ! NET      IR
     .                 + HSv_sv(ikl)                         ! Sensible Heat
     .                 + HLv_sv(ikl)                         ! Latent   Heat
     .                ) *LAI_OK                              !
! #e1     ETVg_d(ikl) =  Hv_Tv0                              ! Veg.Energ.Bal.
C +
          Hswich      =          unun
c #NN     Hswich      = max(zero,                            ! Newton-Raphson
c #NN.                      sign(unun,    abs(Hv_Tv0     )   !         Switch
c #NN.                                       -Hv_MIN      )) !
 
 
C +--Derivative of the Canopy  Energy Budget
C +  ---------------------------------------
 
          dHvdTv    =   dIRdTv(ikl) * max(eps_21,tau_Ca)
     .                - dHSdTv(ikl)
     .                - dHLdTv(ikl)
c #HC.                - dHCdTv(ikl)
 
 
C +--Update Canopy and Surface/Canopy Temperatures
C +  ---------------------------------------------
 
          d_Tveg      = Hv_Tv0       / dHvdTv               !
          d_Tveg      =      sign(unun,d_Tveg)              ! Increment
     .                       *min( abs(d_Tveg)     ,dTvMAX) ! Limitor
          TvegSV(ikl) = TvegSV(ikl)  - Hswich      *d_Tveg  ! Newton-Raphson
          Hv_MAX      = max(Hv_MAX,abs(Hv_Tv0     ))        !
 
 
C +--Update Vegetation Fluxes
C +  ------------------------
 
c #NN     IRv_sv(ikl) = IRv_sv(ikl)-dIRdTv(ikl)    *d_Tveg  ! Emitted  IR
c #NN     HSv_sv(ikl) = HSv_sv(ikl)+dHSdTv(ikl)    *d_Tveg  ! Sensible Heat
c #NN     Evp_sv(ikl) = Evp_sv(ikl)-dEvpdT(ikl)    *d_Tveg  ! Evapotranspir.
c #NN     EvT_sv(ikl) = EvT_sv(ikl)-dEvTdT(ikl)    *d_Tveg  ! Evapotranspir.
c #NN     HLv_sv(ikl) = HLv_sv(ikl)+dHLdTv(ikl)    *d_Tveg  ! Latent   Heat
C +
          IRv_sv(ikl) = IRv_sv(ikl)                *LAI_OK
          HSv_sv(ikl) = HSv_sv(ikl)                *LAI_OK
          Evp_sv(ikl) = Evp_sv(ikl)                *LAI_OK
          EvT_sv(ikl) = EvT_sv(ikl)                *LAI_OK
          HLv_sv(ikl) = HLv_sv(ikl)                *LAI_OK
        END DO
 
c #IX IF (                     nit.lt.nitmax)    GO TO 101
      IF (Hv_MAX.gt.Hv_MIN.and.nit.lt.nitmax)    GO TO 101
 
        DO ikl=1,klonv
          IRv_sv(ikl) = IRv_sv(ikl)                         ! Emitted  IR
     .   +dIRdTv(ikl) *(TvegSV(ikl)-Tveg_0(ikl))            !
          HSv_sv(ikl) = HSv_sv(ikl)                         ! Sensible Heat
     .   -dHSdTv(ikl) *(TvegSV(ikl)-Tveg_0(ikl))            !
          Evp_sv(ikl) = Evp_sv(ikl)                         ! Evaporation
     .   +dEvpdT(ikl) *(TvegSV(ikl)-Tveg_0(ikl))            !
          EvT_sv(ikl) = EvT_sv(ikl)                         ! Transpiration
     .   +dEvTdT(ikl) *(TvegSV(ikl)-Tveg_0(ikl))            !
          HLv_sv(ikl) = HLv_sv(ikl)                         ! Latent   Heat
     .   -dHLdTv(ikl) *(TvegSV(ikl)-Tveg_0(ikl))            !
 
C +--OUTPUT for Stand Alone NetCDF File
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #NC     HLv_KL(ikl) = HLv_sv(ikl)
 
 
C +--Update Canopy Water Content
C +  ---------------------------
 
          rrCaSV(ikl) = rrCaSV(ikl)-(1.-SnoMsk)*Evp_sv(ikl)*dt__SV
          snCaSV(ikl) = snCaSV(ikl)-    SnoMsk *Evp_sv(ikl)*dt__SV
 
C +--Correction for Positive Definiteness (see WKarea/EvpVeg/EvpVeg.f)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          rrCaOK =  max(rrCaSV(ikl), 0.)
          snCaOK =  max(snCaSV(ikl), 0.)
          dEvpOK =     (rrCaOK-rrCaSV(ikl)
     .                 +snCaOK-snCaSV(ikl))/dt__SV
 
          Evp_sv(ikl) = Evp_sv(ikl)       - dEvpOK          ! Evaporation
          HLv_sv(ikl) = HLv_sv(ikl)                         ! Latent   Heat
     .    +(1.-SnoMsk)* Lv_H2O            * dEvpOK          !
     .    +    SnoMsk *(Lv_H2O+Lf_H2O)    * dEvpOK          !
 
          rrCaSV(ikl) = rrCaOK
          snCaSV(ikl) = snCaOK
 
        END DO
 
 
      return
      end
