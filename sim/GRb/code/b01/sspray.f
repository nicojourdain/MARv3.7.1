 
 
      subroutine SSpray
 
C +--------------------------------------------------------------------------+
C |                                                          Sat 29-Jul-2009 |
C |                                                                          |
C |   subroutine SSpray computes contribution of Sea Spray                   |
C |              ^^^^^^                       to Sensible & Latent Heat Flux |
C |                                                                          |
C |   Reference A_03 Andreas, 2003, Preprints.                               |
C |                  12th Conference on Interactions of the Sea & Atmosphere |
C |                  Long Beach, CA, American Meteorological Society         |
C |                  http://ams.confex.com/ams/pdfpapers/77949.pdf           |
C |                                                                          |
C |             AD02 Andreas & Decosmo, 2002, BLM 103, 303-333               |
C |                  The signature of sea spray                              |
C |                                in the HEXOS turbulent heat flux data     |
C |                                                                          |
C |             AE01 Andreas & Emanuel, 2001, JAS  58, 3741-3751             |
C |                  Effects of Sea Spray on Tropical Cyclone Intensity      |
C |                                                                          |
C |             A_95 Andreas, 1995, JAS 52(7) , 852-862                      |
C |                  The Temperature of Evaporating Sea Spray Droplets       |
C |                                                                          |
C |             A_90 Andreas, 1990, Tellus 42B, 481-497                      |
C |                  Time Constants for the Evolution of Sea Spray Droplets  |
C |                                                                          |
C |                                                                          |
C +--------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include  "MARCTR.inc"
      include  "MARphy.inc"
      include  "MARdim.inc"
      include  "MARgrd.inc"
      include  "MAR_DY.inc"
      include  "MAR_SL.inc"
      include  "MAR_WK.inc"
 
 
C +--Internal  Constants and Variables
C +  =================================
 
      real alphaC,Uf_Spr,t50_eq,f50_eq
      real ustar2,alo10g,A13wav,tf_Spr
      real Ta_wet,r50_eq,r50__t,r50n_t,QS_Spr,qq_Spr,QL_Spr
 
 
C +--DATA
C +  ====
 
      data alphaC /0.0185/                                             ! A_03
                                                                       ! ( 3)
                                                                       !
      data Uf_Spr /0.1   /                                             ! A_90
C +...     Uf_Spr = .1 = 1/tau_f where tau_f = 10 sec                  ! fig9
                                                                       !
      data t50_eq /0.2000e+3/                                          ! A_90
                                                                       ! fig9
                                                                       !
      data f50_eq /0.3125e-4/                                          ! A_90
C +...             0.3125e-4 = 50.e-6 m * 0.5          / 0.8           ! p490
C +                                     * 0.5  when RH = 0.8
 
 
C +--Wave Height
C +  ===========
 
      DO j=1,my
      DO i=1,mx
      A13wav = 0.015        * ssvSL(i,j,mz)                            ! AD02
                                                                       ! ( 4)
c #A3 ustar2 = SLuusl(i,j,1)*SLuusl(i,j,1)                             !
c #A3 alo10g =       log(10.*gravit/alphaC)                            !
c #A3 A13wav =                                                         ! A_03
c #A3.  0.015*(ustar2/vonkar)*(2.*ustar2-SLuusl(i,j,1)*(2.*alo10g+8.)) ! (13)
c #A3.                                +(alo10g*alo10g + 2.*alo10g+4.)  !
 
      A13wav =       min(20.,A13wav)
 
 
C +--Sea Spray Residence Time
C +  ========================
 
      tf_Spr = A13wav/Uf_Spr                                           ! A_03
                                                                       ! (12)
 
C +--Sea Spray Sensible Heat Flux Correction
C +  =======================================
 
      Ta_wet = tairDY(i,j,mz)-(Lv_H2O/cp)*(qvswDY(i,j,mz)-qvDY(i,j,mz))! A_95
      Ta_wet =      max(tfrwat       ,Ta_wet)                          !
      QS_Spr = 4.187e6*(tsrfSL(i,j,1)-Ta_wet)                          ! AE01
                                                                       ! ( 7)
     .        *1.65e-6* SLuusl(i,j,1)*SLuusl(i,j,1)*SLuusl(i,j,1)      ! A_03
     .                                                                 ! (14a)
 
 
C +--Sea Spray Latent   Heat Flux Correction
C +  =======================================
 
      r50_eq = f50_eq * min(1.,  qvDY(i,j,mz)      /qvswDY(i,j,mz))    ! A_90
                                                                       ! p490
      r50__t = r50_eq +(50.e-6-r50_eq) *       exp(-tf_Spr/t50_eq)     !
      r50n_t = r50__t / 50.e-6
      qq_Spr = 1000.00                 *(1.-r50n_t *r50n_t*r50n_t)     ! A_03
     .        *2.65e-8* SLuusl(i,j,1)**2.61                            ! (11)
      QL_Spr =        - Lv_H2O         *   qq_Spr                      ! A_03
                                                                       ! (11)
 
C +--Update
C +  ======
 
C +--Increment
C +  ~~~~~~~~~
c #X  pktaDY(i,j,mz) = pktaDY(i,j,mz)+maskSL(i,j)*SLsrfl(i,j,1)        !
c #X .  *(QS_Spr+QL_Spr)*dt_Loc/(1.e3*pstDYn(i,j)*dsigm1(mz)*grvinv)   !
c #X .                              /(  pkDY(i,j,mz)        *cp    )   !
 
       WKxy1(i,j)    =                maskSL(i,j)*SLsrfl(i,j,1)        !
     .  *(QS_Spr+QL_Spr)*dt_Loc/(1.e3*pstDYn(i,j)*dsigm1(mz)*grvinv)   !
     .                              /(  pkDY(i,j,mz)        *cp    )   !
 
C +--Increment (after limiting the fluxes)
C +  ~~~~~~~~~
       WKxy2(i,j) =
     .   max(0.0,sign(1.,pktaDY(i,j,mz) -pktaDY(i,j,mzz)           ))  ! LOWER
     .  *max(0.0,sign(1.,pktaDY(i,j,mzz)-pktaDY(i,j,mz )-WKxy1(i,j)))  !
     .  *max(WKxy1(i,j) ,pktaDY(i,j,mzz)-pktaDY(i,j,mz )            )  !
     . + max(0.0,sign(1.,pktaDY(i,j,mz) -pktaDY(i,j,mzz)+WKxy1(i,j)))  ! UPPER
     .  *max(0.0,sign(1.,pktaDY(i,j,mzz)-pktaDY(i,j,mz )           ))  !
     .  *min(WKxy1(i,j) ,pktaDY(i,j,mzz)-pktaDY(i,j,mz )            )  !
     . + max(0.0,sign(1.,pktaDY(i,j,mz) -pktaDY(i,j,mzz)+WKxy1(i,j))   ! OTHER
     .          *sign(1.,pktaDY(i,j,mz) -pktaDY(i,j,mzz)           ))  !
     .  *    WKxy1(i,j)                                                !
 
       WKxy2(i,j)    =                           sign(1.,WKxy2(i,j))   !  max:
     .                             *min(10.*dt/3600.,abs(WKxy2(i,j)))  ! 10°C/h
      pktaDY(i,j,mz) =   pktaDY(i,j,mz)                 +WKxy2(i,j)
 
      qq_Spr         =   qq_Spr  *  WKxy2(i,j)/(sign(1.0,WKxy1(i,j))
     .                                    * max(eps9,abs(WKxy1(i,j))))
 
        qvDY(i,j,mz) =   qvDY(i,j,mz)+maskSL(i,j)*SLsrfl(i,j,1)        !
     .  * qq_Spr        *dt_Loc/(1.e3*pstDYn(i,j)*dsigm1(mz)*grvinv)   !
 
      ENDDO
      ENDDO
 
      DO j=1,my
      DO i=1,mx
       WKxy1(i,j) = 0.0
       WKxy2(i,j) = 0.0
      ENDDO
      ENDDO
 
      return
      end
