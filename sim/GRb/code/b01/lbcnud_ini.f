 
 
 
      subroutine LBCnud_ini
C +
C +------------------------------------------------------------------------+
C | MAR DYNAMICS LBC                                       18-09-2001  MAR |
C |   SubRoutine LBCnud_ini initialize the Nudging Coefficient             |
C |                 for the lateral boundary conditions of Davies, 1983    |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   REFER. : Davies, MWR   111, p.1002-1012, 1983                        |
C |   ^^^^^^^^                                                             |
C |                                                                        |
C |   OUTPUT : rxLB,ryLB: nudging coefficients of the relaxation zones     |
C |   ^^^^^^^^                                                             |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +--Global Variables
C +  ================
C +
      include 'MARCTR.inc'
      include 'MARphy.inc'
C +
      include 'MARdim.inc'
      include 'MARgrd.inc'
C +
      include 'MAR_LB.inc'
C +
      include 'MAR_IO.inc'
C +
C +
C +--Local  Variables
C +  ================
C +
      real     d25
c #da real     cspeed
C +
C +
C +--Nudging Coefficient Multiplied by the Time Step
C +  ===============================================
C +
c #da   cspeed   = 300.d0*max(n6-1,1)*max(n6-1,1)
c #da.                  /(max(n6-2,1)*max(n6-2,1))
c #da   rxbase   = cspeed/(2.d0*dx)
C +***  rxbase: Optimal Maximum Relaxation Coefficient
C +             (see Davies 1983, MWR 111 p. 1007, 2e col. K*  = K(dx)/c,
C +                               with K*~0.5, c= c_max= sqrt(gH)=300m/s)
c #da   rxfact   = 0.5*dt*max(n6-1,1)*max(n6-1,1)
c #da.                  /(max(n6-2,1)*max(n6-2,1))
C +***  rxfact: Such that nu* < 1, but Diffusion_Relaxation replaces Diffusion
C +             (see Davies 1983, MWR 111 p. 1004, 1e col. nu* = K(dx)/c,
C +                                       p. 1007, 1e col. 21b)
C +
        if (IO_loc.ge.2) write(21,999) rxbase,rxfact
 999    format(/,'   --- Initialisation / LBCnud_ini ---',
     .         /,'       K(relax)max =',f14.6,' (Davies 1983 MWR)',
     .         /,'       K_H(fac)    =',f14.6)
C +
        d25      =(n6-1)*(n6-1)
        rxLB(1)  = 0.d0
        rxLB(mx) = 0.d0
       if (mmx.gt.1) then
        do 1 i=ip11,mx1
        rxLB(i)  =(max(0,n6-i)     *max(0,n6-i)
     .            +max(0,n6-1+i-mx)*max(0,n6-1+i-mx))
     .            *rxbase /d25
 1      continue
       end if
C +
        ryLB(1)  = 0.d0
        ryLB(my) = 0.d0
       if (mmy.gt.1) then
        do 2 j=jp11,my1
        ryLB(j)  =(max(0,n6-j)     *max(0,n6-j)
     .            +max(0,n6-1+j-my)*max(0,n6-1+j-my))
     .            *rxbase /d25
 2      continue
       end if
C +
      return
      end
