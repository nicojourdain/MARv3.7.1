 
 
      subroutine DYNadv_LFB_2v(niSlow,nordAV,ffx)
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS   SLOW                                    22-11-2006  MAR |
C |   SubRoutine DYNadv_LFB_2v solves Advection (LeapFrog Backward Scheme) |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT/        qqmass          : Mass Conservation Switch (+def.var.) |
C |   ^^^^^^        iterun          : Long Time Step Counter               |
C |                 nnSlow          : Time      Step Counter Maximum Value |
C |                                                                        |
C |   INPUT/  (via common block)                                           |
C |   ^^^^^^        dt              : Time Step                            |
C |                 opstDY(mx,my)   : MASS, Time Step n                    |
C |                 pstDYn(mx,my)   : MASS, Time Step n+1                  |
C |                 uairDY(mx,my,mz): Advection Vector: x-----Direction    |
C |                 vairDY(mx,my,mz): Advection Vector: y-----Direction    |
C |                 psigDY(mx,my,mz): Advection Vector: sigma-Direction    |
C |                                                                        |
C |   INPUT/           ffx(mx,my,mz): Advected  Variable                   |
C |   OUTPUT                                                               |
C |   ^^^^^^                                                               |
C |                                                                        |
C |   METHOD:  2th order accurate Time       Scheme (leapfrog backw.) .AND.|
C |   ^^^^^^  (2th order accurate Horizontal Scheme on Arakawa A grid .OR. |
C |            4th order accurate Horizontal Scheme on Arakawa A grid     )|
C |            2th order          Vertical   Scheme                        |
C |                                                                        |
C |            Robert Time Filter (for minimizing the computational mode)  |
C |                                                                        |
C |   CAUTION: This routine must be used                                   |
C |   ^^^^^^^  with a positive  definite restoring Procedure               |
C |            for    positive  definite Variables                         |
C |           (Such a Procedure is set up after digital filtering in MAR)  |
C |                                                                        |
C |   REFER.:  Use of  A grid: Purser   & Leslie,   1988, MWR 116, p.2069  |
C |   ^^^^^^   Time    Scheme: Haltiner & Williams, 1980, 5-2,     p.152   |
C |            Spatial Scheme: Haltiner & Williams, 1980, 5-6-5,   p.135   |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_DY.inc'
      include 'MARqqm.inc'
 
      include 'MAR_WK.inc'
 
      integer  niSlow                   ! Time      Step Counter Maximum Value
      real        ffx(mx,my,mzz)        ! Advected  Variable (t=n)
 
      integer  nordAV
 
 
C +--Local  Variables
C +  ================
 
      logical                    daSlow ! Initialization Switch
      common  /DYNadv_LFB_2v_log/daSlow !
      real                       dtSlow ! Time Step
      common  /DYNadv_LFB_2v_rea/dtSlow !
      real     rtSlow                   !
      integer  itSlow                   ! Time Step Counter
      integer  nnSlow,n0Slow            !
      common  /DYNadv_LFB_2v_int/nnSlow !
 
      real        ffm1                  ! Advected  Variable (t=n-1)
      real        ffp1(mx,my,mzz)       ! Advected  Variable (t=n+1)
      real       dff                    ! Variable  Increment
 
      integer   kk  ,ip2s,ip1s,im1s,im2s,jp2s,jp1s,jm1s,jm2s
 
      real      summ,sumn
 
      logical   LFBord , FLhost
 
      data      LFBord /.FALSE./
      data      FLhost /.TRUE./
 
 
C +--Start the Leapfrog Backward Scheme
C +  ==================================
 
      IF (.NOT.daSlow)                                              THEN
               daSlow =.true.
               n0Slow = niSlow  ! previous           Nb of  Time Steps
      ELSE
               n0Slow = nnSlow  ! previous           Nb of  Time Steps
      END IF
               rtSlow = CFLzDY  ! minimum acceptable Nb of  Time Steps
                                ! in the leap-frog Backward Scheme
 
      DO   k=1,mz
        DO j=1,my
        DO i=1,mx
               rtSlow = max(rtSlow ,abs(uairDY(i,j,k))*2.0*dt/dx       )
               rtSlow = max(rtSlow ,abs(vairDY(i,j,k))*2.0*dt/dx       )
        END DO
        END DO
      END DO
               nnSlow =          rtSlow + 0.5
               nnSlow =      max(nnSlow , ntFast)
      IF  (mod(nnSlow,2).EQ.0)   nnSlow = nnSlow + 1
               dtSlow =  dt   / (nnSlow+1)
 
      IF (nnSlow.NE.n0Slow)                                         THEN
          write(6,6000)  nnSlow,dtSlow
 6000     format(/,'Advection Leap-Frog Backward Properties',
     .           /,' ntSlow = ',i6,
     .           /,' dtSlow = ',f9.2,/,1x)
      END IF
 
 
C +--Mass Conservation
C +  =================
 
      IF (qqmass)                                                   THEN
            summ = 0.
       IF(LFBord)                                                   THEN
        DO   k=1,mz
 
          DO j=lgy ,ldy
            summ = summ + dsigm1(k)*dtx
     .           *(uairDY(lgx ,j,k)
     .                *f2_3*(      (ffx(lgx1,j,k)*opstDY(lgx1,j)
     .                             -ffx(lgx ,j,k)*opstDY(lgx ,j))
     .                      +0.125*(ffx(lgx ,j,k)*opstDY(lgx ,j)
     .                             -ffx(lgx2,j,k)*opstDY(lgx2,j)))
     .            +uairDY(lgxx,j,k)
     .                *f2_3* 0.125*(ffx(lgx ,j,k)*opstDY(lgx ,j)
     .                             -ffx(lgx1,j,k)*opstDY(lgx1,j))
     .            -uairDY(ldx ,j,k)
     .                *f2_3*(      (ffx(ldx1,j,k)*opstDY(ldx1,j)
     .                             -ffx(ldx ,j,k)*opstDY(ldx ,j))
     .                      +0.125*(ffx(ldx ,j,k)*opstDY(ldx ,j)
     .                             -ffx(ldx2,j,k)*opstDY(ldx2,j)))
     .            -uairDY(ldxx,j,k)
     .                *f2_3* 0.125*(ffx(ldx ,j,k)*opstDY(ldx ,j)
     .                             -ffx(ldx1,j,k)*opstDY(ldx1,j)))
          ENDDO
 
          DO i=lgx ,ldx
            summ = summ + dsigm1(k)*dtx
     .           *(vairDY(i,lgy ,k)
     .                *f2_3*(      (ffx(i,lgy1,k)*opstDY(i,lgy1)
     .                             -ffx(i,lgy ,k)*opstDY(i,lgy ))
     .                      +0.125*(ffx(i,lgy ,k)*opstDY(i,lgy )
     .                             -ffx(i,lgy2,k)*opstDY(i,lgy2)))
     .            +vairDY(i,lgyy,k)
     .                *f2_3* 0.125*(ffx(i,lgy ,k)*opstDY(i,lgy )
     .                             -ffx(i,lgy1,k)*opstDY(i,lgy1))
     .            -vairDY(i,ldy ,k)
     .                *f2_3*(      (ffx(i,ldy1,k)*opstDY(i,ldy1)
     .                             -ffx(i,ldy ,k)*opstDY(i,ldy ))
     .                      +0.125*(ffx(i,ldy ,k)*opstDY(i,ldy )
     .                             -ffx(i,ldy2,k)*opstDY(i,ldy2)))
     .            -vairDY(i,ldyy,k)
     .                *f2_3* 0.125*(ffx(i,ldy ,k)*opstDY(i,ldy )
     .                             -ffx(i,ldy1,k)*opstDY(i,ldy1)))
          ENDDO
 
          DO j=lgy ,ldy
          DO i=lgx ,ldx
            summ = summ + opstDY(i,j)   *ffx(i,j,k)   *dsigm1(k)
          END DO
          END DO
        END DO
 
       ELSE IF (FLhost)                                            THEN
        DO   k=1,mz
          DO j=1,my
            summ = summ + dsigm1(k) * dtx             *dsigm1(       k)
     .                  *(opstDY(1   ,j)*ffx(1   ,j,k)*uairDY(1   ,j,k)
     .                   -opstDY(mx  ,j)*ffx(mx  ,j,k)*uairDY(mx  ,j,k))
          END DO
 
          DO i=1,mx
            summ = summ + dsigm1(k) * dtx             *dsigm1(       k)
     .                  *(opstDY(i,1   )*ffx(i,1   ,k)*vairDY(i,1   ,k)
     .                   -opstDY(i,my  )*ffx(i,my  ,k)*vairDY(i,my  ,k))
          END DO
 
          DO j=1,my
          DO i=1,mx
            summ = summ + opstDY(i,j)   *ffx(i,j,k)   *dsigm1(k)
          END DO
          END DO
 
        END DO
 
       ELSE
        DO   k=1,mz
 
          DO j=lgy ,ldy
            summ = summ + dsigm1(k) * dtx
     .                  *(opstDY(lgx ,j)*ffx(lgx ,j,k)*uairDY(lgx ,j,k)
     .                   -opstDY(ldx ,j)*ffx(ldx ,j,k)*uairDY(ldx ,j,k))
          ENDDO
 
          DO i=lgx ,ldx
            summ = summ + dsigm1(k) * dtx
     .                  *(opstDY(i,lgy )*ffx(i,lgy ,k)*vairDY(i,lgy ,k)
     .                   -opstDY(i,ldy )*ffx(i,ldy ,k)*vairDY(i,ldy ,k))
          ENDDO
 
          DO j=lgy ,ldy
          DO i=lgx ,ldx
            summ = summ + opstDY(i,j)   *ffx(i,j,k)   *dsigm1(k)
          END DO
          END DO
        END DO
       END IF
      END IF
 
 
C +--Start Leap-Frog Backward
C +  ========================
 
      DO  itSlow =  1 ,  nnSlow+1
 
 
C +--Mass Divergence
C +  ===============
 
C +--4th centered Differences / x-----Direction
C +  ------------------------------------------
 
        DO    k=1,mz
          DO  i=1,mx
            ip2s=ip2(i)
            ip1s=ip1(i)
            im1s=im1(i)
            im2s=im2(i)
          DO  j=1,my
            WKxyz1(i,j,k)=  uairDY(i   ,j,k)*dxinv2 * fac43
     .             * ( 0.125 *(ffx(ip2s,j,k)-ffx(im2s,j,k))
     .                        -ffx(ip1s,j,k)+ffx(im1s,j,k) )
          END DO
          END DO
        END DO
 
 
C +--4th centered Differences / y-----Direction
C +  ------------------------------------------
 
        DO k=1,mz
          DO j=1,my
            jp2s=jp2(j)
            jp1s=jp1(j)
            jm1s=jm1(j)
            jm2s=jm2(j)
          DO i=1,mx
            WKxyz2(i,j,k)=  vairDY(i,j   ,k)*dyinv2 * fac43
     .             * ( 0.125 *(ffx(i,jp2s,k)-ffx(i,jm2s,k))
     .                        -ffx(i,jp1s,k)+ffx(i,jm1s,k) )
          END DO
          END DO
        END DO
 
 
C +--2th centered Differences  / sigma-Direction  / Energy conserving
C +  --- (Haltiner and Williams, 1980, 7.2.2, Eqn. (7-47b) p.220) ---
C +      --------------------------------------------------------
 
C +--Vertical Differences
C +  ~~~~~~~~~~~~~~~~~~~~
          DO       k=   1,mz
                   kk=kp1(k)
            DO     j=jp11,my1
            DO     i=ip11,mx1
              WKxyz6(i,j,k)=   ffx(i,j,k)   -ffx(i,j,kk)
            END DO
            END DO
          END DO
 
          DO       k=   2,mz
                   kk=km1(k)
            DO     j=jp11,my1
            DO     i=ip11,mx1
              WKxyz7(i,j,k)=   wsigDY(i,j,kk)
              WKxyz8(i,j,k)=   WKxyz6(i,j,kk)
            END DO
            END DO
          END DO
 
                   k=   1
            DO     j=jp11,my1
            DO     i=ip11,mx1
              WKxyz3(i,j,k)=   WKxyz6(i,j,k) * wsigDY(i,j,k)
     .                        *0.5           / dsigm1(1)
            END DO
            END DO
          DO       k=   2,mz
            DO     j=jp11,my1
            DO     i=ip11,mx1
              WKxyz3(i,j,k)=  (WKxyz6(i,j,k) * wsigDY(i,j,k)
     .                        +WKxyz8(i,j,k) * WKxyz7(i,j,k))
     .                        *0.5            /dsigm1(k)
            END DO
            END DO
          END DO
 
 
C +--Mass Update (Leapfrog-Backward)
C +  ===============================
 
        IF  (itSlow.eq.1)                                           THEN
            DO k=   1,mz
            DO j=jp11,my1
            DO i=ip11,mx1
               dff         =(WKxyz1(i,j,k)+WKxyz2(i,j,k)+WKxyz3(i,j,k))
     .                      *dtSlow
               ffx(i,j,k)  =    ffx(i,j,k) + dff
              ffp1(i,j,k)  =    ffx(i,j,k) + dff + dff
            END DO
            END DO
            END DO
        ELSE
          IF (itSlow.le.nnSlow)                                     THEN
            DO k=   1,mz
            DO j=jp11,my1
            DO i=ip11,mx1
               dff         =(WKxyz1(i,j,k)+WKxyz2(i,j,k)+WKxyz3(i,j,k))
     .                      *dtSlow
              ffm1         =    ffx(i,j,k)
               ffx(i,j,k)  =   ffp1(i,j,k)
              ffp1(i,j,k)  =   ffm1        + dff + dff
 
C +--Robert Time Filter
C +  ~~~~~~~~~~~~~~~~~~
c #rt          ffx(i,j,k)  =    ffx(i,j,k)+
c #rt.            Robert*(0.5*(ffp1(i,j,k)+ffm1)-ffx(i,j,k))
            END DO
            END DO
            END DO
          ELSE
            DO k=   1,mz
            DO j=jp11,my1
            DO i=ip11,mx1
               dff         =(WKxyz1(i,j,k)+WKxyz2(i,j,k)+WKxyz3(i,j,k))
     .                      *dtSlow
               ffx(i,j,k)  =    ffx(i,j,k) + dff
            END DO
            END DO
            END DO
C +***      Leapfrog-Backward (e.g. Haltiner and Williams, p.152)
 
          END IF
        END IF
 
      END DO
 
 
C +--Work Arrays Reset
C +  =================
 
        DO k=1,mz
        DO j=1,my
        DO i=1,mx
          WKxyz1(i,j,k) = 0.0
          WKxyz2(i,j,k) = 0.0
          WKxyz3(i,j,k) = 0.0
          WKxyz6(i,j,k) = 0.0
          WKxyz7(i,j,k) = 0.0
          WKxyz8(i,j,k) = 0.0
        END DO
        END DO
        END DO
 
 
C +--Mass Conservation
C +  =================
 
      IF (qqmass)                                                   THEN
          DO k=1,mz
          DO j=1,my
          DO i=1,mx
            ffx(i,j,k) = max(zero,                         ffx(i,j,k))
          END DO
          END DO
          END DO
 
            sumn = 0.
        IF (FLhost)                                                 THEN
          DO k=1,mz
          DO j=1   ,my
          DO i=1   ,mx
            sumn       =     sumn +pstDYn(i,j) *dsigm1(k) *ffx(i,j,k)
          END DO
          END DO
          END DO
        ELSE
          DO k=1,mz
          DO j=lgy ,ldy
          DO i=lgx ,ldx
            sumn       =     sumn +pstDYn(i,j) *dsigm1(k) *ffx(i,j,k)
          END DO
          END DO
          END DO
        END IF
        IF (sumn.gt.zero)                                           THEN
            summ  =  summ  / sumn
          DO k=1,mz
          DO j=1,my
          DO i=1,mx
            ffx(i,j,k) =     summ                         *ffx(i,j,k)
          END DO
          END DO
          END DO
        END IF
 
      END IF
 
      return
      end
