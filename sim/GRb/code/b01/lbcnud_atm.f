 
      subroutine LBCnud_atm(f__LBC,iv_nua,kd_nua)
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS LBC                                   Fri  4-12-2009  MAR |
C |   SubRoutine LBCnud_atm computes the Lateral Boundary Conditions       |
C |              following the Davies (1976) scheme                        |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT / OUTPUT : f__LBC, i.e. uairDY, vairDY, qvDY, pktaDY, pstDYn   |
C |   ^^^^^^^^     for iv_nua =          1,      2,    3,      4,      5   |
C |                    f_LBC0 reevalued on a 5-points width boundary zone  |
C |                                                                        |
C |   INPUT:   iv_nua: Index of the Variable to relax to Outer Conditions  |
C |   ^^^^^^   kd_nua: Maximum Value of the k (vertical) Index             |
C |                                                                        |
C |   INPUT (via common block)                                             |
C |   ^^^^^    reaLBC: Input INI: Previous Dyn.Simulation (MAR .or. GCM)   |
C |            rxfact: Lateral Sponge Coefficient         (A89)            |
C |            rxLB,ryLB: Nudging Coefficient                              |
C |            Independant Term  used in the Implicit Scheme               |
C |                                                                        |
C |   REFER. : Davies, QJRMS 102, pp.405--418, 1976  (relation 11 p.409)   |
C |   ^^^^^^^^                                                             |
C |                                                                        |
C |   INPUT  : Nudging Coefficient rxLB and ryLB                           |
C |   ^^^^^^^^ Inverted Matrices used in the Implicit Scheme               |
C |                        wixgLB (zone x <<), wixdLB (zone x >>)          |
C |                        wiyiLB (zone y <<), wiysLB (zone y >>)          |
C |            Independant Term  used in the Implicit Scheme               |
C |            Variable v: tixgLB (zone x <<), tixdLB (zone x >>)          |
C |            Variable u: tiyiLB (zone y <<), tiysLB (zone y >>)          |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_LB.inc'
      include 'MAR_WK.inc'
 
      real     f__LBC(mx,my,mz)
      integer  kd_nua,iv_nua
 
 
C +--Local  Variables
C +  ================
 
c #OG logical relaxg
 
      integer il,ic,jl,jc
      real    sx(mx,mz)
      real    sy(my,mz)
      real    txg(2:n7,mz),txd(mx-n6:mx1,mz)
      real    tyi(2:n7,mz),tys(my-n6:my1,mz)
 
      real    fmagng(6),fmagnd(6),fmagnb(6),fmagnh(6)
 
      data fmagng/1.0e0,1.0e0,1.0e0,1.0e0,1.0e0,1.0e0/
      data fmagnd/1.0e0,1.0e0,1.0e0,1.0e0,1.0e0,1.0e0/
      data fmagnb/1.0e0,1.0e0,1.0e0,1.0e0,1.0e0,1.0e0/
      data fmagnh/1.0e0,1.0e0,1.0e0,1.0e0,1.0e0,1.0e0/
C +...     fmagnX:magnification factor (=>nudging selectively modified)
 
c #OG data relaxg/.false./
C +...     relaxg=.false.==> NO nudging  at the left boundary.
 
 
!    x Boundaries
!    ============
 
      IF   (mmx   .gt.1)                                          THEN
        IF (iv_nua.eq.2)                                          THEN
C +
          DO j=jp11,my1
 
C +--`Left' Boundary (x <<)
C +  ~~~~~~~~~~~~~~~~~~~~~~
            DO   il = ip11,n7
                DO  k = 1,kd_nua
                  txg(il,k)  = 0.d0
                END DO
              DO ic = ip11,n7
                DO  k = 1,kd_nua
                  txg(il,k)  = txg(il,  k)
     .                   + (tixgLB(ic,j,k)
     .                     +f__LBC(ic,j,k))*wixgLB(il,ic)
                END DO
              END DO
            END DO
            DO i  = ip11,n7
                DO  k = 1,kd_nua
                  f__LBC (i,j,k) = txg(i,   k)
                END DO
            END DO
 
C +--`Right' Boundary (x >>)
C +  ~~~~~~~~~~~~~~~~~~~~~~~
            DO  il = mx-n6,mx1
                DO  k = 1,kd_nua
                  txd(il,k)  = 0.d0
                END DO
              DO ic = mx-n6,mx1
                DO  k = 1,kd_nua
                  txd(il,k)  = txd(il,  k) +
     .                     (tixdLB(ic,j,k)
     .                     +f__LBC(ic,j,k))*wixdLB(il,ic)
                END DO
              END DO
            END DO
              DO i  = mx-n6mxLB,mx1
                DO  k = 1,kd_nua
                  f__LBC (i,j,k) = txd(i,   k)
                END DO
              END DO
          END DO
 
        ELSE
          DO  j=jp11,my1
c #OG       IF (relaxg)                                           THEN
              DO i=ip11,n6-1
                DO  k = 1,kd_nua
                  f__LBC(i,j,k) =              (f__LBC(i,j,k)
     .                  +fmagng(iv_nua)*rxLB(i)*vaxgLB(i,j,k,iv_nua))
     .            /(1.d0+fmagng(iv_nua)*rxLB(i))
                END DO
              END DO
c #OG       END IF
              DO i=mx-n6+2,mx1
                DO  k = 1,kd_nua
                  f__LBC(i,j,k) =              (f__LBC(i,j,k)
     .                  +fmagnd(iv_nua)*rxLB(i)*vaxdLB(i,j,k,iv_nua))
     .            /(1.d0+fmagnd(iv_nua)*rxLB(i))
                END DO
              END DO
          END DO
        END IF
 
 
!    Zero Gradient at y LBC if fmagng,d = 0 / otherwise prescribed LBC
!    -----------------------------------------------------------------
 
          DO   k=1,kd_nua
            DO j=1,my
              f__LBC( 1,j,k) =
     .           (1.-fmagng(iv_nua))*f__LBC(ip11,j,k)
     .         +     fmagng(iv_nua) *vaxgLB(   1,j,k,iv_nua)
              f__LBC(mx,j,k) =
     .           (1.-fmagnd(iv_nua))*f__LBC( mx1,j,k)
     .         +     fmagnd(iv_nua) *vaxdLB( mx ,j,k,iv_nua)
            END DO
          END DO
 
 
!    Nudging
!    -------
 
        DO     i=ip11,n6-1
          DO   k=   1,kd_nua
            DO j=jp11,my1
                WKxyz1(i  ,j,k)   =    f__LBC(  i,j,k) + rxfact*rxLB(i)
     .        *(f__LBC(i+1,j,k)       +f__LBC(i-1,j,k)
     .         -f__LBC(i  ,j,k)       -f__LBC(i  ,j,k)
     .         -vaxgLB(i+1,j,k,iv_nua)-vaxgLB(i-1,j,k,iv_nua)
     .         +vaxgLB(i  ,j,k,iv_nua)+vaxgLB(i  ,j,k,iv_nua))
            END DO
          END DO
        END DO
        DO     i=ip11,n6-1
          DO   k=   1,kd_nua
            DO j=jp11,my1
                    f__LBC(i  ,j,k) =  WKxyz1(i  ,j,k)
            END DO
          END DO
        END DO
 
        DO     i=mx-n6+2,mx1
          DO   k=   1   ,kd_nua
            DO j=jp11   ,my1
                WKxyz1(i  ,j,k)   =    f__LBC(  i,j,k) + rxfact*rxLB(i)
     .        *(f__LBC(i+1,j,k)       +f__LBC(i-1,j,k)
     .         -f__LBC(i  ,j,k)       -f__LBC(i  ,j,k)
     .         -vaxdLB(i+1,j,k,iv_nua)-vaxdLB(i-1,j,k,iv_nua)
     .         +vaxdLB(i  ,j,k,iv_nua)+vaxdLB(i  ,j,k,iv_nua))
            END DO
          END DO
        END DO
        DO     i=mx-n6+2,mx1
          DO   k=  1    ,kd_nua
            DO j=jp11   ,my1
                    f__LBC(i  ,j,k) = WKxyz1(i  ,j,k)
            END DO
          END DO
        END DO
 
 
!    Zero Gradient at y LBC if fmagng,d = 0 / otherwise prescribed LBC
!    -----------------------------------------------------------------
 
          DO   k=1,kd_nua
            DO j=jp11,my1
              f__LBC( 1,j,k) =
     .           (1.-fmagng(iv_nua))*f__LBC(ip11,j,k)
     .         +     fmagng(iv_nua) *vaxgLB(   1,j,k,iv_nua)
              f__LBC(mx,j,k) =
     .           (1.-fmagnd(iv_nua))*f__LBC( mx1,j,k)
     .         +     fmagnd(iv_nua) *vaxdLB( mx ,j,k,iv_nua)
            END DO
          END DO
 
      END IF
 
 
!    y Boundaries
!    ============
 
      IF  (mmy.gt.1)                                              THEN
 
        IF (iv_nua.eq.1)                                          THEN
 
          DO i=1,mx
 
C +--`Bottom' Boundary (y <<)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~
            DO     jl = jp11,n7
                DO  k =    1,kd_nua
                  tyi(jl,k)  = 0.d0
                END DO
              DO   jc = jp11,n7
                DO  k =    1,kd_nua
                  tyi(jl,k)  = tyi(  jl,k)
     .                    +(tiyiLB(i,jc,k)
     .                     +f__LBC(i,jc,k))*wiyiLB(jl,jc)
                END DO
              END DO
            END DO
            DO j  = jp11,n7
                DO  k = 1,kd_nua
                  f__LBC (i,j,k) = tyi(   j,k)
                END DO
            END DO
 
C +--`Top' Boundary (y >>)
C +  ~~~~~~~~~~~~~~~~~~~~~
            DO  jl = my-n6,my-1
                DO  k = 1,kd_nua
                  tys(jl,k)  = 0.d0
                END DO
              DO jc = my-n6,my-1
                DO  k = 1,kd_nua
                  tys(jl,k)  = tys(  jl,k)
     .                    +(tiysLB(i,jc,k)
     .                     +f__LBC(i,jc,k))*wiysLB(jl,jc)
                END DO
              END DO
            END DO
            DO j  = my-n6,my-1
                DO  k = 1,kd_nua
                  f__LBC (i,j,k) = tys(   j,k)
                END DO
            END DO
          END DO
 
        ELSE
          DO     j=jp11,n6-1
            DO   k=  1 ,kd_nua
              DO i=  1 ,mx
                f__LBC(i,j,k) =              (f__LBC(i,j,k)
     .                +fmagnb(iv_nua)*ryLB(j)*vayiLB(i,j,k,iv_nua))
     .          /(1.d0+fmagnb(iv_nua)*ryLB(j))
              END DO
            END DO
          END DO
          DO     j=my-n6+2,my1
            DO   k=  1    ,kd_nua
              DO i=  1    ,mx
                f__LBC(i,j,k) =              (f__LBC(i,j,k)
     .                +fmagnh(iv_nua)*ryLB(j)*vaysLB(i,j,k,iv_nua))
     .          /(1.d0+fmagnh(iv_nua)*ryLB(j))
              END DO
            END DO
          END DO
        END IF
 
 
!    Zero Gradient at y LBC if fmagnb,h = 0 / otherwise prescribed LBC
!    -----------------------------------------------------------------
 
          DO   k=1,kd_nua
            DO i=1,mx
              f__LBC(i, 1,k) =
     .           (1.-fmagnb(iv_nua))*f__LBC(i,jp11,k)
     .         +     fmagnb(iv_nua) *vayiLB(i,   1,k,iv_nua)
              f__LBC(i,my,k) =
     .           (1.-fmagnh(iv_nua))*f__LBC(i, my1,k)
     .         +     fmagnh(iv_nua) *vaysLB(i, my ,k,iv_nua)
            END DO
          END DO
 
 
!    Nudging
!    -------
 
          DO     j=2   ,n6-1
            DO   k=1   ,kd_nua
              DO i=ip11,mx1
                WKxyz2(i,j  ,k)   =    f__LBC(i,j  ,k) + rxfact*ryLB(j)
     .        *(f__LBC(i,j+1,k)       +f__LBC(i,j-1,k)
     .         -f__LBC(i,j  ,k)       -f__LBC(i,j  ,k)
     .         -vayiLB(i,j+1,k,iv_nua)-vayiLB(i,j-1,k,iv_nua)
     .         +vayiLB(i,j  ,k,iv_nua)+vayiLB(i,j  ,k,iv_nua))
              END DO
            END DO
          END DO
          DO     j=2   ,n6-1
            DO   k=1   ,kd_nua
              DO i=ip11,mx1
                f__LBC(i,j  ,k) =  WKxyz2(i,j  ,k)
              END DO
            END DO
          END DO
 
          DO     j=my-n6+2,my1
            DO   k=1      ,kd_nua
              DO i=ip11   ,mx1
                WKxyz2(i,j  ,k)   =    f__LBC(i,j  ,k) + rxfact*ryLB(j)
     .        *(f__LBC(i,j+1,k)       +f__LBC(i,j-1,k)
     .         -f__LBC(i,j  ,k)       -f__LBC(i,j  ,k)
     .         -vaysLB(i,j+1,k,iv_nua)-vaysLB(i,j-1,k,iv_nua)
     .         +vaysLB(i,j  ,k,iv_nua)+vaysLB(i,j  ,k,iv_nua))
              END DO
            END DO
          END DO
          DO     j=my-n6+2,my1
            DO   k=1      ,kd_nua
              DO i=ip11   ,mx1
                    f__LBC(i,j  ,k) =  WKxyz2(i,j  ,k)
              END DO
            END DO
          END DO
 
 
!    Zero Gradient at y LBC if fmagnb,h = 0 / otherwise prescribed LBC
!    -----------------------------------------------------------------
 
          DO   k=1,kd_nua
          DO   i=ip11,mx1
              f__LBC(i, 1,k) =
     .           (1.-fmagnb(iv_nua))*f__LBC(i,jp11,k)
     .         +     fmagnb(iv_nua) *vayiLB(i,   1,k,iv_nua)
              f__LBC(i,my,k) =
     .           (1.-fmagnh(iv_nua))*f__LBC(i, my1,k)
     .         +     fmagnh(iv_nua) *vaysLB(i, my ,k,iv_nua)
            END DO
          END DO
 
 
c #OB       DO k=1     ,kd_nua
c #OB               f__LBC( 1, 1,k) = (f__LBC(   1,jp11,k)
c #OB.                                +f__LBC(ip11,   1,k)) *0.5d0
c #OB               f__LBC(mx, 1,k) = (f__LBC(  mx,jp11,k)
c #OB.                                +f__LBC( mx1,   1,k)) *0.5d0
c #OB               f__LBC( 1,my,k) = (f__LBC(   1, my1,k)
c #OB.                                +f__LBC(ip11, my ,k)) *0.5d0
c #OB               f__LBC(mx,my,k) = (f__LBC( mx , my1,k)
c #OB.                                +f__LBC( mx1, my ,k)) *0.5d0
c #OB       END DO
 
      END IF
 
 
!    Work Arrays Reset
!    =================
 
      DO k=1,mz
      DO j=1,my
      DO i=1,mx
        WKxyz1(i,j,k) = 0.0
        WKxyz2(i,j,k) = 0.0
      END DO
      END DO
      END DO
 
      return
      end
