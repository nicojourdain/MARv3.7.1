      subroutine INIsnd_vl(pij,ptopDY,sigmid,sigma,u_ij,v_ij)
 
C +------------------------------------------------------------------------+
C | MAR INPUT      ATMOS                                   17-02-2004  MAR |
C |   SubRoutine INIsnd_vl initializes                                     |
C |     HORIZONTAL WIND COMPONENTS                     vertical profiles   |
C |     from sounding data (observations or academic situation)            |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:    pij -> pstDY (i,j) : Model Pressure Thickness              |
C |   ^^^^^     ptopDY               Model Pressure Top                    |
C |             sigmid               Model Layer    Interface Coordinate   |
C |             sigma                Model Level              Coordinate   |
C |                                                                        |
C |   INPUT (via common)                                                   |
C |   ^^^^^     uuSND                Sounding       U-Wind    Speed        |
C |             vvSND                Sounding       V-Wind    Speed        |
C |                                                                        |
C |   OUTPUT:    u_ij       -> ugeoDY(i,j,1->mz)                           |
C |   ^^^^^^     v_ij       -> vgeoDY(i,j,1->mz)                           |
C |                                                                        |
C |   CAUTION:   non-zero loav generates erroneous results                 |
C |   ^^^^^^^                                                              |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
      include 'MARdim.inc'
      include 'MARSND.inc'
 
      real      pij   ,ptopDY
      real       sigma(mz)
      real      sigmid(mzz)
 
      real       u_ij(mz,2),v_ij(mz,2)
 
 
C +--Local  Variables
C +  ================
 
      integer   k     ,ksnd  ,loav
      real      gra   ,prl   ,pr1   ,pr2
      real      guu   ,uu1   ,uu2   ,uuav
      real      gvv   ,vv1   ,vv2   ,vvav
 
 
C +--Scheme Initialisation
C +  =====================
 
           gra     = -gravit / RDryAi
           loav    =       0
 
 
C +--Geostrophic Wind Vertical Profile
C +  =================================
 
           k       =  mz
           ksnd    =  1
C +...     ksnd    =  1 (when pSND(mz) -> pSND(0:mz), etc...)
 
C + - -do until
 100   CONTINUE
        if(k  .eq.1) then
           pr1     =  pij *0.5d1 *  sigma(1)     +ptopDY
        else
           pr1     =  pij *1.0d1 * sigmid(k)     +ptopDY
        end if
           pr2     =  pij *1.0d1 * sigmid(k+1)   +ptopDY
C +...     Factor 10 is needed for [cb] --> [mb]
 
C + - - do until
 110    CONTINUE
        if (pSND(ksnd,nSND).lt.pr2    ) go to 111
           ksnd    =  ksnd + 1
        go to 110
 111    CONTINUE
C + - - end do
 
           guu     = (uuSND(ksnd)      - uuSND(ksnd-1)     )
     .             / ( pSND(ksnd,nSND) -  pSND(ksnd-1,nSND))
           uu2     =  uuSND(ksnd-1)     +guu *(pr2-pSND(ksnd-1,nSND))
           gvv     = (vvSND(ksnd)      - vvSND(ksnd-1)     )
     .             / ( pSND(ksnd,nSND) -  pSND(ksnd-1,nSND))
           vv2     =  vvSND(ksnd-1)     +gvv *(pr2-pSND(ksnd-1,nSND))
 
        if(pSND(ksnd,nSND).ge.pr1    ) then
           uuav    =-(uu2          + uuSND(ksnd)     )
     .              *(pr2          -  pSND(ksnd,nSND)) *0.5
           vvav    =-(vv2          + vvSND(ksnd)     )
     .              *(pr2          -  pSND(ksnd,nSND)) *0.5
        else
           loav    =     0
           uuav    =  zero
           vvav    =  zero
        end if
 
C + - - do until
 120    CONTINUE
        if(pSND(ksnd,nSND).lt.pr1    ) go to 121
           ksnd    =  ksnd + 1
        go to 120
 121    CONTINUE
C + - - end do
 
           guu     = (uuSND(ksnd)      - uuSND(ksnd-1)     )
     .             / ( pSND(ksnd,nSND) -  pSND(ksnd-1,nSND))
           uu1     =  uuSND(ksnd-1)      + guu *(pr1 -pSND(ksnd-1,nSND))
           gvv     = (vvSND(ksnd)      - vvSND(ksnd-1)     )
     .             / ( pSND(ksnd,nSND) -  pSND(ksnd-1,nSND))
           vv1     =  vvSND(ksnd-1)      + gvv *(pr1 -pSND(ksnd-1,nSND))
 
        if(loav.gt.   0)          then
           uuav    =  uuav
     .             + (uuSND(ksnd-1)      + uu1       )
     .              *( pSND(ksnd-1,nSND) - pr1       ) *0.5
           vvav    =  vvav
     .             + (vvSND(ksnd-1)      + vv1       )
     .              *( pSND(ksnd-1,nSND) - pr1       ) *0.5
        else
           uuav    =
     .               (uu2                + uu1       )
     .              *(pr2                - pr1       ) *0.5
           vvav    =
     .               (vv2                + vv1       )
     .              *(pr2                - pr1       ) *0.5
        end if
 
 
C +--Layer Average
C +  =============
 
            uuav   =  uuav / (pr2-pr1)
            vvav   =  vvav / (pr2-pr1)
 
 
C +--Large Scale Wind Components in the MAR Coordinate System
C +  ========================================================
 
            u_ij(k,nSND)=  uuav
            v_ij(k,nSND)=  vvav
 
       if (k         .le.1      ) go to 101
           k       =  k    - 1
       go to 100
 101   CONTINUE
C + - -end do
 
      return
      end
