 
 
      function qsat0D(ttq,ss,pstar,pt,lsf)
 
C +------------------------------------------------------------------------+
C | MAR PHYSICS                                         Mc 30-05-2007  MAR |
C |   Function qsat0D computes the Saturation Specific Humidity    (kg/kg) |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT :   ttq            : Air Temperature                       (K) |
C |   ^^^^^^^   pstar * ss + pt: Pressure of sigma level ss          (kPa) |
C |                                                                        |
C |   OUTPUT :  esat: Saturation Vapor Pressure                      (hPa) |
C |   ^^^^^^^   qsat0D: Saturation Specific Humidity               (kg/kg) |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      real     qsat0D,ttq   ,ss    ,pstar ,pt
      integer  lsf
 
 
C +--Local  Variables
C +  ================
 
      real     pr    ,esat
      real     r273p1
 
 
C +--DATA
C +  ====
 
      data     r273p1/273.16e0/
 
      pr   = 10.d0 *(pstar *ss + pt)
C +...pr   : pressure (hPa)
 
      IF (ttq.ge.273.16d0.or.lsf.eq.0)                              THEN
 
        esat =  6.1078d0 * exp (5.138d0*log(r273p1/ttq))
     .                   * exp (6827.d0*(unun/r273p1-unun/ttq))
C +...  esat : saturated vapor pressure with respect to water
C +***  Dudhia (1989) JAS, (B1) and (B2) p.3103
C +     See also Pielke (1984), p.234 and Stull (1988), p.276
 
      ELSE
        esat =  6.107d0  * exp (6150.d0*(unun/r273p1-unun/ttq))
C +...  esat : saturated vapor pressure with respect to ice
C +***  Dudhia (1989) JAS, 1989, (B1) and (B2) p.3103
 
      END IF
 
C +     ******
        qsat0D = max(eps9  , .622d0*esat/(pr-.378d0*esat))
C +     ******
 
      return
      end
