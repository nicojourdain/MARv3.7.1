      subroutine DYNadv_ver
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS   SLOW                                    18-09-2001  MAR |
C |   SubRoutine DYNadv_ver  includes the Vertical  Advection Contribution |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT   (via common block)                                           |
C |   ^^^^^    iterun: Run   Iteration                        Counter      |
C |            uairDY, vairDY, pktaDY Values    / Time Step n              |
C |            uairDY  : x-wind speed component                     (m/s)  |
C |            vairDY  : y-wind speed component                     (m/s)  |
C |            pktaDY: potential temperature divided by 100.[kPa]**(R/Cp)  |
C |                                                                        |
C |   OUTPUT  (via common block)                                           |
C |   ^^^^^^   uairDY, vairDY, pktaDY Values    / Time Step n+1            |
C |                                                                        |
C |   METHOD:  Unstaggered Grid: 1st Accurate in Space Upstream Scheme     |
C |   ^^^^^^^^   Staggered Grid: 2nd Accurate in Space                     |
C |                                                                        |
C | # OPTIONS: #VA: Vertical Average preferred in Centered Conserv Scheme  |
C | # ^^^^^^^^ #NS: NO   Slip Surface BC used  in Centered Conserv Scheme  |
C | #          #UR: Upper Radiating Condition (to be corrected, DO'NT USE) |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_DY.inc'
c #cA include 'MAR_CA.inc'
 
      include 'MAR_WK.inc'
 
c #UR real     uairUP, vairUP, pktaUP
c #UR common  /DYNadv_ver_var/ uairUP(mx,my,0:2),
c #UR.                         vairUP(mx,my,0:2),
c #UR.                         pktaUP(mx,my,0:2)
c #WA integer                  nadvrd
c #WA common  /DYNadv_ver_loc/ nadvrd
 
      logical centrL
c #ZU logical adv3rd
c #ZU real    gat(mx,my,mz),ga0(mx,my)
 
 
C +--Local  Variables
C +  ================
 
      integer itimax,ntimax,       nt__UP,nt    ,kk
      real    cflmax,cflsig,faccfl,dt__UP,dt_sig,dsgm
      real    uair_0,uair_c,uair_1,uair_2,uair_d
      real    vair_0,vair_c,vair_1,vair_2,vair_d
      real    pkta_0,pkta_c,pkta_1,pkta_2,pkta_d
      real    old__u,old__v,old__t
 
 
C +--DATA
C +  ====
 
c #ZU data adv3rd/ .true./
 
      data centrL/ .true./
c #UP      centrL=.false.
 
 
C +--Initialization of the Upper Radiating Boundary Condition
C +  ========================================================
 
c #UR IF (iterun.eq.1)                                            THEN
c #UR   DO       k=    0 ,     2
c #UR           kk=max(1 ,     k)
c #UR   DO       j=jp11,my1
c #UR   DO       i=ip11,mx1
c #UR     uairUP(i,j,k) = uairDY(i,j,kk)
c #UR     vairUP(i,j,k) = vairDY(i,j,kk)
c #UR     pktaUP(i,j,k) = pktaDY(i,j,kk)
c #UR   END DO
c #UR   END DO
c #UR   END DO
c #UR END IF
 
 
C +--Slip condition for Mountain Wave Experiments
C +  ============================================
 
c #OM   DO       j=jp11,my1
c #OM   DO       i=ip11,mx1
c #OM     psigDY(i,j,mz)=0.0
c #OM   END DO
c #OM   END DO
 
 
C +--First and Second Order Schemes
C +  ==============================
 
c #ZU IF (.not.adv3rd)                                            THEN
 
 
C +--Courant Number
C +  --------------
 
           cflmax     = 0.0
 
C +--Centered second Order Scheme on a       staggered Grid
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
       IF (centrL.and.staggr)                                     THEN
 
c #WA     write(6,6001)iterun
 6001     format(i6,' 6001       centrL .and.       staggr /CFL Number')
 
          DO       k=1,mz
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz7(i,j,k)=dt*psigDY(i,j,k)
     .                     /(pstDYn(i,j)*dsigm1(k)*2.0)
            cflsig     = abs(WKxyz7(i,j,k)+WKxyz7(i,j,k))
            cflmax     = max(cflsig,cflmax)
          END DO
          END DO
          END DO
 
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz8(i,j,1)=                 0.00
c #UR       WKxyz8(i,j,1)=dt*psigDY(i,j,1)*0.33
c #UR.                     /(pstDYn(i,j)*dsigm1(1)*2.)
C +...      WKxyz8(i,j,1)<--"psigDY(i,j,0)"
 
c #UR       cflsig     = abs(WKxyz8(i,j,k)+WKxyz8(i,j,k))
c #UR       cflmax     = max(cflsig,cflmax)
          END DO
          END DO
 
          DO       k=kp1(1),mz
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz8(i,j,k)=dt*psigDY(i,j,km1(k))
     .                     /(pstDYn(i,j)*dsigm1(k)*2.)
            cflsig     = abs(WKxyz8(i,j,k)+WKxyz8(i,j,k))
            cflmax     = max(cflsig,cflmax)
          END DO
          END DO
          END DO
 
       ELSE
 
C +--Upstream first  Order Scheme on a       staggered Grid
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        IF           (staggr)                                     THEN
 
c #WA     write(6,6002)iterun
 6002     format(i6,' 6002 .not. centrL .and.       staggr /Wind Speed')
 
          DO       k=kp1(1),mz
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz8(i,j,k)=(psigDY(i,j,k-1)*dsig_1(k-1)
     .                    +psigDY(i,j,k)  *dsig_1(k))
     .                   /(dsig_1    (k-1)+dsig_1(k))
          END DO
          END DO
          END DO
 
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz8(i,j,1)= psigDY(i,j,1)  *dsig_1(1)
     .                   /(dsig_1    (0)  +dsig_1(1))
          END DO
          END DO
 
C +--Upstream first  Order Scheme on a  non  staggered Grid
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ELSE
 
c #WA     write(6,6003)iterun
 6003     format(i6,' 6003 (.not.)centrL.and. .not. staggr /Wind Speed')
 
          DO       k=1,mz
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz8(i,j,k)= psigDY(i,j,k)
          END DO
          END DO
          END DO
 
        END IF
 
C +--Centered second Order Scheme on a  non  staggered Grid
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        IF(centrL)                                                THEN
 
c #WA     write(6,6004)iterun
 6004     format(i6,' 6004        centrL.and. .not. staggr /CFL Number')
 
          DO       k=1,mz
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz7(i,j,k)=dt*WKxyz8(i,j,k)
     .                     /(pstDYn(i,j)*dsigm1(k)*2.)
            cflsig     = abs(WKxyz7(i,j,k))
            cflmax     = max(cflsig,cflmax)
          END DO
          END DO
          END DO
 
C +--Upstream first  Order Scheme on a (non) staggered Grid
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ELSE
 
c #WA     write(6,6005)iterun
 6005     format(i6,' 6005  .not. centrL.and.(.not.)staggr /CFL Number')
 
          DO          k=    1 ,mmz1
          DO          j=jp11,my1
          DO          i=ip11,mx1
            IF(WKxyz8(i,j,k).gt.0.0)                              THEN
               WKxyz7(i,j,k)=-dt*WKxyz8(i,j,k)/(pstDYn(i,j)*dsig_1(k-1))
            ELSE
               WKxyz7(i,j,k)=-dt*WKxyz8(i,j,k)/(pstDYn(i,j)*dsig_1(k  ))
            END IF
               cflsig       =abs(WKxyz7(i,j,k))
               cflmax       =max(cflsig,cflmax)
          END DO
          END DO
          END DO
 
                      k=  mz
          DO          j=jp11,my1
          DO          i=ip11,mx1
            IF(WKxyz8(i,j,k).gt.0.0)                              THEN
               WKxyz7(i,j,k)=-dt*WKxyz8(i,j,k)/(pstDYn(i,j)*dsig_1(k-1))
            ELSE
               WKxyz7(i,j,k)=-dt*WKxyz8(i,j,k)/(pstDYn(i,j)*dsig_1(k  ))
            END IF
               cflsig       =abs(WKxyz7(i,j,k))
               cflmax       =max(cflsig,cflmax)
          END DO
          END DO
 
          DO          j=1,my
          DO          i=1,mx
               WKxyz7(i,j,1)= 0.0
          END DO
          END DO
 
C +--Work Array Reset
C +  ~~~~~~~~~~~~~~~~
          DO          k=1,mz
          DO          j=1,my
          DO          i=1,mx
               WKxyz8(i,j,k)= 0.0
          END DO
          END DO
          END DO
 
        END IF
 
       END IF
 
 
C +--Set Up    of the Local Split Time Differencing
C +  ----------------------------------------------
 
            cflmax    = 2.0  *cflmax
C +...      restricted CFL Criterion
 
            ntimax    =       cflmax
       IF  (centrL)                                               THEN
            ntimax    = max(2,ntimax)
c #WA     write(6,6006)ntimax
 6006     format(i6,' 6006        centrL.and.(.not.)staggr /Nb Iterat.')
       ELSE
            ntimax    = max(1,ntimax)
c #WA     write(6,6007)ntimax
 6007     format(i6,' 6007  .not. centrL.and.(.not.)staggr /Nb Iterat.')
       END IF
 
C +--Update of CFL Number
C +  ~~~~~~~~~~~~~~~~~~~~
       IF  (ntimax.gt.1)                                          THEN
            faccfl       = 1.0           / ntimax
          DO       k=1,mz
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz7(i,j,k)= WKxyz7(i,j,k) * faccfl
            WKxyz8(i,j,k)= WKxyz8(i,j,k) * faccfl
          END DO
          END DO
          END DO
       END IF
 
C +--OUTPUT for Verification
C +  ~~~~~~~~~~~~~~~~~~~~~~~
c #WA  nadvrd      = nadvrd                 + 1
c #WA  write(6,6000) nadvrd,cflmax,ntimax
 6000  format(i6,' CFLmax ',3x,' ',3x,'  =',f7.4,
     .        6x,' ntimax ',8x,  '   =',i4)
 
 
C +--2nd Order Centered Energy conserving:  Local Split Time Differencing
C +  --------- (Haltiner & Williams 1980 7.2.2, (7-47b) p.220) ----------
C +            -----------------------------------------------
 
       IF   (centrL)                                              THEN
 
        IF  (staggr)                                              THEN
 
c #WA     write(6,6008)
 6008     format(6x,' 6008        centrL.and.       staggr /A Contrib.')
 
         DO  itimax=    1,ntimax
 
C +--First        internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          IF(itimax.eq. 1)                                        THEN
 
            DO       j=jp11,my1
 
C +--Vertical Differences
 
                      k=    1
                dsgm         =             2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzp(i,k)   = uairDY(i,j,k)
                WKxzq(i,k)   = vairDY(i,j,k)
                WKxzx(i,k)   = pktaDY(i,j,k)
 
c #VA           WKxzp(i,k)   =(uairDY(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +uairDY(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzq(i,k)   =(vairDY(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +vairDY(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzx(i,k)   =(pktaDY(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +pktaDY(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
 
              DO      i=ip11,mx1
                uair_0       =                  WKxzp(i,  k)
c #UR           uair_0       =                 uairUP(i,j,0)
                WKxza(i,k)   =( WKxzp(i,  k)  -uair_0)
                vair_0       =                  WKxzq(i,  k)
c #UR           vair_0       =                 vairUP(i,j,0)
                WKxzb(i,k)   =( WKxzq(i,  k)  -vair_0)
                pkta_0       =                  WKxzx(i,  k)
c #UR           pkta_0       =                 pktaUP(i,j,0)
                WKxzc(i,k)   =( WKxzx(i,  k)  -pkta_0)
              END DO
 
              DO      k=kp1(1),mmz1
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzp(i,k)   = uairDY(i,j,k)
                WKxzq(i,k)   = vairDY(i,j,k)
                WKxzx(i,k)   = pktaDY(i,j,k)
 
c #VA           WKxzp(i,k)   =(uairDY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +uairDY(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +uairDY(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzq(i,k)   =(vairDY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +vairDY(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +vairDY(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzx(i,k)   =(pktaDY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +pktaDY(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +pktaDY(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
              END DO
 
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =( WKxzp(i,  k)  - WKxzp(i,  k-1))
                WKxzb(i,k)   =( WKxzq(i,  k)  - WKxzq(i,  k-1))
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
              END DO
 
                      k=       mmz
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)
              DO      i=ip11,mx1
                WKxzp(i,k)   = uairDY(i,j,k)
                WKxzq(i,k)   = vairDY(i,j,k)
                WKxzx(i,k)   = pktaDY(i,j,k)
 
c #VA           WKxzp(i,k)   =(uairDY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +uairDY(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
c #VA           WKxzq(i,k)   =(vairDY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +vairDY(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
c #VA           WKxzx(i,k)   =(pktaDY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +pktaDY(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
              END DO
 
              DO      i=ip11,mx1
                WKxza(i,k)   =( WKxzp(i,  k)  - WKxzp(i,  k-1))
                WKxzb(i,k)   =( WKxzq(i,  k)  - WKxzq(i,  k-1))
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
 
                      k=       mzz
              DO      i=ip11,mx1
                WKxy1(i,j)   = 0.0d+0
                WKxy2(i,j)   = 0.0d+0
                WKxy3(i,j)   = 0.0d+0
c #NS           WKxy1(i,j)   =                - WKxzp(i,  k-1)
c #NS           WKxy2(i,j)   =                - WKxzq(i,  k-1)
c #NS           WKxy3(i,j)   =(pktaDY(i,j,k)  - WKxzx(i,  k-1))
              END DO
 
C +--Advection Contribution
 
              DO       k=    1 ,mmz1
              DO       i=ip11,mx1
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxza(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxza(i,k)
                WKxyz1(i,j,k)= uairDY(i,j,k)  -WKxzd(i,k)
                WKxyz4(i,j,k)= uairDY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
 
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxzb(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxzb(i,k)
                WKxyz2(i,j,k)= vairDY(i,j,k)  -WKxzd(i,k)
                WKxyz5(i,j,k)= vairDY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
 
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxzc(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxzc(i,k)
                WKxyz3(i,j,k)= pktaDY(i,j,k)  -WKxzd(i,k)
                WKxyz6(i,j,k)= pktaDY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
              END DO
              END DO
 
                       k=       mmz
              DO       i=ip11,mx1
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy1(i,j)
     .                        +WKxyz8(i,j,k)  *WKxza(i,k)
                WKxyz1(i,j,k)= uairDY(i,j,k)  -WKxzd(i,k)
                WKxyz4(i,j,k)= uairDY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
 
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy2(i,j)
     .                        +WKxyz8(i,j,k)  *WKxzb(i,k)
                WKxyz2(i,j,k)= vairDY(i,j,k)  -WKxzd(i,k)
                WKxyz5(i,j,k)= vairDY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
 
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy3(i,j)
     .                        +WKxyz8(i,j,k)  *WKxzc(i,k)
                WKxyz3(i,j,k)= pktaDY(i,j,k)  -WKxzd(i,k)
                WKxyz6(i,j,k)= pktaDY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
              END DO
 
            END DO
 
C +--Intermediary internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          ELSE IF (itimax.lt.ntimax)                              THEN
 
C +--Vertical Differences
 
            DO       j=jp11,my1
 
                      k=    1
                dsgm         =             2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzp(i,k)   = WKxyz4(i,j,k)
                WKxzq(i,k)   = WKxyz5(i,j,k)
                WKxzx(i,k)   = WKxyz6(i,j,k)
 
c #VA           WKxzp(i,k)   =(WKxyz4(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz4(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzq(i,k)   =(WKxyz5(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz5(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz6(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
 
              DO      i=ip11,mx1
                uair_0       =                  WKxzp(i,  k)
c #UR           uair_0       =                 uairUP(i,j,0)
                WKxza(i,k)   =( WKxzp(i,  k)  -uair_0)
                vair_0       =                  WKxzq(i,  k)
c #UR           vair_0       =                 vairUP(i,j,0)
                WKxzb(i,k)   =( WKxzq(i,  k)  -vair_0)
                pkta_0       =                  WKxzx(i,  k)
c #UR           pkta_0       =                 pktaUP(i,j,0)
                WKxzc(i,k)   =( WKxzx(i,  k)  -pkta_0)
              END DO
 
              DO      k=kp1(1),mmz1
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzp(i,k)   = WKxyz4(i,j,k)
                WKxzq(i,k)   = WKxyz5(i,j,k)
                WKxzx(i,k)   = WKxyz6(i,j,k)
 
c #VA           WKxzp(i,k)   =(WKxyz4(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz4(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz4(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzq(i,k)   =(WKxyz5(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz5(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz5(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz6(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz6(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
              END DO
 
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =( WKxzp(i,  k)  - WKxzp(i,  k-1))
                WKxzb(i,k)   =( WKxzq(i,  k)  - WKxzq(i,  k-1))
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
              END DO
 
                      k=       mmz
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)
              DO      i=ip11,mx1
                WKxzp(i,k)   = WKxyz4(i,j,k)
                WKxzq(i,k)   = WKxyz5(i,j,k)
                WKxzx(i,k)   = WKxyz6(i,j,k)
 
c #VA           WKxzp(i,k)   =(WKxyz4(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz4(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
c #VA           WKxzq(i,k)   =(WKxyz5(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz5(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz6(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
              END DO
 
              DO      i=ip11,mx1
                WKxza(i,k)   =( WKxzp(i,  k)  - WKxzp(i,  k-1))
                WKxzb(i,k)   =( WKxzq(i,  k)  - WKxzq(i,  k-1))
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
 
                      k=       mzz
              DO      i=ip11,mx1
                WKxy1(i,j)   = 0.0
                WKxy2(i,j)   = 0.0
                WKxy3(i,j)   = 0.0
c #NS           WKxy1(i,j)   =                - WKxzp(i,  k-1)
c #NS           WKxy2(i,j)   =                - WKxzq(i,  k-1)
c #NS           WKxy3(i,j)   =(pktaDY(i,j,k)  - WKxzx(i,  k-1))
              END DO
 
C +--Advection Contribution
 
              DO      k=    1 ,mmz1
              DO      i=ip11,mx1
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxza(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxza(i,k)
                old__u       = WKxyz1(i,j,k)
                WKxyz1(i,j,k)= WKxyz4(i,j,k)
                WKxyz4(i,j,k)= old__u        -(WKxzd(i,k)+WKxzd(i,k))
 
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxzb(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxzb(i,k)
                old__v       = WKxyz2(i,j,k)
                WKxyz2(i,j,k)= WKxyz5(i,j,k)
                WKxyz5(i,j,k)= old__v        -(WKxzd(i,k)+WKxzd(i,k))
 
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxzc(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxzc(i,k)
                old__t       = WKxyz3(i,j,k)
                WKxyz3(i,j,k)= WKxyz6(i,j,k)
                WKxyz6(i,j,k)= old__t        -(WKxzd(i,k)+WKxzd(i,k))
              END DO
              END DO
 
                      k=       mmz
              DO      i=ip11,mx1
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy1(i,j)
     .                        +WKxyz8(i,j,k)  *WKxza(i,k)
                old__u       = WKxyz1(i,j,k)
                WKxyz1(i,j,k)= WKxyz4(i,j,k)
                WKxyz4(i,j,k)= old__u        -(WKxzd(i,k)+WKxzd(i,k))
 
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy2(i,j)
     .                        +WKxyz8(i,j,k)  *WKxzb(i,k)
                old__v       = WKxyz2(i,j,k)
                WKxyz2(i,j,k)= WKxyz5(i,j,k)
                WKxyz5(i,j,k)= old__v        -(WKxzd(i,k)+WKxzd(i,k))
 
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy3(i,j)
     .                        +WKxyz8(i,j,k)  *WKxzc(i,k)
                old__t       = WKxyz3(i,j,k)
                WKxyz3(i,j,k)= WKxyz6(i,j,k)
                WKxyz6(i,j,k)= old__t        -(WKxzd(i,k)+WKxzd(i,k))
              END DO
 
            END DO
 
C +--Last         internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          ELSE
 
            DO       j=jp11,my1
 
C +--Vertical Differences
 
                      k=    1
                dsgm         =             2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzp(i,k)   = WKxyz4(i,j,k)
                WKxzq(i,k)   = WKxyz5(i,j,k)
                WKxzx(i,k)   = WKxyz6(i,j,k)
 
c #VA           WKxzp(i,k)   =(WKxyz4(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz4(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzq(i,k)   =(WKxyz5(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz5(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz6(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
 
              DO      i=ip11,mx1
                uair_0       =                  WKxzp(i,  k)
c #UR           uair_0       =                 uairUP(i,j,0)
                WKxza(i,k)   =( WKxzp(i,  k)  -uair_0)
                vair_0       =                  WKxzq(i,  k)
c #UR           vair_0       =                 vairUP(i,j,0)
                WKxzb(i,k)   =( WKxzq(i,  k)  -vair_0)
                pkta_0       =                  WKxzx(i,  k)
c #UR           pkta_0       =                 pktaUP(i,j,0)
                WKxzc(i,k)   =( WKxzx(i,  k)  -pkta_0)
              END DO
 
              DO      k=kp1(1),mmz1
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzp(i,k)   = WKxyz4(i,j,k)
                WKxzq(i,k)   = WKxyz5(i,j,k)
                WKxzx(i,k)   = WKxyz6(i,j,k)
 
c #VA           WKxzp(i,k)   =(WKxyz4(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz4(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz4(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzq(i,k)   =(WKxyz5(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz5(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz5(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz6(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz6(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
              END DO
 
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =( WKxzp(i,  k)  - WKxzp(i,  k-1))
                WKxzb(i,k)   =( WKxzq(i,  k)  - WKxzq(i,  k-1))
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
              END DO
 
                      k=       mmz
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)
              DO      i=ip11,mx1
                WKxzp(i,k)   = WKxyz4(i,j,k)
                WKxzq(i,k)   = WKxyz5(i,j,k)
                WKxzx(i,k)   = WKxyz6(i,j,k)
 
c #VA           WKxzp(i,k)   =(WKxyz4(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz4(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
c #VA           WKxzq(i,k)   =(WKxyz5(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz5(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz6(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
              END DO
 
              DO      i=ip11,mx1
                WKxza(i,k)   =( WKxzp(i,  k)  - WKxzp(i,  k-1))
                WKxzb(i,k)   =( WKxzq(i,  k)  - WKxzq(i,  k-1))
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
 
                      k=       mzz
              DO      i=ip11,mx1
                WKxy1(i,j)   = 0.0d+0
                WKxy2(i,j)   = 0.0d+0
                WKxy3(i,j)   = 0.0d+0
c #NS           WKxy1(i,j)   =                - WKxzp(i,  k-1)
c #NS           WKxy2(i,j)   =                - WKxzq(i,  k-1)
c #NS           WKxy3(i,j)   =(pktaDY(i,j,k)  - WKxzx(i,  k-1))
              END DO
 
C +--Wind     Advection
 
              DO             k=    1 ,mmz1
              DO           i=ip11,mx1
                    uairDY(i,j,k) = WKxyz1(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxza(i,k+1)
     .                             +WKxyz8(i,j,k)  *WKxza(i,k  ))
                    vairDY(i,j,k) = WKxyz2(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxzb(i,k+1)
     .                             +WKxyz8(i,j,k)  *WKxzb(i,k  ))
              END DO
              END DO
 
                             k=       mmz
              DO           i=ip11,mx1
                    uairDY(i,j,k) = WKxyz1(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxy1(i,j)
     .                             +WKxyz8(i,j,k)  *WKxza(i,k))
                    vairDY(i,j,k) = WKxyz2(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxy2(i,j)
     .                             +WKxyz8(i,j,k)  *WKxzb(i,k))
              END DO
 
C +--Pot.Temp.Advect.avoids double Counting in case of convective Adjustment
 
              DO             k=    1 ,mmz1
              DO           i=ip11,mx1
c #cA           IF (adj_CA(i,j).eq.0)                             THEN
                    pktaDY(i,j,k) = WKxyz3(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxzc(i,k+1)
     .                             +WKxyz8(i,j,k)  *WKxzc(i,k  ))
c #cA           END IF
              END DO
              END DO
 
                             k=       mmz
              DO           i=ip11,mx1
c #cA           IF (adj_CA(i,j).eq.0)                             THEN
                    pktaDY(i,j,k) = WKxyz3(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxy3(i,j)
     .                             +WKxyz8(i,j,k)  *WKxzc(i,k))
c #cA           END IF
              END DO
 
            END DO
 
          END IF
 
 
C +--End of the                             Local Split Time Differencing
C +  --------------------------------------------------------------------
 
         END DO
 
 
C +--2nd Order Centered Leap-Frog Backward: Local Split Time Differencing
C +  --------------------------------------------------------------------
 
        ELSE
 
c #WA     write(6,6009)
 6009     format(6x,' 6009        centrL.and. .not. staggr /A Contrib.')
 
         DO  itimax=    1,ntimax
 
C +--First        internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          IF(itimax.eq. 1)                                        THEN
 
            DO       j=jp11,my1
 
C +--Advection Increment
 
                     k=    1
              DO     i=ip11,mx1
                uair_0       =                 uairDY(i,j,k)
c #UR           uair_0       =                 uairUP(i,j,0)
                WKxza(i,k)   =(uairDY(i,j,k+1)-uair_0)
     .                        *WKxyz7(i,j,k)
                vair_0       =                 vairDY(i,j,k)
c #UR           vair_0       =                 vairUP(i,j,0)
                WKxzb(i,k)   =(vairDY(i,j,k+1)-vairDY(i,j,k  ))
     .                        *WKxyz7(i,j,k)
                pkta_0       =                 pktaDY(i,j,k)
c #UR           pkta_0       =                 pktaUP(i,j,0)
                WKxzc(i,k)   =(pktaDY(i,j,k+1)-pktaDY(i,j,k  ))
     .                        *WKxyz7(i,j,k)
              END DO
 
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =(uairDY(i,j,k+1)-uairDY(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =(vairDY(i,j,k+1)-vairDY(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(pktaDY(i,j,k+1)-pktaDY(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
              END DO
 
                      k=       mmz
              DO      i=ip11,mx1
                WKxza(i,k)   =                -uairDY(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =                -vairDY(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(pktaDY(i,j,k+1)-pktaDY(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
 
C +--Advection Contribution
 
              DO       k=    1 ,mmz
              DO       i=ip11,mx1
                WKxyz1(i,j,k)= uairDY(i,j,k)  -WKxza(i,k)
                WKxyz4(i,j,k)= uairDY(i,j,k) -(WKxza(i,k)+WKxza(i,k))
                WKxyz2(i,j,k)= vairDY(i,j,k)  -WKxzb(i,k)
                WKxyz5(i,j,k)= vairDY(i,j,k) -(WKxzb(i,k)+WKxzb(i,k))
                WKxyz3(i,j,k)= pktaDY(i,j,k)  -WKxzc(i,k)
                WKxyz6(i,j,k)= pktaDY(i,j,k) -(WKxzc(i,k)+WKxzc(i,k))
              END DO
              END DO
            END DO
 
C +--Intermediary internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          ELSE IF (itimax.lt.ntimax)                              THEN
 
C +--Advection Increment
 
            DO       j=jp11,my1
 
                     k=    1
              DO     i=ip11,mx1
                uair_0       =                 WKxyz4(i,j,k)
c #UR           uair_0       =                 uairUP(i,j,0)
                vair_0       =                 WKxyz5(i,j,k)
c #UR           vair_0       =                 vairUP(i,j,0)
                pkta_0       =                 WKxyz6(i,j,k)
c #UR           pkta_0       =                 pktaUP(i,j,0)
 
                WKxza(i,k)   =(WKxyz4(i,j,k+1)-uair_0)
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =(WKxyz5(i,j,k+1)-vair_0)
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(WKxyz6(i,j,k+1)-pkta_0)
     .                        *WKxyz7(i,j,k)
              END DO
 
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =(WKxyz4(i,j,k+1)-WKxyz4(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =(WKxyz5(i,j,k+1)-WKxyz5(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(WKxyz6(i,j,k+1)-WKxyz6(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
              END DO
 
                      k=       mmz
              DO      i=ip11,mx1
                WKxza(i,k)   =                -WKxyz4(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =                -WKxyz5(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(pktaDY(i,j,k+1)-WKxyz6(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
 
C +--Advection Contribution
 
              DO      k=    1 ,mmz
              DO      i=ip11,mx1
                old__u       = WKxyz1(i,j,k)
                WKxyz1(i,j,k)= WKxyz4(i,j,k)
                WKxyz4(i,j,k)= old__u        -(WKxza(i,k)+WKxza(i,k))
                old__v       = WKxyz2(i,j,k)
                WKxyz2(i,j,k)= WKxyz5(i,j,k)
                WKxyz5(i,j,k)= old__v        -(WKxzb(i,k)+WKxzb(i,k))
                old__t       = WKxyz3(i,j,k)
                WKxyz3(i,j,k)= WKxyz6(i,j,k)
                WKxyz6(i,j,k)= old__t        -(WKxzc(i,k)+WKxzc(i,k))
              END DO
              END DO
 
            END DO
 
C +--Last         internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          ELSE
 
            DO       j=jp11,my1
 
C +--Advection Increment
 
                      k=    1
              DO      i=ip11,mx1
                uair_0       =                 WKxyz4(i,j,k)
c #UR           uair_0       =                 uairUP(i,j,0)
                vair_0       =                 WKxyz5(i,j,k)
c #UR           vair_0       =                 vairUP(i,j,0)
                pkta_0       =                 WKxyz6(i,j,k)
c #UR           pkta_0       =                 pktaUP(i,j,0)
 
                WKxza(i,k)   =(WKxyz4(i,j,k+1)-uair_0)
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =(WKxyz5(i,j,k+1)-vair_0)
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(WKxyz6(i,j,k+1)-pkta_0)
     .                        *WKxyz7(i,j,k)
              END DO
 
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =(WKxyz4(i,j,k+1)-WKxyz4(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =(WKxyz5(i,j,k+1)-WKxyz5(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(WKxyz6(i,j,k+1)-WKxyz6(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
              END DO
 
                      k=       mmz
              DO      i=ip11,mx1
                WKxza(i,k)   =                -WKxyz4(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =                -WKxyz5(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(pktaDY(i,j,k+1)-WKxyz6(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
 
C +--Wind     Advection
 
              DO             k=    1 ,mmz
                DO           i=ip11,mx1
                      uairDY(i,j,k) = WKxyz1(i,j,k) -WKxza(i,k)
                      vairDY(i,j,k) = WKxyz2(i,j,k) -WKxzb(i,k)
                END DO
 
C +--Pot.Temp.Advect.avoids double Counting in case of convective Adjustment
 
                DO           i=ip11,mx1
c #cA             IF (adj_CA(i,j).eq.0)                           THEN
                      pktaDY(i,j,k) = WKxyz3(i,j,k) -WKxzc(i,k)
c #cA             END IF
                END DO
              END DO
 
            END DO
 
          END IF
 
 
C +--End of the                             Local Split Time Differencing
C +  --------------------------------------------------------------------
 
         END DO
 
        END IF
 
 
C +--First  Order Upstream Scheme:          Local Split Time Differencing
C +  --------------------------------------------------------------------
 
       ELSE
 
c #WA     write(6,6010)
 6010     format(6x,' 6010  .not. centrL.and.(.not.)staggr /A Contrib.')
 
         DO  itimax=    1,ntimax
 
C +--Auxiliary Variables
C +  ~~~~~~~~~~~~~~~~~~~
c #WA     write(6,6011)itimax,WKxyz1(imez,jmez,mz1),WKxyz1(imez,jmez,mz)
c #WA.                       ,uairDY(imez,jmez,mz1),uairDY(imez,jmez,mz)
 6011     format(6x,' 6011  .not. centrL.and.(.not.)staggr /A Contrib.',
     .                        4f9.6)
 
             DO       k=1,mz
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz1(i,j,k)= uairDY(i,j,k)
               WKxyz2(i,j,k)= vairDY(i,j,k)
               WKxyz3(i,j,k)= pktaDY(i,j,k)
             END DO
             END DO
             END DO
 
C +--Vertical Differences
C +  ~~~~~~~~~~~~~~~~~~~~
                      k=1
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz4(i,j,k) = 0.0d+0
               WKxyz5(i,j,k) = 0.0d+0
               WKxyz6(i,j,k) = 0.0d+0
c #UR          WKxyz4(i,j,k) = WKxyz1(i,j,k)-uairUP(i,j,0)
c #UR          WKxyz5(i,j,k) = WKxyz2(i,j,k)-vairUP(i,j,0)
c #UR          WKxyz6(i,j,k) = WKxyz3(i,j,k)-pktaUP(i,j,0)
             END DO
             END DO
 
             DO       k=kp1(1),mz
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz4(i,j,k) = WKxyz1(i,j,k)-WKxyz1(i,j,k-1)
               WKxyz5(i,j,k) = WKxyz2(i,j,k)-WKxyz2(i,j,k-1)
               WKxyz6(i,j,k) = WKxyz3(i,j,k)-WKxyz3(i,j,k-1)
             END DO
             END DO
             END DO
 
                      k=  mzz
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxy1 (i,j)   =              -WKxyz1(i,j,k-1)
               WKxy2 (i,j)   =              -WKxyz2(i,j,k-1)
               WKxy3 (i,j)   = pktaDY(i,j,k)-WKxyz3(i,j,k-1)
             END DO
             END DO
C +
C +--Advection Contribution
C +  ~~~~~~~~~~~~~~~~~~~~~~
             DO       k=    1 ,mmz1
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz1(i,j,k)=uairDY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz4(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxyz4(i,j,k+1)
               WKxyz2(i,j,k)=vairDY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz5(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxyz5(i,j,k+1)
               WKxyz3(i,j,k)=pktaDY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz6(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxyz6(i,j,k+1)
             END DO
             END DO
             END DO
 
                      k=mz
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz1(i,j,k)=uairDY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz4(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxy1( i,j)
               WKxyz2(i,j,k)=vairDY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz5(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxy2( i,j)
               WKxyz3(i,j,k)=pktaDY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz6(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxy3( i,j)
             END DO
             END DO
 
C +--Wind    Update
C +  ~~~~~~~~~~~~~~
             DO             k=1,mz
             DO             j=jp11,my1
               DO           i=ip11,mx1
                     uairDY(i,j,k) = WKxyz1(i,j,k)
                     vairDY(i,j,k) = WKxyz2(i,j,k)
               END DO
 
C +--Pot.Temp.Update avoids double Counting in case of convective Adjustment
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
               DO           i=ip11,mx1
c #cA            IF (adj_CA(i,j).eq.0)                            THEN
                     pktaDY(i,j,k) = WKxyz3(i,j,k)
c #cA            END IF
               END DO
             END DO
             END DO
 
 
C +--End of the                             Local Split Time Differencing
C +  --------------------------------------------------------------------
 
c #WA     write(6,6012)itimax,WKxyz1(imez,jmez,mz1),WKxyz1(imez,jmez,mz)
c #WA.                       ,uairDY(imez,jmez,mz1),uairDY(imez,jmez,mz)
 6012     format(6x,' 6012  .not. centrL.and.(.not.)staggr /A Contrib.',
     .                        4f9.6)
         END DO
 
       END IF
 
 
C +--Upper Radiating Boundary Condition
C +  ----------------------------------
 
c #UR   DO           j=jp11,my1
c #UR   DO           i=ip11,mx1
c #UR       uair_c =  zero
c #UR       uair_1 = (uairDY(i,j,1)+uairUP(i,j,1)) * 0.5
c #UR       uair_2 = (uairDY(i,j,2)+uairUP(i,j,2)) * 0.5
c #UR       uair_d =  uair_2       -uair_1
c #UR    IF(uair_d.ne.zero)
c #UR.      uair_c =-(uairDY(i,j,1)-uairUP(i,j,1))*dsig_1(1)/(dt*uair_d)
c #UR    IF(uair_c.lt.zero)                                         THEN
c #UR       dt__UP = -sigma(1) / uair_c
c #UR       nt__UP =  dt       / dt__UP
c #UR       nt__UP =       max(1,nt__UP)
c #UR       dt__UP =  dt       / nt__UP
c #UR       dt_sig =  dt__UP   * uair_c / sigma(1)
c #UR       DO  nt =           1,nt__UP
c #UR                 uairUP(i,j,0)=uairUP(i,j,0)
c #UR.              -(uair_1       -uairUP(i,j,0))*dt_sig
c #UR       END DO
c #UR    END IF
c #UR    DO  k = 1,2
c #UR                 uairUP(i,j,k)=uairDY(i,j,k)
c #UR    END DO
 
c #UR       vair_c =  zero
c #UR       vair_1 = (vairDY(i,j,1)+vairUP(i,j,1)) * 0.5
c #UR       vair_2 = (vairDY(i,j,2)+vairUP(i,j,2)) * 0.5
c #UR       vair_d =  vair_2       -vair_1
c #UR    IF(vair_d.ne.zero)
c #UR.      vair_c =-(vairDY(i,j,1)-vairUP(i,j,1))*dsig_1(1)/(dt*vair_d)
c #UR    IF(vair_c.lt.zero)                                         THEN
c #UR       dt__UP = -sigma(1) / vair_c
c #UR       nt__UP =  dt       / dt__UP
c #UR       nt__UP =       max(1,nt__UP)
c #UR       dt__UP =  dt       / nt__UP
c #UR       dt_sig =  dt__UP   * vair_c / sigma(1)
c #UR       DO  nt =           1,nt__UP
c #UR                 vairUP(i,j,0)=vairUP(i,j,0)
c #UR.              -(vair_1       -vairUP(i,j,0))*dt_sig
c #UR       END DO
c #UR    END IF
c #UR    DO  k = 1,2
c #UR                 vairUP(i,j,k)=vairDY(i,j,k)
c #UR    END DO
 
c #UR       pkta_c =  zero
c #UR       pkta_1 = (pktaDY(i,j,1)+pktaUP(i,j,1)) * 0.5
c #UR       pkta_2 = (pktaDY(i,j,2)+pktaUP(i,j,2)) * 0.5
c #UR       pkta_d =  pkta_2       -pkta_1
c #UR    IF(pkta_d.ne.zero)
c #UR.      pkta_c =-(pktaDY(i,j,1)-pktaUP(i,j,1))*dsig_1(1)/(dt*pkta_d)
c #UR    IF(pkta_c.lt.zero)                                         THEN
c #UR       dt__UP = -sigma(1) / pkta_c
c #UR       nt__UP =  dt       / dt__UP
c #UR       nt__UP =       max(1,nt__UP)
c #UR       dt__UP =  dt       / nt__UP
c #UR       dt_sig =  dt__UP   * pkta_c / sigma(1)
c #UR       DO  nt =           1,nt__UP
c #UR                 pktaUP(i,j,0)=pktaUP(i,j,0)
c #UR.              -(pkta_1       -pktaUP(i,j,0))*dt_sig
c #UR       END DO
c #UR    END IF
c #UR    DO  k = 1,2
c #UR                 pktaUP(i,j,k)=pktaDY(i,j,k)
c #UR    END DO
 
c #UR   END DO
c #UR   END DO
 
 
C +--Work Arrays Reset
C +  -----------------
 
        DO       j=1,my
        DO       i=1,mx
          WKxy1( i,j)   = 0.0
          WKxy2( i,j)   = 0.0
          WKxy3( i,j)   = 0.0
        END DO
        END DO
 
        DO       k=1,mz
        DO       i=1,mx
          WKxza( i,  k) = 0.0
          WKxzb( i,  k) = 0.0
          WKxzc( i,  k) = 0.0
          WKxzd( i,  k) = 0.0
          WKxzp( i,  k) = 0.0
          WKxzq( i,  k) = 0.0
          WKxzx( i,  k) = 0.0
        END DO
        END DO
 
        DO       k=1,mz
        DO       j=1,my
        DO       i=1,mx
          WKxyz1(i,j,k) = 0.0
          WKxyz2(i,j,k) = 0.0
          WKxyz3(i,j,k) = 0.0
          WKxyz4(i,j,k) = 0.0
          WKxyz5(i,j,k) = 0.0
          WKxyz6(i,j,k) = 0.0
          WKxyz7(i,j,k) = 0.0
          WKxyz8(i,j,k) = 0.0
        END DO
        END DO
        END DO
 
 
C +--Third Order Vertical Scheme
C +  ===========================
 
c #ZU ELSE
 
c #ZU  DO  j=jp11,my1
c #ZU  DO  i=ip11,mx1
c #ZU  ga0(i,j)      = 0.0
c #ZO  ga0(i,j)      = uairDY(i,j,mz)
c #ZU  END DO
c #ZU  END DO
 
c #ZU  DO  k=1,mz
c #ZU  DO  j=jp11,my1
c #ZU  DO  i=ip11,mx1
c #ZU  gat(i,j,k)    = uairDY(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
 
C +    ****************
c #ZU  call DYNadv_cubv(gat,ga0)
C +    ****************
 
c #ZU  DO     k=1,mz
c #ZU  DO     j=jp11,my1
c #ZU  DO     i=ip11,mx1
c #ZU  uairDY(i,j,k) = gat(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
 
c #ZO  DO  j=jp11,my1
c #ZO  DO  i=ip11,mx1
c #ZO  ga0(i,j)      = vairDY(i,j,mz)
c #ZU  END DO
c #ZU  END DO
 
c #ZU  DO  k=1,mz
c #ZU  DO  j=jp11,my1
c #ZU  DO  i=ip11,mx1
c #ZU  gat(i,j,k)    = vairDY(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
 
C +    ****************
c #ZU  call DYNadv_cubv(gat,ga0)
C +    ****************
 
c #ZU  DO     k=1,mz
c #ZU  DO     j=jp11,my1
c #ZU  DO     i=ip11,mx1
c #ZU  vairDY(i,j,k) = gat(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
 
c #ZU  DO  j=jp11,my1
c #ZU  DO  i=ip11,mx1
c #ZU  ga0(i,j)      = pktaDY(i,j,mzz)
c #ZO  ga0(i,j)      = pktaDY(i,j,mz)
c #ZU  END DO
c #ZU  END DO
 
c #ZU  DO  k=1,mz
c #ZU  DO  j=jp11,my1
c #ZU  DO  i=ip11,mx1
c #ZU  gat(i,j,k)    = pktaDY(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
 
C +    ****************
c #ZU  call DYNadv_cubv(gat,ga0)
C +    ****************
 
c #ZU  DO     k=1,mz
c #ZU  DO     j=jp11,my1
c #ZU  DO     i=ip11,mx1
c #ZU  pktaDY(i,j,k) = gat(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
 
c #ZU end if
 
      return
      end
