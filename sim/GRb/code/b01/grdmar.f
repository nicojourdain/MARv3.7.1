 
 
      subroutine GRDmar
 
C +------------------------------------------------------------------------+
C | MAR GRID                                               20-02-2004  MAR |
C |   SubRoutine GRDmar is used to initialize the grid parameters          |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |    INPUT (via common block)                                            |
C |    ^^^^^    sigma(mz) : Independant Variable on      Levels            |
C |             FIslot    : Implicit Filter Parameter                      |
C |                                 (Slow Dynamics / Temperature)          |
C |             FIslou    : ...     (Slow Dynamics / Wind Speed)           |
C |             FIslop    : ...     (Slow Dynamics / Pressure)             |
C |                                                                        |
C |   OUTPUT (via common block)                                            |
C |   ^^^^^^     xxkm(mx) : Distance along the x-axis                 (km) |
C |              yykm(my) : Distance along the y-axis                 (km) |
C |                                                                        |
C |            sigmid(mzz): Independant Variable between Levels (i.e.k-1/2)|
C |            dsigm1(mz ):      Difference  d(sigma)|k                    |
C |            qsigm1(mz ): 1 / [Difference  d(sigma)|k    ]               |
C |            dsigm2(mz ):      Difference 2d(sigma)|k                    |
C |            qsigm2(mz ): 1 / [Difference 2d(sigma)|k    ]               |
C |            dsig_1(mzz):      Difference  d(sigma)|k+1/2                |
C |            qsig_1(mzz): 1 / [Difference  d(sigma)|k+1/2]               |
C |            dsig_2(mzz):      Difference 2d(sigma)|k+1/2                |
C |                                                                        |
C |            Ray_UB(mzabso) : Top Absorbing Layer  Contribution to       |
C |                             Rayleigh             Friction        (-/s) |
C |                                                                        |
C |            TUspon(mzabso) : Top Absorbing Layer  Contribution to       |
C |                             Horizontal Diffusion Coefficient    (m2/s) |
C |                                                                        |
C |            FIspon(mzabso) : Top Absorbing Layer  Contribution          |
C |                             to Implicit Filter Parameter               |
C |                    FIk_st(mz): Implicit Filter Parameter               |
C |                                   (Slow Dynamics / Temperature)        |
C |                    FIk_su(mz): ...(Slow Dynamics / Wind Speed)         |
C |            FIfstu, FIk_fu(mz): ...(Fast Dynamics / Wind Speed)         |
C |            FIfstu, FIk_fp(mz): ...(Fast Dynamics / Pressure,Velocity)  |
C |                                                                        |
C |            n6mxLB, n7mxLB : Effective Length of Lateral Sponge (x-Axe) |
C |            n6myLB, n7myLB : Effective Length of Lateral Sponge (y-Axe) |
C |                                                                        |
C |            im1(mx),2,..: max(i-1, 1), max(i-2, 1), etc...              |
C |            ip1(mx),2,..: min(i+1,mx), min(i+2,mx), etc...              |
C |                                                                        |
C |            jm1(my),2,..: max(j-1, 1), max(j-2, 1), etc...              |
C |            jp1(my),2,..: min(j+1,my), min(j+2,my), etc...              |
C |                                                                        |
C |            km1(mz),2,..: max(k-1, 1), max(k-2, 1), etc...              |
C |            kp1(mz),2,..: min(k+1,mz), min(k+2,mz), etc...              |
C |                                                                        |
C |            CUspxh(mx)  : Cubic Spline Auxiliary Variable (x Direction) |
C |            CUspxb(mx)  : idem                                          |
C |            CUspyh(mx)  : Cubic Spline Auxiliary Variable (y Direction) |
C |            CUspyb(mx)  : idem                                          |
C |            CUspzh(mx)  : Cubic Spline Auxiliary Variable (z Direction) |
C |            CUspzb(mx)  : idem                                          |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_CU.inc'
      include 'MAR_LB.inc'
      include 'MAR_UB.inc'
 
c #NH include 'MAR_NH.inc'
 
      include 'MAR_TU.inc'
      include 'MAR_FI.inc'
 
      include 'MAR_IO.inc'
 
 
C +--Local  Variables
C +  ================
C +
      integer  im10,ip10,im20,ip20
      integer  jm10,jp10,jm20,jp20
      integer  km10,kp10,km20,mzabs
      real                    FIabs
C +
C +
C +--DATA
C +  ====
C +
      logical DFspon
      data    DFspon /.true./
c #KS         DFspon =.false.
C +
C +--Entry Checking Point
C +  ====================
C +
      if (IO_loc.ge.2) write(21,999)
 999  format(//,'   --- Initialisation / GRDmar ---')
C +
C +
C +--Auxiliary Horizontal Independant Variables
C +  ==========================================
C +
      mmx  = mx
      mmx1 = mx1
      mmx1 = max( 1,mmx1)
      mmx2 = mx-2
      mmx2 = max( 1,mmx2)
      mmx3 = mx-3
      mmx3 = max( 1,mmx3)
      mmx4 = mx-4
      mmx4 = max( 1,mmx4)
      mmx5 = mx-5
      mmx5 = max( 1,mmx5)
      mmx6 = mx-6
      mmx6 = max( 1,mmx6)
      m0x2 =    2
      m0x2 = min(mx,m0x2)
      m0x3 =    3
      m0x3 = min(mx,m0x3)
      m0x4 =    4
      m0x4 = min(mx,m0x4)
      m0x5 =    5
      m0x5 = min(mx,m0x5)
      m0x6 =    6
      m0x6 = min(mx,m0x6)
C +
      mmy  = my
      mmy1 = my1
      mmy1 = max( 1,mmy1)
      mmy2 = my-2
      mmy2 = max( 1,mmy2)
      mmy3 = my-3
      mmy3 = max( 1,mmy3)
      mmy4 = my-4
      mmy4 = max( 1,mmy4)
      mmy5 = my-5
      mmy5 = max( 1,mmy5)
      mmy6 = my-6
      mmy6 = max( 1,mmy6)
      m0y2 =    2
      m0y2 = min(my,m0y2)
      m0y3 =    3
      m0y3 = min(my,m0y3)
      m0y4 =    4
      m0y4 = min(my,m0y4)
      m0y5 =    5
      m0y5 = min(my,m0y5)
      m0y6 =    6
      m0y6 = min(my,m0y6)
C +
      mmz  = mz
      mmz1 = mz1
      mmz1 = max(1,mmz1)
      mmz2 = mz-2
      mmz2 = max(1,mmz2)
C +
      dx2=dx*2.0
      dy2=dy*2.0
C +
      IF (mmx.gt.1)                                               THEN
C +
        dtx=dt/dx
        dty=dt/dy
C +
        dxinv =1.0/dx
        dyinv =1.0/dy
        dxinv2=1.0/dx2
        dyinv2=1.0/dy2
C +
       DO i=1,mx
         xxkm(i) = 0.001 * (i-imez) *dx
       END DO
C +
       DO j=1,my
         yykm(j) = 0.001 * (j-jmez) *dy
       END DO
C +
      END IF
C +
C +
C +--4th Order Centered Difference Parameter
C +  ---------------------------------------
C +
       fac43 = 4.0/3.0
C +
C +
C +--Effective Length of the Lateral Sponge
C +  --------------------------------------
C +
      IF   (mmx.eq.1)                                             THEN
          n40xLB = 1
          n50xLB = 1
          n5mxLB = 1
          n6mxLB = 0
          n7mxLB = 1
          n40yLB = 1
          n50yLB = 1
          n5myLB = 1
          n6myLB = 0
          n7myLB = 1
C +
      ELSE
          n40xLB = mx-n6+2
          n50xLB = mx-n6+1
          n5mxLB = n6-1
          n6mxLB = n6
          n7mxLB = n7
C +
        IF (mmy.eq.1)                                             THEN
          n40yLB = 1
          n50yLB = 1
          n5myLB = 1
          n6myLB = 0
          n7myLB = 1
        ELSE
          n40yLB = my-n6+2
          n50yLB = my-n6+1
          n5myLB = n6-1
          n6myLB = n6
          n7myLB = n7
        END IF
C +
      END IF
C +
C +
C +--Boundaries Masks
C +  ----------------
C +
      DO i=1,mx
         im10   =     i-1
         ip10   =     i+1
         im1(i) = max(im10, 1)
         ip1(i) = min(ip10,mx)
         im20   =     i-2
         ip20   =     i+2
         im2(i) = max(im20, 1)
         ip2(i) = min(ip20,mx)
      END DO
C +
      DO j=1,my
         jm10   =     j-1
         jp10   =     j+1
         jm1(j) = max(jm10, 1)
         jp1(j) = min(jp10,my)
         jm20   =     j-2
         jp20   =     j+2
         jm2(j) = max(jm20, 1)
         jp2(j) = min(jp20,my)
      END DO
C +
C +
C +--Auxiliary Vertical   Independant Variables
C +  ==========================================
C +
C +
C +--Boundaries Masks
C +  ----------------
C +
      DO k=1,mz
         km10   =     k-1
         kp10   =     k+1
         km1(k) = max(km10, 1)
         kp1(k) = min(kp10,mz)
         km20   =     k-2
         km2(k) = max(km20, 1)
      END DO
C +
C +
C +--Discretisation
C +  --------------
C +
        dsig_1(0)      = sigma(1)
        dsig_1(1)      = sigma(kp1(1))   - sigma(1)
        dsig_2(1)      = sigma(kp1(1))
        sigmid(1)      = 0.0
        sigmid(mzz)    = 1.0
C +
      DO       k=kp1(1),mmz1
        dsig_1(k)      =  sigma(kp1(k)) -  sigma(k)
        dsig_2(k)      =  sigma(kp1(k)) -  sigma(km1(k))
        sigmid(k)      =( sigma(k)      +  sigma(km1(k))) /2.0
        dsigm1(km1(k)) = sigmid(k)      - sigmid(km1(k))
        dsigm2(km1(k)) = sigmid(k)      - sigmid(km2(k))
      END DO
C +
C +--The lowest layer of the model is assumed to be a constant flux layer
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        dsig_1(mz)      =         1.0       -  sigma(mz)
        dsig_2(mz)      =         sigma(mz) -  sigma(km1(mz))
        sigmid(mz)      = 0.50 *( sigma(mz) +  sigma(km1(mz)))
        dsigm1(km1(mz)) =        sigmid(mz) - sigmid(km1(mz))
        dsigm2(km1(mz)) =        sigmid(mz) - sigmid(km2(mz))
C +
        dsig_1(mzz)     =        dsig_1(mz)
        dsig_2(mzz)     = 2.00 * dsig_1(mzz)
        dsigm1(mz)      = 1.00 - sigmid(mz)
        dsigm2(mz)      = 1.00 - sigmid(km1(mz))
C +
      DO k=0,mzz
        qsig_1(k)       = 1.00 / dsig_1(k)
      END DO
C +
      DO k=1,mz
        qsigm1(k)       = 1.00 / dsigm1(k)
        qsigm2(k)       = 1.00 / dsigm2(k)
      END DO
C +
C +
C +--Filter Parameter    Initialisation (rapidly propagating Waves Dynamics)
C +  =======================================================================
C +
cXF
            FIslou=0.0075*sqrt(dt/60.) ! if too high, MAR crashes at
            FIslop=0.0075*sqrt(dt/60.) ! high resolution
 
            FIfstu    = FIslou /(ntFast+1)
            FIfstp    = FIslop /(ntFast+1)
C +
      DO k = 1,mz
cXF
            FIk_st(k) = FIslot / max(0.05,sqrt(sigma(k)))
            FIk_su(k) = FIslou / max(0.05,    (sigma(k)))
            FIk_fu(k) = FIfstu / max(0.05,    (sigma(k)))
            FIk_fp(k) = FIfstp / max(0.05,sqrt(sigma(k)))
      END DO
C +
C +
C +--Top Absorbing Layer Initialisation
C +  ==================================
C +
            FIabs     = TUkhmx *  4.0 *dtfast /(dx*dx)
C +
      IF   (mz.gt.1)                                              THEN
            mzabs=       mzabso+1
            mzabs=min(mz,mzabs)
        IF (DFspon)                                               THEN
          DO k = 1,mzabso
            FIspon(k) = FIabs  * (sigma(mzabs)-sigma(k))
     .                         / (sigma(mzabs)-sigma(1))
            FIk_st(k) = FIk_st(k) + FIspon(k) *dt/dtfast
            FIk_su(k) = FIk_su(k) + FIspon(k) *dt/dtfast
            FIk_fu(k) = FIk_fu(k) + FIspon(k)
            FIk_fp(k) = FIk_fp(k) + FIspon(k)
            TUspon(k) = zero
          END DO
        ELSE
          DO k = 1,mzabso
            FIspon(k) = zero
            TUspon(k) = TUkhmx * (sigma(mzabs)-sigma(k))
     .                         / (sigma(mzabs)-sigma(1))
          END DO
        END IF
      END IF
 
 
C +--Rayleigh Friction (Ref. ARPS 4.0 User's Guide, para 6.4.3 p.152)
C +  =================
 
      DO k=1,mzabso
        Ray_UB(k) = 0.5*(1.-cos(pi*(sigma(mzabso)-sigma(k))
     .                            /(sigma(mzabso)-sigma(1))))/( 1.5*dt)
c #rf   Ray_UB(k) =                (sigma(mzabso)-sigma(k))
c #rf.                           / (sigma(mzabso)-sigma(1))  /(10.0*dt)
      END DO
 
 
C +--Cubic Spline Initialisation
C +  ===========================
C +
C +  1) x - Direction
C +  ----------------
           CUspxh( 1) = 0.0
           CUspxh(mx) = 0.0
           CUspxb( 1) = 0.0
           CUspxb(mx) = 0.0
         DO i=ip11,mx1
           CUspxh( i) =       CUspxb(im1(i)) + 4.0
           CUspxb( i) =-1.0 / CUspxh(i)
         END DO
C +
C +  2) y - Direction
C +  ----------------
           CUspyh( 1) = 0.0
           CUspyh(my) = 0.0
           CUspyb( 1) = 0.0
           CUspyb(my) = 0.0
      IF (mmy.gt.1)                                               THEN
         DO j=jp11,my1
           CUspyh( j) =       CUspyb(jm1(j)) + 4.0
           CUspyb( j) =-1.0 / CUspyh(j)
         END DO
      END IF
C +
C +  3) Sigma - Direction (to be used in routine DYNadv_cubv)
C +  --------------------------------------------------------
c #ZU      CUspzh( 1)=dsig_1(1  ) /(dsig_1( 1)+ sigma(1))
c #ZU      CUspzh(mz)=dsig_1( mz) /(dsig_1(mz)+dsig_1(mmz1))
c #ZU      CUspzb( 1)= sigma(1)   /(dsig_1( 1)+ sigma(1))
c #ZU      CUspzb(mz)=dsig_1(mmz1)/(dsig_1(mz)+dsig_1(mmz1))
c #ZU    DO k=kp1(1),mmz1
c #ZU      CUspzh( k)=dsig_1(k  ) /(dsig_1(k)+dsig_1(k-1))
c #ZU      CUspzb( k)=dsig_1(k-1) /(dsig_1(k)+dsig_1(k-1))
c #ZU    END DO
C +
      return
      end
