      function SRFdtp_sno(pgra11,pgra21,pgra12,pgra22,
     .                    khist1,khist2,pro1  ,pro2  )
C +
C +------------------------------------------------------------------------+
C | MAR SURFACE                                            01-12-2001  MAR |
C |   Function SRFdtp_sno                                                  |
C |            evaluates the difference between grain types of two layers  |
C |            and returns a value between 0. and 200.                     |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   REFER. : Brun et al.      1989, J. Glaciol 35 pp. 333--342           |
C |   ^^^^^^^^ Brun et al.      1992, J. Glaciol 38 pp.  13-- 22           |
C |            (CROCUS Model, adapted to MAR at CEN by H.Gallee)           |
C |                                                                        |
C |   INPUT : pgra11  1e variable descriptive des grains de la 1e strate   |
C |   ^^^^^^^ pgra21  2e variable descriptive des grains de la 1e strate   |
C |           pgra21  2e variable descriptive des grains de la 1e strate   |
C |           pgra12  1e variable descriptive des grains de la 2e strate   |
C |           pgra22  2e variable descriptive des grains de la 2e strate   |
C |           khist1     variable historique  des grains de la 1e strate   |
C |           khist2     variable historique  des grains de la 2e strate   |
C |           pro1       masse    volumique              de la 1e strate   |
C |           pro2       masse    volumique              de la 2e strate   |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   Methode                                                              |
C |   -------                                                              |
C |      cas de la neige dendritique:                                      |
C |         1ere variable descriptive des grains: dendricite.              |
C |         2eme variable descriptive des grains: sphericite.              |
C |      cas de la neige non dendritique:                                  |
C |         1ere variable descriptive des grains: sphericite.              |
C |         2eme variable descriptive des grains: diametre des grains.     |
C |         variable historique des grains.                                |
C |      poids de ces variables dans le calcul de la difference:           |
C |         sphericite: vdtysp (   .5 par defaut)                          |
C |         masse vol.: vdtyro (500.  par defaut)                          |
C |         diametre  : vdtydi ( 10.  par defaut)                          |
C |         historique: vdtyhi (100.  par defaut)                          |
C |                                                                        |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +--General Variables
C +  =================
C +
      include 'MARphy.inc'
C +
      real     SRFdtp_sno
      real     pgra11,pgra21,pgra12,pgra22,pro1,pro2
      real     SNtypx,vdtysp,vdtyro,vdtydi,vdtyhi
      integer  khist1,khist2
C +
C +
C +--DATA
C +  ====
C +
      data SNtypx/ 200.0e0/
C +...     SNtypx: Plage  pour Difference de Type de Grains
C +
      data vdtysp/   0.5e0/
C +...     vdtysp: Poids de la Sphericite    dans le Calcul
C +
      data vdtyro/ 500.0e3/
C +...     vdtyro: Poids de la Masse Volumique
C +
      data vdtydi/  10.0e0/
C +...     vdtydi: Poids du Diametre             des Grains
C +
      data vdtyhi/ 100.0e0/
C +...     vdtyhi: Poids de l'Historique         des Grains
C +
C +
C +-- Types: dendritique et non dendritique
C +   -------------------------------------
C +
      if     (pgra11*pgra12.lt.-epsi) then
        SRFdtp_sno=SNtypx
C +
C +
C +-- Les deux Strates sont a Grains de Type     dendritique
C +   ------------------------------------------------------
C +
      elseif (pgra11       .lt.-epsi) then
        SRFdtp_sno=abs(pgra11-pgra12)
     .            +abs(pgra21-pgra22)*vdtysp
     .            +abs(pro1  -pro2)  *vdtyro
      else
C +
C +
C +-- Les deux Strates sont a Grains de Type non dendritique
C +   ------------------------------------------------------
C +
        SRFdtp_sno=abs(pgra11-pgra12)
     .            +abs(pgra21-pgra22)*vdtydi
     .            +abs(pro1-pro2)    *vdtyro
      endif
C +
        SRFdtp_sno=min(SNtypx,SRFdtp_sno+abs(khist1-khist2)*vdtyhi)
C +
      return
      end
