 
 
      subroutine GRDsig(zmin,aavu,bbvu,ccvu,vertic)
 
C +------------------------------------------------------------------------+
C | MAR GRID                                               15-02-2008  MAR |
C |   SubRoutine GRDsig is used to initialize the vertical grid            |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:   zmin           : Height above Surface / 1st Sigma Level (m) |
C |   ^^^^^    aavu,bbvu,ccvu : Vertical Discretization Parameters         |
C |            vertic         : Logical Variable caracteris.vertic.discris.|
C |                                                                        |
C |   DATA:    sigpar(10)     : Parish Model Vertical Discretisation       |
C |   ^^^^^                                                                |
C |                                                                        |
C |   OUTPUT (via common block)                                            |
C |   ^^^^^^   Variable  which is  initialized is:                         |
C |               sigma(mz): Independant Variable (Normalized Pressure)    |
C |                                                                        |
C |   ASSUMPTION: sigma is calculated from initial level height amsl       |
C |   ^^^^^^^^^^^                     assumig that T(msl) = SST            |
C |                                                dT/dz  = -0.0065 K/m    |
C |                                                p_s    = 100     hPa    |
C |                                                                        |
C | # OPTIONS: #SA  Regular      Vertical Discretisation                   |
C | # ^^^^^^^^ #PA  Parish Model Vertical Discretisation                   |
C | #          #ll  LMDZ   Model Vertical Discretisation (L. Li)           |
C | #          #HE  NORLAM       Vertical Discretisation (G. Heineman)     |
C | #          #L1  Alternate    Vertical Discretisation (when very fine)  |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MARSND.inc'
 
      include 'MAR_DY.inc'
 
      include 'MAR_TU.inc'
 
      include 'MAR_SL.inc'
 
      include 'MAR_IO.inc'
 
      include 'MAR_WK.inc'
 
      logical  vertic
 
 
C +--Local  Variables
C +  ================
 
      integer   lsf   ,lvg,kk    ,km  ,kn ,k1
 
      real      ga0   ,gaz,zmin  ,dzz ,rz ,rzb,zzo,sh_min
      real      ps_sig,vu ,aavu  ,bbvu,ccvu,ps_max,pstar0
c #HE real      sighei(29)
c #lm real      siglmd(11)
c #PA real      sigpar(10),sigp11,sigp12(11:12),sigp13(10:13)
 
 
C +--DATA
C +  ====
 
      data      ps_sig/100.0e0/
C +...data      ps_sig/101.3e0/
 
c #HE data      sighei
c #HE.       /0.10015,0.19077,0.27276,0.34695,0.41409,0.47483,0.52979,
c #HE.0.57952,0.62452,0.66524,0.70208,0.73542,0.76558,0.79288,0.81757,
c #HE.0.83992,0.86014,0.87844,0.89499,0.90997,0.92352,0.93579,0.94688,
c #HE.0.95692,0.96601,0.97423,0.98167,0.98840,0.99111/
C +***          sighei: DNMI model Vertical Discretisat.  (Heinemann 1996)
 
c #lm data      siglmd/0.014767,0.071835,0.150135,0.270661,0.410669,
c #lm.                 0.565832,0.708390,0.829996,0.913837,0.966484,
c #lm.                 0.990723/
C +***          siglmd: Vertical Discretisation of LMDZ   Model
C +                     (Laurent LI, personal communication,  5 dec. 2000)
 
c #PA data      sigpar/0.100,0.350,0.600,0.800,0.900,
c #PA.                 0.930,0.950,0.970,0.985,0.996/
C +***          sigpar: Vertical Discretisation of Parish Model
C +                     (Bromwich, Du and Parish 1994 MWR 122 No 7 p.1418)
 
c #PA data      sigp11                              /0.998/
c #PA data     (sigp12(k),k=11,12)                  /0.998,0.999/
c #PA data     (sigp13(k),k=10,13)            /0.990,0.996,0.998,0.999/
C +***          sigp1x: Vertical Discretisation of Parish Model (modified)
 
      data lsf / 1/
 
      data ga0 / 0.0065e0/
C +...     ga0 : Standard Atmospheric Lapse Rate
 
      lvg = 0
C +...lvg : set to 1 if |Vg(sounding)| .ne. 0 anywhere
 
 
C +--Entry Checking Point
C +  ====================
 
      if (IO_loc.ge.2) write(21,999)
 999  format(//,'   --- Initialisation / GRDsig ---')
 
 
C +--Temperature Vertical Profile
C +  ============================
 
      gaz = ga0
 
      if (IO_loc.ge.2) write(21,1)gaz,sst_SL,ps_sig,gravit,RDryAi
 1    format(/,'  dT/dz  =',f8.5,' K/m',
     .       /,'  SST    =',f8.2,' K',
     .       /,'  ps_sig =',f8.2,' kPa',
     .       /,'  gravit =',f8.2,' m/s2',
     .       /,'  RDryAi =',f8.2,' J/kg/K')
 
 
C +--Sigma Levels
C +  ============
 
C +- 1) Coarse Resolution of the Surface Layer
C +  -----------------------------------------
 
      IF (.not.vertic)                                            THEN
 
C +       aa = 0.5
C +       bb = 1.5
C +       cc =-1.0
C +...    Reference : E. Richard, these, 1991, p.29
 
          vu =       0.0
        DO k=1,mz
          vu = vu  + 1.0/dble(mzz)
          sigma(k) = aavu*vu + bbvu*vu*vu + ccvu*vu*vu*vu
 
C +- Vertical Discretisation of NORLAM
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #HE     sigma(k) = sighei(k)
        END DO
 
C +- Vertical Discretisation of LMDZ   Model
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #lm   DO k=1,11
c #lm        sigma(k)  = siglmd(k)
c #lm   END DO
 
C +- Vertical Discretisation of Parish Model
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #PA   DO k=1,10
c #PA        sigma(k)  = sigpar(k)
c #PA   END DO
 
c #PA       mmz  = mz
c #PA   if (mmz.gt.10) then
c #PA    if(mmz.eq.11)
c #PA.       sigma(11) = sigp11
c #PA    if(mmz.eq.12) then
c #PA       do k = 11,12
c #PA        sigma( k) = sigp12(k)
c #PA       end do
c #PA    end if
c #PA    if(mmz.eq.13) then
c #PA       do k = 10,13
c #PA        sigma( k) = sigp13(k)
c #PA       end do
c #PA    end if
c #PA   end if
 
        DO k=1,mz
          IF (abs(gaz).gt.1.d-5)                                  THEN
            zsigma(k) =-(   sst_SL /gaz) *    ((1.e0+(sigma(k)-1.e0)
     .                                        *(1.e2/ps_sig))
     .                                   **(RDryAi*gaz/gravit)-1.e0)
          ELSE
            if (IO_loc.ge.2.and.k.eq.1) write(21,116)
 116        format(/,'  t(z)   = CONSTANT')
            zsigma(k) =-(RDryAi*sst_SL/gravit)*log((unun+(sigma(k)-unun)
     .                                        *(1.d2/ps_sig)))
          END IF
        END DO
 
 
C +- 2) Fine   Resolution of the Surface Layer
C +  -----------------------------------------
 
      ELSE
 
        gaz    =max(gaz,epsi)
 
               km =          2
               km =      min(km,mz)
               kn =          1
c #L1          kn =          2
        zsigma( 1)=     zmin
        zsigma(km)=2.0 *zmin
 
        DO k=min(3,mz),mz
            rz     =zmin*aavu **(k-1)
            rzb    =ccvu*bbvu **(k-1)
          IF (TUkhmx.gt.0.0)                                      THEN
            zsigma(k)=rzb   *rz /(rz + rzb  )
          ELSE
            zsigma(k)=       rz
          END IF
 
            zsigma(k)= max(zsigma(k),zsigma(k-1)+zsigma(kn))
        END DO
 
            sh_min   =   8807.0
C +...      sh_min_0     Everest
 
        DO j=1,my
        DO i=1,mx
            sh_min   = min(sh_min,sh(i,j))
        END DO
        END DO
            ps_max   =     ps_sig*(1.0-gaz* sh_min           /sst_SL)
     .                                         **(gravit/(gaz*RDryAi))
            pstar0   =     ps_max-ptopDY
        DO k=1,mz
            kk=mz+1-k
            sigma(kk)=    (ps_sig/pstar0)
     .                          *((1.0-gaz*(sh_min+zsigma(k))/sst_SL)
     .                                         **(gravit/(gaz*RDryAi))
     .                           - 1.0                              )
     .                           + 1.0      + (ps_sig-ps_max)/pstar0
C +...      sigma(kk): the fine resolution  of the surface layer
C +                    is computed using a geometric progression
 
        END DO
      END IF
 
 
C +--Output
C +  ======
 
      do k=1,mz
        kk=  mzz-k
        WKxza(1,k) = zsigma(kk)
      end do
 
      do k=1,mz
        zsigma (k) = WKxza(1,k)
        WKxza(1,k) = 0.0
      end do
 
      IF (IO_loc.ge.2)                                            THEN
       write(21,130)( sigma(k),k=1,mz)
 130   format(/,'  Sigma    Levels :',/,(1x,15f8.4))
       write(21,131)(zsigma(k),k=1,mz)
 131   format(/,'  Altitude Levels :',/,(1x,15f8.1))
      END IF
 
      return
      end
