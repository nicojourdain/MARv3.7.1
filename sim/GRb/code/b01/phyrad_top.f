 
 
      subroutine PHYrad_top(Dis_ST)
 
C +------------------------------------------------------------------------+
C | MAR PHYSICS (INSOL)                                    15-11-2007  MAR |
C |   SubRoutine PHYrad_top computes                                       |
C |      Time Insolation at the Top of the Atmosphere                      |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   REFER.:   Ch.  Tricot, personal communication                        |
C |   ^^^^^^^   M.F. Loutre, personal communication and thesis (1993)      |
C |                                                                        |
C |   INPUT :   mmarGE, jdarGE: Month and Day of the Year                  |
C |   ^^^^^^^   jhurGE, minuGE, jsecGE: Hour, Minute, and Second           |
C |             GElat0, GElon0: Latitude, Longitude                        |
C |             GElatr(mx,my) : Latitude                         (radians) |
C |             GElonh(mx,my) : Longitude                          (hours) |
C |             itizGE(mx,my) : Time Zone                                  |
C |                                                                        |
C |   OUTPUT:   rsunGE        : Insolation normal to Atmosphere Top (W/m2) |
C |   ^^^^^^^   czenGE(mx,my) : Cosinus of the Zenithal Distance           |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
 
      include 'MARSND.inc'
 
      include 'MAR_WK.inc'
 
      include 'MAR_IO.inc'
 
      REAL     Dis_ST
 
 
C +--LOCAL VARIABLES
C +  ===============
 
      real     pirr  ,xl    ,so
      real     xllp  ,xee   ,xse
      real     xlam  ,dlamm ,anm   ,ranm  ,ranv  ,anv   ,tls
      real     Tyear ,step  ,rlam  ,sd    ,cd    ,deltar,delta
 
      real     ddt   ,arg   ,et    ,argc  ,ahc
      real     c1    ,c2    ,c3    ,s1    ,s2    ,s3
      real     timdl ,timh  ,ahor  ,ahorr ,RadLat,chor  ,zenitr
      integer  nj    ,lhc
 
      real     omesun
c #AZ real     slopx ,slopy ,omenor
c #AZ real     comes ,somes ,omeswr,anormr,omenwr
c #AZ integer  momask
 
c #MM real     dxdx  ,azim_0,azim_1,azimdd,azimut,r_azim
c #MM real     azimxx,azimxa,azimxs,azimyy,azimya,azimys
c #MM real     ddxx_2,ddyy_2,ddxy_2,ddzz  ,tmnt_2,cmnt
c #MM integer  k_azim,nax   ,nal   ,na    ,na2   ,ka
c #MM integer  i_azim,j_azim,j_azmn,j_azmx,i_azmn,i_azmx,nocoun
c #MM integer  knazim,ni1   ,ni2   ,nj1   ,nj2
c #MM integer  i1    ,i2    ,j1    ,j2    ,ni
 
      real     om    ,ecc   ,perh  ,xob
 
 
C +--DATA
C +  ====
 
      data     om /0.0172142d0/
 
 
C +--Present Day Insolation
C +  ----------------------
 
      data ecc  /  0.01673/      ! Eccentricity
      data perh /102.4    /      ! Longitude of the Perihelion (degrees)
      data xob  / 23.445  /      ! Obliquity                   (degrees)
 
 
C +--6 kBP       Insolation
C +  ----------------------
 
c #6k data ecc  /  0.018682/     ! Eccentricity
c #6k data perh /  0.87    /     ! Longitude of the Perihelion (degrees)
c #6k data xob  /  24.105  /     ! Obliquity                   (degrees)
 
 
C +--10 kBP      Insolation
C +  ----------------------
 
c #10 data ecc  /  0.019419/     ! Eccentricity
c #10 data perh /294.81    /     ! Longitude of the Perihelion (degrees)
c #10 data xob  / 24.226   /     ! Obliquity                   (degrees)
 
 
C +--Insolation at the Top of the Atmosphere (TIME       PARAMETERS)
C +  ===============================================================
 
C +--Solar declination : delta
C +  -------------------------
 
      nj    = jdarGE+njyrGE(mmarGE)
      Tyear = 365.25d0
      step  = 360.0d0/Tyear
 
      pirr  = degrad /  3600.0
      xl    = perh   +  180.0
      so    = sin(xob*degrad)
C +...so    : sinus of obliquity
 
      xllp  =xl*degrad
      xee   =ecc*ecc
      xse   =sqrt(1.0d0-xee)
      xlam  =(ecc/2.0+ecc*xee/8.0d0)*(1.0+xse)*sin(xllp)-xee/4.0*
     .       (0.5+xse)*sin(2.0*xllp)+ecc*xee/8.0*(1.0/3.0+xse)*
     .        sin(3.0*xllp)
      xlam  =2.0d0*xlam/degrad
      dlamm =xlam+(nj-80)*step
C +...xlam  : true long. sun for mean long. = 0
C +...dlamm : mean long. sun for ma-ja
      anm   =dlamm-xl
      ranm  =anm*degrad
      xee   =xee*ecc
      ranv  =ranm+(2.0*ecc-xee/4.0)*sin(ranm)+5.0/4.0*ecc*ecc*
     .       sin(2.0*ranm)+13.0/12.0*xee*sin(3.0*ranm)
      anv   =ranv/degrad
      tls   =anv+xl
      rlam  =tls*degrad
C +...tls   : longitude vraie   (degrees)
C +...rlam  : longitude vraie   (radian)
C +...anv   : anomalie vraie    (degrees)
C +...ranv  : anomalie vraie    (radian)
 
      sd    = so * sin(rlam)
      cd    = sqrt(1.0d0-sd*sd)
C +...sd and cd: cosinus and sinus of solar declination angle (delta)
C +...sinus delta = sin (obl)*sin(lambda) with lambda = real longitude
C +...(Phd. thesis of Marie-France Loutre, ASTR-UCL, Belgium, 1993)
 
      deltar= atan(sd/cd)
      delta = deltar/degrad
C +...delta: Solar Declination (degrees, angle sun at equator)
 
 
C +--Eccentricity Effect
C +  -------------------
 
      Dis_ST  =(1.0-ecc*ecc)/(1.0+ecc*cos(ranv))
      ddt     = 1.0 /  Dis_ST
C +...ddt  :    1   /  normalized earth's sun distance
 
 
C +--Insolation normal to the atmosphere (W/m2)
C +  ------------------------------------------
 
cXF
      rsunGE  = ddt *ddt *1360.8d0
 
 
C +--Time Equation (Should maybe be modified in case other than present
C +  -------------  conditions are used, minor impact)
 
      arg = om*nj
      c1  = cos(arg)
      c2  = cos(2.d0*arg)
      c3  = cos(3.d0*arg)
      s1  = sin(arg)
      s2  = sin(2.d0*arg)
      s3  = sin(3.d0*arg)
 
      et=0.0072d0*c1 -0.0528d0*c2 -0.0012d0*c3
     .  -0.1229d0*s1 -0.1565d0*s2 -0.0041d0*s3
C +...et (hour)
C +     = difference between true solar and mean solar hour angles.
C +      (connected to the earth orbital rotation speed)
 
 
C +--Insolation at the Top of the Troposphere (Auxiliary Variables)
C +  ==============================================================
 
 
C +--Day Length, Time Sunrise and Sunset at Sounding Grid Point (iSND,jSND)
C +  ----------------------------------------------------------------------
 
      i = iSND
      j = jSND
 
         argc   = -tan(GElatr(i,j))*tan(deltar)
       if (abs(argc).gt. 1.d0) then
         ahc    =  0.d0
        if    (argc .gt. 1.d0) then
         lhc    = -1
         timdl  = 00.d0
C +...   Polar  Night
        else
         lhc    =  1
         timdl  = 24.d0
C +...   Midnight Sun
        end if
         tlsrGE = 00.d0
         tlssGE = 00.d0
       else
         ahc    =  acos(argc)
         lhc    =  0
 
        if(ahc.lt.0.d0)    ahc = -ahc
         ahc    =          ahc / hourad
         timdl  =          ahc * 2.d0
         tlsrGE = 12.d0  - ahc + itizGE(i,j) - et - GElonh(i,j)
         tlssGE = tlsrGE + timdl
       end if
 
         tl__GE = jhurGE + minuGE / 60.d0         - itizGE(i,j)
 
 
C +--Time Angle
C +  ----------
 
      DO j=1,my
      DO i=1,mx
         timh  = jhurGE + minuGE      / 60.d0
         ahor  = timh   + GElonh(i,j) - 12.d0 - et
C +...   ahor  : time angle (hours)
 
         ahorr = ahor   * hourad
C +...   ahorr : time angle (radians)
 
         chor  = cos(ahorr)
 
 
C +--Solar Zenithal Distance zenitr (radians) and
C +  Insolation (W/m2) at the Atmosphere Top  ===
C +  =======================================
 
         czenGE(i,j) =      slatGE(i,j) *sd
     .                    + clatGE(i,j) *cd *chor
         czenGE(i,j) =  max(czenGE(i,j),zero)
 
         cverGE(i,j) =      czenGE(i,j)
 
 
C +--Slope Impact
C +  ------------
 
c #AZ    zenitr      = acos(czenGE(i,j))
c #AZ    WKxy3 (i,j) =  sin(zenitr)
c #AZ    WKxy4 (i,j) =  sin(ahorr)
      END DO
      END DO
 
C +--Slope Azimuth
C +  ~~~~~~~~~~~~~
c #AZ IF (iterun.le.1)                                            THEN
c #AZ   DO j=1,my
c #AZ   DO i=1,mx
c #AZ     slopx        =(sh(ip1(i),j)-sh(im1(i),j))*dxinv2
c #AZ     slopy        =(sh(i,jp1(j))-sh(i,jm1(j)))*dyinv2
c #AZ     slopGE(i,j)  =sqrt(slopx*slopx+slopy*slopy)
c #AZ     slopGE(i,j)  =cos(atan(slopGE(i,j)))
C +...    slopGE ...... Cosine of Fall Line Angle
 
c #AZ     IF (abs(slopx).gt.zero)                                 THEN
c #AZ           omenor = atan(slopy/slopx)
c #AZ       IF   (slopx.lt.zero)
c #AZ.          omenor =  omenor + pi
 
c #AZ       IF (omenor.gt.         pi)
c #AZ.          omenor =  -2.0d0 * pi + omenor
c #AZ       IF (omenor.lt.        -pi)
c #AZ.          omenor =   2.0d0 * pi + omenor
 
c #AZ     ELSE
c #AZ       IF   (slopy.gt.zero) then
c #AZ           omenor =   0.5d0 * pi
c #AZ       ELSE
c #AZ           omenor =   1.5d0 * pi
c #AZ       END IF
c #AZ     END IF
C +...          omenor      :      Fall Line Azimuth   (Upslope Direction)
C +
c #AZ           omenGE(i,j) =      omenor - pi
C +...          omenGE(i,j) :      Fall Line Azimuth (Downslope Direction)
C +                                               (in MAR Reference Frame)
C +                                            (positive counterclockwise)
c #AZ   END DO
c #AZ   END DO
 
C +--Mountains Mask
C +  ~~~~~~~~~~~~~~
c #AZ       momask =   1
c #MM   IF (momask.ne. 1)
c #MM.stop'++++++++ Preprocessing Error: #AZ not removed ++++++++++++++'
C +
c #MM         dxdx   =  dx    * dx
C +
c #MM         daziGE =  2.0d0 * pi /   n_azim
c #MM   DO k_azim   = 1,n_azim
c #MM         azim_0 =  (k_azim-1) *   daziGE
c #MM         azim_1 =   k_azim    *   daziGE
 
c #MM     DO j=1,mmy
c #MM     DO i=1,mmx
c #MM         cmntGE(i,j,k_azim)   = 0.0d00
c #MM     END DO
c #MM     END DO
 
c #MM     IF (abs(cos(azim_0)).gt.abs(sin(azim_0)))               THEN
c #MM           nax    = mx1 / 2
c #MM     ELSE
c #MM           nax    = my1 / 2
c #MM     END IF
c #MM           nal    =          30
c #MM           nax    = min(nax,nal)
 
c #MM     DO  na=1,nax
 
c #MM           na2    = na  / 2
c #MM           na2    = max(na2,1)
 
c #MM           azimdd =                 daziGE /  na
 
c #MM       DO j=1,mmy
c #MM       DO i=1,mmx
c #MM           WKxy1(i,j) = 0.0d00
c #MM           WKxy2(i,j) = 0.0d00
c #MM       END DO
c #MM       END DO
 
c #MM       DO ka=1,na
c #MM           azimut = azim_0 + azimdd * (ka-0.5d0)
 
c #MM           azimxx =(na+demi)*cos(azimut)
c #MM           i_azim =              azimxx
c #MM           azimxa =          abs(azimxx)
c #MM           azimxs =    sign(unun,azimxx)
 
c #MM           azimyy =(na+demi)*sin(azimut)
c #MM           j_azim =              azimyy
c #MM           azimya =          abs(azimyy)
c #MM           azimys =    sign(unun,azimyy )
 
c #MM         IF   (i_azim.eq.0.and.j_azim.eq.0)                  THEN
c #MM           IF (azimxa.gt.azimya)                             THEN
c #MM               i_azim =  azimxs
c #MM           ELSE
c #MM               j_azim =  azimys
c #MM           END IF
c #MM         END IF
 
c #MM         DO j=2,my1
c #MM               j_azmn =  1-j
c #MM               j_azmx = my-j
 
c #MM         DO i=2,mx1
c #MM               i_azmn =  1-i
c #MM               i_azmx = mx-i
 
c #MM                                                     nocoun = 0
c #MM           IF (j_azim.gt.j_azmx.or.j_azim.lt.j_azmn) nocoun = 1
c #MM           IF (i_azim.gt.i_azmx.or.i_azim.lt.i_azmn) nocoun = 1
 
c #MM           IF (nocoun.eq.1)                             GO TO 150
 
c #MM               ddxx_2 =     i_azim*i_azim   *dxdx
c #MM               ddyy_2 =     j_azim*j_azim   *dxdx
c #MM               ddxy_2 =     ddxx_2+ddyy_2
 
c #MM               ddzz  = sh(i+i_azim,j+j_azim)-sh(i,j)
c #MM.                    - sqrt(earthr *earthr + ddxy_2) + earthr
C +...              Correction for Earth Curvature
 
c #MM               ddzz  =   max(ddzz,zero)
 
c #MM               tmnt_2 = ddzz * ddzz / ddxy_2
c #MM               cmnt   = sqrt(tmnt_2 /(unun  +tmnt_2))
 
c #MM               WKxy1(i,j)           =     WKxy1(i,j) + cmnt
c #MM               WKxy2(i,j)           =     WKxy2(i,j) + unun
 
 150            CONTINUE
 
c #MM         END DO
c #MM         END DO
 
c #MM       END DO
 
c #MM       DO j=2,my1
c #MM       DO i=2,mx1
c #MM         IF (WKxy2( i,j).gt.0.d0)                            THEN
c #MM             WKxy1( i,j)       =     WKxy1(i,j)/ WKxy2(i,j)
c #MM             cmntGE(i,j,k_azim)= max(WKxy1(i,j),cmntGE(i,j,k_azim))
c #MM         END IF
c #MM       END DO
c #MM       END DO
 
c #MM     END DO
 
c #MM   END DO
 
c #AZ END IF
 
C +--Sun   Azimuth
C +  ~~~~~~~~~~~~~
c #AZ DO j=1,my
c #AZ DO i=1,mx
 
c #AZ     WKxy3 (i,j) = max(epsi       ,WKxy3 (i,j))
 
c #AZ     comes       =(sd-slatGE(i,j) *czenGE(i,j))
c #AZ.                /(   clatGE(i,j) *WKxy3 (i,j))
C +...    comes: Cosine of Sun Azimuth
 
c #AZ     somes       =(cd*WKxy4(i,j)) /WKxy3 (i,j)
C +...    somes:   Sine of Sun Azimuth
 
c #AZ   IF (abs(comes).gt.zero)                                   THEN
c #AZ           omesun = atan(somes/comes)
c #AZ     IF   (comes.lt.zero)
c #AZ.          omesun =  omesun + pi
 
c #AZ     IF   (omesun.gt.         pi)
c #AZ.          omesun =  -2.0d0 * pi + omesun
c #AZ     IF   (omesun.lt.        -pi)
c #AZ.          omesun =   2.0d0 * pi + omesun
 
c #AZ   ELSE
c #AZ     IF   (somes.gt.zero)                                    THEN
c #AZ           omesun =   0.5d0 * pi
c #AZ     ELSE
c #AZ           omesun =   1.5d0 * pi
c #AZ     END IF
c #AZ   END IF
 
c #AZ   IF (i.eq.iSND.and.j.eq.jSND) omeswr = omesun / degrad
C +
c #AZ           omesun =  -2.0d0 * pi + omesun + GEddxx * degrad
C +...          omesun :  Sun Azimuth              (in MAR Reference Frame)
C +                                             (positive counterclockwise)
 
C +--Minimum Zenithal Distance
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~
c #AZ   czmnGE(i,j) = 0.0d00
c #MM                            r_azim = omesun  / daziGE
c #MM                            k_azim = r_azim
c #MM   IF (k_azim.le.     0)                                     THEN
c #MM                            r_azim = r_azim  + n_azim
c #MM                            k_azim = k_azim  + n_azim
c #MM   END IF
c #MM                            knazim = k_azim  + 1
c #MM   IF (knazim.gt.n_azim)    knazim = knazim  - n_azim
 
c #MM   czmnGE(i,j) = cmntGE(i,j,k_azim)+(r_azim  - k_azim)
c #MM.              *(cmntGE(i,j,knazim)-cmntGE(i,j,k_azim))
 
C +--Cosine of Solar Normal Angle
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #AZ   cverGE(i,j) = slopGE(i,j) * czenGE(i,j)
c #AZ.              +  WKxy3(i,j) * slopGE(i,j)*sqrt(unun  -slopGE(i,j))
c #AZ.                            *              cos(omesun-omenGE(i,j))
c #AZ   cverGE(i,j) =                            max(zero  ,cverGE(i,j))
c #AZ IF            (czenGE(i,j).le.czmnGE(i,j))
c #AZ.  cverGE(i,j) = 0.0d00
c #AZ END DO
c #AZ END DO
 
 
C +--Output
C +  ======
 
c #MM if (    iterun.eq.0                                         ) then
 
c #MM         ni1=imez/20+1
c #MM         ni2=imez/20+1
c #MM         nj1=jmez/20+1
c #MM         nj2=jmez/20+1
 
c #MM  do nj= nj2,nj1,-1
c #MM    j1=(nj-1)*20+1
c #MM    j2= nj   *20
c #MM  do ni= ni1,ni2
c #MM    i1=(ni-1)*20+1
c #MM    i2= ni   *20
 
c #MM  write(4,60)   (             i          ,i=i1,i2),
c #MM.            (j,(1.d-3*sh    (i,j       ),i=i1,i2),j=j2,j1,-1)
 60    format(///,'TOPOGRAPHY',
     .          /,'==========',      /,4x,20i4,/,(i4,20f4.1))
 
c #MM  do k_azim=1,n_azim
c #MM              azimut =         (k_azim-0.5d0) * daziGE / degrad
c #MM  write(4,61) azimut,
c #MM.               (             i          ,i=i1,i2),
c #MM.            (j,(      cmntGE(i,j,k_azim),i=i1,i2),j=j2,j1,-1)
 61    format(///,'AZIMUTH   ',f6.1,
     .          /,'================',/,4x,20i4,/,(i4,20f4.2))
 
c #MM  end do
 
c #MM  end do
c #MM  end do
 
c #MM end if
 
      if ((  jmmMAR.eq.0.and.    jssMAR   .eq.0         .and.
     .     ((IO_loc.ge.2.and.    jhurGE   .eq.0) .or.
     .      (IO_loc.ge.2.and.mod(jhurGE,3).eq.0) .or.
     .      (IO_loc.ge.3)                            )       ).or.
     .       IO_loc.ge.7                                          ) then
 
         ahor  = timh   + GElonh(iSND,jSND)  - 12.d0 - et
         zenitr=     acos(czenGE(iSND,jSND)) / degrad
c #AZ    anormr=     acos(cverGE(iSND,jSND)) / degrad
c #AZ    omenwr= GEddxx - omenGE(iSND,jSND)  / degrad
c #AZ    if (omenwr.lt.  0.) omenwr = omenwr + 360.d0
c #AZ    if (omenwr.gt.360.) omenwr = omenwr - 360.d0
c #AZ                        omeswr = 360.d0 - omeswr
c #AZ    if (omeswr.lt.  0.) omeswr = omeswr + 360.d0
c #AZ    if (omeswr.gt.360.) omeswr = omeswr - 360.d0
 
        write(4,1) GElat0,GElon0,jdarGE,mmarGE,jhurGE,minuGE,jsecGE
 1      format(/,' lat.=',f6.1,3x,'long.=',f7.1,4x,'date :',i3,'-',i2,
     .           ' / ',i2,' h.UT',i3,' min.',i3,' sec.')
        write(4,2) iSND,jSND,GElatr(iSND,jSND)/degrad,GElonh(iSND,jSND)
 2      format(' Sounding at (',i3,i3,') / (',f6.2,'dg,',f6.2,'ho)')
        write(4,3) rsunGE*cverGE(iSND,jSND),ahor,zenitr
c #AZ.            ,omeswr,omenwr           ,     anormr
     .            ,delta
 3      format(' Insolation [W/m2]  = ',f7.2,'   Hor.Angle = ',f7.2,
     .         '   Zenith.Angle = ',f7.2
c #AZ.      ,/,'                      ', 7x ,'   Sol.Azim. = ',f7.2
c #AZ.      ,/,'                      ', 7x ,'   Nrm.Azim. = ',f7.2
c #AZ.        ,'   Normal Angle = ',f7.2
     .      ,/,' Solar Declination  = ',f7.2)
 
       if (lhc.eq.-1)
     .  write(4,4) tlsrGE,timdl,tlssGE
 4      format(' Sun Rise Time [h]  = ',f7.2,'   Day Leng. = ',f7.2,
     .         '   Sun Set Time = ',f7.2,'  -- POLAR  NIGHT --')
       if (lhc.eq. 0)
     .  write(4,5) tlsrGE,timdl,tlssGE
 5      format(' Sun Rise Time [h]  = ',f7.2,'   Day Leng. = ',f7.2,
     .         '   Sun Set Time = ',f7.2,'  -- SOLAR  TIME  --')
       if (lhc.eq. 1)
     .  write(4,6) tlsrGE,timdl,tlssGE
 6      format(' Sun Rise Time [h]  = ',f7.2,'   Day Leng. = ',f7.2,
     .         '   Sun Set Time = ',f7.2,'  -- MIDNIGHT SUN --')
      end if
 
 
C +--Work Arrays Reset
C +  =================
 
c #AZ DO j=1,my
c #AZ DO i=1,mx
c #MM   WKxy1(i,j) = 0.d0
c #MM   WKxy2(i,j) = 0.d0
c #AZ   WKxy3(i,j) = 0.d0
c #AZ   WKxy4(i,j) = 0.d0
c #AZ END DO
c #AZ END DO
 
      return
      end
