 
 
      block data PHYdat
 
C +------------------------------------------------------------------------+
C | MAR PHYSIC                                             28-11-2006  MAR |
C |   Block Data PHYdat is used to define physical constants used in   MAR |
C +------------------------------------------------------------------------+
C |                                                                        |
C | # REMARK: "c  #  " means that data are redefined in SubRoutine PHYmar  |
C |   ^^^^^^                                                               |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MAR_GE.inc'
      include 'MARSND.inc'
c #AO include 'MAR_AO.inc'
 
 
C +--Numeric   Constants
C +  ===================
 
      data  izr /   0             /
      data  iun /   1             /
      data zero /   0.0           /
      data demi /   0.5           /
      data unun /   1.0           /
      data epsi /   0.000001000000/
      data eps9 /   0.000000001000/
      data eps12/   0.000000000001/
      data thous/1000.0           /
 
 
C +--Earth     Constants
C +  ===================
 
      data earthr/6371.229e+3/
C +...     earthr: Earth Radius
 
      data earthv/ 729.217e-7/
C +...     earthv: Earth Angular Velocity
 
      data gravit/   9.81e0/
C +...     gravit: Gravity Acceleration
 
 
C +--Dynamical Constants
C +  ===================
 
      data akmol /   1.35e-5/
C +...     akmol : Air Viscosity
 
      data vonkar/   0.40e0/
C +...     vonkar: von Karman constant
 
      data A_Turb /5.8/
C +...     A_Turb: Stability Coefficient in the SBL Universal Stable Function
C +                for Momt (Cheng et al., 2005, 2004JD004923, p.3, eq.9: 5.8)
 
      data AhTurb /5.4/
C +...     AhTurb: Stability Coefficient in the SBL Universal Stable Function
C +                for Heat (Cheng et al., 2005, 2004JD004923, p.3, eq.9: 5.4)
 
      data AsTurb /4.0/
C +...     AsTurb: Stability Coefficient in the SBL Universal Stable Function
C +                for BLOW*(Bintanja,     2000, BLM 95,       p.390      4.0)
 
      data r_Turb /3.0/
C +...     r_Turb: Turbulent Diffusivities Ratio K*/Km
C +                         (Bintanja,     2000, BLM 95, p. 384           3.0)
 
 
C +--Thermodynamical Constants (Atmosphere)
C +  ======================================
 
      data Ra    / 287.05967 /
      data RDryAi/ 287.05967 /
C +...     RDryAi: Perfect Gas Law  Constant / dry Air     (J/kg/K)
 
      data cp    /1004.708845/
C +...     cp    : Air Specific Heat                       (J/kg/K)
 
c #   data cap   /   0.28586e0/
C +...     cap   = R / Cp
 
      data pcap  /   3.73010e0/
C +...     pcap  = 100 ** (R / Cp)
 
      data RVapor/ 461.e0/
C +...     RVapor: Perfect Gas Law  Constant / Water Vapor (J/kg/K)
 
      data epsq  /   3.00e-6/
C +...     epsq  : Minimum Water Vapor Content
 
      data Lv_H2O/   2.5008e+6/
C +...     Lv_H2O: Latent Heat of Vaporisation for Water   (J/kg)
 
      data Ls_H2O/   2.8345e+6/
C +...     Ls_H2O: Latent Heat of Sublimation  for Ice     (J/kg)
 
      data stefan/   5.67e-8/
C +...     stefan: Stefan-Bolstzman Constant (W/m2/K4)
 
      data qv_MIN/   3.00e-6/
C +...     qv_MIN: Specific Humididy Minimum (Christian Tricot, pers.com.)
 
C +--cpl : constants usefull for coupled configuration (see MAR_AO.inc)
C +  ==================================================================
 
C +...
c #AO data epsAO/     0.97/
C +...     epsAO: emissivity (same in NEMO)
 
c #AO data cpv/    1846.1/
C +...     cpv: water vapor specific heat (J/kg/K)
 
c #AO data cpvir/     0.83746/
C +...     cpvir: [cpv/cp - 1] (usefull value for qsat) (-)
 
c #AO data R_Rv/      0.622/
C +...     R_Rv: [gas cste dry air=287.0] / [gas cste moist air=461.5]
 
 
C +--Temperature Increment as a Function of Phase Change Increment
C +  =============================================================
 
c #   data   r_LvCp                          / 2490.04 /
C +...      [Lv=2500000J/kg]/[Cp=1004J/kg/K] = 2490.04
 
c #   data   r_LcCp                          /  332.27 /
C +...      [Lc= 333600J/kg]/[Cp=1004J/kg/K] =  332.27
 
c #   data   r_LsCp                          / 2822.31 /
C +...      [Ls=2833600J/kg]/[Cp=1004J/kg/K] = 2822.31
 
 
C +--Thermodynamical Constants (Sea)
C +  ===============================
C +
      data tfrwat/ 271.20e0/
C +...     tfrwat: Sea-Water freezing Temperature  (K)
C +                (definition: cfr. data in .main.)
 
      data siclf / 302.00e6/
C +...     siclf : Sea-Ice Heat of Fusion       (J/m3)
 
      data cdsice/   2.04e0/
C +...     cdsice: Conductivity of  Sea Ice    (W/m/K)
 
      data hic0  /   1.00e0/
C +...     hic0  : Sea-Ice Initial Thickness       (m)
C +                (Ross Sea Typical Value)
 
      data ro_Wat/1000.00e0/
C +...     ro_Wat: Density of Water            (kg/m3)
 
      data C__Wat/4186.00e0/
C +...     C__Wat: Heat Capacity of Water     (J/kg/K)
 
      data fracoh/   0.75e0/
C +...     fracoh=   1.00 - 0.25
C +***Hibler (1984):   25% of cooling => Oceanic Heat Flux (ANTARCTIC Ocean)
 
 
C +--Thermodynamical Constants (Ice)
C +  ===============================
 
      data ro_Ice /   920.e0/
C +...     ro_Ice: Density of Pure Ice          (kg/m3)
 
      data cdice  /   2.10e0/
C +...     cdice:  Conductivity of pure Ice     (W/m/K)
 
 
C +--Thermodynamical Constants (Snow)
C +  ================================
 
      data  csnow/2105.00e+0/
C +...      csnow:  Heat Capacity of Snow                        [J/kg/K]
C +***             (see Loth et al. 1993, JGR 98 D6, 2.2.2 2e para p.10453)
 
      data TfSnow/ 273.15e+0/
C +...     TfSnow: Snow      melting  Temperature  (K)
 
      data r0sno /   3.00e+1/
C +...     r0sno : Falling Snow minimal Density(kg/m3)
C +                (CEN/CROCUS Value)
 
!     data blsno /   3.30e+2/
C +...     blsno : Blown   Snow         Density(kg/m3) / until  2-Dec-2009
      data blsno /   2.50e+2/
C +...     blsno : Blown   Snow         Density(kg/m3) / calibration
 
      data Lf_H2O/   3.337e+5/
C +...     Lf_H2O: Latent  Heat of Fusion/ Snow (J/kg)
 
 
C +--Standard Atmosphere (Winter / Middle Latitudes)
C +  ===============================================
 
      data iSND / 1 /
      data jSND / 1 /
 
      data zSND / -143.,    0., 1001., 1993., 2992., 3993., 4994.,
     .            5983., 6978., 7988., 8984., 9970.,10968.,11975.,
     .           12966.,13949.,14945.,15932.,16950.,17900.,18914.,
     .           19884.,20894.,21933.,22985.,23799.,24990.,29928.,
     .           35068.,38589.,46673.,49408.,49583.,49761.,49948.,
     .           50132.,50324.,50521.,50723.,50930.,51143.       ,
     .            -143.,    0., 1001., 1993., 2992., 3993., 4994.,
     .            5983., 6978., 7988., 8984., 9970.,10968.,11975.,
     .           12966.,13949.,14945.,15932.,16950.,17900.,18914.,
     .           19884.,20894.,21933.,22985.,23799.,24990.,29928.,
     .           35068.,38589.,46673.,49408.,49583.,49761.,49948.,
     .           50132.,50324.,50521.,50723.,50930.,51143.       /
 
      data tSND / 277.0, 272.2, 268.7, 265.2, 261.7, 255.7, 249.7,
     .            243.7, 237.7, 231.7, 225.7, 219.7, 219.2, 218.7,
     .            218.2, 217.7, 217.2, 216.7, 216.2, 215.7, 215.2,
     .            215.2, 215.2, 215.2, 215.2, 215.2, 215.2, 217.4,
     .            227.8, 243.2, 258.5, 265.7, 265.7, 265.7, 265.7,
     .            265.7, 265.7, 265.7, 265.7, 265.7, 265.7       ,
     .            277.0, 272.2, 268.7, 265.2, 261.7, 255.7, 249.7,
     .            243.7, 237.7, 231.7, 225.7, 219.7, 219.2, 218.7,
     .            218.2, 217.7, 217.2, 216.7, 216.2, 215.7, 215.2,
     .            215.2, 215.2, 215.2, 215.2, 215.2, 215.2, 217.4,
     .            227.8, 243.2, 258.5, 265.7, 265.7, 265.7, 265.7,
     .            265.7, 265.7, 265.7, 265.7, 265.7, 265.7       /
 
      data qSND /.27e-2,.27e-2,.21e-2,.17e-2,.13e-2,.80e-3,.51e-3,
     .           .32e-3,.14e-3,.67e-4,.35e-4,.18e-4,.20e-4,.20e-4,
     .           .70e-5,.45e-5,.40e-5,.39e-5,.40e-5,.42e-5,.48e-5,
     .           .51e-5,.68e-5,.81e-5,.10e-4,.13e-4,.17e-4,.20e-4,
     .           .14e-4,.10e-4,.14e-4,.69e-5,.70e-5,.72e-5,.74e-5,
     .           .75e-5,.77e-5,.79e-5,.81e-5,.83e-5,.86e-5       ,
     .           .27e-2,.27e-2,.21e-2,.17e-2,.13e-2,.80e-3,.51e-3,
     .           .32e-3,.14e-3,.67e-4,.35e-4,.18e-4,.20e-4,.20e-4,
     .           .70e-5,.45e-5,.40e-5,.39e-5,.40e-5,.42e-5,.48e-5,
     .           .51e-5,.68e-5,.81e-5,.10e-4,.13e-4,.17e-4,.20e-4,
     .           .14e-4,.10e-4,.14e-4,.69e-5,.70e-5,.72e-5,.74e-5,
     .           .75e-5,.77e-5,.79e-5,.81e-5,.83e-5,.86e-5       /
 
      data pSND / 1036., 1018.,  897.,  790.,  694.,  608.,  531.,
     .             463.,  402.,  347.,  299.,  257.,  220.,  188.,
     .             161.,  138.,  118.,  101.,   86.,   74.,   63.,
     .              54.,   46.,   39.,   33.,   29.,   24.,   11.,
     .               5.,    3.,    1.,   0.7, 0.685, 0.669, 0.654,
     .            0.637, 0.622, 0.606, 0.591, 0.576, 0.560       ,
     .            1036., 1018.,  897.,  790.,  694.,  608.,  531.,
     .             463.,  402.,  347.,  299.,  257.,  220.,  188.,
     .             161.,  138.,  118.,  101.,   86.,   74.,   63.,
     .              54.,   46.,   39.,   33.,   29.,   24.,   11.,
     .               5.,    3.,    1.,   0.7, 0.685, 0.669, 0.654,
     .            0.637, 0.622, 0.606, 0.591, 0.576, 0.560       /
 
 
C +--Time Constants
C +  ==============
 
      data  njmoGE
     .     /0,31,28,31,30, 31, 30, 31, 31, 30, 31, 30, 31/
      data  njmbGE
     .     /0, 0, 1, 0, 0,  0,  0,  0,  0,  0,  0,  0,  0/
      data  njyrGE
     .     /0, 0,31,59,90,120,151,181,212,243,273,304,334/
      data  njybGE
     .     /0, 0, 0, 1, 1,  1,  1,  1,  1,  1,  1,  1,  1/
C +...      njmoGE: Nb of Days in each Month of the Year
C +         njmbGE: Leap  Year Correction    to njmoGE
C +         njyrGE: Nb of Days since   Begin of the Year,
C +                            before  Current Month
C +         njybGE: Leap  Year Correction    to njyrGE
 
      data  labmGE
     .     /'---','Jan','Feb','Mar','Apr','May','Jun',
     .            'Jul','Aug','Sep','Oct','Nov','Dec'/
 
 
C +--Characters
C +  ==========
 
      data labnum/'0','1','2','3','4','5','6','7','8','9'/
 
      end
