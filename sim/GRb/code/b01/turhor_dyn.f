 
 
      subroutine TURhor_dyn(dtHDif)
 
C +------------------------------------------------------------------------+
C | MAR TURBULENCE HORIZONTAL                          Sat 05-12-2009  MAR |
C |   SubRoutine TURhor_dyn computes Horizontal Diffusion                  |
C |                              and Correction Terms                      |
C |                         using an Explicit Scheme                       |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C | INPUT: dt_Loc: Time Step between two CaLLs of Horiz. Diffusion Routine |
C | ^^^^^  dtHDif: Time Step between two run   of Horiz. Diffusion Scheme  |
C |               (dt_Loc = n X dtHDif, n = 1,2,...)                       |
C |        micphy: Cloud Microphysical Scheme Switch                       |
C |                                                                        |
C | INPUT (via common block)                                               |
C | ^^^^^  TUkhx(mx,my,mz): Horizontal Diffusion Coefficient (x-Direction) |
C |        TUkhy(mx,my,mz): Horizontal Diffusion Coefficient (y-Direction) |
C |                                                                        |
C | INPUT / OUTPUT                                                         |
C | ^^^^^^^^^^^^^^                                                         |
C |      The Horizontal Diffusion and Correction Terms are included for :  |
C |       1) The Horizontal     x-Wind Component uairDY(mx,my,mz)    [m/s] |
C |       2) The Horizontal     y-Wind Component vairDY(mx,my,mz)    [m/s] |
C |                                                                        |
C |  #NH  3) The Vertical       z-Wind Component wairNH(mx,my,mz)    [m/s] |
C |                                                                        |
C |       4) The Potential      Temperature      pktaDY(mx,my,mzz)         |
C |       5) The Air Specific   Humidity           qvDY(mx,my,mz)  [kg/kg] |
C |                                                                        |
C |  #HY  6) The Ice Crystals   Concentration      qiHY(mx,my,mz)  [kg/kg] |
C |       7) The Ice Crystals   Number           ccniHY(mx,my,mz)  [Nb/m3] |
C |       8) The Cloud Droplets Concentration      qwHY(mx,my,mz)  [kg/kg] |
C |       9) The Snow Flakes    Concentration      qsHY(mx,my,mz)  [kg/kg] |
C |      10) The Rain Drops     Concentration      qrHY(mx,my,mz)  [kg/kg] |
C |                                                                        |
C |  #TC 11) The Tracer         Concentration      qxTC(mx,my,mz,ntrac)    |
C |                                                                        |
C |  REMARK:                                                               |
C |  ^^^^^^^                                                               |
C |  !. `Standard' Horizontal Diffusion is performed on Sigma Surfaces     |
C |  !.  Smagorinski Relation (see Tag et al. 1979, JAM 18, 1429--1441)    |
C |  !.  CAUTION: Horizontal Diffusion is switched on with turhor = .true. |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_DY.inc'
c #NH include 'MAR_NH.inc'
 
c #Di include 'MAR_DI.inc'
 
      include 'MAR_TU.inc'
c _PE include 'MARpen.inc'
c #PE include 'MARpen.inc'
      include 'MAR_FI.inc'
 
      include 'MAR_HY.inc'
c #TC include 'MAR_TC.inc'
 
      include 'MAR_WK.inc'
 
      real     dtHDif
 
 
C +--Local  Variables
C +  ================
 
      integer  nntrac,ntDifH,iter  ,n_kq  ,ivar ,itrac
      real     akhsta,akhloc,cflakh,dx2inv,gdx2
      real      facxx, facxy, facyy, alph2,beta2,akhm2,alph22,beta22
 
c #DC real     rhox  (mx,my,mz)
c _PE real     rikx  (mx,my,mzz)
c #PE real     rikx  (mx,my,mzz)
c _PE real     riky  (mx,my,mzz)
c #PE real     riky  (mx,my,mzz)
 
      real     vartop(mx,my),varbot(mx,my)
c #DC real                   varbou(mx,my)
 
 
C +--DATA
C +  ====
 
      data nntrac/0/
c #TC      nntrac=ntrac
 
 
C +--Reset of the Local Variables
C +  ============================
 
c _PE DO k=1,mzz
c _PE DO j=1,my
c _PE DO i=1,mx
c _PE   rikx(i,j,k) = 0.0
c _PE   riky(i,j,k) = 0.0
c _PE END DO
c _PE END DO
c _PE END DO
 
c #PE DO k=1,mzz
c #PE DO j=1,my
c #PE DO i=1,mx
c #PE   rikx(i,j,k) = 0.0
c #PE   riky(i,j,k) = 0.0
c #PE END DO
c #PE END DO
c #PE END DO
 
      DO k=1,mz
      DO j=1,my
      DO i=1,mx
c _PE     akhm(i,j,k) = 0.0
c _PE   pente1(i,j,k) = 0.0
c _PE   pente2(i,j,k) = 0.0
c _PE   pente3(i,j,k) = 0.0
c #PE     akhm(i,j,k) = 0.0
c #PE   pente1(i,j,k) = 0.0
c #PE   pente2(i,j,k) = 0.0
c #PE   pente3(i,j,k) = 0.0
        WKxyz3(i,j,k) = 0.0
        WKxyz4(i,j,k) = 0.0
        WKxyz5(i,j,k) = 0.0
      END DO
      END DO
      END DO
 
 
C +--Update of Local Variables and
C +            Mesh Averaged Horizontal Diffusion Coefficient akhm
C +  =============================================================
 
        akhsta        = 0.0
      DO k=1,mz
      DO i=ip11,mx1
      DO j=jp11,my1
        WKxyz1(i,j,k) = FIkhmn+0.25*(TUkhx(i,j,k)+TUkhx(im1(i),j,k))
      END DO
      END DO
 
      DO j=jp11,my1
      DO i=ip11,mx1
        WKxyz1(i,j,k) =     WKxyz1(i,j,k)
     .                        +0.25*(TUkhy(i,j,k)+TUkhy(i,jm1(j),k))
        akhsta        = max(WKxyz1(i,j,k),akhsta)
c #PE     akhm(i,j,k) =     WKxyz1(i,j,k)
c #PE     rikx(i,j,k) =     rolvDY(i,j,k) * slopex(i,j,k) *akhm(i,j,k)
c #PE     riky(i,j,k) =     rolvDY(i,j,k) * slopey(i,j,k) *akhm(i,j,k)
c _PE     akhm(i,j,k) =     WKxyz1(i,j,k)
c _PE     rikx(i,j,k) =     rolvDY(i,j,k) * slopex(i,j,k) *akhm(i,j,k)
c _PE     riky(i,j,k) =     rolvDY(i,j,k) * slopey(i,j,k) *akhm(i,j,k)
      END DO
      END DO
      END DO
 
 
C +--Local Time step
C +  ===============
 
      cflakh       = dt_Loc *akhsta / dx / dx
      ntDifH       = 3      *cflakh
      ntDifH       = max(iun,ntDifH)
      dtHDif       = dt_Loc /ntDifH
 
 
C +--Update of Local Coefficients
C +  ============================
 
c _PE IF (mmy.gt.2)                                               THEN
c _PE   DO k=1,mmz1
c _PE   DO j=jp11,my1
c _PE   DO i=ip11,mx1
c _PE     WKxyz1(i,j,k)= rikx(i,j,k) *riky(i,j,k) *gravi2 *0.5
c _PE.                  /max(epsi,akhm(i,j,k))
c _PE.                  /(sigma(k+1)-sigma(k))
c _PE   END DO
c _PE   END DO
c _PE   END DO
c _PE END IF
 
c #PE IF (mmy.gt.2)                                               THEN
c #PE   DO k=1,mmz1
c #PE   DO j=jp11,my1
c #PE   DO i=ip11,mx1
c #PE     WKxyz1(i,j,k)= rikx(i,j,k) *riky(i,j,k) *gravi2 *0.5
c #PE.                  /max(epsi,akhm(i,j,k))
c #PE.                  /(sigma(k+1)-sigma(k))
c #PE   END DO
c #PE   END DO
c #PE   END DO
c #PE END IF
 
         dx2inv = 1.0/dx/dx
 
        DO j=1,my
        DO i=1,mx
c _PE     akhm(i,j,mz) = 0.0
c #PE     akhm(i,j,mz) = 0.0
C +...    The vertical correction to horizontal diffusion is assumed
C +       to be equal to zero in the surface boundary layer
 
          WKxy1(i,j)   = 0.0
          WKxy2(i,j)   = 0.0
          WKxy3(i,j)   = dx2inv / pstDY(i,j)
          WKxy4(i,j)   = 1.0    /(pstDY(i,j)*pstDY(i,j))
c #DC     rhox (i,j,mz)= 0.0
        END DO
        END DO
 
        DO i=1,mx1
        DO j=1,my
          WKxy1(i,j) = 0.5*(pstDY(i,j)+pstDY(ip1(i),j))
        END DO
        END DO
 
      IF (mmy.gt.1)                                               THEN
        DO j=1,my1
        DO i=1,mx
          WKxy2(i,j) = 0.5*(pstDY(i,j)+pstDY(i,jp1(j)))
        END DO
        END DO
      END IF
 
                          gdx2  =  0.5        *gravit / dx
        DO k=1,mz
        DO j=1,my
        DO i=1,mx
          WKxyz3(i,j,k) = gdx2  / (pstDY(i,j) *dsig_2(k))
        END DO
        END DO
        END DO
 
c #DC   DO k=1,mmz1
c #DC   DO j=1,my
c #DC   DO i=1,mx
c #DC     rhox  (i,j,k)  = 0.5 *WKxy4(i,j)
c #DC.                        *(WKxyz1(i,j,k+1) +WKxyz1(i,j,k))
c #DC   END DO
c #DC   END DO
c #DC   END DO
 
        DO j=1,my
        DO i=1,mx
          WKxy4(i,j)     = 0.0
        END DO
        END DO
 
 
C +--Global Variables Loop
C +  =====================
 
             n_kq =   5    ! Nombre de variables à diffuser: u,v,(w),T,q
c #KT        n_kq =   4    !                                 u,v,(w),T
c #KQ        n_kq =  10+nntrac
      DO     iter =1,   ntDifH
      DO     ivar =1,   n_kq
 
c #TC   IF    (ivar.gt.10)                                  GO TO  341
 
        GO TO (331,332,333,334,335,336,337,338,339,340) ivar
 
 
C +--u Wind Speed Component
C +  ----------------------
 
 331    CONTINUE
 
c #DF     facxx = 1.0
c #DF     facxy = 0.5
c #DF     facyy = 0.5
          DO k=1,mz
          DO j=1,my
          DO i=1,mx
            WKxyz1(i,j,k) = uairDY(i,j,k)
c #DF       WKxyz2(i,j,k) = vairDY(i,j,k)
          END DO
          END DO
          END DO
 
          DO j=1,my
          DO i=1,mx
            vartop(i,j)   = ugeoDY(i,j,1)
c #Di       vartop(i,j)   = uairDI(i,j)
            varbot(i,j)   = uairDY(i,j,mz)
c #DC       varbou(i,j)   = vairDY(i,j,mz)
          END DO
          END DO
 
        GO TO 330
 
 
C +--v Wind Speed Component
C +  ----------------------
 
 332    CONTINUE
 
c #DF     facxx = 0.5
c #DF     facxy = 0.5
c #DF     facyy = 1.0
          DO k=1,mz
          DO j=1,my
          DO i=1,mx
            WKxyz1(i,j,k) = vairDY(i,j,k)
c #DF       WKxyz2(i,j,k) = uairDY(i,j,k)
          END DO
          END DO
          END DO
 
          DO j=1,my
          DO i=1,mx
            vartop(i,j)   = vgeoDY(i,j,1)
c #Di       vartop(i,j)   = vairDI(i,j)
            varbot(i,j)   = vairDY(i,j,mz)
c #DC       varbou(i,j)   = uairDY(i,j,mz)
          END DO
          END DO
 
        GO TO 330
 
 
C +--w Wind Speed Component (Non Hydrostatic Option)
C +  -----------------------------------------------
 
 333    CONTINUE
 
c #NH     DO k=1,mz
c #NH     DO j=1,my
c #NH     DO i=1,mx
c #NH       WKxyz1(i,j,k) = wairNH(i,j,k)
c #NH     END DO
c #NH     END DO
c #NH     END DO
 
c #NH     DO j=1,my
c #NH     DO i=1,mx
c #NH       vartop(i,j)   = wairNH(i,j,1)
c #NH       varbot(i,j)   = wairNH(i,j,mz)
c #NH     END DO
c #NH     END DO
 
        GO TO 330
 
 
C +--Potential Temperature
C +  ---------------------
 
 334    CONTINUE
 
          DO k=1,mz
          DO j=1,my
          DO i=1,mx
            WKxyz1(i,j,k) = pktaDY(i,j,k)
          END DO
          END DO
          END DO
 
          DO j=1,my
          DO i=1,mx
            vartop(i,j)   = pktaDY(i,j,1)
c #Di       vartop(i,j)   = pkttDI(i,j)
            varbot(i,j)   = pktaDY(i,j,mz)
          END DO
          END DO
 
        GO TO 330
 
 
C +--Specific Humidity
C +  -----------------
 
 335    CONTINUE
 
          DO k=1,mz
          DO j=1,my
          DO i=1,mx
            WKxyz1(i,j,k) =   qvDY(i,j,k)
          END DO
          END DO
          END DO
 
          DO j=1,my
          DO i=1,mx
            vartop(i,j)   =   qvDY(i,j,1)
c #Di       vartop(i,j)   = qvtoDI(i,j)
            varbot(i,j)   =   qvDY(i,j,mz)
          END DO
          END DO
 
        GO TO 330
 
 
C +--Cloud Droplets Concentration
C +  ----------------------------
 
 336    CONTINUE
 
        IF (micphy)                                               THEN
 
          DO k=1,mz
          DO j=1,my
          DO i=1,mx
            WKxyz1(i,j,k) = qwHY(i,j,k)
          END DO
          END DO
          END DO
 
          DO j=1,my
          DO i=1,mx
            vartop(i,j)   = qwHY(i,j,1)
            varbot(i,j)   = qwHY(i,j,mz)
          END DO
          END DO
 
        END IF
 
        GO TO 330
 
 
C +--Ice Crystals Concentration
C +  --------------------------
 
 337    CONTINUE
 
        IF (micphy)                                               THEN
 
          DO k=1,mz
          DO j=1,my
          DO i=1,mx
            WKxyz1(i,j,k) = qiHY(i,j,k)
          END DO
          END DO
          END DO
 
          DO j=1,my
          DO i=1,mx
            vartop(i,j)   = qiHY(i,j,1)
            varbot(i,j)   = qiHY(i,j,mz)
          END DO
          END DO
 
        END IF
 
        GO TO 330
 
 
C +--Rain Drops Concentration
C +  ------------------------
 
 338    CONTINUE
 
        IF (micphy)                                               THEN
 
          DO k=1,mz
          DO j=1,my
          DO i=1,mx
            WKxyz1(i,j,k) = qrHY(i,j,k)
          END DO
          END DO
          END DO
 
          DO j=1,my
          DO i=1,mx
            vartop(i,j)   = qrHY(i,j,1)
            varbot(i,j)   = qrHY(i,j,mz)
          END DO
          END DO
 
        END IF
 
        GO TO 330
 
 
C +--Snow Flakes Concentration
C +  -------------------------
 
 339    CONTINUE
 
        IF (micphy)                                               THEN
 
          DO k=1,mz
          DO j=1,my
          DO i=1,mx
            WKxyz1(i,j,k) = qsHY(i,j,k)
          END DO
          END DO
          END DO
 
          DO j=1,my
          DO i=1,mx
            vartop(i,j)   = qsHY(i,j,1)
            varbot(i,j)   = qsrfHY(i,j)
          END DO
          END DO
 
        END IF
 
        GO TO 330
 
 
C +--Ice Crystals Number
C +  -------------------
 
 340    CONTINUE
 
        IF (micphy)                                               THEN
 
          DO k=1,mz
          DO j=1,my
          DO i=1,mx
            WKxyz1(i,j,k) = ccniHY(i,j,k)
          END DO
          END DO
          END DO
 
          DO j=1,my
          DO i=1,mx
            vartop(i,j)   = ccniHY(i,j,1)
            varbot(i,j)   = ccniHY(i,j,mz)
          END DO
          END DO
 
        END IF
 
        GO TO 330
 
 
C +--Tracers
C +  -------
 
 341    CONTINUE
 
c #TC   itrac = ivar - 10
 
c #TC     DO k=1,mz
c #TC     DO j=1,my
c #TC     DO i=1,mx
c #TC       WKxyz1(i,j,k) = qxTC(i,j,k,itrac)
c #TC     END DO
c #TC     END DO
c #TC     END DO
 
c #TC     DO j=1,my
c #TC     DO i=1,mx
c #TC       vartop(i,j)   = qxTC(i,j,1,itrac)
c #TC       varbot(i,j)   = qsTC(i,j,  itrac)
c #TC     END DO
c #TC     END DO
 
        GO TO 330
 
 330    CONTINUE
 
 
C +--Boundary Conditions
C +  ===================
 
          DO k=1,mz
          DO j=1,my
            WKxyz1( 1,j,k) = WKxyz1(ip11,j,k)
            WKxyz1(mx,j,k) = WKxyz1(mx1,j,k)
          END DO
          END DO
 
          DO j=1,my
            varbot( 1,j)   = varbot(ip11,j)
            varbot(mx,j)   = varbot(mx1,j)
          END DO
 
        IF (mmy.gt.1)                                             THEN
          DO k=1,mz
          DO i=1,mx
            WKxyz1(i, 1,k) = WKxyz1(i,jp11,k)
            WKxyz1(i,my,k) = WKxyz1(i,my1,k)
          END DO
          END DO
 
          DO i=1,mx
            varbot(i, 1)   = varbot(i,jp11)
            varbot(i,my)   = varbot(i,my1)
          END DO
 
        END IF
 
 
C +--Lateral Diffusion of the Horizontal Wind Speed Components
C + (proportional to the deformation, WITHOUT the hybrid terms)
C +  =========================================================
 
c #DF   IF (ivar.le.2)                                            THEN
 
 
C +--Diffusion in the x Direction on Sigma Surfaces
C +  ----------------------------------------------
 
c #DF     DO k=1,mz
 
c #DF       DO j=1,my
c #DF       DO i=ip11,mx1
c #DF         WKxyz5(i,j,k) = facxx *WKxy3(i,j)*
c #DF.       (WKxy1(i     ,j) *TUkhx(i     ,j,k)
c #DF.                      *(WKxyz1(ip1(i),j,k)-WKxyz1(i,j,k))
c #DF.       -WKxy1(im1(i),j) *TUkhx(im1(i),j,k)
c #DF.                      *(WKxyz1(i,j,k)-WKxyz1(im1(i),j,k)))
C +...        Contribution Axx
 
c #DF       END DO
c #DF       END DO
 
 
C +--Diffusion in the x,y Directions on Sigma Surfaces
C +  -------------------------------------------------
 
c #DF       IF (mmy.gt.2)                                         THEN
 
c #DF         GO TO (3511,3512) ivar
 3511         CONTINUE
 
c #DF           DO j=jp11,my1
c #DF           DO i=ip11,mx1
c #DF             WKxyz5(i,j,k) =       WKxyz5(i,j,k)
c #DF.                          + facxy *WKxy3(i,j)* 0.25*
c #DF.            (pstDY(i     ,jp1(j)) *   akhm(i  ,jp1(j),k)
c #DF.          *(WKxyz2(ip1(i),jp1(j),k)-WKxyz2(im1(i),jp1(j),k))
c #DF.            -pstDY(i     ,jm1(j)) *   akhm(i  ,jm1(j),k)
c #DF.          *(WKxyz2(ip1(i),jm1(j),k)-WKxyz2(im1(i),jm1(j),k)))
C +...          Contribution Axy
 
c #DF           END DO
c #DF           END DO
c #DF         GO TO  3515
 
 3512         CONTINUE
 
c #DF           DO j=jp11,my1
c #DF           DO i=ip11,mx1
c #DF             WKxyz5(i,j,k) =       WKxyz5(i,j,k)
c #DF.                          + facxy *WKxy3(i,j)* 0.25*
c #DF.            (pstDY(ip1(i),j  )    *   akhm(ip1(i),j  ,k)
c #DF.          *(WKxyz2(ip1(i),jp1(j),k)-WKxyz2(ip1(i),jm1(j),k))
c #DF.            -pstDY(im1(i),j  )    *   akhm(im1(i),j  ,k)
c #DF.          *(WKxyz2(im1(i),jp1(j),k)-WKxyz2(im1(i),jm1(j),k)))
C +...          Contribution Axy
 
c #DF           END DO
c #DF           END DO
c #DF         GO TO  3515
 
 3515         CONTINUE
 
 
C +--Diffusion in the y Direction on Sigma Surfaces
C +  ----------------------------------------------
 
c #DF           DO j=jp11,my1
c #DF           DO i=ip11,mx1
c #DF             WKxyz5(i,j,k) =       WKxyz5(i,j,k)
c #DF.                          + facyy *WKxy3(i,j)*
c #DF.            (WKxy2(i,j     ) *  TUkhy(i,j     ,k)
c #DF.          *(WKxyz1(i,jp1(j),k)-WKxyz1(i,j,k))
c #DF.            -WKxy2(i,jm1(j)) *  TUkhy(i,jm1(j),k)
c #DF.          *(WKxyz1(i,j     ,k)-WKxyz1(i,jm1(j),k)))
C +...          Contribution Ayy
 
c #DF           END DO
c #DF           END DO
c #DF       END IF
c #DF     END DO
 
 
C +--Lateral Diffusion of the Horizontal Wind Speed Components
C + (proportional to the deformation, WITH    the hybrid terms)
C +  =========================================================
 
 
C +--Diffusion in the x and Sigma Directions (except Model Top and Bottom)
C +  ---------------------------------------------------------------------
 
c #DC     DO k=kp1(1),mmz1
 
c #DC       DO j=1,my
c #DC       DO i=ip11,mx1
c #DC         WKxyz5(i,j,k) =
c #DC.        WKxyz5(i,j,k) + facxx*WKxyz3(i,j,k)*
c #DC.                             (  rikx(ip1(i),j,k)
c #DC.      *(WKxyz1(ip1(i),j,k+1) -WKxyz1(ip1(i),j,k-1))
c #DC.                             -  rikx(im1(i),j,k)
c #DC.      *(WKxyz1(im1(i),j,k+1) -WKxyz1(im1(i),j,k-1))
C +...        Contribution Bxx
 
c #DC.                             +  rikx(i,j,k+1)
c #DC.      *(WKxyz1(ip1(i),j,k+1) -WKxyz1(im1(i),j,k+1))
c #DC.                             -  rikx(i,j,k-1)
c #DC.      *(WKxyz1(ip1(i),j,k-1) -WKxyz1(im1(i),j,k-1)))
C +...        Contribution Cxx
 
c #DC       END DO
c #DC       END DO
 
c #DC       IF (mmy.ge.2)                                         THEN
 
 
C +--Diffusion in the x,y and Sigma Directions (except Model Top and Bottom)
C +  -----------------------------------------------------------------------
 
c #DC         GO TO (3611,3612) ivar
 3611         CONTINUE
c #DC           DO j=jp11,my1
c #DC           DO i=ip11,mx1
c #DC             WKxyz5(i,j,k) =
c #DC.            WKxyz5(i,j,k) + facxy *WKxyz3(i,j,k)*
c #DC.                                  (  rikx(i,jp1(j),k)
c #DC.          *(WKxyz2(i,jp1(j),k+1)  -WKxyz2(i,jp1(j),k-1))
c #DC.                                  -  rikx(i,jm1(j),k)
c #DC.          *(WKxyz2(i,jm1(j),k+1)  -WKxyz2(i,jm1(j),k-1))
C +...            Contribution Bxy
 
c #DC.                                  +  riky(i,j,k+1)
c #DC.          *(WKxyz2(ip1(i),j,k+1)  -WKxyz2(im1(i),j,k+1))
c #DC.                                  -  riky(i,j,k-1)
c #DC.          *(WKxyz2(ip1(i),j,k-1)  -WKxyz2(im1(i),j,k-1)))
C +...            Contribution Cxy
 
c #DC           END DO
c #DC           END DO
c #DC         GO TO 3615
 
 3612         CONTINUE
c #DC           DO j=jp11,my1
c #DC           DO i=ip11,mx1
c #DC             WKxyz5(i,j,k) =
c #DC.            WKxyz5(i,j,k) + facxy *WKxyz3(i,j,k)*
c #DC.                                  (  riky(ip1(i),j,k)
c #DC.          *(WKxyz2(ip1(i),j,k+1)  -WKxyz2(ip1(i),j,k-1))
c #DC.                                    -riky(im1(i),j,k)
c #DC.          *(WKxyz2(im1(i),j,k+1)  -WKxyz2(im1(i),j,k-1))
C +...            Contribution Bxy
 
c #DC.                                  +rikx(i,j,k+1)
c #DC.          *(WKxyz2(i,jp1(j),k+1)-WKxyz2(i,jm1(j),k+1))
c #DC.                                  -rikx(i,j,k-1)
c #DC.          *(WKxyz2(i,jp1(j),k-1)-WKxyz2(i,jm1(j),k-1)))
C +...            Contribution Cxy
 
c #DC           END DO
c #DC           END DO
c #DC         GO TO 3615
 
 3615         CONTINUE
 
c #DC         DO j=jp11,my1
c #DC         DO i=ip11,mx1
c #DC             WKxyz5(i,j,k) =
c #DC.            WKxyz5(i,j,k) +facyy  *WKxyz3(i,j,k)*
c #DC.                                  (  riky(i,jp1(j),k)
c #DC.          *(WKxyz1(i,jp1(j),k+1)  -WKxyz1(i,jp1(j),k-1))
c #DC.                                  -  riky(i,jm1(j),k)
c #DC.          *(WKxyz1(i,jm1(j),k+1)  -WKxyz1(i,jm1(j),k-1))
C +...            Contribution Byy
 
c #DC.                                  +  riky(i,j,k+1)
c #DC.          *(WKxyz1(i,jp1(j),k+1)  -WKxyz1(i,jm1(j),k+1))
c #DC.                                  -  riky(i,j,k-1)
c #DC.          *(WKxyz1(i,jp1(j),k-1)  -WKxyz1(i,jm1(j),k-1)))
C +...            Contribution Cyy
 
c #DC.                                  +( rhox(i,j,k  )
c #DC.          *(WKxyz2(i,j,k+1)       -WKxyz2(i,j,k))
c #DC.                                  -  rhox(i,j,k-1)
c #DC.          *(WKxyz2(i,j,k)         -WKxyz2(i,j,k-1)))/dsigm1(k-1)
C +...            Contribution Dxy
 
c #DC         END DO
c #DC         END DO
c #DC       END IF
c #DC     END DO
 
 
C +--Diffusion in the x and Sigma Directions (Model Top)
C +  ---------------------------------------------------
 
c #DC         k=1
 
c #DC         DO j=1,my
c #DC         DO i=ip11,mx1
c #DC           WKxyz5(i,j,k) =
c #DC.          WKxyz5(i,j,k) + facxx *WKxyz3(i,j,k)*
c #DC.                                  (rikx(ip1(i),j,k)
c #DC.        *(WKxyz1(ip1(i),j,k+1)  -WKxyz1(ip1(i),j,k  ))
c #DC.                                  -rikx(im1(i),j,k)
c #DC.        *(WKxyz1(im1(i),j,k+1)  -WKxyz1(im1(i),j,k  ))
C +...          Contribution Bxx
 
c #DC.                                  +rikx(i,j,k+1)
c #DC.          *(WKxyz1(ip1(i),j,k+1)-WKxyz1(im1(i),j,k+1)))
C +...          Contribution Cxx
 
c #DC         END DO
c #DC         END DO
 
c #DC       IF (mmy.ge.2)                                         THEN
 
 
C +--Diffusion in the x,y and Sigma Directions (Model Top)
C +  -----------------------------------------------------
 
c #DC         GO TO (3711,3712) ivar
 3711         CONTINUE
 
c #DC           DO  j=jp11,my1
c #DC           DO  i=ip11,mx1
c #DC             WKxyz5(i,j,k) =
c #DC.            WKxyz5(i,j,k) + facxy *WKxyz3(i,j,k)*
c #DC.                                    (rikx(i,jp1(j),k)
c #DC.          *(WKxyz2(i,jp1(j),k+1)  -WKxyz2(i,jp1(j),k  ))
c #DC.                                    -rikx(i,jm1(j),k)
c #DC.          *(WKxyz2(i,jm1(j),k+1)  -WKxyz2(i,jm1(j),k  ))
C +...            Contribution Bxy
 
c #DC.                                    +riky(i,j,k+1)
c #DC.          *(WKxyz2(ip1(i),j,k+1)  -WKxyz2(im1(i),j,k+1)))
C +...            Contribution Cxy
 
c #DC           END DO
c #DC           END DO
c #DC         GO TO 3715
 
 3712         CONTINUE
c #DC           DO  j=jp11,my1
c #DC           DO  i=ip11,mx1
c #DC             WKxyz5(i,j,k) =
c #DC.            WKxyz5(i,j,k) + facxy *WKxyz3(i,j,k)*
c #DC.                                    (riky(ip1(i),j,k)
c #DC.          *(WKxyz2(ip1(i),j,k+1)  -WKxyz2(ip1(i),j,k  ))
c #DC.                                    -riky(im1(i),j,k)
c #DC.          *(WKxyz2(im1(i),j,k+1)  -WKxyz2(im1(i),j,k  ))
C +...            Contribution Bxy
 
c #DC.                                   +rikx(i,j,k+1)
c #DC.          *(WKxyz2(i,jp1(j),k+1) -WKxyz2(i,jm1(j),k+1)))
C +...            Contribution Cxy
 
c #DC           END DO
c #DC           END DO
 
 3715         CONTINUE
 
c #DC           DO  j=jp11,my1
c #DC           DO  i=ip11,mx1
c #DC             WKxyz5(i,j,k) =
c #DC.            WKxyz5(i,j,k) + facyy *WKxyz3(i,j,k)*
c #DC.                                    (riky(i,jp1(j),k)
c #DC.          *(WKxyz1(i,jp1(j),k+1)  -WKxyz1(i,jp1(j),k  ))
c #DC.                                    -riky(i,jm1(j),k)
c #DC.          *(WKxyz1(i,jm1(j),k+1)  -WKxyz1(i,jm1(j),k  ))
C +...            Contribution Byy
 
c #DC.                                    +riky(i,j,k+1)
c #DC.          *(WKxyz1(i,jp1(j),k+1)  -WKxyz1(i,jm1(j),k+1)))
C +...            Contribution Cyy
 
c #DC.             +rhox(i,j,k  )
c #DC.          *(WKxyz2(i,j  ,k+1)     -WKxyz2(i,j  ,k  )) /sigmid(k+1)
C +...            Contribution Dxy
 
c #DC           END DO
c #DC           END DO
 
c #DC       END IF
 
 
C +--Diffusion in the x and Sigma Directions (Model Bottom)
C +  ------------------------------------------------------
 
c #DC         k=mz
 
c #DC         DO j=1,my
c #DC         DO i=ip11,mx1
c #DC           WKxyz5(i,j,k) =
c #DC.          WKxyz5(i,j,k) + facxx *WKxyz3(i,j,k)*
c #DC.                                  (rikx(ip1(i),j,k)
c #DC.          *(varbot(ip1(i),j)    -WKxyz1(ip1(i),j,k-1))
c #DC.                                  -rikx(im1(i),j,k)
c #DC.          *(varbot(im1(i),j)    -WKxyz1(im1(i),j,k-1))
C +...          Contribution Bxx
 
c #DC.                                  -rikx(i,j,k-1)
c #DC.          *(WKxyz1(ip1(i),j,k-1)-WKxyz1(im1(i),j,k-1)))
C +...          Contribution Cxx
 
c #DC         END DO
c #DC         END DO
 
c #DC       IF (mmy.ge.2)                                         THEN
 
 
C +--Diffusion in the x,y and Sigma Directions (Model Bottom)
C +  --------------------------------------------------------
 
c #DC         GO TO (3811,3812) ivar
 3811         CONTINUE
 
c #DC           DO  j=jp11,my1
c #DC           DO  i=ip11,mx1
c #DC             WKxyz5(i,j,k) =
c #DC.            WKxyz5(i,j,k) + facxy *WKxyz3(i,j,k)*
c #DC.                                    (rikx(i,jp1(j),k)
c #DC.          *(varbou(i,jp1(j))      -WKxyz2(i,jp1(j),k-1))
c #DC.                                    -rikx(i,jm1(j),k)
c #DC.          *(varbou(i,jm1(j))      -WKxyz2(i,jm1(j),k-1))
C +...            Contribution Bxy
 
c #DC.                                    -riky(i,j,k-1)
c #DC.          *(WKxyz2(ip1(i),j,k-1)  -WKxyz2(im1(i),j,k-1)))
C +...            Contribution Cxy
 
c #DC           END DO
c #DC           END DO
c #DC         GO TO 3815
 
 3812         CONTINUE
c #DC           DO  j=jp11,my1
c #DC           DO  i=ip11,mx1
c #DC             WKxyz5(i,j,k) =
c #DC.            WKxyz5(i,j,k) + facxy *WKxyz3(i,j,k)*
c #DC.                                    (riky(ip1(i),j,k)
c #DC.          *(varbou(ip1(i),j)      -WKxyz2(ip1(i),j,k-1))
c #DC.                                    -riky(im1(i),j,k)
c #DC.          *(varbou(im1(i),j)      -WKxyz2(im1(i),j,k-1))
C +...            Contribution Bxy
 
c #DC.                                    -rikx(i,j,k-1)
c #DC.          *(WKxyz2(ip1(i),j,k-1)  -WKxyz2(im1(i),j,k-1)))
C +...            Contribution Cxy
 
c #DC           END DO
c #DC           END DO
 
c #DC         GO TO 3815
 
 3815         CONTINUE
 
c #DC           DO j=jp11,my1
c #DC           DO i=ip11,mx1
c #DC             WKxyz5(i,j,k) =
c #DC.            WKxyz5(i,j,k) + facyy *WKxyz3(i,j,k)*
c #DC.                                    (rikx(i,jp1(j),k)
c #DC.          *(varbot(i,jp1(j))      -WKxyz1(i,jp1(j),k-1))
c #DC.                                    -rikx(i,jm1(j),k)
c #DC.          *(varbot(i,jm1(j))      -WKxyz1(i,jm1(j),k-1))
C +...            Contribution Byy
 
c #DC.                                    -riky(i,j,k-1)
c #DC.          *(WKxyz1(i,jp1(j),k-1)  -WKxyz1(i,jm1(j),k-1)))
C +...            Contribution Cyy
 
c #DC.                                    -rhox(i,j,k-1)
c #DC.          *(WKxyz2(i,j     ,k  )  -WKxyz2(i,j  ,k-1)) /dsigm1(k-1)
C +...            Contribution Dxy
 
c #DC           END DO
c #DC           END DO
 
c #DC       END IF
 
c #DF   ELSE
 
 
C +--Lateral Diffusion of non Vectorial Model Variables (c #DF ON)
C +  Lateral Diffusion of all           Model Variables (c #DF OFF)
C +     (proportional to the gradient, terms in sigma surfaces)
C +  ==============================================================
 
 
C +--Diffusion in the x Direction on Sigma Surfaces
C +  ----------------------------------------------
 
            DO k=1,mz
            DO i=ip11,mx1
            DO j=1,my
              WKxyz5(i,j,k) = WKxy3(i,j)*
     .        (WKxy1(i     ,j) *  TUkhx(i     ,j,k)
     .                         *(WKxyz1(ip1(i),j,k)-WKxyz1(    i ,j,k))
     .        -WKxy1(im1(i),j) *  TUkhx(im1(i),j,k)
     .                         *(WKxyz1(i,j,k)     -WKxyz1(im1(i),j,k)))
            END DO
            END DO
            END DO
 
 
C +--Diffusion in the y Direction on Sigma Surfaces
C +  ----------------------------------------------
 
          IF (mmy.gt.2)                                             THEN
 
            DO k=1,mz
            DO j=jp11,my1
            DO i=1,mx
              WKxyz5(i,j,k) = WKxyz5(i,j,k) + WKxy3(i,j)*
     .        (WKxy2(i,j)      *  TUkhy(i,j     ,k)
     .                         *(WKxyz1(i,jp1(j),k)-WKxyz1(i,    j ,k))
     .        -WKxy2(i,jm1(j)) *  TUkhy(i,jm1(j),k)
     .                         *(WKxyz1(i,j,k)     -WKxyz1(i,jm1(j),k)))
            END DO
            END DO
            END DO
 
          END IF
 
 
C +--Lateral Diffusion of non Vectorial Model Variables (c #DF ON)
C +  Lateral Diffusion of all           Model Variables (c #DF OFF)
C +     (proportional to the gradient,            hybrid terms)
C +  ==============================================================
 
 
C +--Diffusion in the x and Sigma Directions (except Model Top and Bottom)
C +  ---------------------------------------------------------------------
 
c #CR       DO  k=kp1(1),mmz1
 
c #CR         DO j=1,my
c #CR         DO i=ip11,mx1
c #CR           WKxy4(i,j) = WKxyz3(i,j,k)*(
c #CR.                         rikx(ip1(i),j,k)
c #CR.                     *(WKxyz1(ip1(i),j,k+1)-WKxyz1(ip1(i),j,k-1))
c #CR.                        -rikx(im1(i),j,k)
c #CR.                     *(WKxyz1(im1(i),j,k+1)-WKxyz1(im1(i),j,k-1))
c #CR.          +rikx(i   ,j,k+1)
c #CR.                     *(WKxyz1(ip1(i),j,k+1)-WKxyz1(im1(i),j,k+1))
c #CR.          -rikx(i   ,j,k-1)
c #CR.                     *(WKxyz1(ip1(i),j,k-1)-WKxyz1(im1(i),j,k-1))
c #CR.                              )
c #CR         END DO
c #CR         END DO
 
c #CR         IF (mmy.eq.1)                                       THEN
 
c #CR           DO i=ip11,mx1
c #CR             WKxyz5(i,j,k) = WKxyz5(i,j,k)
c #CR.                    + 0.25*(WKxy4(ip1(i),1) +2.0*WKxy4(i,1)
c #CR.                           +WKxy4(im1(i),1))
c #CR           END DO
 
 
C +--Diffusion in the y and Sigma Directions (except Model Top and Bottom)
C +  ---------------------------------------------------------------------
 
c #CR         ELSE
 
c #CR           DO j=jp11,my1
c #CR           DO i=1,mx
c #CR             WKxy4(i,j) = WKxy4(i,j)
c #CR.                      + WKxyz3(i,j,k)*(
c #CR.                          riky(i,jp1(j),k)
c #CR.                      *(WKxyz1(i,jp1(j),k+1)-WKxyz1(i,jp1(j),k-1))
c #CR.                         -riky(i,jm1(j),k)
c #CR.                      *(WKxyz1(i,jm1(j),k+1)-WKxyz1(i,jm1(j),k-1))
c #CR.                         +riky(i,j,   k+1)
c #CR.                      *(WKxyz1(i,jp1(j),k+1)-WKxyz1(i,jm1(j),k+1))
c #CR.                         -riky(i,j,   k-1)
c #CR.                      *(WKxyz1(i,jp1(j),k-1)-WKxyz1(i,jm1(j),k-1))
c #CR.                                           )
c #CR           END DO
c #CR           END DO
 
c #CR           DO j=jp11,my1
c #CR           DO i=ip11,mx1
c #CR             WKxyz5(i,j,k) = WKxyz5(i,j,k)  + 0.0625*
c #CR.           (WKxy4(im1(i),jp1(j))+2.0*WKxy4(i,jp1(j))
c #CR.       +    WKxy4(ip1(i),jp1(j)) +
c #CR.        2.0*WKxy4(im1(i),j)     +4.0*WKxy4(i,j)
c #CR.       +2.0*WKxy4(ip1(i),j) +
c #CR.            WKxy4(im1(i),jm1(j))+2.0*WKxy4(i,jm1(j))
c #CR.       +    WKxy4(ip1(i),jm1(j))  )
c #CR           END DO
c #CR           END DO
 
c #CR         END IF
 
c #CR       END DO
 
 
C +--Diffusion in the x and Sigma Directions (Model Top)
C +  ---------------------------------------------------
 
c #CR       k=1
 
c #CR       DO j=1,my
c #CR       DO i=ip11,mx1
c #CR         WKxy4(i,j) = WKxyz3(i,j,k)*(
c #CR.                       rikx(ip1(i),j,k)
c #CR.                   *(WKxyz1(ip1(i),j,k+1)-WKxyz1(ip1(i),j,k  ))
c #CR.                      -rikx(im1(i),j,k)
c #CR.                   *(WKxyz1(im1(i),j,k+1)-WKxyz1(im1(i),j,k  ))
c #CR.                      +rikx(i,j,   k+1)
c #CR.                   *(WKxyz1(ip1(i),j,k+1)-WKxyz1(im1(i),j,k+1)))
c #CR       END DO
c #CR       END DO
 
c #CR       IF (mmy.eq.1)                                         THEN
 
c #CR         DO i=ip11,mx1
c #CR           WKxyz5(i,j,k) = WKxyz5(i,j,k)
c #CR.                  + 0.25*(WKxy4(ip1(i),1) +2.0*WKxy4(i,1)
c #CR.                         +WKxy4(im1(i),1))
c #CR         END DO
 
 
C +--Diffusion in the y and Sigma Directions (Model Top)
C +  ---------------------------------------------------
 
c #CR       ELSE
 
c #CR         DO j=jp11,my1
c #CR         DO i=1,mx
c #CR           WKxy4(i,j) = WKxy4(i,j)
c #CR.                    + WKxyz3(i,j,k)*(
c #CR.                        riky(i,jp1(j),k)
c #CR.                    *(WKxyz1(i,jp1(j),k+1)-WKxyz1(i,jp1(j),k  ))
c #CR.                       -riky(i,jm1(j),k)
c #CR.                    *(WKxyz1(i,jm1(j),k+1)-WKxyz1(i,jm1(j),k  ))
c #CR.                       +riky(i,j,k+1)
c #CR.                    *(WKxyz1(i,jp1(j),k+1)-WKxyz1(i,jm1(j),k+1)))
c #CR         END DO
c #CR         END DO
 
c #CR         DO j=jp11,my1
c #CR         DO i=ip11,mx1
c #CR           WKxyz5(i,j,k) = WKxyz5(i,j,k)  + 0.0625*
c #CR.          (WKxy4(im1(i),jp1(j))+2.0*WKxy4(i,jp1(j))
c #CR.     +     WKxy4(ip1(i),jp1(j)) +
c #CR.       2.0*WKxy4(im1(i),j)     +4.0*WKxy4(i,j)
c #CR.     + 2.0*WKxy4(ip1(i),j) +
c #CR.           WKxy4(im1(i),jm1(j))+2.0*WKxy4(i,jm1(j))
c #CR.     +     WKxy4(ip1(i),jm1(j))  )
c #CR         END DO
c #CR         END DO
 
c #CR       END IF
 
 
C +--Diffusion in the x and Sigma Directions (Model Bottom)
C +  ------------------------------------------------------
 
c #CR       k=mz
 
c #CR       DO j=1,my
c #CR       DO i=ip11,mx1
c #CR         WKxy4(i,j) = WKxyz3(i,j,k)*(
c #CR.                       rikx(ip1(i),j,k)
c #CR.                   *(varbot(ip1(i),j)    -WKxyz1(ip1(i),j,k-1))
c #CR.                      -rikx(im1(i),j,k)
c #CR.                   *(varbot(im1(i),j)    -WKxyz1(im1(i),j,k-1))
c #CR.                      -rikx(i,j,   k-1)
c #CR.                   *(WKxyz1(ip1(i),j,k-1)-WKxyz1(im1(i),j,k-1)))
c #CR       END DO
c #CR       END DO
 
c #CR       IF (mmy.eq.1)                                         THEN
 
c #CR         DO i=ip11,mx1
c #CR           WKxyz5(i,j,k) = WKxyz5(i,j,k)
c #CR.                  + 0.25*(WKxy4(ip1(i),1) +2.0*WKxy4(i,1)
c #CR.                         +WKxy4(im1(i),1))
c #CR         END DO
 
 
C +--Diffusion in the y and Sigma Directions (Model Bottom)
C +  ------------------------------------------------------
 
c #CR       ELSE
 
c #CR         DO j=jp11,my1
c #CR         DO i=1,mx
c #CR           WKxyz5(i,j,k) = WKxyz5(i,j,k)
c #CR.                        + WKxyz3(i,j,k)*(
c #CR.                         riky(i,jp1(j),k)
c #CR.                     *(varbot(i,jp1(j))    -WKxyz1(i,jp1(j),k-1))
c #CR.                        -riky(i,jm1(j),k)
c #CR.                     *(varbot(i,jm1(j))    -WKxyz1(i,jm1(j),k-1))
c #CR.                        -riky(i,j,k-1)
c #CR.                     *(WKxyz1(i,jp1(j),k-1)-WKxyz1(i,jm1(j),k-1))
c #CR.                                                 )
c #CR         END DO
c #CR         END DO
C +
c #CR         DO j=jp11,my1
c #CR         DO i=ip11,mx1
c #CR           WKxyz5(i,j,k) = WKxyz5(i,j,k)  + 0.0625*
c #CR.          (WKxy4(im1(i),jp1(j))+2.0*WKxy4(i,jp1(j))
c #CR.      +    WKxy4(ip1(i),jp1(j)) +
c #CR.       2.0*WKxy4(im1(i),j)     +4.0*WKxy4(i,j)
c #CR.      +2.0*WKxy4(ip1(i),j) +
c #CR.           WKxy4(im1(i),jm1(j))+2.0*WKxy4(i,jm1(j))
c #CR.      +    WKxy4(ip1(i),jm1(j))  )
c #CR         END DO
c #CR         END DO
c #CR       END IF
 
c #DF   END IF
 
 
C +--Update of the Global Variables
C +  ==============================
 
 
c #TC   IF    (ivar.gt.10)                                   GO TO 411
 
        GO TO (401,402,403,404,405,406,407,408,409,410) ivar
 
 
C +--u Wind Speed Component
C +  ----------------------
 
 401    CONTINUE
          DO k=1,mz
          DO j=jp11,my1
          DO i=ip11,mx1
            uairDY(i,j,k) = WKxyz1(i,j,k) + WKxyz5(i,j,k) *dtHDif
          END DO
          END DO
          END DO
        GO TO 400
 
 
C +--v Wind Speed Component
C +  ----------------------
 
 402    CONTINUE
          DO k=1,mz
          DO j=jp11,my1
          DO i=ip11,mx1
            vairDY(i,j,k) = WKxyz1(i,j,k) + WKxyz5(i,j,k) *dtHDif
          END DO
          END DO
          END DO
        GO TO 400
 
 
C +--w Wind Speed Component (Non Hydrostatic Option)
C +  -----------------------------------------------
 
 403    CONTINUE
c #NH     DO k=1,mz
c #NH     DO j=jp11,my1
c #NH     DO i=ip11,mx1
c #NH       wairNH(i,j,k) = WKxyz1(i,j,k) + WKxyz5(i,j,k) *dtHDif
c #NH     END DO
c #NH     END DO
c #NH     END DO
        GO TO 400
 
 
C +--Potential Temperature
C +  ---------------------
 
 404    CONTINUE
          DO k=1,mz
          DO j=jp11,my1
          DO i=ip11,mx1
            pktaDY(i,j,k) = WKxyz1(i,j,k) + WKxyz5(i,j,k) *dtHDif
c #KT.                                    * 0.50
! #KT.                                    * max(0,min(1,mzabso-k))
          END DO
          END DO
          END DO
        GO TO 400
 
 
C +-- Specific Humidity
C +   -----------------
 
 405    CONTINUE
          DO k=1,mz
          DO j=jp11,my1
          DO i=ip11,mx1
              qvDY(i,j,k) = WKxyz1(i,j,k) + WKxyz5(i,j,k) *dtHDif
! #kk.                                    * 0.50
          END DO
          END DO
          END DO
        GO TO 400
 
 
C +--Cloud Droplets Concentration
C +  ----------------------------
 
 406    CONTINUE
          IF (micphy)                                             THEN
            DO k=1,mz
            DO j=jp11,my1
            DO i=ip11,mx1
              qwHY(i,j,k) = WKxyz1(i,j,k) + WKxyz5(i,j,k) *dtHDif
            END DO
            END DO
            END DO
          END IF
        GO TO 400
 
 
C +--Ice Crystals Concentration
C +  --------------------------
 
 407    CONTINUE
          IF (micphy)                                             THEN
            DO k=1,mz
            DO j=jp11,my1
            DO i=ip11,mx1
              qiHY(i,j,k) = WKxyz1(i,j,k) + WKxyz5(i,j,k) *dtHDif
            END DO
            END DO
            END DO
          END IF
        GO TO 400
 
 
C +--Rain Drops Concentration
C +  ------------------------
 
 408    CONTINUE
          IF (micphy)                                             THEN
            DO k=1,mz
            DO j=jp11,my1
            DO i=ip11,mx1
              qrHY(i,j,k) = WKxyz1(i,j,k) + WKxyz5(i,j,k) *dtHDif
            END DO
            END DO
            END DO
          END IF
        GO TO 400
 
 
C +--Snow Flakes Concentration
C +  -------------------------
 
 409    CONTINUE
          IF (micphy)                                             THEN
            DO k=1,mz
            DO j=jp11,my1
            DO i=ip11,mx1
              qsHY(i,j,k) = WKxyz1(i,j,k) + WKxyz5(i,j,k) *dtHDif
            END DO
            END DO
            END DO
          END IF
        GO TO 400
 
 
C +--Ice Crystals Number
C +  -------------------
 
 410    CONTINUE
          IF (micphy)                                             THEN
            DO k=1,mz
            DO j=jp11,my1
            DO i=ip11,mx1
              ccniHY(i,j,k) = WKxyz1(i,j,k) + WKxyz5(i,j,k) *dtHDif
            END DO
            END DO
            END DO
          END IF
        GO TO 400
 
 
C +--Tracers
C +  -------
 
 411    CONTINUE
 
c #TC     itrac = ivar - 10
 
c #TC     DO k=1,mz
c #TC     DO j=jp11,my1
c #TC     DO i=ip11,mx1
c #TC       qxTC(i,j,k,itrac) = WKxyz1(i,j,k) + WKxyz5(i,j,k) *dtHDif
c #TC     END DO
c #TC     END DO
c #TC     END DO
 
 400    CONTINUE
 
      END DO
      END DO
 
 
C +--Slope Effect on the Vertical Diffusion Coefficient
C +  ==================================================
 
c _PE DO k=1,mmz1
c _PE DO j=1,my
c _PE DO i=1,mx
c #PE DO k=1,mmz1
c #PE DO j=1,my
c #PE DO i=1,mx
c _HH   alph2 = 0.50 *(slopex(i,j,k+1) + slopex(i,j,k))
c _HH   beta2 = 0.50 *(slopey(i,j,k+1) + slopey(i,j,k))
c _HH   akhm2 = 0.50 *(  akhm(i,j,k+1) +   akhm(i,j,k))
c _HH   alph22= alph2 *alph2
c _HH   beta22= beta2 *beta2
c _HH   pente3(i,j,k) = akhm2 * (     alph22 +      beta22)
 
c #DF   alph2 = 0.50 *(slopex(i,j,k+1) + slopex(i,j,k))
c #DF   beta2 = 0.50 *(slopey(i,j,k+1) + slopey(i,j,k))
c #DF   akhm2 = 0.50 *(  akhm(i,j,k+1) +   akhm(i,j,k))
c #DF   alph22= alph2 *alph2
c #DF   beta22= beta2 *beta2
c #DF   pente1(i,j,k) = akhm2 * (     alph22 + 0.50*beta22)
c #DF   pente2(i,j,k) = akhm2 * (0.50*alph22 +      beta22)
c #DF   pente3(i,j,k) = akhm2 * (     alph22 +      beta22)
c _PE END DO
c _PE END DO
c _PE END DO
c #PE END DO
c #PE END DO
c #PE END DO
 
 
C +--Work Arrays Reset
C +  =================
 
      DO j=1,my
      DO i=1,mx
        WKxy1(i,j) = 0.00
        WKxy2(i,j) = 0.00
        WKxy3(i,j) = 0.00
        WKxy4(i,j) = 0.00
      END DO
      END DO
 
      DO k=1,mz
      DO j=1,my
      DO i=1,mx
        WKxyz1(i,j,k) = 0.00
        WKxyz2(i,j,k) = 0.00
        WKxyz3(i,j,k) = 0.00
        WKxyz4(i,j,k) = 0.00
        WKxyz5(i,j,k) = 0.00
      END DO
      END DO
      END DO
 
      return
      end
