 
 
      subroutine PHYmar
 
C +------------------------------------------------------------------------+
C | MAR PHYmar                                             16-11-2002  MAR |
C |   Subroutine PHYmar is used to define physical constants used in   MAR |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
      include 'MARdim.inc'
      include 'MAR_GE.inc'
 
      CHARACTER*5     :: RCP_CMIP5
      common/c_RCP_CMIP5/RCP_CMIP5
 
C +--LMDZ Time Constants
C +  ===================
 
      logical  YR_360,YR_365 ! y_360
      integer  n
      integer  njyr30(0:12), njmo30(0:12)
      integer  njyb30(0:12), njmb30(0:12)
 
      character*10 CALENDAR
 
      data  njmo30
     .     /0,30,30,30,30, 30, 30, 30, 30, 30, 30, 30, 30/
      data  njmb30
     .     /0, 0, 0, 0, 0,  0,  0,  0,  0,  0,  0,  0,  0/
      data  njyr30
     .     /0, 0,30,60,90,120,150,180,210,240,270,300,330/
      data  njyb30
     .     /0, 0, 0, 0, 0,  0,  0,  0,  0,  0,  0,  0,  0/
C +...      njmoGE: Nb of Days in each Month of the Year
C +         njmbGE: Leap  Year Correction    to njmoGE
C +         njyrGE: Nb of Days since   Begin of the Year,
C +                            before  Current Month
C +         njybGE: Leap  Year Correction    to njyrGE
 
        YR_360  = .false.
        YR_365  = .false.
      RCP_CMIP5 = "RCP60" ! RCP26,RCP45,RCP60,RCP85
 
      open(unit=10,file="MARscenario.ctr",status="old",err=1000)
      read(10,*,end=1001) RCP_CMIP5
      read(10,*,end=1001) CALENDAR
 
      if(TRIM(CALENDAR)=="YR_360") YR_360=.true.
      if(TRIM(CALENDAR)=="YR_365") YR_365=.true.
 
      goto 1001
 
1000  continue
      print *,"PHYmar ERROR: MARscenario.ctr no found!!!"
 
1001  continue
      print *,"PHYmar WARNING: RCP="//RCP_CMIP5
 
      close(10)
 
      IF (YR_360)                                                   THEN
        print *,"PHYmar WARNING: YR_360=.true."
        DO  n = 0,12
            njmoGE(n) = njmo30(n)
            njmbGE(n) = njmb30(n)
            njyrGE(n) = njyr30(n)
            njybGE(n) = njyb30(n)
        ENDDO
      END IF
 
      IF (YR_365)                                                   THEN
        print *,"PHYmar WARNING: YR_365=.true."
        DO  n = 0,12
            njmbGE(n) = 0
            njybGE(n) = 0
        ENDDO
      END IF
 
C +--CONSTANTS
C +  =========
 
C +--Time          Constants
C +  -----------------------
 
      nhyrGE =(njyrGE(12) + njmoGE(12)) * 24
C +...nhyrGE : Number of Hours in one Year
 
 
C +--Geometric     Constants
C +  -----------------------
 
      third= 1./3.
      pi   = acos(-unun)
C +...pi    /   3.141592653589793238462643/
 
      hourad  = pi / 12.0
      degrad  = pi /180.0
C +...degrad:   Conversion Factor: Radian --> Degrees
 
 
C +--Thermodynamic Constants
C +  -----------------------
 
        CvDryA = Cp             - RDryAi
        cap    = RDryAi         / Cp
        racv   = RDryAi         / CvDryA
 
        r_LvCp = Lv_H2O         / Cp
        r_LcCp =(Ls_H2O-Lv_H2O) / Cp
        r_LsCp = Ls_H2O         / Cp
 
 
C +--Earth         Constants
C +  -----------------------
 
        gravi2 = gravit * gravit
        grvinv = 1.0    / gravit
 
      return
      end
