 
 
      subroutine PHY_SISVAT(ihamr_SIS,nhamr_SIS)
 
C +------------------------------------------------------------------------+
C | MAR          SISVAT_Driver                        Thu 10-Nov-2011  MAR |
C |   SubRoutine PHY_SISVAT    interfaces MAR        with        the       |
C |              Soil/Ice Snow Vegetation Atmosphere Transfer Scheme       |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT: ihamr_SIS: Time Digital Filter Status                         |
C |   ^^^^^  nhamr_SIS: Time Digital Filter Set Up                         |
C |                                                                        |
C |   INPUT    (via common block)                                          |
C |   ^^^^^     VegMod: SISVAT    is set up when .T.                       |
C |             SnoMod: Snow Pack is set up when .T.                       |
C |             reaLBC: Update Bound.Condit.when .T.                       |
C |             iterun: Run Iterations Counter                             |
C |                                                                        |
C |   INPUT    (via common block)                                          |
C |   ^^^^^     xxxxTV: SISVAT/MAR interfacing variables                   |
C |                                                                        |
C | # CAUTION: #sa: Stand Alone Preprocessing Label must be removed        |
C | # ^^^^^^^       when SISVAT is coupled with MAR                        |
C |                                                                        |
C |   Preprocessing  Option: SISVAT PHYSICS                                |
C |   ^^^^^^^^^^^^^^^^^^^^^  ^^^^^^^^^^^^^^                                |
C | #                        #HY                                           |
C | #                        #SN: Snow         Model                       |
C | #                        #BS: Blowing Snow Parameterization            |
C | #                        #SI  Sea-Ice      Parameterization            |
C | #                        #GP  LAI and GLF  Variations not specified    |
C | #                        #OP  SST       is interactive                 |
C |                                                                        |
C | #                        #DS: diffuse radiation differing from direct  |
C |                              (variable RADsod must still be included)  |
C |                                                                        |
C |   Preprocessing  Option: SISVAT PHYSICS: Col de Porte                  |
C |   ^^^^^^^^^^^^^^^^^^^^^  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^                  |
C | #                        #CP: SBL,                       Col de Porte  |
C | #                        #cp  Solar Radiation,           Col de Porte  |
C | #                        #AG: Snow Ageing,               Col de Porte  |
C |                                                                        |
C |                                                                        |
C |   Preprocessing  Option: SISVAT IO (not always a standard preprocess.) |
C |   ^^^^^^^^^^^^^^^^^^^^^  ^^^^^^^^^                                     |
C |   FILE                 |      CONTENT                                  |
C |   ~~~~~~~~~~~~~~~~~~~~~+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ |
C | # ANI.yyyymmdd.LAB.nc  | #NC: OUTPUT on NetCDF File (Stand Alone EXP.) |
C |                        |                                               |
C | # SISVAT_iii_jjj_n     | #E0: OUTPUT on ASCII  File (SISVAT Variables) |
C | #                      |(#E0  MUST BE PREPROCESSED BEFORE #e1 & #e2 !) |
C | # SISVAT_iii_jjj_n     ! #e1: OUTPUT/Verification: Energy Conservation |
C | # SISVAT_iii_jjj_n     ! #e2: OUTPUT/Verification: Energy Consrv.2e pt.|
C |                        |                           (no premature stop) |
C |                        |                                               |
C | # SISVAT_iii_jjj_n     | #m0: OUTPUT/Verification: H2O    Conservation |
C | # SISVAT_iii_jjj_n     | #m1: OUTPUT/Verification: * Mass Conservation |
C | # SISVAT_iii_jjj_n     | #m2: OUTPUT/Verification: SeaIce Conservation |
C |                        |                                               |
C | # SISVAT_zSn.vz        | #vz: OUTPUT/Verification: Snow Layers Agrega. |
C |                        |      unit 41, SubRoutine  SISVAT_zSn **ONLY** |
C | # SISVAT_qSo.vw        | #vw: OUTPUT/Verif+Detail: H2O    Conservation |
C |                        |      unit 42, SubRoutine  SISVAT_qSo **ONLY** |
C | # SISVAT_qSn.vm        | #vm: OUTPUT/Verification: Energy/Water Budget |
C |                        |      unit 43, SubRoutine  SISVAT_qSn **ONLY** |
C | # SISVAT_qSn.vu        | #vu: OUTPUT/Verification: Slush  Parameteriz. |
C |                        |      unit 44, SubRoutine  SISVAT_qSn **ONLY** |
C | # SISVAT_wEq.ve        | #ve: OUTPUT/Verification: Snow/Ice Water Eqv. |
C |                        |      unit 45, SubRoutine  SISVAT_wEq **ONLY** |
C | # SnOptP____.va        | #va: OUTPUT/Verification: Albedo Parameteriz. |
C |                        |      unit 46, SubRoutine  SnOptP     **ONLY** |
C | # SISVAT_GSn.vp        | #vp: OUTPUT/Verification: Snow   Properties   |
C |                        |      unit 47, SubRoutines SISVAT_zSn, _GSn    |
C | # PHY_SISVAT.v0        | #v0: OUTPUT/Verification: DUMP                |
C |                        |      unit 50, SubRoutine  PHY_SISVAT **ONLY** |
C |                        |                                               |
C | # stdout               | #s0: OUTPUT of Snow Buffer Layer              |
C |                        |      unit  6, SubRoutine  SISVAT     **ONLY** |
C | # stdout               | #wx: OUTPUT/Verification: specified i,j,k,n   |
C | # stdout               | #wz: OUTPUT of Roughness Length (Blown Snow)  |
C |                        |      unit  6, SubRoutines SISVAT, PHY_SISVAT  |
C |                        |                                               |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include  "MARCTR.inc"
      include  "MARphy.inc"
 
      include  "MARdim.inc"
      integer   mw0
      parameter(mw0=3)
      include  "MARgrd.inc"
      include  "MAR_GE.inc"
 
      include  "MAR_RA.inc"
 
      include  "MAR_LB.inc"
      include  "MAR_DY.inc"
      include  "MAR_HY.inc"
c #BW include  "MAR_TE.inc"
      include  "MAR_TU.inc"
 
c #TC include  "MAR_TC.inc"
 
      include  "MAR_SV.inc"
      include  "MARdSV.inc"
      include  "MAR0SV.inc"
 
      include  "MAR_SL.inc"
c #AO include  "MAR_AO.inc"
      include  "MAR_TV.inc"
      include  "MAR_BS.inc"
      include  "MARsSN.inc"
      include  "MAR_IB.inc"
 
      include  "MARsIB.inc"
c #PO include  "MAR_PO.inc"
 
      include  "MAR_WK.inc"
 
      integer   ihamr_SIS ,nhamr_SIS          ! Hamming Filter Counters
      integer   newglfSIS                     !
      integer   newsicSI                      !
 
      real          rtime
      integer       ntime
      common/c_time/ntime
 
 
C +--INTERFACE Variables
C +  ===================
 
      include  "MARxSV.inc"
C +--Level of negligible blown Snow Particles Concentration
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      integer                 kB
      common  /SISVAT_MAR__BS/kB
 
C +--10-m Level
C +  ~~~~~~~~~~
        integer                  kSBL
        common /PHY_SISVAT_SBLi/ kSBL
        real                     rSBL10,VV__10(mx,my)
        common /PHY_SISVAT_SBLr/ rSBL10
 
C +--V,  dT(a-s)    Time Moving Averages
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      real             V__mem(klonv,ntaver)   ! ntaver defined in MAR_SL.inc
      real             VVmmem(klonv)          !
      common/SVeSBLmem/V__mem,VVmmem          !
      real             T__mem(klonv,ntaver)   !
      real             dTmmem(klonv)          !
      common/STeSBLmem/T__mem,dTmmem          !
 
!$OMP threadprivate(/xSISVAT_I/,/xSISVAT_R/,/SVeSBLmem/,/STeSBLmem/)
 
C +--u*, u*T*, u*s* Time Moving Averages
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #AM real             u__mem(klonv,ntaver)   ! ntaver defined in MAR_SL.inc
c #AT real             uT_mem(klonv,ntaver)   !
c #AS real             us_mem(klonv,ntaver)   !
c #AM common/S_eSBLmem/u__mem                 !
c #AT.                ,uT_mem                 !
c #AS.                ,us_mem                 !
 
C +--OUTPUT for Stand Alone NetCDF File
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #NC real          SOsoKL(klonv)             ! Absorbed Solar Radiation
c #NC real          IRsoKL(klonv)             ! Absorbed IR    Radiation
c #NC real          HSsoKL(klonv)             ! Absorbed Sensible Heat Flux
c #NC real          HLsoKL(klonv)             ! Absorbed Latent   Heat Flux
c #NC real          HLs_KL(klonv)             ! Evaporation
c #NC real          HLv_KL(klonv)             ! Transpiration
c #NC common/DumpNC/SOsoKL,IRsoKL
c #NC.             ,HSsoKL,HLsoKL
c #NC.             ,HLs_KL,HLv_KL
 
c #NC real          SOsoNC(mx,my,nvx)         ! Absorbed Solar Radiation
c #NC real          IRsoNC(mx,my,nvx)         ! Absorbed IR    Radiation
c #NC real          HSsoNC(mx,my,nvx)         ! Absorbed Sensible Heat Flux
c #NC real          HLsoNC(mx,my,nvx)         ! Absorbed Latent   Heat Flux
c #NC real          HLs_NC(mx,my,nvx)         ! Evaporation
c #NC real          HLv_NC(mx,my,nvx)         ! Transpiration
c #NC real          eta_NC(mx,my,nvx)         ! Soil Humidity
c #NC common/writNC/SOsoNC,IRsoNC             !
c #NC.             ,HSsoNC,HLsoNC             !
c #NC.             ,HLs_NC,HLv_NC,eta_NC      !
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! #wx integer             iSV_v1,jSV_v1,nSV_v1,kSV_v1,lSV_v1
! #wx common/SISVAT_EV/   iSV_v1,jSV_v1,nSV_v1,kSV_v1,lSV_v1
 
 
C +--Internal  Variables
C +  ===================
 
      logical StandA
      logical glfFIX
 
      integer                  ijnmax,nvcmax  ! Control Indices Distribution
      common  /SISVAT_MAR_Loc/ ijnmax,nvcmax  ! Control Indices Distribution
      integer k2i(klonv)                      ! Distributed i      Index
      integer k2j(klonv)                      ! Distributed j      Index
      integer k2n(klonv)                      ! Distributed mosaic Index
c #VR integer               ij0ver(mx,my,mw)  ! Verification of Vectorization
c #VR integer               ij_ver(mx,my,mw)  ! Verification of Vectorization
c #VR integer           ij2,ijdver(mx,my,mw)  ! Verification of Vectorization
 
      character*1   cha  ,chb
      integer iwr  ,ipt  ,l,nvcmax2,ijnmax2
      integer ikl  ,isl  ,isn
      integer ijn  ,ij   ,nnn
      integer nvc  ,nkl  ,n     ,nt
 
      real     slopx ,slopy                   ! Surf.Slope, x, y
      real     czemin                         ! Minimum accepted
C +                                           ! cos(Solar zenith.Dist.)
      real     Upw_IR(mx,my)                  ! Upward IR Flux
      real     IR_aux                         ! Upward IR Flux  (dummy)
 
      real     uqstar                         ! u*q*
      real     rhAir                          ! Air    Densitity
      real     Ua_min                         ! Minimum Air Velocity
      real     rr__DR(mx,my,mw)               ! Desagregated  Rain
      real     hfra  (mx,my,mw)               ! Frazil Thickness
      real     Rnof  (mx,my,mw)               ! RunOFF Intensity
      real     d_snow,SnowOK                  ! Snow Precip.: Total
      real     dbsnow                         ! Snow Precip.: from Drift
c #SZ real     dsastr(mx,my)                  ! z0(Sastrugi): Variation
 
      real     WVaLim(mx,my)                  !
 
      real                    FixSST,VarSST   ! SST forcing switch
      real                           SSTnud   ! SST Nudging Rate
      common  /SISVAT_MAR_ocn/FixSST,VarSST   !
     .                              ,SSTnud   !
 
      real     ifra_t                         !
cXF
      real     SrfSIC,SIc0OK                  ! Oceanic Fraction: previous
      real     FraOcn,SIceOK                  ! Oceanic Fraction
      real     TocnSI                         ! Ocn Temp.=> S-Ice Covered
      real     OcnMin                         ! Oceanic Fraction: Minimum
      real     dzSIce(4)                      ! Sea-Ice Layers    Thickness
      real     SIcMIN                         ! Sea-Ice Layer Min Thickness
      real     SIc_OK(2)                      ! Sea-Ice Switch
      real     c1_zuo,c2_zuo,c3_zuo           ! Run Off Parameters
      real     SnowWE                         ! Snow Water Equivalent[m w.e.]
      real     rosNEW                         ! Added Snow Density    [kg/m3]
      real     S_Eros,SnEros                  ! Snow Erosion (status) = (1,0)
 
c #BW integer  noUNIT
c #BW real     BlowST,SnowSB
 
c #WR integer  ifrVER(mx,my)                  ! Verification Variable:
                                              ! Total Fraction must be 100%
 
      real     tairDY_2D(mx,my),qvDY_2D(mx,my)
 
C +--DATA
C +  ====
 
      data     StandA       /  .true. /
      data     glfFIX       /  .false./
      data     cha/'-'/
      data     chb/':'/
 
      data     czemin       /   1.e-3 /
 
      data     TocnSI       / 270.70  /       ! Ocn Temp.=> S-Ice Covered
      data     OcnMin       /   0.05  /       ! Oceanic Fraction: Minimum
      data     dzSIce /0.5,0.05,0.001,0.0/    ! Sea-Ice Layers    Thickness
      data     SIcMIN /0.1/                   ! Sea-Ice Layer Min Thickness
      data     SIc_OK /1.0,0.00/              ! Sea-Ice Switch
C +
      data    c1_zuo/12.960e+4/,c2_zuo/2.160e+6/,c3_zuo/1.400e+2/ ! Zuoriginal
C #SN data    c1_zuo/ 2.796e+4/,c2_zuo/2.160e+6/,c3_zuo/1.400e+2/ !     Tuning
C +...        Run Off Parameters
C +           86400*1.5 day     ...*25 days (Modif. ETH Camp: 86400*0.3day)
C +           (Zuo and Oerlemans 1996, J.Glacio. 42, 305--317)
 
 
C +--SISVAT Time             Variable
C +  ================================
 
          dt__SV = dtPhys
 
 
C +   ++++++++++++++++  INITIALISATION: BEGIN +++
      IF (.not.INI_SV)                                            THEN
C +   ++++++++++++++++
 
 
C +--OUTPUT point (i,j,n) coordinates
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          iwr_SV = 1
          jwr_SV = 1
          nwr_SV = 1
 
C +--Level of negligible blown Snow Particles Concentration ( ~ 25 magl)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
             kB    =mz
 11      CONTINUE
c #AE    IF (zsigma(kB    ).GT. 25..OR.kB    .LE.1)             GO TO 10
c #AE        kB    =kB    -1
c #AE                                                           GO TO 11
 10      CONTINUE
c #AE        write(6,1000)             kB
 1000        format(/,' BS : Level of negligible '
     .                     ,'blown Snow Particles Concentration is',i4
     .                     ,' (i.e., ~  25. magl)',/)
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! #wx  iSV_v1= imez     ! Snow Erosion Statistics: Grid Point Coordinate
! #wx  jSV_v1= jmez     ! Id.
! #wx  nSV_v1= 1        ! Id.
! #wx  lSV_v1= 1        ! Snow Erosion Statistics: OUTPUT SWITCH (IF   >   0)
                        !                          .LE. 1 Blowing Snow
                        !                          .LE. 2 Blowing Snow (FULL)
                        !                          .EQ. 3 Snow    Agregation
 
 
C +--SISVAT Time Independant Variables
C +  =================================
 
                        StandA=.false.
        IF (VSISVAT.AND.StandA)                                   THEN
          write(6,600)
 600      format(/,'### MAR_SISVAT CRASH,',
     .                      ' STAND ALONE LABEL #sa is ON ###',
     .           /,'    ?!&~@|@[#@#]=!!!',15x,'EMERGENCY STOP')
          stop
        END IF
 
        IF (mw.ne.nvx)                                            THEN
          write(6,601) mw,nvx
 601      format(/,'### MAR_SISVAT CRASH, mw =',i6,
     .                            ' .NE. nvx =',i6,' ###',
     .           /,'    ?!&~@|@[#@#]=!!!',15x,'EMERGENCY STOP')
          stop
        END IF
 
        IF (mw.ne.nsx)                                            THEN
          write(6,602) mw,nsx
 602      format(/,'### MAR_SISVAT CRASH, mw =',i6,
     .                            ' .NE. nvx =',i6,' ###',
     .           /,'    ?!&~@|@[#@#]=!!!',15x,'EMERGENCY STOP')
          stop
        END IF
 
        IF (nsol+1.ne.llx)                                        THEN
          write(6,603)  nsol+1,llx
 603      format(/,'### MAR_SISVAT CRASH, ns =',i6,
     .                            ' .NE. nvx =',i6,' ###',
     .           /,'    ?!&~@|@[#@#]=!!!',15x,'EMERGENCY STOP')
          stop
        END IF
 
        IF (nb_wri.gt.mz      )                                   THEN
          write(6,604) nb_wri,mz
 604      format(/,'### MAR_SISVAT CRASH, nb_wri =',i6,
     .                            ' .GT. mz      =',i3,' ',2x,' ###',
     .           /,'    ?!&~@|@[#@#]=!!!',23x,'EMERGENCY STOP')
c         stop ! ERRRO if mw =5
        END IF
 
        IF (nb_wri.gt.mw *iptx)                                   THEN
          write(6,605) nb_wri,mw ,iptx
 605      format(/,'### MAR_SISVAT CRASH, nb_wri =',i6,
     .                            ' .GT. mw *iptx=',i3,'*',i2,' ###',
     .           /,'    ?!&~@|@[#@#]=!!!',23x,'EMERGENCY STOP')
          stop
        END IF
 
        IF (nb_wri.gt.nsx*iptx)                                   THEN
          write(6,606) nb_wri,nsx,iptx
 606      format(/,'### MAR_SISVAT CRASH, nb_wri =',i6,
     .                            ' .GT. nsx*iptx=',i3,'*',i2,' ###',
     .           /,'    ?!&~@|@[#@#]=!!!',23x,'EMERGENCY STOP')
          stop
        END IF
 
!c #SN   IF (mz    .LT.nsno    )                                   THEN
!c #SN     write(6,607) mzz   ,nsno
 607      format(/,'### MAR_SISVAT CRASH, mz     =',i6,
     .                            ' .GT. nsno    =',i3,' ',2x,' ###',
     .           /,'    ?!&~@|@[#@#]=!!!',23x,'EMERGENCY STOP')
!c #SN     stop
!c #SN   END IF
 
c #BS   IF (klonv.ne.256)                                          THEN
c #BS     write(6,608) klonv
 608      format(/,'#BS MAR_SISVAT CRASH, klonv =',i6,'.ne.256 ###',
     .           /,'    ?!&~@|@[#@#]=!!!',15x,'EMERGENCY STOP')
c #BS     stop
c #BS   END IF
 
 
C +     ****************
        call  SISVAT_ini
C +     ****************
 
 
C +--Grids Correspondance
C +  --------------------
 
        DO isl = -nsol,0
          deptTV(1-isl)= dz_dSV(isl)
        END DO
 
            ntime  = 0
 
            ijnmax = mx2* my2* nvx
        IF      (mod(ijnmax ,klonv).eq.0)                         THEN
            nvcmax = ijnmax /klonv
        ELSE
            nvcmax = ijnmax /klonv + 1
        END IF
C +
C +
C +--Surface Fall Line Slope
C +  -----------------------
C +
        IF (SnoMod)                                               THEN
          IF (mx.eq.1.AND.my.eq.1)                                THEN
            SWfSNo(1,1)=                             ! Normalized Decay of the
     .       exp(-dt__SV                             ! Surficial Water Content
     .          /(c1_zuo                             !(Zuo and Oerlemans 1996,
     .           +c2_zuo*exp(-c3_zuo*slopTV(1,1))))  ! J.Glacio. 42, 305--317)
          ELSE
            DO j=jp11,my1
            DO i=ip11,mx1
            slopx      =(sh(ip1(i),j)-sh(im1(i),j))*dxinv2
            slopy      =(sh(i,jp1(j))-sh(i,jm1(j)))*dyinv2
            slopTV(i,j)= sqrt(slopx *slopx + slopy *slopy)
            SWfSNo(i,j)=                             ! Normalized Decay of the
     .       exp(-dt__SV                             ! Surficial Water Content
     .          /(c1_zuo                             !(Zuo and Oerlemans 1996,
     .           +c2_zuo*exp(-c3_zuo*slopTV(i,j))))  ! J.Glacio. 42, 305--317)
            slopGE(i,j)=    cos(atan(slopTV(i,j)))
            END DO
            END DO
          END IF
        END IF
 
 
C +--Initialization of Surface Types
C +  ===============================
 
        IF (itexpe.EQ.0)                                          THEN
          DO j=jp11,my1
          DO i=ip11,mx1
            IF (maskSL(i,j).eq. 1)                                THEN
                nSLsrf(i,j)   = 1                           ! Ocean Grid Pt
                SLsrfl(i,j,1) = 1.
              IF(mw.gt.1)                                         THEN
              DO n=min(2,mw),nvx
                SLsrfl(i,j,n) = 0.
              END DO
              END IF
            ELSE
                nSLsrf(i,j)   = nvx                         ! Land  Grid Pt
              DO n=1,nvx
                SLsrfl(i,j,n) = ifraTV(i,j,n)
                SLsrfl(i,j,n) = SLsrfl(i,j,n) * 0.01
              END DO
            END IF
 
 
C +--Initialization of z0(Sastrugi)
C +  ==============================
 
! Influence of the Angle(Wind,Sastrugi) (Andreas, 1995, CCREL report 95-16)
! -------------------------------------------------------------------------
 
                ua_0BS(i,j)=uairDY(i,j,mz)
                va_0BS(i,j)=vairDY(i,j,mz)
 
 
!  Sastrugi Height
!  ---------------
 
          DO n=1,mw
c #SZ           Z0SaBS(i,j,n)    = 0.
            DO nt=1,ntavSL
                SLn_z0(i,j,n,nt) = 0.5e-6
                SLn_b0(i,j,n,nt) = 0.5e-6
                SLn_r0(i,j,n,nt) = 0.5e-6
            END DO
          END DO
          END DO
          END DO
 
        END IF
 
 
! Influence of the Angle(Wind,Sastrugi) (Andreas, 1995, CCREL report 95-16)
! -------------------------------------------------------------------------
 
                FracBS=exp(-dt__SV/43200.)
 
 
C +--Initialization of z0(Orography Roughness)
C +  =========================================
 
c #OR    DO k=1,mw
c #OR    DO j=1,my
c #OR    DO i=1,mx
c #OR       SL_z0 (i,j,k) = min(SL_z0 (i,j,k),zsigma(mz)/3.)
c #OR       SLzoro(i,j,k) = min(SLzoro(i,j,k),zsigma(mz)/3.)
c #OR    END DO
c #OR    END DO
c #OR    END DO
 
 
C +--Ocean Status
C +  ============
 
              VarSST        =   0.
c #OP         VarSST        =   1.
              FixSST        =   1.-VarSST
              SSTnud        = exp(-dt__SV/2.592e6)  ! SST    Nudging:
C +...                                              ! e-folding time: 30 Days
C +
        IF (itexpe.EQ.0)                                          THEN
          DO j=jp11,my1
          DO i=ip11,mx1
            IF (maskSL(i,j).gt.0.AND.ifraTV(i,j,1).lt.100)        THEN
                write(6,6000)i,j,(ifraTV(i,j,n),n=1,nvx)
 6000           format(' WARNING on Grid Point',2i4,' Mosaic = (',3i4,
     .                 '): ISLANDS must(will) be excluded')
              DO n=1,nvx
                ifraTV(i,j,n) =   0
                ivegTV(i,j,n) =   0
              END DO
                ifraTV(i,j,1) = 100
            END IF
          END DO
          END DO
 
 
C +--Prescription from SST
C +  ---------------------
 
                                          Tfr_LB = TocnSI
c #RE                                     Tfr_LB = 271.35 + epsi
          DO j=jp11,my1
          DO i=ip11,mx1
             FraOcn          =    (TsolTV(i,j,1,1)-Tfr_LB)/TSIdSV! Open Ocean
             FraOcn          =  1.-sicsIB(i,j)                   ! Prescribed
             FraOcn          = min(  unun,FraOcn)                !      Fract.
             FraOcn          = max(OcnMin,FraOcn)                !
             SLsrfl(i,j,1)   =  (1-maskSL(i,j))  * SLsrfl(i,j,1) ! New  Ocean
     .                          +  maskSL(i,j)   * FraOcn        !
             SrfSIC          =                     SLsrfl(i,j,2) ! Old  Sea Ice
             SIc0OK          = max(zero, sign(unun,SrfSIC-epsi)) !
             SLsrfl(i,j,2)   =  (1-maskSL(i,j))  * SLsrfl(i,j,2) ! New  Sea Ice
     .                          +  maskSL(i,j)*(1.-FraOcn)       !
             SIceOK          = max(zero, sign(unun,SLsrfl(i,j,2) !
     .                                                   -epsi)) !
             ifra_t          =     ifraTV(i,j,1) + ifraTV(i,j,2) ! OCN  Fract.
             ifraTV(i,j,1)   =     SLsrfl(i,j,1) * 100.          !
             ifraTV(i,j,1)   = min(ifraTV(i,j,1) , ifra_t)       !
             ifraTV(i,j,2)   =     ifra_t        - ifraTV(i,j,1) !
 
C +--Sea-Ice Vertical Discretization
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
             nssSNo(i,j,2)   =
     .       nssSNo(i,j,2)                    *(1-maskSL(i,j))
     .     +(nssSNo(i,j,2)   *    SIc0OK
     .      +     3          *(1.-SIc0OK)*SIceOK)*maskSL(i,j)
 
             nisSNo(i,j,2)   =
     .       nisSNo(i,j,2)                    *(1-maskSL(i,j))
     .     +(nisSNo(i,j,2)   *    SIc0OK
     .      +     3          *(1.-SIc0OK)*SIceOK)*maskSL(i,j)
             issSNo(i,j,2)   =                    nisSNo(i,j,2)
 
            DO l=1,nsno
             dzsSNo(i,j,2,l) =
     .       dzsSNo(i,j,2,l)                  *(1-maskSL(i,j))
     .     +(dzsSNo(i,j,2,l) *    SIc0OK
     .      +dzSIce(min(4,l))*(1.-SIc0OK)*SIceOK)*maskSL(i,j)
 
             tisSNo(i,j,2,l) =
     .       tisSNo(i,j,2,l)                  *(1-maskSL(i,j))
     .     +(tisSNo(i,j,2,l) *    SIc0OK
     .      +TsolTV(i,j,1,1) *(1.-SIc0OK)       )*maskSL(i,j)
 
             rosSNo(i,j,2,l) =
     .       rosSNo(i,j,2,l)                  *(1-maskSL(i,j))
     .     +(rosSNo(i,j,2,l) *    SIc0OK
     .      +ro_Ice          *(1.-SIc0OK)*SIceOK)*maskSL(i,j)
 
             g1sSNo(i,j,2,l) =
     .       g1sSNo(i,j,2,l)                  *(1-maskSL(i,j))
     .     +(g1sSNo(i,j,2,l) *    SIc0OK
     .      +G1_dSV          *(1.-SIc0OK)*SIceOK)*maskSL(i,j)
 
             g2sSNo(i,j,2,l) =
     .       g2sSNo(i,j,2,l)                  *(1-maskSL(i,j))
     .     +(g2sSNo(i,j,2,l) *    SIc0OK
     .      +30.             *(1.-SIc0OK)*SIceOK)*maskSL(i,j)
 
             nhsSNo(i,j,2,l) =
     .       nhsSNo(i,j,2,l)                  *(1-maskSL(i,j))
C #SI.new  +(nhsSNo(i,j,2,l) *    SIc0OK
C #SI.new   +istdSV(2)       *(1.-SIc0OK)*SIceOK)*maskSL(i,j)
     .     + istdSV(2)       *                    maskSL(i,j)
            END DO
            DO l=1,llx
             TsolTV(i,j,2,l) =
     .       TsolTV(i,j,2,l)                  *(1-maskSL(i,j))
     .     +(TsolTV(i,j,2,l) *    SIc0OK
     .      +TsolTV(i,j,1,l) *(1.-SIc0OK)       )*maskSL(i,j)
 
             eta_TV(i,j,2,l) =
     .       eta_TV(i,j,2,l)                  *(1-maskSL(i,j))
     .     + eta_TV(i,j,2,l) *    SIc0OK  *       maskSL(i,j)
C +...                            No Pore in Ice => No Water
            END DO
 
c #WI        write(6,6001) jdarGE,labmGE(mmarGE),iyrrGE
c #WI.                    ,jhurGE,minuGE,jsecGE ,TsolTV(i,j,1,1)
c #WI.                    ,FraOcn,ifraTV(i,j,1) ,TsolTV(i,j,2,1)
c #WI.                    ,       nisSNo(i,j,2) ,nssSNo(i,j,2)
 6001        format(/,98('_'),
     .              /,i3,'-',a3,'-',i4,3(':',i2),
     .                2x,'T OCN = ',f7.3,4x,'% OCN = ',f7.3,'(',i3,')',
     .                2x,'T ICE = ',f7.3,
     .             /,42x,'NbIce = ',i3, 11x,'NbSno = ',i3)
 
          END DO
          END DO
        END IF
 
 
C +--Soil Upward IR Flux
C +  ===================
 
        IF (itexpe.EQ.0)                                          THEN
          DO j=jp11,my1
          DO i=ip11,mx1
            IR_aux        =
     .     -eps0SL(i,j)* stefan*TairSL(i,j)*TairSL(i,j)    ! Upward IR Flux
     .                         *TairSL(i,j)*TairSL(i,j)    !
          DO n=   1,nvx
            IRsoil(i,j,n) =     IR_aux
          END DO
 
 
C +--Water Vapor Flux Limitor
C +  ========================
 
c #VX     DO n=   1,nLimit
c #VX       WV__SL(i,j,n) =     1.
c #VX     END DO
 
          END DO
          END DO
 
 
C +--SBL  Characteristics
C +  ====================
 
          DO nt=  1,ntaver
          DO j=   1,my
          DO i=   1,mx
            V_0aSL(i,j,  nt) =  ssvSL(i,j,mz)
          DO n=   1,nvx
            dT0aSL(i,j,n,nt) = tairDY(i,j,mz)-tsrfSL(i,j,n)
          END DO
          END DO
          END DO
          END DO
 
        END IF
 
 
C +--OUTPUT Files Definition
C +  =======================
 
! #v0   open(unit=50,status='unknown',file='PHY_SISVAT.v0')
! #v0   rewind    50
 
        IF (mmy.LE.1.AND.mw.GT.mw0)                               THEN
            open(unit=51,status='unknown',file='Dsagrr.OUT')
            rewind    51
            write(51,5100)
 5100       format(/,' Simple Disagregation Model',
     .             /,' ==========================')
        END IF
 
           iwr = 0
        DO ipt = 1,iptx
           IF (IOi_TV(ipt).EQ.0) IOi_TV(ipt)=imez
           IF (IOj_TV(ipt).EQ.0) IOi_TV(ipt)=jmez
        DO n   = 1,nvx
           iwr = 1+iwr
          IF (iwr.LE.nb_wri)                                      THEN
           no__SV(iwr) = 0
           i___SV(iwr) = IOi_TV(ipt)
           j___SV(iwr) = IOj_TV(ipt)
           n___SV(iwr) = n
          END IF
        END DO
        END DO
 
 
C +--Initialization of V10 Interpolation
C +  ===================================
 
        if   (zsigma(1).GT.10.          )                           THEN
          k = 0
  301     CONTINUE
          k = k + 1
            if (zsigma(k).LT.10.OR.k.gt.mz)                    GO TO 300
                                                               GO TO 301
  300     CONTINUE
          kSBL = k
 
          IF (kSBL.EQ.mz)                                           THEN
              rSBL10 = log(10.           / 0.002)    ! 0.002: typical Z0
     .                /log(zsigma(kSBL)  / 0.002)    !
 
          ELSE
              rSBL10 =    (10.           - zsigma(kSBL))
     .                /   (zsigma(kSBL-1)- zsigma(kSBL))
          END IF
        ELSE
              kSBL   =     mz
              rSBL10 =      1.
        END IF
 
 
C +   ++++++
      END IF
C +   ++++++       +++  INITIALISATION:  END  +++
 
 
C +--Preparation    of V10 Interpolation
C +  ===================================
 
        IF (kSBL.EQ.mz)                                             THEN
          DO j=1,my
          DO i=1,mx
            VV__10(i,j) = rSBL10 *  ssvSL(i,j,kSBL)
          ENDDO
          ENDDO
        ELSE
          DO j=1,my
          DO i=1,mx
            VV__10(i,j) =           ssvSL(i,j,kSBL)
     .                  + rSBL10 * (ssvSL(i,j,kSBL-1)-ssvSL(i,j,kSBL))
          ENDDO
          ENDDO
        END IF
 
 
C +--Preparation    of OUTPUT
C +  ========================
 
        DO n=   1,nvx
        DO j=jp11,my1
        DO i=ip11,mx1
          WKxyz1(i,j,n) = 0.
        END DO
        END DO
        END DO
        DO iwr = 1,nb_wri
          WKxyz1(i___SV(iwr),j___SV(iwr),n___SV(iwr)) = iwr
        END DO
 
 
C +--Update Sea-Ice    Fraction
C +  ==========================
 
      IF (             reaLBC                    )                THEN
 
C +          ******
        call INIsic(ihamr_SIS,nhamr_SIS,newsicSI )
C +          ******
 
      END IF
 
 
C +--Update Green Leaf Fraction
C +  ==========================
 
                                           glfFIX = .true.
      IF (vegmod .AND. reaLBC .AND.  .NOT. glfFIX)                THEN
C +
C +          ******
        call INIglf(ihamr_SIS,nhamr_SIS,newglfSIS)
C +          ******
C +
      END IF
 
 
C +--SISVAT Time   Dependant Variables
C +  =================================
 
C +--Water Vapor Flux Limitor
C +  ------------------------
 
c #VX DO n=1,nLimit-1
c #VX DO j=jp11,my1
c #VX DO i=ip11,mx1
c #VX   WV__SL(i,j,n) = WV__SL(i,j,n+1)
c #VX END DO
c #VX END DO
c #VX END DO
 
c #VX DO j=jp11,my1
c #VX DO i=ip11,mx1
c #VX   uqstar    = max(abs(SLuqs(i,j)),epsi)*sign(1.,SLuqs(i,j))
c #VX   WV__SL(i,j,n)
c #VX. = TUkvh(i,j,mmz1) *(  qvDY(i,j,km2(mz))-  qvDY(i,j,mmz1))
c #VX. /(uqstar          *(zsigma(    km2(mz))-zsigma(    mmz1)))
c #VX END DO
c #VX END DO
 
c #VX DO j=jp11,my1
c #VX DO i=ip11,mx1
c #VX     WVaLim(i,j) = 0.
c #VX   DO n=1,nLimit
c #VX     WVaLim(i,j) = WVaLim(i,j)+WV__SL(i,j,n)
c #VX   ENDDO
c #VX     WVaLim(i,j) = WVaLim(i,j)/nLimit
c #VX END DO
c #VX END DO
 
 
C +--Grid Averages
C +  -------------
 
      DO j=jp11,my1
      DO i=ip11,mx1
c #WR   ifrVER(i,j)   = 0
CCCCC   RAdsol(i,j)   = sol_SL(i,j)/(1.d0-albeSL(i,j))     ! => downward Solar
        albeSL(i,j)   = 0.
        eps0SL(i,j)   = 0.
        Upw_IR(i,j)   = 0.
         SLlmo(i,j)   = 0.
         SLuus(i,j)   = 0.
         SLuts(i,j)   = 0.
         SLuqs(i,j)   = 0.
        uss_HY(i,j)   = 0.
        qsrfHY(i,j)   = 0.
        TairSL(i,j)   = 0.
        draiTV(i,j)   = 0.
c #TC     uqTC(i,j,1) = 0.
c #TC     qsTC(i,j,1) = 0.
 
 
C +--Sastrugi Height decreased by Precipitation if V < 6 m/s (Kotlyakov, 1961)
C + --------------------------------------------------------------------------
 
c #SZ   dsastr(i,j) =       max(0.00,(snowHY(i,j)-sno0HY(i,j))
c #SZ. /max(0.05,0.104*sqrt(max(0.00, VV__10(i,j)-6.00       ))))
 
 
! Influence of the Angle(Wind,Sastrugi) (Andreas, 1995, CCREL report 95-16)
! -------------------------------------------------------------------------
 
        S_Eros     = max(zero,sign(unun,-uss_HY(i,j)-eps9))
        SnEros     = max(zero,sign(unun, uss_HY(i,j)+eps9))
        VVs_BS(i,j) =
     .           SnEros* VVs_BS(i,j)
     .         + S_Eros*(VVs_BS(i,j)   * FracBS +
     .                                   VV__10(i,j))
        RRs_BS(i,j) =
     .           SnEros* RRs_BS(i,j)
     .         + S_Eros*(RRs_BS(i,j)   * FracBS + 1.0             )
        DDs_BS(i,j) =
     .           SnEros* DDs_BS(i,j)
     .         + S_Eros* DDs_BS(i,j)   * FracBS
     .         +       ((vairDY(i,j,mz)*(uairDY(i,j,mz) -ua_0BS(i,j))
     .                  -uairDY(i,j,mz)*(vairDY(i,j,mz) -va_0BS(i,j))))
     .   /(degrad*max(0.3,ssvSL(i,j,mz)*  ssvSL(i,j,mz)))
      END DO
      END DO
      DO j=jp11,my1
      DO i=ip11,mx1
         IF (DDs_BS(i,j).GT.360.) DDs_BS(i,j) = DDs_BS(i,j) - 360.
         IF (DDs_BS(i,j).LT.  0.) DDs_BS(i,j) = DDs_BS(i,j) + 360.
      END DO
      END DO
 
 
C +--Simple "Mosaic" Rain Disagregation Model
C +  ----------------------------------------
 
      IF (mmy.LE.1.AND.mw.gt.mw0)                                 THEN !
 
C +--White Noise Generator
C +  ~~~~~~~~~~~~~~~~~~~~~
        IF (jhurGE.EQ.6.AND.minuGE.EQ.0.AND.jsecGE.EQ.0)          THEN
          rtime=      tairDY(imez,jmez,mz)*1.e3
          ntime= int(rtime)
          rtime=    (rtime -ntime)        *1.e3
          ntime=     rtime
          ntime= mod(ntime ,mw)+1
        END IF
 
C +--Averaged Soil Humidity
C +  ~~~~~~~~~~~~~~~~~~~~~~
        DO j=1,my
        DO i=1,mx
          WKxy1( i,j) = 0.                            ! Averaged Soil Humidity
        DO n=1,mw                                     !
        DO k=-nsol,0                                  !
          WKxy1( i,j) = WKxy1( i,j)                   !
     .                +eta_TV( i,j,n,1-k)*dz_dSV(k)   !
        END DO                                        !
        END DO                                        !
          WKxy1( i,j) = WKxy1( i,j) / (mw*zz_dSV)     !
 
C +--Rain Distribution !  Rain Persistance over "wetter" surfaces
C +  ~~~~~~~~~~~~~~~~~ ! (Taylor et al., 1997, MWR 125, pp.2211-2227)
          WKxy2( i,j)   = 0.                          ! Normalization Factor
        DO n=1,mw                                     !
          rr__DR(i,j,n) =  (mod( mw-n+ntime,mw) + 1)  ! Rain Distribution Arg.
     .     /(mw*WKxy1( i,j)     *WKxy1( i,j))         ! dry ===> sparse Rain
     .     *    eta_TV(i,j,n,1) /WKxy1( i,j)          ! Persistance   Impact
          rr__DR(i,j,n) =   min( rr__DR(i,j,n)        !
     .                          ,argmax*0.1   )       !
          rr__DR(i,j,n) =   exp(-rr__DR(i,j,n))       ! Rain Distribution
          k             =      ( n -1   ) / mw0       ! mw0 basic Mosaics
          k             =        k        * mw0 + 1   !
          rr__DR(i,j,n) =        rr__DR(i,j,k)        ! Rain Distribution Arg.
          WKxy2( i,j)   =        WKxy2( i,j)          ! Normalization Factor
     .                  +        rr__DR(i,j,n)        !
        END DO                                        !
 
        END DO
        END DO
 
        DO j=1,my
        DO i=1,mx
        DO n=1,mw
          rr__DR(i,j,n) = rr__DR(i,j,n)  *mw
     .                   /WKxy2( i,j)
        END DO
        END DO
        END DO
 
        IF(mod(jdarGE,3).EQ.0.AND.jhurGE.EQ.6.AND.
     .                            minuGE.EQ.0.AND.
     .                            jsecGE.EQ.0.AND.
     .     mmy          .LE.1                     )                 THEN
          DO i=1,mx
            IF      (isolSL(i,1).gt.2)                              THEN
              write(51,5101) jdarGE, labmGE(mmarGE) ,iyrrGE,jhurGE,
     .                       i     ,(eta_TV(i,1,n,1),n=1,mw)
 5101         format(i3,'-',a3,'-',i4,':',i2,i6,15f6.3,/,(21x,15f6.3))
              write(51,5102)
     .        ntime,  isolSL(i,1)  ,(rr__DR(i,1,n)  ,n=1,mw)
 5102         format(i12,3x                 ,i6,15f6.2,/,(21x,15f6.2))
            END IF
          END DO
              write(51,5103)
 5103         format(111('-'))
        END IF
      ELSE
        DO j=1,my
        DO i=1,mx
        DO n=1,mw
          rr__DR(i,j,n) = 1.
        END DO
        END DO
        END DO
      END IF
 
c     - Interpolation of temp. and spec. hum. on sub_grid - *CL*
c     --------------------------------------------------------------
 
      do i=1,mx; do j=1,my
       do n=1,nsx
        tairDY_int(i,j,n) = tairDY(i,j,mz)
          qvDY_int(i,j,n) =   qvDY(i,j,mz)
           tairDY_2D(i,j) = tairDY(i,j,mz)
             qvDY_2D(i,j)   = qvDY(i,j,mz)
       enddo
      enddo; enddo
 
      IF (mw .eq. 5) then
C +       ************************************************************
          call interp_subpix(tairDY_2D,tairDY_int,1,-0.01,0.05
     .                       ,gradTM)
C +       ************************************************************
 
C +       ************************************************************
          call interp_subpix(qvDY_2D,qvDY_int,2,-1.0,1.0
     .                       ,gradQM)
C +       ************************************************************
      ENDIF
 
 
C +--Grid  Point   Dependant Variables
C +  ---------------------------------
 
C +--Verification of Vectorization
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
c #VR         ij2           = 0
c #VR     DO n=   1,mw
c #VR     DO j=jp11,my1
c #VR     DO i=ip11,mx1
c #VR         ij0ver(i,j,n) = 0
c #VR         ij_ver(i,j,n) = 0
c #VR         ijdver(i,j,n) = 0
c #VR     END DO
c #VR     END DO
c #VR     END DO
 
C +--SISVAT Variables Update
C +  ^^^^^^^^^^^^^^^^^^^^^^^
 
cXF
       ijnmax2=0
       do i=ip11,mx1
        do j=jp11,my1
         do n=1,mw
          if(SLsrfl(i,j,n)>0) ijnmax2=ijnmax2+1
         enddo
        enddo
       enddo
 
       IF (mod(ijnmax2 ,klonv).eq.0) THEN
             nvcmax2 = ijnmax2 /klonv
       ELSE
             nvcmax2 = ijnmax2 /klonv + 1
       END IF
 
       IF(mx==1) nvcmax2=1
cXF
              ijn         = 0
              nnn         = 0
      DO      nvc         = 1,nvcmax2
              nnn         = nnn+klonv
 
          DO  ikl         = 1,klonv
2000          continue
              ijn         =           ijn      + 1
                n         = nvx - mod(ijn,nvx)
              ij          = 1   +    (ijn-  1)/nvx
               j          = my1 - mod(ij ,my2)
              i           = ip11+    (ij -  1)/my2
              i           =       min(mx1,  i)
              if (SLsrfl(i,j,n)==0) goto 2000
              k2i(ikl)    = i
              k2j(ikl)    = j
              k2n(ikl)    = n
           ENDDO
 
           DO  ikl = 1,klonv
                      i   = k2i(ikl)
                      j   = k2j(ikl)
                      n   = k2n(ikl)
              ii__SV(ikl) = i                              ! Work pt. i Coord.
              jj__SV(ikl) = j                              ! Work pt. j Coord.
              nn__SV(ikl) = n                              ! Work pt. n Coord.
 
! #wz         IF (ikl.EQ.1.AND.jsecGE.EQ.0)
! #wz.        write(6,6659)
 6659         format(20x,'   dsn_SV   us__SV   Z0SaSi   Z0Sa_N'
     .                  ,'   Z0SaSV   Z0m_Sn   Z0m_SV')
 
C +--Atmospheric Forcing                                    (INPUT)
C +  ^^^^^^^^^^^^^^^^^^^                                     ^^^^^
              zSBLSV      =    z__SBL                      ! [m]
              za__SV(ikl) =   (gplvDY(i,j,mz)              !
     .                        -gplvDY(i,j,mzz)) *grvinv    ! [m]
              VV__SV(ikl) =     ssvSL(i,j,mz)              !
              VV10SV(ikl) =    VV__10(i,j)                 !
              VVs_SV(ikl) =    VVs_BS(i,j)/RRs_BS(i,j)     !
              DDs_SV(ikl) =       max(zero,DDs_BS(i,j)-180.)
     .        +180.*min(unun,zero-min(zero,DDs_BS(i,j)-180.))
     .                           +min(zero,DDs_BS(i,j)-180.)
              Ua_min      =  epsi                          !
c #VM         Ua_min      =  0.2 * sqrt(za__SV(ikl)   )    !
              VV__SV(ikl) =  max(Ua_min, ssvSL(i,j,mz))    !
              TaT_SV(ikl) =tairDY_int(i,j,n)               !
              ExnrSV(ikl) =      pkDY(i,j,mz)              !
              rhT_SV(ikl) =   (pstDYn(i,j)   +ptopDY)*1.e3 ! [kg/m3] *CL*
     .                       /(tairDY_int(i,j,n)*RDryAi)   !
              QaT_SV(ikl) =  qvDY_int(i,j,n)               !
c #VX         dQa_SV(ikl) =  max(0.,1.-WVaLim(i,j))        ! Water  Vapor
c #VX.                        * dtDiff/zsigma(mz)          ! Flux Limitor
              qsnoSV(ikl) =      0.                        !
     .                 +min(demi,qsHY(i,j,mz)              !
c #TC.                       +   qxTC(i,j,mz,1)            !
     .                                         )           !
 
C +--Energy Fluxes                                          (INPUT)
C +  ^^^^^^^^^^^^^                                           ^^^^^
              coszSV(ikl) = max(czemin,czenGE(i,j))        ! cos(zenith.Dist.)
              sol_SV(ikl) =            RAdsol(i,j)         !    downward Solar
              IRd_SV(ikl) =            RAd_ir(i,j)         !    downward IR
 
C +--Water  Fluxes                                          (INPUT)
C +  ^^^^^^^^^^^^^                                           ^^^^^
              drr_SV(ikl) =(rainHY(i,j)-rai0HY(i,j))*1.e3  ! [m/s] -> [mm/s]
     .                    * rr__DR(i,j,n)           /dtPhys!          [kg/m2/s]
              d_snow      = snowHY(i,j)-sfa0HY(i,j)        ! Only SnowFall
              dsn_SV(ikl) = d_snow                  *1.e3  ! Erosion NOT incl.
     .                                              /dtPhys!
              SnowOK      =                                ! Correction
     .     max(zero,sign(unun,         qsHY(i,j,mz)-epsi)) !
     .    *max(zero,sign(unun,TfSnow-tairDY_int(i,j,n)-epsi)) !
              dsn_SV(ikl) = dsn_SV(ikl)+drr_SV(ikl)*SnowOK !
              drr_SV(ikl) = drr_SV(ikl)        *(1.-SnowOK)!
c #BS         dbsnow      =-SLussl(i,j,n)                  ! Erosion
c #BS.                     *dtPhys     *rhT_SV(ikl)        !
! #BS         dsnbSV(ikl) =min(max(zero,dbsnow)            !
! #BS.                    /    max(epsi,d_snow),unun)      !
c #BS         dsnbSV(ikl) =1.0-min(qsHY(i,j,kB)            ! kB level ~ 25 magl
c #BS.                        /max(qsHY(i,j,mz),eps9),unun)!(BS negligib.at kB)
!             dsnbSV is used and modified in SISVAT_BSn,
!                  then used for Buffer Layer Update
c #BS         dbs_SV(ikl) = blowSN(i,j,n)                  !          [kg/m2]
c #BS         dbs_Ac(ikl) = 0.
c #BS         dbs_Er(ikl) = 0.
 
C +--Soil/Canopy                                            (INPUT)
C +  ^^^^^^^^^^^                                             ^^^^^
              LSmask(ikl)= 1  - maskSL(i,j)                ! Land/Sea   Mask
              isotSV(ikl)     = isolTV(i,j)                ! Soil       Type
              iWaFSV(ikl)     = iWaFTV(i,j)                ! Soil Drainage
              slopSV(ikl)= atan(slopTV(i,j))               ! Fall Line Slope
              alb0SV(ikl)     = AlbSTV(i,j)                ! Soil Albedo
              ivgtSV(ikl)     = ivegTV(i,j,n)              ! Vegetation Type
              LAI0SV(ikl)     = alaiTV(i,j,n)              ! LAI
              glf0SV(ikl)     = glf_TV(i,j,n)              ! Green Leaf Frac.
 
C +--Energy Fluxes                                          (INPUT/OUTPUT)
C +  ^^^^^^^^^^^^^                                           ^^^^^^^^^^^^
              cld_SV(ikl) =     cld_SL(i,j)                ! Cloudiness
              IRs_SV(ikl) =     IRsoil(i,j,n)              ! Soil upward IR
              LMO_SV(ikl) =     SLlmol(i,j,n)              ! Monin-Obukhov L.
              us__SV(ikl) =     SLuusl(i,j,n)              ! Frict. Velocity
              uts_SV(ikl) =     SLutsl(i,j,n)              ! u*T*
 
C +--Water  Fluxes                                          (INPUT/OUTPUT)
C +  ^^^^^^^^^^^^^                                           ^^^^^^^^^^^^
              uqs_SV(ikl) =     SLuqsl(i,j,n)              ! u*q*
              uss_SV(ikl) =     SLussl(i,j,n)              ! u*s*
c #AE         usthSV(ikl) =     SaltSN(i,j,n)              ! u*_th
 
C +--Soil/Canopy                                            (INPUT/OUTPUT)
C +  ^^^^^^^^^^^                                             ^^^^^^^^^^^^
              Z0m_SV(ikl) =      SL_z0(i,j,n)              ! Moment.Roughn.L.
              Z0h_SV(ikl) =      SL_r0(i,j,n)              ! Heat   Roughn.L.
c #OR         Z0roSV(ikl) =     SLzoro(i,j,n)              ! Orogr. Roughn.L.
              TvegSV(ikl) =     TvegTV(i,j,n)              ! Vegetation Temp.
              snCaSV(ikl) =     CaSnTV(i,j,n)              ! Canopy SnowCover
              rrCaSV(ikl) =     CaWaTV(i,j,n)              ! Canopy RainWater
              psivSV(ikl) =     psivTV(i,j,n)              ! Vegetation Pot.
            DO isl =   -nsol,0
              TsisSV(ikl,isl) = TsolTV(i,j,n,1-isl)        ! Soil Temperature
              eta_SV(ikl,isl) = eta_TV(i,j,n,1-isl)        ! Soil Humidity
            END DO
          ENDDO
 
C +--Snow Roughness                                         (INPUT/OUTPUT)
C +  ^^^^^^^^^^^^^^                                          ^^^^^^^^^^^^
          DO  ikl = 1,klonv
              i   = k2i(ikl)
              j   = k2j(ikl)
              n   = k2n(ikl)
 
C +--Verification of Vectorization
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
c #VR        IF (ijn.LE.ijnmax)                       THEN !
c #VR         ij0ver(i,j,n) = ij0ver(i,j,n) + 1            !
c #VR         ijdver(i,j,n) = ijdver(i,j,n) + ij           !
c #VR        ENDIF                                         !
 
              Z0mmSV(ikl) =     0.                         !
              Z0emSV(ikl) =     0.                         !
              Z0hmSV(ikl) =     0.                         !
            DO nt=   1,ntavSL                              !
              Z0mmSV(ikl) =     Z0mmSV(ikl)                !
     .                    +     SLn_z0(i,j,n,nt)           !
              Z0emSV(ikl) =     Z0emSV(ikl)                !
     .                    +     SLn_b0(i,j,n,nt)           !
              Z0hmSV(ikl) =     Z0hmSV(ikl)                !
     .                    +     SLn_r0(i,j,n,nt)           !
            ENDDO                                          !
              Z0mmSV(ikl) = min(Z0mmSV(ikl)  /ntavSL       !  z0(Mom., Box Av.)
     .                         ,zsigma(mz)   /    3.)      !
              Z0emSV(ikl) =     Z0emSV(ikl)  /ntavSL       !  z0(Eros, Box Av.)
              Z0hmSV(ikl) =     Z0hmSV(ikl)  /ntavSL       !  z0(Heat, Box Av.)
 
c #SZ         Z0SaSV(ikl) =     Z0SaBS(i,j,n)              !  z0(Sastrugi  h)
c #SZ         dz0_SV(ikl) = .01*dsastr(i,j)  *max(2-n,0)   ! dz0(Sastrugi dh)
 
C +--V,  dT(a-s)    Time Moving Averages
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            DO nt=1,ntaver
              V__mem(ikl,nt)     = V_0aSL(i,j,  nt)
              T__mem(ikl,nt)     = dT0aSL(i,j,n,nt)
            ENDDO
 
            DO nt=   1,ntaver-1
              V__mem(ikl,nt    ) = V__mem(ikl,nt+1)
              T__mem(ikl,nt    ) = T__mem(ikl,nt+1)
            ENDDO
              V__mem(ikl,ntaver) = VV__SV(ikl)
              T__mem(ikl,ntaver) = TaT_SV(ikl)-tsrfSL(i,j,n)
 
              VVmmem(ikl)        = 0.0
              dTmmem(ikl)        = 0.0
            DO nt=1,ntaver
              VVmmem(ikl)        = VVmmem(ikl)+V__mem(ikl,nt)
              dTmmem(ikl)        = dTmmem(ikl)+T__mem(ikl,nt)
            ENDDO
              VVmmem(ikl)        = VVmmem(ikl)/ntaver
              dTmmem(ikl)        = dTmmem(ikl)/ntaver
 
C +--u*, u*T*, u*s* Time Moving Averages
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #AM       DO nt=1,ntaver
c #AM         u__mem(ikl,nt)     = u_0aSL(i,j,n,nt)
c #AT         uT_mem(ikl,nt)     = uT0aSL(i,j,n,nt)
c #AS         us_mem(ikl,nt)     = us0aSL(i,j,n,nt)
c #AM       ENDDO
          ENDDO
 
c #vB     DO  ikl = 1,klonv
c #BS         Z0emBS(k2i(ikl),k2j(ikl),k2n(ikl)) = Z0emSV(ikl)
c #vB     ENDDO
 
C +--Snow Pack                                              (INPUT/OUTPUT)
C +  ^^^^^^^^^                                               ^^^^^^^^^^^^
          DO  ikl = 1,klonv
              i   = k2i(ikl)
              j   = k2j(ikl)
              n   = k2n(ikl)
              BufsSV(ikl)     = snohSN(i,j,n)              ! Snow Buffer Lay.
              dsn_SV(ikl)     = dsn_SV(ikl)                !
     .                     +max(BufsSV(ikl)-SMndSV,0.)     !
     .                     /    dt__SV                     !
              BufsSV(ikl) = min(BufsSV(ikl),SMndSV   )     !
              BrosSV(ikl)     = BrosSN(i,j,n)              ! Snow Buffer dens.
              BG1sSV(ikl)     = BG1sSN(i,j,n)              ! Snow Buffer D./S.
              BG2sSV(ikl)     = BG2sSN(i,j,n)              ! Snow Buffer S./S.
              isnoSV(ikl)     = nssSNo(i,j,n)              ! Nb Snow/Ice Lay.
              ispiSV(ikl)     = issSNo(i,j,n)              ! Nb Supr.Ice Lay.
              iiceSV(ikl)     = nisSNo(i,j,n)              ! Nb      Ice Lay.
              zWEcSV(ikl)     = zWEcSN(i,j,n)              ! Non-Erod.*Thick.
              rusnSV(ikl)     = SWaSNo(i,j,n)              ! Surficial Water
              SWS_SV(ikl)     = SWSSNo(i,j,n)              ! Surficial Wat.St.
              SWf_SV(ikl)     = SWfSNo(i,j)                ! Normalized Decay
          ENDDO
          DO  isn = 1,nsno
          DO  ikl = 1,klonv
              i   = k2i(ikl)
              j   = k2j(ikl)
              n   = k2n(ikl)
              istoSV(ikl,isn) = nhsSNo(i,j,n,isn)          !            [-]
              dzsnSV(ikl,isn) = dzsSNo(i,j,n,isn)          !            [m]
              ro__SV(ikl,isn) = rosSNo(i,j,n,isn)          !        [kg/m3]
              eta_SV(ikl,isn) = wasSNo(i,j,n,isn)          !        [m3/m3]
              TsisSV(ikl,isn) = tisSNo(i,j,n,isn)          !            [K]
              G1snSV(ikl,isn) = max(-G1_dSV,min(G1_dSV,    !
     .                          g1sSNo(i,j,n,isn)))        ! [-]        [-]
              G2snSV(ikl,isn) = max(-G1_dSV,min(G1_dSV,    !
     .                          g2sSNo(i,j,n,isn)))        ! [-] [0.0001 m]
              agsnSV(ikl,isn) = agsSNo(i,j,n,isn)          !          [day]
          END DO
          END DO
 
! Grid Point                                                (OUTPUT)
! ^^^^^^^^^^                                                 ^^^^^^
! #wx                                                  kSV_v1=0
 
          DO  ikl = 1,klonv
              i   = k2i(ikl)
              j   = k2j(ikl)
              n   = k2n(ikl)
              HFraSV(ikl)     = 0.                         ! Frazil Thickness
 
C +--RunOFF Intensity                                       (INPUT/OUTPUT)
C +  ^^^^^^^^^^^^^^^^                                        ^^^^^^^^^^^^
              RnofSV(ikl)     = 0.                         ! RunOFF Intensity
 
! Grid Point                                                (OUTPUT)
! ^^^^^^^^^^                                                 ^^^^^^
! #wx         IF (i.eq.iSV_v1.AND.j.eq.jSV_v1.AND.n.EQ.nSV_v1)
! #wx.                                                 kSV_v1=ikl
 
              lwriSV(ikl)     = WKxyz1(i,j,n)
 
c #BW      IF(lwriSV(ikl).ne.0.AND.iterun.gt.0)                    THEN
c #BW         noUNIT =   no__SV(lwriSV(ikl))
c #BW         write(noUNIT,5012)
 5012         format(/,1x)
c #BW         write(noUNIT,5013)
 5013         format(' -----+--------+--------+--------+--------+',
     .             '--------+--------+--------+--------+--------+',
     .             '--------+')
c #BW         write(noUNIT,5014)
 5014         format('    n |     z  |     qs |      V |        |',
     .             '     T  | TKE^0.5|        |        |        |',
     .             '        |',
     .             /,'      |    [m] | [g/kg] |  [m/s] |        |',
     .             '    [K] |  [m/s] |        |        |        |',
     .             '        |')
c #BW           BlowST=0.
c #BW           k=0
 5011         CONTINUE
c #BW           k=k+1
c #BW         IF (                  k         .gt. mz )      GO TO 5010
c #BW         IF (grvinv*gplvDY(i,j,k)-sh(i,j).lt.100.)            THEN
c #BW           BlowST=BlowST+ssvSL(i,j,k)*qsHY(i,j,k)
c #BW.                       *pstDY(i,j)*dsigm1(    k)*1.e3*grvinv
c #BW           write(noUNIT,5015) mzz-k,grvinv*gplvDY(i,j,k)-sh(i,j),
c #BW.            1.e3*qsHY(i,j,k),ssvSL(i,j,k),tairDY(i,j,k),
c #BW.                                     sqrt(ect_TE(i,j,k))
 5015           format(i5,' |',f7.2,' |',f7.3,' |',f7.2,' |',
     .                  8x,'|',f7.2,' |',f7.3,' |', 4(8x,'|'))
c #BW         END IF
c #BW         GO TO 5011
 5010         CONTINUE
 
c #BW           SnowSB =   snohSN(i,j,n)
c #BW         IF        (  nssSNo(i,j,n) .GT.     0      )         THEN
c #BW         DO isn=max(0,nssSNo(i,j,n))   ,nssSNo(i,j,n)
c #BW           SnowSB =   SnowSB
c #BW.                +    dzsSNo(i,j,n,isn)*rosSNo(i,j,n,isn)
c #BW         END DO
c #BW         END IF
 
c #BW         write(noUNIT,5016) BlowST,SnowSB
 5016         format(' * TRANSPORT = ',e12.3,' kg/m/s'  ,  8x,'|',
     .               ' * BUDGET    = ',f12.6,' mm w.e.|',2(8x,'|'))
c #BW         write(noUNIT,5013)
c #BW      END IF
 
! OUTPUT, for Stand-Alone VERIFICATION
! ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
! #v0       IF (i  .ge.  1.and.i  .le. mx)                         THEN
! #v0         write(50,5001) iterun,i,j,n,nvc,ikl,
! #v0.                   za__SV(ikl),VV__SV(ikl),TaT_SV(ikl),
! #v0.                   rhT_SV(ikl),QaT_SV(ikl),qsnoSV(ikl),
! #v0.                   coszSV(ikl),sol_SV(ikl),IRd_SV(ikl),
! #v0.                   drr_SV(ikl),dsn_SV(ikl),dbs_SV(ikl),
! #v0.                   LSmask(ikl),isotSV(ikl),alb0SV(ikl),
! #v0.                   IRs_SV(ikl),
! #v0.                   ivgtSV(ikl),LAI0SV(ikl),glf0SV(ikl),
! #v0.                   TvegSV(ikl),LMO_SV(ikl),us__SV(ikl),
! #v0.                   uqs_SV(ikl),uts_SV(ikl),uss_SV(ikl),
! #v0.                   snCaSV(ikl),rrCaSV(ikl),psivSV(ikl)
 5001         format(/,'c #INFO   iterun          = ',i15  ,
     .               /,'c #INFO   i,j,n           = ',3i5  ,
     .               /,'c #INFO   nvc             = ',i15  ,
     .               /,'c #INFO   ikl             = ',i15  ,
     .               /,'          za__SV(ikl)     = ',e15.6,
     .               /,'          VV__SV(ikl)     = ',e15.6,
     .               /,'          TaT_SV(ikl)     = ',e15.6,
     .               /,'          rhT_SV(ikl)     = ',e15.6,
     .               /,'          QaT_SV(ikl)     = ',e15.6,
     .               /,'          qsnoSV(ikl)     = ',e15.6,
     .               /,'          coszSV(ikl)     = ',e15.6,
     .               /,'          sol_SV(ikl)     = ',e15.6,
     .               /,'          IRd_SV(ikl)     = ',e15.6,
     .               /,'          drr_SV(ikl)     = ',e15.6,
     .               /,'          dsn_SV(ikl)     = ',e15.6,
     .               /,'          dbs_SV(ikl)     = ',e15.6,
     .               /,'          LSmask(ikl)     = ',i15  ,
     .               /,'          isotSV(ikl)     = ',i15  ,
     .               /,'          alb0SV(ikl)     = ',e15.6,
     .               /,'          IRs_SV(ikl)     = ',e15.6,
     .               /,'          ivgtSV(ikl)     = ',i15  ,
     .               /,'          LAI0SV(ikl)     = ',e15.6,
     .               /,'          glf0SV(ikl)     = ',e15.6,
     .               /,'          TvegSV(ikl)     = ',e15.6,
     .               /,'          LMO_SV(ikl)     = ',e15.6,
     .               /,'          us__SV(ikl)     = ',e15.6,
     .               /,'          uqs_SV(ikl)     = ',e15.6,
     .               /,'          uts_SV(ikl)     = ',e15.6,
     .               /,'          uss_SV(ikl)     = ',e15.6,
     .               /,'          snCaSV(ikl)     = ',e15.6,
     .               /,'          rrCaSV(ikl)     = ',e15.6,
     .               /,'          psivSV(ikl)     = ',e15.6)
! #v0         DO isl = -nsol,0
! #v0           write(50,5002)    isl,TsisSV(ikl,isl),
! #v0.                            isl,eta_SV(ikl,isl)
 5002           format('          TsisSV(ikl,',i2,')  = ',e15.6,
     .                 '          eta_SV(ikl,',i2,')  = ',e15.6)
! #v0         END DO
! #v0         DO isl =       1,nsno
! #v0           write(50,5003)    isl,TsisSV(ikl,isl),
! #v0.                            isl,dzsnSV(ikl,isl)
 5003           format('          TsisSV(ikl,',i2,')  = ',e15.6,
     .                 '          dzsnSV(ikl,',i2,')  = ',e15.6)
! #v0         END DO
! #v0       END IF
          END DO
 
C +--SISVAT Execution
C +  ^^^^^^^^^^^^^^^^
          write(daHost,'(i2,a3,i4,i3,2(a1,i2))')
     .          jdarGE,labmGE(mmarGE),iyrrGE,
     .          jhurGE,chb,minuGE,chb,jsecGE
 
! #wz     write(6,6660) jdarGE,mmarGE,iyrrGE,jhurGE,minuGE,jsecGE
 6660     format(2(i2,'-'),2i4,2(':',i2),3x,$)
 
C +       ************
          call  SISVAT
C +       ************
 
 
C +--MAR    Variables Update
C +  ^^^^^^^^^^^^^^^^^^^^^^^
        IF   (nvc.lt.nvcmax)                                      THEN
          DO  ikl = 1,klonv
              i   = k2i(ikl)
              j   = k2j(ikl)
              n   = k2n(ikl)
 
C +--Verification of Vectorization
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
c #VR         ij2             = ij2         + 1
c #VR         ijdver(i,j,n)   = ijdver(i,j,n) - ij2
c #VR         ij_ver(i,j,n)   = ij_ver(i,j,n) + 1
 
C +--Energy Fluxes                                          (INPUT/OUTPUT)
C +  ^^^^^^^^^^^^^                                           ^^^^^^^^^^^^
              IRsoil(i,j,n)       = IRs_SV(ikl)            ! Soil upward IR
              SLlmol(i,j,n)       = LMO_SV(ikl)            ! Monin-Obukhov L.
              SLuusl(i,j,n)       = us__SV(ikl)            ! Frict. Velocity
              SLutsl(i,j,n)       = uts_SV(ikl)            ! u*T*
              SLdSdT(i,j,n)       = dSdTSV(ikl)            ! Sens.H.Flux T-Der.
 
C +--Energy Fluxes                                          (OUTPUT/NetCDF)
C +  ^^^^^^^^^^^^^                                           ^^^^^^^^^^^^^
c #NC         SOsoNC(i,j,n)       = SOsoKL(ikl)            ! Absorb.Sol.Rad.
c #NC         IRsoNC(i,j,n)       = IRsoKL(ikl)            ! Absorb.IR  Rad.
c #NC         HSsoNC(i,j,n)       = HSsoKL(ikl)            ! HS
c #NC         HLsoNC(i,j,n)       = HLsoKL(ikl)            ! HL
c #NC         HLs_NC(i,j,n)       = HLs_KL(ikl)            ! Evaporation
c #NC         HLv_NC(i,j,n)       = HLv_KL(ikl)            ! Transpiration
 
C +--Water  Fluxes                                          (INPUT/OUTPUT)
C +  ^^^^^^^^^^^^^                                           ^^^^^^^^^^^^
              SLuqsl(i,j,n)       = uqs_SV(ikl)            ! u*q*
              SLdLdT(i,j,n)       = dLdTSV(ikl)            ! Latn.H.Flux T-Der.
              SLussl(i,j,n)       = 0.                     !
c #BS         SLussl(i,j,n)       =                        ! MAX        Erosion
c #BS.       (blowSN(i,j,n)- dbs_SV(ikl))                  !-unconsumed Erosion
c #BS.                     /(dtPhys*rhT_SV(ikl))           ! ==> actual u*s*
          ENDDO
          DO  ikl = 1,klonv
              i   = k2i(ikl)
              j   = k2j(ikl)
              n   = k2n(ikl)
c #BS         blowSN(i,j,n)= dtPhys*uss_SV(ikl)            ! NEW  MAX   Erosion
c #BS.                             *rhT_SV(ikl)            ! rho u*s* dt[kg/m2]
c #AE         SaltSN(i,j,n)       = usthSV(ikl)            ! u*_th
 
C +--Soil/Canopy                                            (INPUT/OUTPUT)
C +  ^^^^^^^^^^^                                             ^^^^^^^^^^^^
               SL_z0(i,j,n)       = Z0m_SV(ikl)            ! Moment.Roughn.L.
               SL_r0(i,j,n)       = Z0h_SV(ikl)            ! Heat   Roughn.L.
               cdmSL(i,j,n)       = rCDmSV(ikl)            ! sq.root Contr.Drag
               cdhSL(i,j,n)       = rCDhSV(ikl)            ! sq.root Contr.Drag
              TvegTV(i,j,n)       = TvegSV(ikl)            ! Vegetation Temp.
              CaSnTV(i,j,n)       = snCaSV(ikl)            ! Canopy SnowCover
              CaWaTV(i,j,n)       = rrCaSV(ikl)            ! Canopy RainWater
              psivTV(i,j,n)       = psivSV(ikl)            ! Vegetation Pot.
            DO isl =   -nsol,0                             !
              eta_TV(i,j,n,1-isl) = eta_SV(ikl,isl)        ! Soil Humidity
              TsolTV(i,j,n,1-isl) = TsisSV(ikl,isl)        ! Soil Temperature
            END DO                                         !
          END DO
 
C +--Snow Roughness                                         (INPUT/OUTPUT)
C +  ^^^^^^^^^^^^^^                                          ^^^^^^^^^^^^
          DO  ikl = 1,klonv
              i =  k2i(ikl)
              j =  k2j(ikl)
              n =  k2n(ikl)
            DO nt=   1,ntavSL-1                            !
              SLn_z0(i,j,n,nt)    = SLn_z0(i,j,n,nt+1)     !
              SLn_b0(i,j,n,nt)    = SLn_b0(i,j,n,nt+1)     !
              SLn_r0(i,j,n,nt)    = SLn_r0(i,j,n,nt+1)     !
            ENDDO                                          !
          ENDDO
          DO  ikl = 1,klonv
              i   = k2i(ikl)
              j   = k2j(ikl)
              n   = k2n(ikl)
              SLn_z0(i,j,n,ntavSL)= Z0mnSV(ikl)            ! z0(Momentum)
              SLn_b0(i,j,n,ntavSL)= Z0enSV(ikl)            ! z0(Mom., Erosion)
              SLn_r0(i,j,n,ntavSL)= Z0hnSV(ikl)            ! z0(Heat)
 
c #SZ         Z0SaBS(i,j,n)       = Z0SaSV(ikl)            ! z0(Sastrugi  h)
 
C +--V,  dT(a-s)    Time Moving Averages
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            DO nt=1,ntaver
              V_0aSL(i,j,  nt)    = V__mem(ikl,nt)
              dT0aSL(i,j,n,nt)    = T__mem(ikl,nt)
            ENDDO
 
C +--u*, u*T*, u*s* Time Moving Averages
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #AM       DO nt=1,ntaver
c #AM         u_0aSL(i,j,n,nt)    = u__mem(ikl,nt)
c #AT         uT0aSL(i,j,n,nt)    = uT_mem(ikl,nt)
c #AS         us0aSL(i,j,n,nt)    = us_mem(ikl,nt)
c #AM       ENDDO
          END DO
 
C +--Dust   Fluxes                                          (INPUT/OUTPUT)
C +  ^^^^^^^^^^^^^
c #vD     DO  ikl = 1,klonv
c #vD         i =  k2i(ikl)
c #vD         j =  k2j(ikl)
c #vD         n =  k2n(ikl)
 
c #BD         SLubsl(i,j,n)       =(1-min(1,isnoSV(ikl)))  ! Snow Free  Surface
c #BD.                             *uss_SV(ikl)            ! DUST       Erosion
c #BD.                *max(1,(2-mmy)*3)                    ! Tuning Factor (2D)
c #vD     END DO
 
C +--Snow Pack                                              (INPUT/OUTPUT)
C +  ^^^^^^^^^                                               ^^^^^^^^^^^^
          DO  ikl = 1,klonv
              i =  k2i(ikl)
              j =  k2j(ikl)
              n =  k2n(ikl)
              snohSN(i,j,n)       = BufsSV(ikl)            ! Snow Buffer Lay.
              BrosSN(i,j,n)       = BrosSV(ikl)            ! Snow Buffer dens.
              BG1sSN(i,j,n)       = BG1sSV(ikl)            ! Snow Buffer D./S.
              BG2sSN(i,j,n)       = BG2sSV(ikl)            ! Snow Buffer S./S.
              nssSNo(i,j,n)       = isnoSV(ikl)            ! Nb Snow/Ice Lay.
              issSNo(i,j,n)       = ispiSV(ikl)            ! Nb Supr.Ice Lay.
              nisSNo(i,j,n)       = iiceSV(ikl)            ! Nb      Ice Lay.
              zWE_SN(i,j,n)       = zWE_SV(ikl)            ! Current  *Thick.
              zWEcSN(i,j,n)       = zWEcSV(ikl)            ! Non-Erod.*Thick.
              hSalSN(i,j,n)       = hSalSV(ikl)            ! Salt.Layer Height
              SWaSNo(i,j,n)       = rusnSV(ikl)            ! Surficial Water
              SWSSNo(i,j,n)       = SWS_SV(ikl)            ! Surficial Wat.St.
          END DO
          DO  isn = 1,nsno
          DO  ikl = 1,klonv
              i =  k2i(ikl)
              j =  k2j(ikl)
              n =  k2n(ikl)
              nhsSNo(i,j,n,isn)   = istoSV(ikl,isn)        !            [-]
              dzsSNo(i,j,n,isn)   = dzsnSV(ikl,isn)        !            [m]
              rosSNo(i,j,n,isn)   = ro__SV(ikl,isn)        !        [kg/m3]
              wasSNo(i,j,n,isn)   = eta_SV(ikl,isn)        !        [m3/m3]
              tisSNo(i,j,n,isn)   = TsisSV(ikl,isn)        !            [K]
              g1sSNo(i,j,n,isn)   = G1snSV(ikl,isn)        ! [-]        [-]
              g2sSNo(i,j,n,isn)   = G2snSV(ikl,isn)        ! [-] [0.0001 m]
              agsSNo(i,j,n,isn)   = agsnSV(ikl,isn)        !          [day]
          END DO
          END DO
 
          DO  ikl = 1,klonv
              i =  k2i(ikl)
              j =  k2j(ikl)
              n =  k2n(ikl)
              WKxyz2(i,j,n)       = wes_SV(ikl)            ! Depo. / Subli.
              WKxyz3(i,j,n)       = wem_SV(ikl)            ! Melting
              WKxyz4(i,j,n)       = wer_SV(ikl)            ! Refreezing
cc#IB         weacIB(i,j,n)       = weacIB(i,j,n) + dbs_Ac(ikl) ! #BS acc
              weerIB(i,j,n)       = weerIB(i,j,n) + dbs_Er(ikl) ! #BS ero
 
 
C +--Radiative Properties                                         (OUTPUT)
C +  ^^^^^^^^^^^^^^^^^^^^                                          ^^^^^^
              albxSL(i,j,n)       = alb_SV(ikl)            ! Mosaic Albedo
c #AO.                          *(1-maskSL(i,j))           !
c #AO.                          +    albAO(i,j,n)          ! Mosaic AlbedoNEMO
c #AO.                          *   maskSL(i,j)            !
              WKxyz6(i,j,n)       = emi_SV(ikl)            ! Mosaic Emissivity
              WKxyz7(i,j,n)       = IRu_SV(ikl)            ! Mosaic Upw.IR
              WKxyz8(i,j,n)       = qSalSV(ikl)            ! Saltating Partic.
              hfra  (i,j,n)       = HFraSV(ikl)            ! Frazil  Thickness
              Rnof  (i,j,n)       = RnofSV(ikl)            ! Run OFF Intensity
 
          END DO
 
        ELSE
              nkl = ijnmax2 - nnn - klonv
          DO  ikl = 1,max(1,nkl)
              i   = k2i(ikl)
              j   = k2j(ikl)
              n   = k2n(ikl)
 
C +--Verification of Vectorization
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
c #VR         ij2             = ij2         + 1
c #VR         ijdver(i,j,n)   = ijdver(i,j,n) - ij2
c #VR         ij_ver(i,j,n)   = ij_ver(i,j,n) + 1
 
C +--Energy Fluxes                                          (INPUT/OUTPUT)
C +  ^^^^^^^^^^^^^                                           ^^^^^^^^^^^^
              IRsoil(i,j,n)       = IRs_SV(ikl)            ! Soil upward IR
              SLlmol(i,j,n)       = LMO_SV(ikl)            ! Monin-Obukhov L.
              SLuusl(i,j,n)       = us__SV(ikl)            ! Frict. Velocity
              SLutsl(i,j,n)       = uts_SV(ikl)            ! u*T*
              SLdSdT(i,j,n)       = dSdTSV(ikl)            ! Sens.H.Flux T-Der.
 
C +--Energy Fluxes                                          (OUTPUT/NetCDF)
C +  ^^^^^^^^^^^^^                                           ^^^^^^^^^^^^^
c #NC         SOsoNC(i,j,n)       = SOsoKL(ikl)            ! Absorb.Sol.Rad.
c #NC         IRsoNC(i,j,n)       = IRsoKL(ikl)            ! Absorb.IR  Rad.
c #NC         HSsoNC(i,j,n)       = HSsoKL(ikl)            ! HS
c #NC         HLsoNC(i,j,n)       = HLsoKL(ikl)            ! HL
c #NC         HLs_NC(i,j,n)       = HLs_KL(ikl)            ! Evaporation
c #NC         HLv_NC(i,j,n)       = HLv_KL(ikl)            ! Transpiration
 
C +--Water  Fluxes                                          (INPUT/OUTPUT)
C +  ^^^^^^^^^^^^^                                           ^^^^^^^^^^^^
              SLuqsl(i,j,n)       = uqs_SV(ikl)            ! u*q*
              SLdLdT(i,j,n)       = dLdTSV(ikl)            ! Latn.H.Flux T-Der.
              SLussl(i,j,n)       = 0.                     !
c #BS         SLussl(i,j,n)       =                        ! MAX        Erosion
c #BS.       (blowSN(i,j,n)- dbs_SV(ikl))                  !-unconsumed Erosion
c #BS.                     /(dtPhys*rhT_SV(ikl))           ! ==> actual u*s*
          ENDDO
          DO  ikl = 1,  nkl
              i   = k2i(ikl)
              j   = k2j(ikl)
              n   = k2n(ikl)
c #BS         blowSN(i,j,n)= dtPhys*uss_SV(ikl)            ! NEW  MAX   Erosion
c #BS.                             *rhT_SV(ikl)            ! rho u*s* dt[kg/m2]
c #AE         SaltSN(i,j,n)       = usthSV(ikl)            ! u*_th
 
C +--Soil/Canopy                                            (INPUT/OUTPUT)
C +  ^^^^^^^^^^^                                             ^^^^^^^^^^^^
               SL_z0(i,j,n)       = Z0m_SV(ikl)            ! Moment.Roughn.L.
               SL_r0(i,j,n)       = Z0h_SV(ikl)            ! Heat   Roughn.L.
               cdmSL(i,j,n)       = rCDmSV(ikl)            ! sq.root Contr.Drag
               cdhSL(i,j,n)       = rCDhSV(ikl)            ! sq.root Contr.Drag
              TvegTV(i,j,n)       = TvegSV(ikl)            ! Vegetation Temp.
              CaSnTV(i,j,n)       = snCaSV(ikl)            ! Canopy SnowCover
              CaWaTV(i,j,n)       = rrCaSV(ikl)            ! Canopy RainWater
              psivTV(i,j,n)       = psivSV(ikl)            ! Vegetation Pot.
            DO isl =   -nsol,0                             !
              eta_TV(i,j,n,1-isl) = eta_SV(ikl,isl)        ! Soil Humidity
              TsolTV(i,j,n,1-isl) = TsisSV(ikl,isl)        ! Soil Temperature
            END DO                                         !
          END DO
 
C +--Snow Roughness                                         (INPUT/OUTPUT)
C +  ^^^^^^^^^^^^^^                                          ^^^^^^^^^^^^
          DO  ikl = 1, nkl
              i =  k2i(ikl)
              j =  k2j(ikl)
              n =  k2n(ikl)
            DO nt=   1,ntavSL-1                            !
              SLn_z0(i,j,n,nt)    = SLn_z0(i,j,n,nt+1)     !
              SLn_b0(i,j,n,nt)    = SLn_b0(i,j,n,nt+1)     !
              SLn_r0(i,j,n,nt)    = SLn_r0(i,j,n,nt+1)     !
            ENDDO                                          !
          ENDDO
          DO  ikl = 1,  nkl
              i   = k2i(ikl)
              j   = k2j(ikl)
              n   = k2n(ikl)
              SLn_z0(i,j,n,ntavSL)= Z0mnSV(ikl)            ! z0(Momentum)
              SLn_b0(i,j,n,ntavSL)= Z0enSV(ikl)            ! z0(Mom., Erosion)
              SLn_r0(i,j,n,ntavSL)= Z0hnSV(ikl)            ! z0(Heat)
 
c #SZ         Z0SaBS(i,j,n)       = Z0SaSV(ikl)            ! z0(Sastrugi  h)
 
C +--V,  dT(a-s)    Time Moving Averages
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            DO nt=1,ntaver
              V_0aSL(i,j,  nt)    = V__mem(ikl,nt)
              dT0aSL(i,j,n,nt)    = T__mem(ikl,nt)
            ENDDO
 
C +--u*, u*T*, u*s* Time Moving Averages
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #AM       DO nt=1,ntaver
c #AM         u_0aSL(i,j,n,nt)    = u__mem(ikl,nt)
c #AT         uT0aSL(i,j,n,nt)    = uT_mem(ikl,nt)
c #AS         us0aSL(i,j,n,nt)    = us_mem(ikl,nt)
c #AM       ENDDO
          END DO
 
C +--Dust   Fluxes                                          (INPUT/OUTPUT)
C +  ^^^^^^^^^^^^^
c #vD     DO  ikl = 1, nkl
c #vD         i =  k2i(ikl)
c #vD         j =  k2j(ikl)
c #vD         n =  k2n(ikl)
 
c #BD         SLubsl(i,j,n)       =(1-min(1,isnoSV(ikl)))  ! Snow Free  Surface
c #BD.                             *uss_SV(ikl)            ! DUST       Erosion
c #BD.                *max(1,(2-mmy)*3)                    ! Tuning Factor (2D)
c #vD     END DO
 
C +--Snow Pack                                              (INPUT/OUTPUT)
C +  ^^^^^^^^^                                               ^^^^^^^^^^^^
          DO  ikl = 1, nkl
              i =  k2i(ikl)
              j =  k2j(ikl)
              n =  k2n(ikl)
              snohSN(i,j,n)       = BufsSV(ikl)            ! Snow Buffer Lay.
              BrosSN(i,j,n)       = BrosSV(ikl)            ! Snow Buffer dens.
              BG1sSN(i,j,n)       = BG1sSV(ikl)            ! Snow Buffer D./S.
              BG2sSN(i,j,n)       = BG2sSV(ikl)            ! Snow Buffer S./S.
              nssSNo(i,j,n)       = isnoSV(ikl)            ! Nb Snow/Ice Lay.
              issSNo(i,j,n)       = ispiSV(ikl)            ! Nb Supr.Ice Lay.
              nisSNo(i,j,n)       = iiceSV(ikl)            ! Nb      Ice Lay.
              zWE_SN(i,j,n)       = zWE_SV(ikl)            ! Current  *Thick.
              zWEcSN(i,j,n)       = zWEcSV(ikl)            ! Non-Erod.*Thick.
              hSalSN(i,j,n)       = hSalSV(ikl)            ! Salt.Layer Height
              SWaSNo(i,j,n)       = rusnSV(ikl)            ! Surficial Water
              SWSSNo(i,j,n)       = SWS_SV(ikl)            ! Surficial Wat.St.
          END DO
          DO  isn = 1,nsno
          DO  ikl = 1, nkl
              i =  k2i(ikl)
              j =  k2j(ikl)
              n =  k2n(ikl)
              nhsSNo(i,j,n,isn)   = istoSV(ikl,isn)        !            [-]
              dzsSNo(i,j,n,isn)   = dzsnSV(ikl,isn)        !            [m]
              rosSNo(i,j,n,isn)   = ro__SV(ikl,isn)        !        [kg/m3]
              wasSNo(i,j,n,isn)   = eta_SV(ikl,isn)        !        [m3/m3]
              tisSNo(i,j,n,isn)   = TsisSV(ikl,isn)        !            [K]
              g1sSNo(i,j,n,isn)   = G1snSV(ikl,isn)        ! [-]        [-]
              g2sSNo(i,j,n,isn)   = G2snSV(ikl,isn)        ! [-] [0.0001 m]
              agsSNo(i,j,n,isn)   = agsnSV(ikl,isn)        !          [day]
          END DO
          END DO
 
          DO  ikl = 1, nkl
              i =  k2i(ikl)
              j =  k2j(ikl)
              n =  k2n(ikl)
              WKxyz2(i,j,n)       = wes_SV(ikl)            ! Depo. / Subli.
              WKxyz3(i,j,n)       = wem_SV(ikl)            ! Melting
              WKxyz4(i,j,n)       = wer_SV(ikl)            ! Refreezing
cc#IB         weacIB(i,j,n)       = weacIB(i,j,n) + dbs_Ac(ikl) ! #BS acc
              weerIB(i,j,n)       = weerIB(i,j,n) + dbs_Er(ikl) ! #BS ero
 
C +--Radiative Properties                                         (OUTPUT)
C +  ^^^^^^^^^^^^^^^^^^^^                                          ^^^^^^
              albxSL(i,j,n)       = alb_SV(ikl)            ! Mosaic Albedo MAR
c #AO.                          *(1-maskSL(i,j))           !
c #AO.                          +    albAO(i,j,n)          ! Mosaic AlbedoNEMO
c #AO.                          *   maskSL(i,j)            !
              WKxyz6(i,j,n)       = emi_SV(ikl)            ! Mosaic Emissivity
              WKxyz7(i,j,n)       = IRu_SV(ikl)            ! Mosaic Upw.IR
              WKxyz8(i,j,n)       = qSalSV(ikl)            ! Saltating Partic.
              hfra  (i,j,n)       = HFraSV(ikl)            ! Frazil  Thickness
              Rnof  (i,j,n)       = RnofSV(ikl)            ! Run OFF Intensity
 
          END DO
 
        END IF
 
      END DO
 
 
C +--Surface Temperature: Prescription of relevant Medium (Snow, precribed SST)
C +  ==========================================================================
 
      DO j=jp11,my1
      DO i=ip11,mx1
            DO isl =   -nsol,0                             !
 
 
C +--Open Ocean
C +  ----------
 
              eta_TV(i,j,1,1-isl) =                        !
     .        eta_TV(i,j,1,1-isl) *(1-maskSL(i,j))         !
     .                              + maskSL(i,j)          ! Sea: Humidity:=1
              TsolTV(i,j,1,1-isl) =                        !
     .       (TsolTV(i,j,1,1-isl) *(1-maskSL(i,j))         ! Soil Temperature
     .       +sst_LB(i,j)         *   maskSL(i,j))         ! Prescribed   SST
c #OP.                                            * FixSST !
c #OP.      +(TsolTV(i,j,1,1-isl)                          !
c #op.      +(sst_LB(i,j)                                  !~Prescribed   SST
c #op.       -TsolTV(i,j,1,1-isl))*   maskSL(i,j) * SSTnud ! (Nudging)
c #OP.                                           )* VarSST ! Interactive  SST
 
 
C +--Sea Ice
C +  -------
 
c #AO         eta_TV(i,j,2,1-isl) =                        !
c #AO.        eta_TV(i,j,2,1-isl) *(1-maskSL(i,j))         ! Sea: Humidity:=0
c #AO         TsolTV(i,j,2,1-isl) =                        !
c #AO.       (TsolTV(i,j,2,1-isl) *(1-maskSL(i,j))         ! Soil Temperature
c #AO.       +271.2               *   maskSL(i,j))         ! Prescribed    ST
            END DO
c #AO       DO isl =         0,nsno                        !
c #AO         tisSNo(i,j,2,isl)   =
c #AO.        srftAO(i,j,2)       *   maskSL(i,j)          ! Prescribed   SIT
c #AO       END DO
      ENDDO
      ENDDO
 
 
      DO n=1,mw
      DO j=jp11,my1
      DO i=ip11,mx1
              tsrfSL(i,j,n)       =                        ! Surf.Temperature
     .        TsolTV(i,j,n,1)                              !
     .                           *(1-min(1,nssSNo(i,j,n))) !
     .       +tisSNo(i,j,n,          max(1,nssSNo(i,j,n))) !
     .                           *   min(1,nssSNo(i,j,n))  !
C +                                                        !
c #NC         eta_NC(i,j,n)       = 0.                     !
c #NC         DO isl = -nsol,0                             !
c #NC         eta_NC(i,j,n)       = eta_NC(i,j,n)          ! Soil Moisture
c #NC.       +eta_TV(i,j,n,1-isl) * dz_dSV(    isl)        !
c #NC         END DO                                       !
      ENDDO
      ENDDO
      ENDDO
 
 
C +--Mosaic Cleaning
C +  ===============
 
      DO j=jp11,my1
      DO i=ip11,mx1
        IF (maskSL(i,j  ).EQ.   1)                                  THEN
            nssSNo(i,j,1    ) = 0
            issSNo(i,j,1    ) = 0
            nisSNo(i,j,1    ) = 0
          DO ISL=1,nsno
            tisSNo(i,j,1,isl) = 0.
            dzsSNo(i,j,1,isl) = 0.
            rosSNo(i,j,1,isl) = 0.
            wasSNo(i,j,1,isl) = 0.
            g1sSNo(i,j,1,isl) = 0.
            g2sSNo(i,j,1,isl) = 0.
            agsSNo(i,j,1,isl) = 0.
            nhsSNo(i,j,1,isl) = 0.
          ENDDO
        ENDIF
        IF (SLsrfl(i,j,2).LT.eps9)                                  THEN
            tsrfSL(i,j,2)     = tsrfSL(i,j,1)
          DO isl=1,nsol+1
            TsolTV(i,j,2,isl) = TsolTV(i,j,1,isl)
          ENDDO    ! #n2
            nssSNo(i,j,2    ) = nssSNo(i,j,1    ) * (1 - maskSL(i,j))
            issSNo(i,j,2    ) = issSNo(i,j,1    ) * (1 - maskSL(i,j))
            nisSNo(i,j,2    ) = nisSNo(i,j,1    ) * (1 - maskSL(i,j))
          DO isl=1,nsno       !
            tisSNo(i,j,2,isl) = tisSNo(i,j,1,isl) * (1 - maskSL(i,j))
            dzsSNo(i,j,2,isl) = dzsSNo(i,j,1,isl) * (1 - maskSL(i,j))
            rosSNo(i,j,2,isl) = rosSNo(i,j,1,isl) * (1 - maskSL(i,j))
            wasSNo(i,j,2,isl) = wasSNo(i,j,1,isl) * (1 - maskSL(i,j))
            g1sSNo(i,j,2,isl) = g1sSNo(i,j,1,isl) * (1 - maskSL(i,j))
            g2sSNo(i,j,2,isl) = g2sSNo(i,j,1,isl) * (1 - maskSL(i,j))
            agsSNo(i,j,2,isl) = agsSNo(i,j,1,isl) * (1 - maskSL(i,j))
            nhsSNo(i,j,2,isl) = nhsSNo(i,j,1,isl) * (1 - maskSL(i,j))
          ENDDO    ! #n2
        ENDIF      ! #n2
      ENDDO
      ENDDO
 
 
C +--Grid Averages / Diagnostics
C +  ===========================
 
C +--Grid Averages                                                (OUTPUT)
C +  ^^^^^^^^^^^^^                                                 ^^^^^^
      DO  n=1,mw
      DO  j=jp11,my1
      DO  i=ip11,mx1
              wes_IB(i,j,n)     = - WKxyz2(i,j,n)          !
     .                            + wes_IB(i,j,n)          ! Depo. / Subli.
              wem_IB(i,j,n)       = WKxyz3(i,j,n)          !
     .                            + wem_IB(i,j,n)          ! Melting
              wer_IB(i,j,n)       = WKxyz4(i,j,n)          !
     .                            + wer_IB(i,j,n)          ! Refreezing
              wee_IB(i,j,n)     = - SLuqsl(i,j,n)          !
     .            * 1.e3 * dt__SV * rolvDY(i,j,mz)         !
     .                            + wee_IB(i,j,n)          ! Evapotranspiration
              weu_IB(i,j,n)       = Rnof  (i,j,n)*dt__SV   !
     .                            + weu_IB(i,j,n)          ! Refreezing
 
c #WR         ifrVER(i,j)         = ifrVER(i,j)            !
c #WR.                            + ifraTV(i,j,n)          !
              albeSL(i,j)         = albeSL(i,j)            ! Grid   Albedo
     .            + SLsrfl(i,j,n) * albxSL(i,j,n)          ! Mosaic Albedo
              eps0SL(i,j)         = eps0SL(i,j)            ! Grid   Emissivity
     .            + SLsrfl(i,j,n) * WKxyz6(i,j,n)          ! Mosaic Emissivity
              Upw_IR(i,j)         = Upw_IR(i,j)            !
     .            + SLsrfl(i,j,n) * WKxyz7(i,j,n)          ! Mosaic Upw.IR
               SLlmo(i,j)         =  SLlmo(i,j)            !
     .            + SLsrfl(i,j,n) * SLlmol(i,j,n)          ! Mosaic Mon.Ob.
               SLuus(i,j)         =  SLuus(i,j)            ! Grid   u*
     .            + SLsrfl(i,j,n) * SLuusl(i,j,n)          ! Mosaic u*
               SLuts(i,j)         =  SLuts(i,j)            ! Grid   u*T*
     .            + SLsrfl(i,j,n) * SLutsl(i,j,n)          ! Mosaic u*T*
               SLuqs(i,j)         =  SLuqs(i,j)            ! Grid   u*q*
     .            + SLsrfl(i,j,n) * SLuqsl(i,j,n)          ! Mosaic u*q*
c #BS          uss_HY(i,j)        = uss_HY(i,j)            ! Grid   u*s*
c #BS.            + SLsrfl(i,j,n) * SLussl(i,j,n)          ! Mosaic u*s*
C +...NO !    SLussl(i,j,n)       = uss_SV(ikl)            !        u*s*
C +           Upper Update = wrong Source of Atmospher.Snow!
c #BS         qsrfHY(i,j)         = qsrfHY(i,j)            ! Salt.Part.Concent.
c #BS.            + SLsrfl(i,j,n) * WKxyz8(i,j,n)          !
c #BS.                    *   min(1,nssSNo(i,j,n))         !
c #PO         HFraPO(i,j)         = HFraPO(i,j)            ! Frazil  Thickness
c #PO.            + SLsrfl(i,j,n) * HFra  (i,j,n)          !
              TairSL(i,j)         = TairSL(i,j)            ! Surface Air
     .            +SLsrfl(i,j,n)  * tsrfSL(i,j,n)          !         Temperatur
              draiTV(i,j)         = draiTV(i,j)            ! Run OFF Intensity
     .            + SLsrfl(i,j,n) * Rnof  (i,j,n)          !
c #TC           uqTC(i,j,1)       =   uqTC(i,j,1)          ! Grid   u*b*
c #TC.            + SLsrfl(i,j,n) * SLubsl(i,j,n)          ! Mosaic u*b*
c #TC           qsTC(i,j,1)       =   qsTC(i,j,1)          ! Salt.Part.Concent.
c #TC.            + SLsrfl(i,j,n) * WKxyz8(i,j,n)          !
c #TC.                    *(1-min(1,nssSNo(i,j,n)))        !
      ENDDO
      ENDDO
      ENDDO
 
      DO j=jp11,my1
      DO i=ip11,mx1
        sno0HY(i,j) = snowHY(i,j)                          !
        pktaSL(i,j) = TairSL(i,j)                          !
     .    /exp(cap*log(pstDY(i,j)+ptopDY))                 !
         tviRA(i,j) = sqrt(sqrt(Upw_IR(i,j)/stefan))       ! Brightness Temp.
         rhAir      = rolvDY(i,j,mz) *1.e3                 ! Air    Densitity
        hsenSL(i,j) = -SLuts(i,j)  *  rhAir       *cp      ! Sensible Heat Flux
        qvapSL(i,j) = qvsiDY(i,j,mzz)                      ! Surf.Specif.Humid.
                                                           ! to adapt over soil
        hlatSL(i,j) = -SLuqs(i,j)  *  rhAir       *Lv_H2O  ! Latent   Heat Flux
        evapTV(i,j) = evapTV(i,j)                          ! Total    Evaporat.
     .                -SLuqs(i,j)  *  rhAir       *dt__SV  ! [mm w.e.]
        runoTV(i,j) = runoTV(i,j)  +  draiTV(i,j) *dt__SV  ! Integrated Run OFF
        firmSL(i,j) = Upw_IR(i,j)
      END DO
      END DO
 
 
C +--Blown Snow/Dust Accumulation
C +  ============================
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! #wx IF                            (lSV_v1.EQ.2)
! #wx.  write(6,6011)                uss_HY(iSV_v1,jSV_v1)  *1.e3
 6011   format(10x,'After SISVAT(1): us* [mm/s] =',f9.3)
 
c #BS DO i=ip11,mx1
c #BS DO j=   1,my
c #BS        WKxy6(i     ,j     ) =
c #BS&      uss_HY(im1(i),j     )+2.0*uss_HY(i     ,j     )
c #BS& +    uss_HY(ip1(i),j     )
c #BS ENDDO
c #BS ENDDO
 
c #BS DO j=jp11,my1
c #BS DO i=ip11,mx1
c #BS        WKxy5(i     ,j     ) =    WKxy6(i     ,jm1(j))
c #BS        WKxy7(i     ,j     ) =    WKxy6(i     ,jp1(j))
c #BS ENDDO
c #BS ENDDO
 
c #BS DO j=jp11,my1
c #BS DO i=ip11,mx1
c #BS       uss_HY(i     ,j     ) =
c #BS.       WKxy7(i     ,j     )
c #BS.     + WKxy6(i     ,j     ) +    WKxy6(i     ,j     )
c #BS.     + WKxy5(i     ,j     )
 
!  Previous three Loops Stand for the following unvectorized Loop:
!       WKxy2(i,j) = uss_HY(im1(i),jp1(j))
!    .         +2.d0*uss_HY(i     ,jp1(j)) +     uss_HY(ip1(i),jp1(j))
!    .         +2.d0*uss_HY(im1(i),j)
!    .         +4.d0*uss_HY(i     ,j)      +2.d0*uss_HY(ip1(i),j)
!    .         +     uss_HY(im1(i),jm1(j))
!    .         +2.d0*uss_HY(i     ,jm1(j)) +     uss_HY(ip1(i),jm1(j))
c #BS END DO
c #BS END DO
 
c #BD DO i=1,mx
c #BD DO j=1,my
c #BD   WKxy3(i,j) = uqTC(im1(i),jp1(j),1)
c #BD.         +2.d0*uqTC(i     ,jp1(j),1) +     uqTC(ip1(i),jp1(j),1)
c #BD.         +2.d0*uqTC(im1(i),j     ,1)
c #BD.         +4.d0*uqTC(i     ,j     ,1) +2.d0*uqTC(ip1(i),j     ,1)
c #BD.         +     uqTC(im1(i),jm1(j),1)
c #BD.         +2.d0*uqTC(i     ,jm1(j),1) +     uqTC(ip1(i),jm1(j),1)
c #BD END DO
c #BD END DO
 
c #BS DO i=1,mx
c #BS DO j=1,my
c XF BUG
c #BS   uss_HY(i,j)  = uss_HY(i,j)          * 62.5e-3
cc#BS   snowHY(i,j)  = snowHY(i,j) + dt__SV * rolvDY(i,j,mz)*uss_HY(i,j)
c #BS   weacIB(i,j,1)= weacIB(i,j,1)-dt__SV * rolvDY(i,j,mz)*uss_HY(i,j)
c #BS.               * 1000.
c #BD     uqTC(i,j,1)=  WKxy3(i,j)          * 62.5e-3
c #BS END DO
c #BS END DO
 
! OUTPUT in SISVAT at specified i,j,k,n (see assignation in PHY_SISVAT)
! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! #wx IF                            (lSV_v1.EQ.2)
! #wx.  write(6,6012)                uss_HY(iSV_v1,jSV_v1)  *1.e3
 6012   format(10x,'After SISVAT(2): us* [mm/s] =',f9.3)
 
 
C +--Formation of Lakes
C +  ==================
 
 
 
 
C +--Sea-Ice Ice Floe Size
C +  =====================
 
C +--Prescription from SST
C +  ---------------------
 
      IF (VarSST.le.epsi)                                           THEN
        DO j=jp11,my1
        DO i=ip11,mx1
             FraOcn          =    (TsolTV(i,j,1,1)-Tfr_LB)/TSIdSV! Prescribed
C +                                                              ! from SST
             FraOcn          =  1.-sicsIB(i,j)                   ! Prescribed
C +                                                              ! from SSM/I
             FraOcn          = min(  unun,FraOcn)                ! UpperLimit
             FraOcn          = max(OcnMin,FraOcn)                ! LowerLimit
             SLsrfl(i,j,1)   =  (1-maskSL(i,j))  * SLsrfl(i,j,1) ! New Ocean
     .                          +  maskSL(i,j)   * FraOcn        !
             SrfSIC          =                     SLsrfl(i,j,2) ! Old Sea Ice
             SIc0OK          = max(zero, sign(unun,SrfSIC-epsi)) !
             SLsrfl(i,j,2)   =  (1-maskSL(i,j))  * SLsrfl(i,j,2) ! New Sea Ice
     .                          +  maskSL(i,j)*(1.-FraOcn)       !
             SIceOK          = max(zero, sign(unun,SLsrfl(i,j,2) !
     .                                                   -epsi)) !
             ifra_t          =     ifraTV(i,j,1) + ifraTV(i,j,2) ! OCN Fraction
             ifraTV(i,j,1)   =     SLsrfl(i,j,1) * 100.          !
             ifraTV(i,j,1)   = min(ifraTV(i,j,1) , ifra_t)       !
             ifraTV(i,j,2)   =     ifra_t        - ifraTV(i,j,1) !
 
C +--Sea-Ice Vertical Discretization
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
             nssSNo(i,j,2)   =
     .       nssSNo(i,j,2)                    *(1-maskSL(i,j))
     .     +(max(1
     .      ,nssSNo(i,j,2))  *    SIc0OK
     .      +     3          *(1.-SIc0OK)*SIceOK)*maskSL(i,j)
C +
             nisSNo(i,j,2)   =
     .       nisSNo(i,j,2)                    *(1-maskSL(i,j))
     .     +(max(1
     .      ,nisSNo(i,j,2))  *    SIc0OK
     .      +     3          *(1.-SIc0OK)*SIceOK)*maskSL(i,j)
             issSNo(i,j,2)   =                    nisSNo(i,j,2)
C +
          DO l=1,nsno
             dzsSNo(i,j,2,l) =
     .       dzsSNo(i,j,2,l)                  *(1-maskSL(i,j))
     .     +(max
     .      (SIc_OK(min(2,l))*    SIcMIN
     .      ,dzsSNo(i,j,2,l))*    SIc0OK
     .      +dzSIce(min(4,l))*(1.-SIc0OK)*SIceOK)*maskSL(i,j)
C +
             tisSNo(i,j,2,l) =
     .       tisSNo(i,j,2,l)                  *(1-maskSL(i,j))
     .     +(tisSNo(i,j,2,l) *    SIc0OK
     .      +TsolTV(i,j,1,1) *(1.-SIc0OK)       )*maskSL(i,j)
C +
             rosSNo(i,j,2,l) =
     .       rosSNo(i,j,2,l)                  *(1-maskSL(i,j))
     .     +(max
     .      (SIc_OK(min(2,l))*    ro_Ice
     .      ,rosSNo(i,j,2,l))*    SIc0OK
     .      +ro_Ice          *(1.-SIc0OK)*SIceOK)*maskSL(i,j)
C +
             g1sSNo(i,j,2,l) =
     .       g1sSNo(i,j,2,l)                  *(1-maskSL(i,j))
     .     +(g1sSNo(i,j,2,l) *    SIc0OK
     .      +G1_dSV          *(1.-SIc0OK)*SIceOK)*maskSL(i,j)
C +
             g2sSNo(i,j,2,l) =
     .       g2sSNo(i,j,2,l)                  *(1-maskSL(i,j))
     .     +(g2sSNo(i,j,2,l) *    SIc0OK
     .      +30.             *(1.-SIc0OK)*SIceOK)*maskSL(i,j)
C +
             nhsSNo(i,j,2,l) =
     .       nhsSNo(i,j,2,l)                  *(1-maskSL(i,j))
C #SI.new  +(nhsSNo(i,j,2,l) *    SIc0OK
C #SI.new   +istdSV(2)       *(1.-SIc0OK)*SIceOK)*maskSL(i,j)
     .     + istdSV(2)       *                    maskSL(i,j)
          END DO
          DO l=1,llx
             TsolTV(i,j,2,l) =
     .       TsolTV(i,j,2,l)                  *(1-maskSL(i,j))
     .     +(TsolTV(i,j,2,l) *    SIc0OK
     .      +TsolTV(i,j,1,l) *(1.-SIc0OK)       )*maskSL(i,j)
C +
             eta_TV(i,j,2,l) =
     .       eta_TV(i,j,2,l)                  *(1-maskSL(i,j))
     .     + eta_TV(i,j,2,l) *    SIc0OK  *       maskSL(i,j)
C +...                            No Pore in Ice => No Water
          END DO
C +
c #WI        write(6,6001) jdarGE,labmGE(mmarGE),iyrrGE
c #WI.                    ,jhurGE,minuGE,jsecGE ,TsolTV(i,j,1,1)
c #WI.                    ,FraOcn,ifraTV(i,j,1) ,TsolTV(i,j,2,1)
c #WI.                    ,       nisSNo(i,j,2) ,nssSNo(i,j,2)
C +
        END DO
        END DO
      END IF
 
C +--Otherwise SST and FrLead have been computed in the Sea-Ice Polynya Model
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
C +--Rainfall, Snowfall Time Integral at previous Time Step
C +  ------------------------------------------------------
 
        DO j=1,my
        DO i=1,mx
                            rai0HY(i,j)=rainHY(i,j)    ! Rainfall Time Integral
                            sfa0HY(i,j)=snowHY(i,j)    ! Snowfall Time Integral
        END DO                                         ! Erosion  skipped
        END DO
 
 
!  Wind Horizontal Components         at previous Time Step
!  --------------------------------------------------------
 
        DO j=1,my
        DO i=1,mx
                            ua_0BS(i,j)=uairDY(i,j,mz) !
                            va_0BS(i,j)=vairDY(i,j,mz) !
        END DO                                         !
        END DO
 
 
C +--Additional OUTPUT for VERIFICATION
C +  ----------------------------------
 
c #WR DO j=jp11,my1
c #WR DO i=ip11,mx1
c #WR   IF (ifrVER(i,j).ne.100) write(6,660) isolSL(i,j),i,j,ifrVER(i,j)
c #WR.                                     ,(ifraTV(i,j,n),n=1,nvx)
 660    format(' WARNING: Mosaic',i2,' (',2i4,') = ',i4,i6,2i4)
c #WR END DO
c #WR END DO
 
c #WR i = imez + 10.*111.111e3/dx
c #WR j = jmez
c #WR write(6,6060) itexpe,jdarGE,labmGE(mmarGE),iyrrGE
c #WR.                    ,jhurGE,minuGE        ,GElatr(i,j)/degrad
c #WR.             ,tairDY(i,j,mz),virDY(i,j,mz),1.e3*rolvDY(i,j,mz)
c #WR.             ,hsenSL(i,j)  ,hlatSL(i,j),-86400.0*SLuqs(i,j)
c #WR.        ,1.e3*rainHY(i,j)  ,evapTV(i,j),        runoTV(i,j)
 6060 format(i6,i3,'-',a3,'-',i4,':',i2,':',i2,f6.2,'?N',
     .       f9.3,' K',f6.3,f6.3,' kg/m3',2(f6.1,' W/m2'),
     .       f6.3,' mm/day',3(f9.3,' mm'))
 
C +--Verification of Vectorization
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
c #VR   write(6,6100)
 6100   format(/,'Verification of Vectorization: Before CALL')
c #VR   DO n=mw,1,-1
c #VR   DO j=my,1,-1
c #VR   write(6,6110) (ij0ver(i,j,n),i=1,mx)
 6110   format(132i1)
c #VR   ENDDO
c #VR   write(6,6103)
 6103   format(1x)
c #VR   ENDDO
 
c #VR   write(6,6101)
 6101   format(/,'Verification of Vectorization: After  CALL')
c #VR   DO n=mw,1,-1
c #VR   DO j=my,1,-1
c #VR   write(6,6110) (ij_ver(i,j,n),i=1,mx)
c #VR   ENDDO
c #VR   write(6,6103)
c #VR   ENDDO
 
c #VR   DO n=1,mw
c #VR   DO j=1,my
c #VR   DO i=1,mx
c #VR     IF (ijdver(i,j,n).NE.0 .AND. ij_ver(i,j,n).NE.1)
c #VR.      write(6,6102) i,j,n,ijdver(i,j,n)
 6102       format(' Vectorization ERROR on',3i4,'   (',i6,')')
c #VR   ENDDO
c #VR   ENDDO
c #VR   ENDDO
 
 
C +--Work Array Reset
C +  ================
 
        DO j=1,my
        DO i=1,mx
          WKxy1( i,j)   = 0.
          WKxy2( i,j)   = 0.
          WKxy3( i,j)   = 0.
          WKxy5( i,j)   = 0.
          WKxy6( i,j)   = 0.
          WKxy7( i,j)   = 0.
        END DO
        END DO
 
        DO k=1,mw
        DO j=1,my
        DO i=1,mx
          WKxyz1(i,j,k) = 0.
          WKxyz2(i,j,k) = 0.
          WKxyz3(i,j,k) = 0.
          WKxyz4(i,j,k) = 0.
          WKxyz5(i,j,k) = 0.
          WKxyz6(i,j,k) = 0.
          WKxyz7(i,j,k) = 0.
          WKxyz8(i,j,k) = 0.
        END DO
        END DO
        END DO
 
      IF (.not.INI_SV)
     .         INI_SV = .true.
 
      return
      end
