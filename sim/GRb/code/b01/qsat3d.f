 
 
      subroutine qsat3D
 
C +------------------------------------------------------------------------+
C | MAR PHYSICS                                         Mc 30-05-2007  MAR |
C |   SubRoutine qsat3D computes the Saturation Specific Humidity  (kg/kg) |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT :   TairSL: Surface Air Temperature                        (K) |
C |   ^^^^^^^   TairDY:         Air Temperature                        (K) |
C |              pstDY: Model Pressure Thickness                     (kPa) |
C |                                                                        |
C |   OUTPUT :  qvswDY: Saturation Specific Humidity  over Water   (kg/kg) |
C |   ^^^^^^^   qvsiDY: Saturation Specific Humidity  over Ice     (kg/kg) |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_DY.inc'
      include 'MAR_SL.inc'
      include 'MAR_WK.inc'
 
 
C +--Local  Variables
C +  ================
 
      real     WatIce,ExpWat,ExpWa2,ExpIce
 
 
C +--DATA
C +  ====
 
      data     WatIce/273.16e0/
      data     ExpWat/5.138e0/
      data     ExpWa2/6827.e0/
      data     ExpIce/6150.e0/
 
 
C +--Temperature (K) and Pressure (hPa)
C +  ==================================
 
      DO   k=1,mz
        DO j=1,my
        DO i=1,mx
          WKxyz5(i,j,k)   = tairDY(i,j,k)
          WKxyz6(i,j,k)   = (pstDY(i,j)  * sigma(k) + ptopDY) * 10.0d0
        END DO
        END DO
      END DO
 
        DO j=1,my
        DO i=1,mx
          WKxyz5(i,j,mzz) = TairSL(i,j)
          WKxyz6(i,j,mzz) = (pstDY(i,j)             + ptopDY) * 10.0d0
        END DO
        END DO
 
 
C +--Saturation Vapor Pressure over Ice
C +  ==================================
 
      DO   k=1,mzz
        DO j=1,my
        DO i=1,mx
          WKxyz7(i,j,k) =
     .    6.1070d0 * exp (ExpIce*(unun/WatIce-unun/WKxyz5(i,j,k)))
C +...    Dudhia (1989) JAS, (B1) and (B2) p.3103
 
          WKxyz8(i,j,k) = .622d0*WKxyz7(i,j,k)
     .  /(WKxyz6(i,j,k) - .378d0*WKxyz7(i,j,k))
 
 
C +--Saturation Vapor Pressure over Water
C +  ====================================
 
          WKxyz7(i,j,k) =
     .    6.1078d0 * exp (ExpWat*  log(WatIce     /WKxyz5(i,j,k)))
     .             * exp (ExpWa2*(unun/WatIce-unun/WKxyz5(i,j,k)))
C +...    Dudhia (1989) JAS, (B1) and (B2) p.3103
C +       See also Pielke (1984), p.234 and Stull (1988), p.276
 
          qvswDY(i,j,k) = max(eps9  ,       .622d0*WKxyz7(i,j,k)
     .  /(WKxyz6(i,j,k) -                   .378d0*WKxyz7(i,j,k)))
C +...    Saturation Vapor Specific Concentration over Water
C +       (even for temperatures less than freezing point)
 
 
C +--Water Phase Discriminator
C +  =========================
 
          WKxyz7(i,j,k) = max(zero,sign(unun,WKxyz5(i,j,k)-WatIce))
C +...    WKxyz7(i,j,k) =     1    if        Tair     >    273.16
C +                           0    if        Tair     <    273.16
 
 
C +--Saturation Vapor Specific Concentration over Ice
C +  ================================================
 
          qvsiDY(i,j,k) =
     .       max(eps9   , qvswDY(i,j,k) *      WKxyz7(i,j,k)
     .                  + WKxyz8(i,j,k) *(unun-WKxyz7(i,j,k)))
 
 
C +--Work Area Reset
C +  ===============
 
          WKxyz5(i,j,k) = 0.0
          WKxyz6(i,j,k) = 0.0
          WKxyz7(i,j,k) = 0.0
          WKxyz8(i,j,k) = 0.0
        END DO
        END DO
      END DO
 
      return
      end
