 
 
      subroutine interp_subpix(var,var_int,opt,inf,sup,grad_MS)
 
C     + ---------------------------------------------------------- +
C     / Subroutine interp_subpix interpolates MAR variables        /
C     / (inputs of SISVAT) on the subgrid.                         /
C     / Calculates a local gradient for the variable and adjusts   /
C     / the subgrid values using the elevation difference between  /
C     / the pixel and its subpixels.                               /
C     + ---------------------------------------------------------- +
C     /                                                            /
C     / INPUT :  var : MAR variable to be interpolated		   /
C     / ^^^^^^^			                                   /
C     /                                                            /
C     / OUTPUT : var_int : variable interpolated on the subgrid    /
C     / ^^^^^^^  grad_MS : mean local gradient of var              /
C     /			   (with spat. and temp. smoothing)        /
C     + ---------------------------------------------------------- +
C     / /!\ opt = options					   /
C     / 1 = only interpolation (eg: for temperature)		   /
C     / 2 = interpolated value can not be negative                 /
C     /     (eg: humidity, precipitation ...)                      /
C     /                                                            /
C     / inf, sup = lower and upper limits of the gradient          /
C     /                                                            /
C     / dSH_min = min value of elevation difference between        /
C     /           2 pixels to compute the local gradient = 100 m   /
C     /                                                            /
C     /                                                            /
C     / 				Charlotte Lang	13/03/2015 /
C     + ---------------------------------------------------------- +
 
      IMPLICIT NONE
 
      include  "MARCTR.inc"
      include  "MARphy.inc"
 
      include  "MARdim.inc"
      include  "MARgrd.inc"
      include  "MAR_GE.inc"
 
      include  "MAR_RA.inc"
 
      include  "MAR_LB.inc"
      include  "MAR_DY.inc"
      include  "MAR_HY.inc"
c #BW include  "MAR_TE.inc"
      include  "MAR_TU.inc"
 
c #TC include  "MAR_TC.inc"
 
      include  "MAR_SV.inc"
      include  "MARdSV.inc"
      include  "MAR0SV.inc"
 
      include  "MAR_SL.inc"
c #AO include  "MAR_AO.inc"
      include  "MAR_TV.inc"
      include  "MAR_BS.inc"
      include  "MARsSN.inc"
      include  "MAR_IB.inc"
 
      include  "MARsIB.inc"
c #PO include  "MAR_PO.inc"
 
      include  "MAR_WK.inc"
 
 
      integer  n
      real, parameter :: dSH_min= 100
 
      real var(mx,my), var_int(mx,my,mw)
      real delta_sh, delta_var, grad_old(mx,my)
      real grad_var,grad_M(mx,my),grad_MS(mx,my)
 
C + grad_var = gradient between the pixel and one of its 8 surrounding pixels
C + grad_M   = mean value of the local gradient. Average of the 8 surrounding gr
C + grad_MS  = mean gradient after spatial and temporal smoothing
 
 
      real w, q, qi(3,3)
      real inf, sup
 
      integer opt
      real sum_var_int(mx,my), fact(mx,my)
 
 
 
      do i=1,mx; do j=1,my
      do k=1,nsx
       var_int(i,j,k) = var(i,j)
       if(isnan(var(i,j))) then
           write(6,399) iyrrGE,mmarGE,jdarGE,
     .     jhurGE,i,j
399        format('/!\ CL WARNING: VAR is NaN ',i4,'/',i2,'/',
     .            i2,i3,'h, (i,j)=',i4,i4)
           stop
       endif
      enddo
      enddo; enddo
 
 
 
      DO i=2,mx-1; DO j=2,my-1
 
 
      if (itexpe .eq. 0) grad_MS(i,j) = 0.0
      grad_old(i,j)    = grad_MS(i,j)
 
C + grad_old = value of the mean local gradient at the previous time step. Used
 
 
C + *******************************************
C + ***   Computing of the local gradient   ***
C + *******************************************
 
                 w = 0.0
       grad_M(i,j) = 0.0
 
       IF (isolSL(i,j) >=3) then ! Gradient is computed only for land pixels. gr
 
C + Computation of the gradient between each pixel and its 8 surroung pixels (gr
        do m=-1,1; do n=-1,1
 
        grad_var   = 0.0
 
        if (isolSL(i+m,j+n) >=3) then
        delta_sh   = sh(i+m,j+n)-sh(i,j)
         if (abs(delta_sh).gt. dSH_min) then
C +  If no minimum elevation difference for the computation of the gradient, del
C +  to go through the if loop --> If dSH_min = 0.0, decomment next line.
C         if (delta_sh .neq. 0.0) then
         delta_var =  var(i+m,j+n)-var(i,j)
         grad_var  = delta_var/delta_sh
         w         =  w+1.0
         endif
        endif
C + Computation of the mean local gradient
        grad_M(i,j) = grad_M(i,j) + grad_var
 
        enddo; enddo
 
 
        if (w .ne. 0.0) then                 ! w = # of pixels among the 8 surro
        grad_M(i,j) = grad_M(i,j)/w	     ! If w = 0, no pixel among the 8 surrou
        endif
 
       if(isnan(grad_M(i,j))) then
           write(6,400) iyrrGE,mmarGE,jdarGE,
     .     jhurGE,i,j
400        format('/!\ CL WARNING: grad is NaN ',i4,'/',i2,'/',
     .            i2,i3,'h, (i,j)=',i4,i4)
           stop
       endif
 
       ENDIF
      ENDDO; ENDDO
 
C+ ***   Smoothing of the mean local gradient   ***
      DO i=2,mx-1; DO j=2,my-1
 
                  q = 0.0
       grad_MS(i,j) = 0.0
 
       IF (isolSL(i,j) >=3) then
 
C + ***   Spatial smoothing of the gradient   ***
        do m=-1,1; do n=-1,1
        if (isolSL(i+m,j+n) >=3) then
                                       qi(m,n)=1.0
           if(m .eq. 0 .or.  n .eq. 0) qi(m,n)=2.0
           if(m .eq. 0 .and. n .eq. 0) qi(m,n)=4.0
         q            = q+qi(m,n)
         grad_MS(i,j) = grad_MS(i,j)+qi(m,n)*grad_M(i+m,j+n)
        endif
 
        enddo; enddo
 
 
        grad_MS(i,j) = grad_MS(i,j)/q
 
C + ***   Temporal smoothing of the gradient   ***
 
        if (itexpe .ne. 0) then
        grad_MS(i,j) = 0.75*grad_MS(i,j)+0.25*grad_old(i,j)
        endif
 
C + ***   Lower and upper limits of the mean gradient value   ***
        grad_MS(i,j) = max(inf,min(sup,grad_MS(i,j)))
 
C + ********************************************************
C + ***   Interpolation of the variable on the subgrid   ***
C + ********************************************************
        do k=1,nsx-1
        var_int(i,j,k)   = var(i,j)
     .  +grad_MS(i,j)*(sh_int(i,j,k)-sh(i,j))
        enddo
 
        var_int(i,j,nsx) = var(i,j)
 
 
       ENDIF
      ENDDO; ENDDO
 
 
C + *******************
C + ***   Options   ***
C + *******************
 
C + Opt = 1 --> Nothing more than the interpolation is done
C + Opt = 2 --> Interpolated value has to be positive
 
        IF (opt .eq. 2) then
         DO i=1,mx; DO j=1,my
          do k=1,nsx
           var_int(i,j,k) = max(0.0,var_int(i,j,k))
          enddo
         ENDDO; ENDDO
        ENDIF
 
 
      return
      end
