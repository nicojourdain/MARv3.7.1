 
 
      function Qs_CVA(T_Draf,p_star,p__top,sigma,GlacDg)
C +
C +------------------------------------------------------------------------+
C | MAR CONVECTION                                         25-11-1999  MAR |
C |                                                                        |
C |   Function Qs_CVA computes the Saturation Specific Humidity            |
C |                                                                        |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +--Global Variables
C +  ================
C +
      include 'MARphy.inc'
C +
      real     Qs_CVA,GlacDg
      real     T_Draf,p_star,sigma ,p__top
C +
C +
C +--Local  Variables
C +  ================
C +
c #FC real     Qsat_i,Qsat_w
      real     qsat0D
      integer  int_0 ,int_1
      logical  GlaCld
C +
      data int_0   /   0   /
      data int_1   /   1   /
C +
c #CG data GlaCld / .true.     /
C +
C +
C +--Saturation Specific  Humidity
C +  =============================
C +
c #CG IF      (GlaCld)                                            THEN
c #CG          Qsat_i     = qsat0D(T_Draf,sigma,p_star,p__top,int_1)
C +...         ^^^^ Saturation Mixing Ratio over Ice
C +
c #CG          Qsat_w     = qsat0D(T_Draf,sigma,p_star,p__top,int_0)
C +...         ^^^^ Saturation Mixing Ratio over Liquid Water
C +
c #CG          Qs_CVA     = Qsat_i*GlacDg + Qsat_w*(1.0d+0-GlacDg)
C +...         ^^^^ Saturation Mixing Ratio
C +
c #CG ELSE
               Qs_CVA     = qsat0D(T_Draf,sigma,p_star,p__top,int_1)
c #CG END IF
C +
      return
      end
