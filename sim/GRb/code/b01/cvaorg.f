 
 
      subroutine CVAorg
 
C +------------------------------------------------------------------------+
C | MAR CONVECTION                                         15-11-2004  MAR |
C |                                                                        |
C |   SubRoutine CVAorg     dispatches         THE CONVECTIVE ADJUSTMENT   |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C | INPUT (via common block)                                               |
C | ^^^^^^  itexpe: Iteration        Counter, since Experiment Beginning   |
C |         itConv: Adjustment Calls Counter                               |
C |         dt_Loc: Mass Flux convective Scheme: Time Step                 |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
 
C +--Local  Variables
C +  ================
 
      logical CVA_FC,CVA_LA,CVA_KE
 
 
C +--DATA
C +  ====
 
           CVA_FC  =  .false.      ! Fritsch and Chappell scheme
           CVA_LA  =  .false.      ! Bechtold             scheme
           CVA_KE  =  .false.      ! Kerry Emanuel        Scheme
c #FC      CVA_FC  =  .true.
           CVA_LA  =  .true.
c #KE      CVA_KE  =  .true.
 
 
C +--Fritsch and Chappell convective adjustment scheme
C +  =================================================
 
C +                 ***************
c #FC IF  (CVA_FC)  call CVAgen_MAR
C +                 ***************
 
 
C +--Bechtold             convective adjustment scheme
C +  =================================================
 
C +                 ***************
      IF  (CVA_LA)  call CVAgen_MNH
C +                 ***************
 
 
C +--Emanuel              convective adjustment scheme
C +  =================================================
 
C +                 ***************
c #KE IF  (CVA_KE)  call CVAgen_GCM
C +                 ***************
 
      return
      end
