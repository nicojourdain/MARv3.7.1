 
 
      subroutine TIMcor
 
C +------------------------------------------------------------------------+
C | MAR TIME                                               30-11-2000  MAR |
C |   SubRoutine TIMcor computes Corrected Local Times                     |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |    INPUT (via common block)                                            |
C |    ^^^^^  jhurGE     : Universal Time                        (Hour UT) |
C |           itizGE(i,j): Time Zone      of Grid Point (i,j)              |
C |           jhlrGE(i,j): Local     Time at Grid Point (i,j)    (Hour LT) |
C |           mmarGE     : Month                                           |
C |           jdarGE     : Day                                             |
C |                                                                        |
C |   OUTPUT: mmplus     : Month    (corrected)                            |
C |   ^^^^^^^ jdplus     : Day      (corrected)                            |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
 
 
C +--Corrected Time Base
C +  ===================
 
                                              jdplus = jdarGE
       if (jhurGE+itizGE(i,j).lt.jhlrGE(i,j)) jdplus = jdarGE-1
       if (jhurGE+itizGE(i,j).gt.jhlrGE(i,j)) jdplus = jdarGE+1
       if (jdplus.eq.0)                       then
                                              mmplus = mmarGE-1  +12
                                              mmplus = mod(mmplus,12)
                                              jdplus = njmoGE(mmplus)
       else
        if(jdplus.gt.njmoGE(mmarGE))          then
                                              mmplus = mmarGE+1
                                              jdplus =        1
        else
                                              mmplus = mmarGE
        end if
       end if
 
       return
       end
