 
      subroutine DYNadv_dLF_mp(nordAV,ffx,advffx)
 
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS   FAST                                     4-11-2004  MAR |
C |   SubRoutine DYNadv_dLF generates Advection Contribution               |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT/           ffx(mx,my,mz): Advected  Variable                   |
C |   ^^^^^^                                                               |
C |                                                                        |
C |   INPUT/  (via common block)                                           |
C |   ^^^^^^        uairDY(mx,my,mz): Advection Vector: x-----Direction    |
C |                 vairDY(mx,my,mz): Advection Vector: y-----Direction    |
C |                 wsigDY(mx,my,mz): Advection Vector: sigma-Direction    |
C |                                                                        |
C |   OUTPUT        advffx(mx,my,mz): Advection Contribution               |
C |   ^^^^^^                                                               |
C |                                                                        |
C |   METHOD:  2th order accurate Time       Scheme (leapfrog backw.) .AND.|
C |   ^^^^^^  (2th order accurate Horizontal Scheme on Arakawa A grid .OR. |
C |            4th order accurate Horizontal Scheme on Arakawa A grid     )|
C |            2th order          Vertical   Scheme                        |
C |                                                                        |
C |   REFER.:  Use of  A grid: Purser   & Leslie,   1988, MWR 116, p.2069  |
C |   ^^^^^^   Time    Scheme: Haltiner & Williams, 1980, 5-2,     p.152   |
C |            Spatial Scheme: Haltiner & Williams, 1980, 5-6-5,   p.135   |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_DY.inc'
      include 'MAR_WK.inc'
 
      integer  nordAV
 
      real        ffx(mx,my,mz)
      real     advffx(mx,my,mz)
 
 
C +--Contribution to Advection
C +  =========================
!$OMP PARALLEL DO private (i,j,k)
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
            WTxyz8(i,j,k)=     ffx(i,j,k)
          END DO
          END DO
c        END DO
 
 
C +--2th centered Differences / x-----Direction
C +  ------------------------------------------
 
      IF (nordAV.EQ.2)                                              THEN
 
c       DO    k=1,mz
          DO  i=1,mx
          DO  j=1,my
            WTxyz1(i,j,k)=  uairDY(i,j,k)*dxinv2          *  (
     .                 WTxyz8(im1(i),j,k)-WTxyz8(ip1(i),j,k) )
 
          END DO
          END DO
cc #vL   END DO
 
C +--2th centered Differences / y-----Direction
C +  ------------------------------------------
 
cc #vL   DO k=1,mz
          DO j=1,my
          DO i=1,mx
            WTxyz2(i,j,k)=  vairDY(i,j,k)*dyinv2          *  (
     .                 WTxyz8(i,jm1(j),k)-WTxyz8(i,jp1(j),k) )
          END DO
          END DO
c        END DO
 
      ELSE
 
 
C +--4th centered Differences / x-----Direction
C +  ------------------------------------------
 
c        DO    k=1,mz
          DO  i=1,mx
          DO  j=1,my
            WTxyz1(i,j,k)=  uairDY(i,j,k)*dxinv2 * fac43  *  (
     .         0.125 *(WTxyz8(ip2(i),j,k)-WTxyz8(im2(i),j,k))
     .                -WTxyz8(ip1(i),j,k)+WTxyz8(im1(i),j,k) )
          END DO
          END DO
cc #vL   END DO
 
 
C +--4th centered Differences / y-----Direction
C +  ------------------------------------------
 
cc #vL   DO k=1,mz
          DO j=1,my
          DO i=1,mx
            WTxyz2(i,j,k)=  vairDY(i,j,k)*dyinv2 * fac43  *  (
     .         0.125 *(WTxyz8(i,jp2(j),k)-WTxyz8(i,jm2(j),k))
     .                -WTxyz8(i,jp1(j),k)+WTxyz8(i,jm1(j),k) )
          END DO
          END DO
c        END DO
 
      ENDIF
      ENDDO
!$OMP END PARALLEL DO
 
C +--2th centered Differences  / sigma-Direction  / Energy conserving
C +  --- (Haltiner and Williams, 1980, 7.2.2, Eqn. (7-47b) p.220) ---
C +      --------------------------------------------------------
 
!$OMP PARALLEL DO
          DO     j=jp11,my1
 
          DO       k=   1,mz
c            DO     j=jp11,my1
            DO     i=ip11,mx1
              WTxyz6(i,j,k)=      ffx(i,j,k)-        ffx(i,j,kp1(k))
            END DO
c            END DO
         END DO
 
                   k=   1
c            DO     j=jp11,my1
            DO     i=ip11,mx1
              WTxyz3(i,j,k)=   WTxyz6(i,j,k)     *wsigDY(i,j,k)
     .                        *0.5               /dsigm1(1)
            END DO
c           END DO
          DO       k=   2,mz
c            DO     j=jp11,my1
            DO     i=ip11,mx1
              WTxyz3(i,j,k)=  (WTxyz6(i,j,k)     *wsigDY(i,j,k)
     .                        +WTxyz6(i,j,km1(k))*wsigDY(i,j,km1(k)))
     .                        *0.5               /dsigm1(k)
            END DO
c            END DO
          END DO
 
 
C +--Sum of the Contributions
C +  ========================
 
          DO     i=ip11,mx1
c          DO     j=jp11,my1
          DO     k=   1,mz
              advffx(i,j,k) = WTxyz1(i,j,k)+WTxyz2(i,j,k)+WTxyz3(i,j,k)
          END DO
          END DO
          END DO
!$OMP END PARALLEL DO
 
      return
      end
