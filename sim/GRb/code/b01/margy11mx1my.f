 
 
      subroutine MARgy11mx1my
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS                                           05-07-2004  MAR |
C |   SubRoutine MARgy11mx1my performs Gaussian Elimination along y-Dir.   |
C |    (e.g. Pielke (1984), pp.302--303)                                   |
C |    (needed to solve the implicit scheme developped for filtering)      |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:                                                               |
C |   ^^^^^                                                                |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_WK.inc'
 
      integer  ix
 
      data ix /0/
 
      k    =   1
 
 
C +--Forward  Sweep
C +  ==============
 
      IF (ix.ne.1)                                                  THEN
          DO i= 1,mx
            WKxyz5(i,1,k)= WKxyz2(i,1,k)
            WKxyz6(i,1,k)=-WKxyz1(i,1,k)/WKxyz5(i,1,k)
          END DO
          DO j=jp11,my
          DO i=   1,mx
            WKxyz5(i,j,k)= WKxyz3(i,j,k)*WKxyz6(i,j-1,k)+WKxyz2(i,j,k)
            WKxyz6(i,j,k)=-WKxyz1(i,j,k)/WKxyz5(i,j  ,k)
          END DO
          END DO
      END IF
 
          DO i= 1,mx
            WKxyz7(i,1,k)= WKxyz4(i,1,k)/WKxyz5(i,1,k)
          END DO
 
          DO j=jp11,my
          DO i=   1,mx
            WKxyz7(i,j,k)=(WKxyz4(i,j,k)-WKxyz3(i,j,k)*WKxyz7(i,j-1,k))
     .                    /WKxyz5(i,j,k)
          END DO
          END DO
 
 
C +--Backward Sweep
C +  ==============
 
          DO j=my1,1,-1
          DO i=  1,mx
            WKxyz7(i,j,k)=WKxyz6(i,j,k)*WKxyz7(i,j+1,k)+WKxyz7(i,j,k)
          END DO
          END DO
 
      return
      end
