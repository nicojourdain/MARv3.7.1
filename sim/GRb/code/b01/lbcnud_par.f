      subroutine LBCnud_par
C +
C +------------------------------------------------------------------------+
C | MAR DYNAMICS LBC                                       26-09-2001  MAR |
C |   SubRoutine LBCnud_par initialize the implicit numerical scheme       |
C |               for LBC on Wind Component parallel to the Boundary       |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   REFER. : Davies, QJRMS 102, pp.405--418, 1976                        |
C |   ^^^^^^^^                                                             |
C |                                                                        |
C |   INPUT  : vaXX : large scale values of relevant dependant variables   |
C |   ^^^^^^^^   ^X=(x->x axis border, y->y axis border)                   |
C |               ^X=(g->x small, d->x large, b->y small, h->y large)      |
C |                                                                        |
C |   OUTPUT : wiXX : coefficient used in semi-implicit numerical scheme   |
C |   ^^^^^^^^ tiXX : independant term of semi-implicit numerical scheme   |
C |              ^X=(x->x axis border--variable v,                         |
C |                  y->y axis border--variable u)                         |
C |               ^X=(g->x small, d->x large, b->y small, h->y large)      |
C |                                                                        |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +--Global Variables
C +  ================
C +
      include 'MARCTR.inc'
      include 'MARphy.inc'
C +
      include 'MARdim.inc'
      include 'MARgrd.inc'
C +
      include 'MAR_LB.inc'
C +
C +
C +--Local  Variables
C +  ================
C +
      integer  il,ic,ii,nn,n2,n3,n4,lmin,lmax,jl,jc,iv_nup,n1
      real     wkxd(mx-n6:mx1,mx-n6:mx1)
C +
C +
C +--Matrix Inversion for x large (Reference Boundary)
C +  =================================================
C +
      IF   (iterun.eq.0)                                          THEN
C +
        IF (mmx.gt.1)                                             THEN
C +
          DO il=mx-n6,mx1
          DO ic=mx-n6,mx1
            wkxd(il,ic) = 0.d0
          END DO
          END DO
C +
          DO ii=mx-n6,mmx2
            wkxd(ii,ii+1)  =                  rxLB(ii+1) -rxLB(ii)
          END DO
          DO ii=mx-n6,mmx1
            wkxd(ii,ii  )  = 1 + 2*rxLB(ii) + rxLB(ii+1) -rxLB(ii-1)
          END DO
c #OB       wkxd(mx1,mx1)  = 1              + rxLB(mx  ) -rxLB(mmx2)
          DO ii=mx-n6+1,mmx1
            wkxd(ii,ii-1)  =       rxLB(ii)
          END DO
          DO nn=1,n6-3
             n2=nn+1
             n3=nn+2
             n4=n6-1-nn
          DO ii=mx-n4,mmx1
            wkxd(ii,ii-n2) =       rxLB(ii-nn)-rxLB(ii-n3)
          END DO
          END DO
C +
            wkxd(mx1,mx-n6)=       rxLB(mmx5)
C +
          lmin=mx-n6
          lmax=mx1
C +
C +       ***********
          call matinv(wkxd,wixdLB,lmin,lmax)
C +       ***********
C +
C +--Inverted Matrices at Other Boundaries
C +  =====================================
C +
          DO il=2,n7
          DO ic=2,n7
            wixgLB(il,ic) = wixdLB(mx+1-il,mx+1-ic)
          END DO
          END DO
C +...    x small
C +
        END IF
C +
        IF (mmy.gt.1)                                             THEN
C +
          DO jl=2,n7
          DO jc=2,n7
            wiyiLB(jl,jc) = wixgLB(jl,jc)
          END DO
          END DO
C +...    y small
C +
          DO jl=2,n7
          DO jc=2,n7
          wiysLB(my+1-jl,my+1-jc) = wiyiLB(jl,jc)
          END DO
          END DO
C +...    y large
C +
        END IF
      END IF
C +
C +
C +--Independant Terms (Constant Coefficients)
C +  =========================================
C +
C +--x Boundaries
C +  ------------
C +
      IF (mmx.gt.1)                                               THEN
C +
             iv_nup= 2
        DO   k = 1,mz
          DO j = 1,my
C +
C +--x large
C +  ~~~~~~~
            DO i=mx-n6,mmx1
             tixdLB(i,j,k) = rxLB(n50xLB) *vaxdLB(mx-n6 ,j,k,iv_nup)
            END DO
            DO i=mx-n6,mmx2
             tixdLB(i,j,k) = tixdLB(i,j,k) +(rxLB(i+1)
     .        -rxLB(i)  )   *vaxdLB(i+1   ,j,k,iv_nup)
            END DO
            DO i=mx-n6+1,mmx2
             tixdLB(i,j,k) = tixdLB(i,j,k) +(2*rxLB(i)+rxLB(i+1)
     .        -rxLB(i-1))   *vaxdLB(i     ,j,k,iv_nup)
            END DO
            DO i=mx-n6+2,mmx1
             tixdLB(i,j,k) = tixdLB(i,j,k)
     .       + rxLB(n40xLB) *vaxdLB(n50xLB,j,k,iv_nup)
            END DO
C +
            DO nn=n6-4,n6-3
               n1=nn+1
               n2=nn+2
            DO i =mx-nn,mmx1
             tixdLB(i,j,k) = tixdLB(i,j,k)
     .       +(rxLB(mx-nn)-rxLB(mx-n2)      )*vaxdLB(mx-n1 ,j,k,iv_nup)
            END DO
            END DO
C +
c #OB       IF (openLB)                                           THEN
c #OB        tixdLB(mx1,j,k) = tixdLB(mx1,j,k)
c #OB.       +(rxLB(mmx1)-rxLB(mmx3))*vaxdLB(mmx2  ,j,k,iv_nup)
c #OB.      +3*rxLB(mmx1)            *vaxdLB(mmx1  ,j,k,iv_nup)
c #OB       ELSE
             tixdLB(mx1,j,k) = tixdLB(mx1,j,k)
     .       +(rxLB(mmx1)-rxLB(mmx3))*vaxdLB(mmx2  ,j,k,iv_nup)
     .      +2*rxLB(mmx1)            *vaxdLB(mmx1  ,j,k,iv_nup)
c #OB       END IF
C +
C +--x small
C +  ~~~~~~~
            DO i=n7mxLB,2  ,-1
              tixgLB(i,j,k) = rxLB(n6mxLB) *vaxgLB(n7mxLB,j,k,iv_nup)
            END DO
            DO i=n7mxLB,3  ,-1
              tixgLB(i,j,k) = tixgLB(i,j,k) +(  rxLB(i-1)
     .         -rxLB(i)  )   *vaxgLB(i-1   ,j,k,iv_nup)
            END DO
            DO i=n6   ,3  ,-1
              tixgLB(i,j,k) = tixgLB(i,j,k) +(2*rxLB(i)+rxLB(i-1)
     .         -rxLB(i+1))   *vaxgLB(i     ,j,k,iv_nup)
            END DO
            DO i=n6-1 ,2  ,-1
              tixgLB(i,j,k) = tixgLB(i,j,k)
     .         +rxLB(n5mxLB) *vaxgLB(n6mxLB,j,k,iv_nup)
            END DO
C +
            DO nn=n6-3,n6-2
               n1=nn+1
               n2=nn+2
            DO  i=nn  ,2   ,-1
              tixgLB(i,j,k) = tixgLB(i,j,k)
     .        +(rxLB(nn  )-rxLB(n2))  *vaxgLB(n1  ,j,k,iv_nup)
            END DO
            END DO
C +
c #OB       IF (openLB)                                           THEN
c #OB         tixgLB(2,j,k) = tixgLB(2,j,k)
c #OB.        +(rxLB(m0x2)-rxLB(m0x4))*vaxgLB(m0x3,j,k,iv_nup)
c #OB.       +3*rxLB(m0x2)            *vaxgLB(m0x2,j,k,iv_nup)
c #OB       ELSE
              tixgLB(2,j,k) = tixgLB(2,j,k)
     .        +(rxLB(m0x2)-rxLB(m0x4))*vaxgLB(m0x3,j,k,iv_nup)
     .       +2*rxLB(m0x2)            *vaxgLB(m0x2,j,k,iv_nup)
c #OB       END IF
          END DO
C +
        END DO
C +
      END IF
C +
C +
C +--y Boundaries
C +  ------------
C +
      IF (mmy.gt.1)                                               THEN
C +
             iv_nup = 1
        DO   k=1,mz
          DO i=1,mx
C +
C +--y large
C +  ~~~~~~~
            DO j=my-n6,mmy1
              tiysLB(i,j,k) =   ryLB(n50yLB)
     .                       *vaysLB(i,my-n6myLB,k,iv_nup)
            END DO
            DO j=my-n6,mmy2
              tiysLB(i,j,k) = tiysLB(i,j  ,k) +(ryLB(j+1)
     .         -ryLB(j)  )   *vaysLB(i,j+1,      k,iv_nup)
            END DO
            DO j=my-n6+1,mmy2
              tiysLB(i,j,k) = tiysLB(i,j,k) +(2*ryLB(j)+ryLB(j+1)
     .         -ryLB(j-1))   *vaysLB(i,j        ,k,iv_nup)
            END DO
            DO j=my-n6+2,mmy1
              tiysLB(i,j,k) = tiysLB(i,j,k)
     .         +ryLB(n40yLB) *vaysLB(i,my-n6+1  ,k,iv_nup)
            END DO
C +
            DO nn=n6-4,n6-3
               n1=nn+1
               n2=nn+2
            DO j =my-3,mmy1
              tiysLB(i,j,k) = tiysLB(i,j,k)
     .        +(ryLB(my-nn)-ryLB(my-n2))*vaysLB(i,my-n1  ,k,iv_nup)
            END DO
            END DO
C +
c #OB       IF (openLB)                                           THEN
c #OB         tiysLB(i,my1,k) = tiysLB(i,my1,k)
c #OB.        +(ryLB(mmy1)-ryLB(mmy3))*vaysLB(i,mmy2   ,k,iv_nup)
c #OB.       +3*ryLB(mmy1)            *vaysLB(i,mmy1   ,k,iv_nup)
c #OB       ELSE
              tiysLB(i,my1,k) = tiysLB(i,my1,k)
     .        +(ryLB(mmy1)-ryLB(mmy3))*vaysLB(i,mmy2   ,k,iv_nup)
     .       +2*ryLB(mmy1)            *vaysLB(i,mmy1   ,k,iv_nup)
c #OB       END IF
C +
C +--y small
C +  ~~~~~~~
            DO j=n7   ,2  ,-1
              tiyiLB(i,j,k) = ryLB(n6myLB)   *vayiLB(i,n7myLB,k,iv_nup)
            END DO
            DO j=n7   ,3  ,-1
              tiyiLB(i,j,k) = tiyiLB(i,j,k) +(           ryLB(j-1)
     .         -ryLB(j)  )   *vayiLB(i,j-1   ,k,iv_nup)
            END DO
            DO j=n6  ,3  ,-1
              tiyiLB(i,j,k) = tiyiLB(i,j,k) +(2*ryLB(j) +ryLB(j-1)
     .         -ryLB(j+1))   *vayiLB(i,j     ,k,iv_nup)
            END DO
            DO j=n6-1,2  ,-1
              tiyiLB(i,j,k) = tiyiLB(i,j,k)
     .        + ryLB(n5myLB) *vayiLB(i,n6myLB,k,iv_nup)
            END DO
C +
            DO nn=n6-3,n6-2
             n1=nn+1
             n2=nn+2
            DO j=nn  ,2  ,-1
              tiyiLB(i,j,k) = tiyiLB(i,j,k)
     .        +(ryLB(nn  )-ryLB(n2))*vayiLB(i,n2  ,k,iv_nup)
            END DO
            END DO
C +
             j=2
c #OB       IF (openLB)                                           THEN
c #OB         tiyiLB(i,2,k) = tiyiLB(i,2,k)
c #OB.        +(ryLB(m0y2)-ryLB(m0y4))*vayiLB(i,m0y3,k,iv_nup)
c #OB.       +3*ryLB(m0y2)            *vayiLB(i,m0y2,k,iv_nup)
c #OB       ELSE
              tiyiLB(i,2,k) = tiyiLB(i,2,k)
     .        +(ryLB(m0y2)-ryLB(m0y4))*vayiLB(i,m0y3,k,iv_nup)
     .       +2*ryLB(m0y2)            *vayiLB(i,m0y2,k,iv_nup)
c #OB       END IF
          END DO
        END DO
C +
      END IF
C +
      return
      end
