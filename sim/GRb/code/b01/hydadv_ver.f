 
 
      subroutine HYDadv_ver
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS   SLOW                                    18-09-2001  MAR |
C |   SubRoutine HYDadv_ver  includes the Vertical  Advection Contribution |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT   (via common block)                                           |
C |   ^^^^^    iterun  : Run   Iteration Counter                           |
C |              qvDY, qwHY, qrHY, qiHY, ccniHY, qsHY / Time Step n        |
C |              qwHY  : Cloud Droplets Concentration             (kg/kg)  |
C |              qrHY  : Rain  Drops    Concentration             (kg/kg)  |
C |              qvDY  : Water Vapor    Concentration             (kg/kg)  |
C |              qiHY  : Ice   Crystals Concentration             (kg/kg)  |
C |            ccniHY  : Ice   Crystals Number                             |
C |              qsHY  : Snow  Flakes   Concentration             (kg/kg)  |
C |                                                                        |
C |   OUTPUT :   qvDY, qwHY, qrHY, qiHY, ccniHY, qsHY / Time Step n+1      |
C |   ^^^^^^^^                                                             |
C |                                                                        |
C |   METHOD:  Unstaggered Grid: 1st Accurate in Space Upstream Scheme     |
C |   ^^^^^^^^   Staggered Grid: 2nd Accurate in Space                     |
C |                                                                        |
C | # OPTIONS: #VA: Vertical Average preferred in Centered Conserv Scheme  |
C | # ^^^^^^^^ #NS: NO   Slip Surface BC used  in Centered Conserv Scheme  |
C | #          #WF: Water Conservation  along the Vertical                 |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_DY.inc'
c #cA include 'MAR_CA.inc'
      include 'MAR_HY.inc'
      include 'MAR_SL.inc'
 
      include 'MAR_WK.inc'
 
c #WA integer                  nadvrd
c #WA common  /DYNadv_ver_loc/ nadvrd
 
      logical  centrL
c #ZU logical  adv3rd
c #ZU real     gat(mx,my,mz),ga0(mx,my)
 
 
C +--Local  Variables
C +  ================
 
      integer  ntimax,itimax
 
      real     cflmax,cflsig,faccfl,dsgm
      real     old__u,old__v,old__t
      real     qw_0  ,qr_0  ,qv_0  ,qi_0  ,ccni_0,qs_0
c #WF real     qwVC  ,qrVC  ,qvVC  ,qiVC  ,ciVC  ,qsVC
c #BS real     dh_sno
 
 
C +--DATA
C +  ====
 
c #ZU data adv3rd/ .true./
 
      data centrL/ .true./
c #UP      centrL=.false.
 
 
C +--Slip condition for Mountain Wave Experiments
C +  ============================================
 
c #OM   DO       j=jp11,my1
c #OM   DO       i=ip11,mx1
c #OM     psigDY(i,j,mz)=0.0
c #OM   END DO
c #OM   END DO
 
 
C +--First and Second Order Schemes
C +  ==============================
 
c #ZU IF (.not.adv3rd)                                            THEN
 
 
C +--Courant Number
C +  --------------
 
           cflmax     = 0.0
 
C +--Centered second Order Scheme on a       staggered Grid
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
       IF (centrL.and.staggr)                                     THEN
 
c #WA     write(6,6001)iterun
 6001     format(i6,' 6001       centrL .and.       staggr /CFL Number')
 
          DO       k=1,mz
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz7(i,j,k)=dt*psigDY(i,j,k)
     .                     /(pstDYn(i,j)*dsigm1(k)*2.0)
            cflsig     = abs(WKxyz7(i,j,k)+WKxyz7(i,j,k))
            cflmax     = max(cflsig,cflmax)
          END DO
          END DO
          END DO
 
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz8(i,j,1)=                 0.00
          END DO
          END DO
 
          DO       k=kp1(1),mz
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz8(i,j,k)=dt*psigDY(i,j,km1(k))
     .                     /(pstDYn(i,j)*dsigm1(k)*2.0)
            cflsig     = abs(WKxyz8(i,j,k)+WKxyz8(i,j,k))
            cflmax     = max(cflsig,cflmax)
          END DO
          END DO
          END DO
 
       ELSE
 
C +--Upstream first  Order Scheme on a       staggered Grid
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        IF           (staggr)                                     THEN
 
c #WA     write(6,6002)iterun
 6002     format(i6,' 6002 .not. centrL .and.       staggr /Wind Speed')
 
          DO       k=kp1(1),mz
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz8(i,j,k)=(psigDY(i,j,k-1)*dsig_1(k-1)
     .                    +psigDY(i,j,k)  *dsig_1(k))
     .                   /(dsig_1    (k-1)+dsig_1(k))
          END DO
          END DO
          END DO
 
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz8(i,j,1)= psigDY(i,j,1)  *dsig_1(1)
     .                   /(dsig_1    (0)  +dsig_1(1))
          END DO
          END DO
 
C +--Upstream first  Order Scheme on a  non  staggered Grid
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ELSE
 
c #WA     write(6,6003)iterun
 6003     format(i6,' 6003 (.not.)centrL.and. .not. staggr /Wind Speed')
 
          DO       k=1,mz
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz8(i,j,k)= psigDY(i,j,k)
          END DO
          END DO
          END DO
 
        END IF
 
C +--Centered second Order Scheme on a  non  staggered Grid
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        IF(centrL)                                                THEN
 
c #WA     write(6,6004)iterun
 6004     format(i6,' 6004        centrL.and. .not. staggr /CFL Number')
 
          DO       k=1,mz
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz7(i,j,k)=dt*WKxyz8(i,j,k)
     .                     /(pstDYn(i,j)*dsigm1(k)*2.0)
            cflsig     = abs(WKxyz7(i,j,k))
            cflmax     = max(cflsig,cflmax)
          END DO
          END DO
          END DO
 
C +--Upstream first  Order Scheme on a (non) staggered Grid
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ELSE
 
c #WA     write(6,6005)iterun
 6005     format(i6,' 6005  .not. centrL.and.(.not.)staggr /CFL Number')
 
          DO          k=    1 ,mmz1
          DO          j=jp11,my1
          DO          i=ip11,mx1
            IF(WKxyz8(i,j,k).gt.0.0)                              THEN
               WKxyz7(i,j,k)=-dt*WKxyz8(i,j,k)/(pstDYn(i,j)*dsig_1(k-1))
            ELSE
               WKxyz7(i,j,k)=-dt*WKxyz8(i,j,k)/(pstDYn(i,j)*dsig_1(k  ))
            END IF
               cflsig       =abs(WKxyz7(i,j,k))
               cflmax       =max(cflsig,cflmax)
          END DO
          END DO
          END DO
 
                      k=  mz
          DO          j=jp11,my1
          DO          i=ip11,mx1
            IF(WKxyz8(i,j,k).gt.0.0)                              THEN
               WKxyz7(i,j,k)=-dt*WKxyz8(i,j,k)/(pstDYn(i,j)*dsig_1(k-1))
            ELSE
               WKxyz7(i,j,k)=-dt*WKxyz8(i,j,k)/(pstDYn(i,j)*dsig_1(k  ))
            END IF
               cflsig       =abs(WKxyz7(i,j,k))
               cflmax       =max(cflsig,cflmax)
          END DO
          END DO
 
          DO          j=1,my
          DO          i=1,mx
               WKxyz7(i,j,1)= 0.0
          END DO
          END DO
 
C +--Work Array Reset
C +  ~~~~~~~~~~~~~~~~
          DO          k=1,mz
          DO          j=1,my
          DO          i=1,mx
               WKxyz8(i,j,k)= 0.0
          END DO
          END DO
          END DO
 
        END IF
 
       END IF
 
 
C +--Set Up    of the Local Split Time Differencing
C +  ----------------------------------------------
 
            cflmax    = 2.0  *cflmax
C +...      restricted CFL Criterion
 
            ntimax    =       cflmax
       IF  (centrL)                                               THEN
            ntimax    = max(2,ntimax)
c #WA     write(6,6006)ntimax
 6006     format(i6,' 6006        centrL.and.(.not.)staggr /Nb Iterat.')
       ELSE
            ntimax    = max(1,ntimax)
c #WA     write(6,6007)ntimax
 6007     format(i6,' 6007  .not. centrL.and.(.not.)staggr /Nb Iterat.')
       END IF
 
C +--Update of CFL Number
C +  ~~~~~~~~~~~~~~~~~~~~
       IF  (ntimax.gt.1)                                          THEN
            faccfl       = 1.0d+0        / ntimax
          DO       k=1,mz
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz7(i,j,k)= WKxyz7(i,j,k) * faccfl
            WKxyz8(i,j,k)= WKxyz8(i,j,k) * faccfl
          END DO
          END DO
          END DO
       END IF
 
C +--OUTPUT for Verification
C +  ~~~~~~~~~~~~~~~~~~~~~~~
c #WA  nadvrd      = nadvrd                 + 1
c #WA  write(6,6000) nadvrd,cflmax,ntimax
 6000  format(i6,' CFLmax ',3x,' ',3x,'  =',f7.4,
     .        6x,' ntimax ',8x,  '   =',i4)
 
 
C +--Warm Water Conservation
C +  -----------------------
 
c #WF  DO j=1,my
c #WF  DO i=1,mx
c #WF      WKxy4(i,j) = 0.0d+0
c #WF      WKxy5(i,j) = 0.0d+0
c #WF      WKxy6(i,j) = 0.0d+0
c #WF      WKxy7(i,j) = 0.0d+0
c #WF      WKxy8(i,j) = 0.0d+0
c #WF      WKxy9(i,j) = 0.0d+0
c #WF    DO k=1,mz
c #WF      WKxy4(i,j) = WKxy4(i,j) + dsigm1(k)*qvDY(i,j,k)
c #WF      WKxy5(i,j) = WKxy5(i,j) + dsigm1(k)*qwHY(i,j,k)
c #WF      WKxy6(i,j) = WKxy6(i,j) + dsigm1(k)*qrHY(i,j,k)
c #WF      WKxy7(i,j) =         max(WKxy7(i,j),qvDY(i,j,k))
c #WF      WKxy8(i,j) =         max(WKxy8(i,j),qwHY(i,j,k))
c #WF      WKxy9(i,j) =         max(WKxy9(i,j),qrHY(i,j,k))
c #WF    END DO
c #WF  END DO
c #WF  END DO
 
 
C +--Warm Water
C +--Start Vertical Advection
C +  ------------------------
 
       IF   (centrL)                                              THEN
 
        IF  (staggr)                                              THEN
 
c #WA     write(6,6008)
 6008     format(6x,' 6008        centrL.and.       staggr /A Contrib.')
 
C +--2nd Order Centered Energy conserving:  Local Split Time Differencing
C +  ~~~~~~~~~ (Haltiner & Williams 1980 7.2.2, (7-47b) p.220) ~~~~~~~~~~
C +            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
         DO  itimax=    1,ntimax
 
C +--First        internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          IF(itimax.eq. 1)                                        THEN
 
            DO       j=jp11,my1
 
C +--Vertical Differences
 
                      k=    1
                dsgm         =             2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzp(i,k)   =   qwHY(i,j,k)
                WKxzq(i,k)   =   qrHY(i,j,k)
                WKxzx(i,k)   =   qvDY(i,j,k)
 
c #VA           WKxzp(i,k)   =(  qwHY(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +  qwHY(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzq(i,k)   =(  qrHY(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +  qrHY(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzx(i,k)   =(  qvDY(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +  qvDY(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
 
              DO      i=ip11,mx1
                  qw_0       =                  WKxzp(i,  k)
                WKxza(i,k)   =( WKxzp(i,  k)  -  qw_0)
                  qr_0       =                  WKxzq(i,  k)
                WKxzb(i,k)   =( WKxzq(i,  k)  -  qr_0)
                  qv_0       =                  WKxzx(i,  k)
                WKxzc(i,k)   =( WKxzx(i,  k)  -  qv_0)
              END DO
 
              DO      k=kp1(1),mmz1
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzp(i,k)   =   qwHY(i,j,k)
                WKxzq(i,k)   =   qrHY(i,j,k)
                WKxzx(i,k)   =   qvDY(i,j,k)
 
c #VA           WKxzp(i,k)   =(  qwHY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +  qwHY(i,j,k  )*dsigm1(k  ) *2.0d+0
c #VA.                        +  qwHY(i,j,k+1)*dsigm1(k+1)        )/dsgm
c #VA           WKxzq(i,k)   =(  qrHY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +  qrHY(i,j,k  )*dsigm1(k  ) *2.0d+0
c #VA.                        +  qrHY(i,j,k+1)*dsigm1(k+1)        )/dsgm
c #VA           WKxzx(i,k)   =(  qvDY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +  qvDY(i,j,k  )*dsigm1(k  ) *2.0d+0
c #VA.                        +  qvDY(i,j,k+1)*dsigm1(k+1)        )/dsgm
              END DO
              END DO
 
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =( WKxzp(i,  k)  - WKxzp(i,  k-1))
                WKxzb(i,k)   =( WKxzq(i,  k)  - WKxzq(i,  k-1))
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
              END DO
 
                      k=       mmz
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)
              DO      i=ip11,mx1
                WKxzp(i,k)   =   qwHY(i,j,k)
                WKxzq(i,k)   =   qrHY(i,j,k)
                WKxzx(i,k)   =   qvDY(i,j,k)
 
c #VA           WKxzp(i,k)   =(  qwHY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +  qwHY(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
c #VA           WKxzq(i,k)   =(  qrHY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +  qrHY(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
c #VA           WKxzx(i,k)   =(  qvDY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +  qvDY(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
              END DO
 
              DO      i=ip11,mx1
                WKxza(i,k)   =( WKxzp(i,  k)  - WKxzp(i,  k-1))
                WKxzb(i,k)   =( WKxzq(i,  k)  - WKxzq(i,  k-1))
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
 
                      k=       mzz
              DO      i=ip11,mx1
                WKxy1(i,j)   = 0.0d+0
                WKxy2(i,j)   = 0.0d+0
                WKxy3(i,j)   = 0.0d+0
c #NS           WKxy1(i,j)   =                - WKxzp(i,  k-1)
c #NS           WKxy2(i,j)   =                - WKxzq(i,  k-1)
c #NS           WKxy3(i,j)   =(qvapSL(i,j)    - WKxzx(i,  k-1))
              END DO
 
C +--Advection Contribution
 
              DO       k=    1 ,mmz1
              DO       i=ip11,mx1
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxza(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxza(i,k)
                WKxyz1(i,j,k)=   qwHY(i,j,k)  -WKxzd(i,k)
                WKxyz4(i,j,k)=   qwHY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
 
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxzb(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxzb(i,k)
                WKxyz2(i,j,k)=   qrHY(i,j,k)  -WKxzd(i,k)
                WKxyz5(i,j,k)=   qrHY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
 
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxzc(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxzc(i,k)
                WKxyz3(i,j,k)=   qvDY(i,j,k)  -WKxzd(i,k)
                WKxyz6(i,j,k)=   qvDY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
              END DO
              END DO
 
                       k=       mmz
              DO       i=ip11,mx1
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy1(i,j)
     .                        +WKxyz8(i,j,k)  *WKxza(i,k)
                WKxyz1(i,j,k)=   qwHY(i,j,k)  -WKxzd(i,k)
                WKxyz4(i,j,k)=   qwHY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
 
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy2(i,j)
     .                        +WKxyz8(i,j,k)  *WKxzb(i,k)
                WKxyz2(i,j,k)=   qrHY(i,j,k)  -WKxzd(i,k)
                WKxyz5(i,j,k)=   qrHY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
 
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy3(i,j)
     .                        +WKxyz8(i,j,k)  *WKxzc(i,k)
                WKxyz3(i,j,k)=   qvDY(i,j,k)  -WKxzd(i,k)
                WKxyz6(i,j,k)=   qvDY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
              END DO
 
            END DO
 
C +--Intermediary internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          ELSE IF (itimax.lt.ntimax)                              THEN
 
C +--Vertical Differences
 
            DO       j=jp11,my1
 
                      k=    1
                dsgm         =             2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzp(i,k)   = WKxyz4(i,j,k)
                WKxzq(i,k)   = WKxyz5(i,j,k)
                WKxzx(i,k)   = WKxyz6(i,j,k)
 
c #VA           WKxzp(i,k)   =(WKxyz4(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz4(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzq(i,k)   =(WKxyz5(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz5(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz6(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
 
              DO      i=ip11,mx1
                  qw_0       =                  WKxzp(i,  k)
                WKxza(i,k)   =( WKxzp(i,  k)  -  qw_0)
                  qr_0       =                  WKxzq(i,  k)
                WKxzb(i,k)   =( WKxzq(i,  k)  -  qr_0)
                  qv_0       =                  WKxzx(i,  k)
                WKxzc(i,k)   =( WKxzx(i,  k)  -  qv_0)
              END DO
 
              DO      k=kp1(1),mmz1
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzp(i,k)   = WKxyz4(i,j,k)
                WKxzq(i,k)   = WKxyz5(i,j,k)
                WKxzx(i,k)   = WKxyz6(i,j,k)
 
c #VA           WKxzp(i,k)   =(WKxyz4(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz4(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz4(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzq(i,k)   =(WKxyz5(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz5(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz5(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz6(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz6(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
              END DO
 
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =( WKxzp(i,  k)  - WKxzp(i,  k-1))
                WKxzb(i,k)   =( WKxzq(i,  k)  - WKxzq(i,  k-1))
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
              END DO
 
                      k=       mmz
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)
              DO      i=ip11,mx1
                WKxzp(i,k)   = WKxyz4(i,j,k)
                WKxzq(i,k)   = WKxyz5(i,j,k)
                WKxzx(i,k)   = WKxyz6(i,j,k)
 
c #VA           WKxzp(i,k)   =(WKxyz4(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz4(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
c #VA           WKxzq(i,k)   =(WKxyz5(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz5(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz6(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
              END DO
 
              DO      i=ip11,mx1
                WKxza(i,k)   =( WKxzp(i,  k)  - WKxzp(i,  k-1))
                WKxzb(i,k)   =( WKxzq(i,  k)  - WKxzq(i,  k-1))
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
 
                      k=       mzz
              DO      i=ip11,mx1
                WKxy1(i,j)   = 0.0d+0
                WKxy2(i,j)   = 0.0d+0
                WKxy3(i,j)   = 0.0d+0
c #NS           WKxy1(i,j)   =                - WKxzp(i,  k-1)
c #NS           WKxy2(i,j)   =                - WKxzq(i,  k-1)
c #NS           WKxy3(i,j)   =(qvapSL(i,j)    - WKxzx(i,  k-1))
              END DO
 
C +--Advection Contribution
 
              DO      k=    1 ,mmz1
              DO      i=ip11,mx1
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxza(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxza(i,k)
                old__u       = WKxyz1(i,j,k)
                WKxyz1(i,j,k)= WKxyz4(i,j,k)
                WKxyz4(i,j,k)= old__u        -(WKxzd(i,k)+WKxzd(i,k))
 
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxzb(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxzb(i,k)
                old__v       = WKxyz2(i,j,k)
                WKxyz2(i,j,k)= WKxyz5(i,j,k)
                WKxyz5(i,j,k)= old__v        -(WKxzd(i,k)+WKxzd(i,k))
 
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxzc(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxzc(i,k)
                old__t       = WKxyz3(i,j,k)
                WKxyz3(i,j,k)= WKxyz6(i,j,k)
                WKxyz6(i,j,k)= old__t        -(WKxzd(i,k)+WKxzd(i,k))
              END DO
              END DO
 
                      k=       mmz
              DO      i=ip11,mx1
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy1(i,j)
     .                        +WKxyz8(i,j,k)  *WKxza(i,k)
                old__u       = WKxyz1(i,j,k)
                WKxyz1(i,j,k)= WKxyz4(i,j,k)
                WKxyz4(i,j,k)= old__u        -(WKxzd(i,k)+WKxzd(i,k))
 
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy2(i,j)
     .                        +WKxyz8(i,j,k)  *WKxzb(i,k)
                old__v       = WKxyz2(i,j,k)
                WKxyz2(i,j,k)= WKxyz5(i,j,k)
                WKxyz5(i,j,k)= old__v        -(WKxzd(i,k)+WKxzd(i,k))
 
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy3(i,j)
     .                        +WKxyz8(i,j,k)  *WKxzc(i,k)
                old__t       = WKxyz3(i,j,k)
                WKxyz3(i,j,k)= WKxyz6(i,j,k)
                WKxyz6(i,j,k)= old__t        -(WKxzd(i,k)+WKxzd(i,k))
              END DO
 
            END DO
 
C +--Last         internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          ELSE
 
            DO       j=jp11,my1
 
C +--Vertical Differences
 
                      k=    1
                dsgm         =             2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzp(i,k)   = WKxyz4(i,j,k)
                WKxzq(i,k)   = WKxyz5(i,j,k)
                WKxzx(i,k)   = WKxyz6(i,j,k)
 
c #VA           WKxzp(i,k)   =(WKxyz4(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz4(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzq(i,k)   =(WKxyz5(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz5(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz6(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
 
              DO      i=ip11,mx1
                  qw_0       =                  WKxzp(i,  k)
                WKxza(i,k)   =( WKxzp(i,  k)  -  qw_0)
                  qr_0       =                  WKxzq(i,  k)
                WKxzb(i,k)   =( WKxzq(i,  k)  -  qr_0)
                  qv_0       =                  WKxzx(i,  k)
                WKxzc(i,k)   =( WKxzx(i,  k)  -  qv_0)
              END DO
 
              DO      k=kp1(1),mmz1
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzp(i,k)   = WKxyz4(i,j,k)
                WKxzq(i,k)   = WKxyz5(i,j,k)
                WKxzx(i,k)   = WKxyz6(i,j,k)
 
c #VA           WKxzp(i,k)   =(WKxyz4(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz4(i,j,k  )*dsigm1(k  ) *2.0d+0
c #VA.                        +WKxyz4(i,j,k+1)*dsigm1(k+1)        )/dsgm
c #VA           WKxzq(i,k)   =(WKxyz5(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz5(i,j,k  )*dsigm1(k  ) *2.0d+0
c #VA.                        +WKxyz5(i,j,k+1)*dsigm1(k+1)        )/dsgm
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz6(i,j,k  )*dsigm1(k  ) *2.0d+0
c #VA.                        +WKxyz6(i,j,k+1)*dsigm1(k+1)        )/dsgm
              END DO
              END DO
 
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =( WKxzp(i,  k)  - WKxzp(i,  k-1))
                WKxzb(i,k)   =( WKxzq(i,  k)  - WKxzq(i,  k-1))
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
              END DO
 
                      k=       mmz
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)
              DO      i=ip11,mx1
                WKxzp(i,k)   = WKxyz4(i,j,k)
                WKxzq(i,k)   = WKxyz5(i,j,k)
                WKxzx(i,k)   = WKxyz6(i,j,k)
 
c #VA           WKxzp(i,k)   =(WKxyz4(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz4(i,j,k  )*dsigm1(k  ) *2.0d+0)/dsgm
c #VA           WKxzq(i,k)   =(WKxyz5(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz5(i,j,k  )*dsigm1(k  ) *2.0d+0)/dsgm
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz6(i,j,k  )*dsigm1(k  ) *2.0d+0)/dsgm
              END DO
 
              DO      i=ip11,mx1
                WKxza(i,k)   =( WKxzp(i,  k)  - WKxzp(i,  k-1))
                WKxzb(i,k)   =( WKxzq(i,  k)  - WKxzq(i,  k-1))
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
 
                      k=       mzz
              DO      i=ip11,mx1
                WKxy1(i,j)   = 0.0d+0
                WKxy2(i,j)   = 0.0d+0
                WKxy3(i,j)   = 0.0d+0
c #NS           WKxy1(i,j)   =                - WKxzp(i,  k-1)
c #NS           WKxy2(i,j)   =                - WKxzq(i,  k-1)
c #NS           WKxy3(i,j)   =(qvapSL(i,j)    - WKxzx(i,  k-1))
              END DO
 
C +--Hydromet.Advection
 
              DO             k=    1 ,mmz1
              DO           i=ip11,mx1
                      qwHY(i,j,k) = WKxyz1(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxza(i,k+1)
     .                             +WKxyz8(i,j,k)  *WKxza(i,k  ))
                      qrHY(i,j,k) = WKxyz2(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxzb(i,k+1)
     .                             +WKxyz8(i,j,k)  *WKxzb(i,k  ))
              END DO
              END DO
 
                             k=       mmz
              DO           i=ip11,mx1
                      qwHY(i,j,k) = WKxyz1(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxy1(i,j)
     .                             +WKxyz8(i,j,k)  *WKxza(i,k))
                      qrHY(i,j,k) = WKxyz2(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxy2(i,j)
     .                             +WKxyz8(i,j,k)  *WKxzb(i,k))
              END DO
 
C +--Wat.Vapr.Advect.avoids double Counting in case of convective Adjustment
 
              DO             k=    1 ,mmz1
              DO           i=ip11,mx1
c #cA           IF (adj_CA(i,j).eq.0)                             THEN
                      qvDY(i,j,k) = WKxyz3(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxzc(i,k+1)
     .                             +WKxyz8(i,j,k)  *WKxzc(i,k  ))
c #cA           END IF
              END DO
              END DO
 
                             k=       mmz
              DO           i=ip11,mx1
c #cA           IF (adj_CA(i,j).eq.0)                             THEN
                      qvDY(i,j,k) = WKxyz3(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxy3(i,j)
     .                             +WKxyz8(i,j,k)  *WKxzc(i,k))
c #cA           END IF
              END DO
C +
            END DO
C +
          END IF
 
 
C +--Warm Water
C +--End of the                             Local Split Time Differencing
C +  --------------------------------------------------------------------
 
         END DO
 
 
C +--Warm Water
C +--2nd Order Centered Leap-Frog Backward: Local Split Time Differencing
C +  --------------------------------------------------------------------
C +
        ELSE
C +
c #WA     write(6,6009)
 6009     format(6x,' 6009        centrL.and. .not. staggr /A Contrib.')
C +
         DO  itimax=    1,ntimax
C +
C +--First        internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          IF(itimax.eq. 1)                                        THEN
C +
            DO       j=jp11,my1
C +
C +--Advection Increment
C +
                     k=    1
              DO     i=ip11,mx1
                  qw_0       =                   qwHY(i,j,k)
                WKxza(i,k)   =(  qwHY(i,j,k+1)-  qw_0)
     .                        *WKxyz7(i,j,k)
                  qr_0       =                   qrHY(i,j,k)
                WKxzb(i,k)   =(  qrHY(i,j,k+1)-  qrHY(i,j,k  ))
     .                        *WKxyz7(i,j,k)
                  qv_0       =                   qvDY(i,j,k)
                WKxzc(i,k)   =(  qvDY(i,j,k+1)-  qvDY(i,j,k  ))
     .                        *WKxyz7(i,j,k)
              END DO
C +
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =(  qwHY(i,j,k+1)-  qwHY(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =(  qrHY(i,j,k+1)-  qrHY(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(  qvDY(i,j,k+1)-  qvDY(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
              END DO
C +
                      k=       mmz
              DO      i=ip11,mx1
                WKxza(i,k)   =                -  qwHY(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =                -  qrHY(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(qvapSL(i,j)    -  qvDY(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
C +
C +--Advection Contribution
C +
              DO       k=    1 ,mmz
              DO       i=ip11,mx1
                WKxyz1(i,j,k)=   qwHY(i,j,k)  -WKxza(i,k)
                WKxyz4(i,j,k)=   qwHY(i,j,k) -(WKxza(i,k)+WKxza(i,k))
                WKxyz2(i,j,k)=   qrHY(i,j,k)  -WKxzb(i,k)
                WKxyz5(i,j,k)=   qrHY(i,j,k) -(WKxzb(i,k)+WKxzb(i,k))
                WKxyz3(i,j,k)=   qvDY(i,j,k)  -WKxzc(i,k)
                WKxyz6(i,j,k)=   qvDY(i,j,k) -(WKxzc(i,k)+WKxzc(i,k))
              END DO
              END DO
            END DO
C +
C +--Intermediary internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          ELSE IF (itimax.lt.ntimax)                              THEN
C +
C +--Advection Increment
C +
            DO       j=jp11,my1
C +
                     k=    1
              DO     i=ip11,mx1
                  qw_0       =                 WKxyz4(i,j,k)
                  qr_0       =                 WKxyz5(i,j,k)
                  qv_0       =                 WKxyz6(i,j,k)
C +
                WKxza(i,k)   =(WKxyz4(i,j,k+1)-  qw_0)
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =(WKxyz5(i,j,k+1)-  qr_0)
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(WKxyz6(i,j,k+1)-  qv_0)
     .                        *WKxyz7(i,j,k)
              END DO
C +
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =(WKxyz4(i,j,k+1)-WKxyz4(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =(WKxyz5(i,j,k+1)-WKxyz5(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(WKxyz6(i,j,k+1)-WKxyz6(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
              END DO
C +
                      k=       mmz
              DO      i=ip11,mx1
                WKxza(i,k)   =                -WKxyz4(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =                -WKxyz5(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(qvapSL(i,j)    -WKxyz6(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
C +
C +--Advection Contribution
C +
              DO      k=    1 ,mmz
              DO      i=ip11,mx1
                old__u       = WKxyz1(i,j,k)
                WKxyz1(i,j,k)= WKxyz4(i,j,k)
                WKxyz4(i,j,k)= old__u        -(WKxza(i,k)+WKxza(i,k))
                old__v       = WKxyz2(i,j,k)
                WKxyz2(i,j,k)= WKxyz5(i,j,k)
                WKxyz5(i,j,k)= old__v        -(WKxzb(i,k)+WKxzb(i,k))
                old__t       = WKxyz3(i,j,k)
                WKxyz3(i,j,k)= WKxyz6(i,j,k)
                WKxyz6(i,j,k)= old__t        -(WKxzc(i,k)+WKxzc(i,k))
              END DO
              END DO
C +
            END DO
C +
C +--Last         internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          ELSE
C +
            DO       j=jp11,my1
C +
C +--Advection Increment
C +
                      k=    1
              DO      i=ip11,mx1
                  qw_0       =                 WKxyz4(i,j,k)
                  qr_0       =                 WKxyz5(i,j,k)
                  qv_0       =                 WKxyz6(i,j,k)
C +
                WKxza(i,k)   =(WKxyz4(i,j,k+1)-  qw_0)
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =(WKxyz5(i,j,k+1)-  qr_0)
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(WKxyz6(i,j,k+1)-  qv_0)
     .                        *WKxyz7(i,j,k)
              END DO
C +
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =(WKxyz4(i,j,k+1)-WKxyz4(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =(WKxyz5(i,j,k+1)-WKxyz5(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(WKxyz6(i,j,k+1)-WKxyz6(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
              END DO
C +
                      k=       mmz
              DO      i=ip11,mx1
                WKxza(i,k)   =                -WKxyz4(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =                -WKxyz5(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(qvapSL(i,j)    -WKxyz6(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
C +
C +--Hydromet.Advection
C +
              DO             k=    1 ,mmz
                DO           i=ip11,mx1
                        qwHY(i,j,k) = WKxyz1(i,j,k) -WKxza(i,k)
                        qrHY(i,j,k) = WKxyz2(i,j,k) -WKxzb(i,k)
                END DO
C +
C +--Wat.Vapr.Advect.avoids double Counting in case of convective Adjustment
C +
                DO           i=ip11,mx1
c #cA             IF (adj_CA(i,j).eq.0)                           THEN
                        qvDY(i,j,k) = WKxyz3(i,j,k) -WKxzc(i,k)
c #cA             END IF
                END DO
              END DO
C +
            END DO
C +
          END IF
C +
C +
C +--Warm Water
C +--End of the                             Local Split Time Differencing
C +  --------------------------------------------------------------------
C +
         END DO
C +
        END IF
C +
C +
C +--Warm Water
C +--First  Order Upstream Scheme:          Local Split Time Differencing
C +  --------------------------------------------------------------------
C +
       ELSE
C +
c #WA     write(6,6010)
 6010     format(6x,' 6010  .not. centrL.and.(.not.)staggr /A Contrib.')
C +
         DO  itimax=    1,ntimax
C +
C +--Auxiliary Variables
C +  ~~~~~~~~~~~~~~~~~~~
c #WA     write(6,6011)itimax,WKxyz1(imez,jmez,mz1),WKxyz1(imez,jmez,mz)
c #WA.                       ,  qwHY(imez,jmez,mz1),  qwHY(imez,jmez,mz)
 6011     format(6x,' 6011  .not. centrL.and.(.not.)staggr /A Contrib.',
     .                        4f9.6)
C +
             DO       k=1,mz
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz1(i,j,k)=   qwHY(i,j,k)
               WKxyz2(i,j,k)=   qrHY(i,j,k)
               WKxyz3(i,j,k)=   qvDY(i,j,k)
             END DO
             END DO
             END DO
C +
C +--Vertical Differences
C +  ~~~~~~~~~~~~~~~~~~~~
                      k=1
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz4(i,j,k) = 0.0d+0
               WKxyz5(i,j,k) = 0.0d+0
               WKxyz6(i,j,k) = 0.0d+0
             END DO
             END DO
C +
             DO       k=kp1(1),mz
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz4(i,j,k) = WKxyz1(i,j,k)-WKxyz1(i,j,k-1)
               WKxyz5(i,j,k) = WKxyz2(i,j,k)-WKxyz2(i,j,k-1)
               WKxyz6(i,j,k) = WKxyz3(i,j,k)-WKxyz3(i,j,k-1)
             END DO
             END DO
             END DO
C +
                      k=  mzz
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxy1 (i,j)   =              -WKxyz1(i,j,k-1)
               WKxy2 (i,j)   =              -WKxyz2(i,j,k-1)
               WKxy3 (i,j)   = qvapSL(i,j)  -WKxyz3(i,j,k-1)
             END DO
             END DO
C +
C +--Advection Contribution
C +  ~~~~~~~~~~~~~~~~~~~~~~
             DO       k=    1 ,mmz1
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz1(i,j,k)=  qwHY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz4(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxyz4(i,j,k+1)
               WKxyz2(i,j,k)=  qrHY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz5(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxyz5(i,j,k+1)
               WKxyz3(i,j,k)=  qvDY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz6(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxyz6(i,j,k+1)
             END DO
             END DO
             END DO
C +
                      k=mz
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz1(i,j,k)=  qwHY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz4(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxy1( i,j)
               WKxyz2(i,j,k)=  qrHY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz5(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxy2( i,j)
               WKxyz3(i,j,k)=  qvDY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz6(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxy3( i,j)
             END DO
             END DO
C +
C +--Wind    Update
C +  ~~~~~~~~~~~~~~
             DO             k=1,mz
             DO             j=jp11,my1
               DO           i=ip11,mx1
                       qwHY(i,j,k) = WKxyz1(i,j,k)
                       qrHY(i,j,k) = WKxyz2(i,j,k)
               END DO
C +
C +--Pot.Temp.Update avoids double Counting in case of convective Adjustment
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
               DO           i=ip11,mx1
c #cA            IF (adj_CA(i,j).eq.0)                            THEN
                       qvDY(i,j,k) = WKxyz3(i,j,k)
c #cA            END IF
               END DO
             END DO
             END DO
C +
C +
C +--End of the                             Local Split Time Differencing
C +  --------------------------------------------------------------------
C +
c #WA     write(6,6012)itimax,WKxyz1(imez,jmez,mz1),WKxyz1(imez,jmez,mz)
c #WA.                       ,  qwHY(imez,jmez,mz1),  qwHY(imez,jmez,mz)
 6012     format(6x,' 6012  .not. centrL.and.(.not.)staggr /A Contrib.',
     .                        4f9.6)
         END DO
C +
       END IF
C +
C +
C +--Warm Water Conservation
C +  -----------------------
C +
c #WF  DO j=1,my
c #WF  DO i=1,mx
c #WF      qvVC      = 0.0d+0
c #WF      qwVC      = 0.0d+0
c #WF      qrVC      = 0.0d+0
c #WF    DO k=1,mz
C +
C +--Flux Limitor
C +  ~~~~~~~~~~~~
c #WF      qvDY(i,j,k) =      max( eps9     ,qvDY(i,j,k))
c #WF      qvDY(i,j,k) =      min(WKxy7(i,j),qvDY(i,j,k))
c #WF      qwHY(i,j,k) =      max( zero     ,qwHY(i,j,k))
c #WF      qwHY(i,j,k) =      min(WKxy8(i,j),qwHY(i,j,k))
c #WF      qrHY(i,j,k) =      max( zero     ,qrHY(i,j,k))
c #WF      qrHY(i,j,k) =      min(WKxy9(i,j),qrHY(i,j,k))
C +
C +--Column Average
C +  ~~~~~~~~~~~~~~
c #WF      qvVC      = qvVC      + dsigm1(k)*qvDY(i,j,k)
c #WF      qwVC      = qwVC      + dsigm1(k)*qwHY(i,j,k)
c #WF      qrVC      = qrVC      + dsigm1(k)*qrHY(i,j,k)
c #WF    END DO
C +
C +--Surface Boundary Flux
C +  ~~~~~~~~~~~~~~~~~~~~~
c #WF      qvVC      = qvVC - psigDY(i,j,mz)
c #WF.                        *(qvDY(i,j,mz)-qvapSL(i,j))*dt/pstDYn(i,j)
c #WF      qwVC      = qwVC - psigDY(i,j,mz)
c #WF.                        * qwHY(i,j,mz)             *dt/pstDYn(i,j)
c #WF      qrVC      = qrVC - psigDY(i,j,mz)
c #WF.                        * qrHY(i,j,mz)             *dt/pstDYn(i,j)
C +
C +--Mass Restore
C +  ~~~~~~~~~~~~
c #WF    DO k=1,mz
c #WF      qvDY(i,j,k) = qvDY(i,j,k) * WKxy4(i,j)/max(eps12,qvVC)
c #WF      qwHY(i,j,k) = qwHY(i,j,k) * WKxy5(i,j)/max(eps12,qwVC)
c #WF      qrHY(i,j,k) = qrHY(i,j,k) * WKxy6(i,j)/max(eps12,qrVC)
c #WF    END DO
c #WF  END DO
c #WF  END DO
C +
C +
C +--Warm Water
C +--Work Arrays Reset
C +  -----------------
C +
       DO       j=1,my
       DO       i=1,mx
         WKxy1( i,j)   = 0.0
         WKxy2( i,j)   = 0.0
         WKxy3( i,j)   = 0.0
         WKxy4( i,j)   = 0.0
         WKxy5( i,j)   = 0.0
         WKxy6( i,j)   = 0.0
       END DO
       END DO
C +
       DO       k=1,mz
       DO       i=1,mx
         WKxza( i,  k) = 0.0
         WKxzb( i,  k) = 0.0
         WKxzc( i,  k) = 0.0
         WKxzd( i,  k) = 0.0
       END DO
       END DO
C +
       DO       k=1,mz
       DO       j=1,my
       DO       i=1,mx
         WKxyz1(i,j,k) = 0.0
         WKxyz2(i,j,k) = 0.0
         WKxyz3(i,j,k) = 0.0
         WKxyz4(i,j,k) = 0.0
         WKxyz5(i,j,k) = 0.0
         WKxyz6(i,j,k) = 0.0
       END DO
       END DO
       END DO
C +
C +
C +--Ice  Water Conservation
C +  -----------------------
C +
c #WF  DO j=1,my
c #WF  DO i=1,mx
c #WF      WKxy4(i,j) = 0.0d+0
c #WF      WKxy5(i,j) = 0.0d+0
c #WF      WKxy6(i,j) = 0.0d+0
c #WF      WKxy7(i,j) = 0.0d+0
c #WF      WKxy8(i,j) = 0.0d+0
c #WF      WKxy9(i,j) = 0.0d+0
c #WF    DO k=1,mz
c #WF      WKxy4(i,j) = WKxy4(i,j) + dsigm1(k)*ccniHY(i,j,k)
c #WF      WKxy5(i,j) = WKxy5(i,j) + dsigm1(k)*  qiHY(i,j,k)
c #WF      WKxy6(i,j) = WKxy6(i,j) + dsigm1(k)*  qsHY(i,j,k)
c #WF      WKxy7(i,j) =         max(WKxy7(i,j),ccniHY(i,j,k))
c #WF      WKxy8(i,j) =         max(WKxy8(i,j),  qiHY(i,j,k))
c #WF      WKxy9(i,j) =         max(WKxy9(i,j),  qsHY(i,j,k))
c #WF    END DO
c #WF  END DO
c #WF  END DO
C +
C +
C +--Ice  Water
C +--Start Vertical Advection
C +  ------------------------
C +
       IF   (centrL)                                              THEN
C +
        IF  (staggr)                                              THEN
C +
C +--2nd Order Centered Energy conserving:  Local Split Time Differencing
C +  ~~~~~~~~~ (Haltiner & Williams 1980 7.2.2, (7-47b) p.220) ~~~~~~~~~~
C +            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
         DO  itimax=    1,ntimax
C +
C +--First        internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          IF(itimax.eq. 1)                                        THEN
C +
            DO       j=jp11,my1
C +
C +--Vertical Differences
C +
                      k=    1
                dsgm         =             2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzp(i,k)   =   qiHY(i,j,k)
                WKxzq(i,k)   = ccniHY(i,j,k)
                WKxzx(i,k)   =   qsHY(i,j,k)
C +
c #VA           WKxzp(i,k)   =(  qiHY(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +  qiHY(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzq(i,k)   =(ccniHY(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +ccniHY(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzx(i,k)   =(  qsHY(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +  qsHY(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
C +
              DO      i=ip11,mx1
                  qi_0       =                  WKxzp(i,  k)
                WKxza(i,k)   =( WKxzp(i,  k)  -  qi_0)
                ccni_0       =                  WKxzq(i,  k)
                WKxzb(i,k)   =( WKxzq(i,  k)  -ccni_0)
                  qs_0       =                  WKxzx(i,  k)
                WKxzc(i,k)   =( WKxzx(i,  k)  -  qs_0)
              END DO
C +
              DO      k=kp1(1),mmz1
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzp(i,k)   =   qiHY(i,j,k)
                WKxzq(i,k)   = ccniHY(i,j,k)
                WKxzx(i,k)   =   qsHY(i,j,k)
C +
c #VA           WKxzp(i,k)   =(  qiHY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +  qiHY(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +  qiHY(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzq(i,k)   =(ccniHY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +ccniHY(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +ccniHY(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzx(i,k)   =(  qsHY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +  qsHY(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +  qsHY(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
              END DO
C +
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =( WKxzp(i,  k)  - WKxzp(i,  k-1))
                WKxzb(i,k)   =( WKxzq(i,  k)  - WKxzq(i,  k-1))
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
              END DO
C +
                      k=       mmz
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)
              DO      i=ip11,mx1
                WKxzp(i,k)   =   qiHY(i,j,k)
                WKxzq(i,k)   = ccniHY(i,j,k)
                WKxzx(i,k)   =   qsHY(i,j,k)
C +
c #VA           WKxzp(i,k)   =(  qiHY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +  qiHY(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
c #VA           WKxzq(i,k)   =(ccniHY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +ccniHY(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
c #VA           WKxzx(i,k)   =(  qsHY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +  qsHY(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
              END DO
C +
              DO      i=ip11,mx1
                WKxza(i,k)   =( WKxzp(i,  k)  - WKxzp(i,  k-1))
                WKxzb(i,k)   =( WKxzq(i,  k)  - WKxzq(i,  k-1))
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
C +
                      k=       mzz
              DO      i=ip11,mx1
                WKxy1(i,j)   = 0.0d+0
                WKxy2(i,j)   = 0.0d+0
                WKxy3(i,j)   = 0.0d+0
c #NS           WKxy1(i,j)   =                - WKxzp(i,  k-1)
c #NS           WKxy2(i,j)   =                - WKxzq(i,  k-1)
c #NS           WKxy3(i,j)   =(qsrfHY(i,j)    - WKxzx(i,  k-1))
              END DO
C +
C +--Advection Contribution
C +
              DO       k=    1 ,mmz1
              DO       i=ip11,mx1
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxza(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxza(i,k)
                WKxyz1(i,j,k)=   qiHY(i,j,k)  -WKxzd(i,k)
                WKxyz4(i,j,k)=   qiHY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
C +
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxzb(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxzb(i,k)
                WKxyz2(i,j,k)= ccniHY(i,j,k)  -WKxzd(i,k)
                WKxyz5(i,j,k)= ccniHY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
C +
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxzc(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxzc(i,k)
                WKxyz3(i,j,k)=   qsHY(i,j,k)  -WKxzd(i,k)
                WKxyz6(i,j,k)=   qsHY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
              END DO
              END DO
C +
                       k=       mmz
              DO       i=ip11,mx1
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy1(i,j)
     .                        +WKxyz8(i,j,k)  *WKxza(i,k)
                WKxyz1(i,j,k)=   qiHY(i,j,k)  -WKxzd(i,k)
                WKxyz4(i,j,k)=   qiHY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
C +
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy2(i,j)
     .                        +WKxyz8(i,j,k)  *WKxzb(i,k)
                WKxyz2(i,j,k)= ccniHY(i,j,k)  -WKxzd(i,k)
                WKxyz5(i,j,k)= ccniHY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
C +
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy3(i,j)
     .                        +WKxyz8(i,j,k)  *WKxzc(i,k)
                WKxyz3(i,j,k)=   qsHY(i,j,k)  -WKxzd(i,k)
                WKxyz6(i,j,k)=   qsHY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
              END DO
C +
            END DO
C +
C +--Intermediary internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          ELSE IF (itimax.lt.ntimax)                              THEN
C +
C +--Vertical Differences
C +
            DO       j=jp11,my1
C +
                      k=    1
                dsgm         =             2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzp(i,k)   = WKxyz4(i,j,k)
                WKxzq(i,k)   = WKxyz5(i,j,k)
                WKxzx(i,k)   = WKxyz6(i,j,k)
C +
c #VA           WKxzp(i,k)   =(WKxyz4(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz4(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzq(i,k)   =(WKxyz5(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz5(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz6(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
C +
              DO      i=ip11,mx1
                  qi_0       =                  WKxzp(i,  k)
                WKxza(i,k)   =( WKxzp(i,  k)  -  qi_0)
                ccni_0       =                  WKxzq(i,  k)
                WKxzb(i,k)   =( WKxzq(i,  k)  -ccni_0)
                  qs_0       =                  WKxzx(i,  k)
                WKxzc(i,k)   =( WKxzx(i,  k)  -  qs_0)
              END DO
C +
              DO      k=kp1(1),mmz1
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzp(i,k)   = WKxyz4(i,j,k)
                WKxzq(i,k)   = WKxyz5(i,j,k)
                WKxzx(i,k)   = WKxyz6(i,j,k)
C +
c #VA           WKxzp(i,k)   =(WKxyz4(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz4(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz4(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzq(i,k)   =(WKxyz5(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz5(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz5(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz6(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz6(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
              END DO
C +
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =( WKxzp(i,  k)  - WKxzp(i,  k-1))
                WKxzb(i,k)   =( WKxzq(i,  k)  - WKxzq(i,  k-1))
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
              END DO
C +
                      k=       mmz
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)
              DO      i=ip11,mx1
                WKxzp(i,k)   = WKxyz4(i,j,k)
                WKxzq(i,k)   = WKxyz5(i,j,k)
                WKxzx(i,k)   = WKxyz6(i,j,k)
C +
c #VA           WKxzp(i,k)   =(WKxyz4(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz4(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
c #VA           WKxzq(i,k)   =(WKxyz5(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz5(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz6(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
              END DO
C +
              DO      i=ip11,mx1
                WKxza(i,k)   =( WKxzp(i,  k)  - WKxzp(i,  k-1))
                WKxzb(i,k)   =( WKxzq(i,  k)  - WKxzq(i,  k-1))
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
C +
                      k=       mzz
              DO      i=ip11,mx1
                WKxy1(i,j)   = 0.0d+0
                WKxy2(i,j)   = 0.0d+0
                WKxy3(i,j)   = 0.0d+0
c #NS           WKxy1(i,j)   =                - WKxzp(i,  k-1)
c #NS           WKxy2(i,j)   =                - WKxzq(i,  k-1)
c #NS           WKxy3(i,j)   =(qsrfHY(i,j)    - WKxzx(i,  k-1))
              END DO
C +
C +--Advection Contribution
C +
              DO      k=    1 ,mmz1
              DO      i=ip11,mx1
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxza(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxza(i,k)
                old__u       = WKxyz1(i,j,k)
                WKxyz1(i,j,k)= WKxyz4(i,j,k)
                WKxyz4(i,j,k)= old__u        -(WKxzd(i,k)+WKxzd(i,k))
C +
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxzb(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxzb(i,k)
                old__v       = WKxyz2(i,j,k)
                WKxyz2(i,j,k)= WKxyz5(i,j,k)
                WKxyz5(i,j,k)= old__v        -(WKxzd(i,k)+WKxzd(i,k))
C +
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxzc(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxzc(i,k)
                old__t       = WKxyz3(i,j,k)
                WKxyz3(i,j,k)= WKxyz6(i,j,k)
                WKxyz6(i,j,k)= old__t        -(WKxzd(i,k)+WKxzd(i,k))
              END DO
              END DO
C +
                      k=       mmz
              DO      i=ip11,mx1
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy1(i,j)
     .                        +WKxyz8(i,j,k)  *WKxza(i,k)
                old__u       = WKxyz1(i,j,k)
                WKxyz1(i,j,k)= WKxyz4(i,j,k)
                WKxyz4(i,j,k)= old__u        -(WKxzd(i,k)+WKxzd(i,k))
C +
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy2(i,j)
     .                        +WKxyz8(i,j,k)  *WKxzb(i,k)
                old__v       = WKxyz2(i,j,k)
                WKxyz2(i,j,k)= WKxyz5(i,j,k)
                WKxyz5(i,j,k)= old__v        -(WKxzd(i,k)+WKxzd(i,k))
C +
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy3(i,j)
     .                        +WKxyz8(i,j,k)  *WKxzc(i,k)
                old__t       = WKxyz3(i,j,k)
                WKxyz3(i,j,k)= WKxyz6(i,j,k)
                WKxyz6(i,j,k)= old__t        -(WKxzd(i,k)+WKxzd(i,k))
              END DO
C +
            END DO
C +
C +--Last         internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          ELSE
C +
            DO       j=jp11,my1
C +
C +--Vertical Differences
C +
                      k=    1
                dsgm         =             2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzp(i,k)   = WKxyz4(i,j,k)
                WKxzq(i,k)   = WKxyz5(i,j,k)
                WKxzx(i,k)   = WKxyz6(i,j,k)
C +
c #VA           WKxzp(i,k)   =(WKxyz4(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz4(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzq(i,k)   =(WKxyz5(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz5(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz6(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
C +
              DO      i=ip11,mx1
                  qi_0       =                  WKxzp(i,  k)
                WKxza(i,k)   =( WKxzp(i,  k)  -  qi_0)
                ccni_0       =                  WKxzq(i,  k)
                WKxzb(i,k)   =( WKxzq(i,  k)  -ccni_0)
                  qs_0       =                  WKxzx(i,  k)
                WKxzc(i,k)   =( WKxzx(i,  k)  -  qs_0)
              END DO
C +
              DO      k=kp1(1),mmz1
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzp(i,k)   = WKxyz4(i,j,k)
                WKxzq(i,k)   = WKxyz5(i,j,k)
                WKxzx(i,k)   = WKxyz6(i,j,k)
C +
c #VA           WKxzp(i,k)   =(WKxyz4(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz4(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz4(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzq(i,k)   =(WKxyz5(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz5(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz5(i,j,k+1)*dsigm1(k+1)     )/dsgm
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz6(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz6(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
              END DO
C +
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =( WKxzp(i,  k)  - WKxzp(i,  k-1))
                WKxzb(i,k)   =( WKxzq(i,  k)  - WKxzq(i,  k-1))
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
              END DO
C +
                      k=       mmz
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)
              DO      i=ip11,mx1
                WKxzp(i,k)   = WKxyz4(i,j,k)
                WKxzq(i,k)   = WKxyz5(i,j,k)
                WKxzx(i,k)   = WKxyz6(i,j,k)
C +
c #VA           WKxzp(i,k)   =(WKxyz4(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz4(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
c #VA           WKxzq(i,k)   =(WKxyz5(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz5(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz6(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
              END DO
C +
              DO      i=ip11,mx1
                WKxza(i,k)   =( WKxzp(i,  k)  - WKxzp(i,  k-1))
                WKxzb(i,k)   =( WKxzq(i,  k)  - WKxzq(i,  k-1))
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
C +
                      k=       mzz
              DO      i=ip11,mx1
                WKxy1(i,j)   = 0.0
                WKxy2(i,j)   = 0.0
                WKxy3(i,j)   = 0.0
c #NS           WKxy1(i,j)   =                - WKxzp(i,  k-1)
c #NS           WKxy2(i,j)   =                - WKxzq(i,  k-1)
c #NS           WKxy3(i,j)   =(qsrfHY(i,j)    - WKxzx(i,  k-1))
              END DO
C +
C +--Hydromet.Advection
C +
              DO             k=    1 ,mmz1
              DO           i=ip11,mx1
                      qiHY(i,j,k) = WKxyz1(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxza(i,k+1)
     .                             +WKxyz8(i,j,k)  *WKxza(i,k  ))
                    ccniHY(i,j,k) = WKxyz2(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxzb(i,k+1)
     .                             +WKxyz8(i,j,k)  *WKxzb(i,k  ))
                      qsHY(i,j,k) = WKxyz3(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxzc(i,k+1)
     .                             +WKxyz8(i,j,k)  *WKxzc(i,k  ))
              END DO
              END DO
C +
                             k=       mmz
              DO           i=ip11,mx1
                      qiHY(i,j,k) = WKxyz1(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxy1(i,j)
     .                             +WKxyz8(i,j,k)  *WKxza(i,k))
                    ccniHY(i,j,k) = WKxyz2(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxy2(i,j)
     .                             +WKxyz8(i,j,k)  *WKxzb(i,k))
                      qsHY(i,j,k) = WKxyz3(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxy3(i,j)
     .                             +WKxyz8(i,j,k)  *WKxzc(i,k))
              END DO
C +
            END DO
C +
          END IF
C +
C +
C +--Ice  Water
C +--End of the                             Local Split Time Differencing
C +  --------------------------------------------------------------------
C +
         END DO
C +
C +
C +--Ice  Water
C +--2nd Order Centered Leap-Frog Backward: Local Split Time Differencing
C +  --------------------------------------------------------------------
C +
        ELSE
C +
         DO  itimax=    1,ntimax
C +
C +--First        internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          IF(itimax.eq. 1)                                        THEN
C +
            DO       j=jp11,my1
C +
C +--Advection Increment
C +
                     k=    1
              DO     i=ip11,mx1
                  qi_0       =                   qiHY(i,j,k)
                WKxza(i,k)   =(  qiHY(i,j,k+1)-  qi_0)
     .                        *WKxyz7(i,j,k)
                ccni_0       =                 ccniHY(i,j,k)
                WKxzb(i,k)   =(ccniHY(i,j,k+1)-ccniHY(i,j,k  ))
     .                        *WKxyz7(i,j,k)
                  qv_0       =                   qsHY(i,j,k)
                WKxzc(i,k)   =(  qsHY(i,j,k+1)-  qsHY(i,j,k  ))
     .                        *WKxyz7(i,j,k)
              END DO
C +
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =(  qiHY(i,j,k+1)-  qiHY(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =(ccniHY(i,j,k+1)-ccniHY(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(  qsHY(i,j,k+1)-  qsHY(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
              END DO
C +
                      k=       mmz
              DO      i=ip11,mx1
                WKxza(i,k)   =                -  qiHY(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =                -ccniHY(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(qsrfHY(i,j)    -  qsHY(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
C +
C +--Advection Contribution
C +
              DO       k=    1 ,mmz
              DO       i=ip11,mx1
                WKxyz1(i,j,k)=   qiHY(i,j,k)  -WKxza(i,k)
                WKxyz4(i,j,k)=   qiHY(i,j,k) -(WKxza(i,k)+WKxza(i,k))
                WKxyz2(i,j,k)= ccniHY(i,j,k)  -WKxzb(i,k)
                WKxyz5(i,j,k)= ccniHY(i,j,k) -(WKxzb(i,k)+WKxzb(i,k))
                WKxyz3(i,j,k)=   qsHY(i,j,k)  -WKxzc(i,k)
                WKxyz6(i,j,k)=   qsHY(i,j,k) -(WKxzc(i,k)+WKxzc(i,k))
              END DO
              END DO
            END DO
C +
C +--Intermediary internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          ELSE IF (itimax.lt.ntimax)                              THEN
C +
C +--Advection Increment
C +
            DO       j=jp11,my1
C +
                     k=    1
              DO     i=ip11,mx1
                  qi_0       =                 WKxyz4(i,j,k)
                ccni_0       =                 WKxyz5(i,j,k)
                  qv_0       =                 WKxyz6(i,j,k)
C +
                WKxza(i,k)   =(WKxyz4(i,j,k+1)-  qi_0)
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =(WKxyz5(i,j,k+1)-ccni_0)
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(WKxyz6(i,j,k+1)-  qv_0)
     .                        *WKxyz7(i,j,k)
              END DO
C +
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =(WKxyz4(i,j,k+1)-WKxyz4(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =(WKxyz5(i,j,k+1)-WKxyz5(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(WKxyz6(i,j,k+1)-WKxyz6(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
              END DO
C +
                      k=       mmz
              DO      i=ip11,mx1
                WKxza(i,k)   =                -WKxyz4(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =                -WKxyz5(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(qsrfHY(i,j)    -WKxyz6(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
C +
C +--Advection Contribution
C +
              DO      k=    1 ,mmz
              DO      i=ip11,mx1
                old__u       = WKxyz1(i,j,k)
                WKxyz1(i,j,k)= WKxyz4(i,j,k)
                WKxyz4(i,j,k)= old__u        -(WKxza(i,k)+WKxza(i,k))
                old__v       = WKxyz2(i,j,k)
                WKxyz2(i,j,k)= WKxyz5(i,j,k)
                WKxyz5(i,j,k)= old__v        -(WKxzb(i,k)+WKxzb(i,k))
                old__t       = WKxyz3(i,j,k)
                WKxyz3(i,j,k)= WKxyz6(i,j,k)
                WKxyz6(i,j,k)= old__t        -(WKxzc(i,k)+WKxzc(i,k))
              END DO
              END DO
C +
            END DO
C +
C +--Last         internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          ELSE
C +
            DO       j=jp11,my1
C +
C +--Advection Increment
C +
                      k=    1
              DO      i=ip11,mx1
                  qi_0       =                 WKxyz4(i,j,k)
                ccni_0       =                 WKxyz5(i,j,k)
                  qv_0       =                 WKxyz6(i,j,k)
C +
                WKxza(i,k)   =(WKxyz4(i,j,k+1)-  qi_0)
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =(WKxyz5(i,j,k+1)-ccni_0)
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(WKxyz6(i,j,k+1)-  qv_0)
     .                        *WKxyz7(i,j,k)
              END DO
C +
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =(WKxyz4(i,j,k+1)-WKxyz4(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =(WKxyz5(i,j,k+1)-WKxyz5(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(WKxyz6(i,j,k+1)-WKxyz6(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
              END DO
C +
                      k=       mmz
              DO      i=ip11,mx1
                WKxza(i,k)   =                -WKxyz4(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =                -WKxyz5(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(qsrfHY(i,j)    -WKxyz6(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
C +
C +--Hydromet.Advection
C +
              DO             k=    1 ,mmz
                DO           i=ip11,mx1
                        qiHY(i,j,k) = WKxyz1(i,j,k) -WKxza(i,k)
                      ccniHY(i,j,k) = WKxyz2(i,j,k) -WKxzb(i,k)
                        qsHY(i,j,k) = WKxyz3(i,j,k) -WKxzc(i,k)
                END DO
              END DO
C +
            END DO
C +
          END IF
C +
C +
C +--Ice  Water
C +--End of the                             Local Split Time Differencing
C +  --------------------------------------------------------------------
C +
         END DO
C +
        END IF
C +
C +
C +--Ice  Water
C +--First  Order Upstream Scheme:          Local Split Time Differencing
C +  --------------------------------------------------------------------
C +
       ELSE
C +
         DO  itimax=    1,ntimax
C +
C +--Auxiliary Variables
C +  ~~~~~~~~~~~~~~~~~~~
             DO       k=1,mz
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz1(i,j,k)=   qiHY(i,j,k)
               WKxyz2(i,j,k)= ccniHY(i,j,k)
               WKxyz3(i,j,k)=   qsHY(i,j,k)
             END DO
             END DO
             END DO
C +
C +--Vertical Differences
C +  ~~~~~~~~~~~~~~~~~~~~
                      k=1
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz4(i,j,k) = 0.0d+0
               WKxyz5(i,j,k) = 0.0d+0
               WKxyz6(i,j,k) = 0.0d+0
             END DO
             END DO
C +
             DO       k=kp1(1),mz
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz4(i,j,k) = WKxyz1(i,j,k)-WKxyz1(i,j,k-1)
               WKxyz5(i,j,k) = WKxyz2(i,j,k)-WKxyz2(i,j,k-1)
               WKxyz6(i,j,k) = WKxyz3(i,j,k)-WKxyz3(i,j,k-1)
             END DO
             END DO
             END DO
C +
                      k=  mzz
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxy1 (i,j)   =              -WKxyz1(i,j,k-1)
               WKxy2 (i,j)   =              -WKxyz2(i,j,k-1)
               WKxy3 (i,j)   = qsrfHY(i,j)  -WKxyz3(i,j,k-1)
             END DO
             END DO
C +
C +--Advection Contribution
C +  ~~~~~~~~~~~~~~~~~~~~~~
             DO       k=    1 ,mmz1
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz1(i,j,k)=  qiHY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz4(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxyz4(i,j,k+1)
               WKxyz2(i,j,k)=ccniHY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz5(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxyz5(i,j,k+1)
               WKxyz3(i,j,k)=  qsHY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz6(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxyz6(i,j,k+1)
             END DO
             END DO
             END DO
C +
                      k=mz
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz1(i,j,k)=  qiHY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz4(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxy1( i,j)
               WKxyz2(i,j,k)=ccniHY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz5(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxy2( i,j)
               WKxyz3(i,j,k)=  qsHY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz6(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxy3( i,j)
             END DO
             END DO
C +
C +--Hydrom. Update
C +  ~~~~~~~~~~~~~~
             DO             k=1,mz
             DO             j=jp11,my1
               DO           i=ip11,mx1
                       qiHY(i,j,k) = WKxyz1(i,j,k)
                     ccniHY(i,j,k) = WKxyz2(i,j,k)
                       qsHY(i,j,k) = WKxyz3(i,j,k)
               END DO
             END DO
             END DO
C +
C +
C +--Ice  Water
C +--End of the                             Local Split Time Differencing
C +  --------------------------------------------------------------------
C +
         END DO
C +
       END IF
C +
C +
C +--Ice  Water Conservation
C +  -----------------------
C +
c #WF  DO j=1,my
c #WF  DO i=1,mx
c #WF      ciVC      = 0.0d+0
c #WF      qiVC      = 0.0d+0
c #WF      qsVC      = 0.0d+0
c #WF    DO k=1,mz
C +
C +--Flux Limitor
C +  ~~~~~~~~~~~~
c #WF      ccniHY(i,j,k) =      max( zero     ,ccniHY(i,j,k))
c #WF      ccniHY(i,j,k) =      min(WKxy7(i,j),ccniHY(i,j,k))
c #WF        qiHY(i,j,k) =      max( zero     ,  qiHY(i,j,k))
c #WF        qiHY(i,j,k) =      min(WKxy8(i,j),  qiHY(i,j,k))
c #WF        qsHY(i,j,k) =      max( zero     ,  qsHY(i,j,k))
c #WF        qsHY(i,j,k) =      min(WKxy9(i,j),  qsHY(i,j,k))
C +
C +--Column Average
C +  ~~~~~~~~~~~~~~
c #WF      ciVC      = ciVC      + dsigm1(k)  *ccniHY(i,j,k)
c #WF      qiVC      = qiVC      + dsigm1(k)  *  qiHY(i,j,k)
c #WF      qsVC      = qsVC      + dsigm1(k)  *  qsHY(i,j,k)
c #WF    END DO
C +
C +--Surface Boundary Flux
C +  ~~~~~~~~~~~~~~~~~~~~~
c #WF      ciVC      = ciVC - psigDY(i,j,mz)
c #WF.                       *ccniHY(i,j,mz)             *dt/pstDYn(i,j)
c #WF      qiVC      = qiVC - psigDY(i,j,mz)
c #WF.                        * qiHY(i,j,mz)             *dt/pstDYn(i,j)
c #WF      qsVC      = qsVC - psigDY(i,j,mz)
c #WF.                        *(qsHY(i,j,mz)-qsrfHY(i,j))*dt/pstDYn(i,j)
C +
C +--Mass Restore
C +  ~~~~~~~~~~~~
c #WF    DO k=1,mz
c #WF      ccniHY(i,j,k) = ccniHY(i,j,k) *(WKxy4(i,j)/max(eps12,ciVC))
c #WF        qiHY(i,j,k) =   qiHY(i,j,k) * WKxy5(i,j)/max(eps12,qiVC)
c #WF        qsHY(i,j,k) =   qsHY(i,j,k) * WKxy6(i,j)/max(eps12,qsVC)
c #WF    END DO
c #WF  END DO
c #WF  END DO
C +
C +
C +--Work Arrays Reset
C +  -----------------
C +
       DO       j=1,my
       DO       i=1,mx
         WKxy1( i,j)   = 0.0
         WKxy2( i,j)   = 0.0
         WKxy3( i,j)   = 0.0
         WKxy4( i,j)   = 0.0
         WKxy5( i,j)   = 0.0
         WKxy6( i,j)   = 0.0
       END DO
       END DO
C +
       DO       k=1,mz
       DO       i=1,mx
         WKxza( i,  k) = 0.0
         WKxzb( i,  k) = 0.0
         WKxzc( i,  k) = 0.0
         WKxzd( i,  k) = 0.0
       END DO
       END DO
C +
       DO       k=1,mz
       DO       j=1,my
       DO       i=1,mx
         WKxyz1(i,j,k) = 0.0
         WKxyz2(i,j,k) = 0.0
         WKxyz3(i,j,k) = 0.0
         WKxyz4(i,j,k) = 0.0
         WKxyz5(i,j,k) = 0.0
         WKxyz6(i,j,k) = 0.0
       END DO
       END DO
       END DO
C +
C +
C +--Courant Number
C +--Work Arrays Reset
C +  -----------------
C +
       DO       k=1,mz
       DO       j=1,my
       DO       i=1,mx
         WKxyz7(i,j,k) = 0.0
         WKxyz8(i,j,k) = 0.0
       END DO
       END DO
       END DO
C +
C +
C +--Third Order Vertical Scheme
C +  ===========================
C +
c #ZU ELSE
C +
c #ZU  DO  j=jp11,my1
c #ZU  DO  i=ip11,mx1
c #ZU  ga0(i,j)      = 0.
c #ZO  ga0(i,j)      =   qwHY(i,j,mz)
c #ZU  END DO
c #ZU  END DO
C +
c #ZU  DO  k=1,mz
c #ZU  DO  j=jp11,my1
c #ZU  DO  i=ip11,mx1
c #ZU  gat(i,j,k)    =   qwHY(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
C +
C +    ****************
c #ZU  call DYNadv_cubv(gat,ga0)
C +    ****************
C +
c #ZU  DO     k=1,mz
c #ZU  DO     j=jp11,my1
c #ZU  DO     i=ip11,mx1
c #ZU    qwHY(i,j,k) = gat(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
C +
c #ZO  DO  j=jp11,my1
c #ZO  DO  i=ip11,mx1
c #ZO  ga0(i,j)      =   qrHY(i,j,mz)
c #ZU  END DO
c #ZU  END DO
C +
c #ZU  DO  k=1,mz
c #ZU  DO  j=jp11,my1
c #ZU  DO  i=ip11,mx1
c #ZU  gat(i,j,k)    =   qrHY(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
C +
C +    ****************
c #ZU  call DYNadv_cubv(gat,ga0)
C +    ****************
C +
c #ZU  DO     k=1,mz
c #ZU  DO     j=jp11,my1
c #ZU  DO     i=ip11,mx1
c #ZU    qrHY(i,j,k) = gat(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
C +
c #ZU  DO  j=jp11,my1
c #ZU  DO  i=ip11,mx1
c #ZU  ga0(i,j)      = qvapSL(i,j)
c #ZO  ga0(i,j)      =   qvDY(i,j,mz)
c #ZU  END DO
c #ZU  END DO
C +
c #ZU  DO  k=1,mz
c #ZU  DO  j=jp11,my1
c #ZU  DO  i=ip11,mx1
c #ZU  gat(i,j,k)    =   qvDY(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
C +
C +    ****************
c #ZU  call DYNadv_cubv(gat,ga0)
C +    ****************
C +
c #ZU  DO     k=1,mz
c #ZU  DO     j=jp11,my1
c #ZU  DO     i=ip11,mx1
c #ZU    qvDY(i,j,k) = gat(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
C +
c #ZU  DO  j=jp11,my1
c #ZU  DO  i=ip11,mx1
c #ZU  ga0(i,j)      = 0.0
c #ZO  ga0(i,j)      =   qiHY(i,j,mz)
c #ZU  END DO
c #ZU  END DO
C +
c #ZU  DO  k=1,mz
c #ZU  DO  j=jp11,my1
c #ZU  DO  i=ip11,mx1
c #ZU  gat(i,j,k)    =   qiHY(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
C +
C +    ****************
c #ZU  call DYNadv_cubv(gat,ga0)
C +    ****************
C +
c #ZU  DO     k=1,mz
c #ZU  DO     j=jp11,my1
c #ZU  DO     i=ip11,mx1
c #ZU    qiHY(i,j,k) = gat(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
C +
c #ZO  DO  j=jp11,my1
c #ZO  DO  i=ip11,mx1
c #ZO  ga0(i,j)      = ccniHY(i,j,mz)
c #ZU  END DO
c #ZU  END DO
C +
c #ZU  DO  k=1,mz
c #ZU  DO  j=jp11,my1
c #ZU  DO  i=ip11,mx1
c #ZU  gat(i,j,k)    = ccniHY(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
C +
C +    ****************
c #ZU  call DYNadv_cubv(gat,ga0)
C +    ****************
C +
c #ZU  DO     k=1,mz
c #ZU  DO     j=jp11,my1
c #ZU  DO     i=ip11,mx1
c #ZU  ccniHY(i,j,k) = gat(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
C +
c #ZU  DO  j=jp11,my1
c #ZU  DO  i=ip11,mx1
c #ZU  ga0(i,j)      = qsrfHY(i,j)
c #ZO  ga0(i,j)      =   qsHY(i,j,mz)
c #ZU  END DO
c #ZU  END DO
C +
c #ZU  DO  k=1,mz
c #ZU  DO  j=jp11,my1
c #ZU  DO  i=ip11,mx1
c #ZU  gat(i,j,k)    =   qsHY(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
C +
C +    ****************
c #ZU  call DYNadv_cubv(gat,ga0)
C +    ****************
C +
c #ZU  DO     k=1,mz
c #ZU  DO     j=jp11,my1
c #ZU  DO     i=ip11,mx1
c #ZU    qsHY(i,j,k) = gat(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
C +
c #ZU end if
C +
C +
C +--Impact on Snow Erosion/Deposition
C +  =================================
C +
c #BS DO     j=jp11,my1
c #BS DO     i=ip11,mx1
c #BS   dh_sno = psigDY(i,j,mz)*(qsHY(i,j,mz)-qsrfHY(i,j))*dt*grvinv
C +...  dh_sno contains an implicit factor 1.d3[kPa-->Pa]/ro_Wat[kg/m2-->mWE]
C +            > 0 ==> Atmospheric Loss ==> Surface Gain
C +
c #BS   snohSL(i,j) = snohSL(i,j) + max(zero,dh_sno)
c #BS   snobSL(i,j) = snobSL(i,j) + min(zero,dh_sno)
c #BS   snowHY(i,j) = snowHY(i,j) +          dh_sno
c #BS END DO
c #BS END DO
C +
      return
      end
