 
 
      subroutine PHY_KDsvat(ihamr_KDs,nhamr_KDs)
 
C +------------------------------------------------------------------------+
C | MAR PHYSICS                                             7-06-2002  MAR |
C |   SubRoutine PHY_KDsvat    interfaces MAR        with        the       |
C |              DeRidder Soil Vegetation Atmosphere Transfer Scheme       |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables (MAR)
C +  ======================
C +
      include 'MARCTR.inc'
      include 'MARphy.inc'
C +
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
C +
      include 'MAR_DY.inc'
C +
      include 'MAR_HY.inc'
      include 'MAR_RA.inc'
C +
      include 'MAR_SL.inc'
      include 'MAR_SV.inc'
c #sn include 'MAR_SN.inc'
C +
      include 'MAR_WK.inc'
      include 'MAR_IO.inc'
C +
      integer  ihamr_KDs ,nhamr_KDs
      integer  newglfKDs
C +
C +
C +--Local  Variables
C +  ================
C +
      logical   glfFIX
      integer   n        ,ii       ,iprnt
C +
      real      czemin
      real      vdt      ,vVAir    ,vTAir    ,vQAir    ,argqs ,vqsat
      real      vRadS    ,vRadL    ,crain    ,vRain    ,rhAir ,psrfc
      real      vzAir    ,cosZA
      real      vTsBr    ,vTsIR    ,vAlbS    ,vEmiS
      real      vTa_S    ,vQa_S    ,vHS_S    ,vHL_S    ,vTauS
C +
C +
C +--DATA
C +  ====
C +
      data glfFIX/ .false.   /
      data czemin/0.10000e-03/
C +
C +
C +--Update Green Leaf Fraction
C +  ==========================
C +
                                           glfFIX = .true.
      IF (vegmod .AND. reaLBC .AND.  .NOT. glfFIX)                THEN
C +
C +          ******
        call INIglf(ihamr_KDs,nhamr_KDs,newglfKDs)
C +          ******
C +
      END IF
C +
C +
C +--Surface Physics
C +  ===============
C +
      DO j=jp11,my1
      DO i=ip11,mx1
C +
                                                     IO_loc = IO_gen
        if (IO_gen.ge.1.and.jmmMAR.eq.0.and.jssMAR.eq.0) then
         do 60 ii=1,5
          if (igrdIO(ii).eq.i.and.(jgrdIO(ii).eq.j)) IO_loc = IO_gen+2
  60     continue
        end if
C +
       go to (1,2,3,4,4) isolSL(i,j)
C +
C +--Ocean
C +  ~~~~~
 1     continue
C +
C +          ***************
c #FR        call SRFfrm_sea
C +          ***************
C +
       go to 10
C +
C +--Sea Ice
C +  ~~~~~~~
 2     continue
C +
C +          ****************
c #FR        call SRFfrm_sice
C +          ****************
C +
       go to 10
C +
C +--Ice  + Snow
C +  ~~~~~~~~~~~
 3     continue
C +
c #sn   if (snomod) then
C +
C +          ***************
c #sn        call SRFmod_sno
C +          ***************
C +
c #sn   else
C +
C +          ***************
c #FR        call SRFfrm_sno
C +          ***************
C +
c #sn   end if
C +
       go to 10
C +
C +--Soil + Snow
C +  ~~~~~~~~~~~
 4     continue
C +
c #sn   IF      (snomod)                                          THEN
C +
C +          ***************
c #sn        call SRFmod_sno
C +          ***************
C +
c #sn   ELSE
C +
C +--Vegetation
C +  ~~~~~~~~~~
c #SV     IF     (vegmod)                                         THEN
C +
c #SV               vdt= dtPhys
C +
c #SV             vVAir=  ssvSL(i,j,mz)
c #SV             vTAir= tairDY(i,j,mz)
c #SV             vQAir=   qvDY(i,j,mz)
c #SV             argqs= 17.27e0*(vTAir-273.e0)/(vTAir-36.0e0)
c #SV             vqsat=  3.8e-3*exp(argqs)
c #SV         IF (vqsat.LE.vQAir) vQAir=vqsat
C +
c #SV             vRadS= sol_SL(i,j) / (1.-albeSL(i,j))
c #SV             RAdsol(i,j)= vRadS
c #SV             vRadL= RAd_ir(i,j)
C +
                  crain= rainHY(i,j)
c #SV             vRain=(crain      -rai0HY(i,j))*1000./dtPhys
                         rai0HY(i,j)=rainHY(i,j)
C +
c #SV             rhAir= rolvDY(i,j,mz)                 *1.0e+3
c #SV             psrfc=( pstDY(i,j)+ptopDY)            *1.0e+3
c #SV             vzAir=(gplvDY(i,j,mz)-gplvDY(i,j,mzz))*grvinv
C +
c #SV                          cosZA =     czenGE(i,j)
c #SV         IF (vRadS.GT.0.) cosZA = max(cosZA,czemin)
c #SV         if (jmmMAR.eq.0.and.jssMAR.eq.0)                    THEN
c #SV             iprnt   =  1
c #SV         ELSE
c #SV             iprnt   =  0
c #SV         END IF
C +
C +           *********
c #SV         call svat
c #SV.          (vvair,vTAir,vQAir,vRadS,vRadL,vRain,cosZA,rhAir,vzAir,
c #SV.           i    ,j    ,iprnt,vdt  ,
c #SV.           vTsBr,vTsIR,vAlbS,vEmiS,vTa_S,vQa_S,vHS_S,vHL_S,vTauS)
C +           *********
C +
c #SV         eps0SL(i,j)  =     vEmiS
c #SV         tsrfSL(i,j,1)=     vTa_S
c #SV         tviRA (i,j)  =     vTsIR
c #SV         qvapSL(i,j)  = min(vQa_S,qvswDY(i,j,mzz))
c #SV         SLuqsl(i,j,1)=    -vHL_S/(rhAir*Lv_H2O)
c #SV         hlatSL(i,j)  =     vHL_S
c #SV         SLutsl(i,j,1)=    -vHS_S/(rhAir*cp)
c #SV         hsenSL(i,j)  =     vHS_S
c #SV         SLuusl(i,j,1)=       sqrt(vTauS/rhAir)
c #SV         albeSL(i,j)  =     vAlbS
C +
c #SV     ELSE
C +
C +--Soil
C +  ~~~~
C +              ****************
c #FR            call SRFfrm_soil
C +              ****************
C +
c #SV     END IF
C +
c #sn   END IF
C +
       go to 10
C +
 10    continue
C +
      END DO
      END DO
                                                     IO_loc = IO_gen
C +
C +
C +--Lateral Boundary Conditions Surface Variables
C +  =============================================
C +
c #SA IF(.not.sALONE) THEN
        DO n=1,mw
          DO j=1,my
            tsrfSL( 1,j,n) = tsrfSL(ip11,j,n)
            tsrfSL(mx,j,n) = tsrfSL( mx1,j,n)
             dtgSL( 1,j,n) = 0.
             dtgSL(mx,j,n) = 0.
          END DO
         IF (mmy.GT.1)                                            THEN
          DO i=1,mx
            tsrfSL(i, 1,n) = tsrfSL(i,jp11,n)
            tsrfSL(i,my,n) = tsrfSL(i, my1,n)
             dtgSL(i, 1,n) = 0.
             dtgSL(i,my,n) = 0.
          END DO
         END IF
        END DO
c #SA ELSE
c #SA   DO n=1,mw
c #SA     DO j=1,my
c #SA        dtgSL( 1,j,n) =  dtgSL(ip11,j,n)
c #SA        dtgSL(mx,j,n) =  dtgSL( mx1,j,n)
c #SA     END DO
c #SA     DO i=1,mx
c #SA        dtgSL(i, 1,n) =  dtgSL(i,jp11,n)
c #SA        dtgSL(i,my,n) =  dtgSL(i, my1,n)
c #SA     END DO
c #SA   END DO
c #SA END IF
C +
      return
      end
