C +************************************************************************+
C |                                                                        |
C |                                                                        |
C |                                                                        |
C |                                                                        |
C |                                                                        |
C |                                                                        |
C |                                                                        |
C |               M        M       AAAAAAA        RRRRRRR                  |
C |               MM      MM      A       A      R       R                 |
C |               M M    M M      A       A      R       R                 |
C |               M  M  M  M      A       A      R       R                 |
C |               M   MM   M      A AAAAA A      R RRRRR R                 |
C |               M        M      A       A      R R                       |
C |     (MODELE tridimensionnel ATMOSPHERIQUE a l'echelle REGIONALE)       |
C |               M        M      A       A      R   R                     |
C |               M        M      A       A      R    R                    |
C |               M        M      A       A      R     R                   |
C |               M        M      A       A      R      R                  |
C |                                                                        |
C |                 \__ _                    ____ /                        |
C |               \_/     @@@@              /    \                         |
C |               / \    @@@@@@            /    / \                        |
C |              /   \  @@@@@@            |    /   |                       |
C |                    ... ***             \  /   /    LGGE/CNRS           |
C |                   .... **               \/___/                         |
C |                  .... **                / IAG/UCL                      |
C |                                                                        |
C |   Laboratoire de Glaciologie et de G�ophysique de l'Environnement      |
C |      Institut d'Astronomie   et de Geophysique Georges Lema�tre        |
C |   Laboratoire d'etudes des Transferts en Hydrologie et Environnement   |
C |                                                                        |
C |                                                                        |
C |                                                                        |
C |                                                                        |
C |                                                                        |
C |                                                                        |
C |                                                                        |
C +************************************************************************+
C |                                                                        |
C | Version MARv3.6.3    4 May 2007 (HG) + 31 Aug 2016 (XF)                |
C | ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                  |
C
C Tunning
C =======
C
C 1. To have more precipitation, humidty and LWD:
C     - FacFIk=10.00 instead of FacFIk=05.00 in MAR_FI.inc
C     - cnos,cnos2 (#ur,#up, ...) in hydmic.f impact the ratio inland/margin pre
C       and the amount of precipitation
C     - uncomment the 2 lines BUGBUG in DYNadv_LFB_2p
C
C 2. TURtke_difv(dtLLoc,0.5) instead of TURtke_difv(dtLLoc,0.0) increases
C the temperature over Tundra and decreases over the ice sheet in summer but MAR
C is less stable. In winter, TURtke_difv(dtLLoc,0.5) is colder.
C
C 3. qsmlt =     psmlt * dt in Hydmic (option #GL) increases the ratio RF/SF
C
C 4. To have more SWD/LWD (in PHYrad_CEP.f)
C  (1.-min(1.,exp((tairDY(i,j,lkl)-258.15)*0.1))) instead of 273.15
C  * (                 qsHY(i,j        ,lkl)*XX where xx <<
C    lower mzhyd, larger the amount of clould is
C
C 5. If MAR is too warm in summer, decrease
C    sEX_sv(ikl,isn) to  0.95  in snoptp.f
c    FacFIk but this impacts the amount of precip
C
C 6. In cvagen_mnh, if OsetA0=T,
c higher PTdcv0 is, lower precip is
c lower PTscv0 is, lower precip is
c lower pdtCV0 is, higher precip is
 
C WARNING
C =======
c
c 1. Be carfull about itexpe. Itexpe is interger*8 by default. This asks to have
c as integer*8 in NESTOR (for the MARdyn.dat used when itexpe=0).
c
C +************************************************************************+
C |                                                                        |
C |     MAR CONTRIBUTORS:                                                  |
C |                                                                        |
C | --- Hubert Gallee     : Development of the Dry Model Version...........|
C |                         Explicit Hydrological Cycle Revision...........|
C |                         Energy  Snow     Model.........................|
C |                         Blowing Snow     Model.........................|
C |                         Polynya          Model.........................|
C |                         Generic Chemical Model.........................|
C |     Adress : LGGE/CNRS, 54, rue Moli�re, BP96, ........................|
C |              F-38402 Saint-Martin d'H�res CEDEX, ......................|
C |     e-mail : gallee@lgge.obs.ujf-grenoble.fr ..........................|
C |                                                                        |
C | --- Olivier Brasseur  : Mass Flux convective Scheme: Development ......|
C |                         2 1/2 Turbulence Parameterization..............|
C |                         NESTing Organization for Regional models ......|
C |     Adress : Cellule interregionale de l'Environnement ................|
C |              10-11, avenue des Arts, B-1210 Bruxelles .................|
C |     e-mail : Brasseur@irceline.fr .....................................|
C |                                                                        |
C | --- Laurent Delobbe   : Explicit Hydrological Cycle Revision...........|
C |                         StratoCumulus Experiment.......................|
C |     Adress : Royal Meteorological Institute of Belgium, ...............|
C |              Observations Department, Av. Circulaire 3  1180 Brussels .|
C |     e-mail : Laurent.Delobbe@oma.be ...................................|
C |                                                                        |
C | --- Koen De Ridder    : Treatment of Processes Relative to Vegetation..|
C |     Adress : Vlaams Instelling voor Technische Onderzoek, .............|
C |                   Boeretang 200, B-2400 Mol, Belgie ...................|
C |     e-mail : DRidderK@vito.be .........................................|
C |                                                                        |
C | --- Xavier Fettweis   : Energy  Snow/Ice Model.........................|
C |     Adress : Institut d'Astronomie et de Geophysique G.Lemaitre........|
C |                   2, chemin du Cyclotron, B-1348 LLN, Belgique.........|
C |     e-mail : fettweis@astr.ucl.ac.be...................................|
C |                                                                        |
C | --- Filip Lefebre     : Energy  Snow/Ice Model.........................|
C |     Adress : Vlaams Instelling voor Technische Onderzoek, .............|
C |                   Boeretang 200, B-2400 Mol, Belgie ...................|
C |     e-mail : filip.lefebre@vito.be ....................................|
C |                                                                        |
C | --- Philippe Marbaix  : Map projection and nesting interpolation.......|
C |                         NW Europe Climate, Post-processing.............|
C |     Adress : Laboratoire des Sciences du Climat et de l'Environnement..|
C |     e-mail : philippe.marbaix@lsce.saclay.cea.fr.......................|
C |                                                                        |
C | --- Christophe Messager:  West African Climate / HCM Coupling..........|
C |     Adress : IRD/LTHE, BP53, F-38041 Grenoble CEDEX 9, ................|
C |     e-mail : messager@hmg.inpg.fr .....................................|
C |     URL    : http://www.lthe.hmg.inpg.fr/GRID .........................|
C |                                                                        |
C | --- Wilfran Moufouma-Oka: West African Climate / Interannual Variab....|
C |     Adress : IRD/LTHE, BP53, F-38041 Grenoble CEDEX 9, ................|
C |     e-mail : moufouma@hmg.inpg.fr .....................................|
C |                                                                        |
C | --- Romain  Ramel:        West African Climate / MCC and Convection....|
C |     Adress : IRD/LTHE, BP53, F-38041 Grenoble CEDEX 9, ................|
C |     e-mail :    ramel@hmg.inpg.fr .....................................|
C |                                                                        |
C | --- Christian Tricot  : Solar/IR Radiative Routines Implementation ....|
C |     Adress : Royal Meteorological Institute of Belgium, ...............|
C |              Climatology  Department, Av. Circulaire 3  1180 Brussels .|
C |     e-mail : clim@atmos.oma.be ........................................|
C |                                                                        |
C | --- Emilie Vanvyve    : European Hydrology and Climate.................|
C |     Adress : Institut d'Astronomie et de Geophysique G.Lemaitre........|
C |                   2, chemin du Cyclotron, B-1348 LLN, Belgique.........|
C |     e-mail : vanvyve@astr.ucl.ac.be....................................|
C |                                                                        |
C +************************************************************************+
C |                                                                        |
C | REFERENCES :                                                           |
C | ^^^^^^^^^^^^                                                           |
C | + Gallee H. and Schayes G.,                                      1992. |
C |   Dynamical Aspects of Katabatic Winds Evolution in the                |
C |   Antarctic Coastal Zone.                                              |
C |   Boundary Layer Meteorology                      59,  141--161        |
C |                                                                        |
C | + Thunis, P., Grossi, P., Graziani, G., Gallee, H., Moyaux, B.,        |
C |   and Schayes G.,                                                1992. |
C |   Preliminary simulations of the flow field over the Attic Peninsula   |
C |   Environmental Software                           8,   43--54         |
C |                                                                        |
C | + Gallee H. and Schayes G.,                                      1994. |
C |   Development of a Three-Dimensional Meso--gamma                       |
C |   Primitive Equations Model,                                           |
C |   Katabatic Winds Simulation in the area of Terra Nova Bay, Antarctica |
C |   Monthly Weather Review                         122,  671--685        |
C |                                                                        |
C | + Gallee, H.,                                                    1995. |
C |   Simulation of the mesocyclonic activity in the Ross Sea,  Antarctica |
C |   Monthly Weather Review                         123, 2051--2069       |
C |                                                                        |
C | + Gallee, H., Fontaine de Ghelin, O., and van den Broecke, M.,   1995. |
C |   Simulation of Atmospheric Circulations during GIMEX 91 Experiment    |
C |   using a Meso-gamma Primitive Equations Model                         |
C |   Journal of Climate                               8, 2843--2869       |
C |                                                                        |
C | + Gallee, H.,                                                    1996. |
C |   Possible Influence of Mesocyclonic Activity on Snow Precipitation    |
C |   in the Antarctic Coastal Zone                                        |
C |   Zeitschrift fur Gletscherkunde und Glazialgeologie 31, 17--24        |
C |                                                                        |
C | + Gallee, H.,                                                    1996. |
C |   Mesoscale atmospheric circulations                                   |
C |   over the southwestern Ross Sea sector, Antarctica,                   |
C |   Journal of Applied Meteorology                  35, 1142--1152       |
C |                                                                        |
C | + Gallee, H., P. Pettre, and G. Schayes,                         1996. |
C |   Sudden Cessation of Katabatic Wind in Adelie Land, Antarctica        |
C |   Journal of Applied Meteorology                  35, 1129--1141       |
C |                                                                        |
C | + van den Broecke, M. and Gallee, H.,                            1996. |
C |   Observation and Simulation of barrier winds                          |
C |   at the western margin of the Greenland ice sheet,                    |
C |   Quart. J. R. M. Soc.                           122, 1365--1383       |
C |                                                                        |
C | + Gallee, H.,                                                    1997. |
C |   Air-Sea Interactions over Terra Nova Bay during Winter:              |
C |   Simulation with a coupled Atmosphere-Polynya Model                   |
C |   Journal of Geophysical Research              102, 13813--13824       |
C |                                                                        |
C | + Gallee, H., and P. Duynkerke,                                  1997. |
C |   Air-Snow Interactions and the Surface Energy and Mass Balance        |
C |   over the Melting Zone of West Greenland during GIMEX                 |
C |   Journal of Geophysical Research              102, 13835--13849       |
C |                                                                        |
C | + Gallee, H., and P. Pettre,                                     1998. |
C |   Dynamical Constraints on Katabatic Wind Cessation in Adelie Land,    |
C |   Antarctica                                                           |
C |   Journal of Atmospheric Sciences               55,  1755--1770        |
C |                                                                        |
C | + Gallee, H.,                                                    1998. |
C |   A simulation of blowing snow over the Antarctic ice sheet            |
C |   Annals of Glaciology                          26,   203--206         |
C |                                                                        |
C | + Brasseur, O., C. Tricot, H. Gallee, K. De Ridder                     |
C |   and G. Schayes,                                                1998. |
C |   Impact of turbulent closure, dynamics and grid resolution            |
C |   on diurnal temperature evolution for clear sky situations            |
C |   over Belgium using a three-dimensional mesoscale model               |
C |   Boundary Layer Meteorology                    87,   163--193         |
C |                                                                        |
C | + Delobbe, L.,  and H. Gallee,                                   1998. |
C |   Simulation of marine Stratocumulus:                                  |
C |   Effect of Precipitation Parameterization and                         |
C |   Sensitivity to Droplet Number Concentration                          |
C |   Boundary Layer Meteorology                    89,    75--107         |
C |                                                                        |
C | + De Ridder,K., and H. Gallee,                                   1998. |
C |   Land Surface-induced regional climate change in Southern Israel      |
C |   Journal of Applied Meteorology                37,  1470--1485        |
C |                                                                        |
C | + Delobbe, L.,  and H. Gallee,                                   2000. |
C |   A sensitivity study of the subtropical ocean surface energy balance  |
C |   to the parameterization of precipitation from stratocumulus clouds.  |
C |   Boundary Layer Meteorology                    94,   399--422         |
C |                                                                        |
C | + Gallee H., G. Guyomarc'h, and E. Brun                          2001. |
C |   Impact of the snow drift                                             |
C |   on the Antarctic ice sheet surface mass balance:                     |
C |   possible sensitivity study to snow surface properties                |
C |   Boundary Layer Meteorology                    99,     1--19          |
C |                                                                        |
C | + Brasseur, O.                                                   2001. |
C |   Development and application of a physical approach                   |
C |   to estimate wind gusts.                                              |
C |   Monthly Weather Review                       129,     5--25          |
C |                                                                        |
C | + Gallee H., G. Wendler and O. Brasseur                          2001. |
C |   A simulation of an extreme katabatic wind event.                     |
C |   Meteorology and Atmospheric Physics           (submitted)            |
C |                                                                        |
C | + Brasseur O., Gallee H., JD. Creutin, T. Lebel, and P. Marbaix  2002. |
C |   High resolution simulations of precipitation over the Alps           |
C |   with the perspective of coupling to hydrological models.             |
C |   Advances in Global Change Research, 10 (M. Beniston, Ed.), 75--100   |
C |                                                                        |
C | + Brasseur O., Gallee H., Boyen H., and Tricot C.,               2002. |
C |   Reply to comments on : Development and application                   |
C |   of a physical approach to estimate wind gusts                        |
C |   Monthly Weather Review                       130,  1936--1942        |
C |                                                                        |
C | + Lefebre, F., H. Gallee, J.-P. van Ypersele and P. Huybrechts,  2002. |
C |   Modelling of large-scale melt parameters with a regional climate     |
C |   model in South Greenland during the 1991 melt season                 |
C |   Annals of Glaciology                          35,   391--397         |
C |                                                                        |
C | + Lefebre, F., H. Gallee, J.-P. van Ypersele and W. Greuell,     2003. |
C |   Modelling of  snow and ice melt at ETH-Camp (West Greenland):        |
C |   a study of surface albedo                                            |
C |   Journal of Geophysical Research       108(D8),  10.1029/2001JD001160 |
C |                                                                        |
C | + Naithani, J., H.Gallee and G. Schayes,                         2002. |
C |   Marine air intrusion into the Adelie land sector of east Antarctica  |
C |   A study using Regional Climate Model (MAR)                           |
C |   Journal of Geophysical Research       107(D11), 10.1029/2000JD000274 |
C |                                                                        |
C | + Marbaix, P., H. Gallee, O. Brasseur, and J.-P. van Ypersele,   2003. |
C |   Lateral Boundary Conditions in regional climate models:              |
C |   a detailed study of the relaxation procedure                         |
C |   Monthly Weather Review                       131,   461--479         |
C |                                                                        |
C | + Messager, C., H. Gall�e and O. Brasseur,                       2004. |
C |   Precipitation sensitivity to regional SST                            |
C |   in a regional climate simulation                                     |
C |   during the West African Monsoon for two dry years                    |
C |   Climate Dynamics           22:249-266, doi 10.1007/s00382-003-0381-x |
C |                                                                        |
C | + Gall�e H., W. Moufouma-Okia, P. Bechtold, O. Brasseur, I. Dupays     |
C |   P. Marbaix, C. Messager R. Ramel and T. Lebel                  2004. |
C |   A high resolution simulation of a West African rainy season          |
C |   using a regional climate model                                       |
C |   Journal of Geophysical Research 109 D05108, doi 10.1029/2003JD004020 |
C |                                                                        |
C | + Fettweis, X., H. Gall�e, F. Lefebre, and J. van Ypersele       2005. |
C |   Greenland Surface Mass Balance simulated by a Regional Climate Model |
C |   and Comparison with satellite derived data in 1990-1991.             |
C |   Climate Dynamics                       doi 10.1007/s00382-005-0010-y |
C |                                                                        |
C | + Gall�e H., V. Peyaud and I. Goodwin                            2005. |
C |   Temporal and spatial variability of the Antarctic Ice Sheet          |
C |   Surface Mass Balance assessed from a comparison between              |
C |   snow stakes measurements and regional climate modeling.              |
C |   Annals of Glaciology                          41,    17--22.         |
C |                                                                        |
C | + Lefebre, F., X. Fettweis, H. Gall�e, J. van Ypersele, P. Marbaix,    |
C |   W. Greuell, and P. Calanca                                     2005. |
C |   Evaluation of a high-resolution regional climate simulation          |
C |   over Greenland.                                                      |
C |   Climate Dynamics                       doi 10.1007/s00382-005-0005-8 |
C |                                                                        |
C | + Fettweis, X., H. Gall�e, F. Lefebre, and J. van Ypersele       2006. |
C |   The 1988-2003 Greenland ice sheet melt extent using                  |
C |   passive microwave satellite data and a regional climate model        |
C |   Climate Dynamics                       doi 10.1007/s00382-006-0150-8 |
C |                                                                        |
C | + Messager, C., H. Gall�e, O. Brasseur, B. Cappelaere, C. Peugeot,     |
C |   L. S�guis, M. Vauclin, R. Ramel, G. Grasseau, Laurent L�ger and      |
C |   D. Girou.                                                      2006. |
C |   Influence of observed and RCM-simulated precipitation on the water   |
C |   discharge over the Sirba basin, Burkina Faso/Niger                   |
C |   Climate Dynamics                       doi 10.1007/s00382-006-0131-y |
C |                                                                        |
C | + Ramel, R., H. Gallee and C. Messager                           2006. |
C |   On the northward shift of the West African Monsoon                   |
C |   Climate Dynamics                       doi 10.1007/s00382-005-0093-5 |
C |                                                                        |
C | + M.R. Swain and H. Gall�e                                       2006. |
C |   Antarctic Boundary Layer Seeing                                      |
C |   Astronomical Society of the Pacific          118,  1190--1197.       |
C |                                                                        |
C | + Fettweis, X., J. van Ypersele, H. Gall�e, F. Lefebre, and            |
C |   W. Lefebvre                                                    2007  |
C |   The 1979-2005 Greenland ice sheet melt extent                        |
C |   from passive microwave data using an improved version                |
C |   of the melt retrieval XPGR algorithm                                 |
C |   Geophys. Res. Lett.,       34, L05502, doi:10.1029/2006GL028787.     |
C |                                                                        |
C | + Gallee H.                                                      2006. |
C |   Validation of a Regional Climate Model over East Antarctica          |
C |   Climate Dynamics                              (in preparation)       |
C |                                                                        |
C +************************************************************************+
C |                                                                        |
C | SUMMARY : THE MODEL USES THE FULL COMPRESSIBLE PRIMITIVES EQUATIONS    |
C | ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^    |
C |                                                                        |
C | Vertical Coordinate : Normalized Pressure Sigma                        |
C | Horizontal Grid     : Arakawa A-grid                                   |
C |                       (Purser and Leslie, MWR 116, 2069--2080, 1988)   |
C | Modes : 1-Dimensional (mx=1,my=1,mz) OR                                |
C | ^^^^^^^ 2-Dimensional (mx  ,my=1,mz) OR                                |
C |         3-Dimensional (mx  ,my  ,mz)                                   |
C |             Condition  mx > my       must be fulfilled in this case    |
C |                       (cfr."MARdim.inc")                               |
C |                                                                        |
C +************************************************************************+
C |                                                                        |
C | THE FILE MAR___.FOR contains the BASIC SOURCE CODE                     |
C | ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                     |
C |                                                                        |
C | It may be used as is in 1-D, 2-D or 3-D Mode                           |
C | It may be modified in order to allow other possibilities.              |
C |    Modifications are performed by replacing labels `c #XY' by blanks.  |
C |   (see Preprocessor MAR_pp.for)                                        |
C |                                                                        |
C | #    MAIN       OPTIONS:                                               |
C | #^^^^^^^^^^^^^^^^^^^^^^^                                               |
C |                                                                        |
C | #     ADDITIONAL OPTIONS: Dynamics                                     |
C | #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                     |
C |                                                                        |
C | #     ADDITIONAL OPTIONS: Sea, Polynya and Snow Models                 |
C | #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                 |
C |                                                                        |
C | #     ADDITIONAL OPTIONS:                                              |
C | #^^^^^^^^^^^^^^^^^^^^^^^^                                              |
C |                                                                        |
C | #     ADDITIONAL OPTIONS: BOUNDARY CONDITIONS                          |
C | #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                          |
C |                                                                        |
C | #     ADDITIONAL OPTIONS: CONVECTIVE ADJUSTMENT                        |
C | #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                        |
C |                                                                        |
C | #     ADDITIONAL OPTIONS: VERTICAL  TURBULENCE                         |
C | #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                         |
C |                                                                        |
C | #     ADDITIONAL OPTIONS: SURFACE LAYER                                |
C | #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                |
C |                                                                        |
C | #     ADDITIONAL OPTIONS: HORIZONTAL DIFFUSION                         |
C | #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                         |
C |  !. `Standard' Horizontal Diffusion is performed on Sigma Surfaces     |
C |  !.  Smagorinski Relation (see Tag et al. 1979, JAM 18, 1429--1441)    |
C |  !.  CAUTION: Horizontal Diffusion is switched on with turhor = .true. |
C |  2.      _PE, _HH,     : Diffus.on Sigma Surfaces (%Grad.) +Vert.Corr. |
C |  3.      _PE, _HH, #CR : Diffus.on Sigma Surfaces (%Grad.) +all  Corr. |
C |  4. #DF, #PE,          : Diffus.on Sigma Surfaces (%Strain)+Vert.Corr. |
C |  5. #DF, #PE, #DC, #CR : Diffus.on Sigma Surfaces (%Strain)+all  Corr. |
C |                          (#DC -> u,v; #CR -> other Variables)          |
C |  CAUTION: IF #QE, THEN #qe MUST BE SWITCHED ON before 2, 3, 4 OR 5     |
C |  ^^^^^^^  IF #HY, THEN #se MUST BE SWITCHED ON before 2, 3, 4 OR 5     |
C |                                                                        |
C +************************************************************************+
 
 
      program MAR
C +   ^^^^^^^^^^^
 
c #AO USE mod_prism_proto !
c #AO USE mar_module      !TANGO modules
c #AO USE flincom         !
 
      IMPLICIT NONE
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
 
      include 'MARSND.inc'
 
      include 'MAR_DY.inc'
      include 'MARqqm.inc'
c #NH include 'MAR_NH.inc'
c #OL include 'MAR_OL.inc'
 
      include 'MAR_LB.inc'
      include 'MAR_UB.inc'
c #Di include 'MAR_DI.inc'
 
      include 'MAR_TE.inc'
      include 'MAR_TU.inc'
c #BR include 'MAR_BR.inc'
      include 'MAR_CA.inc'
      include 'MAR_FI.inc'
 
      include 'MAR_HY.inc'
c #TC include 'MAR_TC.inc'
      include 'MAR_RA.inc'
 
      include 'MAR_SL.inc'
c #AO include 'MAR_AO.inc'
 
      include 'MAR_WK.inc'
 
      include 'MAR_SV.inc'
      include 'MARlSV.inc'
      include 'MARsSN.inc'
      include 'MAR_IB.inc'
      include 'MAR_IO.inc'
 
      include 'radCEP.inc'
 
C +--Local  Variables
C +  ================
 
      include 'MARvec.inc'
      integer  norder_0
      integer  newlbc_0
      real     rhcrit_0
      real     tstart_0
 
      character*3 DYNadv
 
c #MR logical     RadMAR
c #LL logical     RadLMD
 
c #OL logical     TURver
 
      real    dt_inv,dtLLoc,dtDifH,dt_Out,deltaF,cfladv,csnd
      real    hham  ,hhac  ,fham  ,thac  ,argham,hhhnnn,tdt   ,afdt
      real    pav   ,ppp   ,wwwabs,wwwmax,ectnew,pente ,dthdz ,adum ,adu
      real    pnhLav,pnh_av
 
      integer n     ,mlg   ,mlh   ,mlm   ,mld   ,iargum,i__min,i__max
      integer kk    ,kdim  ,ksig  ,iv    ,iw
      integer nt_Loc,jt_Loc,it_Loc,ntLLoc,itLLoc,itPhys
      integer iham  ,nham  ,ihamr ,nhamr ,jham  ,ibd
      integer jmmd  ,jm10  ,jh10  ,jh1   ,jd10  ,jd1
      integer iteChi
      integer iprint,log_nc
      integer iout  ,idum  ,jdum  ,id6   ,i_wmax,j_wmax,k_wmax
      integer ntracr,lotrac
 
      logical           ntFlog                              ! Auxil.Variables
      integer           nt_BAK,nt_sig                       !(variable nt_Mix)
      real              VLoc  ,VLocmx,rtFact,CFLinv,TLocmn  !
      integer           iLocmx,jLocmx,kLocmx                !
 
      REAL              DistST
C +...                  DistST:   Normalized Earth's Sun Distance
 
      character* 8      ttime
 
 
C +--Lateral Boundary Conditions
C +  ---------------------------
 
c #RB real      dumy3U(mx,my,mz)
c #RB real      dumy3V(mx,my,mz)
 
 
C +--Vertically Integrated Normalized Mass Flux
C +  ------------------------------------------
 
      real      fu(mx,my),fv(mx,my)
 
 
C +--Hamming Filter
C +  --------------
 
c #HF real      uuuham(mx,my,mz),vvvham(mx,my,mz)
c #HF real      pktham(mx,my,mz),qvaham(mx,my,mz),pnnham(mx,my)
C +...Hamming Filter variables for use in the Initialisation
C +   (Lynch and Huang 1992, MWR 120, 1019--1034)
 
 
C +--Atmospheric Water: Mass Conservation
C +  ------------------------------------
 
      real      dumy3Q(mx,my,mz)
 
c #WB real         dt_Bal
 
 
C +--Tracers:           Mass Conservation
C +  ------------------------------------
 
c #MV character*15 latrac(4)
c #MV real         totrac(4)
c #MV real         tracmx(4)
c #MV integer      itrcmx(4)
c #MV integer      jtrcmx(4)
c #MV integer      ktrcmx(4)
 
 
C +--Machine Precision
C +  -----------------
 
       REAL   reamin,reamax
 
 
C +--IO
C +  --
 
      real      zza(5)
      real      tta(5)
 
 
C +--DATA
C +  ====
 
c #MV data  (latrac(n),n=1,4)
c #MV.      /'Before Hor. ADV','Before Ver. ADV',
c #MV.       'Before Subgrids','After  Subgrids'/
 
 
C +--Flags
C +  =====
 
      openmp    = .false.
      openmp    = .true.
 
c #SA sALONE    = .true.
c #GE geoNST    = .true.
c #MR RadMAR    = .true.
c #LL RadLMD    = .true.
c #OL TURver    = .FALSE.
 
      ini_KA_TE = .false.
 
      VSISVAT   = .true.
c #FR VSISVAT   = .FALSE.
c #SV VSISVAT   = .FALSE.
      iniIRs    = .false.
      iniOUT    = .false.
 
C +--Blowing Snow
C +  ~~~~~~~~~~~~
       BloMod=.FALSE.
c #AE  BloMod=.true.
 
C +--Advection
C +  ~~~~~~~~~
       DYNadv='LFB'
c #UW  DYNadv='UPW'
 
       no_vec=.false.
       no_vec=.true.
       ntFlog=.FALSE.
 
       openLB=.FALSE.
c #OB  openLB=.true.
 
       sommlb=.FALSE.
c #RB  sommlb=.true.
 
       SBLitr=.true.
c #BR  SBLitr=.true.
C +... SBLitr=.true. ==> SBL is iterated
 
       tur_25=.FALSE.
c #BR  tur_25=.true.
 
 
C +--CONSTANTS
C +  =========
 
      ntracr = 0
c #TC ntracr = ntrac
 
 
C +--Grid Constants
C +  --------------
 
      rxy= 1.e-6/(mx*my)
      m  = mx
      m1 = m -1
      m2 = m -2
      m3 = m -3
      m4 = m -4
      mn3= mn-3
      mn4= mn-4
c #OL mlg= mz- 5
c #OL mlh= mz-19
c #OL mlm= max(1,mlh)
c #OL mld= mlm - mlh
c #OL mlg= mlg + mld
c #OL mlh= mlm
 
      ttime='        '
 
      write(6,6)
 6    format(
     .     /,"                                               "
     .      ,"                   "
     .    ,/," **********************************************"
     .      ,"****************** "
     .    ,/," *                                             "
     .      ,"                 * "
     .    ,/," *  MM      MM   AAAA   RRRRRR      OOOO   NN  "
     .      ,"    NN HH    HH  * "
     .    ,/," *  MMMM  MMMM AA    AA RR    RR  OO    OO NNNN"
     .      ,"    NN HH    HH  * "
     .    ,/," *  MM  MM  MM AAAAAAAA RRRRRR    OO    OO NN  "
     .      ,"NN  NN HHHHHHHH  * "
     .    ,/," *  MM      MM AA    AA RR  RR    OO    OO NN  "
     .      ,"  NNNN HH    HH  * "
     .    ,/," *  MM      MM AA    AA RR    RR    OOOO   NN  "
     .      ,"    NN HH    HH  * "
     .    ,/," *                                             "
     .      ,"                 * "
     .    ,/," *   Mod�le Atmosph�rique R�gional A Option Non"
     .      ,"-Hydrostatique   * "
     .    ,/," *                                             "
     .      ,"                 * "
     .    ,/," **********************************************"
     .      ,"****************** "
     .    ,/,"                                               "
     .      ,"                   "
     .    ,/,"                     - MARv3.6.2 - 24/03/2016 -"
     .      ,"                   "
     .    ,/,"                                               "
     .      ,"                   ")
 
      write(6,7) "    mx=",mx
      write(6,7) "    my=",my
      write(6,7) "    mz=",mz
      write(6,7) "    mw=",mw
      write(6,7) "  nsno=",nsno
      write(6,7) "mzabso=",mzabso
      write(6,7) " mzhyd=",mzhyd
 
7     format(a7,i4)
 
C +--Machine Precision
C +  =================
 
C +        ******
      call relrea
C +        ******
 
 
C +--Min and Max Arguments of Function exp(x)
C +  ----------------------------------------
 
      argmin = log( rrmin)
      iargum =     argmin
      i__min =     iargum + 7
      argmax = log( rrmax)
      iargum =     argmax
      i__max =     iargum - 8
 
      write(6,600) argmin,i__min,argmax,i__max
 600  format(/,' Function  exp(x)    :   Arguments:',
     .       /,' Minimum Value       : ',e12.4, 5x,'==> (',i3,')',
     .       /,' Maximum Value       : ',e12.4, 5x,'==> (',i3,')')
 
      argmin =     i__min
      argmax =     i__max
 
 
C +--PHYSICAL DATA
C +  =============
 
C +        ******
      call PHYmar
C +        ******
 
 
C +--CONTROL PARAMETERS
C +  ==================
 
      open(unit=3, status='old',file='MARctr.dat')
      rewind 3
      read(3,31)reaVAR,reaLBC,safVAR
 31   format(l12)
      if (.NOT. reaVAR)geoNST=.FALSE.
      read(3,31)hamfil
C +...          hamfil: Initialisation based on Temporal Filtering  (Hamming)
      read(3,31)conmas
C +...          conmas: Initialis. Constrained (Mass            Conservation)
      read(3,31)potvor
C +...          potvor: Initialis. Constrained (Potent. Vortic. Conservation)
      read(3,31)brocam
      read(3,31)center
C +...          center=.T. => Pressure       Spatial Scheme  centered
      read(3,32)nordps
 32   format(i12)
C +...          nordps= 4  :  Pressure       Spatial Scheme Precision
      read(3,31)staggr
C +...          staggr=.T. =>                Vertical  Grid staggered
      read(3,31)turhor
      read(3,31)chimod
C +...          chimod=.F.: Atmospheric Chemical Model  turned OFF
C +             chimod=.T.: Atmospheric Chemical Model  turned ON
      read(3,31)convec
C +...          convec=.T.: Mass Flux convective Scheme turned ON
      read(3,31)micphy
C +...          micphy=.F.: only the dry model is run
C +             micphy=.T.: the explicit hydrological cycle is included
      read(3,31)fracld
C +...          fracld=.T.: Fraction.Cloudiness Scheme  turned ON
      read(3,33)rhcrit_0
                rhcrHY= rhcrit_0
C +...          rhcrHY: relative humidity critical saturation value
 
      IF       (rhcrHY.gt.1.0d+0)                                 THEN
        write(6,300)      rhcrHY
 300    format(/,' ***********************************',
     .            '********************************',
     .         /,' * Critical Humidity =',f6.2,
     .           ' [%] / new units: [-] => divide by 100 *',
     .         /,' ***********************************',
     .            '********************************',/,1x)
                rhcrHY =  rhcrHY * 1.0d-2
      END IF
 
      read(3,33)tstart_0
                tim_HY= tstart_0
C +...          tim_HY: hydrological cycle starting time (prefarably uses 0.)
 33   format(f12.4)
      read(3,34)cz0_GE
 34   format(d12.4)
      read(3,31)physic
C +...          physic: Physics   are   included
      read(3,31)vegmod
C +...          vegmod =.true. : Interactive SVAT             turned ON
      if (.not. physic) vegmod =.FALSE.
      read(3,31)snomod
C +...          snomod =.true. : Interactive Snow    Model    turned ON
      if (.not. physic) snomod =.FALSE.
      read(3,31)polmod
C +...          polmod =.true. : Interactive Polynya Dynamics turned ON
      read(3,33)hic0
C +...          hic0   : assumed initial sea-ice Thickness
      read(3,33)fxlead
C +...          fxlead : assumed initial minimal Leads Fraction
      read(3,31)qsolSL
C +...          qsolSL: Deardorff model for soil humidity
      read(3,33)dt
                dt_inv = 1.0 / dt
      read(3,32)nboucl
      read(3,32)nprint
      read(3,32)ntFast                                       !
      IF   (mod(ntFast,2).EQ.0) THEN                         !
        write(6,301)     ntFast                              !
 301    format(/,' ***********************************',     !
     .            '********************************',        !
     .         /,' * Value of   ntFast =',i6  ,              !
     .           ' is even (precluded) =====> 1 is added *', !
     .         /,' ***********************************',     !
     .            '********************************',/,1x)   !
                ntFast = ntFast + 1                          ! Fixed    ntFast
      END IF                                                 !
                nt_Mix =          1                          ! variable nt_Mix
 
      read(3,33)dtDiff
      read(3,33)dtPhys
C +...          dtPhys: Surface   Physics Time Step
      read(3,33)dtRadi
C +...          dtRadi: Radiation         Time Step
      read(3,34)rxbase
C +...          rxbase : Nudging        Coefficient
      read(3,34)rxfact
C +...          rxfact : Lateral Sponge Coefficient
      close(unit=3)
 
 
C +--New Control Parameters
C +  ----------------------
 
C +--Robert Time Filter
C +  ~~~~~~~~~~~~~~~~~~
c #rt Robert = 0.01                                      ! STD. Value = 0.01
                                                         ! MAX. Value = 0.20
C +--Non-Hydrostatic Dynamics
C +  ~~~~~~~~~~~~~~~~~~~~~~~~
c #NH csnd   = 330.
C +...csnd   : Prescribed Sound Speed (m/s)
 
c #NH c2NH   = csnd * csnd
 
C +--Chemical Model
C +  ~~~~~~~~~~~~~~
      lotrac = 0
c #TC lotrac = 1
C +
c #TC          jtAdvH = 1
c #TC          dtAdvH = dt
c #TC          jt_ODE = 1
c #TC if (.not.chimod) then
c #TC          dt_ODE = dt
c #TC          jt_ODE = 1
c #TC          nt_ODE = 1
c #TC          ikTC(1)= 1
c #TC end if
 
 
C +--Print Characteristic
C +  --------------------
 
      if (nprint.lt.0) then
       nprint =        - nprint
       log_nc = 1
      else
       log_nc = 0
      end if
       ipr_nc = 1
       npr_nc = 1      + nprint
C +... ipr_nc  (npr_nc): Netcdf Output File: Current No (Total Nb) of Prints
 
 
       nterun = nboucl * nprint
 
 
C +-- Initialize coupling  (cpl)
C +   ==========================
 
c #AO WRITE(6,*) 'Initialize coupling in MAR'           !cpl
 
C +        ******
c #AO CALL inicma                                       !cpl
C +        ******
 
c #AO WRITE(6,*) 'Coupling initialization done in MAR'  !cpl
 
 
C +--OUTPUT Files
C +  ============
 
      open(unit= 4,status='new',file='MARphy.out')
      rewind     4
 
      open(unit=21,status='new',file='MAR.log')
      rewind    21
 
 
C +--Katabatic Jump Diagnostics
C +  --------------------------
 
      IF (mx.gt.1.and.my.le.1)                                    THEN
        open(unit=22,status='unknown',file='MAR.uuu')
        rewind    22
        open(unit=23,status='unknown',file='MAR.ttt')
        rewind    23
        open(unit=24,status='unknown',file='MAR.ppp')
        rewind    24
      END IF
 
c #WE open(unit=25,status='unknown',file='MAR.TKE__VER')
c #WE rewind    25
 
c #SE open(unit=29,status='unknown',file='MAR.SNOW_VER')
c #SE rewind    29
 
 
C +--Dynamical Balance (Output in dynbil)
C +  ------------------------------------
 
c #DY open(unit=61,status='unknown',file='MAR.DY1')
c #DY rewind    61
c #DY open(unit=62,status='unknown',file='MAR.DY2')
c #DY rewind    62
c #DY open(unit=63,status='unknown',file='MAR.DY3')
c #DY rewind    63
c #DY open(unit=64,status='unknown',file='MAR.DY4')
c #DY rewind    64
c #DY open(unit=65,status='unknown',file='MAR.DY5')
c #DY rewind    65
c #DY open(unit=66,status='unknown',file='MAR.DY6')
c #DY rewind    66
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ INITIALISATION +++++++++++++++++++++++++++++++++++++++++++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
C +        ******
      call INIgen
C +        ******
 
 
C +        **********
c #DB call Debugg_MAR('INIgen    ')
C +...     Debugg_MAR: cfr. DEBUGG.d Directory
C +        **********
 
 
C +--PBL Initialization Parameter
C +  ============================
 
      IF (itexpe.le.0)                                              THEN
          log_1D =  0
      ELSE
          log_1D =  1
      END IF
 
 
C +--HAMMING Filter Parameters
C +  =========================
 
       iham  = 0
       nham  = 0
 
c #HF IF (hamfil)                                                   THEN
 
c #HF  hham  = 3.  *3600.
C +... hham  : Time Span for Diabatic Initialisation
C +            (cfr. Huang and Lynch 1993 MWR 121, p. 590)
c #HF  hhac  = hham
C +... hhac  : Filter Cutoff Time Period
C +            (cfr. Lynch and Huang 1992 MWR 120, p.1021)
c #HF  nham  = hham*demi/dt
c #HF  iham  =-nham
c #HF  fham  = pi / (nham+1)
c #HF  thac  =(pi / (nham  ))*(hham/hhac)
C +... thac  : Filter Cutoff Frequency
C +            (cfr. Lynch and Huang 1992 MWR 120, p.1021)
 
c #HF END IF
 
       ihamr = iham
       nhamr = nham
 
 
C +--Domain Averaged Pressure Thickness
C +  ==================================
 
        pav  = 0.
      DO j=1,my
      DO i=1,mx
        pav  = pav + pstDYn(i,j)
      END DO
      END DO
        pav  = pav / (mx*my)
 
 
C +--Dynamical Balance
C +  =================
 
c #DY  ibd= 0
c #DY  tdt= 0.
c #DY afdt= 1.
c #DY fnam='                '
 
C +        ******
c #DY call dynbil(tdt,afdt,jmez,ibd)
C +        ******
 
 
C +--OUTPUT
C +  ======
 
C +   ---------------------
      if (IO_loc.ge.2) then
C +   ---------------------
 
       DO i=1,5
         tta(i) = tsrfSL(igrdIO(i),jgrdIO(i),1)-TfSnow
       END DO
         write(21,607)(igrdIO(i),jgrdIO(i),       i=1,5),
     .             (sh(igrdIO(i),jgrdIO(i)),tta(i),i=1,5)
  607    format(//,5(5x,' (',i4,',',i4,')',5x,'!! '),
     .           /,5(' altitude ! temperat. ','!! '),
     .           /,5(10('-'),'!',11('-'),'!!-'),
     .           /,5(f8.1,'  ! ',f8.2,'  !! '))
       DO kk=1,mz
          k=mz+1-kk
         DO i=1,5
          zza(i) = gplvDY(igrdIO(i),jgrdIO(i),k)*grvinv
          tta(i) = pktaDY(igrdIO(i),jgrdIO(i),k)*pcap
         END DO
          write(21,609)(zza(i),tta(i),i=1,5)
  609     format(5(f8.1,'  ! ',f8.2,'  !! '))
       END DO
          write(21,611)
  611     format(1x)
 
C +   ------
      end if
C +   ------
 
      IF (mmx.gt.1.and.mmy.le.1)                                    THEN
        write(22,221)itexpe,(xxkm(i),i=imez-10,imez+30)
        write(23,221)itexpe,(xxkm(i),i=imez-10,imez+30)
        write(24,221)itexpe,(xxkm(i),i=imez-10,imez+30)
      END IF
 
c #DY   write(61,221)itexpe,(xxkm(i),i=imez-10,imez+30)
c #DY   write(62,221)itexpe,(xxkm(i),i=imez-10,imez+30)
c #DY   write(63,221)itexpe,(xxkm(i),i=imez-10,imez+30)
c #DY   write(64,221)itexpe,(xxkm(i),i=imez-10,imez+30)
c #DY   write(65,221)itexpe,(xxkm(i),i=imez-10,imez+30)
c #DY   write(66,221)itexpe,(xxkm(i),i=imez-10,imez+30)
 
 
C +--NetCDF Files
C +  ------------
 
      nbhour =          0
 1    CONTINUE
      nbhour = nbhour + 1
      IF (mod(3600*nbhour,idt).ne.0)                           GO TO 1
 
      IF (log_nc.eq.1)                                            THEN
 
                     dt_Loc =   dt
 
C +           ******
         call OUT_nc
C +           ******
 
      END IF
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ BEGIN of the EXTERNAL TIME INCREMENTATION (nprint over dt * nboucl) ++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
      iprint = 0
 3    CONTINUE
 
 
C +--Output Files Label
C +  ==================
 
        fnam( 1: 3) = 'si_'
        jmmd        = 1 + mod(minuGE,10)
        jm10        = 1 +     minuGE/10
        jh10        = 1 +     jhaMAR/10
        jh1         = 1 + mod(jhaMAR,10)
        jd10        = 1 +     jdaMAR/10
      IF (jd10.gt.10)                                             THEN
        fnam( 3: 3) =   '+'
        jd10        =     mod(jd10  ,10)
      END IF
        jd1         = 1 + mod(jdaMAR,10)
        fnam( 4: 4) = labnum(jd10)
        fnam( 5: 5) = labnum(jd1)
        fnam( 6: 6) = labnum(jh10)
        fnam( 7: 7) = labnum(jh1)
        fnam( 8: 8) = labnum(jm10)
        fnam( 9: 9) = labnum(jmmd)
        fnam(10:10) = '.'
        fnam(11:13) = explIO
        fnam(14:16) = '   '
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ BEGIN of the INTERNAL TIME INCREMENTATION (nboucl over dt) +++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
      iboucl = 1
 2    CONTINUE
 
 
C +--cpl : GET FIELDS FROM OASIS
C +  ===========================
 
C +        ***********
c #AO call OASIS_2_MAR
C +        ***********
 
 
C +--Modification of the Surface  Forcing
C +  ====================================
 
C +         ******
       call SBCnew
       call FILatmo
C +         ******
 
      IF ( iterun <=  1 .or. mod(iterun,12*3600/int(dt))==0)
     .call time_steps
 
 
C +--Branch to  Stand Alone Surface Model
C +  ====================================
 
c #SA IF (sALONE)  GO TO 4
C +...             go to Stand Alone Surface Model
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ BEGIN of DIABATIC INITIALISATION +++++++++++++++++++++++++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
 150  CONTINUE
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ BEGIN of FAST PROPAGATING WAVES DYNAMICS     (HYDROSTATIC PART) ++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
      IF ( mmx .gt. 1 .and. log_1D.eq.1 )                         THEN
C +...``Dynamics'' is active only after the 1-D Initialisation Phase
 
 
C +--Update of Horizontal Wind Speed and Mass
C +  ========================================
 
        DO k=1,mz
        DO j=1,my
        DO i=1,mx
          ubefDY(i,j,k)  = uairDY(i,j,k)
          vbefDY(i,j,k)  = vairDY(i,j,k)
        END DO
        END DO
        END DO
 
        DO j=1,my
        DO i=1,mx
           pstDY( i,j)   = pstDYn(i,j)
          opstDY( i,j)   = pstDYn(i,j)
        END DO
        END DO
 
 
C +--Update of nt_Mix parameter (CFL criterion on Max Wind Speed)
C +  ============================================================
 
        IF (.NOT.ntFlog)                                            THEN
            ntFlog    =.TRUE.
            CFLinv    = dt/dx          ! Inverse           CFL Number
            rtFact    = 500.0 * CFLinv ! Sound Speed upper Bound (500m/s)
                                       ! Normalized by the CFL Number
 
C +--Local     Wind Speed
C +  --------------------
 
            VLocmx    = 0.
            TLocmn    = 273.15
            iLocmx    = 0
            jLocmx    = 0
            kLocmx    = 0
          DO j=1,my
          DO i=1,mx
          DO k=1,mz
            WKxyz1(i,j,k) =             (abs(uairDY(i,j,k))
     .                     + min(1,my-1)*abs(vairDY(i,j,k)))
          ENDDO
          ENDDO
          ENDDO
 
          DO j=1,my
          DO i=1,mx
          DO k=1,mz
            VLocmx    =      max(VLocmx     ,WKxyz1(i,j,k))
          ENDDO
          ENDDO
          ENDDO
 
          DO j=1,my
          DO i=1,mx
            TLocmn   =       min(TLocmn     ,tairDY(i,j,mz-1))
          ENDDO
          ENDDO
 
          DO j=1,my
          DO i=1,mx
          DO k=1,mz
            IF (WKxyz1(i,j,k).GT.VLocmx-epsi)                       THEN
              iLocmx      =      i
              jLocmx      =      j
              kLocmx      =      k
            ENDIF
          ENDDO
          ENDDO
          ENDDO
 
          TLocmn    = TLocmn - 273.15
          nt_sig    =          1  +  CFLzDY  /  ntFast
          nt_BAK    =                           nt_Mix
          nt_Mix    = max(nt_sig,           int(rtFact + CFLinv*VLocmx))
cc #VN     if(TLocmn<-60.) nt_Mix=min(nt_Mix*2,
cc #VN.                           nt_Mix+(-50-int(TLocmn))/10)
 
          IF (nt_Mix .ne. nt_BAK)                                   THEN
 
            dtFast      = dt     / ((ntFast+1) *nt_Mix) ! see INIgen
            FIfstu      = FIslou / ((ntFast+1) *nt_Mix) ! see GRDmar
            FIfstp      = FIslop / ((ntFast+1) *nt_Mix) ! see GRDmar
            DO k = 1,mz
cXF
              FIk_fu(k) = FIfstu /(nt_Mix*max(0.05,    (sigma(k))))  ! see GRDma
              FIk_fp(k) = FIfstp /(nt_Mix*max(0.05,sqrt(sigma(k))))  ! see GRDma
            END DO
 
            write(6,1001)
     .                    TLocmn,VLocmx,iLocmx,jLocmx ,kLocmx
     .                   ,nt_BAK,nt_Mix,itexpe
     .                   ,jdarGE,labmGE(mmarGE),iyrrGE
     .                   ,jhurGE,       minuGE ,jsecGE
 1001       format('WARNING: TT min =',f8.2,
     .        ' S(|V|)max =',f8.1,' (',3i4,')',
     .        ' ==> update nt_Mix(=',i4,');:=', i4,' at iteration',i8,
     .        '     Time is ',i2,'-',a3,'-',i4,
     .                    '/',i2,'.',i2,'.',i2,' UT')
 
          ENDIF
        END IF
 
 
C +--Begin the Time Loop
C +  ===================
 
        DO it_Mix = 1,nt_Mix
        DO itFast = 1,ntFast+1
 
 
C +--Integration of the Hydrostatic Relation
C +  =======================================
 
          IF (.not.brocam)                                          THEN
 
           if(openmp) then
C +            ******
            call DYNgpo_mp
C +            ******
           else
C +            ******
            call DYNgpo
C +            ******
           endif
 
C +...      WARNING : Place of this routine DYNgpo in the organigram depends
C +         if Brown-Campana (1978, MWR, p.1125) time scheme is used or not!
C +         Here is the place when the Brown-Campana time scheme is NOT used.
 
C +              **********
c #db       call Debugg_MAR('DYNgpo .F.')
C +              **********
 
          END IF
 
 
C +--Mass Continuity
C +  ===============
 
          norder_0=nordps
 
          if(openmp) then
C +            ******
          call DYNdps_mp(norder_0)
C +            ******
          else
C +            ******
          call DYNdps(norder_0)
C +            ******
          endif
 
 
C +            **********
c #db     call Debugg_MAR('DYNdps .F.')
C +            **********
 
 
C +--Nudging
C +  -------
 
 
c #ob     DO j=1,my
c #ob     DO i=1,mx
c #ob       dumy3D(i,j,1)=pstDYn(i,j)
c #ob     END DO
c #ob     END DO
 
c #ob                            kdim = 1
 
C +            **********
c #ob     call LBC000_run(dumy3D,kdim)
C +            **********
 
c #ob     DO j=1,my
c #ob     DO i=1,mx
c #ob       pstDYn(i,j)  =dumy3D(i,j,1)
c #ob     END DO
c #ob     END DO
 
 
C +--Filtering
C +  ---------
 
          IF (FIfstp.gt.0.)                                         THEN
 
              DO j=1,my
              DO i=1,mx
                dumy3D(i,j,1) = pstDYn(i,j)   - pstDY1(i,j)
              END DO
              END DO
                dumeps(    1) = FIfstp
                kdim          = 1
 
            IF(mmy.le.1)                                            THEN
 
c #OB         IF (openLB)                                           THEN
 
C +                  **********
c #OB           call DYNfil_1D0(dumy3D,dumeps,kdim)
C +                  **********
 
c #OB         ELSE
 
C +                  *********
                call DYNfil_1D (dumy3D,dumeps,kdim)
C +                  *********
 
c #OB         END IF
 
            ELSE
c #OB         IF (openLB)                                           THEN
 
C +                  **********
c #OB           call DYNfil_3D0(dumy3D,dumeps,kdim)
C +                  **********
 
c #OB         ELSE
 
                IF (no_vec)                                         THEN
 
C +                    *********
                  call DYNfil_3D (dumy3D,dumeps,kdim)
C +                    *********
 
                ELSE
 
C +                    *********
                  call DYNfilv3D (dumy3D,dumeps,kdim)
C +                    *********
 
                END IF
 
c #OB         END IF
 
            END IF
 
              DO j=1,my
              DO i=1,mx
                pstDYn(i,j)   = dumy3D(i,j,1) + pstDY1(i,j)
              END DO
              END DO
 
C +              **********
c #db       call Debugg_MAR('DYNfil Dp*')
C +              **********
 
          END IF
 
 
C +--Integration of the Hydrostatic Relation
C +  =======================================
 
          IF (brocam)                                               THEN
 
 
           if(openmp) then
C +            ******
            call DYNgpo_mp
C +            ******
           else
C +            ******
            call DYNgpo
C +            ******
           endif
 
C +...      WARNING : The place of routine DYNgpo in the organigram depends
C +         if Brown-Campana (1978, MWR, p.1125) time scheme is used or not!
C +         Here is the place when Brown-Campana time scheme is used.
 
C +              **********
c #db       call Debugg_MAR('DYNfil Dp*')
C +              **********
 
          END IF
 
 
C +--Contribution of Horizontal Pressure Gradient Force
C +  ==================================================
 
                      norder_0=nordps
 
        if(openmp) then
C +          **********
        call DYNdgz_mp(norder_0)
C +          **********
        else
C +          **********
        call DYNdgz(norder_0)
C +          **********
        endif
 
C +            **********
c #db     call Debugg_MAR('DYNdgz    ')
C +            **********
 
 
C +--Contribution of the Divergence Damper (Hydrostatic PART)
C +  ========================================================
 
C +            ******
c #DD     call DYNdmp
C +            ******
 
C +            **********
c #db     call Debugg_MAR('DYNdmp    ')
C +            **********
 
 
C +--Filtering of the Horizontal Wind Speed Components
C +  =================================================
 
          IF     (itFast.le.ntFast)                                 THEN
            IF   (FIk_fu(1).gt.0.0)                                 THEN
              IF (mmy.le.1)                                         THEN
 
                DO k=1,mz
                DO i=1,mx
                  dumy3D(i,1,k) = uairDY(i,1,k)
                END DO
                  dumeps(    k) = FIk_fu(    k)
                END DO
                  kdim          =            mz
 
c #OB           IF (openLB)                                         THEN
 
C +                    **********
c #OB             call DYNfil_1D0(dumy3D,dumeps,mz)
C +                    **********
 
c #OB           ELSE
 
C +                    *********
                  call DYNfil_1D (dumy3D,dumeps,mz)
C +                    *********
 
c #OB           END IF
 
C +...            The PGF does not contribute  to v in the 2-D version
C +                       making the filtering of v unnecessary.
 
                DO k=1,mz
                DO i=1,mx
                  uairDY(i,1,k) = dumy3D(i,1,k)
                END DO
                END DO
 
              ELSE
 
                DO k=1,mz
                  dumeps(k)     = FIk_fu(    k)
                END DO
                  kdim          =            mz
 
                DO k=1,mz
                DO j=1,my
                DO i=1,mx
                  dumy3D(i,j,k) = uairDY(i,j,k)
                END DO
                END DO
                END DO
 
c #OB           IF (openLB)                                         THEN
 
C +                    **********
c #OB             call DYNfil_3D0(dumy3D,dumeps,kdim)
C +                    **********
 
c #OB           ELSE
 
                  IF (no_vec)                                       THEN
                    if(openmp) then
C +                      **********
                    call DYNfil_3D_mp (dumy3D,dumeps,kdim)
C +                      **********
                    else
C +                      **********
                    call DYNfil_3D (dumy3D,dumeps,kdim)
C +                      **********
                    endif
                  ELSE
 
C +                      *********
                    call DYNfilv3D (dumy3D,dumeps,kdim)
C +                      *********
 
                  END IF
 
c #OB           END IF
 
                DO k=1,mz
                DO j=1,my
                DO i=1,mx
                  uairDY(i,j,k) = dumy3D(i,j,k)
                  dumy3D(i,j,k) = vairDY(i,j,k)
                END DO
                END DO
                END DO
 
c #OB           IF (openLB)                                         THEN
 
C +                    **********
c #OB             call DYNfil_3D0(dumy3D,dumeps,kdim)
C +                    **********
 
c #OB           ELSE
 
                  IF (no_vec)                                       THEN
 
                    if(openmp) then
C +                      **********
                    call DYNfil_3D_mp (dumy3D,dumeps,kdim)
C +                      **********
                    else
C +                      **********
                    call DYNfil_3D (dumy3D,dumeps,kdim)
C +                      **********
                    endif
 
                  ELSE
 
C +                      *********
                    call DYNfilv3D (dumy3D,dumeps,kdim)
C +                      *********
 
                  END IF
 
c #OB           END IF
 
                DO k=1,mz
                DO j=1,my
                DO i=1,mx
                  vairDY(i,j,k) = dumy3D(i,j,k)
                END DO
                END DO
                END DO
 
              END IF
 
            END IF
 
C +              **********
c #db       call Debugg_MAR('DYNfil u,v')
C +              **********
 
 
C +--Filtering of the Vertical H Wind Speed Component
C +  ================================================
 
          ELSE
            IF     (itFast.eq.ntFast+1)                             THEN
              IF   (FIk_fp(1).gt.0.0  )                             THEN
                IF (mmy.le.1)                                       THEN
 
                  DO k=1,mz
                  DO i=1,mx
                     dumy3D(i,1,k)= psigDY(i,1,k)
                  END DO
                     dumeps(    k)= FIk_fp(    k)
                  END DO
                     kdim         =           mz
 
c #OB             IF (openLB)                                       THEN
 
C +                      **********
c #OB               call DYNfil_1D0(dumy3D,dumeps,kdim)
C +                      **********
 
c #OB             ELSE
 
C +                      *********
                    call DYNfil_1D (dumy3D,dumeps,kdim)
C +                      *********
 
c #OB             END IF
 
C +...              The PGF does not contribute to v in the 2-D version
C +                         making the filtering of v unnecessary.
 
                  DO k=1,mz
                  DO i=1,mx
                    psigDY(i,1,k) = dumy3D(i,1,k)
                  END DO
                  END DO
 
                ELSE
 
                  DO k=1,mz
                    dumeps(k)     = FIk_fp(    k)
                  END DO
                    kdim          =            mz
 
                  DO k=1,mz
                  DO j=1,my
                  DO i=1,mx
                    dumy3D(i,j,k) = psigDY(i,j,k)
                  END DO
                  END DO
                  END DO
 
c #OB             IF (openLB)                                       THEN
 
C +                      **********
c #OB               call DYNfil_3D0(dumy3D,dumeps,kdim)
C +                      **********
 
c #OB             ELSE
 
                    IF (no_vec)                                     THEN
 
                    if(openmp) then
C +                      **********
                    call DYNfil_3D_mp (dumy3D,dumeps,kdim)
C +                      **********
                    else
C +                      **********
                    call DYNfil_3D (dumy3D,dumeps,kdim)
C +                      **********
                    endif
 
                    ELSE
 
C +                        *********
                      call DYNfilv3D (dumy3D,dumeps,kdim)
C +                        *********
 
                    END IF
 
c #OB             END IF
 
                  DO k=1,mz
                  DO j=1,my
                  DO i=1,mx
                    psigDY(i,j,k) = dumy3D(i,j,k)
                  END DO
                  END DO
                  END DO
                END IF
              END IF
            END IF
          END IF
 
        END DO
        END DO
 
 
C +--Dynamical Balance (Pressure Gradient Force)
C +  ===========================================
 
c #DY    ibd= 1
c #DY    tdt= 0.
c #DY   afdt= 1.
 
C +          ******
c #DY   call dynbil(tdt,afdt,jmez,ibd)
C +          ******
 
C +          **********
c #DB   call Debugg_MAR('After Fast')
C +          **********
 
 
C +--Update of nt_Mix parameter (CFL criterion on Max Wind Speed)
C +  ============================================================
 
 
C +--Local     Wind Speed
C +  --------------------
 
            VLocmx    = 0.
            TLocmn    = 273.15
            iLocmx    = 0
            jLocmx    = 0
            kLocmx    = 0
          DO j=1,my
          DO i=1,mx
          DO k=1,mz
            WKxyz1(i,j,k) =              (abs(uairDY(i,j,k))
     .                      + min(1,my-1)*abs(vairDY(i,j,k)))
          ENDDO
          ENDDO
          ENDDO
 
          DO j=1,my
          DO i=1,mx
          DO k=1,mz
            VLocmx    =      max(VLocmx     ,WKxyz1(i,j,k))
          ENDDO
          ENDDO
          ENDDO
 
          DO j=1,my
          DO i=1,mx
            TLocmn   =       min(TLocmn     ,tairDY(i,j,mz-1))
          ENDDO
          ENDDO
 
          DO j=1,my
          DO i=1,mx
          DO k=1,mz
            IF (WKxyz1(i,j,k).GT.VLocmx-epsi)                       THEN
              iLocmx      =      i
              jLocmx      =      j
              kLocmx      =      k
            ENDIF
          END DO
          END DO
          END DO
 
          TLocmn    = TLocmn - 273.15
          nt_sig    =          1  +  CFLzDY  /  ntFast
          nt_BAK    =                           nt_Mix
          nt_Mix    = max(nt_sig,           int(rtFact + CFLinv*VLocmx))
cc #VN     if(TLocmn<-60.) nt_Mix=min(nt_Mix*2,
cc #VN.                           nt_Mix+(-50-int(TLocmn))/10)
 
          IF (nt_Mix .ne. nt_BAK)                                   THEN
 
            dtFast      = dt     / ((ntFast+1) *nt_Mix) ! see INIgen
            FIfstu      = FIslou / ((ntFast+1) *nt_Mix) ! see GRDmar
            FIfstp      = FIslop / ((ntFast+1) *nt_Mix) ! see GRDmar
            DO k = 1,mz
              FIk_fu(k) = FIfstu / (nt_Mix*max(0.05,    (sigma(k)))) ! see GRDma
              FIk_fp(k) = FIfstp / (nt_Mix*max(0.05,sqrt(sigma(k)))) ! see GRDma
            END DO
 
            write(6,1001)
     .                    TLocmn,VLocmx,iLocmx,jLocmx ,kLocmx
     .                   ,nt_BAK,nt_Mix,itexpe
     .                   ,jdarGE,labmGE(mmarGE),iyrrGE
     .                   ,jhurGE,       minuGE ,jsecGE
 
          ENDIF
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ END   of FAST PROPAGATING WAVES DYNAMICS     (HYDROSTATIC PART) ++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ BEGIN of FAST PROPAGATING WAVES DYNAMICS (NON-HYDROSTATIC PART) ++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
C +--Non-Hydrostatic Dynamics
C +  ========================
 
C +              ******
c #NH       call DYN_NH
C +              ******
 
C +              **********
c #db       call Debugg_MAR('DYN_NH    ')
C +              **********
 
 
C +--Radiating Boundary Conditions
C +  -----------------------------
 
 
C +--Statistics
C +  ~~~~~~~~~~
C +              **********
c #db       call Debugg_MAR('LBCnud_000')
C +              **********
 
 
C +--Filtering
C +  ---------
 
C +              *********
c #NH       call DYNfil_NH
C +              *********
 
 
C +--Water Budget
C +  ------------
 
c #WB                         dt_Bal = dt
C +              ******
c #WB       call H2O_WB(0,-1.,dt_Bal,.FALSE.,.FALSE.)
C +              ******
 
C +              ******
c #WB       call H2O_WB(0, 1.,dt_Bal,.FALSE.,.FALSE.)
C +              ******
 
C +              **********
c #db       call Debugg_MAR('DYNfil_NH ')
C +              **********
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ END   of FAST PROPAGATING WAVES DYNAMICS (NON-HYDROSTATIC PART) ++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ BEGIN of SLOW PROPAGATING WAVES DYNAMICS +++++++++++++++++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
C +--Advection
C +  =========
 
C +--Tracer Mass Verification (Advection)
C +  ------------------------------------
 
c #MV     IF    (jtAdvH.eq.1)                                       THEN
c #MV            totrac(1) = 0.
c #MV            tracmx(1) = 0.
c #MV       DO k=1,mz
c #MV       DO j=1,my
c #MV       DO i=1,mx
c #MV            totrac(1) = totrac(1)+qxTC(i,j,k,1)*opstDY(i,j)
c #MV.                                              *dsigm1(k)
c #MV         IF(tracmx(1)        .lt. qxTC(i,j,k,1))               THEN
c #MV            tracmx(1) =           qxTC(i,j,k,1)
c #MV            itrcmx(1) =                  i
c #MV            jtrcmx(1) =                  j
c #MV            ktrcmx(1) =                  k
c #MV         END IF
c #MV       END DO
c #MV       END DO
c #MV       END DO
c #MV     END IF
 
 
C +--Leapfrog Backward Scheme
C +  ------------------------
 
        IF    (DYNadv.EQ.'LFB')                                     THEN
 
c #WB                         dt_Bal = dt
C +              ******
c #WB       call H2O_WB(1,-1.,dt_Bal,.FALSE.,.FALSE.)
C +              ******
 
                            norder_0 = nordps
C +              **********
            call DYNadv_LFB(norder_0)
C +              **********
 
C +              **********
c #DB       call Debugg_MAR('DYNadv_LFB')
C +              **********
 
c #WB                         dt_Bal = dt
C +              ******
c #WB       call H2O_WB(1, 1.,dt_Bal,.FALSE.,.FALSE.)
C +              ******
 
        END IF
 
 
C +--Forward           Scheme
C +  ------------------------
 
        IF    (DYNadv.EQ.'UPW')                                     THEN
 
C +--Vertical Advection: (Thermo)Dynamics
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
C +              **********
            call DYNadv_ver
C +              **********
 
C +              **********
c #DB       call Debugg_MAR('DYNadv_ver')
C +              **********
 
C +--Vertical Advection: Water    Species
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
          IF (.not.micphy)                                        THEN
 
C +              ***********
            call DYNadv_verq
C +              ***********
 
C +              **********
c #DB       call Debugg_MAR('DYNadv_vrq')
C +              **********
 
          ELSE
 
C +              **********
            call HYDadv_ver
C +              **********
 
C +              **********
c #DB       call Debugg_MAR('HYDadv_ver')
C +              **********
C +
          END IF
 
C +--Vertical Advection: Tracers
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
C +              **********
c #TC       call TRCadv_ver
C +              **********
 
C +              **********
c #DB       call Debugg_MAR('TRCadv_ver')
C +              **********
 
C +--Horizontal Advection: Momentum
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
            FirstC = .FALSE.
            qqmass = .FALSE.
 
C +              **********
            call DYNadv_hor(uairDY,opstDY,pstDYn,uairDY,vairDY)
C +              **********
 
C +              **********
            call DYNadv_hor(vairDY,opstDY,pstDYn,uairDY,vairDY)
C +              **********
 
C +--Horizontal Advection: (Thermo)dynamics
C +  ~~~~~~~~~~~~~~~~~~~~~ Water    Species
C +                        ~~~~~~~~~~~~~~~~
            FirstC = .true.
            qqmass = .true.
 
            DO k=1,mz
            DO j=1,my
            DO i=1,mx
              dumy3D(i,j,k) = pktaDY(i,j,k)
            END DO
            END DO
            END DO
 
C +              **********
            call DYNadv_hor(dumy3D,opstDY,pstDYn,uairDY,vairDY)
C +              **********
 
            DO k=1,mz
            DO j=1,my
            DO i=1,mx
              pktaDY(i,j,k) = dumy3D(i,j,k)
            END DO
            END DO
            END DO
 
            FirstC = .FALSE.
 
C +              **********
            call DYNadv_hor(  qvDY,opstDY,pstDYn,uairDY,vairDY)
C +              **********
 
            IF (micphy)                                           THEN
 
C +                **********
              call DYNadv_hor(ccniHY,opstDY,pstDYn,uairDY,vairDY)
C +                **********
 
 
C +                **********
              call DYNadv_hor(  qiHY,opstDY,pstDYn,uairDY,vairDY)
C +                **********
 
 
C +                **********
              call DYNadv_hor(  qsHY,opstDY,pstDYn,uairDY,vairDY)
C +                **********
 
 
C +                **********
              call DYNadv_hor(  qwHY,opstDY,pstDYn,uairDY,vairDY)
C +                **********
 
 
C +                **********
              call DYNadv_hor(  qrHY,opstDY,pstDYn,uairDY,vairDY)
C +                **********
 
            END IF
 
C +--Statistics
C +  ~~~~~~~~~~
C +              **********
c #DB       call Debugg_MAR('DYNadv_hor')
C +              **********
 
C +--Horizontal Advection: Saltating Snow
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
C +              **********
c #BS       call DYNadv_sal
C +              **********
 
C +--Horizontal Advection: Tracers
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
c #TC     IF    (mod(itexpe,jtAdvH).eq.0)                         THEN
 
c #TC       qqmass =.true.
 
C +              **********
c #TC       call TRCadv_hor
C +              **********
 
C +              **********
c #DB       call Debugg_MAR('TRCadv_hor')
C +              **********
 
C +--New Time Step
C +  ~~~~~~~~~~~~~
c #TC         cfladv =     epsi
c #TC       DO k=1,mz
c #TC       DO j=1,my
c #TC       DO i=1,mx
c #TC         cfladv = max(cfladv,abs(uairDY(i,j,k)))
c #TC         cfladv = max(cfladv,abs(vairDY(i,j,k)))
c #TC       END DO
c #TC       END DO
c #TC       END DO
 
c #TC         dtAdvH = demi   *dx/cfladv
c #TC         dtAdvH = min(dtAdvH,dt_ODE)
c #TC         dtAdvH = max(dtAdvH,dt    )
 
c #TC         jtAdvH =  dtAdvH / dt
C +...        jtAdvH :  Number of  Dynamical Steps for 1 Advection     Step
 
c #TC         dtAdvH =  dt * jtAdvH
C +...        dtAdvH :  Calibrated Advection       Time Step
 
c #TC         ntAdvH =   1
 
c #TC     END IF
 
        END IF
 
 
C +--Tracer Mass Verification (Advection)
C +  ------------------------------------
 
c #MV     IF    (jtAdvH.eq.1)                                     THEN
c #MV        totrac(2) = 0.
c #MV        tracmx(2) = 0.
c #MV       DO k=1,mz
c #MV       DO j=1,my
c #MV       DO i=1,mx
c #MV            totrac(2) = totrac(2)+qxTC(i,j,k,1)*pstDYn(i,j)
c #MV.                                              *dsigm1(k)
c #MV         IF(tracmx(2)        .lt. qxTC(i,j,k,1))             THEN
c #MV            tracmx(2) =           qxTC(i,j,k,1)
c #MV            itrcmx(2) =                  i
c #MV            jtrcmx(2) =                  j
c #MV            ktrcmx(2) =                  k
c #MV         END IF
c #MV       END DO
c #MV       END DO
c #MV       END DO
c #MV     END IF
 
 
C +--Dynamical Balance (Advection)
C +  -----------------------------
 
c #DY      ibd= 3
c #DY      tdt= 1.
c #DY     afdt= 1.
 
C +            ******
c #DY     call dynbil(tdt,afdt,jmez,ibd)
C +            ******
 
 
C +--Rayleigh Friction (Ref. ARPS 4.0 User's Guide, para 6.4.3 p.152)
C +  =================
 
      DO k=1,mzabso
        DO j=1,my
        DO i=1,mx
          pktaDY(i,j,k)=(pktaDY(i,j,k) + Ray_UB(k)*dt    *pktaUB(i,j,k))
     .                 /(1.0           + Ray_UB(k)*dt                  )
        END DO
        END DO
      END DO
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ END   of SLOW PROPAGATING WAVES DYNAMICS +++++++++++++++++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
      ELSE
 
           if(openmp) then
C +            ******
            call DYNgpo_mp
C +            ******
           else
C +            ******
            call DYNgpo
C +            ******
           endif
 
 
C +--Mid-Level Geopotential
C +  ----------------------
 
           k=1
        DO j=1,my
        DO i=1,mx
          gpmiDY(i,j,k) = 0.5 *(3.5*gplvDY(i,j,  1)-0.5d0*gplvDY(i,j,2))
        END DO
        END DO
 
        DO k=kp1(1),mz
        DO j=1,my
        DO i=1,mx
          gpmiDY(i,j,k) = 0.5 *(    gplvDY(i,j,k-1)+      gplvDY(i,j,k))
        END DO
        END DO
        END DO
 
           k=       mzz
        DO j=1,my
        DO i=1,mx
          gpmiDY(i,j,k) =(0.5 *     z__SBL         +          sh(i,j)  )
     .                  *           gravit
        END DO
        END DO
 
C +          **********
c #DB   call Debugg_MAR('DYNgpo    ')
C +          **********
 
      END IF
 
 
C +--Specific Mass
C +  =============
 
C +          ******
        call DYNrho
C +          ******
 
C +          **********
c #db   call Debugg_MAR('DYNrho    ')
C +          **********
 
 
C +--Saturation Specific Humidity
C +  ============================
 
C +          ******
        call qsat3D
C +          ******
 
 
C +--Vertical Velocity in Cartesian Coordinates
C +  ==========================================
 
 
C +                                 ******
      IF (convec.and.mmx.gt.1) call DYNwww
C +                                 ******
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ BEGIN of MAR "SUBGRID ZONE" (INCLUDING CORIOLIS FORCE) +++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
C +--Tracer Mass Verification
C +  ========================
 
c #MV IF    (jtAdvH.eq.1)                                         THEN
c #MV        totrac(3) = 0.
c #MV        tracmx(3) = 0.
c #MV   DO k=1,mz
c #MV   DO j=1,my
c #MV   DO i=1,mx
c #MV        totrac(3) = totrac(3) +qxTC(i,j,k,1) *pstDYn(i,j)*dsigm1(k)
c #MV     IF(tracmx(3)         .lt. qxTC(i,j,k,1))                THEN
c #MV        tracmx(3) =            qxTC(i,j,k,1)
c #MV        itrcmx(3) =                 i
c #MV        jtrcmx(3) =                 j
c #MV        ktrcmx(3) =                 k
c #MV     END IF
c #MV   END DO
c #MV   END DO
c #MV   END DO
c #MV END IF
 
 
C +--Local Temporal Parameters
C +  =========================
 
      IF (log_1D.eq.0     .and.
     .    tequil.gt.0.)                                           THEN
 
C +--Boundary Layer Initialisation over time tequil
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
       tequil = tequil*3600.
C +... Conversion [h]->[s]
       dt_Loc = dtquil
       nt_Loc = tequil/dtquil
       jt_Loc = 1
      ELSE
 
C +--Boundary Layer is iterated    over time dt
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
       dt_Loc = dtDiff
       nt_Loc = ntDiff
       jt_Loc = jtDiff
c #OM  dt_Loc = dt
c #OM  nt_Loc = 1
c #OM  jt_Loc = 1
      END IF
 
 
C +--Begin of Subgrid Loop
C +  =====================
 
      IF (mod(itexpe,jt_Loc).eq.0)                                THEN
 
        DO it_Loc= 1,nt_Loc
 
 
C +--Coriolis Force Contribution (Implicit Scheme)
C +  ---------------------------------------------
 
          DO i=1,mx
          DO j=1,my
          DO k=1,mz
            uairDY(i,j,k) = uairDY(i,j,k)
     .       + fcorDY(i,j)*(vairDY(i,j,k)-vgeoDY(i,j,k)) *dt_Loc
            vairDY(i,j,k) = vairDY(i,j,k)
     .       - fcorDY(i,j)*(uairDY(i,j,k)-ugeoDY(i,j,k)) *dt_Loc
          END DO
          END DO
          END DO
 
C +--Dynamical Balance (Coriolis Force)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #DY      ibd= 4
c #DY      tdt= 1.
c #DY     afdt= 0.
 
C +            ******
c #DY     call dynbil(tdt,afdt,jmez,ibd)
C +            ******
 
 
C +--Horizontal Subgrid Processes
C +  ----------------------------
 
          IF (turhor      .and.
     .        log_1D.eq.1 .and.
     .        mmx   .gt.1)                                        THEN
 
c #WB                         dt_Bal = dt_Loc
C +              ******
c #WB       call H2O_WB(2,-1.,dt_Bal,.FALSE.,.FALSE.)
C +              ******
 
C +--Horizontal Diffusion Coefficient
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
C +              *********
            call TURhor_kh
C +              *********
 
C +              **********
c #db       call Debugg_MAR('TURhor_kh ')
C +              **********
 
C +--Contribution of Horizontal Diffusion
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
C +              **********
            call TURhor_dyn(dtDifH)
C +              **********
 
C +              **********
c #DB       call Debugg_MAR('TURhor_dyn')
C +              **********
 
c #WB                         dt_Bal = dt_Loc
C +              ******
c #WB       call H2O_WB(2, 1.,dt_Bal,.FALSE.,.FALSE.)
C +              ******
 
          END IF
 
 
C +--Water Vapor and Precipitation Loading
C +  -------------------------------------
 
C +            ******
          call DYNloa
C +            ******
 
C +            **********
c #db     call Debugg_MAR('DYNloa    ')
C +            **********
 
 
C +--Vertical   Subgrid Processes
C +  ----------------------------
 
          IF  (dtDiff.gt.0.)                                      THEN
 
C +--Turbulent Kinetic Energy
C +  ~~~~~~~~~~~~~~~~~~~~~~~~
            IF (jt_Loc.gt.1)                                      THEN
                dtLLoc =  min(dt_Loc,dtAdvH)
                dtLLoc =  min(dtLLoc,dtDifH)
C +...          CAUTION:             dtDifH computed in TURhor_dyn
 
                dtLLoc =  max(dtLLoc,dt    )
                ntLLoc =      dt_Loc/dtLLoc
                ntLLoc =  max(ntLLoc,iun   )
                dtLLoc =      dt_Loc/ntLLoc
            ELSE
                dtLLoc =      dt_Loc
                ntLLoc =           1
            END IF
 
            DO  itLLoc =           1,ntLLoc
 
c #BR       IF (tur_25)                                           THEN
 
C +               *********
c #BR        call TURpbl_25(dtLLoc)
C +               *********
 
C +               **********
c #DB        call Debugg_MAR('TURpbl_25 ')
C +               **********
 
c #BR       ELSE
 
C +                         ****************
              IF (mmx.gt.1) call TURtke_advh(dtLLoc)
              IF (mmx.gt.1) call TURtke_advv(dtLLoc)
              IF (mmx.gt.1) call TURtke_difh(dtLLoc)
                            call TURtke_difv(dtLLoc,0.)
                            call TURtke_gen (dtLLoc)
C +                         ***************
 
C +               **********
c #DB        call Debugg_MAR('TURtke Cie')
C +               **********
 
c #BR       END IF
 
            END DO
 
C +--Surface Layer
C +  ~~~~~~~~~~~~~
c #FR       IF (VSISVAT)                                          THEN
 
              DO k=1,mz
              DO j=1,my
              DO i=1,mx
                ssvSL(i,j,k) = sqrt(max(uairDY(i,j,k)*uairDY(i,j,k)
     .                                 +vairDY(i,j,k)*vairDY(i,j,k)
     .                                 ,epsi))
              END DO
              END DO
              END DO
 
c #FR       ELSE
 
C +                ******
c #FR         call TURsbl
C +                ******
 
C +                **********
c #db         call Debugg_MAR('TURsbl    ')
C +                **********
 
c #FR       END IF
 
C +--Mass Flux convective Scheme
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #WB                           dt_Bal = dt_Loc
C +                ******
c #WB         call H2O_WB(3,-1.,dt_Bal,.FALSE.,.FALSE.)
C +                ******
 
              itConv = itexpe*nt_Loc/jt_Loc+it_Loc
 
            IF (convec)                                             THEN
 
C +                ******
              call CVAorg
C +                ******
 
C +                **********
c #DB         call Debugg_MAR('CVAorg    ')
C +                **********
 
            END IF
 
c #WB                           dt_Bal = dt_Loc
C +                ******
c #WB         call H2O_WB(3, 1.,dt_Bal,.FALSE.,.FALSE.)
C +                ******
 
C +--Contribution of Turbulent Vertical Diffusion
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #OL       IF (TURver)                                             THEN
c #WB                           dt_Bal = dt_Loc
C +                ******
c #WB         call H2O_WB(4,-1.,dt_Bal,.FALSE.,.FALSE.)
C +                ******
 
C +                ******
              call TURabl
              call SSpray
C +                ******
 
C +                **********
c #DB         call Debugg_MAR('TURabl    ')
C +                **********
 
c #WB                           dt_Bal = dt_Loc
C +                ******
c #WB         call H2O_WB(4, 1.,dt_Bal,.FALSE.,.FALSE.)
C +                ******
 
C +--Contribution of Turbulent Vertical Diffusion (Non Hydrostatic Variables)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
C +                ******
c #NH         call TURvNH
C +                ******
 
C +                **********
c #db         call Debugg_MAR('TURvNH    ')
C +                **********
 
c #OL       END IF
 
          END IF
 
 
C +--Dynamical Balance (Subgrids)
C +  ----------------------------
 
c #DY      ibd= 2
c #DY      tdt= 0.
c #DY     afdt= 1.
 
C +            ******
c #DY     call dynbil(tdt,afdt,jmez,ibd)
C +            ******
 
        END DO
 
      END IF
 
 
C +--Tracers Turbulent Transfert
C +  ===========================
 
c #TC IF             (dt_ODE .ne.dtDiff)                          THEN
 
c #TC   IF(mod(itexpe,jt_ODE).eq.0)                               THEN
c #TC          dt_Loc=dt_ODE
c #TC          nt_Loc=nt_ODE
c #TC     DO   it_Loc=1,nt_Loc
 
C +              *********
c #TC       call TURabl_TC
C +              *********
 
c #TC     END DO
c #TC   END IF
 
C +              **********
c #db       call Debugg_MAR('TURabl_TC ')
C +              **********
 
c #TC END IF
 
 
C +--Tracer Mass Verification
C +  ========================
 
c #MV IF  (mod(itexpe,jt_ODE).eq.0)                               THEN
c #MV      totrac(4) = 0.
c #MV      tracmx(4) = 0.
c #MV   DO k=1,mz
c #MV   DO j=1,my
c #MV   DO i=1,mx
c #MV      totrac(4) = totrac(4) + qxTC(i,j,k,1) *pstDYn(i,j) *dsigm1(k)
c #MV   IF(tracmx(4)          .lt. qxTC(i,j,k,1))                 THEN
c #MV      tracmx(4) =             qxTC(i,j,k,1)
c #MV      itrcmx(4) =                  i
c #MV      jtrcmx(4) =                  j
c #MV      ktrcmx(4) =                  k
c #MV   END IF
c #MV   END DO
c #MV   END DO
c #MV   END DO
c #MV END IF
 
 
C +--OUTPUT
C +  ------
 
c #MV  write(6,133)iterun,(latrac(n),totrac(n),tracmx(n),
c #MV.                     itrcmx(n),jtrcmx(n),ktrcmx(n),n=1,4)
 133   format(i8,3x,a15,2d15.6,3i6,4(/,11x,a15,2d15.6,3i6))
 
 
C +--Initialized Temperature Vertical Profiles
C +  =========================================
 
C +   ----------------------------------
      IF (IO_loc.ge.2 .and. log_1D.eq.0)                          THEN
C +   ----------------------------------
 
        DO i=1,5
            tta(i) = tsrfSL(igrdIO(i),jgrdIO(i),1)-TfSnow
        END DO
            write(21,607) (igrdIO(i),jgrdIO(i),       i=1,5),
     .                 (sh(igrdIO(i),jgrdIO(i)),tta(i),i=1,5)
        DO kk=1,mz
           k=mzz-kk
          DO i=1,5
            zza(i) =       gplvDY(igrdIO(i),jgrdIO(i),k)*grvinv
            tta(i) =       pktaDY(igrdIO(i),jgrdIO(i),k)*pcap
          END DO
            write(21,609)(zza(i),tta(i),i=1,5)
        END DO
            write(21,611)
 
C +   ------
      END IF
C +   ------
 
      log_1D  = 1
C +...log_1D  = 1 <==> PBL initialisation is performed
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ END   of MAR "SUBGRID ZONE" (INCLUDING CORIOLIS FORCE) +++++++++++++++
C +++ EXPLICIT HYDROLOGICAL CYCLE  +++++++++++++++++++++++++++++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
C +--Cloud Microphysical Processes
C +  =============================
 
      IF (micphy)                                              THEN
 
c #WB                     dt_Bal = dt
C +          ******
c #WB   call H2O_WB(5,-1.,dt_Bal,.FALSE.,.FALSE.)
C +          ******
 
C +          ******
        call HYDgen
C +          ******
 
C +          **********
c #DB   call Debugg_MAR('HYDgen    ')
C +          **********
 
c #WB                     dt_Bal = dt
C +          ******
c #WB   call H2O_WB(5, 1.,dt_Bal,.FALSE.,.FALSE.)
C +          ******
 
      ELSE
 
 
C +--Elimination of Water Vapor in Excess
C +  ====================================
 
        DO k=1,mz
        DO j=1,my
        DO i=1,mx
          qvDY(i,j,k)=max(zero         ,qvDY(i,j,k))
          qvDY(i,j,k)=min(qvswDY(i,j,k),qvDY(i,j,k))
        END DO
        END DO
        END DO
 
      END IF
 
 
C +--Dust  Microphysical Processes
C +  =============================
 
C +                    ****
c #BD IF (BloMod) call DUST
C +                    ****
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ CHEMICAL PROCESSES +++++++++++++++++++++++++++++++++++++++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
c #CH IF (chimod)                                                 THEN
 
C +                                        **********
c #CH   IF (    itexpe        .eq.0)  call CHImie_INI
C +                                        **********
 
C +            **********
c #db     call Debugg_MAR('CHImie_INI')
C +            **********
 
c #CH                                                 iteChi = itexpe
C +
C +                                        **********
c #CH   IF (mod(iteChi,jt_ODE).eq.0)  call CHImie_ODE(iteChi)
C +                                        **********
c #CH END IF
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ BEGIN of LATERAL BOUNDARY CONDITIONS and FILTERING +++++++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
C +--Modification of the External Forcing
C +  ====================================
 
      IF  (mmx           .gt.1     )                              THEN
 
C +--LBC are provided by a Large Scale (3-D) Model
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        IF (reaLBC)                                               THEN
 
C +            ******
          call INIlbc(ihamr,nhamr,newlbc_0)
          call INIubc(ihamr,nhamr,newlbc_0)
C +            ******
 
C +            **********
c #db     call Debugg_MAR('INIlbc    ')
C +            **********
 
C +                               **********
          IF (newlbc_0.eq.1) call LBCnud_par
C +                               **********
 
C +            **********
c #db     call Debugg_MAR('LBCnud_par')
C +            **********
 
C +--LBC are provided by one Sounding (Horizontal Homogeneity is assumed)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ELSE
 
C +            ******
          call INIsnd
C +            ******
 
C +            **********
c #db     call Debugg_MAR('INIsnd    ')
C +            **********
 
C +            **********
          call LBCnud_par
C +            **********
 
C +            **********
c #db     call Debugg_MAR('LBCnud_par')
C +            **********
 
        END IF
 
 
C +--Lateral Boundary Conditions  for Mass Continuity p*
C +  ("Nudging" Type / Davies, QJRMS, 1976, pp.405--418)
C +    ("Open" Lateral Boundary Condition is possible)
C +  ===================================================
 
 
          ksig = 1
 
          iv   = 5
 
c #RB   IF (sommlb)                                               THEN
 
c #RB     IF (mmx.gt.1)                                           THEN
c #RB       DO j=1,my
c #RB           fu( 1,j) = 0.
c #RB           fu(mx,j) = 0.
c #RB         DO k=1,mz
c #RB           fu( 1,j) = fu( 1,j) + dsigm1(k)*uairDY( 1,j,k)
c #RB           fu(mx,j) = fu(mx,j) + dsigm1(k)*uairDY(mx,j,k)
c #RB         END DO
c #RB       END DO
c #RB     END IF
 
c #RB     IF (mmy.gt.1)                                           THEN
c #RB       DO i=1,mx
c #RB           fv(i, 1) = 0.
c #RB           fv(i,my) = 0.
c #RB         DO k=1,mz
c #RB           fv(i, 1) = fv(i, 1) + dsigm1(k)*vairDY(i, 1,k)
c #RB           fv(i,my) = fv(i,my) + dsigm1(k)*vairDY(i,my,k)
c #RB         END DO
c #RB       END DO
c #RB     END IF
 
C +            **********
c #RB     call LBCrad_atm(pstDYn,fu,fv,iv,ksig)
C +            **********
 
C +            **********
c #DB     call Debugg_MAR('LBCrad p* ')
C +            **********
 
c #RB   ELSE
 
          DO j=1,my
          DO i=1,mx
            dumy3D(i,j,1)=pstDYn(i,j)
          END DO
          END DO
C +
C +            **********
          call LBCnud_atm(dumy3D,iv,ksig)
C +            **********
C +
          DO j=1,my
          DO i=1,mx
            pstDYn(i,j) = dumy3D(i,j,1)
          END DO
          END DO
 
C +            **********
c #db     call Debugg_MAR('LBCnud p* ')
C +            **********
 
c #RB   END IF
 
 
C +--START of:
C +--Lateral Boundary Conditions  for Wind, Temperature, Specific Humidity
C +  ("Nudging" Type / Davies, QJRMS, 1976, pp.405--418)
C +  =====================================================================
 
 
c #WB                         dt_Bal = dt
C +              ******
c #WB       call H2O_WB(6,-1.,dt_Bal,.FALSE.,.FALSE.)
C +              ******
 
 
C +--Radiative Lateral Boundary Conditions: Auxiliary Variables
C +  ==========================================================
 
            kdim=mz
 
c #RB   DO   k=1,mz
c #RB     DO j=1,my
c #RB     DO i=1,mx
c #RB       dumy3U(i,j,k) = uairDY(i,j,k)
c #RB       dumy3V(i,j,k) = vairDY(i,j,k)
c #RB     END DO
c #RB     END DO
c #RB   END DO
 
 
C +--Lateral Boundary Conditions and Horizontal Filter
C +  =================================================
 
C +--Wind x-Direction
C +  ----------------
 
             iv = 1
 
C +--Dummy Variable
C +  ~~~~~~~~~~~~~~
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
            dumy3D(i,j,k) = uairDY(i,j,k)
          END DO
          END DO
        END DO
 
C +--Radiating LBC
C +  ~~~~~~~~~~~~~
c #RB   IF (sommlb)                                               THEN
C +
C +              **********
c #RB       call LBCrad_atm(dumy3D,dumy3U,dumy3V,iv,kdim)
C +              **********
 
C +--Nudging   LBC
C +  ~~~~~~~~~~~~~
c #RB   ELSE
 
C +              **********
            call LBCnud_atm(dumy3D,iv,kdim)
C +              **********
 
C +              **********
c #ob       call LBC000_run(dumy3D,   kdim)
C +              **********
 
c #RB   END IF
 
C +--Horizontal Filter
C +  ~~~~~~~~~~~~~~~~~
        IF   (FIk_fu(1).gt.0.0)                                   THEN
            DO  k=1,mz
              dumeps(    k) = FIk_fu(    k)
            END DO
              kdim          =            mz
 
          IF (mmy.le.1)                                           THEN
c #OB       IF (openLB)                                           THEN
 
C +                **********
c #OB         call DYNfil_1D0(dumy3D,dumeps,kdim)
C +                **********
 
c #OB       ELSE
 
C +                *********
              call DYNfil_1D (dumy3D,dumeps,kdim)
C +                *********
 
c #OB       END IF
 
          ELSE
c #OB       IF (openLB)                                           THEN
 
C +                **********
c #OB         call DYNfil_3D0(dumy3D,dumeps,kdim)
C +                **********
 
c #OB       ELSE
 
              IF (no_vec)                                         THEN
 
                    if(openmp) then
C +                      **********
                    call DYNfil_3D_mp (dumy3D,dumeps,kdim)
C +                      **********
                    else
C +                      **********
                    call DYNfil_3D (dumy3D,dumeps,kdim)
C +                      **********
                    endif
 
              ELSE
 
C +                  *********
                call DYNfilv3D (dumy3D,dumeps,kdim)
C +                  *********
 
              END IF
 
c #OB       END IF
          END IF
        END IF
 
C +--Update
C +  ~~~~~~
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
            uairDY(i,j,k)  =  dumy3D(i,j,k)
          END DO
          END DO
        END DO
 
 
C +--Wind y-Direction
C +  ----------------
 
             iv = 2
 
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
            dumy3D(i,j,k) = vairDY(i,j,k)
          END DO
          END DO
        END DO
 
C +--Radiating LBC
C +  ~~~~~~~~~~~~~
c #RB   IF (sommlb)                                               THEN
 
C +              **********
c #RB       call LBCrad_atm(dumy3D,dumy3U,dumy3V,iv,kdim)
C +              **********
 
C +--Nudging   LBC
C +  ~~~~~~~~~~~~~
c #RB   ELSE
 
C +              **********
            call LBCnud_atm(dumy3D,iv,kdim)
C +              **********
 
C +              **********
c #ob       call LBC000_run(dumy3D,   kdim)
C +              **********
 
c #RB   END IF
 
C +--Horizontal Filter
C +  ~~~~~~~~~~~~~~~~~
        IF   (FIk_fu(1).gt.0.0)                                   THEN
          IF (mmy.le.1)                                           THEN
            DO k=1,mz
              dumeps(    k) = FIk_su(    k)
            END DO
              kdim          =            mz
 
c #OB       IF (openLB)                                           THEN
 
C +                **********
c #OB         call DYNfil_1D0(dumy3D,dumeps,kdim)
C +                **********
 
c #OB       ELSE
 
C +                *********
              call DYNfil_1D (dumy3D,dumeps,kdim)
C +                *********
 
c #OB       END IF
 
          ELSE
            DO k=1,mz
              dumeps(    k) = FIk_fu(    k)
            END DO
              kdim          =            mz
 
c #OB       IF (openLB)                                           THEN
 
C +                **********
c #OB         call DYNfil_3D0(dumy3D,dumeps,kdim)
C +                **********
 
c #OB       ELSE
 
              IF (no_vec)                                         THEN
 
                    if(openmp) then
C +                      **********
                    call DYNfil_3D_mp (dumy3D,dumeps,kdim)
C +                      **********
                    else
C +                      **********
                    call DYNfil_3D (dumy3D,dumeps,kdim)
C +                      **********
                    endif
 
              ELSE
 
C +                  *********
                call DYNfilv3D (dumy3D,dumeps,kdim)
C +                  *********
 
              END IF
 
c #OB       END IF
          END IF
        END IF
 
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
            vairDY(i,j,k)  =  dumy3D(i,j,k)
          END DO
          END DO
        END DO
 
 
C +--Specific Humidity
C +  -----------------
 
             iv = 3
 
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
            dumy3D(i,j,k) =   qvDY(i,j,k)
          END DO
          END DO
        END DO
 
C +--Water Mass
C +  ~~~~~~~~~~
C +              ******
            call DYNqqm(dumy3D,'BAK','FIL_Qv')
C +              ******
 
C +--Radiating LBC
C +  ~~~~~~~~~~~~~
c #RB   IF (sommlb)                                               THEN
 
C +              **********
c #RB       call LBCrad_atm(dumy3D,dumy3U,dumy3V,iv,kdim)
C +              **********
 
C +--Nudging   LBC
C +  ~~~~~~~~~~~~~
c #RB   ELSE
 
C +              **********
            call LBCnud_atm(dumy3D,iv,kdim)
C +              **********
 
C +              **********
c #ob       call LBC000_run(dumy3D,   kdim)
C +              **********
 
c #RB   END IF
 
C +--Horizontal Filter
C +  ~~~~~~~~~~~~~~~~~
        IF   (FIk_st(1).gt.0.0)                                   THEN
            DO k=1,mz
              dumeps(    k) = FIslot
c #KQ         dumeps(    k) = FIk_st(    k)
            END DO
              kdim          =            mz
 
          IF (mmy.le.1)                                           THEN
c #OB       IF (openLB)                                           THEN
 
C +                **********
c #OB         call DYNfil_1D0(dumy3D,dumeps,kdim)
C +                **********
 
c #OB       ELSE
 
C +                *********
              call DYNfil_1D (dumy3D,dumeps,kdim)
C +                *********
 
c #OB       END IF
 
          ELSE
c #OB       IF (openLB)                                           THEN
 
C +                **********
c #OB         call DYNfil_3D0(dumy3D,dumeps,kdim)
C +                **********
 
c #OB       ELSE
 
              IF (no_vec)                                         THEN
 
                    if(openmp) then
C +                      **********
                    call DYNfil_3D_mp (dumy3D,dumeps,kdim)
C +                      **********
                    else
C +                      **********
                    call DYNfil_3D (dumy3D,dumeps,kdim)
C +                      **********
                    endif
 
              ELSE
 
C +                  *********
                call DYNfilv3D (dumy3D,dumeps,kdim)
C +                  *********
 
              END IF
 
c #OB       END IF
          END IF
        END IF
 
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
            dumy3Q(i,j,k)=max(dumy3D(i,j,k),epsq)
          END DO
          END DO
        END DO
 
C +--Restore the Water Vapor total Mass
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
C +              ******
            call DYNqqm(dumy3Q,'SET','FIL_Qv')
C +              ******
 
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
              qvDY(i,j,k) =   dumy3Q(i,j,k)
          END DO
          END DO
        END DO
 
 
C +--Potential Temperature
C +  ---------------------
 
             iv = 4
 
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
            dumy3D(i,j,k) = pktaDY(i,j,k)
          END DO
          END DO
        END DO
 
C +--Radiating LBC
C +  ~~~~~~~~~~~~~
c #RB   IF (sommlb)                                               THEN
 
C +              **********
c #RB       call LBCrad_atm(dumy3D,dumy3U,dumy3V,iv,kdim)
C +              **********
 
C +--Nudging   LBC
C +  ~~~~~~~~~~~~~
c #RB   ELSE
 
C +              **********
            call LBCnud_atm(dumy3D,iv,kdim)
C +              **********
 
C +              **********
c #ob       call LBC000_run(dumy3D,   kdim)
C +              **********
 
c #RB   END IF
C +
C +--Horizontal Filter
C +  ~~~~~~~~~~~~~~~~~
        IF   (FIk_st(1).gt.0.0)                                   THEN
            DO k=1,mz
              dumeps(    k) = FIk_st(    k)
            END DO
              kdim          =            mz
 
          IF (mmy.le.1)                                           THEN
c #OB       IF (openLB)                                           THEN
 
C +                **********
c #OB         call DYNfil_1D0(dumy3D,dumeps,kdim)
C +                **********
 
c #OB       ELSE
 
C +                *********
              call DYNfil_1D (dumy3D,dumeps,kdim)
C +                *********
 
c #OB       END IF
 
          ELSE
c #OB       IF (openLB)                                           THEN
 
C +                **********
c #OB         call DYNfil_3D0(dumy3D,dumeps,kdim)
C +                **********
 
c #OB       ELSE
 
              IF (no_vec)                                         THEN
 
                    if(openmp) then
C +                      **********
                    call DYNfil_3D_mp (dumy3D,dumeps,kdim)
C +                      **********
                    else
C +                      **********
                    call DYNfil_3D (dumy3D,dumeps,kdim)
C +                      **********
                    endif
 
              ELSE
 
C +                  *********
                call DYNfilv3D (dumy3D,dumeps,kdim)
C +                  *********
 
              END IF
 
c #OB       END IF
          END IF
        END IF
 
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
            pktaDY(i,j,k)  =  dumy3D(i,j,k)
          END DO
          END DO
        END DO
 
 
C +--TKE (Filtering only)
C +  --------------------
 
C +--Horizontal Filter
C +  ~~~~~~~~~~~~~~~~~
c #FE   IF   (FIk_st(1).gt.0.0)                                   THEN
c #FE       DO k=1,mz
c #FE       DO j=1,my
c #FE       DO i=1,mx
c #FE         dumy3D(i,j,k) = ect_TE(i,j,k)
c #FE       END DO
c #FE       END DO
c #FE       END DO
 
c #FE       DO k=1,mz
c #FE         dumeps(    k) = FIk_st(    k)
c #FE       END DO
c #FE         kdim          =            mz
 
c #FE     IF (mmy.le.1)                                           THEN
c #FO       IF (openLB)                                           THEN
 
C +                **********
c #FO         call DYNfil_1D0(dumy3D,dumeps,kdim)
C +                **********
 
c #FO       ELSE
 
C +                *********
c #FE         call DYNfil_1D(dumy3D,dumeps,kdim)
C +                *********
 
c #FO       END IF
 
c #FE     ELSE
c #FO       IF (openLB)                                           THEN
 
C +                **********
c #FO         call DYNfil_3D0(dumy3D,dumeps,kdim)
C +                **********
 
c #FO       ELSE
 
c #fe         IF (no_vec)                                         THEN
 
c #fe               if(openmp) then
C +                      **********
c #fe               call DYNfil_3D_mp (dumy3D,dumeps,kdim)
C +                      **********
c #fe               else
C +                      **********
c #fe               call DYNfil_3D (dumy3D,dumeps,kdim)
C +                      **********
c #fe               endif
 
c #fe         ELSE
 
C +                  *********
c #FE           call DYNfilv3D (dumy3D,dumeps,kdim)
C +                  *********
 
c #fe         END IF
 
c #FO       END IF
c #FE     END IF
c #FE   END IF
 
c #FE   DO   k=1,mz
c #FE     DO j=1,my
c #FE     DO i=1,mx
c #FE       ectnew        = max(epsi  ,dumy3D(i,j,k))
c #FE       tranTE(i,j,k) =    (ectnew-ect_TE(i,j,k)) * dt_inv
c #FE.    + tranTE(i,j,k)
c #FE       ect_TE(i,j,k) =     ectnew
c #FE     END DO
c #FE     END DO
c #FE   END DO
 
 
C +--Statistics
C +  ----------
 
C +          **********
c #DB   call Debugg_MAR('LBC u,v,..')
C +          **********
 
 
C +--Lateral Boundary Conditions and Horizontal Filter (Microphysics)
C +  ================================================================
 
        IF (iterun.eq.0)                                          THEN
              FIsloQ = FIslot
        END IF
 
        IF   (micphy)                                             THEN
 
 
C +--Filter Parameter, H2O Variables
C +  -------------------------------
 
              DO k = 1,mz
                dumeps(k) = FIsloQ/max(0.05,sigma(k))*sqrt(dt/60.)
cXF
              END DO
                kdim          = mz
 
 
C +--Cloud Ice Crystals Number
C +  -------------------------
 
               iv   = 3
               kdim = mz
C +
          DO   k=1,mz
            DO j=1,my
            DO i=1,mx
              dumy3D(i,j,k) = ccniHY(i,j,k)
            END DO
            END DO
          END DO
 
C +--Water Mass
C +  ~~~~~~~~~~
C +                ******
              call DYNqqm(dumy3D,'BAK','FIL_CN')
C +                ******
 
C +--Nudging   LBC
C +  ~~~~~~~~~~~~~
C +                **********
              call LBCnud_000(dumy3D,iv,kdim)
C +                **********
 
C +--Horizontal Filter
C +  ~~~~~~~~~~~~~~~~~
          IF     (FIsloQ   .gt.0.0)                               THEN
            IF   (mmy.le.1)                                       THEN
c #HO         IF (openLB)                                         THEN
 
C +                  **********
c #HO           call DYNfil_1D0(dumy3D,dumeps,kdim)
C +                  **********
 
c #HO         ELSE
 
C +                  *********
                call DYNfil_1D (dumy3D,dumeps,kdim)
C +                  *********
 
c #HO         END IF
 
            ELSE
c #HO         IF (openLB)                                         THEN
 
C +                  **********
c #HO           call DYNfil_3D0(dumy3D,dumeps,kdim)
C +                  **********
 
c #HO         ELSE
 
                IF (no_vec)                                       THEN
 
                    if(openmp) then
C +                      **********
                    call DYNfil_3D_mp (dumy3D,dumeps,kdim)
C +                      **********
                    else
C +                      **********
                    call DYNfil_3D (dumy3D,dumeps,kdim)
C +                      **********
                    endif
 
                ELSE
 
C +                    *********
                  call DYNfilv3D (dumy3D,dumeps,kdim)
C +                    *********
 
                END IF
 
c #HO         END IF
            END IF
          END IF
 
          DO   k=1,mz
            DO j=1,my
            DO i=1,mx
              dumy3Q(i,j,k) = max(dumy3D(i,j,k),zero)
            END DO
            END DO
          END DO
 
C +--Restore the Hydrometeor total Mass
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
C +                ******
              call DYNqqm(dumy3Q,'SET','FIL_CN')
C +                ******
 
          DO   k=1,mz
            DO j=1,my
            DO i=1,mx
              ccniHY(i,j,k)=      dumy3Q(i,j,k)
            END DO
            END DO
          END DO
 
 
C +--Cloud Ice Crystals Concentration
C +  --------------------------------
 
               iv   = 3
               kdim = mz
 
          DO   k=1,mz
            DO j=1,my
            DO i=1,mx
              dumy3D(i,j,k) =   qiHY(i,j,k)
            END DO
            END DO
          END DO
 
C +--Water Mass
C +  ~~~~~~~~~~
C +                ******
              call DYNqqm(dumy3D,'BAK','FIL_Qi')
C +                ******
 
C +--Nudging   LBC
C +  ~~~~~~~~~~~~~
C +                **********
              call LBCnud_000(dumy3D,iv,kdim)
C +                **********
 
C +--Horizontal Filter
C +  ~~~~~~~~~~~~~~~~~
          IF     (FIsloQ   .gt.0.0)                               THEN
            IF   (mmy.le.1)                                       THEN
c #HO         IF (openLB)                                         THEN
 
C +                  **********
c #HO           call DYNfil_1D0(dumy3D,dumeps,kdim)
C +                  **********
 
c #HO         ELSE
 
C +                  *********
                call DYNfil_1D (dumy3D,dumeps,kdim)
C +                  *********
 
c #HO         END IF
 
            ELSE
c #HO         IF (openLB)                                         THEN
 
C +                  **********
c #HO           call DYNfil_3D0(dumy3D,dumeps,kdim)
C +                  **********
 
c #HO         ELSE
 
                IF (no_vec)                                       THEN
 
                    if(openmp) then
C +                      **********
                    call DYNfil_3D_mp (dumy3D,dumeps,kdim)
C +                      **********
                    else
C +                      **********
                    call DYNfil_3D (dumy3D,dumeps,kdim)
C +                      **********
                    endif
 
                ELSE
 
C +                    *********
                  call DYNfilv3D (dumy3D,dumeps,kdim)
C +                    *********
 
                END IF
 
c #HO         END IF
            END IF
          END IF
 
          DO   k=1,mz
            DO j=1,my
            DO i=1,mx
              dumy3Q(i,j,k)=max(dumy3D(i,j,k),zero)
            END DO
            END DO
          END DO
 
C +--Restore the Hydrometeor total Mass
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
C +                ******
              call DYNqqm(dumy3Q,'SET','FIL_Qi')
C +                ******
 
          DO   k=1,mz
            DO j=1,my
            DO i=1,mx
                qiHY(i,j,k)=    dumy3Q(i,j,k)
            END DO
            END DO
          END DO
 
 
C +--Snow     Flakes
C +  ---------------
 
               iv   = 3
               kdim = mz
 
          DO   k=1,mz
            DO j=1,my
            DO i=1,mx
              dumy3D(i,j,k) =   qsHY(i,j,k)
            END DO
            END DO
          END DO
 
C +--Water Mass
C +  ~~~~~~~~~~
C +                ******
              call DYNqqm(dumy3D,'BAK','FIL_Qs')
C +                ******
 
C +--Nudging   LBC
C +  ~~~~~~~~~~~~~
C +                **********
              call LBCnud_000(dumy3D,iv,kdim)
C +                **********
 
C +--Horizontal Filter
C +  ~~~~~~~~~~~~~~~~~
          IF     (FIsloQ   .gt.0.0)                               THEN
            IF   (mmy.le.1)                                       THEN
c #HO         IF (openLB)                                         THEN
 
C +                  **********
c #HO           call DYNfil_1D0(dumy3D,dumeps,kdim)
C +                  **********
 
c #HO         ELSE
 
C +                  *********
                call DYNfil_1D (dumy3D,dumeps,kdim)
C +                  *********
 
c #HO         END IF
 
            ELSE
c #HO         IF (openLB)                                         THEN
 
C +                  **********
c #HO           call DYNfil_3D0(dumy3D,dumeps,kdim)
C +                  **********
 
c #HO         ELSE
 
                IF (no_vec)                                       THEN
 
                    if(openmp) then
C +                      **********
                    call DYNfil_3D_mp (dumy3D,dumeps,kdim)
C +                      **********
                    else
C +                      **********
                    call DYNfil_3D (dumy3D,dumeps,kdim)
C +                      **********
                    endif
C +
                ELSE
 
C +                   **********
                  call DYNfilv3D (dumy3D,dumeps,kdim)
C +                    *********
 
                END IF
 
c #HO         END IF
            END IF
          END IF
 
          DO   k=1,mz
            DO j=1,my
            DO i=1,mx
              dumy3Q(i,j,k)=max(dumy3D(i,j,k),zero)
            END DO
            END DO
          END DO
 
C +--Restore the Hydrometeor total Mass
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
C +                ******
              call DYNqqm(dumy3Q,'SET','FIL_Qs')
C +                ******
 
          DO   k=1,mz
            DO j=1,my
            DO i=1,mx
                qsHY(i,j,k)=    dumy3Q(i,j,k)
            END DO
            END DO
          END DO
 
 
C +--Cloud    Droplets
C +  -----------------
 
               iv   = 3
               kdim = mz
 
          DO   k=1,mz
            DO j=1,my
            DO i=1,mx
              dumy3D(i,j,k) =   qwHY(i,j,k)
            END DO
            END DO
          END DO
 
C +--Water Mass
C +  ~~~~~~~~~~
C +                ******
              call DYNqqm(dumy3D,'BAK','FIL_Qw')
C +                ******
 
C +--Nudging   LBC
C +  ~~~~~~~~~~~~~
C +                **********
              call LBCnud_000(dumy3D,iv,kdim)
C +                **********
 
C +--Horizontal Filter
C +  ~~~~~~~~~~~~~~~~~
          IF     (FIsloQ   .gt.0.0)                               THEN
            IF   (mmy.le.1)                                       THEN
c #HO         IF (openLB)                                         THEN
 
C +                  **********
c #HO           call DYNfil_1D0(dumy3D,dumeps,kdim)
C +                  **********
 
c #HO         ELSE
 
C +                  *********
                call DYNfil_1D (dumy3D,dumeps,kdim)
C +                  *********
 
c #HO         END IF
 
            ELSE
c #HO         IF (openLB)                                         THEN
 
C +                  **********
c #HO           call DYNfil_3D0(dumy3D,dumeps,kdim)
C +                  **********
 
c #HO         ELSE
 
                IF (no_vec)                                       THEN
 
                    if(openmp) then
C +                      **********
                    call DYNfil_3D_mp (dumy3D,dumeps,kdim)
C +                      **********
                    else
C +                      **********
                    call DYNfil_3D (dumy3D,dumeps,kdim)
C +                      **********
                    endif
 
                ELSE
 
C +                    *********
                  call DYNfilv3D (dumy3D,dumeps,kdim)
C +                    *********
 
                END IF
 
c #HO         END IF
            END IF
          END IF
 
          DO   k=1,mz
            DO j=1,my
            DO i=1,mx
              dumy3Q(i,j,k)=max(dumy3D(i,j,k),zero)
            END DO
            END DO
          END DO
 
C +--Restore the Hydrometeor total Mass
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
C +                ******
              call DYNqqm(dumy3Q,'SET','FIL_Qw')
C +                ******
 
          DO   k=1,mz
            DO j=1,my
            DO i=1,mx
                qwHY(i,j,k) =   dumy3Q(i,j,k)
            END DO
            END DO
          END DO
 
 
C +--Rain     Drops
C +  --------------
 
               iv   = 3
               kdim = mz
 
          DO   k=1,mz
            DO j=1,my
            DO i=1,mx
              dumy3D(i,j,k) =     qrHY(i,j,k)
            END DO
            END DO
          END DO
 
C +--Water Mass
C +  ~~~~~~~~~~
C +                ******
              call DYNqqm(dumy3D,'BAK','FIL_Qr')
C +                ******
 
C +--Nudging   LBC
C +  ~~~~~~~~~~~~~
C +                  **********
                call LBCnud_000(dumy3D,iv    ,kdim)
C +                  **********
 
C +--Horizontal Filter
C +  ~~~~~~~~~~~~~~~~~
          IF     (FIsloQ   .gt.0.0)                               THEN
            IF   (mmy.le.1)                                       THEN
c #HO         IF (openLB)                                         THEN
 
C +                  **********
c #HO           call DYNfil_1D0(dumy3D,dumeps,kdim)
C +                  **********
 
c #HO         ELSE
 
C +                  *********
                call DYNfil_1D (dumy3D,dumeps,kdim)
C +                  *********
 
c #HO         END IF
 
            ELSE
c #HO         IF (openLB)                                         THEN
 
C +                  **********
c #HO           call DYNfil_3D0(dumy3D,dumeps,kdim)
C +                  **********
 
c #HO         ELSE
 
                IF (no_vec)                                       THEN
 
                    if(openmp) then
C +                      **********
                    call DYNfil_3D_mp (dumy3D,dumeps,kdim)
C +                      **********
                    else
C +                      **********
                    call DYNfil_3D (dumy3D,dumeps,kdim)
C +                      **********
                    endif
 
                ELSE
 
C +                    *********
                  call DYNfilv3D (dumy3D,dumeps,kdim)
C +                    *********
 
                END IF
 
c #HO         END IF
            END IF
          END IF
 
          DO   k=1,mz
            DO j=1,my
            DO i=1,mx
              dumy3Q(i,j,k)=max(dumy3D(i,j,k),zero)
            END DO
            END DO
          END DO
 
C +--Restore the Hydrometeor total Mass
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
C +                ******
              call DYNqqm(dumy3Q,'SET','FIL_Qr')
C +                ******
 
          DO   k=1,mz
            DO j=1,my
            DO i=1,mx
                qrHY(i,j,k)=    dumy3Q(i,j,k)
            END DO
            END DO
          END DO
 
 
C +--Statistics
C +  ----------
 
c #WB                         dt_Bal = dt
C +              ******
c #WB       call H2O_WB(6, 1.,dt_Bal,.FALSE.,.FALSE.)
C +              ******
 
        END IF
 
C +          **********
c #DB   call Debugg_MAR('DYNfil qHY')
C +          **********
 
 
C +--Filtering  of Tracer        Variables
C +  =====================================
 
        IF    (ntracr.gt.0)                                       THEN
c #TC   DO n=1,ntrac
 
 
C +--Mass
C +  ----
 
c #TC     DO   k=1,mz
c #TC         sumv(      k) =   0.0
c #TC         dumeps(    k) =   FIsloQ
c #TC       DO j=1,my
c #TC       DO i=1,mx
c #TC         sumv(      k) =   qxTC(i,j,k,n)*pstDYn(i,j) + sumv(k)
c #TC         dumy3D(i,j,k) =   qxTC(i,j,k,n)
c #TC       END DO
c #TC       END DO
c #TC     END DO
 
 
C +--Filtering (2D)
C +  --------------
 
c #TC     IF   (mmy.le.1)                                         THEN
c #TO       IF (openLB)                                           THEN
 
C +           ***************
c #TO         call DYNfil_1D0(dumy3D,dumeps,kdim)
C +           ***************
 
c #TO       ELSE
 
C +           **************
c #TC         call DYNfil_1D (dumy3D,dumeps,kdim)
C +           **************
 
c #TO       END IF
 
 
C +--Filtering (3D)
C +  --------------
 
c #TC     ELSE
c #TO       IF (openLB)                                           THEN
 
C +           ***************
c #TO         call DYNfil_3D0(dumy3D,dumeps,kdim)
C +           ***************
 
c #TO       ELSE
 
c #tc           IF (no_vec)                                       THEN
 
c #tc               if(openmp) then
C +                      **********
c #tc               call DYNfil_3D_mp (dumy3D,dumeps,kdim)
C +                      **********
c #tc               else
C +                      **********
c #tc               call DYNfil_3D (dumy3D,dumeps,kdim)
C +                      **********
c #tc               endif
 
c #tc           ELSE
 
C +               **************
c #TC             call DYNfilv3D (dumy3D,dumeps,kdim)
C +               **************
 
c #tc           END IF
 
c #TO       END IF
c #TC     END IF
 
 
C +--Restore Mass
C +  ------------
 
c #TC     DO   k=1,mz
c #TC         sumvn   =   0.0
c #TC       DO j=1,my
c #TC       DO i=1,mx
c #TC                     qxTC(i,j,k,n) = max(zero,dumy3D(i,j,k))
c #TC         sumvn   =   qxTC(i,j,k,n) * pstDYn(i,j)  +  sumvn
c #TC       END DO
c #TC       END DO
 
c #TC       IF (sumvn.gt.0.0)                                     THEN
c #TC           sumvn = sumv(k) / sumvn
c #TC         DO j=1,my
c #TC         DO i=1,mx
c #TC                     qxTC(i,j,k,n) = qxTC(i,j,k,n) * sumvn
c #TC         END DO
c #TC         END DO
c #TC       ELSE
c #TC         DO j=1,my
c #TC         DO i=1,mx
c #TC                     qxTC(i,j,k,n) = 0.0
c #TC         END DO
c #TC         END DO
c #TC       END IF
 
c #TC     END DO
 
c #TC   END DO
        END IF
 
C +          **********
c #db   call Debugg_MAR('DYNfil qx ')
C +          **********
 
      END IF
 
 
C +--Horizontal Dynamical Balance (Filtering)
C +  ========================================
 
c #DY  ibd= 6
c #DY afdt= 1.0
 
C +        ******
c #DY call dynbil(tdt,afdt,jmez,ibd)
C +        ******
 
 
C +--HAMMING Filter performs the Diabatic Initialisation
C +  = Lynch and Huang 1992, Huang and Lynch 1993, MWR =
C +   =================================================
 
c #HF IF (hamfil)                                                 THEN
 
c #HF   IF (itexpe.eq.0.and.iham.eq.-nham)                        THEN
c #HF       jham    =  -iham
 
c #HF     DO j=1,my
c #HF     DO i=1,mx
c #HF       pnnham(i,j)   = pstDYn (i,j)
c #HF     END DO
c #HF     END DO
 
c #HF     DO k=1,mz
c #HF     DO j=1,my
c #HF     DO i=1,mx
c #HF       uuuham(i,j,k) = uairDY(i,j,k)
c #HF       vvvham(i,j,k) = vairDY(i,j,k)
c #HF       pktham(i,j,k) = pktaDY(i,j,k)
c #HF       qvaham(i,j,k) =   qvDY(i,j,k)
c #HF     END DO
c #HF     END DO
c #HF     END DO
c #HF   END IF
 
c #HF   IF (itexpe.eq.0.and.iham.le. nham.and.iham.ne.0)          THEN
c #HF       argham  =      jham*fham
c #HF       hhhnnn  = (sin(jham*thac)/(jham*pi))*sin(argham)/argham
 
c #HF     DO j=1,my
c #HF     DO i=1,mx
c #HF       pnnham(i,j)   = pstDYn (i,j)  *hhhnnn + pnnham(i,j)
c #HF     END DO
c #HF     END DO
 
c #HF     DO k=1,mz
c #HF     DO j=1,my
c #HF     DO i=1,mx
c #HF       uuuham(i,j,k) = uairDY(i,j,k)*hhhnnn + uuuham(i,j,k)
c #HF       vvvham(i,j,k) = vairDY(i,j,k)*hhhnnn + vvvham(i,j,k)
c #HF       pktham(i,j,k) = pktaDY(i,j,k)*hhhnnn + pktham(i,j,k)
c #HF       qvaham(i,j,k) =   qvDY(i,j,k)*hhhnnn + qvaham(i,j,k)
c #HF     END DO
c #HF     END DO
c #HF     END DO
c #HF   END IF
 
c #HF   IF (itexpe.eq.0.and.iham.eq.nham)                         THEN
 
c #HF     write(21,1560)
 1560     format(/,'   --- Initialisation / END Hamming Filter ---',/)
c #HF     DO j=1,my
c #HF     DO i=1,mx
c #HF       pstDY  (i,j)  = pnnham(i,j)
c #HF       pstDYn (i,j)  = pnnham(i,j)
c #HF       pstDY1 (i,j)  = pnnham(i,j)
c #HF     END DO
c #HF     END DO
 
c #HF     DO k=1,mz
c #HF     DO j=1,my
c #HF     DO i=1,mx
c #HF       uairDY(i,j,k) =                     uuuham(i,j,k)
c #HF       vairDY(i,j,k) =                     vvvham(i,j,k)
c #HF       pktaDY(i,j,k) =                     pktham(i,j,k)
c #HF         qvDY(i,j,k) =                     qvaham(i,j,k)
c #HF     END DO
c #HF     END DO
c #HF     END DO
 
c #HF       ihamr  =      0
c #HF       nhamr  =      0
c #HF       itexpe =   nham
 
c #HF   END IF
 
c #HF   IF (iham.gt.nham)                                    GO TO 151
c #HF     iham   = 1+iham
c #HF     ihamr  =   iham
c #HF                                                        GO TO 150
 151    CONTINUE
 
c #HF END IF
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ END   of DIABATIC INITIALISATION +++++++++++++++++++++++++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
C +--Global Correction for p*
C +  ========================
 
C +        **********
c #PS call DYNdps_cor(pav)
C +        **********
 
C +        **********
c #db call Debugg_MAR('DYNdps_cor')
C +        **********
 
 
C +--Top Boundary Conditions (fixed)
C +  ===============================
 
c #Di DO j=1,my
c #Di DO i=1,mx
c #Di     qvDY(i,j,1) = qvtoDI(i,j)
c #Di   pktaDY(i,j,1) = pkttDI(i,j)
c #Di   uairDY(i,j,1) = uairDI(i,j)
c #Di   vairDY(i,j,1) = vairDI(i,j)
c #Di END DO
c #Di END DO
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ PHYSICS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
      GO TO 41
 
 
C +--BEGIN Feed Stand Alone Surface Model
C +  ====================================
 
 4    CONTINUE
 
 
C +--Surface Layer
C +  -------------
 
c #FR   IF (VSISVAT)                                              THEN
 
              DO j=1,my
              DO i=1,mx
                ssvSL(i,j,mz) = sqrt(max(uairDY(i,j,mz)*uairDY(i,j,mz)
     .                                  +vairDY(i,j,mz)*vairDY(i,j,mz)
     .                                  ,epsi))
              END DO
              END DO
 
c #FR   ELSE
c #FR                 dt_Loc = dt
 
C +            ******
c #FR     call TURsbl
C +            ******
 
c #FR   END IF
 
 
C +--END   Feed Stand Alone Surface Model
C +  ====================================
 
 41   CONTINUE
 
 
C +--Radiative Processes and 1D Surface Physics
C +  ==========================================
 
      IF     (physic)                                               THEN
 
        IF   (mod(itexpe,jtRadi).eq.0)                              THEN
 
c #MR     IF (RadMAR)                                               THEN
 
C +                **********
c #MR         call PHYrad_MAR
C +                **********
 
c #MR     ELSE
c #LL       IF (RadLMD)                                             THEN
 
C +                **********
c #LL         call PHYrad_LMD
C +                **********
 
c #LL       ELSE
 
C +                **********
              call PHYrad_top(DistST)
C +                **********
 
              if (openmp.and.klonr.eq.1) then
               if(iterun<=2.or.itexpe<100) then
C +                  **********
                call PHYrad_CEP(DistST)
                call PHYrad_CEP_mp(DistST)
C +                  **********
               else
C +                  *************
                call PHYrad_CEP_mp(DistST)
C +                  *************
               endif
              else
C +                 **********
               call PHYrad_CEP(DistST)
C +                 **********
              endif
c #LL       END IF
 
c #MR     END IF
 
        END IF
 
        IF   (mod(itexpe,  jtPhys).eq.0)                            THEN
 
          IF (VSISVAT)                                              THEN
 
            if(openmp) then
C +              *************
            call PHY_SISVAT_MP(ihamr ,nhamr )
C +              *************
            else
            DO      itPhys=1,ntPhys
C +              **********
            call PHY_SISVAT(ihamr ,nhamr )
C +              **********
            ENDDO
            endif
 
          ELSE
 
           DO      itPhys=1,ntPhys
C +              **********
            call PHY_KDsvat(ihamr ,nhamr )
C +              **********
           ENDDO
          END IF
 
        END IF
 
 
C +          **********
c #DB   call Debugg_MAR('PHYgen    ')
C +          **********
 
      END IF
 
 
C +--Polynya Model
C +  =============
 
c #PO IF (polmod)                                                   THEN
 
C +          **********
c #PO   call SRFmod_pol
C +          **********
 
C +          **********
c #db   call Debugg_MAR('SRFmod_pol')
C +          **********
 
c #PO END IF
 
 
C +--Update of Surface Temperature
C +  =============================
 
      DO n=1,mw
      DO j=1,my
      DO i=1,mx
        tsrfSL(i,j,n)   =   tsrfSL(i,j,n) +  dtgSL(i,j,n)
      END DO
      END DO
      END DO
 
 
      if (reaLBC)                                                   THEN
 
C +          **********
        call LBCnud_srf
C +          **********
 
C +          **********
c #db   call Debugg_MAR('LBCnud_srf')
C +          **********
 
      END IF
 
      DO j=1,my
      DO i=1,mx
        TairSL(i,j)     = 0.
      END DO
      END DO
      DO iw=1,mw
      DO j=1,my
      DO i=1,mx
        TairSL(i,j)     = TairSL(i,j) + SLsrfl(i,j,iw) * tsrfSL(i,j,iw)
      END DO
      END DO
      END DO
      DO j=1,my
      DO i=1,mx
        pktaDY(i,j,mzz) = TairSL(i,j) / exp(cap*log(pstDY(i,j)+ptopDY))
      END DO
      END DO
 
 
C +--Min./Max. 2-m Air Temperature
C +  =============================
 
C +        ******
c #T2 call TUR_2m
C +        ******
 
C +        **********
c #db call Debugg_MAR('TUR_2m    ')
C +        **********
 
 
C +--Contribution from Diabatic Heating
C +  ==================================
 
c #K1  pente = 0.005
c #K1  dthdz = (tSND(20,1)-tSND(15,1))/(zSND(20,1)-zSND(15,1)) + 9.77d-3
C +... 1D Katabatic Flow  Simulation (Forcing : dTheta/dt = -w dTheta/dz)
       DO k=1,mz
       DO j=1,my
       DO i=1,mx
c #GR    IF (i.ge.imez-2.and.
c #GR.       i.le.imez+2     )
c #GR.     pktRAd(i,j,k) = pktRAd(i,j,k) -31.d-6*dt
C +...     2D Gravity Current Simulation (Forcing : -10C/jour)
           pktaDY(i,j,k)=pktaDY(i,j,k) + pktRAd(i,j,k)
c #K1.      + dt *pente *uairDY(i,j,k) *dthdz
c #K1.           /exp(cap*log(sigma(k) *  pstDY(i,j)+ptopDY))
       END DO
       END DO
       END DO
 
 
C +--cpl : GIVE FIELDS TO OASIS
C +  ==========================
 
C +        ***********
c #AO call MAR_2_OASIS
C +        ***********
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ TIME BASE ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
      itexpe  =   itexpe + 1
      iterun  =   iterun + 1
 
C +        ******
      call TIMgeo
      call TIMcur
C +        ******
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ OUTPUT +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
C +--Model Domain Water Budget
C +  =========================
 
C +        ******
c #WB call H2O_WB(9, 0.,dt_Bal,.FALSE.,.FALSE.)
C +        ******
 
 
C +--Linear Mountain Wave Experiment: RMSE
C +  =====================================
 
C +        ******
c #OL call OUTlmw
C +        ******
 
 
C +--Ice-Sheet Surface Mass Balance
C +  ==============================
 
C +                               ******
      if(iterun.eq.1.or.mod(iterun,4).eq.0) call OUTice
C +                               ******
 
 
C +--Particular Output for Wind Vector
C +  =================================
 
                                                            iout=0
c #W6                                                       iout=1
      IF (mmy.gt.1.and.mod(jmmMAR, 2).eq.0.and.jssMAR.eq.0) iout=1
      IF (mmy.eq.1.and.mod(jmmMAR,10).eq.0.and.jssMAR.eq.0) iout=1
c #SA                                                       iout=0
 
      IF (iout.eq.1)                                                THEN
 
              idum  = 1
              jdum  = 1
              adum  = 0.0
        DO i=ip11,mx1
        DO j=1,my
          IF (adum.LT.abs(uairDY(ip1(i),j,mz)-uairDY(im1(i),j,mz))) THEN
              idum  = i
              jdum  = j
              adum  = abs(uairDY(ip1(i),j,mz)-uairDY(im1(i),j,mz))
          END IF
        END DO
        END DO
 
C +--Non-Hydrostatic Pressure Perturbation
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          pnhLav = 0.
          pnh_av = 0.
c #NH   DO k=   1,mz
c #NH   DO j=jp11,my1
c #NH     pnhLav = pnhLav+pairNH(ip11,   j,k)*pstDYn(ip11,   j)*sigma(k)
c #NH.                   +pairNH( mx1,   j,k)*pstDYn( mx1,   j)*sigma(k)
c #NH   ENDDO
c #NH   IF (mmy.GT.1)                                             THEN
c #NH   DO i=ip11,mx1
c #NH     pnhLav = pnhLav+pairNH(   i,jp11,k)*pstDYn(   i,jp11)*sigma(k)
c #NH.                   +pairNH(   i, my1,k)*pstDYn(   i, my1)*sigma(k)
c #NH   ENDDO
c #NH   END IF
c #NH   DO j=jp11,my1
c #NH   DO i=ip11,mx1
c #NH     pnh_av = pnh_av+pairNH(   i,   j,k)*pstDYn(   i,   j)*sigma(k)
c #NH   END DO
c #NH   END DO
c #NH   END DO
c #NH   IF (mmy.EQ.1)                                             THEN
c #NH     pnhLav = pnhLav /(       2           *mz)
c #NH     pnh_av = pnh_av /(   (mx-2)          *mz)
c #NH   ELSE
c #NH     pnhLav = pnhLav /((2*(mx-2)+2*(my-2))*mz)
c #NH     pnh_av = pnh_av /(   (mx-2)  *(my-2) *mz)
c #NH   END IF
 
         id6 =            6
         idum=max(idum,   7)
         idum=min(idum,mx-6)
C +...   2-D and 3-D Simulations
 
c #GR    idum=         mx-6
C +
c #BS   IF (mmy.eq.1)
c #BS.   idum=         imez
 
        IF (mmx.eq.1)                                             THEN
         id6 =            0
         idum=            1
C +...   1-D         Simulations
 
        END IF
 
        DO i=idum-id6,idum+id6
            vecx1(i)= 10.0 *(pstDYn(i,jdum) + ptopDY)
c #NH       vecx3(i)= 10.0 * pstDYn(i,jdum) * pairNH(i,jdum,mz)
c #W6       vecx4(i)=        tairDY(i,jdum,mz)
c #W6.                     - TfSnow
c #BS       vecx4(i)=  0.0
c #BS    DO k=1,mz
c #BS       vecx4(i)= vecx4(i)+ssvSL(i,jdum,k)*qsHY(i,jdum,k)
c #BS.                        *pstDY(i,jdum)*dsigm1(       k)
c #BS.                        *1.0e3        *grvinv
c #BS    END DO
        END DO
        IF (mmx.gt.1 .and. mmy.eq.1)                              THEN
          DO i=imez-10, imez+30
            vecx2(i)= 10.0 *(pstDYn(i,1)-pstDYn(imez-10,1)
     .                      -pstDY1(i,1)+pstDY1(imez-10,1))
          END DO
        END IF
        IF   (mmx.eq.1)                                           THEN
          write(21,21)itexpe,jdarGE,    labmGE(mmarGE),iyrrGE,
     .                       jhlrGE(iSND,jSND),minuGE ,jsecGE,
     .                (uairDY(1,1,k),k=mz-9,mz),
     .                (vairDY(1,1,k),k=mz-9,mz)
 21       format(i5,i3,'-',a3,'-',i4,'/',i2,'.',i2,'.',i2,' ||',10f6.2,
     .         /,24x,                                   'LT ||',10f6.2)
        ELSE
          IF (mmy.gt.1)                                           THEN
c #W6       write( 6,22)itexpe,jdarGE,labmGE(mmarGE),iyrrGE,
c #W6.                         jhurGE,       minuGE ,jsecGE,
c #W6.                  (uairDY(i,jdum,mz),i=idum-6,idum-1),
c #W6.        idum,jdum,(uairDY(i,jdum,mz),i=idum,idum+5),
c #W6.        ttime,                  itizGE(idum,jdum),
c #W6.                  (vecx1(i),i=idum-6,idum-1),
c #W6.       xxkm(idum),(vecx1(i),       i=idum,idum+5)
c #W6.                 ,(vecx4(i),i=idum-6,idum-1),
c #W6.                  (vecx4(i),       i=idum,idum+5)
            write(21,22)itexpe,jdarGE,labmGE(mmarGE),iyrrGE,
     .                         jhurGE,       minuGE ,jsecGE,
     .                  (uairDY(i,jdum,mz),i=idum-6,idum-1),
     .        idum,jdum,(uairDY(i,jdum,mz),i=idum,idum+5),
     .        ttime,                  itizGE(idum,jdum),
     .                  (vecx1(i),i=idum-6,idum-1),
     .       xxkm(idum),(vecx1(i),       i=idum,idum+5)
c #W6.                 ,(vecx4(i),i=idum-6,idum-1),
c #W6.                  (vecx4(i),       i=idum,idum+5)
 22         format(i7,i3,'-',a3,'-',i4,'/',i2,'.',i2,'.',i2,' ||',6f7.1,
     .                             ' | (',i3,',',i3,')',f5.1,' |',5f7.1,
     .                                /,3x,a8,9x,'UT (',i3,') ||',6f7.1,
     .                             ' |',f6.0,'km',      f7.1,' |',5f7.1
c #W6.                            ,   /,3x,8x,9x,'    ',3x,'  ||',6f7.2,
c #W6.                             ' |', 6x ,'  ',      f7.2,' |',5f7.2
     .                                                                 )
          ELSE
c #W6       write( 6,23)itexpe,jdarGE,labmGE(mmarGE),iyrrGE,
c #W6.                         jhurGE,       minuGE ,jsecGE,
c #W6.                  (uairDY(i,jdum,mz),i=idum-6,idum-1),
c #W6.        idum,jdum,(uairDY(i,jdum,mz),i=idum,idum+5),
c #W6.        ttime,                  itizGE(idum,jdum),
c #W6.                  (vecx1(i),i=idum-6,idum-1),
c #W6.       xxkm(idum),(vecx1(i),       i=idum,idum+5)
c #W6.                 ,(vecx4(i),i=idum-6,idum-1),
c #W6.                  (vecx4(i),       i=idum,idum+5)
            write(21,23)itexpe,jdarGE,labmGE(mmarGE),iyrrGE,
     .                         jhurGE,       minuGE ,jsecGE,
     .                  (uairDY(i,jdum,mz),i=idum-6,idum-1),
     .        idum,jdum,(uairDY(i,jdum,mz),i=idum,idum+5),
     .        ttime,                  itizGE(idum,jdum),
     .                  (vecx1(i),i=idum-6,idum-1),
     .       xxkm(idum),(vecx1(i),       i=idum,idum+5)
c #W6.                 ,(vecx4(i),i=idum-6,idum-1),
c #W6.                  (vecx4(i),       i=idum,idum+5)
 23         format(i7,i3,'-',a3,'-',i4,'/',i2,'.',i2,'.',i2,' ||',6f7.1,
     .                             ' | (',i4,',',i2,')',f5.1,' |',5f7.1,
     .                                /,3x,a8,9x,'UT (',i3,') ||',6f7.1,
     .                             ' |',f6.0,'km',      f7.1,' |',5f7.1
c #W6.                            ,   /,3x,8x,9x,'    ',3x,'  ||',6f7.2,
c #W6.                             ' |', 6x ,'  ',      f7.2,' |',5f7.2
     .                                                                 )
          END IF
c #w6       write( 6,24)(vecx3(i),i=idum-6,idum-1),
c #w6.                  (vecx3(i),       i=idum,idum+5)
c #NH       write(21,24)(vecx3(i),i=idum-6,idum-1),
c #NH.                  (vecx3(i),       i=idum,idum+5)
 24         format(                     3x,8x,9x,'    ',3x,'  ||',6f7.1,
     .                             ' |', 6x ,'  ',      f7.1,' |',5f7.1)
c #w6       write( 6,25) 1000.*pnh_av,1000.*pnhLav
c #w6       write(21,25) 1000.*pnh_av,1000.*pnhLav
 25         format(             '   p_HN Averages (Domain/LB) ||',42x  ,
     .                             ' | [Pa]   ',        f7.1,' |', f7.1)
        END IF
C +
        IF (mmx.gt.1 .and. mmy.eq.1)                              THEN
          ppp = 10*pstDY(imez-10,1)
          write(22,221)itexpe,(uairDY(i,1,mz),       i=imez-10,imez+30)
          write(23,221)itexpe,(tairDY(i,1,mz)-TfSnow,i=imez-10,imez+30)
 221      format(i10,20f5.1,/,10x,21f5.1)
          write(24,223)itexpe,(vecx2(i), i=imez-10,imez+30)
 223      format(i10,20f5.2,/,10x,21f5.2)
        END IF
C +
c #OL     write(21,2004)
c #OL.     ((1.d3*(uairDY(imez,1,k)-ugeoDY(imez,1,k))),k=mlh,mlg),
c #OL.                      (0.1019*gplvDY(imez,1,k)  ,k=mlh,mlg)
 2004     format(21x,                          ' || ',15f7.2,
     .         /,21x,                          ' || ',15f7.0)
      END IF
C +
C +
C +--Horizontal Dynamical Balance (Summary)
C +  ======================================
C +
c #DY  ibd= 7
c #DY  tdt= 0.0
c #DY afdt= 0.0
C +
C +   ***********
c #DY call dynbil(tdt,afdt,jmez,ibd)
C +   ***********
C +
C +
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ END   of the INTERNAL TIME INCREMENTATION (nboucl over dt) +++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +
C +
          iboucl =  iboucl + 1
      IF (iboucl.le.nboucl)                                   GO TO  2
C +
C +
C +--Vertical Wind Speed wairDY (z Coordinate system)
C +  ================================================
C +
c #SA IF (sALONE)                                             GO TO 40
C +
C +                                   ***********
        IF (.not.convec.and.mmx.gt.1) call DYNwww
C +                                   ***********
C +
C +
c #WA     wwwabs=00.d+0
c #WA   DO k=1,mz
c #WA   DO j=1,my
c #WA   DO i=1,mx
c #WA     IF      (abs(wairDY(i,j,k)).gt.wwwabs)                  THEN
c #WA       wwwabs=abs(wairDY(i,j,k))
c #WA       wwwmax=    wairDY(i,j,k)
c #WA       i_wmax=i
c #WA       j_wmax=j
c #WA       k_wmax=k
c #WA     END IF
c #WA   END DO
c #WA   END DO
c #WA   END DO
c #WA       write(6,6010) i_wmax,j_wmax,sh(i_wmax,j_wmax),
c #WA.                        0.109*gplvDY(i_wmax,j_wmax,k_wmax),
c #WA.                              wairDY(i_wmax,j_wmax,k_wmax),
c #WA.                             (psigDY(i_wmax,j_wmax,k),k=1,mz)
 6010       format(6x,' z_Surf(',i3,',',i3,') =',f7.0,
     .             6x,'  w_Max(',f8.0,'m) =',f6.0,'cm/s',
     .           /,6x,' psigDY:',10f9.6,/,(14x,10f9.6))
C +
 40   CONTINUE
 
 
C +--OUTPUT for Graphs
C +  =================
 
           iprint =      iprint + 1
           ipr_nc =      ipr_nc + 1
 
      IF (log_nc.eq.1)                                            THEN
C +
C +...              dt_Loc is assumed
 
C +     ***********
        call OUT_nc
C +     ***********
 
      ELSE
 
C +     ***********
        call OUTgks
C +     ***********
 
      END IF
 
 
C +--Save of Model Variables
C +  =======================
 
      IF (safVAR.and.jdh_LB.ne.-1)                                THEN
C +
C +     ***********
        call OUTsav
C +     ***********
C +
      ELSE IF       (jdh_LB.eq.-1)                                THEN
        write(6,6600)
 6600   format(/,'############################################',
     .         /,'# NO LATERAL BOUNDARY CONDITIONS AVAILABLE #',
     .         /,'############################################',/,1x)
        stop
      END  IF
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ END   of the EXTERNAL TIME INCREMENTATION (nprint over dt * nboucl) ++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
      IF (iprint.ge.nprint)                                   GO TO 30
      GO TO 3
 30   CONTINUE
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ CLOSE FILES ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
      close(unit=4)
 
      close(unit=21)
 
      IF (mx.gt.1.and.my.le.1)                                    THEN
        close(unit=22)
        close(unit=23)
        close(unit=24)
      END IF
 
c #WE close(unit=25)
 
c #SE close(unit=29)
 
c #DY close(unit=61)
c #DY close(unit=62)
c #DY close(unit=63)
c #DY close(unit=64)
c #DY close(unit=65)
c #DY close(unit=66)
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ END OF RUN +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
C +   ***********
      call SBCnew
C +   ***********
 
 
C +--PSMILe termination (cpl)
C +  ========================
 
C +   **************************
c #AO CALL prism_terminate_proto (info)
C +   **************************
 
c #AO IF (info .NE. PRISM_Ok) THEN
c #AO     WRITE (6,*) 'An error occured in '
c #AO     WRITE (6,*) 'prism_terminate = '
c #AO     WRITE (6,*) info
c #AO END IF
 
 
C +--MAR    termination
C +  ==================
 
      open (unit=1,status='unknown',file='MAR.OK')
           write(1,1000) itexpe,jdarGE,labmGE(mmarGE),iyrrGE,
     .                          jhurGE,       minuGE ,jsecGE
 1000      format('MAR execution stopped normaly at iteration',i8,
     .          /,'Time          is',i13,'-',a3,'-',i4,
     .                            '/',i2,'.',i2,'.',i2,' UT')
      close(unit=1)
 
      stop
      end
