 
 
      subroutine DYNadv_LFB(norder)
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS   SLOW                                     4-11-2004  MAR |
C |   SubRoutine DYNadv_LFB manages Leap-Frog Backward   Advection Scheme  |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT/  (via common block)                                           |
C |   ^^^^^^        iterun          : Long Time Step Counter               |
C |                 ntFast          : Time      Step Counter Maximum Value |
C |                 micphy          : Hydrometeors   Switch                |
C |                                                                        |
C |   INPUT/  (via common block)                                           |
C |   OUTPUT        pktaDY(mx,my,mzz) Potent. Temperat. / p_0**kappa       |
C |   ^^^^^^          qvDY(mx,my,mz): Water Vapor  Concentration   [kg/kg] |
C |                 ccniHY(mx,my,mz): Ice crystals Number              [-] |
C |                   qiHY(mx,my,mz): Ice crystals Concentration   [kg/kg] |
C |                   qsHY(mx,my,mz): Snow  Flakes Concentration   [kg/kg] |
C |                   qwHY(mx,my,mz): Cloud Dropl. Concentration   [kg/kg] |
C |                   qrHY(mx,my,mz): Rain  Drops  Concentration   [kg/kg] |
C |   SEE DYNdgz:   uairDY(mx,my,mz): Wind  Speed  x-Direction       [m/s] |
C |                 vairDY(mx,my,mz): Wind  Speed  y-Direction       [m/s] |
C |                                                                        |
C |   METHOD:  2th order accurate Time       Scheme (leapfrog backw.) .AND.|
C |   ^^^^^^  (2th order accurate Horizontal Scheme on Arakawa A grid .OR. |
C |            4th order accurate Horizontal Scheme on Arakawa A grid     )|
C |            2th order          Vertical   Scheme                        |
C |                                                                        |
C |   CAUTION: This routine must be used                                   |
C |   ^^^^^^^  with a positive  definite restoring Procedure               |
C |            for    positive  definite Variables                         |
C |           (Such a Procedure is set up after digital filtering in MAR)  |
C |                                                                        |
C |   REFER.:  Use of  A grid: Purser   & Leslie,   1988, MWR 116, p.2069  |
C |   ^^^^^^   Time    Scheme: Haltiner & Williams, 1980, 5-2,     p.152   |
C |            Spatial Scheme: Haltiner & Williams, 1980, 5-6-5,   p.135   |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
 
      include 'MARgrd.inc'
 
      include 'MAR_DY.inc'
      include 'MAR_SL.inc'
      include 'MAR_HY.inc'
c #TC include 'MAR_TC.inc'
 
      integer  norder
 
 
C +--Local  Variables
C +  ================
 
      real         ff(mx,my,mzz)     ! Advected  Variable
      integer  ntSlow                ! Time      Step Counter Maximum Value
      integer  n
 
 
C +--DATA
C +  ====
 
               ntSlow = ntFast
 
 
C +--Advection of x-Momentum (uairDY)
C +  ================================
 
      qqmass = .FALSE.
 
!      DO   k=1,mz
!        DO j=1,my
!        DO i=1,mx
!            ff(i,j,k) =  uairDY(i,j,k)
!        END DO
!        END DO
!      END DO
!           k=  mzz
!        DO j=1,my
!        DO i=1,mx
!            ff(i,j,k) =  0.
!        END DO
!        END DO
 
!c #NV IF (no_vec)                                                  THEN
!C +                *************
!c #NV   call       DYNadv_LFB_2s(ntSlow,norder,ff)
!C +                *************
!c #NV ELSE
 
!C +                *************
!        call       DYNadv_LFB_2v(ntSlow,norder,ff)
!C +                *************
 
!c #NV END IF
 
!      DO   k=1,mz
!        DO j=1,my
!        DO i=1,mx
!          uairDY(i,j,k) =   ff(i,j,k)
!        END DO
!        END DO
!      END DO
 
 
!C +--Advection of y-Momentum (vairDY)
!C +  ================================
 
!      DO   k=1,mz
!        DO j=1,my
!        DO i=1,mx
!            ff(i,j,k) =  vairDY(i,j,k)
!        END DO
!        END DO
!      END DO
!           k=  mzz
!        DO j=1,my
!        DO i=1,mx
!            ff(i,j,k) =  0.
!        END DO
!        END DO
 
!c #NV IF (no_vec)                                                  THEN
 
!C +                *************
!c #NV   call       DYNadv_LFB_2s(ntSlow,norder,ff)
!C +                *************
 
!c #NV ELSE
 
!C +                *************
!        call       DYNadv_LFB_2v(ntSlow,norder,ff)
!C +                *************
 
!c #NV END IF
 
!      DO   k=1,mz
!        DO j=1,my
!        DO i=1,mx
!          vairDY(i,j,k) =   ff(i,j,k)
!        END DO
!        END DO
!      END DO
 
 
C +--Advection of Heat (pktaDY)
C +  ==========================
 
      qqmass = .FALSE.
      norder =  4
 
      DO   k=1,mz
        DO j=1,my
        DO i=1,mx
            ff(i,j,k) =  pktaDY(i,j,k)
        END DO
        END DO
      END DO
           k=  mzz
        DO j=1,my
        DO i=1,mx
            ff(i,j,k) =  pktaDY(i,j,k)
        END DO
        END DO
 
      IF (no_vec)                                                  THEN
         if(openmp) then
C +                ****************
          call     DYNadv_LFB_2p(ntSlow,norder,ff)
C +                ****************
         else
C +                *************
          call     DYNadv_LFB_2s   (ntSlow,norder,ff)
C +                *************
         endif
      ELSE
 
C +                *************
        call       DYNadv_LFB_2v(ntSlow,norder,ff)
C +                *************
 
      END IF
 
      DO   k=1,mz
        DO j=1,my
        DO i=1,mx
          pktaDY(i,j,k) =   ff(i,j,k)
        END DO
        END DO
      END DO
 
 
C +--Advection of Water Vapor (qvDY)
C +  ===============================
 
      qqmass = .TRUE.
 
      DO   k=1,mz
        DO j=1,my
        DO i=1,mx
            ff(i,j,k) =    qvDY(i,j,k)
        END DO
        END DO
      END DO
           k=  mzz
        DO j=1,my
        DO i=1,mx
            ff(i,j,k) =  qvapSL(i,j)
        END DO
        END DO
 
      IF (no_vec)                                                  THEN
 
         if(openmp) then
C +                ****************
          call     DYNadv_LFB_2p(ntSlow,norder,ff)
C +                ****************
         else
C +                *************
          call     DYNadv_LFB_2s   (ntSlow,norder,ff)
C +                *************
         endif
 
      ELSE
 
C +                *************
        call       DYNadv_LFB_2v(ntSlow,norder,ff)
C +                *************
 
      END IF
 
      DO   k=1,mz
        DO j=1,my
        DO i=1,mx
            qvDY(i,j,k) =   ff(i,j,k)
        END DO
        END DO
      END DO
 
 
C +--Advection of Ice Crystals Nb (ccniHY)
C +  =====================================
 
      IF (micphy)                                                 THEN
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
              ff(i,j,k) =  ccniHY(i,j,k)
          END DO
          END DO
        END DO
             k=  mzz
          DO j=1,my
          DO i=1,mx
              ff(i,j,k) =       0.
          END DO
          END DO
 
        IF (no_vec)                                               THEN
 
         if(openmp) then
C +                ****************
          call     DYNadv_LFB_2p(ntSlow,norder,ff)
C +                ****************
         else
C +                *************
          call     DYNadv_LFB_2s   (ntSlow,norder,ff)
C +                *************
         endif
 
        ELSE
 
C +                  *************
          call       DYNadv_LFB_2v(ntSlow,norder,ff)
C +                  *************
 
        END IF
 
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
            ccniHY(i,j,k) =   ff(i,j,k)
          END DO
          END DO
        END DO
 
 
C +--Advection of Cloud Crystals  (qiHY)
C +  ===================================
 
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
              ff(i,j,k) =    qiHY(i,j,k)
          END DO
          END DO
        END DO
             k=  mzz
          DO j=1,my
          DO i=1,mx
              ff(i,j,k) =       0.
          END DO
          END DO
 
        IF (no_vec)                                               THEN
 
         if(openmp) then
C +                ****************
          call     DYNadv_LFB_2p(ntSlow,norder,ff)
C +                ****************
         else
C +                *************
          call     DYNadv_LFB_2s   (ntSlow,norder,ff)
C +                *************
         endif
 
        ELSE
 
C +                  *************
          call       DYNadv_LFB_2v(ntSlow,norder,ff)
C +                  *************
 
        END IF
 
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
              qiHY(i,j,k) =   ff(i,j,k)
          END DO
          END DO
        END DO
 
 
C +--Advection of Snow  Flakes    (qsHY)
C +  ===================================
 
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
              ff(i,j,k) =    qsHY(i,j,k)
          END DO
          END DO
        END DO
             k=  mzz
          DO j=1,my
          DO i=1,mx
              ff(i,j,k) =  qsrfHY(i,j)
          END DO
          END DO
 
        IF (no_vec)                                               THEN
 
         if(openmp) then
C +                ****************
          call     DYNadv_LFB_2p(ntSlow,norder,ff)
C +                ****************
         else
C +                *************
          call     DYNadv_LFB_2s   (ntSlow,norder,ff)
C +                *************
         endif
 
        ELSE
 
C +                  *************
          call       DYNadv_LFB_2v(ntSlow,norder,ff)
C +                  *************
 
        END IF
 
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
              qsHY(i,j,k) =   ff(i,j,k)
          END DO
          END DO
        END DO
 
 
C +--Advection of Cloud Dropplets (qwHY)
C +  ===================================
 
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
              ff(i,j,k) =    qwHY(i,j,k)
          END DO
          END DO
        END DO
             k=  mzz
          DO j=1,my
          DO i=1,mx
              ff(i,j,k) =       0.
          END DO
          END DO
 
        IF (no_vec)                                               THEN
 
         if(openmp) then
C +                ****************
          call     DYNadv_LFB_2p(ntSlow,norder,ff)
C +                ****************
         else
C +                *************
          call     DYNadv_LFB_2s   (ntSlow,norder,ff)
C +                *************
         endif
 
        ELSE
 
C +                  *************
          call       DYNadv_LFB_2v(ntSlow,norder,ff)
C +                  *************
 
        END IF
 
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
              qwHY(i,j,k) =   ff(i,j,k)
          END DO
          END DO
        END DO
 
 
C +--Advection of Rain  Drops     (qrHY)
C +  ===================================
 
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
              ff(i,j,k) =    qrHY(i,j,k)
          END DO
          END DO
        END DO
             k=  mzz
          DO j=1,my
          DO i=1,mx
              ff(i,j,k) =       0.
          END DO
          END DO
 
        IF (no_vec)                                               THEN
 
         if(openmp) then
C +                ****************
          call     DYNadv_LFB_2p(ntSlow,norder,ff)
C +                ****************
         else
C +                *************
          call     DYNadv_LFB_2s   (ntSlow,norder,ff)
C +                *************
         endif
 
        ELSE
 
C +                  *************
          call       DYNadv_LFB_2v(ntSlow,norder,ff)
C +                  *************
 
        END IF
 
        DO   k=1,mz
          DO j=1,my
          DO i=1,mx
              qrHY(i,j,k) =   ff(i,j,k)
          END DO
          END DO
        END DO
 
      END IF
 
 
C +--Advection of Tracers         (qxTC)
C +  ===================================
 
c #TC DO     n=1,ntrac
c #TC   DO   k=1,mz
c #TC     DO j=1,my
c #TC     DO i=1,mx
c #TC         ff(i,j,k) =    qxTC(i,j,k,n)
c #TC     END DO
c #TC     END DO
c #TC   END DO
c #TC        k=  mzz
c #TC     DO j=1,my
c #TC     DO i=1,mx
c #TC         ff(i,j,k) =       0.
c #TC     END DO
c #TC     END DO
 
c #tc   IF (no_vec)                                               THEN
 
c #tc    if(openmp) then
C +                ****************
c #tc     call     DYNadv_LFB_2p(ntSlow,norder,ff)
C +                ****************
c #tc    else
C +                *************
c #tc     call     DYNadv_LFB_2s   (ntSlow,norder,ff)
C +                *************
c #tc    endif
 
c #tc   ELSE
 
C +                  *************
c #TC     call       DYNadv_LFB_2v(ntSlow,norder,ff)
C +                  *************
 
c #tc   END IF
 
c #TC   DO   k=1,mz
c #TC     DO j=1,my
c #TC     DO i=1,mx
c #TC         qxTC(i,j,k,n) =   ff(i,j,k)
c #TC     END DO
c #TC     END DO
c #TC   END DO
c #TC END DO
 
      return
      end
