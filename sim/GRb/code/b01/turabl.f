 
 
      subroutine TURabl
 
C +------------------------------------------------------------------------+
C | MAR TURBULENCE (ABL)                                   28-07-2006  MAR |
C |   SubRoutine TURabl includes the Contribution of Vertical Turbulence   |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |  INPUT (via common block)                                              |
C |  ^^^^^  micphy         : Cloud Microphysics Switch                     |
C |         dt_Loc         : Vertical Diffusion Time Step              [s] |
C |                                                                        |
C |         TUkvm(mx,my,mz): Vertical Turbulent Coeffic.(momentum) [m2/s]  |
C |         TUkvh(mx,my,mz): Vertical Turbulent Coeffic.(heat)     [m2/s]  |
C |         SLuus(mx,my)   : Friction Velocity                     [m/s]   |
C |         SLuts(mx,my)   : Surface Layer Heat     Turbulent Flux [mK/s]  |
C |         SLuqs(mx,my)   : Surface Layer Moisture Turbulent Flux [m/s]   |
C |        uss_HY(mx,my)   : Surface Layer Blowing* Turbulent Flux [m/s]   |
C |                                                                        |
C |  INPUT / OUTPUT: The Vertical Turbulent Fluxes are included for:       |
C |  ^^^^^^^^^^^^^^                                                        |
C |       1) The Horizontal     x-Wind Component uairDY(mx,my,mz)    [m/s] |
C |       2) The Horizontal     y-Wind Component vairDY(mx,my,mz)    [m/s] |
C |                                                                        |
C |       3) The Potential      Temperature      pktaDY(mx,my,mzz)         |
C |       4) The Air Specific   Humidity           qvDY(mx,my,mz)  [kg/kg] |
C |                                                                        |
C |       5) The Ice Crystals   Concentration      qiHY(mx,my,mz)  [kg/kg] |
C |       6) The Ice Crystals   Number           ccniHY(mx,my,mz)  [Nb/m3] |
C |       7) The Cloud Droplets Concentration      qwHY(mx,my,mz)  [kg/kg] |
C |       8) The Snow Flakes    Concentration      qsHY(mx,my,mz)  [kg/kg] |
C |       9) The Rain Drops     Concentration      qrHY(mx,my,mz)  [kg/kg] |
C |                                                                        |
C |      10) The Tracer         Concentration      qxTC(mx,my,mz,ntrac)    |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_DY.inc'
c #NH include 'MAR_NH.inc'
c #Di include 'MAR_DI.inc'
 
      include 'MAR_TU.inc'
c _PE include 'MARpen.inc'
c #PE include 'MARpen.inc'
 
      include 'MAR_HY.inc'
c #TC include 'MAR_TC.inc'
c #EW include 'MAR_EW.inc'
 
      include 'MAR_SL.inc'
 
      include 'MAR_WK.inc'
 
      logical                Q_Impl
      common  /TURabl_lo/    Q_Impl
 
      integer                lous  ,lotu
      common  /TURabl_IN/    lous  ,lotu
 
      real                   alpha ,beta  ,ab
      common  /TURabl_re/    alpha ,beta  ,ab
 
C +--OUTPUT of Snow Erosion Statistics (see assignation in PHY_SISVAT)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #EV integer                iEVwri,jEVwri,nEVwri,kEVwri,lEVwri
c #EV common  /SISVAT_EV/    iEVwri,jEVwri,nEVwri,kEVwri,lEVwri
 
 
C +--Local  Variables
C +  ================
 
      integer  i1_tua,i2_tua,j1_tua,j2_tua,k1_tua,k2_tua,n,km,kp
      real     uustar       ,ssvu(mx,my),ssvv(mx,my)
      real     utstar       ,uqstar     ,qvap
      real     waterb       ,ussno      ,dd_sno
c #CL real     h0
 
 
C +--Parameters
C +  ==========
 
      IF (iterun.EQ.0)                                              THEN
 
 
C +--Parameters for the Inclusion of the Friction Velocity u*
C +  --------------------------------------------------------
 
        lous =1
c #EK   lous =0
        lous =1
C +...  lous =1 : SLuus    is          used
C +     lous =0 : SLuus**2 is (partly) replaced by  K du / dz
C +     CAUTION : DO NOT USE lous =0 EXCEPT WHEN VERIFYING THE EKMAN SPIRAL
 
 
C +--Parameters for the Numerical Scheme of Vertical Turbulent Transport
C +  -------------------------------------------------------------------
 
        Q_Impl=.FALSE.
        Q_Impl=.TRUE.
        Q_Impl=.FALSE.
c #TC   IF     (Q_Impl)
c #TC.    stop       ' #~�@�! BAD Vertical Diffusion of gaseous tracers'
 
      END IF
 
        alpha = 0.25               !
        beta  = 1.00-alpha         ! Impliciteness
        ab    = alpha/beta         !
 
 
C +-------------------------------------------------------------------------
 
 
C +--INITIALIZATION
C +  ==============
 
      IF (itexpe.EQ.0)                                              THEN
        DO  j=1,my
        DO  i=1,mx
           ssvSL(i,j,mz)=  max(sqrt(uairDY(i,j,mz)*uairDY(i,j,mz)
     .                             +vairDY(i,j,mz)*vairDY(i,j,mz)),epsi)
        DO  n=1,mw
           cdmSL(i,j,n) =  0.04
           cdhSL(i,j,n) =  0.04
          SLuusl(i,j,n) = cdmSL(i,j,n) *ssvSL(i,j,mz)
        ENDDO
        ENDDO
        ENDDO
 
        DO  j=1,my
        DO  i=1,mx
          duusSL(i,j)   = 0.
          dutsSL(i,j)   = 0.
        ENDDO
        ENDDO
      END IF
 
 
C +-------------------------------------------------------------------------
 
 
C +--Vertical Diffusion of Horizontal Momentum
C +  =========================================
 
C +--Implicit Surface Scheme
C +  -----------------------
 
        DO j = jp11,my1
        DO i = ip11,mx1
            Kv__SL(i,j) = 0.
            aeCdSL(i,j) = 0.
        END DO
        END DO
 
        DO  n=1,mw
        DO  j=jp11,my1
        DO  i=ip11,mx1
            aeCdSL(i,j) = aeCdSL(i,j) +cdmSL(i,j,n)*SLuusl(i,j,n) ! aerodynamic
     .                                             *Slsrfl(i,j,n) ! conductance
        ENDDO
        ENDDO
        ENDDO
 
 
c #EK   DO  j=jp11,my1
c #EK   DO  i=ip11,mx1
c #EK       Kv__SL(i,j) =                                         ! Kv Contrib.
c #EK.     -gravi2*romiDY(i,j,mz) *TUkvm(i,j,mz)          *beta   ! Ekman Spir.
c #EK.            *rolvDY(i,j,mz)/(pstDY2(i,j)* dsigm1(mz)*dsig_1(  mz))
C +                                TUkvm(i,j,mz) accounted only
C +                                        in Ekman Spiral Test
C +                                       (i.e.,  when lotu = 1)
c #EK   ENDDO
c #EK   ENDDO
 
 
        DO  j=jp11,my1
        DO  i=ip11,mx1
            Kv__SL(i,j) =                                         ! Kv Contrib.
     .     -gravit                *aeCdSL(i,j)            *beta   ! in      SL
     .            *rolvDY(i,j,mz)/(pstDY (i,j)* dsigm1(mz))
        ENDDO
        ENDDO
 
 
C +--Tridiagonal Matrix Coefficients : u and v
C +  -----------------------------------------
 
        DO  j=jp11,my1
        DO  i=ip11,mx1
          ssvu(i,j) = uairDY(i,j,mz) / ssvSL(i,j,mz)
          ssvv(i,j) = vairDY(i,j,mz) / ssvSL(i,j,mz)
        END DO
        END DO
 
C +--Diagonal A
C +  ~~~~~~~~~~
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz1(i,j,mz)=             Kv__SL(i,j)
        END DO
        END DO
 
      DO  k=mmz1,1,-1
         kp=kp1(k)
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz1(i,j,k)=-gravi2*beta*romiDY(i,j,k)*(TUkvm(i,j,k)
c _HH.                                                +pente3(i,j,k)
     .              )*rolvDY(i,j,k)/(pstDY2(i,j)*dsigm1(k)*dsig_1(k))
c #DF     WKxyz1(i,j,k)=-gravi2*beta*romiDY(i,j,k)*(TUkvm(i,j,k)
c #PE.                                                +pente1(i,j,k)
c #DF.              )*rolvDY(i,j,k)/(pstDY2(i,j)*dsigm1(k)*dsig_1(k))
        END DO
        END DO
 
C +--Diagonal C
C +  ~~~~~~~~~~
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz3(i,j,kp)=WKxyz1(i,j,k)   *dsigm1(k)     /dsigm1(kp)
     .                                  *(rolvDY(i,j,kp)/rolvDY(i,j,k))
        END DO
        END DO
 
      END DO
 
C +--A, B, C
C +  ~~~~~~~
      DO  k=1,mmz1
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz1(i,j,k)  =       WKxyz1(i,j,k) * dt_Loc
          WKxyz3(i,j,k)  =       WKxyz3(i,j,k) * dt_Loc
          WKxyz2(i,j,k)  = 1.0 - WKxyz3(i,j,k) - WKxyz1(i,j,k)
        END DO
        END DO
      END DO
 
C +--Vertical B.C.
C +  ~~~~~~~~~~~~~
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz3(i,j, 1) = 0.0
          WKxyz2(i,j, 1) = 1.0 - WKxyz1(i,j,1)
 
          WKxyz1(i,j,mz) =       WKxyz1(i,j,mz) *dt_Loc
          WKxyz3(i,j,mz) =       WKxyz3(i,j,mz) *dt_Loc
          WKxyz2(i,j,mz) = 1.0 - WKxyz3(i,j,mz) -WKxyz1(i,j,mz)
        END DO
        END DO
 
 
C +--Second Member of the Tridiagonal System - u
C +  -------------------------------------------
 
           kp=kp1(1)
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz4(i,j,1) = WKxyz1(i,j,1)
     .               *ab*(uairDY(i,j,1)-uairDY(i,j,kp))
c #Di     WKxyz1(i,j,1) = 0.0
c #Di     WKxyz2(i,j,1) = 1.0
c #Di     WKxyz4(i,j,1) = uairDI(i,j)
        END DO
        END DO
 
      DO    k=kp1(1),mmz1
           kp=kp1(k)
           km=km1(k)
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz4(i,j,k) =
     .    WKxyz1(i,j,k) *ab*(uairDY(i,j,k )-uairDY(i,j,kp))
     .   -WKxyz3(i,j,k) *ab*(uairDY(i,j,km)-uairDY(i,j,k ))
        END DO
        END DO
      END DO
 
        DO  j=jp11,my1
        DO  i=ip11,mx1
 
          uustar         =     SLuus(i,j)*SLuus(i,j)
 
C +--Implicit Surface Scheme
C +  ~~~~~~~~~~~~~~~~~~~~~~~
          uustar        = 0.1*duusSL(i,j)                         ! explicit
 
          WKxyz4(i,j,mz)=
     .    WKxyz1(i,j,mz)*ab* uairDY(i,j,mz)
     .   -WKxyz3(i,j,mz)*ab*(uairDY(i,j,mmz1)-uairDY(i,j,mz))
     .    -lous*alpha * gravit * romiDY(i,j,mz) *dt_Loc
     .    *uustar              *   ssvu(i,j)    /(pstDY(i,j)*dsigm1(mz))
        END DO
        END DO
 
 
C +--Tridiagonal Matrix Inversion - u
C +  --------------------------------
 
              k1_tua= 1
c #Di         k1_tua= 2
      DO    k=k1_tua,mz
        DO  j=     jp11,my1
        DO  i=     ip11,mx1
          WKxyz4(i,j,k)  = WKxyz4(i,j,k) + uairDY(i,j,k)
        END DO
        END DO
      END DO
 
      k1_tua = 1
      k2_tua = mz
 
C +        ************
      call MARgz_2mx1y1(k1_tua,k2_tua)
C +        ************
 
      DO    k=1,mz
        DO  j=jp11,my1
        DO  i=ip11,mx1
          uairDY(i,j,k) = WKxyz7(i,j,k)
        END DO
        END DO
      END DO
 
 
C +--Tridiagonal Matrix Coefficients - v
C +  (RELOAD if horiz.correct. included)
C +  -----------------------------------
 
C +--Diagonal A
C +  ~~~~~~~~~~
c #DF DO    k=mz,1,-1
c #DF       kp=kp1(k)
C +
c #DF   DO  j=jp11,my1
c #DF   DO  i=ip11,mx1
c #DF     WKxyz1(i,j,k)=-gravi2*beta*romiDY(i,j,k)*(TUkvm(i,j,k)
c _HH.                                            +pente3(i,j,k)
c #PE.                                            +pente2(i,j,k)
c #DF.          )*rolvDY(i,j,k)/(pstDY2(i,j)*dsigm1(k)*dsig_1(k))
c #DF   END DO
c #DF   END DO
 
C +--Diagonal C
C +  ~~~~~~~~~~
c #DF   DO  j=jp11,my1
c #DF   DO  i=ip11,mx1
c #DF     WKxyz3(i,j,kp)=WKxyz1(i,j,k)  *dsigm1(k)  /dsigm1(kp)
c #DF.                            /rolvDY(i,j,k)*rolvDY(i,j,kp)
c #DF   END DO
c #DF   END DO
C +
c #DF END DO
 
C +--A, B, C
C +  ~~~~~~~
c #DF DO       k=1,mmz1
c #DF   DO  j=jp11,my1
c #DF   DO  i=ip11,mx1
c #DF     WKxyz1(i,j,k)  =       WKxyz1(i,j,k) * dt_Loc
c #DF     WKxyz3(i,j,k)  =       WKxyz3(i,j,k) * dt_Loc
c #DF     WKxyz2(i,j,k)  = 1.0 - WKxyz3(i,j,k) - WKxyz1(i,j,k)
c #DF   END DO
c #DF   END DO
c #DF END DO
 
C +--Vertical B.C.
C +  ~~~~~~~~~~~~~
c #DF   DO  j=jp11,my1
c #DF   DO  i=ip11,mx1
c #DF     WKxyz3(i,j, 1) = 0.0
c #DF     WKxyz2(i,j, 1) = 1.0 - WKxyz1(i,j,1)
c #DF     WKxyz1(i,j,mz) =       WKxyz1(i,j,mz) * dt_Loc
c #DF     WKxyz3(i,j,mz) =       WKxyz3(i,j,mz) * dt_Loc
c #DF     WKxyz2(i,j,mz) = 1.0 - WKxyz3(i,j,mz) - WKxyz1(i,j,mz)
c #DF.    +lous*beta*gravit*dt_Loc*romiDY(i,j,mz)
c #DF.      *SLuus(i,j)*SLuus(i,j)/(ssvSL(i,j,mz)*pstDY(i,j)*dsigm1(mz))
c #DF   END DO
c #DF   END DO
 
 
C +--Second Member of the Tridiagonal System - v
C +  -------------------------------------------
 
            kp=kp1(1)
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz4(i,j,1) = WKxyz1(i,j,1)
     .               *ab*(vairDY(i,j,1)-vairDY(i,j,kp))
c #Di     WKxyz1(i,j,1) = 0.0
c #Di     WKxyz2(i,j,1) = 1.0
c #Di     WKxyz4(i,j,1) = vairDI(i,j)
        END DO
        END DO
 
      DO    k=kp1(1),mmz1
           km=km1(k)
           kp=kp1(k)
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz4(i,j,k) =
     .    WKxyz1(i,j,k) *ab*(vairDY(i,j,k )-vairDY(i,j,kp))
     .   -WKxyz3(i,j,k) *ab*(vairDY(i,j,km)-vairDY(i,j,k ))
        END DO
        END DO
      END DO
 
        DO  j=jp11,my1
        DO  i=ip11,mx1
 
          uustar         =     SLuus(i,j)*SLuus(i,j)
 
C +--Implicit Surface Scheme
C +  ~~~~~~~~~~~~~~~~~~~~~~~
          uustar        = 0.1*duusSL(i,j)                         ! explicit
          duusSL(i,j)   = 0.9*duusSL(i,j)                         !
 
          WKxyz4(i,j,mz)=
     .    WKxyz1(i,j,mz)*ab* vairDY(i,j,mz)
     .   -WKxyz3(i,j,mz)*ab*(vairDY(i,j,mmz1)-vairDY(i,j,mz))
     .    -lous*alpha * gravit * romiDY(i,j,mz) *dt_Loc
     .    *uustar              *   ssvv(i,j)    /(pstDY(i,j)*dsigm1(mz))
        END DO
        END DO
 
 
C +--Tridiagonal Matrix Inversion - v
C +  --------------------------------
 
              k1_tua= 1
c #Di         k1_tua= 2
      DO    k=k1_tua,mz
        DO  j=     jp11,my1
        DO  i=     ip11,mx1
          WKxyz4(i,j,k)  = WKxyz4(i,j,k) + vairDY(i,j,k)
        END DO
        END DO
      END DO
 
      k1_tua = 1
      k2_tua = mz
 
C +        ************
      call MARgz_2mx1y1(k1_tua,k2_tua)
C +        ************
 
        DO  j=jp11,my1
        DO  i=ip11,mx1
          uustar=aeCdSL(i,j)*((alpha* WKxy1(i,j)   +beta*uairDY(i,j,mz))
     .                       *         ssvu(i,j)
     .                       +(alpha*vairDY(i,j,mz)+beta*WKxyz7(i,j,mz))
     .                       *         ssvv(i,j)                       )
          duusSL(i,j)   = duusSL(i,j) + SLuus(i,j)*SLuus(i,j) - uustar
        END DO
        END DO
 
      DO    k=1,mz
        DO  j=jp11,my1
        DO  i=ip11,mx1
          vairDY(i,j,k) = WKxyz7(i,j,k)
        END DO
        END DO
      END DO
 
 
C +-------------------------------------------------------------------------
 
 
C +--Vertical Diffusion of Heat and Water Vapor
C +  ==========================================
 
C +--Implicit Surface Scheme
C +  -----------------------
 
        DO  j=jp11,my1
        DO  i=ip11,mx1
            Kv__SL(i,j) = 0.
            aeCdSL(i,j) = 0.
        ENDDO
        ENDDO
 
        DO  n=1,mw
        DO  j=jp11,my1
        DO  i=ip11,mx1
            aeCdSL(i,j) = aeCdSL(i,j) +cdhSL(i,j,n)*SLuusl(i,j,n) ! aerodynamic
     .                                             *Slsrfl(i,j,n) ! conductance
        END DO
        END DO
        END DO
 
        DO  j=jp11,my1
        DO  i=ip11,mx1
C +         Kv__SL(i,j) =                                         ! Kv Contrib.
C +  .     -gravi2*romiDY(i,j,mz) * TUkvh(i,j,mz)         *beta   ! above   SL
C +  .            *rolvDY(i,j,mz)/(pstDY2(i,j)* dsigm1(mz)*dsig_1(mz))
 
            Kv__SL(i,j) =                                         ! Kv Contrib.
     .     -gravit                *aeCdSL(i,j)            *beta   ! in      SL
     .            *rolvDY(i,j,mz)/(pstDY (i,j)* dsigm1(mz))       !
        END DO
        END DO
 
 
C +--Tridiag. Matrix Coeff. : pktaDY,   qvDY
C +  ---------------------------------------
 
C +--Diagonal A
C +  ~~~~~~~~~~
            k=  mz
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz1(i,j,k)=                            Kv__SL(i,j)
c #qe.                  -gravi2*beta*romiDY(i,j,k)*(  zero
c _HH.                                             +pente3(i,j,k)
c #PE.                                             +pente3(i,j,k)
c #qe.           )*rolvDY(i,j,k)/(pstDY2(i,j)*dsigm1(k)*dsig_1(k))
        END DO
        END DO
      DO    k=mmz1,1,-1
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz1(i,j,k)=-gravi2*beta*romiDY(i,j,k)*( TUkvh(i,j,k)
c _HH.                                             +pente3(i,j,k)
c #PE.                                             +pente3(i,j,k)
     .           )*rolvDY(i,j,k)/(pstDY2(i,j)*dsigm1(k)*dsig_1(k))
        END DO
        END DO
      END DO
 
C +--Diagonal C
C +  ~~~~~~~~~~
      DO    k=mz  ,1,-1
           kp=kp1(k)
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz3(i,j,kp)=WKxyz1(i,j,k)  *dsigm1(k)    /dsigm1(kp)
     .                                  /rolvDY(i,j,k)*rolvDY(i,j,kp)
        END DO
        END DO
      END DO
 
C +--A, B, C
C +  ~~~~~~~
      DO    k=1,mz
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz1(i,j,k)  =       WKxyz1(i,j,k) * dt_Loc
          WKxyz3(i,j,k)  =       WKxyz3(i,j,k) * dt_Loc
          WKxyz2(i,j,k)  = 1.0 - WKxyz3(i,j,k) - WKxyz1(i,j,k)
        END DO
        END DO
      END DO
 
C +--Vertical B.C.
C +  ~~~~~~~~~~~~~
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz3(i,j, 1) = 0.0
          WKxyz2(i,j, 1) = 1.0 - WKxyz1(i,j,1)
        END DO
        END DO
 
 
C +-------------------------------------------------------------------------
 
 
C +--Vertical Diffusion of Heat
C +  ==========================
 
c #CL   DO  j=jp11,my1
c #CL   DO  i=ip11,mx1
 
C +--Set-Up of the Convective Mixed Layer Test
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #CL     h0      =  100.0
C +...           i.e.100 W/m2 upward
c #CL     SLuts (i,j)   =-h0 /cp /rolvDY(i,j,mz) /1000.
c #CL     SLutsl(i,j,1) =-h0 /cp /rolvDY(i,j,mz) /1000.
C +...    In Order to Test the Convective Mixed Layer
 
c #CL   END DO
c #CL   END DO
 
 
C +--SBC           of the Tridiagonal System - pktaDY
C +  ------------------------------------------------
 
        DO  j=jp11,my1
        DO  i=ip11,mx1
          utstar        =  SLuts(i,j)
 
C +--Implicit Surface Scheme
C +  ~~~~~~~~~~~~~~~~~~~~~~~
          utstar        = 0.1*dutsSL(i,j)                         ! explicit
          dutsSL(i,j)   = 0.9*dutsSL(i,j)                         !
                                                                  ! set := 0
          WKxyz4(i,j,mz)=     WKxyz1(i,j,mz)                      ! partly
     .                  *(ab *pktaDY(i,j,mz)  -pktaSL(i,j)/beta)  ! explicit
     .   -WKxyz3(i,j,mz)* ab*(pktaDY(i,j,mmz1)-pktaDY(i,j,mz))    !
     .                          -gravit*dt_Loc*rolvDY(i,j,mz)     ! u*T* all
     .                  * utstar/(pcap* pstDY(i,j)*dsigm1(mz))    ! explicit
        END DO
        END DO
 
 
C +--Second Member of the Tridiagonal System - pktaDY
C +  ------------------------------------------------
 
      DO    k=kp1(1),mmz1
           km=km1(k)
           kp=kp1(k)
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz4(i,j,k) =
     .    WKxyz1(i,j,k) *ab*(pktaDY(i,j,k )-pktaDY(i,j,kp))
     .   -WKxyz3(i,j,k) *ab*(pktaDY(i,j,km)-pktaDY(i,j,k ))
        END DO
        END DO
      END DO
 
 
C +--UBC           of the Tridiagonal System - pktaDY
C +  ------------------------------------------------
 
           kp=kp1(1)
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz4(i,j,1) =
     .    WKxyz1(i,j,1) *ab*(pktaDY(i,j,1)-pktaDY(i,j,kp))
c #Di     WKxyz1(i,j,1) = 0.0
c #Di     WKxyz2(i,j,1) = 1.0
c #Di     WKxyz4(i,j,1) = pkttDI(i,j)
        END DO
        END DO
 
 
C +--Tridiagonal Matrix Inversion - pktaDY
C +  -------------------------------------
 
              k1_tua= 1
c #Di         k1_tua= 2
      DO    k=k1_tua,mz
        DO  j=     jp11,my1
        DO  i=     ip11,mx1
          WKxyz4(i,j,k)    = WKxyz4(i,j,k) + pktaDY(i,j,k)
        END DO
        END DO
      END DO
 
      k1_tua = 1
      k2_tua = mz
 
C +        ************
      call MARgz_2mx1y1(k1_tua,k2_tua)
C +        ************
 
        DO  j=jp11,my1
        DO  i=ip11,mx1
          utstar=aeCdSL(i,j)*(alpha*pktaDY(i,j,mz)+beta*WKxyz7(i,j,mz)
     .                       -      pktaDY(i,j,mzz)                   )
     .                      * pcap
          dutsSL(i,j)   = dutsSL(i,j) + SLuts(i,j) - utstar
        END DO
        END DO
 
      DO    k=1,mz
        DO  j=jp11,my1
        DO  i=ip11,mx1
          pktaDY(i,j,k) = WKxyz7(i,j,k)
        END DO
        END DO
      END DO
 
 
C +-------------------------------------------------------------------------
 
 
C +--Vertical Diffusion of Moisture
C +  ==============================
 
C +--Tridiag. Matrix Coeff. : qvDY
C +  -----------------------------
 
C +--Diagonal A
C +  ~~~~~~~~~~
            k=  mz
        DO  j=jp11,my1
        DO  i=ip11,mx1
          Kv__SL(i,j)  = 0.
          WKxyz1(i,j,k)=                            Kv__SL(i,j)
c #qe.                  -gravi2*beta*romiDY(i,j,k)*(  zero
c _HH.                                             +pente3(i,j,k)
c #PE.                                             +pente3(i,j,k)
c #qe.           )*rolvDY(i,j,k)/(pstDY2(i,j)*dsigm1(k)*dsig_1(k))
c #qe     WKxyz1(i,j,k)= WKxyz1(i,j,k)* dt_Loc
 
C +--Diagonal B
C +  ~~~~~~~~~~
          WKxyz2(i,j,k)  = 1.00 - WKxyz3(i,j,k)
c #qe.                          - WKxyz1(i,j,k)
        END DO
        END DO
 
 
C +--UBC           of the Tridiagonal System - qvDY
C +  ----------------------------------------------
 
           kp=kp1(1)
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz4(i,j,1)=
     .    WKxyz1(i,j,1)*ab*(qvDY(i,j,1)-qvDY(i,j,kp))
C +...    Upper BC: zero gradient                 (Condition von Neuman)
C +
c #V+     WKxyz1(i,j,1)= 1.0
c #V+     WKxyz2(i,j,1)=-1.0
c #V+     WKxyz4(i,j,1)=-(gplvDY(i,j,1)-gplvDY(i,j,2))*qvtoDI(i,j)
c #V+.                   *grvinv
C +...    c #V+ pour un gradient constant non nul (Condition von Neuman)
 
c #Di     WKxyz1(i,j,1)= 0.0
c #Di     WKxyz2(i,j,1)= 1.0
c #Di     WKxyz4(i,j,1)= qvtoDI(i,j)
        END DO
        END DO
 
 
C +--Second Member of the Tridiagonal System - qvDY
C +  ----------------------------------------------
 
      DO    k=kp1(1),mmz1
           kp=kp1(k)
           km=km1(k)
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz4(i,j,k)= WKxyz1(i,j,k)*ab*(qvDY(i,j,k )-qvDY(i,j,kp))
     .                  -WKxyz3(i,j,k)*ab*(qvDY(i,j,km)-qvDY(i,j,k ))
        END DO
        END DO
      END DO
 
 
C +--SBC           of the Tridiagonal System - qvDY
C +  ----------------------------------------------
 
        DO  j=jp11,my1
        DO  i=ip11,mx1
 
C +--Implicit Surface Scheme
C +  ~~~~~~~~~~~~~~~~~~~~~~~
            qvap       =   qvDY(i,j,mz)
     .                 -  SLuqs(i,j)/aeCdSL(i,j)
 
            uqstar     = 0.                                       ! explicit
C +         uqstar    is replaced by aeCdSL*(  qvDY-qvapSL)       ! set := 0
 
C +--Explicit Surface Scheme
C +  ~~~~~~~~~~~~~~~~~~~~~~~
            qvap       = qvapSL(i,j)
            uqstar     =  SLuqs(i,j)
 
          WKxyz4(i,j,mz)=     WKxyz1(i,j,mz)                      ! partly
     .                  *(ab *  qvDY(i,j,mz)  -  qvap / beta)     ! explicit
     .   -WKxyz3(i,j,mz)* ab*(  qvDY(i,j,mmz1)-  qvDY(i,j,mz))
     .                          -gravit*dt_Loc*rolvDY(i,j,mz)     ! u*q* all
     .                  * uqstar /(     pstDY(i,j)*dsigm1(mz))    ! explicit
        END DO
        END DO
 
 
C +--Tridiagonal Matrix Inversion - qvDY
C +  -----------------------------------
 
              k1_tua =1
c #Di         k1_tua =2
      DO    k=k1_tua,mz
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz4(i,j,k)  = WKxyz4(i,j,k) + qvDY(i,j,k)
        END DO
        END DO
      END DO
 
      k1_tua = 1
      k2_tua = mz
 
C +        ************
      call MARgz_2mx1y1(k1_tua,k2_tua)
C +        ************
 
      DO    k=1,mz
        DO  j=jp11,my1
        DO  i=ip11,mx1
          qvDY(i,j,k) = WKxyz7(i,j,k)
        END DO
        END DO
      END DO
 
 
C +-------------------------------------------------------------------------
 
 
C +--Vertical Diffusion of gazeous Tracers
C +  =====================================
 
c #TC IF (dt_ODE.eq.dtDiff)                                         THEN
 
 
C +--Second Member of the Tridiagonal System - qxTC
C +  ----------------------------------------------
 
c #TC   DO     n=nterr+1,ntrac            ! CAUTION: defines nterr as the Nb
                                          !          of terregenous aerosols
                                          !          (usually 0 .OR. 1)
c #TC       DO j=jp11,my1
c #TC       DO i=ip11,mx1
c #TC         WKxyz1(i,j,1)= 0.0
c #TC         WKxyz2(i,j,1)= 1.0
c #TC         WKxyz4(i,j,1)= qxTC(i,j,1,n)
c #TC       END DO
c #TC       END DO
 
c #TC     DO   k=kp1(1),mmz1
c #TC         kp=kp1(k)
c #TC         km=km1(k)
c #TC       DO j=jp11,my1
c #TC       DO i=ip11,mx1
c #TC         WKxyz4(i,j,k) =
c #TC.        WKxyz1(i,j,k)*ab*(qxTC(i,j,k ,n)-qxTC(i,j,kp,n))
c #TC.       -WKxyz3(i,j,k)*ab*(qxTC(i,j,km,n)-qxTC(i,j,k ,n))
c #TC       END DO
c #TC       END DO
c #TC     END DO
 
c #TC     IF (.NOT.BloMod)                                          THEN
c #TC       DO j=jp11,my1
c #TC       DO i=ip11,mx1
c #TC         uqTC(i,j,n)=-cdhSL(i,j,1)* SLuusl(i,j,1)
c #TC.                                 *  (qsTC(i,j,n)- qxTC(i,j,mz,n))
c #TC       END DO
c #TC       END DO
c #TC     END IF
 
c #TC       DO j=jp11,my1
c #TC       DO i=ip11,mx1
c #TC         WKxyz4(i,j,mz)=  WKxyz1(i,j,mz)
c #TC.                     *(ab* qxTC(i,j,mz  ,n)-qsTC(i,j,n)/beta)
c #TC.      - WKxyz3(i,j,mz)*ab*(qxTC(i,j,mmz1,n)-qxTC(i,j,mz,n))
c #TC.            +(gravit*dt_Loc
c #TC.              *rolvDY(i,j,mz)/(pstDY(i,j)*(sigmid(mz)-1.0d+0)))
c #TC.                *uqTC(i,j,n)
c #TC       END DO
c #TC       END DO
 
 
C +--Tridiagonal Matrix Inversion - qxTC
C +  -----------------------------------
 
c #TC     DO   k=kp1(1),mz
c #TC       DO j=jp11,my1
c #TC       DO i=ip11,mx1
c #TC         WKxyz4(i,j,k) = WKxyz4(i,j,k) + qxTC(i,j,k,n)
c #TC       END DO
c #TC       END DO
c #TC     END DO
 
c #TC     k1_tua = 1
c #TC     k2_tua = mz
 
C +            ************
c #TC     call MARgz_2mx1y1(k1_tua,k2_tua)
C +            ************
 
c #TC     DO   k=1,mz
c #TC       DO j=jp11,my1
c #TC       DO i=ip11,mx1
c #TC        qxTC(i,j,k,n) = WKxyz7(i,j,k)
c #TC       END DO
c #TC       END DO
c #TC     END DO
 
c #TC   END DO
 
c #TC END IF
 
 
C +-------------------------------------------------------------------------
 
 
C +--Atmospheric Water Budget
C +  ========================
 
c #EW   DO   j=jp11,my1
c #EW   DO   i=ip11,mx1
c #EW       wat0EW(i,j) = 0.0
c #EW     DO k=1,mz
c #EW       wat0EW(i,j) = wat0EW(i,j)
c #EW.                  +  (qvDY(i,j,k)
c #EW.                  +   qwHY(i,j,k) + qrHY(i,j,k)
c #EW.                  +   qiHY(i,j,k) + qsHY(i,j,k)) *dsigm1(k)
c #EW     END DO
c #EW       wat0EW(i,j) = wat0EW(i,j)  * pstDY(i,j)    *grvinv
c #EW   END DO
c #EW   END DO
 
 
C +-------------------------------------------------------------------------
 
 
C +--Vertical Diffusion of Hydrometeors and non gazeous Tracers
C +  ==========================================================
 
C +--Parameters for the Numerical Scheme of Vertical Turbulent Transport
C +  -------------------------------------------------------------------
 
      alpha = 0.00               ! Expliciteness = 0 (positive definite)
      beta  = 1.00-alpha         ! Impliciteness
      ab    = alpha/beta         !
 
 
C +--Tridiagonal Matrix Coefficients:  qiHY, ccniHY, qwHY, qrHY
C +  (Turbulent Diffusion Coefficient X 3:  Bintanja, 2000, BLM) --+
C +  -----------------------------------------------------------   V
 
C +--Diagonal A
C +  ~~~~~~~~~~
      DO    k=mz,1,-1
           kp=kp1(k)
 
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz8(i,j,k)=                            TUkvh(i,j,k)*r_Turb
c _HH.                                            +pente3(i,j,k)
c #PE.                                            +pente3(i,j,k)
        END DO
        END DO
 
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz1(i,j,k)=-gravi2*beta*romiDY(i,j,k)*WKxyz8(i,j,k)
     .           *rolvDY(i,j,k)/(pstDY2(i,j)*dsigm1(k)*dsig_1(k))
        END DO
        END DO
 
C +--Diagonal C
C +  ~~~~~~~~~~
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz3(i,j,kp)=WKxyz1(i,j,k)  *dsigm1(k)    /dsigm1(kp)
     .                                  /rolvDY(i,j,k)*rolvDY(i,j,kp)
        END DO
        END DO
 
      END DO
 
C +--A, B, C
C +  ~~~~~~~
      DO    k=1,mz
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz1(i,j,k)  =       WKxyz1(i,j,k) * dt_Loc
          WKxyz3(i,j,k)  =       WKxyz3(i,j,k) * dt_Loc
          WKxyz2(i,j,k)  = 1.0 - WKxyz3(i,j,k) - WKxyz1(i,j,k)
        END DO
        END DO
      END DO
 
C +--Vertical B.C.
C +  ~~~~~~~~~~~~~
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz3(i,j, 1) = 0.0
          WKxyz2(i,j, 1) = 1.0 - WKxyz1(i,j,1)
        END DO
        END DO
 
 
C +-------------------------------------------------------------------------
 
 
C +--BEGIN Cloud Microphysics (qiHY, ccniHY, qwHY, qrHY)
C +  ===================================================
 
      IF (micphy)                                                   THEN
 
 
C +--Vertical Diffusion of Ice Crystals
C +  ==================================
 
 
C +--Second Member of the Tridiagonal System - qiHY
C +  ----------------------------------------------
 
          DO  j=jp11,my1
          DO  i=ip11,mx1
            WKxyz1(i,j,1)= 0.0
            WKxyz2(i,j,1)= 1.0
            WKxyz4(i,j,1)= qiHY(i,j,1)
          END DO
          END DO
 
        DO    k=kp1(1),mmz1
             kp=kp1(k)
             km=km1(k)
          DO  j=jp11,my1
          DO  i=ip11,mx1
            WKxyz4(i,j,k) =
     .      WKxyz1(i,j,k)*ab*(qiHY(i,j,k )-qiHY(i,j,kp))
     .     -WKxyz3(i,j,k)*ab*(qiHY(i,j,km)-qiHY(i,j,k ))
          END DO
          END DO
        END DO
 
          DO  j=jp11,my1
          DO  i=ip11,mx1
            WKxyz4(i,j,mz)=
     .      WKxyz1(i,j,mz)*ab*(qiHY(i,j,mz)  -zero        )
     .    - WKxyz3(i,j,mz)*ab*(qiHY(i,j,mmz1)-qiHY(i,j,mz))
          END DO
          END DO
 
 
C +--Tridiagonal Matrix Inversion - qiHY
C +  -----------------------------------
 
        DO    k=kp1(1),mz
          DO  j=jp11,my1
          DO  i=ip11,mx1
            WKxyz4(i,j,k) = WKxyz4(i,j,k) + qiHY(i,j,k)
          END DO
          END DO
        END DO
 
        k1_tua = 1
        k2_tua = mz
 
C +          ************
        call MARgz_2mx1y1(k1_tua,k2_tua)
C +          ************
 
        DO    k=1,mz
          DO  j=jp11,my1
          DO  i=ip11,mx1
            qiHY(i,j,k) = WKxyz7(i,j,k)
          END DO
          END DO
        END DO
 
 
C +--Second Member of the Tridiagonal System - ccniHY
C +  ------------------------------------------------
 
          DO  j=jp11,my1
          DO  i=ip11,mx1
            WKxyz1(i,j,1)= 0.0
            WKxyz2(i,j,1)= 1.0
            WKxyz4(i,j,1)= ccniHY(i,j,1)
          END DO
          END DO
 
        DO    k=kp1(1),mmz1
             kp=kp1(k)
             km=km1(k)
          DO  j=jp11,my1
          DO  i=ip11,mx1
            WKxyz4(i,j,k) =
     .      WKxyz1(i,j,k)*ab*(ccniHY(i,j,k )-ccniHY(i,j,kp))
     .     -WKxyz3(i,j,k)*ab*(ccniHY(i,j,km)-ccniHY(i,j,k ))
          END DO
          END DO
        END DO
 
          DO  j=jp11,my1
          DO  i=ip11,mx1
            WKxyz4(i,j,mz)=
     .      WKxyz1(i,j,mz)*ab*(ccniHY(i,j,mz)  -zero          )
     .    - WKxyz3(i,j,mz)*ab*(ccniHY(i,j,mmz1)-ccniHY(i,j,mz))
          END DO
          END DO
 
 
C +--Tridiagonal Matrix Inversion - ccniHY
C +  -------------------------------------
 
        DO    k=kp1(1),mz
          DO  j=jp11,my1
          DO  i=ip11,mx1
            WKxyz4(i,j,k) = WKxyz4(i,j,k) + ccniHY(i,j,k)
          END DO
          END DO
        END DO
 
        k1_tua = 1
        k2_tua = mz
 
C +          ************
        call MARgz_2mx1y1(k1_tua,k2_tua)
C +          ************
 
        DO    k=1,mz
          DO  j=jp11,my1
          DO  i=ip11,mx1
            ccniHY(i,j,k) = WKxyz7(i,j,k)
          END DO
          END DO
        END DO
 
 
C +--Precipitation
C +  -------------
 
          DO  j=jp11,my1
          DO  i=ip11,mx1
            dd_sno      =               dt_Loc * rolvDY(i,j,mz)
     .                         * WKxyz8(i,j,mz)*   qiHY(i,j,mz)
     .                         /(gplvDY(i,j,mz)* grvinv-sh(i,j))
            crysHY(i,j) = crysHY(i,j) + dd_sno
            snohSL(i,j) = snohSL(i,j) + dd_sno
          END DO
          END DO
 
 
C +-------------------------------------------------------------------------
 
 
C +--Vertical Diffusion of Cloud Droplets
C +  ====================================
 
 
C +--Second Member of the Tridiagonal System - qwHY
C +  ----------------------------------------------
 
          DO  j=jp11,my1
          DO  i=ip11,mx1
            WKxyz1(i,j,1)= 0.0
            WKxyz2(i,j,1)= 1.0
            WKxyz4(i,j,1)= qwHY(i,j,1)
          END DO
          END DO
 
        DO    k=kp1(1),mmz1
             kp=kp1(k)
             km=km1(k)
          DO  j=jp11,my1
          DO  i=ip11,mx1
            WKxyz4(i,j,k) =
     .      WKxyz1(i,j,k)*ab*(qwHY(i,j,k )-qwHY(i,j,kp))
     .     -WKxyz3(i,j,k)*ab*(qwHY(i,j,km)-qwHY(i,j,k ))
          END DO
          END DO
        END DO
 
          DO  j=jp11,my1
          DO  i=ip11,mx1
            WKxyz4(i,j,mz)=
     .      WKxyz1(i,j,mz)*ab*(qwHY(i,j,mz)  -zero        )
     .    - WKxyz3(i,j,mz)*ab*(qwHY(i,j,mmz1)-qwHY(i,j,mz))
          END DO
          END DO
 
 
C +--Tridiagonal Matrix Inversion - qwHY
C +  -----------------------------------
 
        DO    k=kp1(1),mz
          DO  j=jp11,my1
          DO  i=ip11,mx1
            WKxyz4(i,j,k) = WKxyz4(i,j,k) + qwHY(i,j,k)
          END DO
          END DO
        END DO
 
        k1_tua = 1
        k2_tua = mz
 
C +          ************
        call MARgz_2mx1y1(k1_tua,k2_tua)
C +          ************
 
        DO    k=1,mz
          DO  j=jp11,my1
          DO  i=ip11,mx1
            qwHY(i,j,k) = WKxyz7(i,j,k)
          END DO
          END DO
        END DO
 
 
C +--Precipitation
C +  -------------
 
          DO  j=jp11,my1
          DO  i=ip11,mx1
            rainHY(i,j) = rainHY(i,j) + dt_Loc * rolvDY(i,j,mz)
     .                         * WKxyz8(i,j,mz)*   qwHY(i,j,mz)
     .                         /(gplvDY(i,j,mz)* grvinv-sh(i,j))
          END DO
          END DO
 
 
C +-------------------------------------------------------------------------
 
 
C +--Vertical Diffusion of Rain Drops
C +  ================================
 
 
C +--Second Member of the Tridiagonal System - qrHY
C +  ----------------------------------------------
 
          DO  j=jp11,my1
          DO  i=ip11,mx1
            WKxyz1(i,j,1)= 0.0
            WKxyz2(i,j,1)= 1.0
            WKxyz4(i,j,1)= qrHY(i,j,1)
          END DO
          END DO
 
        DO    k=kp1(1),mmz1
             kp=kp1(k)
             km=km1(k)
          DO  j=jp11,my1
          DO  i=ip11,mx1
            WKxyz4(i,j,k) =
     .      WKxyz1(i,j,k)*ab*(qrHY(i,j,k )-qrHY(i,j,kp))
     .     -WKxyz3(i,j,k)*ab*(qrHY(i,j,km)-qrHY(i,j,k ))
          END DO
          END DO
        END DO
 
          DO  j=jp11,my1
          DO  i=ip11,mx1
            WKxyz4(i,j,mz)=
     .      WKxyz1(i,j,mz)*ab*(qrHY(i,j,mz)  -zero        )
     .    - WKxyz3(i,j,mz)*ab*(qrHY(i,j,mmz1)-qrHY(i,j,mz))
          END DO
          END DO
 
 
C +--Tridiagonal Matrix Inversion - qrHY
C +  -----------------------------------
 
        DO    k=kp1(1),mz
          DO  j=jp11,my1
          DO  i=ip11,mx1
            WKxyz4(i,j,k) = WKxyz4(i,j,k) + qrHY(i,j,k)
          END DO
          END DO
        END DO
 
        k1_tua = 1
        k2_tua = mz
 
C +          ************
        call MARgz_2mx1y1(k1_tua,k2_tua)
C +          ************
 
        DO    k=1,mz
          DO  j=jp11,my1
          DO  i=ip11,mx1
            qrHY(i,j,k) = WKxyz7(i,j,k)
          END DO
          END DO
        END DO
 
 
C +--Precipitation
C +  -------------
 
          DO  j=jp11,my1
          DO  i=ip11,mx1
            rainHY(i,j) = rainHY(i,j) + dt_Loc * rolvDY(i,j,mz)
     .                         * WKxyz8(i,j,mz)*   qrHY(i,j,mz)
     .                         /(gplvDY(i,j,mz)* grvinv-sh(i,j))
          END DO
          END DO
 
 
C +--END   Cloud Microphysics (qiHY, ccniHY, qwHY, qrHY)
C +  ===================================================
 
      END IF
 
 
C +-------------------------------------------------------------------------
 
 
C +--Vertical Diffusion of (Terrigeneous) Hydrometeors and Tracers
C +  =============================================================
 
C +--Tridiagonal Matrix Coefficients: Modifications for qsHY, qxTC
C +  -------------------------------------------------------------
 
C +--Diagonal A
C +  ~~~~~~~~~~
            k=  mz
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz1(i,j,k)= 0.
c #se     WKxyz1(i,j,k)=-gravi2*beta*romiDY(i,j,k)*(zero
c _HH.                                            +pente3(i,j,k)
c #PE.                                            +pente3(i,j,k)
c #se.          )*rolvDY(i,j,k)/(pstDY2(i,j)*dsigm1(k)*dsig_1(k))
        END DO
        END DO
 
C +--A, B, C
C +  ~~~~~~~
            k=  mz
        DO  j=jp11,my1
        DO  i=ip11,mx1
          WKxyz1(i,j,k)  =       WKxyz1(i,j,k) * dt_Loc
          WKxyz2(i,j,k)  = 1.0 - WKxyz3(i,j,k) - WKxyz1(i,j,k)
        END DO
        END DO
 
 
C +-------------------------------------------------------------------------
 
 
C +--BEGIN Cloud Microphysics (qsHY)
C +  ===============================
 
      IF (micphy)                                                   THEN
 
 
C +--Vertical Diffusion of Snow Flakes
C +  =================================
 
 
C +--Second Member of the Tridiagonal System - qsHY
C +  ----------------------------------------------
 
          DO  j=jp11,my1
          DO  i=ip11,mx1
            WKxyz1(i,j,1)= 0.0
            WKxyz2(i,j,1)= 1.0
            WKxyz4(i,j,1)= qsHY(i,j,1)
          END DO
          END DO
 
        DO    k=kp1(1),mmz1
             kp=kp1(k)
             km=km1(k)
          DO  j=jp11,my1
          DO  i=ip11,mx1
            WKxyz4(i,j,k) =
     .      WKxyz1(i,j,k)*ab*(qsHY(i,j,k )-qsHY(i,j,kp))
     .     -WKxyz3(i,j,k)*ab*(qsHY(i,j,km)-qsHY(i,j,k ))
          END DO
          END DO
        END DO
 
          DO  j=jp11,my1
          DO  i=ip11,mx1
            ussno   = uss_HY(i,j)
            WKxyz4(i,j,mz)=
     .      WKxyz1(i,j,mz)
     .                   *(ab* qsHY(i,j,mz)  -qsrfHY(i,j)/beta)
     .    - WKxyz3(i,j,mz)*ab*(qsHY(i,j,mmz1)-  qsHY(i,j,mz))
     .    +(gravit*dt_Loc *  rolvDY(i,j,mz)
     .                      /(pstDY(i,j) *(sigmid(mz)-1.0)))
     .                    *   ussno
          END DO
          END DO
 
 
C +--Tridiagonal Matrix Inversion - qsHY
C +  -----------------------------------
 
        DO    k=kp1(1),mz
          DO  j=jp11,my1
          DO  i=ip11,mx1
            WKxyz4(i,j,k) = WKxyz4(i,j,k) + qsHY(i,j,k)
          END DO
          END DO
        END DO
 
        k1_tua = 1
        k2_tua = mz
 
C +          ************
        call MARgz_2mx1y1(k1_tua,k2_tua)
C +          ************
 
        DO    k=1,mz
          DO  j=jp11,my1
          DO  i=ip11,mx1
            qsHY(i,j,k) = WKxyz7(i,j,k)
          END DO
          END DO
        END DO
 
C +--OUTPUT of Snow Erosion Statistics
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #EV   IF (lEVwri.GT.0  .AND.  lEVwri.LE.2)                        THEN
c #EV     write(6,6011)  itexpe,iEVwri,jEVwri,nEVwri,
c #EV.                   uss_HY(iEVwri,jEVwri)  *1.e3
 6011     format(/,'Iteration',i6,'    Point',3i4,
     .         /,10x,'After TURabl   : us* [mm/s] =',f9.3)
c #EV     write(6,6012)   (qsHY(iEVwri,jEVwri,k)*1.e3,k=mz,mz-4,-1)
 6012     format(10x,'               : q   [g/kg] =',5f9.6)
c #EV   END IF
 
 
C +--END   Cloud Microphysics (qsHY)
C +  ===============================
 
      END IF
 
 
C +-------------------------------------------------------------------------
 
 
C +--Vertical Diffusion of non gazeous Tracers
C +  =========================================
 
c #TC IF (dt_ODE.eq.dtDiff.AND.nterr.GT.0)                          THEN
 
 
C +--Second Member of the Tridiagonal System - qxTC
C +  ----------------------------------------------
 
c #TC   DO     n=1,nterr
 
c #TC       DO j=jp11,my1
c #TC       DO i=ip11,mx1
c #TC         WKxyz1(i,j,1)= 0.0
c #TC         WKxyz2(i,j,1)= 1.0
c #TC         WKxyz4(i,j,1)= qxTC(i,j,1,n)
c #TC       END DO
c #TC       END DO
 
c #TC     DO   k=kp1(1),mmz1
c #TC         kp=kp1(k)
c #TC         km=km1(k)
c #TC       DO j=jp11,my1
c #TC       DO i=ip11,mx1
c #TC         WKxyz4(i,j,k) =
c #TC.        WKxyz1(i,j,k)*ab*(qxTC(i,j,k ,n)-qxTC(i,j,kp,n))
c #TC.       -WKxyz3(i,j,k)*ab*(qxTC(i,j,km,n)-qxTC(i,j,k ,n))
c #TC       END DO
c #TC       END DO
c #TC     END DO
 
c #TC     IF (.NOT.BloMod)                                          THEN
c #TC       DO j=jp11,my1
c #TC       DO i=ip11,mx1
c #TC         uqTC(i,j,n)=-cdhSL(i,j,1)* SLuusl(i,j,1)
c #TC.                                 *  (qsTC(i,j,n)- qxTC(i,j,mz,n))
c #TC       END DO
c #TC       END DO
c #TC     END IF
 
c #TC       DO j=jp11,my1
c #TC       DO i=ip11,mx1
c #TC         WKxyz4(i,j,mz)=  WKxyz1(i,j,mz)
c #TC.                     *(ab* qxTC(i,j,mz  ,n)-qsTC(i,j,n)/beta)
c #TC.      - WKxyz3(i,j,mz)*ab*(qxTC(i,j,mmz1,n)-qxTC(i,j,mz,n))
c #TC.            +(gravit*dt_Loc
c #TC.              *rolvDY(i,j,mz)/(pstDY(i,j)*(sigmid(mz)-1.0d+0)))
c #TC.                *uqTC(i,j,n)
c #TC       END DO
c #TC       END DO
 
 
C +--Tridiagonal Matrix Inversion - qxTC
C +  -----------------------------------
 
c #TC     DO   k=kp1(1),mz
c #TC       DO j=jp11,my1
c #TC       DO i=ip11,mx1
c #TC         WKxyz4(i,j,k) = WKxyz4(i,j,k) + qxTC(i,j,k,n)
c #TC       END DO
c #TC       END DO
c #TC     END DO
 
c #TC     k1_tua = 1
c #TC     k2_tua = mz
 
C +            ************
c #TC     call MARgz_2mx1y1(k1_tua,k2_tua)
C +            ************
 
c #TC     DO   k=1,mz
c #TC       DO j=jp11,my1
c #TC       DO i=ip11,mx1
c #TC        qxTC(i,j,k,n) = WKxyz7(i,j,k)
c #TC       END DO
c #TC       END DO
c #TC     END DO
 
c #TC   END DO
 
c #TC END IF
 
 
C +-------------------------------------------------------------------------
 
 
C +--Atmospheric Water Budget
C +  ========================
 
c #EW DO     i=ip11,mx1
c #EW DO     j=jp11,my1
c #EW     wat1EW(i,j) =  0.00
c #EW   DO   k=1,mz
c #EW     wat1EW(i,j) =  wat1EW(i,j)
c #EW.                +   (qvDY(i,j,k)
c #EW.                +    qwHY(i,j,k) + qrHY(i,j,k)
c #EW.                +    qiHY(i,j,k) + qsHY(i,j,k) ) *dsigm1(k)
c #EW   END DO
c #EW     wat1EW(i,j) =  wat1EW(i,j)  * pstDY(i,j)     *grvinv
c #EW     watfEW(i,j) =-(uss_HY(i,j)  + SLuqs(i,j))
c #EW.          *dt_Loc *rolvDY(i,j,mz)
c #EW END DO
c #EW END DO
 
 
C +-------------------------------------------------------------------------
 
 
C +--Atmospheric Water Budget: Output
C +  ================================
C +
c #EW     waterb = wat1EW(imez,jmez)
c #EW.            -wat0EW(imez,jmez)-watfEW(imez,jmez)
c #EW     write(6,606) jdaMAR,jhaMAR,jmmMAR,
c #EW.               1.d3*wat0EW(imez,jmez),1.d3*wat1EW(imez,jmez),
c #EW.                                      1.d3*watfEW(imez,jmez),
c #EW.                                      1.d3*waterb
 606    format(3i3,'  Before vDif:      ', 12x ,'  W0 = ',f9.6,
     .        /,9x,'  After  vDif:      ', 12x ,'  W1 = ',f9.6,
     .                                          '  W Flux =',f9.6,
     .                                          '  Div(W) =',e9.3)
 
 
C +-------------------------------------------------------------------------
 
 
C +--Work Arrays Reset
C +  =================
 
      DO k=1,mz
         DO j=jp11,my1
         DO i=ip11,mx1
            WKxyz1(i,j,k) = 0.00
            WKxyz2(i,j,k) = 0.00
            WKxyz3(i,j,k) = 0.00
            WKxyz4(i,j,k) = 0.00
            WKxyz5(i,j,k) = 0.00
            WKxyz6(i,j,k) = 0.00
            WKxyz7(i,j,k) = 0.00
         END DO
         END DO
      END DO
 
         DO j=jp11,my1
         DO i=ip11,mx1
            WKxy1(i,j) = 0.00
         END DO
         END DO
 
      return
      end
