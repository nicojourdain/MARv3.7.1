 
 
      subroutine TURtke_difh(dt_dif)
 
C +------------------------------------------------------------------------+
C | MAR TURBULENCE (TKE)                                   18-09-2001  MAR |
C |   SubRoutine TURtke_difh includes TKE Horizontal Diffusion Contribution|
C |      to Turbulent Kinetic Energy (ect_TE) and Dissipation (eps_TE)     |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |  INPUT:TUkhx(mx,my,mz): Horizontal Diffusion Coefficient (x-Direction) |
C |  ^^^^^ TUkhy(mx,my,mz): Horizontal Diffusion Coefficient (y-Direction) |
C |                                                                        |
C |  INPUT / OUTPUT                                                        |
C |  ^^^^^^^^^^^^^^                                                        |
C |      The Horizontal Diffusion and Correction Terms are included for :  |
C |       a) Turbulent Kinetic Energy             ect_TE(mx,my,mz) [m2/s2] |
C |       b) Turbulent Kinetic Energy Dissipation eps_TE(mx,my,mz) [m2/s2] |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_DY.inc'
 
      include 'MAR_TE.inc'
      include 'MAR_TU.inc'
 
      include 'MAR_WK.inc'
 
      real     dt_dif
 
 
C +--Local  Variables
C +  ================
 
      real     tran  ,dx2inv
 
 
C +--Computes Horizontal Diffusion using an Explicit Scheme
C +  ======================================================
 
      DO i=1,mx1
      DO j=1,my
        WKxy1 (i,j) =  0.50*(pstDY(i,j)+pstDY(ip1(i),j))
      END DO
      END DO
 
      DO j=1,my1
      DO i=1,mx
        WKxy2 (i,j) =  0.50*(pstDY(i,j)+pstDY(i,jp1(j)))
      END DO
      END DO
 
                      dx2inv = 1.d0/dx/dx
      DO j=1,my
      DO i=1,mx
        WKxy3 (i,j) = dx2inv/pstDY(i,j)
      END DO
      END DO
 
C +   ================
      DO k=kp1(1),mmz1
C +   ================
 
 
C +--Horizontal Diffusion, x-Direction
C +  =================================
 
        DO i=ip11,mx1
        DO j=jp11,my1
C +
         tran          =          WKxy3(i     ,j)
     .                         * (WKxy1(i     ,j)  * TUkhx(i     ,j,k)
     .                         *(ect_TE(ip1(i),j,k)-ect_TE(i     ,j,k))
     .                           -WKxy1(im1(i),j)  * TUkhx(im1(i),j,k)
     .                         *(ect_TE(i     ,j,k)-ect_TE(im1(i),j,k)))
         tranTE(i,j,k) = tranTE(i,j,k) + tran
         WKxyz1(i,j,k) = dt_dif        * tran
 
         WKxyz2(i,j,k) = dt_dif * WKxy3(i     ,j)
     .                         * (WKxy1(i     ,j)  * TUkhx(i     ,j,k)
     .                         *(eps_TE(ip1(i),j,k)-eps_TE(i,j,k))
     .                           -WKxy1(im1(i),j)  * TUkhx(im1(i),j,k)
     .                         *(eps_TE(i     ,j,k)-eps_TE(im1(i),j,k)))
        END DO
        END DO
 
        DO j=jp11,my1
        DO i=ip11,mx1
         ect_TE(i,j,k) = ect_TE(i,j,k) + WKxyz1(i,j,k)
         eps_TE(i,j,k) = eps_TE(i,j,k) + WKxyz2(i,j,k)
        END DO
        END DO
 
 
C +--Horizontal Diffusion, y-Direction
C +  =================================
 
      IF (mmy.ge.2)                                                 THEN
 
        DO j=jp11,my1
        DO i=ip11,mx1
 
         tran          =          WKxy3(i,j)
     .                         * (WKxy2(i,j)       * TUkhy(i,j     ,k)
     .                         *(ect_TE(i,jp1(j),k)-ect_TE(i,j,k))
     .                           -WKxy2(i,jm1(j))  * TUkhy(i,jm1(j),k)
     .                         *(ect_TE(i,j     ,k)-ect_TE(i,jm1(j),k)))
         tranTE(i,j,k) = tranTE(i,j,k) + tran
         WKxyz1(i,j,k) = dt_dif        * tran
 
         WKxyz2(i,j,k) = dt_dif * WKxy3(i,j)
     .                         * (WKxy2(i,j)       * TUkhy(i,j     ,k)
     .                         *(eps_TE(i,jp1(j),k)-eps_TE(i,j,k))
     .                           -WKxy2(i,jm1(j))  * TUkhy(i,jm1(j),k)
     .                         *(eps_TE(i,j     ,k)-eps_TE(i,jm1(j),k)))
        END DO
        END DO
 
        DO j=jp11,my1
        DO i=ip11,mx1
         ect_TE(i,j,k) = ect_TE(i,j,k) + WKxyz1(i,j,k)
         eps_TE(i,j,k) = eps_TE(i,j,k) + WKxyz2(i,j,k)
        END DO
        END DO
 
      END IF
 
C +   ======
      END DO
C +   ======
 
 
C +--Work Arrays Reset
C +  =================
 
      DO     j=1,my
      DO     i=1,mx
        WKxy1(i,j) = 0.0
        WKxy2(i,j) = 0.0
        WKxy3(i,j) = 0.0
        WKxy4(i,j) = 0.0
      END DO
      END DO
 
      DO     k=1,mz
      DO     j=1,my
      DO     i=1,mx
        WKxyz1(i,j,k) = 0.0
        WKxyz2(i,j,k) = 0.0
      END DO
      END DO
      END DO
 
      return
      end
