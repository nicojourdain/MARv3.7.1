 
 
      subroutine DYNrho
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS                                           16-12-2000  MAR |
C |   SubRoutine DYNrho computes the Air Specific Mass                     |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT   (via common block)                                           |
C |   ^^^^^   pstDY(mx,my)   : Atmosphere Thickness                  [kPa] |
C |          tairDY(mx,my,mz): Air Temperature                         [K] |
C |           virDY(mx,my,mz): Air Loading by Water Vapor and Hydrometeors |
C |                                                                        |
C |   OUTPUT  (via common block)                                           |
C |   ^^^^^^ rolvDY(mx,my,mz): Air Specific Mass at Sigma Levels    [T/m3] |
C |          romiDY(mx,my,mz): Air Specific Mass in Sigma Layers    [T/m3] |
C |          pstDY2(mx,my)   : Atmosphere Thickness Squared         [kPa2] |
C |                                                                        |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +--Global Variables
C +  ================
C +
      include 'MARphy.inc'
C +
      include 'MARdim.inc'
      include 'MARgrd.inc'
C +
      include 'MAR_DY.inc'
C +
      include 'MAR_SL.inc'
C +
C +
C +--Local  Variables
C +  ================
C +
      real    sigmmz
C +
C +
C +--Perfect Gas Law Relationship
C +  ----------------------------
C +
      DO k=1,mz
      DO j=1,my
      DO i=1,mx
        rolvDY(i,j,k)=(pstDYn(i,j)*sigma(k)+ptopDY)
     .               /(RDryAi
     .               * tairDY(i,j,k)*(1.0+0.715*virDY(i,j,k)))
C +...  rolvDY(i,j,k) : Air Specific Mass at grid point (i,j,k)
C +                  0.715 allows to include vir by 0.715=0.608/0.850
C +
      END DO
      END DO
      END DO
C +
      DO k=1,mmz1
      DO j=1,my
      DO i=1,mx
        romiDY(i,j,k) =(pstDYn(i,j)*sigmid(kp1(k))+ptopDY)
     .                /(RDryAi
     .      *0.5*(tairDY(i,j,k)     *(1.0+0.715*virDY(i,j,k))
     .           +tairDY(i,j,kp1(k))*(1.0+0.715*virDY(i,j,kp1(k)))))
C +...  romiDY(i,j,k) : Air Specific Mass at grid point (i,j,k+1/2)
C +
      END DO
      END DO
      END DO
C +
                                    sigmmz = 0.500 *(1.0+sigma(mz))
      DO j=1,my
      DO i=1,mx
        romiDY(i,j,mz)=(pstDYn(i,j)*sigmmz         +ptopDY)
     .                /(RDryAi*0.50*(tairDY(i,j,mz)+TairSL(i,j))
     .                             *(1.0  +  0.715 * virDY(i,j,mz)))
        pstDY2(i,j)   = pstDY(i,j)*pstDY(i,j)
      END DO
      END DO
C +
      return
      end
