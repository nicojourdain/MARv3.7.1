      subroutine DYNfil_3D_mp(f3_fil,e3_fil,k3_fil)
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS FILTER (3-D)                           19-11-2012-XF  MAR |
C |   SubRoutine DYNfil_3D_mp is used to Filter Horizontal Fields in 3DCode|
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:   f3_fil(i,j,k): variable to be filtered (in surface k)       |
C |   ^^^^^    e3_fil(    k): value of the selectivity parameter           |
C |            k3_fil       : vertical dimension of the variable           |
C |                                                                        |
C |   OUTPUT:  f3_fil(i,j,k)                                               |
C |   ^^^^^^^                                                              |
C |                                                                        |
C |   LATERAL BOUNDARIES:                                                  |
C |   ^^^^^^^^^^^^^^^^^^^                                                  |
C |   1. The value    of the variable is fixed at the Boundary             |
C |   2. Filter Selectivity Parameter may be increased near the Boundary   |
C |                                                                (#EP)   |
C |                                                                        |
C |   REFER. : Raymond and Garder, MWR 116, Jan 1988, p209                 |
C |   ^^^^^^^^                                                             |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_WK.inc'
 
      real     f3_fil(mx,my,mz),e3_fil(mz)
      integer  k3_fil
 
 
C +--Local  Variables
C +  ================
 
      real     eps2(   mz)
      real     eps3(   mz)
      real     eps4(   mz)
      real     eps5(   mz)
      real     a1_fil(1:mx,mz) ,b1_fil(1:mx,mz) ,aa_fil(mz)
      real     a2_fil(1:my,mz) ,b2_fil(1:my,mz) ,bb_fil(mz)
 
 
C +--Initialisation
C +  ==============
 
      m  = mx
      m1 = mx1
      m2 = mx2
c #EP m3 = mx-3
c #EP m4 = mx-4
      mn = my
      mn1= my1
      mn2= my2
c #EP mn3= my-3
c #EP mn4= my-4
 
 
C +--1st Matrix Initialisation
C +  -------------------------
 
!$OMP PARALLEL DO default(shared)
!$OMP.private(a1_fil,b1_fil,a2_fil,b2_fil,
!$OMP.        WTxy4,WTxy6,WTxy7,i,j,k)
      DO k=1,k3_fil
        a1_fil( 1,k) = 0.0
        b1_fil( 1,k) = 1.0
        a1_fil(mx,k) = 0.0
        b1_fil(mx,k) = 1.0
 
c #EP   eps5( k) =      e3_fil(k)+e3_fil( k)
c #EP   eps4( k) =        eps5(k)+eps5(k)
c #EP   eps3( k) =        eps4(k)+eps4(k)
c #EP   eps2( k) =        eps3(k)+eps3(k)
C +..   Pour diminution de la sectivite du filtre vers les bords
C +       (augmentation --X 2-- parametre selectivite a chaque pas spatial)
 
        aa_fil(   k) =      1.0-e3_fil(k)
        bb_fil(   k) = 2.0*(1.0+e3_fil(k))
 
      DO i=ip11,mx1
        a1_fil(i, k) = aa_fil(k)
        b1_fil(i, k) = bb_fil(k)
      END DO
 
c #EP   a1_fil(2,k) =      1.0-eps2(k)
c #EP   a1_fil(3,k) =      1.0-eps3(k)
c #EP   a1_fil(4,k) =      1.0-eps4(k)
c #EP   a1_fil(5,k) =      1.0-eps5(k)
c #EP   b1_fil(2,k) = 2.0*(1.0+eps2(k))
c #EP   b1_fil(3,k) = 2.0*(1.0+eps3(k))
c #EP   b1_fil(4,k) = 2.0*(1.0+eps4(k))
c #EP   b1_fil(5,k) = 2.0*(1.0+eps5(k))
 
c #EP   a1_fil(m1,k)= a1_fil(2,k)
c #EP   a1_fil(m2,k)= a1_fil(3,k)
c #EP   a1_fil(m3,k)= a1_fil(4,k)
c #EP   a1_fil(m4,k)= a1_fil(5,k)
c #EP   b1_fil(m1,k)= b1_fil(2,k)
c #EP   b1_fil(m2,k)= b1_fil(3,k)
c #EP   b1_fil(m3,k)= b1_fil(4,k)
c #EP   b1_fil(m4,k)= b1_fil(5,k)
 
 
C +--2th Matrix Initialisation
C +  -------------------------
 
        a2_fil( 1,k) = 0.0
        b2_fil( 1,k) = 1.0
        a2_fil(my,k) = 0.0
        b2_fil(my,k) = 1.0
 
      DO j=jp11,my1
        a2_fil(j,k)  = aa_fil(k)
        b2_fil(j,k)  = bb_fil(k)
      END DO
 
c #EP   a2_fil(2,k) = a1_fil(2,k)
c #EP   a2_fil(3,k) = a1_fil(3,k)
c #EP   a2_fil(4,k) = a1_fil(4,k)
c #EP   a2_fil(5,k) = a1_fil(5,k)
c #EP   b2_fil(2,k) = b1_fil(2,k)
c #EP   b2_fil(3,k) = b1_fil(3,k)
c #EP   b2_fil(4,k) = b1_fil(4,k)
c #EP   b2_fil(5,k) = b1_fil(5,k)
 
c #EP   a2_fil(mn1,k) = a1_fil(2,k)
c #EP   a2_fil(mn2,k) = a1_fil(3,k)
c #EP   a2_fil(mn3,k) = a1_fil(4,k)
c #EP   a2_fil(mn4,k) = a1_fil(5,k)
c #EP   b2_fil(mn1,k) = b1_fil(2,k)
c #EP   b2_fil(mn2,k) = b1_fil(3,k)
c #EP   b2_fil(mn3,k) = b1_fil(4,k)
c #EP   b2_fil(mn4,k) = b1_fil(5,k)
 
c      END DO
 
C +--1st Equations System
C +  ====================
 
C +--Gaussian Elimination Algorithm: Set Up
C +  --------------------------------------
 
c      DO k=1,k3_fil
      DO j=jp11,my1
 
        WTxy4(1     ,j) =
     &  f3_fil( 1,jm1(j),k) +2.0*f3_fil( 1,j,k)+f3_fil( 1,jp1(j),k)
        WTxy4(mx    ,j) =
     &  f3_fil(mx,jm1(j),k) +2.0*f3_fil(mx,j,k)+f3_fil(mx,jp1(j),k)
 
        DO i=ip11,mx1
              WTxy4(i     ,j        ) =
     &        f3_fil(im1(i),jp1(j),k)+2.0*f3_fil(i,jp1(j),k)
     &   +    f3_fil(ip1(i),jp1(j),k)+
     &    2.0*f3_fil(im1(i),j     ,k)+4.0*f3_fil(i,j     ,k)
     &   +2.0*f3_fil(ip1(i),j     ,k)+
     &        f3_fil(im1(i),jm1(j),k)+2.0*f3_fil(i,jm1(j),k)
     &   +    f3_fil(ip1(i),jm1(j),k)
        END DO
 
      END DO
c     END DO
 
 
C +--Gaussian Elimination Algorithm: F-B Sweep ==> WTxy7
C +  ----------------------------------------------------
C +
c       DO k=1,k3_fil
        DO j=1,my
 
C +--Forward  Sweep
 
            WTxy6(1,j)=-a1_fil(1,k)/b1_fil(1,k)
            WTxy7(1,j)= WTxy4(1,j)/b1_fil(1,k)
          DO i=ip11,mx
            WTxy6(i,j)=-a1_fil(i,k)
     .                   /(a1_fil(i,k)*WTxy6(i-1,j)+b1_fil(i,k))
            WTxy7(i,j)=(WTxy4(i,j)-a1_fil(i,k)*WTxy7(i-1,j))
     .                   /(a1_fil(i,k)*WTxy6(i-1,j)+b1_fil(i,k))
          END DO
 
C +--Backward Sweep
 
          DO i=mx1,1,-1
            WTxy7(i,j)=WTxy6(i,j)*WTxy7(i+1,j)+WTxy7(i,j)
          END DO
 
        END DO
c       END DO
 
 
C +--2th Equations System
C +  ====================
 
C +--Gaussian Elimination Algorithm: Set Up
C +  --------------------------------------
 
c     DO k=1,k3_fil
      DO i=ip11,mx1
          WTxy4(i, 1) = f3_fil(i, 1, k)
          WTxy4(i,my) = f3_fil(i,my, k)
        DO j=jp11,my1
          WTxy4(i, j) = WTxy7(i, j)
        END DO
      END DO
c     END DO
 
 
C +--Gaussian Elimination Algorithm: F-B Sweep ==> WTxy7
C +  ----------------------------------------------------
 
c       DO k=1,k3_fil
        DO i=1,mx
 
C +--Forward  Sweep
 
            WTxy6(i,1)=-a2_fil(1,  k)/b2_fil(1,k)
            WTxy7(i,1)= WTxy4(i,1)/b2_fil(1,k)
          DO j=jp11,my
            WTxy6(i,j)=-a2_fil(  j,k)
     .                   /(a2_fil(  j,k)*WTxy6(i,j-1)+b2_fil(j,k))
            WTxy7(i,j)=(WTxy4(i,j)-a2_fil(j,k)*WTxy7(i,j-1))
     .                   /(a2_fil(  j,k)*WTxy6(i,j-1)+b2_fil(j,k))
          END DO
 
C +--Backward Sweep
 
          DO j=my1,1,-1
            WTxy7(i,j)=WTxy6(i,j)*WTxy7(i,j+1)+WTxy7(i,j)
          END DO
 
        END DO
c       END DO
 
 
C +--Result
C +  ======
 
c     DO k=1,k3_fil
      DO j=jp11,my1
      DO i=ip11,mx1
        f3_fil(i,j,k) = WTxy7(i,j)
      END DO
      END DO
      END DO
!$OMP END PARALLEL DO
 
      return
      end
