      function psih(zetaIh)
C +
C +------------------------------------------------------------------------+
C | MAR TURBULENCE (ASL)                                    9-10-2001  MAR |
C |   Function psih is the Integrated Universal Function for Heat          |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C | OPTIONS:Duynkerke,MWR 119,                         324--341, 1991 (#DU |
C | ^^^^^^^ Businger, Workshop on Micrometeorology AMS, 67--100, 1973 (#BU |
C |         Dyer,     BLM 7,                           363--372, 1974 (#DR |
C |         Noihlan,  EERM Internal Note No 171,                 1986 (#NO |
C |                                                                        |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
      include 'MARphy.inc'
C +
      real     psih
      real     zetaIh
      real     r9p3  ,beta,r     ,y
C +
      data     r9p3/9.375e0/
c #DR data     beta/5.0e0/,r/1.0e0/
c #BU data     beta/4.7e0/,r/.74e0/
C +
C +--Stability
C +  ~~~~~~~~~
      IF  (zetaIh.gt.0.0)                                          THEN
           psih=-6.0*zetaIh
C +...     Bintanja 1997, Annals of Glaciology
C +
c #DU      psih=-exp(0.80*log(unun+r9p3*zetaIh))
c #NO   IF(zetaIh.lt.1.0)                                          THEN
c #DR      psih=-beta*zetaIh
c #BU      psih=-beta*zetaIh/r
c #NO   ELSE
C +
C +--Strong Stability
C +  ~~~~~~~~~~~~~~~~
c #NO      psih=-beta*(unun+log(zetaIh))/r
c #NO   END IF
      ELSE
C +
C +--Unstability
C +  ~~~~~~~~~~~
           y   = sqrt(1.0-15.0*zetaIh)
c #DR      y   = sqrt(1.0-16.0*zetaIh)
c #BU      y   = sqrt(1.0- 9.0*zetaIh)
           psih= 2.0*log(demi*(unun+y))
      END IF
C +
      return
      end
