 
 
      subroutine MARgau_y(i1,i2,j1,j2,k1,k2)
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS                                           17-12-2000  MAR |
C |   SubRoutine MARgau_y performs Gaussian Elimination Algorithm along y  |
C |    (e.g. Pielke (1984), pp.302--303)                                   |
C |    (needed to solve the implicit scheme developped for filtering)      |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:              i1,i2,j1,j2,k1,k2: i,j,k Loops Limits            |
C |   ^^^^^                                                                |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_WK.inc'
 
      integer i1,i2,j1,j2,k1,k2,ix
 
      data ix /0/
 
 
C +--Forward  Sweep
C +  ==============
 
      IF (ix.ne.1)                                                THEN
          DO k=k1,k2
          DO i=i1,i2
            WKxyz5(i,j1,k)= WKxyz2(i,j1,k)
            WKxyz6(i,j1,k)=-WKxyz1(i,j1,k)/WKxyz5(i,j1,k)
          END DO
          END DO
        DO j=jp1(j1),j2
          DO k=k1,k2
          DO i=i1,i2
            WKxyz5(i,j,k)= WKxyz3(i,j,k)*WKxyz6(i,j-1,k)+WKxyz2(i,j,k)
            WKxyz6(i,j,k)=-WKxyz1(i,j,k)/WKxyz5(i,j  ,k)
          END DO
          END DO
        END DO
      END IF
 
          DO k=k1,k2
          DO i=i1,i2
            WKxyz7(i,j1,k)= WKxyz4(i,j1,k)/WKxyz5(i,j1,k)
          END DO
          END DO
 
        DO j=jp1(j1),j2
          DO k=k1,k2
          DO i=i1,i2
            WKxyz7(i,j,k)=(WKxyz4(i,j,k)-WKxyz3(i,j,k)*WKxyz7(i,j-1,k))
     .                    /WKxyz5(i,j,k)
          END DO
          END DO
        END DO
 
 
C +--Backward Sweep
C +  ==============
 
        DO j=jm1(j2),j1,-1
          DO k=k1,k2
          DO i=i1,i2
            WKxyz7(i,j,k)=WKxyz6(i,j,k)*WKxyz7(i,j+1,k)+WKxyz7(i,j,k)
          END DO
          END DO
        END DO
 
      return
      end
