      subroutine SVAsav(Ordr)
 
C +------------------------------------------------------------------------+
C | MAR OUTPUT                                         Sun 14-06-2009  MAR |
C |   SubRoutine SVAsav is used to save the main SVAT  Variables           |
C |                                                                        |
C | # OPTIONS:   #OA: Ocean Albedo is prescribed                           |
C | # ^^^^^^^^                                                             |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
 
      include 'MARdim.inc'
 
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
 
      include 'MAR_LB.inc'
 
      include 'MAR_SL.inc'
      include 'MAR_SV.inc'
      include 'MAR_TV.inc'
      include 'MAR_VB.inc'
      include 'MARsSN.inc'
      include 'MAR_BS.inc'
      include 'MAR_IB.inc'
 
      character*4 Ordr
 
 
C +--Local  Variables
C +  ================
 
      character*6 vartyp
      integer     n1    ,n2    ,iteSVA,iyrSVA,mmaSVA,jdaSVA,jhuSVA,n
      integer     i50   ,j50   ,iim   ,jjm   ,msc
 
      logical     iniWRI
 
      DATA        iniWRI/.true./
 
      real mskSNo2(mx,my)
 
 
C +--READ
C +  ====
 
      IF (Ordr.eq.'read')                                         THEN
 
 
C +--Open File
C +  ---------
 
          OPEN (unit=11,status='old'    ,form='unformatted',
     .                                   file='MARsvt.DAT')
 
 
C +--Read DATA
C +  ---------
 
            READ(11) iteSVA
            READ(11) iyrSVA,mmaSVA,jdaSVA,jhuSVA
          IF   (itexpe.eq.0)                                      THEN
            IF (iteSVA.ne.itexpe.or.
     .          iyrSVA.ne.iyr0GE.or.
     .          mmaSVA.ne.mma0GE.or.
     .          jdaSVA.ne.jda0GE.or.
     .          jhuSVA.ne.jhu0GE)
     .        write(6,600)itexpe,iyrSVA,mmaSVA,jdaSVA,jhuSVA,
     .                           iyr0GE,mma0GE,jda0GE,jhu0GE
 600          format(' ++WARNING++ MARsvt improperly specified ',
     .             /,'          iyr    mma    jda    jhu',
     .             /,i6,4i7,' Old',/,6x,4i7,' Current')
          ELSE
            IF (iteSVA.ne.itexpe.or.
     .          iyrSVA.ne.iyrrGE.or.
     .          mmaSVA.ne.mmarGE.or.
     .          jdaSVA.ne.jdarGE.or.
     .          jhuSVA.ne.jhurGE)
     .        write(6,600)itexpe,iyrSVA,mmaSVA,jdaSVA,jhuSVA,
     .                           iyrrGE,mmarGE,jdarGE,jhurGE
          END IF
 
            READ(11) IOi_TV  ! IO i Index
            READ(11) IOj_TV  ! IO j Index
            READ(11) isolTV  ! Soil       Type Index
            READ(11) iWaFTV  ! =0 ==> no Water Flux
                             ! =1 ==> free Drainage
            READ(11) AlbSTV  ! Dry Soil       Albedo
            READ(11) ivegTV  ! Vegetation Type Index
            READ(11) ifraTV  ! Vegetation Class Coverage
            READ(11) alaiTV  !       Leaf Area Index                [-]
            READ(11) glf_TV  ! Green Leaf Fraction                  [-]
            READ(11) TsolTV  ! Soil Temperature                     [K]
            READ(11) eta_TV  ! Soil Moisture                    [m3/m3]
 
 
C +--If Simulation   start, THEN initialize further
C +  ----------------------------------------------
 
        IF (itexpe.eq.0)                                          THEN
          DO j=1,my
          DO i=1,mx
            IF (isolSL(i,j).eq. 1)                                THEN
                ifraTV(i,j,1) = 100
                ivegTV(i,j,1) = 0
                isolTV(i,j)   = 0
c #OA           AlbSTV(i,j)   = 0.15
            END IF
            IF (isolSL(i,j).eq. 5)                                THEN
                tsrfSL(i,j,1) = 0.0d+0
              DO n=1,nvx
                tsrfSL(i,j,1) = tsrfSL(i,j,1)
     .                        + TsolTV(i,j,n,1)*ifraTV(i,j,n)
              END DO
                tsrfSL(i,j,1) = tsrfSL(i,j,1)  *1.0d-2
                albsSL(i,j)   = AlbSTV(i,j)
                alb0SL(i,j)   = albsSL(i,j)
                albeSL(i,j)   = albsSL(i,j)
              IF (tsrfSL(i,j,1).lt.200.)
     .            write(6,6000) i,j
 6000             format(' WARNING: undefined Surface Temperature',
     .                   ' (i j) = (',2i4,')')
 
            END IF
 
C +--Set of SISVAT Variables
C +  ~~~~~~~~~~~~~~~~~~~~~~~
            IF      (VSISVAT)                                     THEN
              IF    (isolSL(i,j).le.2)                            THEN
                     ifraTV(i,j,1)   = 100
                     ivegTV(i,j,1)   = 0
                     isolTV(i,j)     = 0
                IF  (reaVAR.AND.reaLBC)                           THEN
                  DO n= 1,       nvx
                     TvegTV(i,j,n)   = sst_LB(i,j)
                  DO k= 1,       llx
                     TsolTV(i,j,n,k) = sst_LB(i,j)
                     eta_TV(i,j,n,k) = 1.
                  END DO
                  END DO
                ELSE
                  DO n= 1,       nvx
                     TvegTV(i,j,n)   = SST_SL
                  DO k= 1,       llx
                     TsolTV(i,j,n,k) = SST_SL
                     eta_TV(i,j,n,k) = 1.
                  END DO
                  END DO
                END IF
 
              ELSE
                IF (.not.reaLBC)                                  THEN
                  DO n= 1,       nvx
                     TvegTV(i,j,n)   = SST_SL
                  END DO
                ELSE
                  DO n= 1,       nvx
                     TvegTV(i,j,n)   = TairSL(i,j)
                  END DO
                END IF
              END IF
            END IF
 
              DO n= 1, max(mw,nvx)
                           n1   = min(n,mw)
                           n2   = min(n,nvx)
                tsrfSL(i,j,n1)  = TsolTV(i,j,n2,1)
              END DO
 
          END DO
          END DO
 
          IF (iniWRI)                                             THEN
                                  n = 123
                                                 i50 = min(mx,66)
                                                 j50 = min(my,66)
                           vartyp =  'isolTV'
              write(4,4001)vartyp,n,((isolTV(i,j  ),i=1,i50),j=j50,1,-1)
 4001        format(/,' --- SVAsav --- ',a6,' ---',i4,' ---',/,(66i2))
 4002        format(/,' --- SVAsav --- ',a6,' ---',i4,' ---',/,(66f6.2))
 
                           vartyp =  'iWaFTV'
              write(4,4001)vartyp,n,((iWaFTV(i,j  ),i=1,i50),j=j50,1,-1)
            DO                    n =            1,nvx
                           vartyp =  'ifraTV'
              write(4,4002)vartyp,n,((ifraTV(i,j,n),i=1,i50),j=j50,1,-1)
                           vartyp =  'ivegTV'
              write(4,4001)vartyp,n,((ivegTV(i,j,n),i=1,i50),j=j50,1,-1)
            END DO
 
              iniWRI = .false.
          END IF
        END IF
 
C +--If SISVAT is set up
C +  ~~~~~~~~~~~~~~~~~~~
        IF (VSISVAT.AND..NOT.reaVAR)                              THEN
          DO j=1,my
          DO i=1,mx
            sst_LB(i,j) = SST_SL
            sst1LB(i,j) = SST_SL
            sst2LB(i,j) = SST_SL
          END DO
          END DO
        END IF
 
 
C +--If Simulation   start, THEN set        to zero
C +  ----------------------------------------------
 
        IF (itexpe.eq.0)                                          THEN
            DO msc=1,nvx
            DO jjm=1,jmx
            DO iim=1,imx
                     CaSnTV(iim,jjm,msc) =  0.0
                     CaWaTV(iim,jjm,msc) =  0.0
                     WEq_SN(iim,jjm,msc) =  0.0
            ENDDO
            ENDDO
            ENDDO
            DO msc=1,nsx
            DO jjm=1,jmx
            DO iim=1,imx
                     snohSN(iim,jjm,msc) =  0.0
            ENDDO
            ENDDO
            ENDDO
        END IF
 
 
C +--If Simulation restart, THEN read       further
C +  ----------------------------------------------
 
        IF (itexpe.ne.0.and.reaVAR)                               THEN
 
            READ(11) CaSnTV  ! Canopy Intercepted Snow  Content[m w.e.]
            READ(11) CaWaTV  ! Canopy Intercepted Water Content [kg/m2]
            READ(11) psivTV  ! Vegetation   Hydraulic Potential     [m]
            READ(11) psigTV  ! Ground       Hydraulic Potential     [m]
            READ(11) TvegTV  ! Skin      Vegetation Temperature     [K]
            READ(11) TgrdTV  ! Skin            Soil Temperature     [K]
 
            DO msc=1,nvx
            DO jjm=1,jmx
            DO iim=1,imx
                 IF (CaWaTV(iim,jjm,msc).LE.1.d-20)
     .               CaWaTV(iim,jjm,msc) =  0.d+00
            ENDDO
            ENDDO
            ENDDO
 
            READ(11) evapTV  ! Total Evapotranspiration [mm w.e.]
            READ(11) draiTV  ! Drainage            Flow    [mm/s]
            READ(11) runoTV  ! Integrated Drainage Flow      [mm]
 
 
C +--Snow Pack Characteristics
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~
C #SN     IF (SnoMod)                                             THEN
            if(mw.ne.5) then
            READ(11) mskSNo2 ! Snow/Ice Type Index            [-]
            do i=1,mx ; do j=1,my
            mskSNo(i,j,1)=mskSNo2(i,j)
            enddo     ; enddo
            else
            READ(11) mskSNo  ! Snow/Ice Type Index            [-]
            endif
            READ(11) nssSNo  ! Nb Snow and     Ice Layers
            READ(11) issSNo  ! Nb Superimposed Ice Layers
            READ(11) nisSNo  ! Nb              Ice Layers
            READ(11) nhsSNo  ! Snow History                   [-]
            READ(11) dzsSNo  ! Snow Layers  Thickness         [m]
            READ(11) rosSNo  ! Snow Volumic Mass          [kg/m3]
            READ(11) wasSNo  ! Snow Water   Content       [kg/kg]
            READ(11) tisSNo  ! Snow Temperature               [K]
            READ(11) g1sSNo  ! Snow Dendricity / Sphericity   [-]
            READ(11) g2sSNo  ! Snow Sphericity / Size         [-] [0.0001 m]
            READ(11) agsSNo  ! Snow Age                     [day]
            READ(11) snohSN  ! Snow Buffer Layer    [kg/m2], [mm]
            READ(11) BrosSN  ! Snow Buffer Layer Density  [kg/m3]
            READ(11) BG1sSN  ! Snow Buffer Layer Dendri/Spher.[-]
            READ(11) BG2sSN  ! Snow Buffer Layer Spheri/Size  [-] [0.0001 m]
            READ(11) SWaSNo  ! Snow Surficial Water           [m]
            READ(11) zWE0SN  ! Initial      Snow Thickn.[mm w.e.]
            READ(11) zWE_SN  ! Current      Snow Thickn.[mm w.e.]
            READ(11) zWEcSN  ! Non-erodible Snow Thickn.[mm w.e.]
            READ(11) SaltSN  ! u*_th                        [m/s]
            READ(11) SLussl  ! u*_s*                  [kg/kg.m/s]
            READ(11) blowSN  ! NEW  MAX Erosion           [kg/m2]
            READ(11) WEq_SN  ! Added Snow Amount         [m w.e.]
            READ(11) SLn_z0  ! Z0_momentum    (instantaneous) [m]
            READ(11) SLn_r0  ! Z0_scalar      (instantaneous) [m]
c #BS       READ(11) SLn_b0  ! Z0_erosion     (instantaneous) [m]
            READ(11) ua_0BS  !  Wind ,  x-component (t-dt)  [m/s]
            READ(11) va_0BS  !  Wind ,  y-component (t-dt)  [m/s]
            READ(11) VVs_BS  ! (wind ,  Sastrugi) Relevance [m/s]
            READ(11) RRs_BS  ! (wind ,  Sastrugi) Counter     [-]
            READ(11) DDs_BS  ! (wind ,  Sastrugi) Angle      [dg]
c #SZ       READ(11) Z0SaBS  ! Z0      (Sastrugi Height)      [m]
            READ(11) wes_IB  ! Snow/ice Sublimation     [mm w.e.]
            READ(11) wee_IB  ! Evapotranspiration       [mm w.e.]
            READ(11) wem_IB  ! Snow/ice Melting         [mm w.e.]
            READ(11) wer_IB  ! Snow/ice Refreezing      [mm w.e.]
            READ(11) wei0IB  ! Bottom Snow/ice added    [mm w.e.]
            READ(11) weu_IB  ! Run-off                  [mm w.e.]
            READ(11) zn0IB   ! Initial Snow Height            [m]
            READ(11) mb0IB   ! Initial Mass Balance        [mmWE]
            if (mw .eq. 5) then
            READ(11) gradTM !*CL* Local temp. gradient  [C/m]
            READ(11) gradQM !*CL* Local hum. gradient   [g/kg/m]
            endif
C #SN     END IF
 
        ELSE IF (itexpe.EQ.0)                                     THEN
            DO j=1,my
            DO i=1,mx
                         evapTV(i,j)= 0.0
                         VVs_BS(i,j)=10.0
                         RRs_BS(i,j)= 1.0
                         DDs_BS(i,j)= 0.0
            END DO
            END DO
        ENDIF
 
          CLOSE(unit=11)
 
 
C +--SVAT Prescribed Evolutive VBC (Vegetation Boundary  Condition)
C +                                (i.e.,      Green Leaf Fraction)
C +  --------------------------------------------------------------
 
          tim1VB=ou2sGE(iyrSVA,mmaSVA,jdaSVA,jhuSVA,0,0)
          tim2VB=   tim1VB
 
          DO n=1,nvx
          DO j=1,my
          DO i=1,mx
c #LN       LAI1VB(i,j,n) = alaiTV(i,j,n)
c #LN       LAI2VB(i,j,n) = alaiTV(i,j,n)
            glf1VB(i,j,n) = glf_TV(i,j,n)
            glf2VB(i,j,n) = glf_TV(i,j,n)
          END DO
          END DO
          END DO
 
c         MAR-GRISLI coupling
          call ice_sheet_model_coupling
 
      END IF
 
 
C +--WRITE
C +  =====
 
      IF (Ordr.eq.'writ')                                         THEN
 
        OPEN(unit=11,status='unknown',form='unformatted',
     .                                file='MARsvt.DAT')
           WRITE(11) itexpe
           WRITE(11) iyrrGE,mmarGE,jdarGE,jhurGE
 
           WRITE(11) IOi_TV  ! IO i Index
           WRITE(11) IOj_TV  ! IO j Index
           WRITE(11) isolTV  ! Soil       Type Index
           WRITE(11) iWaFTV  ! =0 ==> no Water Flux
                             ! =1 ==> free Drainage
           WRITE(11) AlbSTV  ! Dry Soil       Albedo
           WRITE(11) ivegTV  ! Vegetation Type Index
           WRITE(11) ifraTV  ! Vegetation Class Coverage
           WRITE(11) alaiTV  !       Leaf Area Index                [-]
           WRITE(11) glf_TV  ! Green Leaf Fraction                  [-]
           WRITE(11) TsolTV  ! Soil Temperature                     [K]
           WRITE(11) eta_TV  ! Soil Moisture                    [m3/m3]
 
           WRITE(11) CaSnTV  ! Canopy Intercepted Snow  Content[m w.e.]
           WRITE(11) CaWaTV  ! Canopy Intercepted Water Content [kg/m2]
           WRITE(11) psivTV  ! Vegetation   Hydraulic Potential     [m]
           WRITE(11) psigTV  ! Ground       Hydraulic Potential     [m]
           WRITE(11) TvegTV  ! Skin      Vegetation Temperature     [K]
           WRITE(11) TgrdTV  ! Skin            Soil Temperature     [K]
 
           WRITE(11) evapTV  ! Total Evapotranspiration       [mm w.e.]
           WRITE(11) draiTV  ! Drainage            Flow          [mm/s]
           WRITE(11) runoTV  ! Integrated Drainage Flow            [mm]
 
           do i=1,mx ; do j=1,my
             mskSNo2(i,j)=mskSNo(i,j,1)
           enddo     ; enddo
 
C +--Snow Pack Characteristics
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~
C #SN    IF (SnoMod)                                              THEN
 
           if(mw.ne.5) then
           WRITE(11) mskSNo2 ! Snow/Ice Type Index               [-]
           else
           WRITE(11) mskSNo  ! Snow/Ice Type Index               [-]
           endif
           WRITE(11) nssSNo  ! Nb Snow and     Ice Layers
           WRITE(11) issSNo  ! Nb Superimposed Ice Layers
           WRITE(11) nisSNo  ! Nb              Ice Layers
           WRITE(11) nhsSNo  ! Snow History                      [-]
           WRITE(11) dzsSNo  ! Snow Layers  Thickness            [m]
           WRITE(11) rosSNo  ! Snow Volumic Mass             [kg/m3]
           WRITE(11) wasSNo  ! Snow Water   Content          [m3/m3]
           WRITE(11) tisSNo  ! Snow Temperature                  [K]
           WRITE(11) g1sSNo  ! Snow Dendricity / Sphericity      [-]
           WRITE(11) g2sSNo  ! Snow Sphericity / Size            [-] [0.0001 m]
           WRITE(11) agsSNo  ! Snow Age                        [day]
           WRITE(11) snohSN  ! Snow Buffer    Layer [kg/m2],    [mm]
           WRITE(11) BrosSN  ! Snow Buffer Layer Density  [kg/m3]
           WRITE(11) BG1sSN  ! Snow Buffer Layer Dendri/Spher.   [-]
           WRITE(11) BG2sSN  ! Snow Buffer Layer Spheri/Size     [-] [0.0001 m]
           WRITE(11) SWaSNo  ! Snow Surficial Water              [m]
           WRITE(11) zWE0SN  ! Initial      Snow Thickness [mm w.e.]
           WRITE(11) zWE_SN  ! Current      Snow Thickness [mm w.e.]
           WRITE(11) zWEcSN  ! Non-erodible Snow Thickness [mm w.e.]
           WRITE(11) SaltSN  ! u*_th                           [m/s]
           WRITE(11) SLussl  ! u*_s*                     [kg/kg.m/s]
           WRITE(11) blowSN  ! NEW  MAX Erosion              [kg/m2]
           WRITE(11) WEq_SN  ! Added Snow Amount            [m w.e.]
           WRITE(11) SLn_z0  ! Z0_momentum    (instantaneous)    [m]
           WRITE(11) SLn_r0  ! Z0_scalar      (instantaneous)    [m]
c #BS      WRITE(11) SLn_b0  ! Z0_erosion     (instantaneous)    [m]
           WRITE(11) ua_0BS  !  Wind ,  x-component (t-dt)     [m/s]
           WRITE(11) va_0BS  !  Wind ,  y-component (t-dt)     [m/s]
           WRITE(11) VVs_BS  ! (wind ,  Sastrugi) Relevance    [m/s]
           WRITE(11) RRs_BS  ! (wind ,  Sastrugi) Counter        [-]
           WRITE(11) DDs_BS  ! (wind ,  Sastrugi) Angle         [dg]
c #SZ      WRITE(11) Z0SaBS  ! Z0      (Sastrugi Height)         [m]
           WRITE(11) wes_IB  ! Snow/ice Sublimation     [mm w.e.]
           WRITE(11) wee_IB  ! Evapotranspiration       [mm w.e.]
           WRITE(11) wem_IB  ! Snow/ice Melting         [mm w.e.]
           WRITE(11) wer_IB  ! Snow/ice Refreezing      [mm w.e.]
           WRITE(11) wei0IB  ! Bottom Snow/ice added    [mm w.e.]
           WRITE(11) weu_IB  ! Run-off                  [mm w.e.]
           WRITE(11) zn0IB   ! Initial Snow Height            [m]
           WRITE(11) mb0IB   ! Initial Mass Balance        [mmWE]
           if (mw .eq. 5) then
           WRITE(11) gradTM !*CL* Local temp. gradient  [C/m]
           WRITE(11) gradQM !*CL* Local hum. gradient   [g/kg/m]
           endif
cXF
C #SN    END IF
 
        CLOSE(unit=11)
 
      END IF
 
      return
      end
