 
 
      subroutine cva_filtering(wrk1,level1)
 
 
C +------------------------------------------------------------------------+
C | MAR CONVECTION                                         11-01-2016  MAR |
C |                                                                        |
C |   SubRoutine CVA_filtering                                             |
C |                                                                        |
C +------------------------------------------------------------------------+
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_DY.inc'
 
      real,parameter :: weight = 8 ! weight for the central pixel
 
      integer l,kk,level1,level2
      real   wrk1(mx,my,mz),uu,vv
      real   wrk2(mx,my),wrk3(mx,my),w
 
      level2=min(max(1,level1),mz)
 
!$OMP PARALLEL DO
!$OMP.private(i,j,k,kk,l,w,uu,vv,wrk3,wrk2)
      DO kk= 1,level2
 
        DO j = 4,my-3
        DO i = 4,mx-3
 
        wrk3(i,j)=0
        wrk2(i,j)=0
 
        do k=-1,1
        do l=-1,1
                                     w=1
         if(l==0)                    w=2
         if(k==0)                    w=2
         if(l==0.and.k==0)           w=weight
 
         if(i+k>mx-3.or.i+k<4)     w=0
         if(j+l>my-3.or.j+l<4)     w=0
 
         wrk3(i,j)=wrk3(i,j)                 +w
         wrk2(i,j)=wrk2(i,j)+wrk1(i+k,j+l,kk)*w
        enddo
        enddo
 
        wrk2(i,j)=wrk2(i,j)/wrk3(i,j)
       ENDDO
       ENDDO
 
        DO j = 4,my-3
        DO i = 4,mx-3
        wrk1(i,j,kk)=wrk2(i,j)
       ENDDO
       ENDDO
 
      ENDDO
!$OMP END PARALLEL DO
 
 
      end
