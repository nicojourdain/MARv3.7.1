      function GlaCVA(T)
C +
C +------------------------------------------------------------------------+
C | MAR CONVECTION                                         25-11-1999  MAR |
C |                                                                        |
C |   Function GlaCVA computes Glaciation Degree in the Convective Cloud   |
C |                                                                        |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +--Global Variables
C +  ================
C +
      include 'MARphy.inc'
C +
      real     GlaCVA
      real     T
C +
C +
C +--Glaciation Degree
C +  =================
C =
               GlaCVA = (268. -T)/ 20.
C +...         GlaCVA = (T2     -T)/(T2-T1)
C +           (Kain and Fritsch 1990, JAS p.2788)
C +
               GlaCVA = max(GlaCVA,zero)
               GlaCVA = min(GlaCVA,unun)
C +
      return
      end
