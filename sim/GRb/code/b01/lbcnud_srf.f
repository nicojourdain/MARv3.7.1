 
 
      subroutine LBCnud_srf
C +
C +------------------------------------------------------------------------+
C | MAR, Routine LBCnud_srf                                 4-06-2002  MAR |
C |              LBCnud_srf includes the Surface Boundary Conditions       |
C |              corresponding to Surface Variables                        |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT : sst_LB: Surface Temperature LBC                              |
C |   ^^^^^^^                                                              |
C |                                                                        |
C |   OUTPUT: tsrfSL: Surface Temperature                                  |
C |   ^^^^^^^                                                              |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +--Global Variables
C +  ================
C +
      include 'MARCTR.inc'
      include 'MARphy.inc'
C +
      include 'MARdim.inc'
      include 'MARgrd.inc'
C +
      include 'MAR_LB.inc'
C +
      include 'MAR_SL.inc'
C +
C +
C +--Local  Variables
C +  ================
C +
      logical     MAR_SI
C +
C +
C +--Initialization
C +  ==============
C +
                  MAR_SI=.false.
      IF(VSISVAT) MAR_SI=.true.
C +
C +
C +--LBC: New Surface Temperatures
C +  =============================
C +
      IF (.NOT.polmod .and. .not. MAR_SI)                         THEN
        DO j=1,my
        DO i=1,mx
          IF   (isolSL(i,j).le.2      )                           THEN
                tsrfSL(i,j,1) = sst_LB(i,j)
 
C +---1. Open Water
C +   ~~~~~~~~~~~~~
            IF (sst_LB(i,j).gt. Tfr_LB)                           THEN
                isolSL(i,j)   = 1
                 d1_SL(i,j)   = 2.09d+8
                albeSL(i,j)   = 0.10
                eps0SL(i,j)   = 0.97
                 SL_z0(i,j,1) =          zs_SL
                 SL_r0(i,j,1) = 0.1     *zs_SL
                 ch0SL(i,j)   = 0.00132
                rsurSL(i,j)   = 0.0
 
C +---2. Sea Ice
C +   ~~~~~~~~~~
            ELSE
                isolSL(i,j)   = 2
                 d1_SL(i,j)   = 1.05d+5
                albeSL(i,j)   = 0.70d00
                eps0SL(i,j)   = 0.97d00
                 SL_z0(i,j,1) =          zn_SL
                 SL_r0(i,j,1) = 0.1     *zn_SL
                 ch0SL(i,j)   = 0.0021
C +...          (Kondo and Yamazaki, 1990, JAM 29, p.376)
                rsurSL(i,j)   = 0.0
            END IF
          END IF
        END DO
        END DO
      END IF
C +
      return
      end
