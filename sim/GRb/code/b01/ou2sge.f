 
 
 
      function ou2sGE (yr,mo,da,hh,mm,ss)
 
C +------------------------------------------------------------------------+
C | MAR INPUT                                            31-11-2012-XF MAR |
C +------------------------------------------------------------------------+
 
      IMPLICIT NONE
 
      include 'MARdim.inc'
      include 'MAR_GE.inc'
 
      integer   y,yr,mo,da,hh,mm,ss
 
      ou2sGE=0
      do y=iyr0GE,yr-1
       ou2sGE=ou2sGE+365+njmbGE(2)*max(0,1-mod(y,4))
      enddo
 
      ou2sGE=ou2sGE+njyrGE(mo)+njybGE(mo)*max(0,1-mod(yr,4))+da-1
 
      ou2sGE=ou2sGE*24   + hh
      ou2sGE=ou2sGE*3600 + mm*60 + ss
 
      return
      end
