 
 
      subroutine INIglf(ihamr_glf,nhamr_glf,newglfINI)
C +
C +------------------------------------------------------------------------+
C | MAR INPUT      SVAT                                     7-06-2002  MAR |
C |   SubRoutine INIglf is used to initialize MAR Green Leaf Fractions     |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:  ihamr_glf: Time Digital Filter Status                        |
C |   ^^^^^   nhamr_glf: Time Digital Filter Set Up                        |
C |                                                                        |
C |   OUTPUT: newglfINI: (0,1) ==> (NO new glf , new glf)                  |
C |   ^^^^^^^                                                              |
C |                                                                        |
C |   OUTPUT: alaiTV: Current                     Leaf  Area Index    (LAI)|
C |   ^^^^^^^ LAI1VB: Previous Nesting Time Step  Leaf  Area Index         |
C |           LAI2VB: Next     Nesting Time Step  Leaf  Area Index         |
C |           glf_TV: Current                     Green Leaf Fraction (GLF)|
C |           glf1VB: Previous Nesting Time Step  Green Leaf Fraction      |
C |           glf2VB: Next     Nesting Time Step  Green Leaf Fraction      |
C |           tim1VB,tim2VB:   Times  n, n+1  of  LAI and GLF              |
C |                                                                        |
C |   CAUTION: It is assumed that tim1VB and tim2VB do not change when the |
C |   ^^^^^^^^ Variables are reassigned after the dynamical Initialization |
C |            (Reassignation => itexpe := nham => timar := timar-nham*dt) |
C |                                                                        |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +--Global Variables
C +  ================
C +
      include 'MARCTR.inc'
      include 'MARphy.inc'
C +
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
C +
      include 'MAR_SL.inc'
      include 'MAR_SV.inc'
      include 'MAR_TV.inc'
      include 'MAR_VB.inc'
C +
      integer  ihamr_glf,nhamr_glf
      integer  newglfINI
C +
C +
C +--Local  Variables
C +  ================
C +
cXF
      integer*8  itisva,iv_glf
! int*8 is needed for making future projections!!!!
      real       rate
C +
C +
C +--Current Time
C +  ============
C +
          itisva=ou2sGE(iyrrGE,mmarGE,jdarGE,jhurGE,minuGE,jsecGE)
c #HF.           + (ihamr_glf+nhamr_glf)                           * idt
C +
C +
C +--Reinitialization of the Leaf Area Index and the Green Leaf Fraction
C +  -------------------------------------------------------------------
C +
      IF (iterun.eq.0)                                            THEN
          jdh_VB = 1
          iyr_VB = iyrrGE
          mma_VB = mmarGE
          jda_VB = jdarGE
          jhu_VB = jhurGE
          tim1VB = itisva
          tim2VB = itisva
       DO iv_glf=1,nvx
         DO j=1,my
         DO i=1,mx
c #LN      LAI1VB(i,j,iv_glf) = alaiTV(i,j,iv_glf)
c #LN      LAI2VB(i,j,iv_glf) = alaiTV(i,j,iv_glf)
           glf1VB(i,j,iv_glf) = glf_TV(i,j,iv_glf)
           glf2VB(i,j,iv_glf) = glf_TV(i,j,iv_glf)
         END DO
         END DO
       END DO
C +
      END IF
C +
C +
C +--New VBC
C +  =======
C +
      IF (itisva.gt.   tim2VB)                                    THEN
C +
          tim1VB =     tim2VB
C +
          write(6,6001)jda_VB,labmGE(mma_VB),iyr_VB,
     .                 jhu_VB,                      tim1VB,
     .                 jdarGE,labmGE(mmarGE),iyrrGE,
     .                 jhurGE,minuGE,        jsecGE,itisva
 6001     format(/, '  1st VBC /',i3,'-',a3,'-',i4,i3,' ',2x,'/',2x,
     .              '   t =',i12,'s A.P.',
     .           /, '  Current /',i3,'-',a3,'-',i4,i3,':',i2,':',i2,
     .              '   t =',i12)
C +
       IF (jdh_VB.eq.0)jdh_VB = -1
       open (unit=11,status='old',form='unformatted',file='MARglf.DAT')
       rewind     11
 11    CONTINUE
       IF (jdh_VB.le.0)                                       GO TO 10
C +
C +
C +--VBC at nesting time step n
C +  --------------------------
C +
       DO iv_glf=1,nvx
         DO j=1,my
         DO i=1,mx
c #LN      LAI1VB(i,j,iv_glf) = LAI2VB(i,j,iv_glf)
           glf1VB(i,j,iv_glf) = glf2VB(i,j,iv_glf)
c #LN      LAI2VB(i,j,iv_glf) = 0.d0
           glf2VB(i,j,iv_glf) = 0.d0
         END DO
         END DO
       END DO
C +
C +
C +--VBC at nesting time step n+1
C +  ----------------------------
C +
       read      (11) iyr_VB,mma_VB,jda_VB,jhu_VB,jdh_VB
       read      (11) glf2VB
c #LN  read      (11) LAI2VB
C +
       tim2VB=ou2sGE(iyr_VB,mma_VB,jda_VB,jhu_VB,0,0)
C +
       IF(itisva.gt.tim2VB)                                   GO TO 11
C +
          write(6,6002)jda_VB,labmGE(mma_VB),iyr_VB,
     .                 jhu_VB,jdh_VB,               tim2VB
 6002     format(   '  2nd VBC /',i3,'-',a3,'-',i4,i3,' ',2x,'/(',i1,
     .              ')  t =',i12,/,1x)
C +
 10    CONTINUE
       close(unit=11)
C +
      ELSE
c #WR     write(6,6003)jdarGE,labmGE(mmarGE),iyrrGE,
c #WR.                 jhurGE,minuGE,        jsecGE,itisva
 6003     format(   '  Current /',i3,'-',a3,'-',i4,i3,':',i2,':',i2,
     .              '   t =',i12,'s A.P.')
      END IF
C +
C +
C +--Time Interpolation
C +  ==================
C +
      IF            (itisva.le.tim2VB  .and.   tim1VB.lt.tim2VB)  THEN
C +
        rate = float(itisva  - tim1VB) / float(tim2VB  - tim1VB)
        DO iv_glf=1,nvx
          DO j=1,my
          DO i=1,mx
c #LN       alaiTV(i,j,iv_glf)=LAI1VB(i,j,iv_glf) +
c #LN.     (LAI2VB(i,j,iv_glf)-LAI1VB(i,j,iv_glf))*rate
            glf_TV(i,j,iv_glf)=glf1VB(i,j,iv_glf) +
     .     (glf2VB(i,j,iv_glf)-glf1VB(i,j,iv_glf))*rate
          END DO
          END DO
        END DO
C +
        newglfINI = 1
C +
      ELSE
        newglfINI = 0
      END IF
C +
      return
      end
