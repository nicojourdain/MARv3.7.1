 
 
      subroutine DYNadv_verq
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS   SLOW                                    18-09-2001  MAR |
C |   SubRoutine DYNadv_verq includes the Vertical  Advection Contribution |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT   (via common block)                                           |
C |   ^^^^^      iterun,            Run Iteration  Counter                 |
C |                qvDY,            Air Vap. Water Values / Time Step n    |
C |              qvapSL,            SBC Vap. Water Values / Time Step n    |
C |                                                                        |
C |   OUTPUT  (via common block)                                           |
C |   ^^^^^^       qvDY,            Air Vap. Water Values / Time Step n+1  |
C |                                                                        |
C |   METHOD:  Unstaggered Grid: 1st Accurate in Space Upstream Scheme     |
C |   ^^^^^^^^   Staggered Grid: 2nd Accurate in Space                     |
C |                                                                        |
C | # OPTIONS: #VA: Vertical Average preferred in Centered Conserv Scheme  |
C | # ^^^^^^^^ #NS: NO   Slip Surface BC used  in Centered Conserv Scheme  |
C |                                                                        |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +--Global Variables
C +  ================
C +
      include 'MARCTR.inc'
      include 'MARphy.inc'
C +
      include 'MARdim.inc'
      include 'MARgrd.inc'
C +
      include 'MAR_DY.inc'
c #cA include 'MAR_CA.inc'
      include 'MAR_SL.inc'
C +
      include 'MAR_WK.inc'
C +
c #WA integer                  nadvrd
c #WA common  /DYNadv_ver_loc/ nadvrd
C +
      logical  centrL
c #ZU logical  adv3rd
c #ZU real     gat(mx,my,mz),ga0(mx,my)
C +
C +
C +--Local  Variables
C +  ================
C +
      integer  itimax,ntimax
      real     cflmax,cflsig,faccfl,dsgm,qv_0
      real     old__u,old__v,old__t,qw_0,qr_0
C +
C +
C +--DATA
C +  ====
C +
c #ZU data adv3rd/ .true./
C +
      data centrL/ .true./
c #UP      centrL=.false.
C +
C +
C +--Slip condition for Mountain Wave Experiments
C +  ============================================
C +
c #OM   DO       j=jp11,my1
c #OM   DO       i=ip11,mx1
c #OM     psigDY(i,j,mz)=0.0
c #OM   END DO
c #OM   END DO
C +
C +
C +--First and Second Order Schemes
C +  ==============================
C +
c #ZU IF (.not.adv3rd)                                            THEN
C +
C +
C +--Courant Number
C +  --------------
C +
           cflmax     = 0.0
C +
C +--Centered second Order Scheme on a       staggered Grid
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
       IF (centrL.and.staggr)                                     THEN
C +
c #WA     write(6,6001)iterun
 6001     format(i6,' 6001       centrL .and.       staggr /CFL Number')
C +
          DO       k=1,mz
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz7(i,j,k)=dt*psigDY(i,j,k)
     .                     /(pstDYn(i,j)*dsigm1(k)*2.0)
            cflsig     = abs(WKxyz7(i,j,k)+WKxyz7(i,j,k))
            cflmax     = max(cflsig,cflmax)
          END DO
          END DO
          END DO
C +
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz8(i,j,1)=   0.0
          END DO
          END DO
C +
          DO       k=kp1(1),mz
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz8(i,j,k)=dt*psigDY(i,j,km1(k))
     .                     /(pstDYn(i,j)*dsigm1(k)*2.0)
            cflsig     = abs(WKxyz8(i,j,k)+WKxyz8(i,j,k))
            cflmax     = max(cflsig,cflmax)
          END DO
          END DO
          END DO
C +
       ELSE
C +
C +--Upstream first  Order Scheme on a       staggered Grid
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        IF           (staggr)                                     THEN
C +
c #WA     write(6,6002)iterun
 6002     format(i6,' 6002 .not. centrL .and.       staggr /Wind Speed')
C +
          DO       k=kp1(1),mz
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz8(i,j,k)=(psigDY(i,j,k-1)*dsig_1(k-1)
     .                    +psigDY(i,j,k)  *dsig_1(k))
     .                   /(dsig_1    (k-1)+dsig_1(k))
          END DO
          END DO
          END DO
C +
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz8(i,j,1)= psigDY(i,j,1)  *dsig_1(1)
     .                   /(dsig_1    (0)  +dsig_1(1))
          END DO
          END DO
C +
C +--Upstream first  Order Scheme on a  non  staggered Grid
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ELSE
C +
c #WA     write(6,6003)iterun
 6003     format(i6,' 6003 (.not.)centrL.and. .not. staggr /Wind Speed')
C +
          DO       k=1,mz
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz8(i,j,k)= psigDY(i,j,k)
          END DO
          END DO
          END DO
C +
        END IF
C +
C +--Centered second Order Scheme on a  non  staggered Grid
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        IF(centrL)                                                THEN
C +
c #WA     write(6,6004)iterun
 6004     format(i6,' 6004        centrL.and. .not. staggr /CFL Number')
C +
          DO       k=1,mz
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz7(i,j,k)=dt*WKxyz8(i,j,k)
     .                     /(pstDYn(i,j)*dsigm1(k)*2.0)
            cflsig     = abs(WKxyz7(i,j,k))
            cflmax     = max(cflsig,cflmax)
          END DO
          END DO
          END DO
C +
C +--Upstream first  Order Scheme on a (non) staggered Grid
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ELSE
C +
c #WA     write(6,6005)iterun
 6005     format(i6,' 6005  .not. centrL.and.(.not.)staggr /CFL Number')
C +
          DO          k=    1 ,mmz1
          DO          j=jp11,my1
          DO          i=ip11,mx1
            IF(WKxyz8(i,j,k).gt.0.0)                              THEN
               WKxyz7(i,j,k)=-dt*WKxyz8(i,j,k)/(pstDYn(i,j)*dsig_1(k-1))
            ELSE
               WKxyz7(i,j,k)=-dt*WKxyz8(i,j,k)/(pstDYn(i,j)*dsig_1(k  ))
            END IF
               cflsig       =abs(WKxyz7(i,j,k))
               cflmax       =max(cflsig,cflmax)
          END DO
          END DO
          END DO
C +
                      k=  mz
          DO          j=jp11,my1
          DO          i=ip11,mx1
            IF(WKxyz8(i,j,k).gt.0.0)                              THEN
               WKxyz7(i,j,k)=-dt*WKxyz8(i,j,k)/(pstDYn(i,j)*dsig_1(k-1))
            ELSE
               WKxyz7(i,j,k)=-dt*WKxyz8(i,j,k)/(pstDYn(i,j)*dsig_1(k  ))
            END IF
               cflsig       =abs(WKxyz7(i,j,k))
               cflmax       =max(cflsig,cflmax)
          END DO
          END DO
C +
          DO          j=1,my
          DO          i=1,mx
               WKxyz7(i,j,1)= 0.0
          END DO
          END DO
C +
C +--Work Array Reset
C +  ~~~~~~~~~~~~~~~~
          DO          k=1,mz
          DO          j=1,my
          DO          i=1,mx
               WKxyz8(i,j,k)= 0.0
          END DO
          END DO
          END DO
C +
        END IF
C +
       END IF
C +
C +
C +--Set Up    of the Local Split Time Differencing
C +  ----------------------------------------------
C +
            cflmax    = 2.0 *cflmax
C +...      restricted CFL Criterion
C +
            ntimax    =       cflmax
       IF  (centrL)                                               THEN
            ntimax    = max(2,ntimax)
c #WA     write(6,6006)ntimax
 6006     format(i6,' 6006        centrL.and.(.not.)staggr /Nb Iterat.')
       ELSE
            ntimax    = max(1,ntimax)
c #WA     write(6,6007)ntimax
 6007     format(i6,' 6007  .not. centrL.and.(.not.)staggr /Nb Iterat.')
       END IF
C +
C +--Update of CFL Number
C +  ~~~~~~~~~~~~~~~~~~~~
       IF  (ntimax.gt.1)                                          THEN
            faccfl       = 1.0d+0        / ntimax
          DO       k=1,mz
          DO       j=jp11,my1
          DO       i=ip11,mx1
            WKxyz7(i,j,k)= WKxyz7(i,j,k) * faccfl
            WKxyz8(i,j,k)= WKxyz8(i,j,k) * faccfl
          END DO
          END DO
          END DO
       END IF
C +
C +--OUTPUT for Verification
C +  ~~~~~~~~~~~~~~~~~~~~~~~
c #WA  nadvrd      = nadvrd                 + 1
c #WA  write(6,6000) nadvrd,cflmax,ntimax
 6000  format(i6,' CFLmax ',3x,' ',3x,'  =',f7.4,
     .        6x,' ntimax ',8x,  '   =',i4)
C +
C +
C +--2nd Order Centered Energy conserving:  Local Split Time Differencing
C +  --------- (Haltiner & Williams 1980 7.2.2, (7-47b) p.220) ----------
C +            -----------------------------------------------
C +
       IF   (centrL)                                              THEN
C +
        IF  (staggr)                                              THEN
C +
c #WA     write(6,6008)
 6008     format(6x,' 6008        centrL.and.       staggr /A Contrib.')
C +
         DO  itimax=    1,ntimax
C +
C +--First        internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          IF(itimax.eq. 1)                                        THEN
C +
            DO       j=jp11,my1
C +
C +--Vertical Differences
C +
                      k=    1
                dsgm         =             2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzx(i,k)   =   qvDY(i,j,k)
C +
c #VA           WKxzx(i,k)   =(  qvDY(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +  qvDY(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
C +
              DO      i=ip11,mx1
                  qv_0       =                  WKxzx(i,  k)
                WKxzc(i,k)   =( WKxzx(i,  k)  -  qv_0)
              END DO
C +
              DO      k=kp1(1),mmz1
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzx(i,k)   =   qvDY(i,j,k)
C +
c #VA           WKxzx(i,k)   =(  qvDY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +  qvDY(i,j,k  )*dsigm1(k  ) *2.0d+0
c #VA.                        +  qvDY(i,j,k+1)*dsigm1(k+1)        )/dsgm
              END DO
              END DO
C +
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
              END DO
C +
                      k=       mmz
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)
              DO      i=ip11,mx1
                WKxzx(i,k)   =   qvDY(i,j,k)
C +
c #VA           WKxzx(i,k)   =(  qvDY(i,j,k-1)*dsigm1(k-1)
c #VA.                        +  qvDY(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
              END DO
C +
              DO      i=ip11,mx1
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
C +
                      k=       mzz
              DO      i=ip11,mx1
                WKxy3(i,j)   = 0.0
c #NS           WKxy3(i,j)   =(qvapSL(i,j)    - WKxzx(i,  k-1))
              END DO
C +
C +--Advection Contribution
C +
              DO       k=    1 ,mmz1
              DO       i=ip11,mx1
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxzc(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxzc(i,k)
                WKxyz3(i,j,k)=   qvDY(i,j,k)  -WKxzd(i,k)
                WKxyz6(i,j,k)=   qvDY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
              END DO
              END DO
C +
                       k=       mmz
              DO       i=ip11,mx1
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy3(i,j)
     .                        +WKxyz8(i,j,k)  *WKxzc(i,k)
                WKxyz3(i,j,k)=   qvDY(i,j,k)  -WKxzd(i,k)
                WKxyz6(i,j,k)=   qvDY(i,j,k) -(WKxzd(i,k)+WKxzd(i,k))
              END DO
C +
            END DO
C +
C +--Intermediary internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          ELSE IF (itimax.lt.ntimax)                              THEN
C +
C +--Vertical Differences
C +
            DO       j=jp11,my1
C +
                      k=    1
                dsgm         =             2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzx(i,k)   = WKxyz6(i,j,k)
C +
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz6(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
C +
              DO      i=ip11,mx1
                  qv_0       =                  WKxzx(i,  k)
                WKxzc(i,k)   =( WKxzx(i,  k)  -  qv_0)
              END DO
C +
              DO      k=kp1(1),mmz1
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzx(i,k)   = WKxyz6(i,j,k)
C +
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz6(i,j,k  )*dsigm1(k  ) *2.0d+0
c #VA.                        +WKxyz6(i,j,k+1)*dsigm1(k+1)        )/dsgm
              END DO
              END DO
C +
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
              END DO
C +
                      k=       mmz
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)
              DO      i=ip11,mx1
                WKxzx(i,k)   = WKxyz6(i,j,k)
C +
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz6(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
              END DO
C +
              DO      i=ip11,mx1
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
C +
                      k=       mzz
              DO      i=ip11,mx1
                WKxy3(i,j)   = 0.0d+0
c #NS           WKxy3(i,j)   =(qvapSL(i,j)    - WKxzx(i,  k-1))
              END DO
C +
C +--Advection Contribution
C +
              DO      k=    1 ,mmz1
              DO      i=ip11,mx1
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxza(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxza(i,k)
                old__u       = WKxyz1(i,j,k)
                WKxyz1(i,j,k)= WKxyz4(i,j,k)
                WKxyz4(i,j,k)= old__u        -(WKxzd(i,k)+WKxzd(i,k))
C +
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxzb(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxzb(i,k)
                old__v       = WKxyz2(i,j,k)
                WKxyz2(i,j,k)= WKxyz5(i,j,k)
                WKxyz5(i,j,k)= old__v        -(WKxzd(i,k)+WKxzd(i,k))
C +
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxzc(i,k+1)
     .                        +WKxyz8(i,j,k)  *WKxzc(i,k)
                old__t       = WKxyz3(i,j,k)
                WKxyz3(i,j,k)= WKxyz6(i,j,k)
                WKxyz6(i,j,k)= old__t        -(WKxzd(i,k)+WKxzd(i,k))
              END DO
              END DO
C +
                      k=       mmz
              DO      i=ip11,mx1
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy1(i,j)
     .                        +WKxyz8(i,j,k)  *WKxza(i,k)
                old__u       = WKxyz1(i,j,k)
                WKxyz1(i,j,k)= WKxyz4(i,j,k)
                WKxyz4(i,j,k)= old__u        -(WKxzd(i,k)+WKxzd(i,k))
C +
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy2(i,j)
     .                        +WKxyz8(i,j,k)  *WKxzb(i,k)
                old__v       = WKxyz2(i,j,k)
                WKxyz2(i,j,k)= WKxyz5(i,j,k)
                WKxyz5(i,j,k)= old__v        -(WKxzd(i,k)+WKxzd(i,k))
C +
                WKxzd( i,k)  = WKxyz7(i,j,k)  *WKxy3(i,j)
     .                        +WKxyz8(i,j,k)  *WKxzc(i,k)
                old__t       = WKxyz3(i,j,k)
                WKxyz3(i,j,k)= WKxyz6(i,j,k)
                WKxyz6(i,j,k)= old__t        -(WKxzd(i,k)+WKxzd(i,k))
              END DO
C +
            END DO
C +
C +--Last         internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          ELSE
C +
            DO       j=jp11,my1
C +
C +--Vertical Differences
C +
                      k=    1
                dsgm         =             2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzx(i,k)   = WKxyz6(i,j,k)
C +
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz6(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
C +
              DO      i=ip11,mx1
                  qv_0       =                  WKxzx(i,  k)
                WKxzc(i,k)   =( WKxzx(i,  k)  -  qv_0)
              END DO
C +
              DO      k=kp1(1),mmz1
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)+dsigm1(k+1)
              DO      i=ip11,mx1
                WKxzx(i,k)   = WKxyz6(i,j,k)
C +
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz6(i,j,k  )*dsigm1(k  ) *2.0
c #VA.                        +WKxyz6(i,j,k+1)*dsigm1(k+1)     )/dsgm
              END DO
              END DO
C +
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
              END DO
C +
                      k=       mmz
                dsgm         = dsigm1(k-1)+2.0*dsigm1(k)
              DO      i=ip11,mx1
                WKxzx(i,k)   = WKxyz6(i,j,k)
C +
c #VA           WKxzx(i,k)   =(WKxyz6(i,j,k-1)*dsigm1(k-1)
c #VA.                        +WKxyz6(i,j,k  )*dsigm1(k  ) *2.0)/dsgm
              END DO
C +
              DO      i=ip11,mx1
                WKxzc(i,k)   =( WKxzx(i,  k)  - WKxzx(i,  k-1))
              END DO
C +
                      k=       mzz
              DO      i=ip11,mx1
                WKxy3(i,j)   = 0.0d+0
c #NS           WKxy3(i,j)   =(qvapSL(i,j)    - WKxzx(i,  k-1))
              END DO
C +
C +--Wat.Vapr.Advect.avoids double Counting in case of convective Adjustment
C +
              DO             k=    1 ,mmz1
              DO           i=ip11,mx1
c #cA           IF (adj_CA(i,j).eq.0)                             THEN
                      qvDY(i,j,k) = WKxyz3(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxzc(i,k+1)
     .                             +WKxyz8(i,j,k)  *WKxzc(i,k  ))
c #cA           END IF
              END DO
              END DO
C +
                             k=       mmz
              DO           i=ip11,mx1
c #cA           IF (adj_CA(i,j).eq.0)                             THEN
                      qvDY(i,j,k) = WKxyz3(i,j,k)
     .                            -(WKxyz7(i,j,k)  *WKxy3(i,j)
     .                             +WKxyz8(i,j,k)  *WKxzc(i,k))
c #cA           END IF
              END DO
C +
            END DO
C +
          END IF
C +
C +
C +--End of the                             Local Split Time Differencing
C +  --------------------------------------------------------------------
C +
         END DO
C +
C +
C +--2nd Order Centered Leap-Frog Backward: Local Split Time Differencing
C +  --------------------------------------------------------------------
C +
        ELSE
C +
c #WA     write(6,6009)
 6009     format(6x,' 6009        centrL.and. .not. staggr /A Contrib.')
C +
         DO  itimax=    1,ntimax
C +
C +--First        internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          IF(itimax.eq. 1)                                        THEN
C +
            DO       j=jp11,my1
C +
C +--Advection Increment
C +
                     k=    1
              DO     i=ip11,mx1
                  qv_0       =                   qvDY(i,j,k)
                WKxzc(i,k)   =(  qvDY(i,j,k+1)-  qvDY(i,j,k  ))
     .                        *WKxyz7(i,j,k)
              END DO
C +
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxzc(i,k)   =(  qvDY(i,j,k+1)-  qvDY(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
              END DO
C +
                      k=       mmz
              DO      i=ip11,mx1
                WKxzc(i,k)   =(qvapSL(i,j)    -  qvDY(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
C +
C +--Advection Contribution
C +
              DO       k=    1 ,mmz
              DO       i=ip11,mx1
                WKxyz3(i,j,k)=   qvDY(i,j,k)  -WKxzc(i,k)
                WKxyz6(i,j,k)=   qvDY(i,j,k) -(WKxzc(i,k)+WKxzc(i,k))
              END DO
              END DO
            END DO
C +
C +--Intermediary internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          ELSE IF (itimax.lt.ntimax)                              THEN
C +
C +--Advection Increment
C +
            DO       j=jp11,my1
C +
                     k=    1
              DO     i=ip11,mx1
                  qw_0       =                 WKxyz4(i,j,k)
                  qr_0       =                 WKxyz5(i,j,k)
                  qv_0       =                 WKxyz6(i,j,k)
C +
                WKxza(i,k)   =(WKxyz4(i,j,k+1)-  qw_0)
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =(WKxyz5(i,j,k+1)-  qr_0)
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(WKxyz6(i,j,k+1)-  qv_0)
     .                        *WKxyz7(i,j,k)
              END DO
C +
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =(WKxyz4(i,j,k+1)-WKxyz4(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =(WKxyz5(i,j,k+1)-WKxyz5(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(WKxyz6(i,j,k+1)-WKxyz6(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
              END DO
C +
                      k=       mmz
              DO      i=ip11,mx1
                WKxza(i,k)   =                -WKxyz4(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =                -WKxyz5(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(qvapSL(i,j)    -WKxyz6(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
C +
C +--Advection Contribution
C +
              DO      k=    1 ,mmz
              DO      i=ip11,mx1
                old__u       = WKxyz1(i,j,k)
                WKxyz1(i,j,k)= WKxyz4(i,j,k)
                WKxyz4(i,j,k)= old__u        -(WKxza(i,k)+WKxza(i,k))
                old__v       = WKxyz2(i,j,k)
                WKxyz2(i,j,k)= WKxyz5(i,j,k)
                WKxyz5(i,j,k)= old__v        -(WKxzb(i,k)+WKxzb(i,k))
                old__t       = WKxyz3(i,j,k)
                WKxyz3(i,j,k)= WKxyz6(i,j,k)
                WKxyz6(i,j,k)= old__t        -(WKxzc(i,k)+WKxzc(i,k))
              END DO
              END DO
C +
            END DO
C +
C +--Last         internal Time Step
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          ELSE
C +
            DO       j=jp11,my1
C +
C +--Advection Increment
C +
                      k=    1
              DO      i=ip11,mx1
                  qw_0       =                 WKxyz4(i,j,k)
                  qr_0       =                 WKxyz5(i,j,k)
                  qv_0       =                 WKxyz6(i,j,k)
C +
                WKxza(i,k)   =(WKxyz4(i,j,k+1)-  qw_0)
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =(WKxyz5(i,j,k+1)-  qr_0)
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(WKxyz6(i,j,k+1)-  qv_0)
     .                        *WKxyz7(i,j,k)
              END DO
C +
              DO      k=kp1(1),mmz1
              DO      i=ip11,mx1
                WKxza(i,k)   =(WKxyz4(i,j,k+1)-WKxyz4(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =(WKxyz5(i,j,k+1)-WKxyz5(i,j,k-1))
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(WKxyz6(i,j,k+1)-WKxyz6(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
              END DO
C +
                      k=       mmz
              DO      i=ip11,mx1
                WKxza(i,k)   =                -WKxyz4(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzb(i,k)   =                -WKxyz5(i,j,k-1)
     .                        *WKxyz7(i,j,k)
                WKxzc(i,k)   =(qvapSL(i,j)    -WKxyz6(i,j,k-1))
     .                        *WKxyz7(i,j,k)
              END DO
C +
C +--Wat.Vapr.Advect.avoids double Counting in case of convective Adjustment
C +
              DO             k=    1 ,mmz
                DO           i=ip11,mx1
c #cA             IF (adj_CA(i,j).eq.0)                           THEN
                        qvDY(i,j,k) = WKxyz3(i,j,k) -WKxzc(i,k)
c #cA             END IF
                END DO
              END DO
C +
            END DO
C +
          END IF
C +
C +
C +--End of the                             Local Split Time Differencing
C +  --------------------------------------------------------------------
C +
         END DO
C +
        END IF
C +
C +
C +--First  Order Upstream Scheme:          Local Split Time Differencing
C +  --------------------------------------------------------------------
C +
       ELSE
C +
c #WA     write(6,6010)
 6010     format(6x,' 6010  .not. centrL.and.(.not.)staggr /A Contrib.')
C +
         DO  itimax=    1,ntimax
C +
C +--Auxiliary Variables
C +  ~~~~~~~~~~~~~~~~~~~
c #WA     write(6,6011)itimax,WKxyz1(imez,jmez,mz1),WKxyz1(imez,jmez,mz)
c #WA.                       ,  qvDY(imez,jmez,mz1),  qvDY(imez,jmez,mz)
 6011     format(6x,' 6011  .not. centrL.and.(.not.)staggr /A Contrib.',
     .                        4f9.6)
C +
             DO       k=1,mz
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz3(i,j,k)=   qvDY(i,j,k)
             END DO
             END DO
             END DO
C +
C +--Vertical Differences
C +  ~~~~~~~~~~~~~~~~~~~~
                      k=1
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz4(i,j,k) = 0.0d+0
               WKxyz5(i,j,k) = 0.0d+0
               WKxyz6(i,j,k) = 0.0d+0
             END DO
             END DO
C +
             DO       k=kp1(1),mz
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz4(i,j,k) = WKxyz1(i,j,k)-WKxyz1(i,j,k-1)
               WKxyz5(i,j,k) = WKxyz2(i,j,k)-WKxyz2(i,j,k-1)
               WKxyz6(i,j,k) = WKxyz3(i,j,k)-WKxyz3(i,j,k-1)
             END DO
             END DO
             END DO
C +
                      k=  mzz
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxy1 (i,j)   =              -WKxyz1(i,j,k-1)
               WKxy2 (i,j)   =              -WKxyz2(i,j,k-1)
               WKxy3 (i,j)   = qvapSL(i,j)  -WKxyz3(i,j,k-1)
             END DO
             END DO
C +
C +--Advection Contribution
C +  ~~~~~~~~~~~~~~~~~~~~~~
             DO       k=    1 ,mmz1
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz3(i,j,k)=  qvDY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz6(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxyz6(i,j,k+1)
             END DO
             END DO
             END DO
C +
                      k=mz
             DO       j=jp11,my1
             DO       i=ip11,mx1
               WKxyz3(i,j,k)=  qvDY(i,j,k)
     .             +min(zero,WKxyz7(i,j,k))*WKxyz6(i,j,k)
     .             +max(zero,WKxyz7(i,j,k))*WKxy3( i,j)
             END DO
             END DO
C +
C +--Wat.Vapr.Update avoids double Counting in case of convective Adjustment
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
             DO             k=1,mz
             DO             j=jp11,my1
               DO           i=ip11,mx1
c #cA            IF (adj_CA(i,j).eq.0)                            THEN
                       qvDY(i,j,k) = WKxyz3(i,j,k)
c #cA            END IF
               END DO
             END DO
             END DO
C +
C +
C +--End of the                             Local Split Time Differencing
C +  --------------------------------------------------------------------
C +
c #WA     write(6,6012)itimax,WKxyz1(imez,jmez,mz1),WKxyz1(imez,jmez,mz)
c #WA.                       ,  qvDY(imez,jmez,mz1),  qvDY(imez,jmez,mz)
 6012     format(6x,' 6012  .not. centrL.and.(.not.)staggr /A Contrib.',
     .                        4f9.6)
         END DO
C +
       END IF
C +
C +
C +--Work Arrays Reset
C +  -----------------
C +
        DO       j=1,my
        DO       i=1,mx
          WKxy1( i,j)   = 0.0
          WKxy2( i,j)   = 0.0
          WKxy3( i,j)   = 0.0
        END DO
        END DO
C +
        DO       k=1,mz
        DO       i=1,mx
          WKxza( i,  k) = 0.0
          WKxzb( i,  k) = 0.0
          WKxzc( i,  k) = 0.0
          WKxzd( i,  k) = 0.0
        END DO
        END DO
C +
        DO       k=1,mz
        DO       j=1,my
        DO       i=1,mx
          WKxyz1(i,j,k) = 0.0
          WKxyz2(i,j,k) = 0.0
          WKxyz3(i,j,k) = 0.0
          WKxyz4(i,j,k) = 0.0
          WKxyz5(i,j,k) = 0.0
          WKxyz6(i,j,k) = 0.0
          WKxyz7(i,j,k) = 0.0
          WKxyz8(i,j,k) = 0.0
        END DO
        END DO
        END DO
C +
C +
C +--Third Order Vertical Scheme
C +  ===========================
C +
c #ZU ELSE
C +
c #ZU  DO  j=jp11,my1
c #ZU  DO  i=ip11,mx1
c #ZU  ga0(i,j)      = qvapSL(i,j)
c #ZO  ga0(i,j)      =   qvDY(i,j,mz)
c #ZU  END DO
c #ZU  END DO
C +
c #ZU  DO  k=1,mz
c #ZU  DO  j=jp11,my1
c #ZU  DO  i=ip11,mx1
c #ZU  gat(i,j,k)    =   qvDY(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
C +
C +    ****************
c #ZU  call DYNadv_cubv(gat,ga0)
C +    ****************
C +
c #ZU  DO     k=1,mz
c #ZU  DO     j=jp11,my1
c #ZU  DO     i=ip11,mx1
c #ZU    qvDY(i,j,k) = gat(i,j,k)
c #ZU  END DO
c #ZU  END DO
c #ZU  END DO
C +
c #ZU end if
C +
      return
      end
