 
 
      subroutine CVAgen_MNH
 
C +------------------------------------------------------------------------+
C | MAR CONVECTION                                     Mon  7-06-2010  MAR |
C |   SubRoutine CVAgen links MAR to a CONVECTIVE ADJUSMENT procedure      |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT (via common block)                                             |
C |   ^^^^^         itexpe           : Experiment Iteration Counter        |
C |                 itConv           : Adjustment Calls     Counter        |
C |                 dt_Loc           : Mass Flux  Scheme:   Time Step      |
C |                                                                        |
C |                     dx           : grid  spacing                   (m) |
C |                     dy           : grid  spacing                   (m) |
C |                 tairDY(mx,my,mz) : air   temperature               (K) |
C |                   qiHY(mx,my,mz) : air   cloud crystals conc.  (kg/kg) |
C |                   qsHY(mx,my,mz) : air   snow  flakes   conc.  (kg/kg) |
C |                   qwHY(mx,my,mz) : air   cloud droplets conc.  (kg/kg) |
C |                   qrHY(mx,my,mz) : air   rain  drops    conc.  (kg/kg) |
C |                                                                        |
C |   INPUT / OUTPUT:   dx           : grid  spacing                   (m) |
C |   ^^^^^^^^^^^^^^^   dy           : grid  spacing                   (m) |
C |                 pktaDY(mx,my,mz) : air   temperature               (K) |
C |                   qvDY(mx,my,mz) : air   specific humidity     (kg/kg) |
C |                 rainHY(mx,my)    : rain  Precipitation             (m) |
C |                 rainCA(mx,my)    : rain  Precipitation             (m) |
C |                                                                        |
C |   REFER. : 1) MesoNH CONVECTIVE ADJUSMENT Routine                      |
C |   ^^^^^^^^ 2) cfr.  head of SubRoutine CONVECTION                      |
C |                                                                        |
C | # OPTIONS: #pb  Limited Scalar Operations ==>   NO vectorization       |
C | # ^^^^^^^^ #EW  Energy and Water ?Conservation                         |
C | #          #AN  Anabatic Wind Parameterization                         |
C | #          #GU  Gust Front    Parameterization                         |
C | #          #gu  Gust Front    Parameterization (NO vectorization)      |
C | #          #GW  Gust Front    Parameterization (OUTPUT)                |
C |                                                                        |
C |   MODIF. HGall?e: 18-11-2004: Adaptation to CVAmnh.f90.laurent         |
C |   ^^^^^^                      (Argument kensbl of CONVECTION removed)  |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_DY.inc'
      include 'MAR_TE.inc'
 
      include 'MAR_HY.inc'
      include 'MAR_CA.inc'
      include 'MAR_PB.inc'
c #EW include 'MAR_EW.inc'
 
      include 'MAR_SL.inc'
 
c #AN real                    rANA,hANA(mx,my)
c #AN common  /CVAgen_MNH_ANA/rANA,hANA
 
c #GW integer                    i_Gmax,k_Gmax,i_Gmin,k_Gmin
c #GW real                       waGmax       ,TaGmin
c #GU real                       waGust(mx,my,mzz)
c #GU real                       TaGust(mx,my,mz),    dtxLoc
c #GU common  /CVAgen_MNHgust/   TaGust          ,    dtxLoc
 
      logical                Odeep ,Oshal
      common  /CVAgen_MNH_lt/Odeep ,Oshal
      REAL                   pdtCVx,pdtCV ,PTdcv ,PTscv
      integer                nntCV0,jjtCV0,iitCV0
      common  /CVAgen_MNH_rt/pdtCVx,pdtCV ,PTdcv ,PTscv
      common  /CVAgen_MNH_nt/nntCV0,jjtCV0,iitCV0
 
      integer   KLON_0,KLEV_0
      parameter(KLON_0=KLON,KLEV_0=KLEV)
 
 
C +--Local  Variables
C +  ================
 
      character*3 vectcv
      integer     klcvOK
      integer     iklon ,klc
 
      logical  Odeep0,Oshal0,Orset0,Odown0,OsetA0,OCvTC0
      integer  kidia0,kfdia0,kbdia0,ktdia0,kIce_0,kensbl
 
      REAL     pdtCV0,PTdcv0,PTscv0
      REAL     Pdxdy0(KLON)
      REAL     P_pa_0(KLON,KLEV)
      REAL     P_za_0(KLON,KLEV)
      REAL     P_Ta_0(KLON,KLEV)
      REAL     P_Qa_0(KLON,KLEV)
      REAL     P_Qw_0(KLON,KLEV)
      REAL     P_Qi_0(KLON,KLEV)
      REAL     P_Ua_0(KLON,KLEV)
      REAL     P_Va_0(KLON,KLEV)
      REAL     P_Wa_0(KLON,KLEV)
 
      integer locCVA
      real    OK_CVA,MAX_TT(mx,my),min_TT_off
      real    wrk1(mx,my,mz),wrk1_mx
 
c #AN real    bANA,zANA,wANA(mx,my,mz),zlev
c #AN real    dANA,vANA,xANA
 
 
C +--Diagnostic Variables
C +  --------------------
 
c #EW integer irmx  ,jrmx  ,iter_0
c #EW real    rr_max,temp_r,energ0,water0,waterb
 
 
C +--Mass Flux convective Scheme: Set Up DATA
C +  ========================================
 
      data kidia0  /     1  /
C +...     kidia0  :  value of the first point in x
 
      data kbdia0  /     1  /
C +...     kbdia0  :  vertical computations: lowest                 level
 
      data ktdia0  /     1  /
C +...     ktdia0  :  vertical computations: over KLEV + 1 - ktdia0 levels
 
      data pdtCV0  /  600. /
cXF
C +...     pdtCV0  :  time interval between 2 CALLs of deep convection
 
      data Odeep0  / .true. /
C +...     Odeep0  :  Deep    Convection Switch
 
      data Oshal0  / .true. /
C +...     Oshal0  :  Shallow Convection Switch
 
      data Orset0  / .true. /
C +...     Orset0  :  refresh or not all tendencies at every call
 
      data Odown0  / .true. /
C +...     Odown0  :  take or not convective downdrafts into account
 
      data kIce_0  /     1  /
C +...     kIce_0  :  flag for ice ( 1 = yes,
C +                                  0 = no ice )
      data OsetA0  / .false. /
C +...     OsetA0  :  logical to set convective adjustment time by user
 
      data PTdcv0  /  1200. /
C +...     PTdcv0  :  user defined deep    adjustment time
 
      data PTscv0  /  1200. /
C +...     PTscv0  :  user defined shallow adjustment time
 
      data kensbl  /     3  /
C +...     kensbl  :  value for a "climate" run
 
      data OCvTC0  / .false. /
C +...     OCvTC0  :  flag to compute convective transport
C +                                   for  chemical tracer
 
c XF
      data min_TT_off /267.15/
C +        min_TT_off : temperature min for switching on the convect. adjust.
 
C +--Anabatic Breeze Parameterization
C +  --------------------------------
 
c #AN data xANA    /   10.0e+3  /
C +...     xANA    : Characteristic Mountain Width       Scale
 
c #AN data vANA    /    4.0e+0  /
C +...     vANA    : Characteristic Mountain Breeze Wind Scale
 
 
C +--SET UP CONVECTION SWITCHES
C +  ==========================
 
       do i=1,mx ; do j=1,my
        if(adj_CA(i,j)==0.or.itexpe==0) then
         drr_CA(i,j)=0.
         dss_CA(i,j)=0.
         DO k= 1,mz
          dpktCA(i,j,k)=0.
          dqv_CA(i,j,k)=0.
          dqw_CA(i,j,k)=0.
          dqi_CA(i,j,k)=0.
         ENDDO
        endif
       enddo ; enddo
 
      IF   (iterun.EQ.0)                                            THEN
cXF
        IF (MFLX_d)                                                 THEN
            Odeep  = MFLX_d
        ELSE
            Odeep  = Odeep0
            write(6,*) 'Deep    Convection Switch     set to ',Odeep
        END IF
        IF (MFLX_s)                                                 THEN
            Oshal  = MFLX_s
        ELSE
            Oshal  = Oshal0
            write(6,*) 'Shallow Convection Switch     set to ',Oshal
        END IF
 
        IF (tMFLXd.GT.0.)                                           THEN
            pdtCVx = tMFLXd
        ELSE
            pdtCVx = pdtCV0
            write(6,*) 'Interv. Convection CALL       set to ',pdtCVx
        END IF
 
        IF (aMFLXd.GT.0.)                                           THEN
            PTdcv  = aMFLXd
        ELSE
cXF
            PTdcv  = max(300.,min(PTdcv0,4.*dt))
            write(6,*) 'Deep    Convection Time Scale set to ',PTdcv
        END IF
        IF (aMFLXs.GT.0.)                                           THEN
            PTscv  = aMFLXs
        ELSE
            PTscv  = PTscv0
            write(6,*) 'Shallow Convection Time Scale set to ',PTscv
        END IF
 
      END IF
 
 
C +--Set UP Anabatic Breeze Parameterization
C +  =======================================
 
      IF (itexpe.EQ.0)                                              THEN
 
c #GU   open(unit=70,status='new',file='W_GUST.out')
c #GU   rewind    70
 
c #GU     dtxLoc    = dt_Loc / dx
 
c #AN     rANA      = 2.0d+0 * vANA / xANA
C +...    rANA      : Subgrid Mountain Breeze: Horizontal Divergence
C +                  (Factor 2 included  for 2 horizontal Directions)
c #AN   DO j=1,my
c #AN   DO i=1,mx
c #AN     dANA      = sh(i,j)
c #AN.         -0.25*(sh(im1(i),j)+sh(ip1(i),j)
c #AN.               +sh(i,jm1(j))+sh(i,jp1(j)))
c #AN     hANA(i,j) = abs(dANA)*max(zero,dx/xANA-unun)
C +...    hANA: D("Subgrid Mountain" Height - "Resolved Mountain" Height)
 
c #AN     hANA(i,j) = sh(i,j) * 2.0d+0
c #AN   END DO
c #AN   END DO
 
c #GU   DO k=1,mz
c #GU   DO j=1,my
c #GU   DO i=1,mx
c #GU     TaGust(i,j,k) = 0.
c #GU   END DO
c #GU   END DO
c #GU   END DO
 
 
C +--Set UP Verification
C +  ===================
 
                  klcvOK = mx2*my2
                  klcvOK =       1
            IF   (klon.ne. klcvOK)                                THEN
              IF (klon.gt.1)                                      THEN
                  vectcv = 'NON'
              ELSE
                  vectcv = '   '
              END IF
                write(6,6000) klon,klcvOK,vectcv
 6000           format(/,'++++++++ klon (MARdim.inc) =',i6,' .NE.',i6,
     .                       ' ++++++++++++++',
     .                 /,'++++++++ NOT adapted to a ',a3,' vectorized ',
     .                   'code ++++++++++++++',
     .                 /,'++++++++ BAD SET UP of #pb or klon parameter',
     .                      '  ++++++++++++++',
     .                 /,'     ==> !?%@&* Emergency EXIT in CVAgen_MNH')
                STOP
            END IF
 
 
C +--Mass Flux convective Scheme: Set Up Energy/Water Verification
C +  =============================================================
 
c #EW     energ0      = 0.0
c #EW     water0      = 0.0
c #EW     iter_0      = 0
c #EW     write(6,600)
 600      format(/,' CVAgen_MNH: Energy/Water Verification Set UP')
      END IF
 
 
C +--Mass Flux Scheme: Set Up Time Stepping
C +  ======================================
 
      IF (iterun.EQ.0)                                            THEN
            pdtCV=           pdtCVx
c #xx       pdtCV=min(dt_Loc,pdtCVx)
        if (pdtCV .lt.dt_Loc)                                     then
            pdtCV  =  dt_Loc
            jjtCV0 =   1
        else
            jjtCV0 =  pdtCV  / dt_Loc
C +...      jjtCV0 :  Number of  Diffusion Steps for 1 Convective Step
 
            pdtCV  =  dt_Loc * jjtCV0
C +...      pdtCV  :  Calibrated Convection                  Time Step
 
        end if
            iitCV0 =  0
      END IF
 
 
C +--Update Convective Mass Flux
C +  ===========================
 
      IF (mod(iitCV0,jjtCV0).EQ.0)                                THEN
 
 
C +--Contribution from Subgrid Mountain Breeze
C +  -----------------------------------------
 
c #AN   DO k=1,mz
c #AN   DO j=1,my
c #AN   DO i=1,mx
c #AN             zlev=      gplvDY(i,j,k)     *grvinv
c #AN             bANA=    min(zlev,            zi__TE(i,j))
c #AN             zANA=        hANA(i,j) + 2.0 *bANA
c #AN         IF (zlev   .LE.  zANA   .AND.
c #AN.            TairSL(i,j)         .GT.      tairDY(i,j,mz))   THEN
c #AN             wANA(i,j,k)= rANA      * 0.5 *bANA
C +...                         Half Integrated Horizontal Divergence
 
c #AN         ELSE
c #AN             wANA(i,j,k)= 0.0
c #AN         END IF
c #AN   END DO
c #AN   END DO
c #AN   END DO
 
 
C +--Contribution from the Cold Air Pool
C +  -----------------------------------
 
c #GW     waGmax        =    0.
c #GW     TaGmin        =    0.
 
c #GU   DO k=     mz,2,-1
c #GU   DO j=jp11,my1
c #GU   DO i=ip11,mx1
c #GU     waGust(i,j,k) =(  (4.0                 *TaGust(i,j     ,k)
c #GU.                      -TaGust(ip1(i),j,k)  -TaGust(i,jp1(j),k)
c #GU.                      -TaGust(im1(i),j,k)  -TaGust(i,jm1(j),k))
c #GU.                    * (0.50e6              /dx                )
c #GU.                    * (gpmiDY(i     ,j,k)  -gpmiDY(i,j     ,k+1))
c #GU.                   +   waGust(i     ,j,k+1)*gplvDY(i,j     ,k+1))
c #GU.                  /    gplvDY(i     ,j,k)
c #GU.                  *max(zero,    sign(unun,zi__TE(i,j)
c #GU.                                         -gplvDY(i,j,k)*grvinv))
c #GU     waGust(i,j,k) =max(zero,              waGust(i,j,k))
 
c #GW     IF (TaGmin.lt.TaGust(i,j,k))                            THEN
c #GW         TaGmin  = TaGust(i,j,k)
c #GW         i_Gmin  = i
c #GW         k_Gmin  = k
c #GW     END IF
c #GW     IF (waGmax.lt.waGust(i,j,k))                            THEN
c #GW         waGmax  = waGust(i,j,k)
c #GW         i_Gmax  = i
c #GW         k_Gmax  = k
c #GW     END IF
 
c #GU   END DO
c #GU   END DO
c #GU   END DO
 
c #GW   i_Gmax=max(  2,i_Gmax)
c #GW   i_Gmax=min(mx1,i_Gmax)
c #GW   k_Gmax=max(  1,k_Gmax)
c #GW   k_Gmax=min(mz ,k_Gmax)
c #GW   write(70,700) itexpe,
c #GW.         i_Gmax,k_Gmax,(waGust(i,1,k_Gmax), i=i_Gmax-1,i_Gmax+1),
c #GW.         i_Gmin,k_Gmin,(TaGust(i,1,k_Gmax), i=i_Gmax-1,i_Gmax+1),
c #GW.                grvinv* gplvDY(i_Gmax,1,k_Gmax),zi__TE(i_Gmax,1)
 700    format(2i6,i4,3f9.3,i6,i4,3f9.3,2f12.0)
 
 
C +--Mass Flux convective Scheme: Set Up Vertical Profiles
C +  -----------------------------------------------------
 
            kfdia0  =    klon
C +...      kfdia0  :   value of the last  point in x
 
            iklon = 0
 
cXF
            max_TT=-273.15
 
!$OMP PARALLEL DO
!$OMP.firstprivate(i,k,klc,iklon,OK_CVA,
!$OMP.             kidia0, kfdia0, kbdia0, ktdia0,
!$OMP.             pdtCV , Odeep , Oshal , Orset0, Odown0, kIce_0,
!$OMP.             OsetA0, PTdcv , PTscv ,
!$OMP.             kensbl,
!$OMP.     P_pa_0, P_za_0, Pdxdy0,
!$OMP.     P_Ta_0, P_Qa_0, P_Qw_0, P_Qi_0, P_Ua_0, P_Va_0, P_Wa_0,
!$OMP.     Kstep1, PdTa_1, PdQa_1, PdQw_1, PdQi_1,
!$OMP.                     Pdrr_1, Pdss_1,
!$OMP.     PuMF_1, PdMF_1, Pfrr_1, Pfss_1, Pcape1, K_CbT1, K_CbB1,
!$OMP.     OCvTC0, P_CH_0, PdCH_1)
!$OMP.schedule(dynamic)
 
        DO j = 4,my-3
        DO i = 4,mx-3
 
            iklon = 1 + iklon
            iklon = 1
 
             Pdxdy0(iklon)    = dx * dy
C +...       Pdxdy0           : grid area                            [m2]
 
          DO klc= 1,klev
             k = mzz-klc
             P_pa_0(iklon,klc) = (pstDY(i,j)*sigma( k)   + ptopDY) *1.e3
C +...       P_pa_0            : pressure     in layer               [Pa]
 
             P_za_0(iklon,klc) = gplvDY(i,j,k) * grvinv
C +...       P_za_0            : height of model layer                [m]
 
             P_Ta_0(iklon,klc) = tairDY(i,j,k)
C +...       P_Ta_0            : grid scale T           at time t     [K]
 
cXF
             max_TT(i,j)       = max(max_TT(i,j),tairDY(i,j,k))
cXF
             P_Qa_0(iklon,klc) =   qvDY(i,j,k)
C +...       P_Qa_0            : grid scale water vapor at time t [kg/kg]
 
             P_Qw_0(iklon,klc) =   qwHY(i,j,k) / (1.0-qwHY(i,j,k))
cXF bug ?
C +...       P_Qw_0            : grid scale Cloud drops at time t [kg/kg]
 
             P_Qi_0(iklon,klc) =   qiHY(i,j,k) / (1.0-qiHY(i,j,k))
cXF bug ?
C +...       P_Qi_0            : grid scale Cloud ice   at time t [kg/kg]
 
             P_Ua_0(iklon,klc) = uairDY(i,j,k)
C +...       P_Ua_0            : grid scale hor. wind u at time t   [m/s]
 
             P_Va_0(iklon,klc) = vairDY(i,j,k)
C +...       P_Va_0            : grid scale hor. wind v at time t   [m/s]
 
             P_Wa_0(iklon,klc) = wairDY(i,j,k) *0.01
     .                  +sqrt(2.*ect_TE(i,j,k) /3.)
c #AN.                         +   wANA(i,j,k)
c #GU.                         + waGust(i,j,k)
C +...       P_Wa_0            : grid scale vertic.wind at time t   [m/s]
 
          END DO
          adj_CA(i,j)=0.
 
C +--Mass Flux convective Scheme: iteration, in case of no vectorization
C +  -------------------------------------------------------------------
 
          if(MAX_TT(i,j)>=min_tt_off) then
C +       ***************
          call CONVECTION(
     .             KLON_0, KLEV_0, kidia0, kfdia0, kbdia0, ktdia0,
     .             pdtCV , Odeep , Oshal , Orset0, Odown0, kIce_0,
     .             OsetA0, PTdcv , PTscv ,
     .             kensbl,
     .     P_pa_0, P_za_0, Pdxdy0,
     .     P_Ta_0, P_Qa_0, P_Qw_0, P_Qi_0, P_Ua_0, P_Va_0, P_Wa_0,
     .     Kstep1, PdTa_1, PdQa_1, PdQw_1, PdQi_1,
     .                     Pdrr_1, Pdss_1,
     .     PuMF_1, PdMF_1, Pfrr_1, Pfss_1, Pcape1, K_CbT1, K_CbB1,
     .     OCvTC0, KTCCH0, P_CH_0, PdCH_1)
C +       ***************
 
C +--Mass Flux convective Scheme: products,  in case of no vectorization
C +  -------------------------------------------------------------------
 
             capeCA(i,j)   = Pcape1(iklon)
             adj_CA(i,j)   = Kstep1(iklon)
             drr_CA(i,j)   = Pdrr_1(iklon)    * dt_Loc
             dss_CA(i,j)   = Pdss_1(iklon)    * dt_Loc
 
             if(isnan(capeCA(i,j))) adj_CA(i,j)=-1
             if(isnan(drr_CA(i,j))) adj_CA(i,j)=-1
             if(isnan(dss_CA(i,j))) adj_CA(i,j)=-1
             if(drr_CA(i,j)>0.1)    adj_CA(i,j)=-1
             if(drr_CA(i,j)<0)      adj_CA(i,j)=-1
             if(dss_CA(i,j)>0.1)    adj_CA(i,j)=-1
             if(dss_CA(i,j)<0)      adj_CA(i,j)=-1
 
          DO klc= 1,klev
             k = mzz-klc
 
             dpktCA(i,j,k) = PdTa_1(iklon,klc) * dt_Loc /pkDY(i,j,k)
             dqv_CA(i,j,k) = PdQa_1(iklon,klc) * dt_Loc
             dqw_CA(i,j,k) = PdQw_1(iklon,klc) * dt_Loc
             dqi_CA(i,j,k) = PdQi_1(iklon,klc) * dt_Loc
 
c #gu        TaGust(i,j,k) = TaGust(i,j,k)*exp(-dtxLoc*ssvSL(i,j,k))
c #gu.                     + PdTa_1(iklon,klc) * dt_Loc
c #gu        TaGust(i,j,k) = min(TaGust(i,j,k),zero)
 
             if(isnan(dpktCA(i,j,k))) adj_CA(i,j)=-1
             if(isnan(dqv_CA(i,j,k))) adj_CA(i,j)=-1
             if(isnan(dqw_CA(i,j,k))) adj_CA(i,j)=-1
             if(isnan(dqi_CA(i,j,k))) adj_CA(i,j)=-1
          END DO
          endif
 
          if(adj_CA(i,j)<=0.or.MAX_TT(i,j)<min_tt_off) then
             capeCA(i,j)=0.
             drr_CA(i,j)=0.
             dss_CA(i,j)=0.
             DO klc= 1,klev
              k = mzz-klc
              dpktCA(i,j,k)=0.
              dqv_CA(i,j,k)=0.
              dqw_CA(i,j,k)=0.
              dqi_CA(i,j,k)=0.
             ENDDO
          endif
 
        END DO
        END DO
!$OMP END PARALLEL DO
 
C +--Mass Flux convective Scheme: iteration, in case of    vectorization
C +  -------------------------------------------------------------------
 
        IF (klon.gt.1)                                            THEN
 
C +       ***************
          call CONVECTION(
     .             KLON_0, KLEV_0, kidia0, kfdia0, kbdia0, ktdia0,
     .             pdtCV , Odeep , Oshal , Orset0, Odown0, kIce_0,
     .             OsetA0, PTdcv , PTscv ,
     .             kensbl,
     .     P_pa_0, P_za_0, Pdxdy0,
     .     P_Ta_0, P_Qa_0, P_Qw_0, P_Qi_0, P_Ua_0, P_Va_0, P_Wa_0,
     .     Kstep1, PdTa_1, PdQa_1, PdQw_1, PdQi_1,
     .                     Pdrr_1, Pdss_1,
     .     PuMF_1, PdMF_1, Pfrr_1, Pfss_1, Pcape1, K_CbT1, K_CbB1,
     .     OCvTC0, KTCCH0, P_CH_0, PdCH_1)
C +       ***************
 
 
C +--Mass Flux convective Scheme: products,  in case of    vectorization
C +  -------------------------------------------------------------------
 
             iklon = 0
          DO j = jp11,my1
          DO i = ip11,mx1
             iklon = 1 + iklon
 
              capeCA(i,j)   = Pcape1(iklon)
              adj_CA(i,j)   = Kstep1(iklon)
              drr_CA(i,j)   = Pdrr_1(iklon)    * dt_Loc
              dss_CA(i,j)   = Pdss_1(iklon)    * dt_Loc
 
            DO klc= 1,klev
               k = mzz-klc
 
              dpktCA(i,j,k) = PdTa_1(iklon,klc) * dt_Loc /pkDY(i,j,k)
              dqv_CA(i,j,k) = PdQa_1(iklon,klc) * dt_Loc
              dqw_CA(i,j,k) = PdQw_1(iklon,klc) * dt_Loc
              dqi_CA(i,j,k) = PdQi_1(iklon,klc) * dt_Loc
 
c #GU         TaGust(i,j,k) = TaGust(i,j,k)*exp(-dtxLoc*ssvSL(i,j,k))
c #GU.                      + PdTa_1(iklon,klc) * dt_Loc
c #GU         TaGust(i,j,k) = min(TaGust(i,j,k),zero)
            END DO
          END DO
          END DO
 
        END IF
 
C +--Vertical Integrated Energy and Water Content
C +  ============================================
 
c #EW DO   j=jp11,my1
c #EW DO   i=ip11,mx1
c #EW     enr0EW(i,j) = 0.0
c #EW     wat0EW(i,j) = 0.0
 
c #EW   DO k=1,mz
c #EW     temp_r      = pktaDY(i,j,k)*pkDY(i,j,k)
c #EW     enr0EW(i,j) = enr0EW(i,j)
c #EW.                +(temp_r
c #EW.                  -(qwHY(i,j,k)+qrHY(i,j,k)) *r_LvCp
c #EW.                  -(qiHY(i,j,k)+qsHY(i,j,k)) *r_LsCp   )*dsigm1(k)
c #EW     wat0EW(i,j) = wat0EW(i,j)
c #EW.                +  (qvDY(i,j,k)
c #EW.                +   qwHY(i,j,k)+qrHY(i,j,k)
c #EW.                +   qiHY(i,j,k)+qsHY(i,j,k)            )*dsigm1(k)
c #EW   END DO
 
c #EW     enr0EW(i,j) = enr0EW(i,j) * pstDYn(i,j) * grvinv
c #EW     wat0EW(i,j) = wat0EW(i,j) * pstDYn(i,j) * grvinv
C +...    wat0EW [m]    contains implicit factor 1.d3 [kPa-->Pa] /ro_Wat
 
c #EW     energ0      = energ0 - enr0EW(i,j)
c #EW     water0      = water0 - wat0EW(i,j)
c #EW END DO
c #EW END DO
 
 
C +     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
C +--filtering
C +  =========
 
      DO j = jp11,my1
      DO i = ip11,mx1
       wrk1(i,j,1)=adj_CA(i,j)
       wrk1(i,j,2)=drr_CA(i,j)
       wrk1(i,j,3)=dss_CA(i,j)
      ENDDO
      ENDDO
 
      call cva_filtering(wrk1,3)
      DO j = jp11,my1
      DO i = ip11,mx1
       if(wrk1(i,j,1)<=0) then
        drr_CA(i,j)=0.
        dss_CA(i,j)=0.
        adj_CA(i,j)=0.
       else
        adj_CA(i,j)=max(1,nint(wrk1(i,j,1)))
        drr_CA(i,j)=wrk1(i,j,2)
        dss_CA(i,j)=wrk1(i,j,3)
       endif
      ENDDO
      ENDDO
 
      call cva_filtering(dpktCA,mz)
      call cva_filtering(dqv_CA,mz)
      call cva_filtering(dqw_CA,mz)
      call cva_filtering(dqi_CA,mz)
 
      ENDIF
 
C +--Mass Flux convective Scheme
C +  ===========================
 
        DO j = 4,my-3
        DO i = 4,mx-3
 
C +     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
            locCVA        = min(adj_CA(i,j),1)
            OK_CVA        = max(locCVA     ,0)
cXF
        IF (adj_CA(i,j).gt.0
     .      .and.max(drr_CA(i,j),dss_CA(i,j))<0.25*dt_loc/3600
     .      .and.min(drr_CA(i,j),dss_CA(i,j))>=0)               THEN
                !MIN= 0 - MAX= 250mm/h
            OK_CVA        =     1.0
            adj_CA(i,j)   =     adj_CA(i,j)-1
C +...      ^^^^ Number of remaining time step before the end of convection
 
            adj_CA(i,j)   = max(adj_CA(i,j),0)
 
C +----Temporal tendencies on pktaDY, qvDY and rainHY
C +    ----------------------------------------------
 
          DO k=1,mz
            pktaDY(i,j,k) =      pktaDY(i,j,k) + dpktCA(i,j,k) * OK_CVA
              qvDY(i,j,k) = max(   qvDY(i,j,k) + dqv_CA(i,j,k) * OK_CVA
     .                         ,   epsq                                )
cXF
              if(k>=mzhyd) then
              qwHY(i,j,k)=  max(qwHY(i,j,k) + dqw_CA(i,j,k)*OK_CVA,eps9)
              qiHY(i,j,k)=  max(qiHY(i,j,k) + dqi_CA(i,j,k)*OK_CVA,eps9)
              endif
cXF
          ENDDO
 
            rainHY(i,j)   =      rainHY(i,j)   + drr_CA(i,j)   * OK_CVA
            rainCA(i,j)   =      rainCA(i,j)   + drr_CA(i,j)   * OK_CVA
 
            snowHY(i,j)   =      snowHY(i,j)   + dss_CA(i,j)   * OK_CVA
            snowCA(i,j)   =      snowCA(i,j)   + dss_CA(i,j)   * OK_CVA
            snohSL(i,j)   =      snohSL(i,j)   + dss_CA(i,j)   * OK_CVA
 
        ELSE          !  { adj_CA(i,j).gt. 0 }
 
         DO k= 1,mz
          dpktCA(i,j,k)=0.
          dqv_CA(i,j,k)=0.
          dqw_CA(i,j,k)=0.
          dqi_CA(i,j,k)=0.
         ENDDO
 
        ENDIF
 
C +     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
      END DO
      END DO
 
 
C +--Vertical Integrated Energy and Water Content
C +  ============================================
 
c #EW DO   j=jp11,my1
c #EW DO   i=ip11,mx1
c #EW     enr1EW(i,j) = 0.0
c #EW     wat1EW(i,j) = 0.0
c #EW     watfEW(i,j) =-drr_CA(i,j)
 
c #EW   DO k=1,mz
c #EW     temp_r      = pktaDY(i,j,k)*pkDY(i,j,k)
c #EW     enr1EW(i,j) = enr1EW(i,j)
c #EW.                +(temp_r
c #EW.                  -(qwHY(i,j,k)+qrHY(i,j,k)) *r_LvCp
c #EW.                  -(qiHY(i,j,k)+qsHY(i,j,k)) *r_LsCp   )*dsigm1(k)
c #EW     wat1EW(i,j) = wat1EW(i,j)
c #EW.                +  (qvDY(i,j,k)
c #EW.                +   qwHY(i,j,k)+qrHY(i,j,k)
c #EW.                +   qiHY(i,j,k)+qsHY(i,j,k)            )*dsigm1(k)
c #EW   END DO
 
c #EW     enr1EW(i,j) = enr1EW(i,j) * pstDYn(i,j)  *grvinv
c #EW.                - drr_CA(i,j)                *r_LvCp
c #EW     wat1EW(i,j) = wat1EW(i,j) * pstDYn(i,j)  *grvinv
C +...    wat1EW [m]    contains implicit factor 1.d3 [kPa-->Pa] /ro_Wat
 
c #EW     energ0      = energ0 + enr1EW(i,j)
c #EW     water0      = water0 + wat1EW(i,j)
c #EW     iter_0      = iter_0 + 1
c #EW END DO
c #EW END DO
 
 
C +--Vertical Integrated Energy and Water Content: OUTPUT
C +  ====================================================
 
c #EW       irmx   =       imez
c #EW       jrmx   =       jmez
c #EW       rr_max =       0.0
c #EW DO  j=jp11,my1
c #EW DO  i=ip11,mx1
c #EW   IF (drr_CA(i,j).gt.rr_max)                                THEN
c #EW       rr_max =       drr_CA(i,j)
c #EW       irmx   =              i
c #EW       jrmx   =                j
c #EW   END IF
c #EW END DO
c #EW END DO
c #EW   waterb = wat1EW(irmx,jrmx)-wat0EW(irmx,jrmx)-watfEW(irmx,jrmx)
c #EW   write(6,606) itexpe,enr0EW(irmx,jrmx),1.d3*wat0EW(irmx,jrmx),
c #EW.            irmx,jrmx,enr1EW(irmx,jrmx),1.d3*wat1EW(irmx,jrmx),
c #EW.                                        1.d3*watfEW(irmx,jrmx),
c #EW.                                        1.d3*waterb           ,
c #EW.                     energ0/iter_0    ,     water0/iter_0
 606    format(i9,'  Before CVAj:  E0 =',f12.6,'  W0 = ',f9.6,
     .    /,i5,i4,'  After  CVAj:  E1 =',f12.6,'  W1 = ',f9.6,
     .                                         '  W Flux =',f9.6,
     .                                         '  Div(W) =',e9.3,
     .       /,9x,'         Mean   dE =',f12.9,'  dW = ',e9.3)
 
      iitCV0 = iitCV0 + 1
 
      return
      end
