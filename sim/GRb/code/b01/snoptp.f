 
 
      subroutine SnOptP
 
C +------------------------------------------------------------------------+
C | MAR/SISVAT   SnOptP                                     8-06-2010  MAR |
C |   SubRoutine SnOptP computes the Snow Pack optical Properties          |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   PARAMETERS:  klonv: Total Number of columns =                        |
C |   ^^^^^^^^^^        = Total Number of continental     Grid Boxes       |
C |                     X       Number of Mosaic Cell per Grid Box         |
C |                                                                        |
C |   INPUT:   isnoSV   = total Nb of Ice/Snow Layers                      |
C |   ^^^^^    ispiSV   = 0,...,nsno: Uppermost Superimposed Ice Layer     |
C |                                                                        |
C |            ivgtSV   = 0,...,12:   Vegetation Type                      |
C |                       0:          Water, Solid or Liquid               |
C |                                                                        |
C |   INPUT:   G1snSV   : Dendricity (<0) or Sphericity (>0) of Snow Layer |
C |   ^^^^^    G2snSV   : Sphericity (>0) or Size            of Snow Layer |
C |            agsnSV   : Snow       Age                             [day] |
C |            ro__SV   : Snow/Soil  Volumic Mass                  [kg/m3] |
C |            eta_SV   : Water      Content                       [m3/m3] |
C |            rusnSV   : Surficial  Water   Thickness   [kg/m2] .OR. [mm] |
C |            SWS_SV   : Surficial  Water   Status                        |
C |            dzsnSV   : Snow       Layer   Thickness                 [m] |
C |                                                                        |
C |            albssv   : Soil       Albedo                            [-] |
C |            zzsnsv   : Snow       Pack    Thickness                 [m] |
C |                                                                        |
C |   OUTPUT:  albisv   : Snow/Ice/Water/Soil Integrated Albedo        [-] |
C |   ^^^^^^   sEX_sv   : Verticaly Integrated Extinction Coefficient      |
C |                                                                        |
C |   Internal Variables:                                                  |
C |   ^^^^^^^^^^^^^^^^^^                                                   |
C |            SnOpSV   : Snow Grain optical Size                      [m] |
C |            EX1_sv   : Integrated Snow Extinction (0.3--0.8micr.m)      |
C |            EX2_sv   : Integrated Snow Extinction (0.8--1.5micr.m)      |
C |            EX3_sv   : Integrated Snow Extinction (1.5--2.8micr.m)      |
C |                                                                        |
C |   METHODE:    Calcul de la taille optique des grains ? partir de       |
C |   ^^^^^^^    -leur type decrit par les deux variables descriptives     |
C |                    continues sur la plage -99/+99 passees en appel.    |
C |              -la taille optique (1/10mm) des etoiles,                  |
C |                                          des grains fins et            |
C |                                          des jeunes faces planes       |
C |                                                                        |
C |   METHOD:     Computation of the optical diameter of the grains        |
C |   ^^^^^^      described with the CROCUS formalism G1snSV / G2snSV      |
C |                                                                        |
C |   REFERENCE: Brun et al.      1989, J. Glaciol 35 pp. 333--342         |
C |   ^^^^^^^^^  Brun et al.      1992, J. Glaciol 38 pp.  13-- 22         |
C |              Eric Martin Sept.1996                                     |
C |                                                                        |
C | # OPTIONS: #AG: Generalisation of Col de Porte Ageing Parameterization |
C | # ^^^^^^^  #CZ: Albedo Correction (Zenith Angle)                       |
C | #          #AW: Output of Soil-Ice-Snow Albedo                         |
C |                                                                        |
C |   CAUTION: Vegetation is not taken into account in albedo computations |
C |   ^^^^^^^  Suggestion: 1) Reduce the displacement height  and/or LAI   |
C |            (when snow)    for radiative transfert through vegetation   |
C |                        2) Adapt leaf optical parameters                |
C |                                                                        |
C |                                                                        |
C |   Preprocessing  Option: SISVAT IO (not always a standard preprocess.) |
C |   ^^^^^^^^^^^^^^^^^^^^^  ^^^^^^^^^                                     |
C |   FILE                 |      CONTENT                                  |
C |   ~~~~~~~~~~~~~~~~~~~~~+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ |
C | # SnOptP____.va        | #va: OUTPUT/Verification: Albedo Parameteriz. |
C |                        |      unit 46, SubRoutine  SnOptP     **ONLY** |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include  "MARphy.inc"
c #AG include  "MARdim.inc"
c #AG include  "MAR_GE.inc"
      include  "MAR_SV.inc"
      include  "MARdSV.inc"
c #cp include  "MARdCP.inc"
C +
      include  "MARxSV.inc"
      include  "MARySV.inc"
!$OMP threadprivate(/xSISVAT_I/,/xSISVAT_R/,/ySISVAT_I/,/ySISVAT_R/)
 
C +--Internal Variables
C +  ==================
 
      real      coalb1(klonv)                      ! weighted Coalbedo, Vis.
      real      coalb2(klonv)                      ! weighted Coalbedo, nIR 1
      real      coalb3(klonv)                      ! weighted Coalbedo, nIR 2
      real      coalbm                             ! weighted Coalbedo, mean
      real      sExt_1(klonv)                      ! Extinction Coeff., Vis.
      real      sExt_2(klonv)                      ! Extinction Coeff., nIR 1
      real      sExt_3(klonv)                      ! Extinction Coeff., nIR 2
      real      SnOpSV(klonv,      nsno)           ! Snow Grain optical Size
c #AG real      agesno
 
      integer   isn   ,ikl   ,isn1  ,jjtime
      real      sbeta1,sbeta2,sbeta3,sbeta4,sbeta5
      real      AgeMax,AlbMin,HSnoSV,HIceSV,doptmx,SignG1,Sph_OK
      real      dalbed,dalbeS,dalbeW
      real      bsegal,czemax,csegal
      real      RoFrez,DiffRo,SignRo,SnowOK,OpSqrt
      real      albSn1,albIc1,a_SnI1,a_SII1
      real      albSn2,albIc2,a_SnI2,a_SII2
      real      albSn3,albIc3,a_SnI3,a_SII3
      real      albSno,albIce,albSnI,albSII,albWIc,albmax
      real      doptic,Snow_H,SIce_H,SnownH,SIcenH
      real      exarg1,exarg2,exarg3,sign_0,sExt_0
      real      albedo_old
      real      ro_ave,dz_ave,minalb
 
C +--OUTPUT of SISVAT Trace Statistics (see assignation in PHY_SISVAT)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #SR integer             iSRwri,jSRwri,nSRwri,kSRwri,lSRwri
c #SR common/SISVAT_trace/iSRwri,jSRwri,nSRwri,kSRwri,lSRwri
 
C +--Albedo: IO
C +  ~~~~~~~~~~
! #va logical         aw_opn                       ! IO       Switch
! #va common/SnOptP_L/aw_opn                       !
 
 
C +--Local   DATA
C +  ============
 
C +--For the computation of the solar irradiance extinction in snow
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      data      sbeta1/0.0192/,sbeta2/0.4000/,sbeta3/0.1098/
      data      sbeta4/1.0000/
      data      sbeta5/2.00e1/
 
C +--Snow Age Maximum (Taiga, e.g. Col de Porte)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      data      AgeMax  /60.0/
C +...          AgeMax:  Snow Age Maximum                              [day]
 
      data      AlbMin  /0.94/
C +...          AlbMin:  Albedo   Minimum / visible (0.3--0.8 micrometers)
 
      data      HSnoSV  /0.01/
C +...          HSnoSV:  Snow     Thickness through witch
C +                      Albedo is interpolated to Ice  Albedo
      data      HIceSV  /0.10/
C +...          HIceSV:  Snow/Ice Thickness through witch
C +                      Albedo is interpolated to Soil Albedo
      data      doptmx  /2.3e-3/
C +...          doptmx:  Maximum  optical Diameter    (pi * R**2)        [m]
C +
      data      czeMAX  /0.173648178/  ! 80.deg (Segal et al., 1991 JAS)
      data      bsegal  /4.00       /  !
      data      albmax  /0.99       /  ! Albedo max
 
 
C +--Snow Grain optical Size
C +  =======================
 
        DO ikl=1,klonv
         DO   isn=1,max(1,isnoSV(ikl))
 
          G2snSV(ikl,isn) =  max(epsi,G2snSV(ikl,isn))
C +...    Avoid non physical Values
 
          SignG1          = sign(unun,G1snSV(ikl,isn))
          Sph_OK          =  max(zero,SignG1)
 
          SnOpSV(ikl,isn) =   1.e-4 *
C +...    SI:           (from 1/10 mm to m)
 
 
C +--Contribution of Non Dendritic Snow
C +  ----------------------------------
 
     .    (    Sph_OK *(      G2snSV(ikl,isn)*G1snSV(ikl,isn)/G1_dSV
     .              +max(demi*G2snSV(ikl,isn),DFcdSV)
     .                 *(unun-G1snSV(ikl,isn)                /G1_dSV))
 
 
C +--Contribution of     Dendritic Snow
C +  ----------------------------------
 
     .    +(1.-Sph_OK)*(     -G1snSV(ikl,isn)*DDcdSV         /G1_dSV
     .                 +(unun+G1snSV(ikl,isn)                /G1_dSV)
     .                  *    (G2snSV(ikl,isn)*DScdSV         /G1_dSV
     .                 +(unun-G2snSV(ikl,isn)                /G1_dSV)
     .                                       *DFcdSV                 )))
          SnOpSV(ikl,isn) =  max(zero,SnOpSV(ikl,isn))
        END DO
      END DO
 
 
C +--Snow/Ice Albedo
C +  ===============
 
C +--Snow Age (Influence on Albedo)
C +  ------------------------------
 
c #AG jjtime = jhurGE*3600+minuGE*60+jsecGE
c #AG IF (iabs(mod(jjtime,86400)).lt.dt__SV)                        THEN
c #AG   DO isn=1,nsno
c #AG   DO ikl=1,klonv
c #AG     agsnSV(ikl,isn) = agsnSV(ikl,isn) + 1.
c #AG.           + max(zero,DH_dSV(ivgtSV(ikl))-DH_dSV(4)) ! High Vegetation
C +                                                        ! Impurities
C +...                      CAUTION: crude parameterization
C +                         ^^^^^^^
c #AG   END DO
c #AG   END DO
c #AG END IF
 
 
C +--Uppermost effective Snow Layer
C +  ------------------------------
 
        DO ikl=1,klonv
 
           isn   =  max(iun,isnoSV(ikl))
 
          SignRo = sign(unun, rocdSV - ro__SV(ikl,isn))
          SnowOK =  max(zero,SignRo) ! Ice Density Threshold
 
          OpSqrt = sqrt(SnOpSV(ikl,isn))
 
          albSn1 =  0.96-1.580*OpSqrt
          albSn1 =  max(albSn1,AlbMin)
 
          albSn1 =  max(albSn1,zero)
          albSn1 =  min(albSn1,unun)
 
          albSn2 =  0.95-15.40*OpSqrt
          albSn2 =  max(albSn2,zero)
          albSn2 =  min(albSn2,unun)
 
          doptic =  min(SnOpSV(ikl,isn),doptmx)
          albSn3 =  346.3*doptic -32.31*OpSqrt +0.88
          albSn3 =  max(albSn3,zero)
          albSn3 =  min(albSn3,unun)
 
          albSno =  So1dSV*albSn1
     .           +  So2dSV*albSn2
     .           +  So3dSV*albSn3
 
cXF
          minalb =  (aI2dSV + (aI3dSV -aI2dSV)
     .           *  (ro__SV(ikl,isn)-ro_Ice)/(roSdSV-ro_Ice))
          minalb =  min(aI3dSV,max(aI2dSV,minalb)) ! pure/firn albedo
 
          SnowOK =  SnowOK*max(zero,sign(unun,     albSno-minalb))
          albSn1 =  SnowOK*albSn1+(1.0-SnowOK)*max(albSno,minalb)
          albSn2 =  SnowOK*albSn2+(1.0-SnowOK)*max(albSno,minalb)
          albSn3 =  SnowOK*albSn3+(1.0-SnowOK)*max(albSno,minalb)
 
c +           ro < roSdSV |          min al > aI3dSV
c +  roSdSV < ro < rocdSV | aI2dSV < min al < aI3dSV (fct of density)
 
 
C +--Snow/Ice Pack Thickness
C +  -----------------------
 
          isn    =    max(min(isnoSV(ikl) ,ispiSV(ikl)),0)
          Snow_H =  zzsnsv(ikl,isnoSV(ikl))-zzsnsv(ikl,isn)
          SIce_H =  zzsnsv(ikl,isnoSV(ikl))
          SnownH =  Snow_H  /  HSnoSV
          SnownH =  min(unun,  SnownH)
          SIcenH =  SIce_H  / (HIceSV
     .           +  max(zero,Z0mdSV(ivgtSV(ikl))
     .           -           Z0mdSV(4)         ))
          SIcenH =  min(unun,  SIcenH)
 
C +       The value of SnownH is set to 1 in case of ice lenses above
C +       1m of dry snow (ro<700kg/m3) for using CROCUS albedo
 
          ro_ave =  0.
          dz_ave =  0.
          SnowOK =  1.
       do isn    =  isnoSV(ikl),1,-1
          ro_ave =  ro_ave + ro__SV(ikl,isn) * dzsnSV(ikl,isn) * SnowOK
          dz_ave =  dz_ave +                   dzsnSV(ikl,isn) * SnowOK
          SnowOK =  max(zero,sign(unun,1.-dz_ave))
       enddo
 
          ro_ave =  ro_ave / max(dz_ave,epsi)
          SnowOK =  max(zero,sign(unun,roSdSV-ro_ave))
 
          SnownH =  SnowOK + SnownH * (1. - SnowOK)
 
 
C +--Integrated Snow/Ice Albedo: Case of Water on Bare Ice
C +  -----------------------------------------------------
 
          isn    =  max(min(isnoSV(ikl) ,ispiSV(ikl)),0)
 
          albWIc =  aI1dSV-(aI1dSV-aI2dSV)
     .           *  exp(-(rusnSV(ikl)                      !
     .           *  (1. -SWS_SV(ikl)                       ! 0 <=> freezing
     .           *  (1  -min(1,iabs(isn-isnoSV(ikl)))))    ! 1 <=> isn=isnoSV
     .           /   ru_dSV)**0.25)                        !
          albWIc = max(aI1dSV,min(aI2dSV,albWIc+slopSV(ikl)*1.5))
 
          SignRo = sign(unun,ro_Ice-5.-ro__SV(ikl,isn))    ! RoSN<920kg/m3
          SnowOK =  max(zero,SignRo)
 
          albWIc = (1. - SnowOK) * albWIc + SnowOK
     .           * (aI2dSV + (aI3dSV -aI2dSV)
     .           * (ro__SV(ikl,isn)-ro_Ice)/(roSdSV-ro_Ice))
 
c +  rocdSV < ro < ro_ice | aI2dSV< al <aI3dSV (fct of density)
c +           ro > ro_ice | aI1dSV< al <aI2dSV (fct of superficial water content
 
 
C +--Integrated Snow/Ice      Albedo
C +  -------------------------------
 
          a_SII1      =     albWIc      +(albSn1-albWIc)     *SnownH
          a_SII1      = min(a_SII1       ,albSn1)
 
          a_SII2      =     albWIc      +(albSn2-albWIc)     *SnownH
          a_SII2      = min(a_SII2       ,albSn2)
 
          a_SII3      =     albWIc      +(albSn3-albWIc)     *SnownH
          a_SII3      = min(a_SII3       ,albSn3)
 
c #AG     agesno =      min(agsnSV(ikl,isn)          ,AgeMax)
c #AG     a_SII1      =     a_SII1      -0.175*agesno/AgeMax
C +...                                   Impurities: Col de Porte Parameter.
 
 
!    Zenith Angle Correction (Segal et al.,         1991, JAS 48, p.1025)
!    ----------------------- (Wiscombe & Warren, dec1980, JAS   , p.2723)
!                            (Warren,               1982,  RG   , p.  81)
!                            --------------------------------------------
 
 
          dalbed = 0.0
          csegal = max(czemax                   ,coszSV(ikl))
c #cz     dalbeS =   ((bsegal+unun)/(unun+2.0*bsegal*csegal)
c #cz.                -       unun                          )*0.32
c #cz.              /  bsegal
c #cz     dalbeS = max(dalbeS,zero)
c #cz     dalbed =     dalbeS      *       min(1,isnoSV(ikl))
 
          dalbeW =(0.64 - csegal  )*0.0625  ! Warren 1982, RevGeo, fig.12b
                                            ! 0.0625 = 5% * 1/0.8,   p.81
                                            ! 0.64   = cos(50)
          dalbed =     dalbeW      *       min(1,isnoSV(ikl))
 
C +--Col de Porte Integrated Snow/Ice Albedo
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #cp     IF (ColPrt.AND.TotSol.gt.0.)                              THEN
c #cp       albSII =  (((Dr_1SN*a_SII1+Dr_2SN*a_SII2+Dr_3SN*a_SII3)
c #cp.                  +dalbed                                    )
c #cp.                  *DirSol
c #cp.                 +(Df_1SN*a_SII1+Df_2SN*a_SII2+Df_3SN*a_SII3)
c #cp.                  *DifSol*(1.   -cld_SV(ikl))
c #cp.                 +(Dfc1SN*a_SII1+Dfc2SN*a_SII2+Dfc3SN*a_SII3)
c #cp.                  *DifSol*       cld_SV(ikl)                  )
c #cp.                 / TotSol
 
C +--Elsewhere    Integrated Snow/Ice Albedo
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #cp     ELSE
            albSII =     So1dSV*a_SII1
     .                 + So2dSV*a_SII2
     .                 + So3dSV*a_SII3
c #cp     END IF
 
 
C +--Integrated Snow/Ice/Soil Albedo
C +  -------------------------------
 
            alb1sv(ikl) =     albssv(ikl) +(a_SII1-albssv(ikl))*SIcenH
            alb1sv(ikl) = min(alb1sv(ikl)  ,a_SII1)
 
            alb2sv(ikl) =     albssv(ikl) +(a_SII2-albssv(ikl))*SIcenH
            alb2sv(ikl) = min(alb2sv(ikl)  ,a_SII2)
 
            alb3sv(ikl) =     albssv(ikl) +(a_SII3-albssv(ikl))*SIcenH
            alb3sv(ikl) = min(alb3sv(ikl)  ,a_SII3)
 
            albisv(ikl) =     albssv(ikl) +(albSII-albssv(ikl))*SIcenH
            albisv(ikl) = min(albisv(ikl)  ,albSII)
 
 
C +--Integrated Snow/Ice/Soil Albedo: Clouds Correction! Greuell & all., 1994
C +  --------------------------------------------------! Glob.&t Planet.Change
                                                       ! (9):91-114
c #cp     IF (.NOT.ColPrt)                                          THEN
            alb1sv(ikl) = alb1sv(ikl) + 0.05 *(cld_SV(ikl)-0.5)*SIcenH
     .                  + dalbed      *    (1.-cld_SV(ikl))
            alb2sv(ikl) = alb2sv(ikl) + 0.05 *(cld_SV(ikl)-0.5)*SIcenH
     .                  + dalbed      *    (1.-cld_SV(ikl))
            alb3sv(ikl) = alb3sv(ikl) + 0.05 *(cld_SV(ikl)-0.5)*SIcenH
     .                  + dalbed      *    (1.-cld_SV(ikl))
            albisv(ikl) = albisv(ikl) + 0.05 *(cld_SV(ikl)-0.5)*SIcenH
     .                  + dalbed      *    (1.-cld_SV(ikl))
c #cp     END IF
 
C +--Integrated Snow/Ice/Soil Albedo: Minimum snow albedo = aI1dSV
C +  -------------------------------------------------------------
 
            albedo_old  = albisv(ikl)
            albisv(ikl) = max(albisv(ikl),aI1dSV   * SIcenH
     .                  + albssv(ikl) *(1.0        - SIcenH))
            alb1sv(ikl) = alb1sv(ikl) - 1.0/3.0             ! 33 %
     .                  * (albedo_old-albisv(ikl)) / So1dSV
            alb2sv(ikl) = alb2sv(ikl) - 1.0/3.0             ! 33 %
     .                  * (albedo_old-albisv(ikl)) / So2dSV
            alb3sv(ikl) = alb3sv(ikl) - 1.0/3.0             ! 33 %
     .                  * (albedo_old-albisv(ikl)) / So3dSV
 
 
C +--Integrated Snow/Ice/Soil Albedo: Maximum albedo = 95%
C +  -----------------------------------------------------
 
            albedo_old  = albisv(ikl)
            albisv(ikl) = min(albisv(ikl),0.95)
            alb1sv(ikl) = alb1sv(ikl) - 1.0/3.0             ! 33 %
     .                  * (albedo_old-albisv(ikl)) / So1dSV
            alb2sv(ikl) = alb2sv(ikl) - 1.0/3.0             ! 33 %
     .                  * (albedo_old-albisv(ikl)) / So2dSV
            alb3sv(ikl) = alb3sv(ikl) - 1.0/3.0             ! 33 %
     .                  * (albedo_old-albisv(ikl)) / So3dSV
 
            alb1sv(ikl) = min(max(zero,alb1sv(ikl)),albmax)
            alb2sv(ikl) = min(max(zero,alb2sv(ikl)),albmax)
            alb3sv(ikl) = min(max(zero,alb3sv(ikl)),albmax)
 
        END DO
 
 
C +--Extinction Coefficient: Exponential Factor
C +  ==========================================
 
        DO ikl=1,klonv
          sExt_1(ikl)        = 1.
          sExt_2(ikl)        = 1.
          sExt_3(ikl)        = 1.
          sEX_sv(ikl,nsno+1) = 1.
 
          coalb1(ikl) = (1.          -alb1sv(ikl))*So1dSV
          coalb2(ikl) = (1.          -alb2sv(ikl))*So2dSV
          coalb3(ikl) = (1.          -alb3sv(ikl))*So3dSV
          coalbm      =  coalb1(ikl) +coalb2(ikl) +coalb3(ikl)
          coalb1(ikl) =  coalb1(ikl)              /coalbm
          coalb2(ikl) =  coalb2(ikl)              /coalbm
          coalb3(ikl) =  coalb3(ikl)              /coalbm
        END DO
 
cXF
 
        DO   isn=nsno,1,-1
          DO ikl=1,klonv
            sEX_sv(ikl,isn) = 1.0
           !sEX_sv(ikl,isn) = 0.95 ! if MAR is too warm in summer
          END DO
        END DO
 
        DO ikl=1,klonv
         DO isn=max(1,isnoSV(ikl)),1,-1
 
          SignRo = sign(unun, rocdSV - ro__SV(ikl,isn))
          SnowOK =  max(zero,SignRo) ! Ice Density Threshold
 
          RoFrez =  1.e-3      * ro__SV(ikl,isn) * (1.0-eta_SV(ikl,isn))
 
          OpSqrt = sqrt(max(epsi,SnOpSV(ikl,isn)))
          exarg1 =      SnowOK  *1.e2 *max(sbeta1*RoFrez/OpSqrt,sbeta2)
     .            +(1.0-SnowOK)           *sbeta5
          exarg2 =      SnowOK  *1.e2 *max(sbeta3*RoFrez/OpSqrt,sbeta4)
     .            +(1.0-SnowOK)           *sbeta5
          exarg3 =      SnowOK  *1.e2     *sbeta5
     .            +(1.0-SnowOK)           *sbeta5
 
C +--Col de Porte Snow Extinction Coefficient
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #cp     IF (ColPrt.AND.TotSol.gt.0.)                              THEN
c #cp       exarg1 = exarg1*(Dr_1SN*DirSol
c #cp.                      +Df_1SN*DifSol*(1.-cld_SV(ikl))
c #cp.                      +Dfc1SN*DifSol*    cld_SV(ikl) )
c #cp.                     /(Dr_1SN*TotSol)
c #cp       exarg2 = exarg2*(Dr_2SN*DirSol
c #cp.                      +Df_2SN*DifSol*(1.-cld_SV(ikl))
c #cp.                      +Dfc2SN*DifSol*    cld_SV(ikl) )
c #cp.                     /(Dr_2SN*TotSol)
c #cp       exarg3 = exarg3*(Dr_3SN*DirSol
c #cp.                      +Df_3SN*DifSol*(1.-cld_SV(ikl))
c #cp.                      +Dfc3SN*DifSol*    cld_SV(ikl) )
c #cp.                     /(Dr_3SN*TotSol)
c #cp     END IF
 
 
C +--Integrated Extinction of Solar Irradiance (Normalized Value)
C +  ============================================================
 
          sExt_1(ikl) = sExt_1(ikl)
     .                          * exp(min(0.0,-exarg1 *dzsnSV(ikl,isn)))
          sign_0      =              sign(unun,eps9   -sExt_1(ikl))
          sExt_0      =               max(zero,sign_0)*sExt_1(ikl)
          sExt_1(ikl) = sExt_1(ikl)                   -sExt_0
 
          sExt_2(ikl) = sExt_2(ikl)
     .                          * exp(min(0.0,-exarg2 *dzsnSV(ikl,isn)))
          sign_0      =              sign(unun,eps9   -sExt_2(ikl))
          sExt_0      =               max(zero,sign_0)*sExt_2(ikl)
          sExt_2(ikl) = sExt_2(ikl)                   -sExt_0
 
          sExt_3(ikl) = sExt_3(ikl)
     .                          * exp(min(0.0,-exarg3 *dzsnSV(ikl,isn)))
          sign_0      =              sign(unun,eps9   -sExt_3(ikl))
          sExt_0      =               max(zero,sign_0)*sExt_3(ikl)
          sExt_3(ikl) = sExt_3(ikl)                   -sExt_0
 
          sEX_sv(ikl,isn) = coalb1(ikl) * sExt_1(ikl)
     .                    + coalb2(ikl) * sExt_2(ikl)
     .                    + coalb3(ikl) * sExt_3(ikl)
        END DO
      END DO
 
      DO   isn=0,-nsol,-1
        DO ikl=1,klonv
          sEX_sv(ikl,isn) = 0.0
        END DO
      END DO
 
 
C +--Albedo: IO
C +  ==========
 
! #va IF (.NOT.aw_opn)                                              THEN
! #va          aw_opn = .true.
! #va          open(unit=46,status='unknown',file='SnOptP____.va')
! #va          rewind(   46)
! #va END IF
 
! #va     ikl=1
! #va     write(46,460)daHost
 460      format('---------------------------------+----+',
     .          '-------+-------+-------+-------+-------+-------+',
     .                                  '-------+-------+-------+',
     .         /,'Snow/Ice Pack ',a18,' |    |',
     .          ' z [m] |0.3/0.8|0.8/1.5|1.5/2.8| Full  |Opt[mm]|',
     .                                  ' G1    | G2    | ro    |',
     .         /,'---------------------------------+----+',
     .          '-------+-------+-------+-------+-------+-------+',
     .                                  '-------+-------+-------+')
!         ______________________________________________________________
! #va     write(46,461)            SIce_H,
! #va.                             alb1sv(ikl),alb2sv(ikl),alb3sv(ikl),
! #va.                             albisv(ikl)
 461      format('Integrated Snow/Ice/Soil  Albedo |',
     .            3x,' |',  f6.3,' |' ,4(f6.3,' |'), 6x ,' |',
     .                                            3( 6x ,' |'))
!         ______________________________________________________________
! #va     write(46,462)ispiSV(ikl),a_SII1,a_SII2,a_SII3,albSII
 462      format('Integrated Snow/Ice       Albedo |',
     .            i3,' |',   6x ,' |' ,4(f6.3,' |'), 6x ,' |',
     .                                            3( 6x ,' |'))
!         ______________________________________________________________
! #va     write(46,463)            rusnSV(ikl),         albWIc,
! #va.                             SWS_SV(ikl)
 463      format('Integrated Water/Bare Ice Albedo |',
     .            3x,' |',  f6.3,'w|' ,3( 6x, ' |'),
     .                                   f6.3,' |' ,f6.3,' |',
     .                                            3( 6x ,' |'))
!         ______________________________________________________________
! #va     write(46,464)     LiceOK,a_SnI1,a_SnI2,a_SnI3,albSnI
 464      format('Integrated Snow/Ice Lense Albedo |',
     .           f4.0,'|',    6x,' |' ,4(f6.3,' |'), 6x ,' |',
     .                                            3( 6x ,' |'))
!         ______________________________________________________________
! #va     write(46,465)isn1       ,zzsnsv(ikl,isn1),
! #va.                             albIc1,albIc2,albIc3,albIce,
! #va.                        1.e3*SnOpSV(ikl,max(iun,isnoSV(ikl)-iun)),
! #va.                             G1snSV(ikl,max(iun,isnoSV(ikl)-iun)),
! #va.                             G2snSV(ikl,max(iun,isnoSV(ikl)-iun)),
! #va.                             ro__SV(ikl,max(iun,isnoSV(ikl)-iun))
! #va.                      *(1. - eta_SV(ikl,max(iun,isnoSV(ikl)-iun)))
 465      format('Surficial       Ice Lense        |',
     .            i3,' |', (f6.3,'i|'),4(f6.3,' |'),f6.3,' |',
     .                                            3(f6.1,' |'))
!         ______________________________________________________________
! #va     write(46,466)isnoSV(ikl),zzsnsv(ikl,isnoSV(ikl)),
! #va.                             albSn1,albSn2,albSn3,albSno,
! #va.                        1.e3*SnOpSV(ikl,isnoSV(ikl)),
! #va.                             G1snSV(ikl,isnoSV(ikl)),
! #va.                             G2snSV(ikl,isnoSV(ikl)),
! #va.                             ro__SV(ikl,isnoSV(ikl))
! #va.                      *(1. - eta_SV(ikl,isnoSV(ikl)))
 466      format('Uppermost  Effective Snow Layer  |',
     .            i3,' |', (f6.3,'*|'),4(f6.3,' |'),f6.3,' |',
     .                                            3(f6.1,' |'))
 
      return
      end
