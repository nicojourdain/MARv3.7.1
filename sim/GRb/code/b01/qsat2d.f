 
 
      subroutine qsat2D(tair2D,pst2D,tsrf2D,qvsi2D,qvsw2D)
 
C +------------------------------------------------------------------------+
C | MAR PHYSICS                                         Mc 30-05-2007  MAR |
C |   SubRoutine qsat2D computes the Saturation Specific Humidity  (kg/kg) |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT :   TairSL: Surface Air Temperature                        (K) |
C |   ^^^^^^^   Tair2D:         Air Temperature                        (K) |
C |              pst2D: Model Pressure Thickness                     (kPa) |
C |                                                                        |
C |   OUTPUT :  qvsw2D: Saturation Specific Humidity  over Water   (kg/kg) |
C |   ^^^^^^^   qvsi2D: Saturation Specific Humidity  over Ice     (kg/kg) |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_DY.inc'
      include 'MAR_2D.inc'
 
C +--Local Variables
C +  ===============
 
      integer il,klq
 
      real    WatIce,ExpWat,ExpWa2,ExpIce
 
 
C +--DATA
C +  ====
 
      data WatIce/273.16e0/
      data ExpWat/5.138e0/
      data ExpWa2/6827.e0/
      data ExpIce/6150.e0/
 
 
C +--Temperature (K) and Pressure (hPa)
C +  ==================================
 
      DO   klq=1,klev
        DO il =1,klon
          W2xyz5(il,klq)   = tair2D(il,klq)
          W2xyz6(il,klq)   = (pst2D(il) * sigma(klq) + ptopDY) * 10.0d0
        END DO
      END DO
 
        DO il=1,klon
          W2xyz5(il,klev+1) = tsrf2D(il)
          W2xyz6(il,klev+1) = (pst2D(il)             + ptopDY) * 10.0d0
        END DO
 
 
C +--Saturation Vapor Pressure over Ice
C +  ==================================
 
      DO   klq=1,klev+1
        DO il =1,klon
          W2xyz7(il,klq) =
     .    6.1070d0 * exp (ExpIce*(unun/WatIce-unun/W2xyz5(il,klq)))
C +...    Dudhia (1989) JAS, (B1) and (B2) p.3103
 
          W2xyz8(il,klq) = .622d0*W2xyz7(il,klq)
     .  /(W2xyz6(il,klq) - .378d0*W2xyz7(il,klq))
 
 
C +--Saturation Vapor Pressure over Water
C +  ====================================
 
          W2xyz7(il,klq) =
     .    6.1078d0 * exp (ExpWat*  log(WatIce     /W2xyz5(il,klq)))
     .             * exp (ExpWa2*(unun/WatIce-unun/W2xyz5(il,klq)))
C +...    Dudhia (1989) JAS, (B1) and (B2) p.3103
C +       See also Pielke (1984), p.234 and Stull (1988), p.276
 
          qvsw2D(il,klq) = max(eps9  ,      .622d0*W2xyz7(il,klq)
     .  /(W2xyz6(il,klq) -                  .378d0*W2xyz7(il,klq)))
C +...    Saturation Vapor Specific Concentration over Water
C +       (even for temperatures less than freezing point)
 
 
C +--Water Phase Discriminator
C +  =========================
 
          W2xyz7(il,klq) = max(zero,sign(unun,W2xyz5(il,klq)-WatIce))
C +...    W2xyz7(il,klq) =     1    if        Tair     >    273.16
C +                            0    if        Tair     <    273.16
 
C +--Saturation Vapor Specific Concentration over Ice
C +  ================================================
C +
          qvsi2D(il,klq) =
     .       max(eps9    , qvsw2D(il,klq) *      W2xyz7(il,klq)
     .                   + W2xyz8(il,klq) *(unun-W2xyz7(il,klq)))
 
 
C +--Work Area Reset
C +  ===============
 
          W2xyz5(il,klq) = 0.0
          W2xyz6(il,klq) = 0.0
          W2xyz7(il,klq) = 0.0
          W2xyz8(il,klq) = 0.0
        END DO
      END DO
 
      return
      end
