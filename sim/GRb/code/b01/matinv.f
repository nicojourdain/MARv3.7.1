      subroutine matinv(wk1,wk2,l_min,l_max)
C +
C +------------------------------------------------------------------------+
C | MAR DYNAMICS LBC                                       27-09-2001  MAR |
C |   SubRoutine matinv performs a Matrix Inversion using a Companion      |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT  : wk1 : Matrix to be inverted                                 |
C |   ^^^^^^^^       Dimension : (lmin:lmax) -> jdim=lmax-lmin+1           |
C |                  This Matrix is Loss after the Matrix Inversion        |
C |                                                                        |
C |   OUTPUT : wk2 : Inverted Matrix                                       |
C |   ^^^^^^^^                                                             |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +--Global Variables
C +  ================
C +
      integer      l_min,l_max
      real     wk1(l_min:l_max,l_min:l_max),wk2(l_min:l_max,l_min:l_max)
C +
C +
C +--Local  Variables
C +  ================
C +
      integer  l ,k ,ll,llp1,llx1,llm1
      real     xx,yy
C +
C +
C +--wk2 = Compagnion Matrix = Unity Matrix
C +  --------------------------------------
C +
      DO   l=l_min,l_max
        DO k=l_min,l_max
          wk2(l,k)=0.d0
        END DO
          wk2(l,l)=1.d0
      END DO
C +
C +...Combinaisons lineaires de lignes de la matrice wk1 de facon a y faire
C +   apparaitre des 0 dans le triangle inferieur et des 1 sur la diagonale.
C +   Memes operations effectuees systematiquement sur la matrice compagnon.
C +
      ll=l_min
 50   llp1=ll+1
      xx=wk1(ll,ll)
C +
C +...Si l'element diagonal est nul,la ligne le contenant est interchangee
C +   avec une des suivantes...jusqu'a ce qu'on obtienne un element non nul.
C +   Dans le cas contraire la matrice est singuliere.
C +
      IF (xx.eq.0.0d0)                                            THEN
        llx1=ll+1
 90     IF (wk1(llx1,llx1).eq.0.0d0)                              THEN
                llx1=llx1+1
          IF   (llx1.gt.l_max) write(21,*)'Matrice non inversible'
          GO TO 90
        END IF
        DO k=l_min,l_max
          yy=wk1(ll,k)
          wk1(ll,k)=wk1(llx1,k)
          wk1(llp1,k)=yy
          yy=wk2(ll,k)
          wk2(ll,k)=wk2(llx1,k)
          wk2(llp1,k)=yy
        END DO
          xx=wk1(ll,ll)
      END IF
C +
C +...Division pour obtenir 1 sur la diagonale
C +
      DO k=l_min,l_max
          wk1(ll,k)=wk1(ll,k)/xx
          wk2(ll,k)=wk2(ll,k)/xx
      END DO
C +
C +...Soustraction pour obtenir 0 dans le triangle inferieur
C +
      DO l=llp1,l_max
          xx=wk1(l,ll)
        DO k=l_min,l_max
          wk1(l,k)=wk1(l,k)-xx*wk1(ll,k)
          wk2(l,k)=wk2(l,k)-xx*wk2(ll,k)
        END DO
      END DO
C +
C +...Passage a la ligne suivante
C +
      IF (ll.lt.l_max)                                            THEN
          ll=ll+1
        GO TO 50
      END IF
C +
C +...Combinaisons lineaires effectuees sur la matrice wk1
C +   pour y faire apparaitre des 0 dans le triangle superieur.
C +   Memes operations effectuees sur la matrice compagnon
C +
      ll=l_max
 60   llm1=ll-1
      DO l=llm1,l_min,-1
          xx=wk1(l,ll)
        DO k=l_min,l_max
          wk1(l,k)=wk1(l,k)-xx*wk1(ll,k)
          wk2(l,k)=wk2(l,k)-xx*wk2(ll,k)
        END DO
      END DO
      IF (ll.gt.l_min)                                            THEN
          ll=ll-1
        GO TO 60
      END IF
C +
      return
      end
