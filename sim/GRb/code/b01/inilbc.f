 
 
      subroutine INIlbc(ihamr_lbc,nhamr_lbc,newlbc)
 
C +------------------------------------------------------------------------+
C | MAR INPUT      ATMOS                               Tue 03-11-2009  MAR |
C |   SubRoutine INIlbc initializes MAR Lateral Boundaries                 |
C |                     verifies    MARlbc.DAT  EOF                        |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT:  ihamr_lbc : Time Digital Filter Status                       |
C |   ^^^^^   nhamr_lbc : Time Digital Filter Set Up                       |
C |                                                                        |
C |   OUTPUT: newlbc    : (0,1) ==> (NO new LBC ,new LBC)                  |
C |   ^^^^^^^                                                              |
C |                                                                        |
C |   OUTPUT (via common block)                                            |
C |   ^^^^^^  vaxgLB,vaxdLB,vayiLB,vaysLB: Current                    LBCs |
C |           v1xgLB,v1xdLB,v1yiLB,v1ysLB: Previous Nesting Time Step LBCs |
C |           v2xgLB,v2xdLB,v2yiLB,v2ysLB: Next     Nesting Time Step LBCs |
C |           tim1LB,tim2LB              : LBC Nesting      Times  n, n+1  |
C |                                                                        |
C |   CAUTION: It is assumed that tim1LB and tim2LB do not change when the |
C |   ^^^^^^^^ Variables are reassigned after the dynamical Initialization |
C |            (Reassignation => itexpe:= nham => itimar:= itimar-nham*dt) |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
 
      include 'MAR_DY.inc'
      include 'MAR_LB.inc'
 
      include 'MAR_SL.inc'
 
      integer  ihamr_lbc ,nhamr_lbc
      integer  newlbc
      real              pst__1,pst_mx
      common/INIlbcRLoc/pst__1,pst_mx
 
 
C +--Local  Variables
C +  ================
 
      integer*8  itimar,iv_ilb
      real     rate
      real     vax_An,vax_Ap,vax_A
      real     qsat0D,qs    ,tt
c #CE real     qse_0D,qse
c #FS real     rh_min,qq
c #FS real     fac_qq
 
c #FS data     rh_min/0.80/   ! relative humidity above which clouds exist
c #FS data     fac_qq/1.50/   ! prescribed specific humidity tuning at LBC
!                     1.50  ==> 0.8 + 1.5 * 0.2 = 1.1
 
C +--Current Time
C +  ============
 
          itimar=ou2sGE(iyrrGE,mmarGE,jdarGE,jhurGE,minuGE,jsecGE)
c #HF.          + (ihamr_lbc+nhamr_lbc)                           * idt
 
cXF
C WARNING Compile with -i8 if the simulation run > 50yrs!!
cXF
 
C +--New LBC
C +  =======
 
      IF (itimar.gt.   tim2LB)                                    THEN
 
          tim1LB =     tim2LB
 
          write(6,6001)jda_LB,labmGE(mma_LB),iyr_LB,
     .                 jhu_LB,                      tim1LB,
     .                 jdarGE,labmGE(mmarGE),iyrrGE,
     .                 jhurGE,minuGE,        jsecGE,itimar
 6001     format(/, '  1st LBC /',i3,'-',a3,'-',i4,i3,' ',2x,'/',2x,
     .              '   t =',i12,'s A.P.',
     .           /, '  Current /',i3,'-',a3,'-',i4,i3,':',i2,':',i2,
     .              '   t =',i12)
 
       IF (jdh_LB.eq.0)jdh_LB = -1
       open (unit=11,status='old',form='unformatted',file='MARlbc.DAT')
       rewind     11
 11    CONTINUE
       IF (jdh_LB.le.0)                                       GO TO 10
 
 
C +--LBC at nesting time step n
C +  --------------------------
 
       DO iv_ilb=1,5
         DO i=1,n7mxLB
         DO k=1,mz
         DO j=1,my
           v1xgLB(i,j,k,iv_ilb) = v2xgLB(i,j,k,iv_ilb)
           v2xgLB(i,j,k,iv_ilb) = 0.0
         END DO
         END DO
         END DO
 
         DO i=mx-n6mxLB,mx
         DO k=1,mz
         DO j=1,my
           v1xdLB(i,j,k,iv_ilb) = v2xdLB(i,j,k,iv_ilb)
           v2xdLB(i,j,k,iv_ilb) = 0.0
         END DO
         END DO
         END DO
 
         DO j=1,n7myLB
         DO k=1,mz
         DO i=1,mx
           v1yiLB(i,j,k,iv_ilb) = v2yiLB(i,j,k,iv_ilb)
           v2yiLB(i,j,k,iv_ilb) = 0.0
         END DO
         END DO
         END DO
 
         DO j=my-n6myLB,my
         DO k=1,mz
         DO i=1,mx
           v1ysLB(i,j,k,iv_ilb) = v2ysLB(i,j,k,iv_ilb)
           v2ysLB(i,j,k,iv_ilb) = 0.0
         END DO
         END DO
         END DO
 
       END DO
 
         DO j=1,my
         DO i=1,mx
           sst1LB(i,j)      = sst2LB(i,j)
         END DO
         END DO
 
 
C +--LBC at nesting time step n+1
C +  ----------------------------
 
       read      (11) iyr_LB,mma_LB,jda_LB,jhu_LB,jdh_LB
       read      (11) v2xgLB,v2xdLB,v2yiLB,v2ysLB
       read      (11) sst2LB
 
       tim2LB=ou2sGE(iyr_LB,mma_LB,jda_LB,jhu_LB,0,0)
 
       DO iv_ilb=1,2
         DO i=1,n7mxLB
         DO k=1,mz
         DO j=1,my
           v2xgLB(i,j,k,iv_ilb) = v2xgLB(i,j,k,iv_ilb)  / SFm_DY(i,j)
         END DO
         END DO
         END DO
 
         DO i=mx-n6mxLB,mx
         DO k=1,mz
         DO j=1,my
           v2xdLB(i,j,k,iv_ilb) = v2xdLB(i,j,k,iv_ilb)  / SFm_DY(i,j)
         END DO
         END DO
         END DO
 
         DO j=1,n7myLB
         DO k=1,mz
         DO i=1,mx
           v2yiLB(i,j,k,iv_ilb) = v2yiLB(i,j,k,iv_ilb)  / SFm_DY(i,j)
         END DO
         END DO
         END DO
 
         DO j=my-n6myLB,my
         DO k=1,mz
         DO i=1,mx
           v2ysLB(i,j,k,iv_ilb) = v2ysLB(i,j,k,iv_ilb)  / SFm_DY(i,j)
         END DO
         END DO
         END DO
 
       END DO
 
       IF(itimar.gt.tim2LB)                                   GO TO 11
 
          write(6,6002)jda_LB,labmGE(mma_LB),iyr_LB,
     .                 jhu_LB,jdh_LB,               tim2LB
 6002     format(   '  2nd LBC /',i3,'-',a3,'-',i4,i3,' ',2x,'/(',i1,
     .              ')  t =',i12)
 
 10    CONTINUE
       close(unit=11)
 
 
C +--Force Sursaturation where relative humidity larger than rh_min (i.e., 80%)
C +  --------------------------------------------------------------------------
 
          DO i=1,n7mxLB
          DO k=1,mz
          DO j=1,my
            tt =                    v2xgLB(i,j,k,4)
     .                 *exp(cap*log(v2xgLB(i,j,1,5)*sigma(k)+ptopDY))
            qs = qsat0D(tt,sigma(k),v2xgLB(i,j,1,5),ptopDY,0)
c #FS       qq = qs                                           * rh_min
c #CE       qse= qse_0D(tt,sigma(k),v2xgLB(i,j,1,5),ptopDY)
c #FS       v2xgLB(i,j,k,3) =   min(v2xgLB(i,j,k,3),qq)
c #FS.                  +fac_qq*max(v2xgLB(i,j,k,3)-qq,0.)
c #CE       v2xgLB(i,j,k,3) =       v2xgLB(i,j,k,3)*qs/qse
          END DO
          END DO
          END DO
 
          DO i=mx-n6mxLB,mx
          DO k=1,mz
          DO j=1,my
            tt =                    v2xdLB(i,j,k,4)
     .                 *exp(cap*log(v2xdLB(i,j,1,5)*sigma(k)+ptopDY))
            qs = qsat0D(tt,sigma(k),v2xdLB(i,j,1,5),ptopDY,0)
c #FS       qq = qs                                           * rh_min
c #CE       qse= qse_0D(tt,sigma(k),v2xdLB(i,j,1,5),ptopDY)
c #FS       v2xdLB(i,j,k,3) =   min(v2xdLB(i,j,k,3),qq)
c #FS.                  +fac_qq*max(v2xdLB(i,j,k,3)-qq,0.)
c #CE       v2xdLB(i,j,k,3) =       v2xdLB(i,j,k,3)*qs/qse
          END DO
          END DO
          END DO
 
          DO j=1,n7myLB
          DO k=1,mz
          DO i=1,mx
            tt =                    v2yiLB(i,j,k,4)
     .                 *exp(cap*log(v2yiLB(i,j,1,5)*sigma(k)+ptopDY))
            qs = qsat0D(tt,sigma(k),v2yiLB(i,j,1,5),ptopDY,0)
c #FS       qq = qs                                           * rh_min
c #CE       qse= qse_0D(tt,sigma(k),v2yiLB(i,j,1,5),ptopDY)
c #FS       v2yiLB(i,j,k,3) =   min(v2yiLB(i,j,k,3),qq)
c #FS.                  +fac_qq*max(v2yiLB(i,j,k,3)-qq,0.)
c #CE       v2yiLB(i,j,k,3) =       v2yiLB(i,j,k,3)*qs/qse
          END DO
          END DO
          END DO
 
          DO j=my-n6myLB,my
          DO k=1,mz
          DO i=1,mx
            tt =                    v2ysLB(i,j,k,4)
     .                 *exp(cap*log(v2ysLB(i,j,1,5)*sigma(k)+ptopDY))
            qs = qsat0D(tt,sigma(k),v2ysLB(i,j,1,5),ptopDY,0)
c #FS       qq = qs                                           * rh_min
c #CE       qse= qse_0D(tt,sigma(k),v2ysLB(i,j,1,5),ptopDY)
c #FS       v2ysLB(i,j,k,3) =   min(v2ysLB(i,j,k,3),qq)
c #FS.                  +fac_qq*max(v2ysLB(i,j,k,3)-qq,0.)
c #CE       v2ysLB(i,j,k,3) =       v2ysLB(i,j,k,3)*qs/qse
          END DO
          END DO
          END DO
 
      ELSE
c #WR     write(6,6003)jdarGE,labmGE(mmarGE),iyrrGE,
c #WR.                 jhurGE,minuGE,        jsecGE,itimar
 6003     format(   '  Current /',i3,'-',a3,'-',i4,i3,':',i2,':',i2,
     .              '   t =',i12,'s A.P.')
      END IF
 
 
C +--Time Interpolation
C +  ==================
 
      IF            (itimar.le.tim2LB  .and.   tim1LB.lt.tim2LB)  THEN
 
        rate = float(itimar  - tim1LB) / float(tim2LB  - tim1LB)
        DO iv_ilb=1,5
          DO i=1,n7mxLB
          DO k=1,mz
          DO j=1,my
            vaxgLB(i,j,k,iv_ilb)=v1xgLB(i,j,k,iv_ilb) +
     .     (v2xgLB(i,j,k,iv_ilb)-v1xgLB(i,j,k,iv_ilb))*rate
          END DO
          END DO
          END DO
 
          DO i=mx-n6mxLB,mx
          DO k=1,mz
          DO j=1,my
            vaxdLB(i,j,k,iv_ilb)=v1xdLB(i,j,k,iv_ilb) +
     .     (v2xdLB(i,j,k,iv_ilb)-v1xdLB(i,j,k,iv_ilb))*rate
          END DO
          END DO
          END DO
 
          DO j=1,n7myLB
          DO k=1,mz
          DO i=1,mx
            vayiLB(i,j,k,iv_ilb)=v1yiLB(i,j,k,iv_ilb) +
     .     (v2yiLB(i,j,k,iv_ilb)-v1yiLB(i,j,k,iv_ilb))*rate
          END DO
          END DO
          END DO
 
          DO j=my-n6myLB,my
          DO k=1,mz
          DO i=1,mx
            vaysLB(i,j,k,iv_ilb)=v1ysLB(i,j,k,iv_ilb) +
     .     (v2ysLB(i,j,k,iv_ilb)-v1ysLB(i,j,k,iv_ilb))*rate
          END DO
          END DO
          END DO
        END DO
 
 
C +--Zonally Averaged Version
C +  ------------------------
 
        IF   (mmy   .eq.1)                                        THEN
          IF (itexpe.eq.1)                                        THEN
              pst__1 = vaxgLB( 1,1,1,5)
              pst_mx = vaxdLB(mx,1,1,5)
          END IF
 
C +--LBC: Smooth Set up of the Large Scale Wind
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #IN     IF        (itexpe*dt.lt.86400.)                         THEN
c #IN         rate = itexpe*dt   /86400.
c #IN       DO iv_ilb=1,2
c #IN       DO i=1,n7mxLB
c #IN       DO k=1,mz
c #IN       DO j=1,my
c #IN         vaxgLB(i,j,k,iv_ilb)=vaxgLB(i,j,k,iv_ilb) * rate
c #IN       END DO
c #IN       END DO
c #IN       END DO
 
c #IN       DO i=mx-n6mxLB,mx
c #IN       DO k=1,mz
c #IN       DO j=1,my
c #IN         vaxdLB(i,j,k,iv_ilb)=vaxdLB(i,j,k,iv_ilb) * rate
c #IN       END DO
c #IN       END DO
c #IN       END DO
c #IN       END DO
c #IN     END IF
 
C +--Meridional Wind: zero Mass Flux Correction, excedent Mass Flux Reduction
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
             iv_ilb=2
             j= 1
          DO i= 1,n7mxLB
                  vax_An =  0.
                  vax_Ap =  0.
              DO k=1,mz
                  vax_An =  vax_An +min(real(vaxgLB(i,j,k,iv_ilb)),0.)
     .                                      *dsigm1(k)
                  vax_Ap =  vax_Ap +max(real(vaxgLB(i,j,k,iv_ilb)),0.)
     .                                      *dsigm1(k)
              END DO
                  vax_A  =  vax_An+vax_Ap
            IF   (vax_A.gt.0.)                                    THEN
                  vax_A  = (vax_Ap-vax_A)/max( epsi,vax_Ap)
              DO k=1,mz
              IF (vaxgLB(i,j,k,iv_ilb).gt.0.)
     .            vaxgLB(i,j,k,iv_ilb) = vaxgLB(i,j,k,iv_ilb) * vax_A
              END DO
            ELSE
                  vax_A  = (vax_An-vax_A)/min(-epsi,vax_An)
              DO k=1,mz
              IF (vaxgLB(i,j,k,iv_ilb).lt.0.)
     .            vaxgLB(i,j,k,iv_ilb) = vaxgLB(i,j,k,iv_ilb) * vax_A
              END DO
            END IF
          END DO
 
          DO i=mx-n6mxLB,mx
                  vax_An =  0.
                  vax_Ap =  0.
              DO k=1,mz
                  vax_An =  vax_An +min(real(vaxdLB(i,j,k,iv_ilb)),0.)
     .                                      *dsigm1(k)
                  vax_Ap =  vax_Ap +max(real(vaxdLB(i,j,k,iv_ilb)),0.)
     .                                      *dsigm1(k)
              END DO
                  vax_A  =  vax_An+vax_Ap
            IF   (vax_A.gt.0.)                                    THEN
                  vax_A  = (vax_Ap-vax_A)/max( epsi,vax_Ap)
              DO k=1,mz
              IF (vaxdLB(i,j,k,iv_ilb).gt.0.)
     .            vaxdLB(i,j,k,iv_ilb) = vaxdLB(i,j,k,iv_ilb) * vax_A
              END DO
            ELSE
                  vax_A  = (vax_An-vax_A)/min(-epsi,vax_An)
              DO k=1,mz
              IF (vaxdLB(i,j,k,iv_ilb).lt.0.)
     .            vaxdLB(i,j,k,iv_ilb) = vaxdLB(i,j,k,iv_ilb) * vax_A
              END DO
            END IF
          END DO
 
C +--Surface Pressure:  no Mass       Variation
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          DO i= 1,n7mxLB
                  vaxgLB(i,1,1,5) = pst__1                             ! [kPa]
          END DO
          DO i=mx-n6mxLB,mx
                  vaxdLB(i,1,1,5) = pst_mx                             ! [kPa]
          END DO
        END IF
 
 
C +--Sea Surface Temperatures
C +  ------------------------
 
          DO j=1,my
          DO i=1,mx
c #AO       sst1LB(i,j)     =sst_LB(i,j)
c #AO       sst2LB(i,j)     =sst_LB(i,j)
            sst_LB(i,j)     =sst1LB(i,j)      +
     .     (sst2LB(i,j)     -sst1LB(i,j)     )*rate
          END DO
          END DO
 
        newlbc = 1
 
      ELSE
        newlbc = 0
      END IF
 
      return
      end
