C
C***********************************************************************
C
      REAL             FUNCTION SLAMC3( A, B )
C
C  -- LAPACK auxiliary routine (version 1.0) --
C     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
C     Courant Institute, Argonne National Lab, and Rice University
C     February 29, 1992
C
C     .. Scalar Arguments ..
      REAL               A, B
C     ..
C
C  Purpose
C  =======
C
C  SLAMC3  is intended to force  A  and  B  to be stored prior to doing
C  the addition of  A  and  B ,  for use in situations where optimizers
C  might hold one of these in a register.
C
C  Arguments
C  =========
C
C  A, B    (input) REAL
C          The values A and B.
C
C
C     .. Executable Statements ..
C
      SLAMC3 = A + B
C
      RETURN
C
C     End of SLAMC3
C
      END
