 
 
      subroutine TURtke_advv(dt_dif)
 
C +------------------------------------------------------------------------+
C | MAR TURBULENCE (TKE)                                   14-09-2001  MAR |
C |   SubRoutine TURtke_advv includes TKE Vertical Advection Contribution  |
C |              solved by a 1st Order Accurate in Space Upstream Scheme   |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |  INPUT / OUTPUT: The Vertical Turbulent Fluxes are included for:       |
C |  ^^^^^^^^^^^^^^                                                        |
C |       a) Turbulent Kinetic Energy             ect_TE(mx,my,mz) [m2/s2] |
C |       b) Turbulent Kinetic Energy Dissipation eps_TE(mx,my,mz) [m2/s3] |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
 
      include 'MAR_DY.inc'
      include 'MAR_TE.inc'
 
      include 'MAR_WK.inc'
 
      real     dt_dif
 
 
C +--Local  Variables
C +  ================
 
      real     tran
 
 
C +--Vertical Courant Number
C +  =======================
 
      IF (staggr)                                                   THEN
        DO  k=1,mmz1
        DO  j=1,my
        DO  i=1,mx
          WKxyz3(i,j,k) =                          psigDY(i,j,k)
        END DO
        END DO
        END DO
      ELSE
        DO  k=1,mmz1
        DO  j=1,my
        DO  i=1,mx
          WKxyz3(i,j,k) = 0.50*(psigDY(i,j,kp1(k))+psigDY(i,j,k))
        END DO
        END DO
        END DO
      END IF
 
        DO  k=1,mmz1
        DO  j=1,my
        DO  i=1,mx
          WKxyz1(i,j,k) = - max(zero,WKxyz3(i,j,k))
     .                      * dt_dif/(pstDYn(i,j)*dsigm1(k)  )
          WKxyz2(i,j,k) = - min(zero,WKxyz3(i,j,k))
     .                      * dt_dif/(pstDYn(i,j)*dsigm1(k+1))
        END DO
        END DO
        END DO
 
            k=  mz
        DO  j=1,my
        DO  i=1,mx
          WKxyz1(i,j,k) = - max(zero,WKxyz3(i,j,k))
     .                      * dt_dif/(pstDYn(i,j)*dsigm1(k)  )
        END DO
        END DO
 
 
C +--Vertical Advection of TKE and TKE Dissipation
C +  =============================================
 
        DO  k=kp1(1),mz
        DO  j=1,my
        DO  i=1,mx
          WKxyz4(i,j,k) = ect_TE(i,j,k)-ect_TE(i,j,k-1)
          WKxyz5(i,j,k) = eps_TE(i,j,k)-eps_TE(i,j,k-1)
        END DO
        END DO
        END DO
 
        DO  k=kp1(1),mmz1
        DO  j=1,my
        DO  i=1,mx
          tran         =    zero
          tran         =WKxyz1(i,j,k)*WKxyz4(i,j,k)
     .                 +WKxyz2(i,j,k)*WKxyz4(i,j,k+1)
          ect_TE(i,j,k)=ect_TE(i,j,k) + tran
          eps_TE(i,j,k)=eps_TE(i,j,k)
     .                 +WKxyz1(i,j,k)*WKxyz5(i,j,k)
     .                 +WKxyz2(i,j,k)*WKxyz5(i,j,k+1)
 
          tranTE(i,j,k)=tranTE(i,j,k) + tran / dt_dif
 
        END DO
        END DO
        END DO
 
 
C +--Work Arrays Reset
C +  =================
 
        DO  k=1,mz
        DO  j=1,my
        DO  i=1,mx
          WKxyz1(i,j,k) = 0.0
          WKxyz2(i,j,k) = 0.0
          WKxyz3(i,j,k) = 0.0
          WKxyz4(i,j,k) = 0.0
          WKxyz5(i,j,k) = 0.0
        END DO
        END DO
        END DO
 
      return
      end
