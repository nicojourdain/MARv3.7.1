 
 
      subroutine SISVAT_qVg
 
C +------------------------------------------------------------------------+
C | MAR          SISVAT_qVg                                22-09-2001  MAR |
C |   SubRoutine SISVAT_qVg computes the Canopy Water  Balance             |
C |                                  including  Root   Extraction          |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   PARAMETERS:  klonv: Total Number of columns =                        |
C |   ^^^^^^^^^^        = Total Number of continental     grid boxes       |
C |                     X       Number of Mosaic Cell per grid box         |
C |                                                                        |
C |   INPUT:   ivgtSV   = 0,...,12:   Vegetation Type                      |
C |   ^^^^^               0:          Water, Solid or Liquid               |
C |                                                                        |
C |   INPUT:   rhT_SV   : SBL Top    Air  Density                  [kg/m3] |
C |   ^^^^^    QaT_SV   : SBL Top    Specific  Humidity            [kg/kg] |
C |                                                                        |
C |            TvegSV   : Canopy   Temperature                         [K] |
C |            rrCaSV   : Canopy     Water     Content             [kg/m2] |
C |            rrMxsv   : Canopy Maximum Intercepted Rain          [kg/m2] |
C |            rah_sv   : Aerodynamic Resistance for Heat            [s/m] |
C |            EvT_sv   : EvapoTranspiration                       [kg/m2] |
C |            Sigmsv   : Canopy Ventilation Factor                    [-] |
C |            glf_sv   : Green Leaf Fraction of NOT fallen Leaves     [-] |
C |            LAIesv   : Leaf Area  Index (effective / transpiration) [-] |
C |            psi_sv   : Soil       Water    Potential                [m] |
C |            Khydsv   : Soil   Hydraulic    Conductivity           [m/s] |
C |                                                                        |
C |   INPUT /  psivSV   : Leaf       Water    Potential                [m] |
C |   OUTPUT:                                                              |
C |   ^^^^^^                                                               |
C |                                                                        |
C |   OUTPUT:  Rootsv   : Root Water Pump                        [kg/m2/s] |
C |   ^^^^^^                                                               |
C |                                                                        |
C |   Internal Variables:                                                  |
C |   ^^^^^^^^^^^^^^^^^^                                                   |
C |                                                                        |
C |   REMARK: Water Extraction by roots calibrated by Evapotranspiration   |
C |   ^^^^^^  (computed in the Canopy Energy Balance)                      |
C |                                                                        |
C | # OPTIONS: #KW: Root Water Flow slowed by Soil Hydraulic Conductivity  |
C | # ^^^^^^^                                                              |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include  "MARphy.inc"
      include  "MAR_SV.inc"
      include  "MARdSV.inc"
      include  "MAR0SV.inc"
 
      include  "MARxSV.inc"
      include  "MARySV.inc"
!$OMP threadprivate(/xSISVAT_I/,/xSISVAT_R/,/ySISVAT_I/,/ySISVAT_R/)
 
C +--Internal Variables
C +  ==================
 
      integer   ikl   ,isl                    ! Grid Point, Layer Indices
      integer   nitmax,nit                    ! Iterations Counter
      real      PlantW(klonv)                 ! Plant  Water
      real      dPdPsi(klonv)                 ! Plant  Water psi Derivative
      real      psidif                        ! Soil-Canopy  Water Pot. Differ.
      real      Root_W                        ! Root   Water     Flow
      real      RootOK                        ! Roots take Water in Soil Layer
      real      d_psiv                        ! Canopy Water     Increment
      real      dpvMAX                        ! Canopy Water     Increment MAX
      real      BWater                        ! Imbalance of Canopy Water Budg.
      real      BW_MAX                        ! MAX Imbal.of Canopy Water Budg.
      real      BW_MIN                        ! MIN Imbal.of Canopy Water Budg.
      real      dBwdpv                        ! Derivativ.of Canopy Water Budg.
      real      Bswich                        ! Newton-Raphson         Switch
      real      psiv_0(klonv)                 ! Canopy Temperature,  Previous t
      real      EvFrac                        ! Condensat./Transpiration Switch
      real      den_qs,arg_qs,qsatvg          ! Canopy Saturat. Spec. Humidity
      real      EvTran                        ! EvapoTranspiration
      real      dEdpsi                        ! Evapotranspiration  Derivative
      real      Fac_Ev,FacEvT                 ! Evapotranspiration  Factors
      real      denomE                        ! Evapotranspiration  Denominator
      real      F_Stom                        ! Funct.  (Leaf Water Potential)
      real      dFdpsi                        ! Derivative  of F_Stom
      real      denomF                        ! Denominator of F_Stom
      real      F___OK                        ! (psi>psi_c) => F_Stom swich  ON
      real      R0Stom                        ! Minimum Stomatal Resistance
      real      R_Stom                        !         Stomatal Resistance
      real      dRdpsi                        ! Derivat.Stomatal Resistance
      real      numerR                        ! Numerat.Stomatal Resistance
 
 
C +--Internal DATA
C +  =============
 
      data      nitmax /   5     /            ! Maximum  Iterations    Number
      data      dpvMAX /  20.    /            ! Canopy   Water   Increment MAX
      data      BW_MIN /   4.e-8 /            ! MIN Imbal. of Surf.Energy Budg.
 
 
C +--Newton-Raphson Scheme
C +  =====================
 
      nit    = 0
  101 CONTINUE
      nit    = nit + 1
      BW_MAX = 0.
 
 
C +--W.Potential of the Previous Time Step
C +  -------------------------------------
 
        DO ikl=1,klonv
          psiv_0(ikl) = psivSV(ikl)
 
 
C +--Extraction of Soil Water through the Plant Roots
C +  ------------------------------------------------
 
          PlantW(ikl)     = 0.                              ! Plant Water
          dPdPsi(ikl)     = 0.                              ! Idem, Derivat.
        END DO
      DO   isl=-nsol,0
        DO ikl=1,klonv
          psidif = psivSV(ikl)-(DH_dSV(ivgtSV(ikl))         ! Soil-Canopy Water
     .                         +psi_sv(       ikl ,isl))    ! Potential  Diff.
          Root_W = Ro_Wat     * RF__SV(ivgtSV(ikl),isl)     ! If > 0, Contrib.
     .              /max(eps_21,PR_dSV(ivgtSV(ikl))         !     to Root Water
c #KW.                         +Khydsv(ikl   ,isl )*1.e-4   ! (DR97, eqn.3.20)
     .                                                   )  !
C +!!!    Pas de prise en compte de la resistance sol/racine dans proto-svat
C +       (DR97, eqn.3.20)
          RootOK =      max(zero, sign(unun,psidif))
          Rootsv(ikl,isl) = Root_W*max(zero,psidif)         ! Root  Water
          PlantW(ikl)     = PlantW(ikl) + Rootsv(ikl ,isl)  ! Plant Water
          dPdPsi(ikl)     = dPdPsi(ikl) + RootOK*Root_W     ! idem, Derivat.
        END DO
      END DO
 
 
C +--Latent   Heat Flux
C +  ------------------
 
C +--Canopy Saturation Specific Humidity
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        DO ikl=1,klonv
          den_qs =         TvegSV(ikl)- 35.8
          arg_qs = 17.27 *(TvegSV(ikl)-273.16)/den_qs
          qsatvg = .0038 * exp(arg_qs)
 
C +--Canopy Stomatal Resistance
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^
          R0Stom = min(         StodSV(ivgtSV(ikl))
     .                /max(epsi,glf_sv(       ikl)),StxdSV) ! Min Stomatal R.
          denomF =              pscdSV-psivSV(ikl)
          F___OK =     max(zero,sign(unun,denomF))
          denomF =     max(epsi,          denomF)           !
          F_Stom =              pscdSV  / denomF            ! F(Leaf Wat.Pot.)
          dFdpsi =             -F_Stom  / denomF            !
C +                                                         ! DR97, eqn. 3.22
          numerR = R0Stom / max(LAIesv(ikl), R0Stom/StxdSV) !
          R_Stom = numerR *                  F_Stom         ! Can.Stomatal R.
C +                                                         ! DR97, eqn. 3.21
          dRdpsi = R_Stom *                  dFdpsi         !
 
C +--Evaporation / Evapotranspiration
C +  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          EvFrac = max(zero, sign(unun,QaT_SV(ikl)-qsatvg)) ! Condensation/
          EvFrac = EvFrac                                   ! Transpiration
     .       + (1.-EvFrac)     *rrCaSV(ikl)/ rrMxsv(ikl)    !        Switch
          Fac_Ev = rhT_SV(ikl) *Sigmsv(ikl)                 ! idem,  Factor
          denomE = rah_sv(ikl) +R_Stom     * Sigmsv(ikl)
          FacEvT = Fac_Ev * (1.-EvFrac)    / denomE         !
          EvTran = FacEvT     *(qsatvg     - QaT_SV(ikl))   ! EvapoTranspir.
          dEdpsi =(EvTran     / denomE)    * dRdpsi         ! EvT Derivative
 
 
C +--Imbalance  of the Canopy  Water  Budget
C +  ---------------------------------------
 
          BWater      =( PlantW(ikl)                        ! Available  Water
     .                 - EvTran     )*        F___OK        ! Transpired Water
 
          Bswich      = max(zero,                           ! Newton-Raphson
     .                      sign(unun,    abs(BWater)       !         Switch
     .                                       -BW_MIN))      !
 
 
C +--Derivative of the Canopy  Water  Budget
C +  ---------------------------------------
 
          dBwdpv      = dPdpsi(ikl)
     .                - dEdpsi
          dBwdpv      = sign(  unun,    dBwdpv)             !
     .                *  max(eps_21,abs(dBwdpv))            !
 
 
C +--Update Canopy and Surface/Canopy Temperatures
C +  ---------------------------------------------
 
          d_psiv      = BWater       / dBwdpv               !
          d_psiv      =      sign(unun,d_psiv)              ! Increment
     .                       *min( abs(d_psiv)     ,dpvMAX) ! Limitor
          psivSV(ikl) = psivSV(ikl)  - Bswich      *d_psiv  ! Newton-Raphson
          BW_MAX      = max(BW_MAX,abs(BWater))
        END DO
 
 
C +--Update Root Water Fluxes | := Evapotranspiration
C +  ------------------------------------------------
 
      DO   isl=-nsol,0
        DO ikl=1,klonv
          Rootsv(ikl,isl) = Rootsv(ikl,isl)*EvT_SV(ikl)     ! Root  Water
     .                          /max(eps_21,PlantW(ikl))    !
        END DO
      END DO
 
      IF (BW_MAX.gt.BW_MIN.and.nit.lt.nitmax)    GO TO 101
 
      return
      end
