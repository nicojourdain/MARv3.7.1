      function SRFdia_sno(pgran1,pgran2)
C +
C +------------------------------------------------------------------------+
C | MAR SURFACE                                            01-12-2001  MAR |
C |   Function SRFdia_sno computes the optical Grain Size                  |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   METHOD: cette fonction rend le diametre optique des grains           |
C |   ^^^^^^^ dont le type est decrit par les deux variables descriptives  |
C |                continues sur la plage -99/+99 passees en appel.        |
C |           la taille optique (1/10mm) des etoiles,                      |
C |                                          grains fins et                |
C |                                          jeunes faces planes           |
C |           sert de base de calcul                                       |
C |                                                                        |
C |           this function gives the optical diameter of the grains       |
C |                         described with the CROCUS formalism            |
C |                         pgran1 / pgran2                                |
C |                                                                        |
C |   REFER. : Brun et al.      1992, J. Glaciol 38 pp.  13-- 22           |
C |   ^^^^^^^^ Eric Martin Sept.1996                                       |
C |            (CROCUS Model, adapted to MAR at CEN by H.Gallee)           |
C |                                                                        |
C |    INPUT : pgran1  Dendricity (<0)  or Sphericity (>0) of Snow layer   |
C |    ^^^^^^^ pgran2  Sphericity (>0)  os Size            of Snow layer   |
C |                                                                        |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +-- General Variables
C +   =================
C +
      include 'MARdim.inc'
C +
      include 'MARphy.inc'
      include 'MAR_SV.inc'
      include 'MAR_SN.inc'
C +
      real     SRFdia_sno
      real     pgran1,pgran2
C +
C +
C +-- Anderson Formulation
C +   ====================
C +
C +    dsno     = 1.6d-4
C +  .          + 1.1d-13 *(roSNow(i,j,mg1)*roSNow(i,j,mg1)
C +  .                     *roSNow(i,j,mg1)*roSNow(i,j,mg1))
C +... dsno     : Grain Size (Loth et al. 1993 JGR 98 D6 p.10454)
C +                     from  Anderson    1976
C +
C +
C +-- Neige     dendritique
C +   =====================
C +
      if (pgran1.lt.-epsi)then
C +
         SRFdia_sno=-pgran1*DiEtSN/vg1SNo
     .        +(1.d0+pgran1       /vg1SNo)*
     .              (pgran2*DiGfSN/vg1SNo
     .        +(1.d0-pgran2       /vg1SNo)*DiFcSN)
C +
C +
C +-- Neige non dendritique
C +   =====================
C +
      else
C +
         SRFdia_sno= pgran2*pgran1/vg1SNo
     .          +max(DiFcSN,pgran2*demi)
     .        *(1.d0-pgran1       /vg1SNo)
C +
      endif
C +
C +
C +-- Unite S.I.
C +   ==========
C +
      SRFdia_sno=1.e-4 * SRFdia_sno
C +
      return
      end
