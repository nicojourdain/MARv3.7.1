 
      subroutine DYNdps_mp(norder)
 
C +------------------------------------------------------------------------+
C | MAR DYNAMICS   FAST                                    10-08-2004  MAR |
C |   SubRoutine DYNdps solves the Mass Conservation Equation              |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT/  (via common block)                                           |
C |   ^^^^^         iterun          : long  time step counter              |
C |                 itFast          : short time step counter              |
C |                 norder          : numerical scheme: order of precision |
C |                                                                        |
C |   INPUT/  (via common block)                                           |
C |   OUTPUT        pstDYn(mx,my)   : Pressure Depth p*(t)           (kPa) |
C |   ^^^^^^         pstDY(mx,my)   : Pressure Depth p*(t-dt)              |
C |                                                                        |
C |   INPUT   (via common block)                                           |
C |   ^^^^^         uairDY(mx,my,mz): x-Wind Speed                   (m/s) |
C |                 vairDY(mx,my,mz): y-Wind Speed                   (m/s) |
C |                                                                        |
C |   OUTPUT  (via common block)                                           |
C |   ^^^^^^  psigDY: p* X Vertical Wind Speed; Sigma Syst.(i.e. p* Ds/Dt) |
C |                   psigDY Computed  ON the Sigma Levels   (unstaggered) |
C |                                    IN           Layers     (staggered) |
C |                                                                        |
C |   METHOD: Implicit Time Scheme (pImplc  Switch is .true. )             |
C |   ^^^^^^       2th order accurate Time  Scheme (semi-implicit)   .AND. |
C |                2th order accurate Space Scheme on Arakawa A grid       |
C |                                                                        |
C |           Explicit Time Scheme (pImplc  Switch is .false.)             |
C |            Centered     Scheme (center  Switch is .true. )             |
C |                2th order accurate Time  Scheme (leapfrog backw.) .AND. |
C |   norder.EQ.2 (2th order accurate Space Scheme on Arakawa A grid .OR.  |
C |   norder.NE.2  4th order accurate Space Scheme on Arakawa A grid)      |
C |                                                                   .OR. |
C |            Non-Centered Scheme (center  Switch is .false.)             |
C |                0th order accurate Space Scheme (Bott) (norder=0)  .OR. |
C |                4th order accurate Space Scheme (Bott) (norder=4)       |
C |                                                                        |
C |            Robert Time Filter may be used to remove computational mode |
C |                                                                        |
C |   REFER.:  Use of A grid: Purser and Leslie, MWR 116, 2069--2080, 1988 |
C |   ^^^^^^   Time Scheme  : Lin    and Rood    MWR 124, 2046--2070, 1996 |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
 
      include 'MAR_DY.inc'
c #ON include 'MAR_TE.inc'
 
      include 'MAR_WK.inc'
 
      integer  norder
 
 
C +--Local  Variables
C +  ================
 
      logical  pImplc,locorr
 
      integer   mxx     ,myy
      parameter(mxx=mx+1,myy=my+1)
      real      vecx(0:mxx),flux(0:mxx)
      real      aa0x(0:mxx),aa1x(0:mxx),aa2x(0:mxx)
      real                  aa3x(0:mxx),aa4x(0:mxx)
      real      cnpx(0:mxx),cnmx(0:mxx)
      real      sipx(0:mxx),simx(0:mxx),sidx(0:mxx)
      real      vecy(0:myy),fluy(0:myy)
      real      aa0y(0:myy),aa1y(0:myy),aa2y(0:myy)
      real                  aa3y(0:myy),aa4y(0:myy)
      real      cnpy(0:myy),cnmy(0:myy)
      real      sipy(0:myy),simy(0:myy),sidy(0:myy)
 
      integer it_pst,nt_pst,idir_x,jdir_y
      integer i1_dps,i2_dps,j1_dps,j2_dps,k1_dps,k2_dps
      real    alphpp,betapp,Fp__pp,Fpa_pp,Fpb_pp,facovr
      real    CorArg,CorrNH,SRes_0,SRes_1,SRes10,pst_n1
      real    dtcorr,dtxfas,dtyfas,uuface,vvface
 
      integer              numdps,ntpsig
      common  /DYNdps_int/ numdps,ntpsig
 
      real                 psigad(mx,my,mz)
      common  /DYNdps_rea/ psigad
 
 
C +--DATA
C +  ====
 
      data     pImplc/.false./
C +...         pImplc=.true.  ==> Implicit Scheme is used to damp Lamb Waves
 
               numdps    = numdps+1
 
 
C +--Save Mass at the Lateral Boundaries
C +  ===================================
 
        do     j=1,my
        do     i=1,mx
           WTxy1( i, j) = pstDY( i,j)
C +                       p*(n-1)
           WTxy2( i, j) = pstDYn(i,j)
C +                       p*(n)
        end do
        end do
 
         do    j=1,my
           pstDY(  1,j) = pstDYn( 1,j)
           pstDY( mx,j) = pstDYn(mx,j)
         end do
 
        if (mmy.gt.1) then
         do    i=1,mx
           pstDY( i, 1) = pstDYn(i, 1)
           pstDY( i,my) = pstDYn(i,my)
         end do
        end if
 
 
C ++++++++++++++++++++++++++++++++++
C +--IMPLICIT SCHEME       (BEGIN) +
C ++++++++++++++++++++++++++++++++++
 
 
      IF   (pImplc)                                                 THEN
        IF (it_Mix.EQ.1.AND.itFast.EQ.1)                            THEN
 
 
C +--Horizontal Wind Speed: Average along the Vertical
C +  -------------------------------------------------
 
          DO j=1,my
          DO i=1,mx
            WTxy3(i,j) = 0.0
            WTxy4(i,j) = 0.0
          END DO
          END DO
 
          DO k=1,mz
          DO j=1,my
          DO i=1,mx
            WTxy3(i,j) = WTxy3(i,j) + uairDY(i,j,k) * dsigm1(k)
            WTxy4(i,j) = WTxy4(i,j) + vairDY(i,j,k) * dsigm1(k)
          END DO
          END DO
          END DO
 
 
C +--Tridiagonal Matrix Coefficients
C +  -------------------------------
 
            betapp    = 0.6
            alphpp    = 1.0    - betapp
            Fp__pp    = dt     / dx
            Fpa_pp    = Fp__pp * alphpp
            Fpb_pp    = Fp__pp * betapp
          DO  i = ip11,mx1
          DO  j = jp11,my1
            WTxyz1(i,j,1) = Fpb_pp *WTxy3(ip1(i),j) ! k=1: 3-Diag Matrix, x-Dir
            WTxyz3(i,j,1) =-Fpb_pp *WTxy3(im1(i),j) ! k=1: 3-Diag Matrix, x-Dir
          END DO
          END DO
 
          DO  j = jp11,my1
          DO  i = ip11,mx1
            WTxyz1(i,j,2) = Fpb_pp *WTxy4(i,jp1(j)) ! k=2: 3-Diag Matrix, y-Dir
            WTxyz3(i,j,2) =-Fpb_pp *WTxy4(i,jm1(j)) ! k=2: 3-Diag Matrix, y-Dir
          END DO
          END DO
 
 
C +--Overrelaxation Starting Block
C +  -----------------------------
 
C +--Independant Term: constant contribution        !                     x-Dir
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          DO i=ip11,mx1
          DO j=jp11,my1
              WTxyz8(i,j,1) =                   pstDY(    i ,j)
     .              - Fpa_pp *WTxy3(ip1(i),j) * pstDY(ip1(i),j)
     .              + Fpa_pp *WTxy3(im1(i),j) * pstDY(im1(i),j)
          END DO
          END DO
 
          DO  j = jp11,my1
          DO  i = ip11,mx1
              WTxyz8(i,j,1) =                  WTxyz8(i,    j ,1)
     .              - Fpa_pp *WTxy4(i,jp1(j)) * pstDY(i,jp1(j))
     .              + Fpa_pp *WTxy4(i,jm1(j)) * pstDY(i,jm1(j))
          END DO
          END DO
 
C +--Dirichlet Condition x-LBC                      !                     x-Dir
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~
               i=     mx1
            DO j=jp11,my1
              WTxyz8(i,j,1) =                  WTxyz8(    i ,j,1)
     .              - Fpb_pp *WTxy3(ip1(i),j) * pstDY(ip1(i),j)
              WTxyz1(i,j,1) = 0.0
c #WR         write(6,*)            ip1(i),' ', pstDY(ip1(i),j)
            END DO
 
               i=ip11
            DO j=jp11,my1
              WTxyz8(i,j,1) =                  WTxyz8(    i ,j,1)
     .              + Fpb_pp *WTxy3(im1(i),j) * pstDY(im1(i),j)
              WTxyz3(i,j,1) = 0.0
c #WR         write(6,*)            im1(i),' ', pstDY(im1(i),j)
            END DO
 
C +--Dirichlet Condition y-LBC
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~
          IF (mmy.gt.1)                                             THEN
               j=     my1
            DO i=ip11,mx1
              WTxyz8(i,j,1) =                  WTxyz8(i,    j,1)
     .              - Fpb_pp *WTxy3(i,jp1(j)) * pstDY(i,jp1(j))
              WTxyz1(i,j,2) = 0.0
c #WR         write(6,*)            jp1(j),' ', pstDY(i,jp1(j))
            END DO
C +
               j=jp11
            DO i=ip11,mx1
              WTxyz8(i,j,1) =                  WTxyz8(i,    j,1)
     .              + Fpb_pp *WTxy3(i,jm1(j)) * pstDY(i,jm1(j))
              WTxyz3(i,j,2) = 0.0
c #WR         write(6,*)            jm1(j),' ', pstDY(i,jm1(j))
            END DO
          END IF
 
C +--First Estimate
C +  ~~~~~~~~~~~~~~
          DO  j = jp11,my1
          DO  i = ip11,mx1
            WTxyz7(i,j,1) =  pstDY(i,j)              ! Previous       Estimate
            WTxyz7(i,j,2) =  0.0                     ! Half-Iteration Estimate
            WTxyz7(i,j,3) =  pstDY(i,j)              ! Next to update Estimate
          END DO
          END DO
 
C +--Recurrence
C +  ~~~~~~~~~~
          facovr= 1.1
          nt_pst= 4
          it_pst= 0
          SRes_1= 0.0
 1000     CONTINUE
          it_pst= it_pst + 1
 
 
C +--Resolution along the x-Direction
C +  --------------------------------
 
C +--Tridiagonal Matrix Coefficients
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
              DO j=jp11,my1
              DO i=ip11,mx1
                WTxyz1(i,j,3) = WTxyz1(i,j,1)        ! Index 1 ==>      x-Dir
                WTxyz2(i,j,3) = 1.0d+0               !
                WTxyz3(i,j,3) = WTxyz3(i,j,1)        !
              END DO
              END DO
 
C +--Independant Term
C +  ~~~~~~~~~~~~~~~~
              DO j=jp11,my1
              DO i=ip11,mx1
                WTxyz4(i,j,3) = WTxyz8(i,j,1)        ! Index 1 ==>    ALL-Dir
     .                        - WTxyz1(i,j,2) *WTxyz7(i,jp1(j),1)
     .                        - WTxyz3(i,j,2) *WTxyz7(i,jm1(j),1)
              END DO
              END DO
 
C +--Tridiagonal Matrix Inversion                    ! OUTPUT is WTxyz7(i,j,3)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            i1_dps = ip11
            i2_dps = mx1
            j1_dps = jp11
            j2_dps = my1
            k1_dps = 3
            k2_dps = 3
 
C +              ********
            call MARgau_x(i1_dps,i2_dps,j1_dps,j2_dps,k1_dps,k2_dps)
C +              ********
 
 
C +--Resolution along the y-Direction
C +  --------------------------------
 
            IF (mmy.gt.1)                                           THEN
 
C +--Tridiagonal Matrix Coefficients                 ! Index 2 ==>      y-Dir
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
              DO j=jp11,my1
              DO i=ip11,mx1
                WTxyz1(i,j,3) = WTxyz1(i,j,2)
                WTxyz2(i,j,3) = 1.
                WTxyz3(i,j,3) = WTxyz3(i,j,2)
                WTxyz7(i,j,2) = WTxyz7(i,j,3)        ! Half-Iteration Estimate
              END DO
              END DO
 
C +--Independant Term                                !                  y-Dir
C +  ~~~~~~~~~~~~~~~~
              DO i=ip11,mx1
              DO j=jp11,my1
                WTxyz4(i,j,3) = WTxyz8(i,j,1)        ! Index 1 ==>    ALL-Dir
     .                        - WTxyz1(i,j,1) *WTxyz7(ip1(i),j,2)
     .                        - WTxyz3(i,j,1) *WTxyz7(im1(i),j,2)
              END DO
              END DO
 
C +--Tridiagonal Matrix Inversion                    ! OUTPUT is WTxyz7(i,j,3)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
              i1_dps = ip11
              i2_dps = mx1
              j1_dps = jp11
              j2_dps = my1
              k1_dps = 3
              k2_dps = 3
 
C +                ********
              call MARgau_y(i1_dps,i2_dps,j1_dps,j2_dps,k1_dps,k2_dps)
C +                ********
 
            END IF
 
C +--Residual is obtained by substracting next from former estimated Equation
C +  ------------------------------------------------------------------------
 
            DO  i = ip11,mx1
            DO  j = jp11,my1
              WTxyz6(i,j,1) = WTxyz1(i,j,1) * WTxyz7(ip1(i),j,3)
     .                      + WTxyz3(i,j,1) * WTxyz7(im1(i),j,3)
            END DO
            END DO
 
            DO  j = jp11,my1
            DO  i = ip11,mx1
              WTxyz6(i,j,1) = WTxyz6(i,j,1)
     .                      + WTxyz1(i,j,2) * WTxyz7(i,jp1(j),3)
     .                      + WTxyz3(i,j,2) * WTxyz7(i,jm1(j),3)
     .                      +                 WTxyz7(i,j     ,3)
     .                      - WTxyz8(i,j,1)
            END DO
            END DO
 
              SRes_1        =  0.0
            DO  j = jp11,my1
            DO  i = ip11,mx1
              SRes_1        =  SRes_1   + abs(WTxyz6(i,j,1))
            END DO
            END DO
 
            IF (it_pst.gt.1)                                      THEN
              SRes10        =  SRes_1 / SRes_0
            ELSE
              SRes_0        =  SRes_1
              SRes10        =  1.0
            END IF
 
 
C +--New Estimate
C +  ------------
 
            IF (SRes10.gt.0.1   .and.it_pst.lt.nt_pst
     .                          .and.mmy   .gt.     1)              THEN
              DO  j = jp11,my1
              DO  i = ip11,mx1
                WTxyz7(i,j,1) = WTxyz7(i,j,3)
     .                - facovr* WTxyz6(i,j,1)/WTxyz2(i,j,1)
                WTxyz7(i,j,3) = WTxyz7(i,j,1)
              END DO
              END DO
            END IF
C +
c #WR         write(6,1001) iterun,it_pst,SRes10
 1001         format(2i9,f21.15)
C +
          IF    (SRes10.gt.1.0d-1.and.it_pst.lt.nt_pst
     .                           .and.mmy   .gt.     1)       GO TO 1000
 
 
C +--Final Estimate
C +  --------------
 
          DO j=jp11,my1
          DO i=ip11,mx1
                pstDY( i,j)   = WTxyz7(i,j,3)
                pstDYn(i,j)   = WTxyz7(i,j,3)
          END DO
          END DO
 
C +--Lateral Boundary Conditions
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~
            DO j=jp11,my1
                pstDY(  1,j) = WTxy1( 1,j)
                pstDY( mx,j) = WTxy1(mx,j)
                pstDYn( 1,j) = WTxy1( 1,j)
                pstDYn(mx,j) = WTxy1(mx,j)
            END DO
 
          IF (mmy.gt.1)                                             THEN
            DO i=ip11,mx1
                pstDY( i, 1) = WTxy1(i, 1)
                pstDY( i,my) = WTxy1(i,my)
                pstDYn(i, 1) = WTxy1(i, 1)
                pstDYn(i,my) = WTxy1(i,my)
            END DO
          END IF
 
 
C +--Contribution to Vertical Material Speed
C +  ---------------------------------------
 
               DO k=1,mz
 
                 DO j=1,my
                 DO i=1,mx
                   WTxyz3(i,j,k)=uairDY(i,j,k) * pstDYn(i,j)
                   WTxyz4(i,j,k)=vairDY(i,j,k) * pstDYn(i,j)
                   WTxyz8(i,j,k)=                0.
                 END DO
                 END DO
 
                 DO  j=1,my
                 DO  i=1,mx
                   WTxyz7(i,j,k)=(WTxyz3(im1(i),j,k)-WTxyz3(ip1(i),j,k)
     .                           )
                 ENDDO
                 ENDDO
 
                 DO  i=1,mx
                 DO  j=1,my
                   WTxyz7(i,j,k)=(WTxyz7(i,    j ,k)
     .                           +WTxyz4(i,jm1(j),k)-WTxyz4(i,jp1(j),k)
     .                           )                  *dxinv2
                 END DO
                 END DO
 
 ! Two previous Loops are the vectorized version of the following Loop
 !               DO  j=1,my
 !               DO  i=1,mx
 !                 WTxyz7(i,j,k)=               -dxinv2
 !   .                      *(WTxyz3(ip1(i),j,k)-WTxyz3(im1(i),j,k)
 !   .                       +WTxyz4(i,jp1(j),k)-WTxyz4(i,jm1(j),k))
 !               END DO
 !               END DO
 
               END DO
 
        END IF
 
 
C ++++++++++++++++++++++++++++++++++
C +--IMPLICIT SCHEME         (END) +
C ++++++++++++++++++++++++++++++++++
 
 
C ++++++++++++++++++++++++++++++++++
C +--EXPLICIT SCHEME       (BEGIN) +
C ++++++++++++++++++++++++++++++++++
 
 
      ELSE
 
 
C ++++++++++++++++++++++++++++++++++
C +--ERROR TEST            (BEGIN) +
C ++++++++++++++++++++++++++++++++++
 
 
        IF  (norder.lt.0)                                         THEN
 
          stop
     .   '++++++++ Horizontal Advection badly conditioned / Order<0 ++'
 
        ELSE
 
 
C ++++++++++++++++++++++++++++++++++
C +...Is  Centered Scheme  (BEGIN) +
C ++++++++++++++++++++++++++++++++++
 
 
          IF (center)                                             THEN
 
              locorr = .false.
              dtcorr =  1.000
 
 
C +--Mass Divergence / First  Direction
C +  ==================================
 
 
C +--Mass Flux Update / x-Direction
C +  ------------------------------
 
c #SP       IF (mod(numdps,2).eq.0.or.mmy.eq.1)                     THEN
 
               DO j=1,my
               DO i=1,mx
                 WTxy3(i,j)=0.0
               END DO
               END DO
 
!$OMP PARALLEL DO private(i,j,k)
               DO k=1,mz
 
              IF (norder.EQ.2)                                      THEN
 
c               DO k=1,mz
               DO j=1,my
               DO i=1,mx
                   WTxyz3(i,j,k)=uairDY(i,j,k) * pstDYn(i,j)
c #2Z.                          *clatGE(i,j)
               END DO
               END DO
cc #vL          END DO
 
cc #vL          DO k=1,mz
               DO i=1,mx
               DO j=1,my
                   WTxyz7(i,j,k)=                dxinv2          *  (
     .                        WTxyz3(im1(i),j,k)-WTxyz3(ip1(i),j,k) )
c #2Z.                          /clatGE(i,j)
               END DO
               END DO
c               END DO
 
              ELSE
 
c               DO k=1,mz
               DO j=1,my
               DO i=1,mx
                   WTxyz3(i,j,k)=uairDY(i,j,k) * pstDYn(i,j)
c #2Z.                          *clatGE(i,j)
               END DO
               END DO
cc #vL          END DO
 
cc #vL          DO k=1,mz
               DO i=1,mx
               DO j=1,my
                   WTxyz7(i,j,k)=                dxinv2 * fac43  *  (
     .                 0.125*(WTxyz3(ip2(i),j,k)-WTxyz3(im2(i),j,k))
     .                       -WTxyz3(ip1(i),j,k)+WTxyz3(im1(i),j,k) )
c #2Z.                          /clatGE(i,j)
               END DO
               END DO
c               END DO
 
              END IF
 
 
C +--Mass Flux Update / y-Direction
C +  ------------------------------
C +
c #SP       ELSE
 
              IF (norder.EQ.2)                                      THEN
 
c               DO k=1,mz
               DO j=1,my
               DO i=1,mx
                   WTxyz4(i,j,k)=vairDY(i,j,k) * pstDYn(i,j)
               END DO
               END DO
cc #vL          END DO
 
cc #vL          DO k=1,mz
               DO j=1,my
               DO i=1,mx
                   WTxyz8(i,j,k)=                dyinv2          *  (
     .                        WTxyz4(i,jm1(j),k)-WTxyz4(i,jp1(j),k) )
               END DO
               END DO
 
c #SP          DO j=1,my
c #SP          DO i=1,mx
c #SP              WTxyz7(i,j,k)=                WTxyz8(i,j,k)
c #SP              WTxyz8(i,j,k)=                0.0
c #SP          END DO
c #SP          END DO
c               END DO
 
              ELSE
 
c               DO k=1,mz
               DO j=1,my
               DO i=1,mx
                   WTxyz4(i,j,k)=vairDY(i,j,k) * pstDYn(i,j)
               END DO
               END DO
cc #vL          END DO
 
cc #vL          DO k=1,mz
               DO j=1,my
               DO i=1,mx
                   WTxyz8(i,j,k)=                dyinv2 * fac43  *  (
     .                 0.125*(WTxyz4(i,jp2(j),k)-WTxyz4(i,jm2(j),k))
     .                       -WTxyz4(i,jp1(j),k)+WTxyz4(i,jm1(j),k) )
               END DO
               END DO
 
c #SP          DO j=1,my
c #SP          DO i=1,mx
c #SP              WTxyz7(i,j,k)=                WTxyz8(i,j,k)
c #SP              WTxyz8(i,j,k)=                0.0
c #SP          END DO
c #SP          END DO
c              END DO
 
              ENDIF
 
c #SP       END IF
            ENDDO
!$OMP END PARALLEL DO
 
C +--Pressure Depth Increment
C +  ------------------------
 
               DO k=1,mz
               DO j=1,my
               DO i=1,mx
                 WTxy3(i,j)=WTxy3(i,j)
     .                   -(WTxyz7(i,j,k)+WTxyz8(i,j,k))*dsigm1(k)
               END DO
               END DO
               END DO
 
C +--Pressure Depth    Update (Leapfrog-Backward)
C +  --------------------------------------------
 
            IF  (itFast.eq.1)                                       THEN
                DO j=1,my
                DO i=1,mx
                  pstDY( i,j)   = WTxy2(i,j) -  WTxy3(i,j)      *dtfast
                  pstDYn(i,j)   = WTxy2(i,j) -  WTxy3(i,j) *2.0 *dtfast
                END DO
                END DO
            ELSE
              IF (itFast.le.ntFast)                                 THEN
                DO j=1,my
                DO i=1,mx
                  pstDY( i,j)   = WTxy2(i,j)
                  pstDYn(i,j)   = WTxy1(i,j) -  WTxy3(i,j) *2.0*dtfast
 
C +--Robert Time Filter
C +  ~~~~~~~~~~~~~~~~~~
c #rt             pstDY( i,j)   = pstDY( i,j)+
c #rt.               Robert*(0.5*(pstDYn(i,j)+WTxy1(i,j))-pstDY( i,j))
                END DO
                END DO
              ELSE
                DO j=1,my
                DO i=1,mx
                  pstDY( i,j)   = WTxy2(i,j)
                  pstDYn(i,j)   = WTxy1(i,j) -  WTxy3(i,j)      *dtfast
                END DO
                END DO
C +***          Leapfrog-Backward (e.g. Haltiner and Williams, p.152)
 
              END IF
            END IF
 
 
C +--Mass Divergence / Second Direction
C +  ==================================
 
c #SP       IF  (mmy.gt.1)                                          THEN
 
 
C +--Mass Flux Update / x-Direction
C +  ------------------------------
 
c #SP         IF (mod(numdps,2).eq.1)                               THEN
 
c #SP          IF    (norder   .EQ.2)                               THEN
 
c #SP           DO  k=1,mz
 
c #SP             DO j=1,my
c #SP             DO i=1,mx
c #SP               WTxyz3(i,j,k)=uairDY(i,j,k) * pstDYn(i,j)
c #SP             END DO
c #SP             END DO
 
c #SP             DO  j=1,my
c #SP             DO  i=1,mx
c #SP               WTxyz8(i,j,k)=                dxinv2          *  (
c #SP.                         WTxyz3(im1(i),j,k)-WTxyz3(ip1(i),j,k) )
c #SP             END DO
c #SP             END DO
 
c #SP           END DO
 
c #SP          ELSE
 
c #SP           DO  k=1,mz
 
c #SP             DO j=1,my
c #SP             DO i=1,mx
c #SP               WTxyz3(i,j,k)=uairDY(i,j,k) * pstDYn(i,j)
c #SP             END DO
c #SP             END DO
 
c #SP             DO  j=1,my
c #SP             DO  i=1,mx
c #SP               WTxyz8(i,j,k)=                dxinv2 * fac43  *  (
c #SP.                  0.125*(WTxyz3(ip2(i),j,k)-WTxyz3(im2(i),j,k))
c #SP.                        -WTxyz3(ip1(i),j,k)+WTxyz3(im1(i),j,k) )
c #SP             END DO
c #SP             END DO
 
c #SP           END DO
 
c #SP          END IF
 
 
C +--Mass Flux Update / y-Direction
C +  ------------------------------
 
c #SP         ELSE
 
c #SP          IF    (norder   .EQ.2)                               THEN
 
c #SP           DO  k=1,mz
 
c #SP             DO j=1,my
c #SP             DO i=1,mx
c #SP               WTxyz4(i,j,k)=vairDY(i,j,k) * pstDYn(i,j)
c #SP             END DO
c #SP             END DO
 
c #SP             DO j=1,my
c #SP             DO i=1,mx
c #SP               WTxyz8(i,j,k)=                dyinv2          *  (
c #SP.                         WTxyz4(i,jm1(j),k)-WTxyz4(i,jp1(j),k) )
c #SP             END DO
c #SP             END DO
 
c #SP           END DO
 
c #SP          ELSE
 
c #SP           DO  k=1,mz
 
c #SP             DO j=1,my
c #SP             DO i=1,mx
c #SP               WTxyz4(i,j,k)=vairDY(i,j,k) * pstDYn(i,j)
c #SP             END DO
c #SP             END DO
 
c #SP             DO j=1,my
c #SP             DO i=1,mx
c #SP               WTxyz8(i,j,k)=                dyinv2 * fac43  *  (
c #SP.                  0.125*(WTxyz4(i,jp2(j),k)-WTxyz4(i,jm2(j),k))
c #SP.                        -WTxyz4(i,jp1(j),k)+WTxyz4(i,jm1(j),k) )
c #SP             END DO
c #SP             END DO
 
c #SP           END DO
 
c #SP          END IF
 
c #SP         END IF
 
 
C +--Pressure Depth Increment
C +  ------------------------
 
                DO j=1,my
                DO i=1,mx
                  WTxy4(i,j)=0.0
                END DO
                END DO
 
c #SP           DO k=1,mz
c #SP           DO j=1,my
c #SP           DO i=1,mx
c #SP             WTxy4(i,j)=WTxy4(i,j)-WTxyz8(i,j,k)*dsigm1(k)
c #SP           END DO
c #SP           END DO
c #SP           END DO
 
c #SP           DO j=1,my
c #SP           DO i=1,mx
c #SP             WTxy3(i,j)=WTxy3(i,j)+ WTxy4(i,j)
c #SP           END DO
c #SP           END DO
 
 
C +--Pressure Depth    Update (Leapfrog-Backward)
C +  --------------------------------------------
 
c #SP         IF  (itFast.eq.1)                                     THEN
c #SP             DO j=1,my
c #SP             DO i=1,mx
c #SP               pstDY( i,j) = pstDY( i,j) - WTxy4(i,j)     *dtfast
c #SP               pstDYn(i,j) = pstDYn(i,j) - WTxy4(i,j) *2.0*dtfast
c #SP             END DO
c #SP             END DO
c #SP         ELSE
c #SP           IF (itFast.le.ntFast)                               THEN
c #SP             DO j=1,my
c #SP             DO i=1,mx
c #SP               pstDY( i,j) = WTxy2( i,j)
c #SP               pst_n1      = pstDYn(i,j)
c #SP               pstDYn(i,j) = pstDYn(i,j) - WTxy4(i,j) *2.0*dtfast
 
C +--Robert Time Filter
C +  ~~~~~~~~~~~~~~~~~~
c #rt             pstDY( i,j)   = pstDY( i,j)+
c #rt.               Robert*(0.5*(pstDYn(i,j)+pst_n1    )-pstDY( i,j))
c #SP             END DO
c #SP             END DO
c #SP           ELSE
c #SP             DO j=1,my
c #SP             DO i=1,mx
c #SP               pstDY( i,j) = WTxy2( i,j)
c #SP               pstDYn(i,j) = pstDYn(i,j) - WTxy4(i,j)      *dtfast
c #SP             END DO
c #SP             END DO
C +***            Leapfrog-Backward (e.g. Haltiner and Williams, p.152)
 
c #SP           END IF
c #SP         END IF
 
c #SP       END IF
 
            norder = -1
 
C ++++++++++++++++++++++++++++++++++
C +...Is  Centered Scheme    (END) +
C +...Non Centered Schemes (BEGIN) +
C ++++++++++++++++++++++++++++++++++
 
 
          ELSE
 
            locorr = .true.
            dtcorr = dtfast
 
 
C +--Vector for Positive Definite Variables
C +  ======================================
 
            dtxfas = dtx /(ntFast+1)
            dtyfas = dty /(ntFast+1)
 
            DO     k=1,mz
 
            DO     j=1,my
            DO     i=1,mx
              WTxyz1(i,j,k) =       - uairDY(i,j,k)             *dtxfas
              WTxyz2(i,j,k) =       - vairDY(i,j,k)             *dtyfas
              uuface        = 0.5  * (uairDY(i,j,k)+uairDY(ip1(i),j,k))
              vvface        = 0.5  * (vairDY(i,j,k)+vairDY(i,jp1(j),k))
              WTxyz3(i,j,k) = 0.5  * (abs(uuface ) +  uuface  ) *dtxfas
              WTxyz5(i,j,k) = 0.5  * (abs(uuface ) -  uuface  ) *dtxfas
              WTxyz4(i,j,k) = 0.5  * (abs(vvface ) +  vvface  ) *dtyfas
              WTxyz6(i,j,k) = 0.5  * (abs(vvface ) -  vvface  ) *dtyfas
            END DO
            END DO
 
            END DO
 
 
C +--Advection (Time Splitting)
C +  ==========================
 
C +--Mass and Mass Flux
C +  ~~~~~~~~~~~~~~~~~~
            do     j=1,my
            do     i=1,mx
              WTxy5(i,j)=          pstDY(    i ,j)
              WTxy1(i,j)=          WTxy5(    i ,j)
              WTxy2(i,j)=          WTxy5(    i ,j)
              WTxy3(i,j)=          0.
            end do
            end do
 
 
C +--Conservative Scheme Order 0
C +  ===========================
C +
            IF (norder.eq.0)                                      THEN
                norder = -1
 
 
C +--Time Splitting
C +  --------------
 
C +--x-Direction First
C +  ~~~~~~~~~~~~~~~~~
              IF (mod(numdps,2).eq.0)                             THEN
 
                do     k=1,mz
                do     j=1,my
                do     i=1,mx
                 WTxyz7(    i ,    j ,k)  =
 
     .         - WTxyz3(    i ,    j ,k)  *  WTxy5(    i ,    j )
     .         + WTxyz5(    i ,    j ,k)  *  WTxy5(ip1(i),    j )
     .         + WTxyz3(im1(i),    j ,k)  *  WTxy5(im1(i),    j )
     .         - WTxyz5(im1(i),    j ,k)  *  WTxy5(    i ,    j )
 
                  WTxy3(    i ,    j   )  =  WTxy3(    i ,    j )
     .         + WTxyz7(    i ,    j ,k)  *  dsigm1(k)
                end do
                end do
                end do
 
                do     k=1,mz
                do     j=1,my
                do     i=1,mx
                 WTxyz8(    i ,    j ,k)  =
 
     .         - WTxyz4(    i ,    j ,k)  *  WTxy3(    i,     j )
     .         + WTxyz6(    i ,    j ,k)  *  WTxy3(    i ,jp1(j))
     .         + WTxyz4(    i ,jm1(j),k)  *  WTxy3(    i ,jm1(j))
     .         - WTxyz6(    i ,jm1(j),k)  *  WTxy3(    i ,    j )
 
                  WTxy3(i,j)  =              WTxy3(    i ,    j )
     .         + WTxyz8(    i ,    j ,k)  *  dsigm1(k)
                end do
                end do
                end do
 
C +--y-Direction First
C +  ~~~~~~~~~~~~~~~~~
              ELSE
 
                do     k=1,mz
                do     j=1,my
                do     i=1,mx
                 WTxyz7(    i ,    j ,k)  =
 
     .         - WTxyz4(    i ,    j ,k)  *  WTxy5(    i,     j )
     .         + WTxyz6(    i ,    j ,k)  *  WTxy5(    i ,jp1(j))
     .         + WTxyz4(    i ,jm1(j),k)  *  WTxy5(    i ,jm1(j))
     .         - WTxyz6(    i ,jm1(j),k)  *  WTxy5(    i ,    j )
 
                  WTxy3(i,j)  =              WTxy3(    i ,    j )
     .         + WTxyz7(    i ,    j ,k)  *  dsigm1(k)
                end do
                end do
                end do
 
                do     k=1,mz
                do     j=1,my
                do     i=1,mx
                 WTxyz8(    i ,    j ,k)  =
 
     .         - WTxyz3(    i ,    j ,k)  *  WTxy3(    i ,    j )
     .         + WTxyz5(    i ,    j ,k)  *  WTxy3(ip1(i),    j )
     .         + WTxyz3(im1(i),    j ,k)  *  WTxy3(im1(i),    j )
     .         - WTxyz5(im1(i),    j ,k)  *  WTxy3(    i ,    j )
 
                  WTxy3(i,j)  =              WTxy3(    i ,    j )
     .         + WTxyz8(    i ,    j ,k)  *  dsigm1(k)
                end do
                end do
                end do
 
              END IF
 
            END IF
 
 
C +--Conservative Scheme order 4
C +  ===========================
 
            IF (norder.eq.4)                                      THEN
                norder = -1
 
 
C +--Time Splitting
C +  --------------
 
C +--Parameters
C +  ~~~~~~~~~~
              IF (mod(numdps,2).eq.0)                             THEN
                   idir_x = 2
                   jdir_y = 3
              ELSE
                   idir_x = 3
                   jdir_y = 2
              END IF
 
C +--Auxiliary  Variables
C +  ~~~~~~~~~~~~~~~~~~~~
              do j=1,my
              do i=1,mx
                WTxy3(i,j) =  WTxy5(i,j)
                WTxy4(i,j) =  WTxy5(i,j)
              end do
              end do
 
 
C +--1D Computation
C +  --------------
 
 401          CONTINUE
 
              IF (idir_x.eq.0 .or.
     .            jdir_y.eq.3)                               GO TO 402
 
C +--x-Direction  here
C +  ~~~~~~~~~~~~~~~~~
                DO  k=1,mz
                DO  j=1,my
                  DO i=1,mx
                    cnpx(i)   = WTxyz3( i,j,k)
                    cnmx(i)   = WTxyz5( i,j,k)
                    vecx(i)   =  WTxy2( i,j)
                  END DO
                    cnpx(  0) = WTxyz3( 1,j,k)
                    cnpx(mxx) = WTxyz3(mx,j,k)
                    cnmx(  0) = WTxyz5( 1,j,k)
                    cnmx(mxx) = WTxyz5(mx,j,k)
                    vecx(  0) =  WTxy2( 1,j)
                    vecx(mxx) =  WTxy2(mx,j)
 
C +                      ********
                    call ADVbot_4(flux,vecx,aa0x,aa1x,aa2x,aa3x,aa4x,
     .                                 cnpx,cnmx,sipx,simx,sidx,mxx,1)
C +                      ********
 
                  DO i=1,mx
                    WTxyz7(i,j,k)=            - flux(i) + flux(im1(i))
                  END DO
 
                  DO i=1,mx
                     WTxy3(i,j)  = WTxy3(i,j) + WTxyz7(i,j,k)*dsigm1(k)
                  END DO
                END DO
                END DO
 
C +--Assignation in case of Time Splitting
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                IF (idir_x.gt.1.or.
     .              jdir_y.gt.1)                                  THEN
                  do j=1,my
                  do i=1,mx
                    WTxy1(i,j) = WTxy3(i, j)
                    WTxy4(i,j) = WTxy3(i, j)
                  end do
                  end do
                END IF
 
                    idir_x =  0
 
 402          CONTINUE
 
              IF (idir_x.eq.0 .and.
     .            jdir_y.eq.0)                               GO TO 403
 
C +--y-Direction  here
C +  ~~~~~~~~~~~~~~~~~
                DO  k=1,mz
                DO  i=1,mx
                  DO j=1,my
                    cnpy(j)   = WTxyz4(i, j,k)
                    cnmy(j)   = WTxyz6(i, j,k)
                    vecy(j)   =  WTxy1(i, j)
                  END DO
                    cnpy(  0) = WTxyz4(i, 1,k)
                    cnpy(myy) = WTxyz4(i,my,k)
                    cnmy(  0) = WTxyz6(i, 1,k)
                    cnmy(myy) = WTxyz6(i,my,k)
                    vecy(  0) =  WTxy1(i, 1)
                    vecy(myy) =  WTxy1(i,my)
 
C +                      ********
                    call ADVbot_4(fluy,vecy,aa0y,aa1y,aa2y,aa3y,aa4y,
     .                                 cnpy,cnmy,sipy,simy,sidy,myy,1)
C +                      ********
 
                  DO j=1,my
                    WTxyz8(i,j,k)=         - fluy(j) + fluy(jm1(j))
                  END DO
 
                  DO j=1,my
                    WTxy4(i,j)= WTxy4(i,j) + WTxyz8(i,j,k)*dsigm1(k)
                  END DO
                END DO
                END DO
 
C +--Assignation in case of Time Splitting
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                IF (idir_x.gt.1.or.
     .              jdir_y.gt.1)                                  THEN
                  do j=1,my
                  do i=1,mx
                    WTxy2(i,j) = WTxy4(i, j)
                    WTxy3(i,j) = WTxy4(i, j)
                  end do
                  end do
                END IF
 
                    jdir_y =  0
 
                                                             GO TO 401
 
 403          CONTINUE
 
            END IF
 
 
C +--Update
C +  ======
 
              DO j=1,my
              DO i=1,mx
                pstDYn(i,j) =             WTxy3(i,j)
C +...          New Pressure Thickness
 
                 WTxy3(i,j) = pstDY(i,j) -WTxy3(i,j)
C +...          New Pressure Thickness Increment
 
              END DO
              END DO
 
 
C ++++++++++++++++++++++++++++++++++
C +...Non Centered Schemes   (END) +
C ++++++++++++++++++++++++++++++++++
 
 
          END IF
 
 
C ++++++++++++++++++++++++++++++++++
C +--ERROR TEST              (END) +
C ++++++++++++++++++++++++++++++++++
 
 
        END IF
        IF (norder.ge.0)
     .    stop
     .   '++++++++ Horizontal Advection badly conditioned ++++++++++++'
 
      END IF
 
 
C ++++++++++++++++++++++++++++++++++
C +--EXPLICIT SCHEME         (END) +
C ++++++++++++++++++++++++++++++++++
 
 
C +--Vertical Wind Speed (sigma coordinate)
C +  ======================================
 
C +--Staggered Vertical Grid
C +  -----------------------
 
 
        IF (  itFast.eq.1) ntpsig = 0
 
!$OMP PARALLEL DO private (i,j,k)
          DO j=1,my
 
        IF (  itFast.eq.       1                      )           THEN
            DO k=1,mz
c            DO j=1,my
            DO i=1,mx
              psigad(i,j,k) = 0.
            END DO
c            END DO
            END DO
        ENDIF
 
        IF (staggr)                                                 THEN
 
c            DO j=1,my
            DO i=1,mx
              WTxyz3(i,j,1)=sigmid(2)*  WTxy3(i,j)
     .                     +dsigm1(1)*(WTxyz7(i,j,1)+WTxyz8(i,j,1))
            END DO
c            END DO
 
            DO k=kp1(1),mmz
c            DO j=1,my
            DO i=1,mx
              WTxyz3(i,j,k)= dsigm1(k)*(WTxy3(i,j)
     .                 +(WTxyz7(i,j,k)
     .                  +WTxyz8(i,j,k)            ))+WTxyz3(i,j,k-1)
C +...        Computation of p*Sigma. BETWEEN Sigma Levels
 
            END DO
c            END DO
            END DO
 
 
C +--Unstaggered Vertical Grid
C +  -------------------------
 
        ELSE
 
c            DO j=1,my
            DO i=1,mx
              WTxyz3(i,j,1)= sigma(1)*  WTxy3(i,j)
     .                     + sigma(1)*(WTxyz7(i,j,1)+WTxyz8(i,j,1))
C +...        Open Upper Boundary Condition: WTxyz7(i,j,0)=WTxyz7(i,j,1)
C +                                          WTxyz8(i,j,0)=WTxyz8(i,j,1)
            END DO
c            END DO
 
            DO k=kp1(1),mz
c            DO j=1,my
            DO i=1,mx
              WTxyz3(i,j,k)= dsig_1(k-1) * (WTxy3(i,j)
     .            +0.50*(WTxyz7(i,j,k)+WTxyz7(i,j,k-1)
     .                  +WTxyz8(i,j,k)+WTxyz8(i,j,k-1)))
     .                  +WTxyz3(i,j,k-1)
C +...        Computation of p*Sigma. ON      Sigma Levels
 
            END DO
c            END DO
            END DO
 
        END IF
       ENDDO
!$OMP END PARALLEL DO
 
              ntpsig       = 1    +    ntpsig
!$OMP PARALLEL DO private (i,j,k)
        DO k=1,mz
            DO j=1,my
            DO i=1,mx
              psigad(i,j,k)=           psigad(i,j,k)
     .                     +           WTxyz3(i,j,k)
            END DO
            END DO
c            END DO
 
        IF (((itFast.eq.ntFast+1).and..not.pImplc).OR.
     .      ((itFast.eq.       1).and.     pImplc)    )           THEN
 
c            DO k=1,mz
            DO j=1,my
            DO i=1,mx
              psigDY(i,j,k)=           psigad(i,j,k)
     .                     /           ntpsig
            END DO
            END DO
c            END DO
 
          IF (locorr)                                             THEN
c            DO k=1,mz
            DO j=1,my
            DO i=1,mx
              psigDY(i,j,k)=psigDY(i,j,k) / dtcorr
            END DO
            END DO
c            END DO
          END IF
        END IF
       ENDDO
!$OMP END PARALLEL DO
 
C +--Simple non-hydrostatic Correction
C +  =================================
 
C +--Filtering
C +  ~~~~~~~~~
c #ON   do     j=1,my
c #ON   do     i=1,mx
c #ON     WTxy4(i,j) =
c #ON.          zi__TE(im1(i),jp1(j))+2.0*zi__TE(    i ,jp1(j))
c #ON.                               +    zi__TE(ip1(i),jp1(j))
c #ON.    + 2.0*zi__TE(im1(i),j)     +4.0*zi__TE(    i ,    j )
c #ON.                               +2.0*zi__TE(ip1(i),    j )
c #ON.    +     zi__TE(im1(i),jm1(j))+2.0*zi__TE(    i ,jm1(j))
c #ON.                             +      zi__TE(ip1(i),jm1(j))
c #ON   end do
c #ON   end do
c #ON   do     j=1,my
c #ON   do     i=1,mx
c #ON     WTxy4(i,j) = 0.0625* WTxy4(i,j)
c #ON   end do
c #ON   end do
 
 
C +--Correction
C +  ~~~~~~~~~~
 
c #ON   do     j=1,my
c #ON   do     i=1,mx
c #ON       CorArg = 1.0 + WTxy4(i,j) * WTxy4(i,j) / (4.0 * dx * dx)
c #ON       CorrNH = 1.0 / sqrt(CorArg)
C +...      Weisman et al., 1997, MWR125, p.541
C +
c #ON     do     k=1,mz
c #ON       psigDY(i,j,k)= psigDY(i,j,k)*CorrNH
c #ON     end do
c #ON   end do
c #ON   end do
 
 
C +--Vertical Velocity (sigma coordinates)
C +  =====================================
 
        IF (staggr)                                                 THEN
          DO       k=   1,mz
            DO     j=jp11,my1
            DO     i=ip11,mx1
              wsigDY(i,j,k)=   psigDY(i,j,k)
     .                       / pstDYn(i,j)
            END DO
            END DO
          END DO
 
        ELSE
          DO       k=   1,mz1
            DO     j=jp11,my1
            DO     i=ip11,mx1
              wsigDY(i,j,k)=  (psigDY(i,j,k)+psigDY(i,j,kp1(k)))*0.5
     .                       / pstDYn(i,j)
            END DO
            END DO
          END DO
                   k=     mz
            DO     j=jp11,my1
            DO     i=ip11,mx1
              wsigDY(i,j,k)=   psigDY(i,j,k)                    *0.5
     .                       / pstDYn(i,j)
            END DO
            END DO
        END IF
 
 
C +--Maximum Vertical Courant Number
C +  ===============================
 
              CFLzDY     =     0.0
      DO   k=1,mz
        DO j=1,my
        DO i=1,mx
              CFLzDY     = max(CFLzDY
     .                    ,abs(wsigDY(i,j,k))*2.0*dt/dsigm1(k))
        END DO
        END DO
      END DO
 
 
C +--Slip condition for Mountain Wave Experiments
C +  ============================================
 
c #OM   DO       j=1,my
c #OM   DO       i=1,mx
c #OM     psigDY(i,j,mz)=0.0
c #OM   END DO
c #OM   END DO
 
      return
      end
