 
 
      subroutine INIsnd
 
C +------------------------------------------------------------------------+
C | MAR INPUT      ATMOS                                   19-02-2004  MAR |
C |   SubRoutine INIsnd includes Large Scale Conditions from a Sounding    |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |    INPUT (via common block)                                            |
C |    ^^^^^^ itexpe: Experiment Iteration Index                           |
C |           iterun: Run        Iteration Index (n Run for 1 Experiment)  |
C |           log_1D=(0,1) (before,after)  Boundary Layer Initialization   |
C |           potvor:"Potential Vorticity conserved" Initialization Switch |
C |           potvor=.T. => Potential Vorticity (PV) Conservation          |
C |                         Constraint is used (in 2-D Mode only)          |
C |           conmas:"Mass                conserved" Initialization Switch |
C |           conmas=.T. => Mass Conservation is used                      |
C |                                                                        |
C |    INPUT: Sounding(s) / File MARSND.dat                                |
C |    ^^^^^^                                                              |
C |                                                                        |
C |   OUTPUT (via common block)                                            |
C |   ^^^^^^  MESOSCALE VARIABLES                                          |
C |           ^^^^^^^^^^^^^^^^^^^                                          |
C |           nSND             : Sounding Number                           |
C |           pstDY (mx,my)    : Initial Model Thickness             [kPa] |
C |           pstDY1(mx,my)    : Initial Model Thickness -FIXED-     [kPa] |
C |           ugeoDY(mx,my,mz) : Initial Geo. Wind (x-Direction)     [m/s] |
C |           vgeoDY(mx,my,mz) : Initial Geo. Wind (y-Direction)     [m/s] |
C |           uairDY(mx,my,mz) : Initial      Wind (x-Direction)     [m/s] |
C |           vairDY(mx,my,mz) : Initial      Wind (y-Direction)     [m/s] |
C |           tairDY(mx,my,mz) : Initial                   Temperature [K] |
C |           TairSL(mx,my)    : Initial      Surface Air  Temperature [K] |
C |                             (Sounding Extrapolated to the Surface;     |
C |                              - dtagSL                            )     |
C |           pktaDY(mx,my,mzz): Initial Reduced Potential Temperature     |
C |             qvDY(mx,my,mz) : Initial Specific Humididity       [kg/kg] |
C |           qvapSL(mx,my)    : Initial Specific Humididity       [kg/kg] |
C |                             (Sounding Extrapolated to the Surface)     |
C |                                                                        |
C |   OUTPUT (via common block)                                            |
C |   ^^^^^^  LARGE SCALE VARIABLES (BOUNDARIES and DOMAIN AVERAGE)        |
C |           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^        |
C |        1. LBC                                                          |
C |           vaxgLB  (mx,my,mz,n):Large Scale Values of the Variables     |
C |                             (n=1,2,3,4,5) <=> (u,v,qv,pktaDY,pstDY),   |
C |                                                               for x << |
C |           vaxdLB : idem,                                          x >> |
C |           vayiLB : idem,                                          y << |
C |           vaysLB : idem,                                          y >> |
C |           zetaD  : Potential Vorticity  (CAUTION: Time Independant)    |
C |        2. UBC                                                          |
C |           uairUB, vairUB, pktaUB                                       |
C |                                                                        |
C |   METHOD: Vertical  Interpolation on Sigma Levels  is first performed  |
C |   ^^^^^^^ For two Soundings nearest in Time (before and after runtime) |
C |           Then Time Interpolation                                      |
C |           This order was preferred to the reverse because it allows    |
C |           to use Soundings having a different vertical Discretization  |
C |                                                                        |
C | # OPTIONS: _SC  Supersaturation not allowed (DO NOT USE for SCu cloud) |
C | # ^^^^^^^^ #WR  Additional Output                                      |
C |                                                                        |
C |   CAUTION: INIsnd may be used only for Large Scale Thermodynamics      |
C |   ^^^^^^^^ independant of x and y (i.e.for not too large model domain) |
C |            zeSND fixed at its initial value whenever Sounding varies   |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
 
      include 'MARSND.inc'
c #PV include 'MAR_PV.inc'
 
      include 'MAR_DY.inc'
c #NH include 'MAR_NH.inc'
 
      include 'MAR_LB.inc'
      include 'MAR_UB.inc'
 
      include 'MAR_SL.inc'
 
      include 'MAR_WK.inc'
 
      include 'MAR_IO.inc'
 
 
C +--Local Variables
C +  ===============
 
c #CS logical   consnd
 
      integer   logcon
      integer   lbcfix,lsf   ,lvg
      integer   intpol,jhsnd1,jhsnd2,i0snd ,j0snd ,ksnd  ,msnd
      integer   ishmin,jshmin,ii    ,jj    ,inew  ,jnew
      integer   iyrnew,mmanew,jdanew,jhunew
      real      tsurf0,qsurf0,gradti,graddt,gra   ,timar ,timarn,timmar
      real      shmin ,distmn,distij,ta_inv,pksh  ,gsnd  ,ddnew
      real      zetaD ,dul   ,ttij_1,ttij_2
      real      qsat0D,acap  ,dem1
 
      real      pksnd(0:40,2)
C +...          pksnd: pSND      ** (R/Cp),
C +             where (pSND/100) ** (R/Cp) : Exner Function
 
      real      tmsnd(0:40,2)
C +...          tmsnd: Potential Temperature
C +                    averaged over the layer between ksnd-1/2 and ksnd+1/2
 
      real      pint   (2)
      real      tint   (2),qint   (2)
 
      real      ttij(mz,2),qvij(mz,2)
      real      ulij(mz,2),vlij(mz,2)
 
      real       dpt(mz),   dqa(mz)
      real       dug(mz),   dvg(mz)
 
      real      fftt(3),   ddtt(3)
 
 
C +--DATA
C +  ====
 
      data lbcfix/1/
 
c #CS data consnd/.true./
 
      data dem1  /1.0e-1/
 
      data lsf   /0/
 
c #OB      openLB=.true.
c #OB      lbcfix=0
 
      lvg    = 0
C +...lvg:     is set to 1 if |Vg(sounding)| .ne. 0 anywhere
 
      acap   = 1.0 / cap
C +...acap:    Inverse of R / Cp
 
      ttij_1 = 0.0
      ttij_2 = 0.0
 
 
C +--Time Parameters
C +  ===============
 
      IF (iterun.eq.0)                                            THEN
        tiSND1 =                                           -99999999.99
        tiSND2 =                                           -99999999.99
         iSND  =   imez
         jSND  =   jmez
        loSND  =   1
      END IF
 
        timar  = ((iyrrGE *365   +iyrrGE*0.25
     .            +njyrGE(mmarGE)+jdarGE           ) *24
     .           + jhurGE        +itizGE(iSND,jSND)     ) *36.d2
     .           + minuGE * 6.d1 +jsecGE
        timarn =   timar
 
 
C +--Interpolation Parameter
C +  =======================
 
      IF (tiSND1.ge.tiSND2)
     .  intpol = 0
C +...  intpol = 0 ==> No Interpolation between two soundings
C +              1 ==>    Interpolation between two soundings
 
 
C +  +++++++++++++++++++++++++++++++
C +--Search of the relevant Sounding
C +  +++++++++++++++++++++++++++++++
 
 
C +--Main Dependant Variables
C +  ========================
 
C +   ------------------------------------
      IF (timar .gt.tiSND2.and.loSND.eq.1)                        THEN
C +   ------------------------------------
 
        open (unit=2,status='old',file='MARsnd.dat')
        rewind     2
         read     (2,202)iyrSND,mmaSND,jdaSND,jhuSND
 202     format   (4i4,f4.0)
         read     (2,203)
 203     format   (1x)
         read     (2,202)iSND,jSND
         read     (2,203)
         read     (2,203)
         read     (2,204)(tSND(ksnd,1),qSND(ksnd,1),
     .                    zSND(ksnd,1),pSND(ksnd,1),
     .                    fSND(ksnd,1),dSND(ksnd,1),ksnd=40,0,-1)
 204     format((6d13.6))
         read     (2,204)zeSND     (1)
         read     (2,202)loSND
 
C +- Time Parameters
C +  ~~~~~~~~~~~~~~~
         jhsnd1 =  (iyrSND*365+iyrSND/4
     .      +njyrGE(mmaSND)+jdaSND)*24+jhuSND+itizGE(iSND,jSND)
         tiSND1 = ((iyrSND*365+iyrSND/4
     .      +njyrGE(mmaSND)+jdaSND)*24+jhuSND+itizGE(iSND,jSND))
     .            *3.6d3
 
C +     ~~~~~~~~~
C +- - -do  until
        msnd = 1
 200    CONTINUE
C +     ~~~~~~~~~
 
          IF (timar .gt.tiSND1.and.loSND.eq.1)                    THEN
            msnd   = msnd +1
            intpol = 1
 
            read     (2,202)iyrnew,mmanew,jdanew,jhunew
            read     (2,203)
            read     (2,202)inew,jnew
            read     (2,203)
            read     (2,203)
            read     (2,204)(tSND(ksnd,2),qSND(ksnd,2),
     .                       zSND(ksnd,2),pSND(ksnd,2),
     .                       fSND(ksnd,2),dSND(ksnd,2),ksnd=40,0,-1)
            read     (2,204)zeSND     (2)
            read     (2,202)loSND
 
            IF (abs(zeSND(2)-zeSND(1)).gt.epsi)
     .        write(6,1)
 1            format(' **********************************',
     .                '**********************************',
     .             /,' * CAUTION: zeSND is Time Dependant',
     .               ' (NOT taken into account in MAR) *',
     .             /,' **********************************',
     .                '**********************************',/,1x)
 
C +- Time Parameters
C +  ~~~~~~~~~~~~~~~
            jhsnd2 =  (iyrnew*365+iyrnew/4
     .         +njyrGE(mmanew)+jdanew)*24+jhunew+itizGE(iSND,jSND)
            tiSND2 = ((iyrnew*365+iyrnew/4
     .         +njyrGE(mmanew)+jdanew)*24+jhunew+itizGE(iSND,jSND))
     .               *3.6d3
 
C +- Change  of Year
C +  ~~~~~~~~~~~~~~~
                timarn = timar
            IF (mmaSND.eq.12.and.mmanew.eq.1)                     THEN
c #YR           jhsnd2 = jhsnd2 + nhyrGE
c #YR           tiSND2 = tiSND2 + nhyrGE * 3600.0
C +...          Change in case of iyrSND and iyrnew not defined (#YR)
 
              IF (mmarGE.eq.1)
     .          timarn = timar  + nhyrGE * 3600.0
            END IF
 
C +- Constant Sounding  IF either imposed (logcon = 1) or MARsnd.dat at EOF
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                 logcon = 0
c #CS       IF (itexpe.eq.0    .and.consnd     ) logcon = 1
            IF (timarn.gt.tiSND2.or.logcon.eq.1)                  THEN
 
                intpol         = 0
 
                jhsnd1         = jhsnd2
                tiSND1         = tiSND2
                iyrSND         = iyrnew
                mmaSND         = mmanew
                jdaSND         = jdanew
                jhuSND         = jhunew
              DO ksnd=0,40
                 tSND(ksnd,1) =   tSND(ksnd,2)
                 qSND(ksnd,1) =   qSND(ksnd,2)
                 zSND(ksnd,1) =   zSND(ksnd,2)
                 pSND(ksnd,1) =   pSND(ksnd,2)
                 fSND(ksnd,1) =   fSND(ksnd,2)
                 dSND(ksnd,1) =   dSND(ksnd,2)
              END DO
            END IF
 
          ELSE
 
C +- Constant Sounding  IF Simulation starts before 1st Sounding Time
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
                intpol         = 0
 
                jhsnd2         = jhsnd1
                tiSND2         = tiSND1
                iyrnew         = iyrSND
                mmanew         = mmaSND
                jdanew         = jdaSND
                jhunew         = jhuSND
              DO ksnd=0,40
                 tSND(ksnd,2) =   tSND(ksnd,1)
                 qSND(ksnd,2) =   qSND(ksnd,1)
                 zSND(ksnd,2) =   zSND(ksnd,1)
                 pSND(ksnd,2) =   pSND(ksnd,1)
                 fSND(ksnd,2) =   fSND(ksnd,1)
                 dSND(ksnd,2) =   dSND(ksnd,1)
              END DO
 
          END IF
 
C +- Continue Read
C +  ~~~~~~~~~~~~~
 
C +     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if (timarn.gt.tiSND2.and.loSND.eq.1)                 go to 200
C +- - -end  do until
C +     ~~~~~~~~~~~~~
 
C +- STOP     Read
C +  ~~~~~~~~~~~~~
 
        close(unit=2)
 
C +- Output  Listing
C +  ~~~~~~~~~~~~~~~
        write(4,205)msnd-intpol,jdaSND,labmGE(mmaSND),iyrSND,
     .                          jhuSND,jhuSND+itizGE(iSND,jSND)
 205    format(/,' SOUNDING No',i2,i6,1x,a3,i5,i4,' TU  (',i2,' LT)',
     .             ' --- MARsnd ---',
     .           /,' =============',
     .           /,' z   (m) | p  (Pa) | T   (K) | qv (kg/kg) |',
     .             ' ff(m/s) | dd(deg) |'
     .           /,'---------+---------+---------+------------+',
     .             '---------+---------+')
 
        write(4,206)(zSND(ksnd,1), pSND(ksnd,1),
     .               tSND(ksnd,1), qSND(ksnd,1),
     .               fSND(ksnd,1), dSND(ksnd,1), ksnd=40,0,-1)
 206    format((2(f8.0,' |'),f8.2,' |',d11.3,' |',f8.2,' |',f8.1,' |'))
C +
        IF (intpol.eq.1)                                          THEN
          write(4,205)msnd  ,jdanew,labmGE(mmanew),iyrnew,
     .                       jhunew,jhunew+itizGE(iSND,jSND)
          write(4,206)(zSND(ksnd,2), pSND(ksnd,2),
     .                 tSND(ksnd,2), qSND(ksnd,2),
     .                 fSND(ksnd,2), dSND(ksnd,2), ksnd=40,0,-1)
        END IF
 
C +   ------
      END IF
C +   ------
 
 
C +--Additional Variables
C +  ====================
 
C +--Time Parameters
C +  ---------------
 
      IF (tiSND2.gt.tiSND1)                                       THEN
          gradti = (timarn-tiSND1)/(tiSND2-tiSND1)
          graddt =  dt            /(tiSND2-tiSND1)
      ELSE
          gradti =  0.0
          graddt =  0.0
      END IF
 
 
C +--Exner Function and Potential Temperature
C +  ----------------------------------------
 
        DO    nSND=1,intpol+1
          DO  ksnd=0,40
            pksnd(ksnd,nSND) = exp(cap*log(dem1*pSND(ksnd,nSND)))
            tpSND(ksnd,nSND) = tSND(ksnd,nSND) *pcap/pksnd(ksnd,nSND)
          END DO
 
 
C +--Potential Temperature Averaging
C +  -------------------------------
 
            tmsnd(   0,nSND) =       tpSND(     1,nSND)
          DO ksnd=1,39
            tmsnd(ksnd,nSND) = 0.5 *(tpSND(ksnd  ,nSND)
     .                              +tpSND(ksnd+1,nSND))
          END DO
            tmsnd(  40,nSND) =       tpSND(  40,nSND)
        END DO
 
 
C +  ++++++++++++++
C +--INITIALISATION
C +  ++++++++++++++
 
C +   ++++++++++++++++
      IF (itexpe.eq.0)                                            THEN
C +   ++++++++++++++++
 
 
C +--Reference Sea Level Air Temperature (K)
C +  =======================================
 
c #NH   taNH    =          tSND(1,1)
 
 
C +--Initialisation   of  the  main    Thermodynamical   Variables
C +  Pressure Thickness, Surface Temperature and Specific Humidity
C +  =============================================================
 
        gra    = -gravit/RDryAi
        pstSND =  0.1 *(pSND(1,1)+gradti*(pSND(1,intpol+1)-pSND(1,1)))
     .                 -ptopDY
        tsurf0 =        tSND(1,1)+gradti*(tSND(1,intpol+1)-tSND(1,1))
        qsurf0 =        qSND(1,1)+gradti*(qSND(1,intpol+1)-qSND(1,1))
 
C +- Reference Grid Point for Temperature Vertical Profile Initialisation
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                  ishmin = iSND
                  jshmin = jSND
C +...            indices for the lowest grid point
 
                   shmin = sh(ishmin,jshmin)
                  distmn =    mx    +my
 
        DO j=1,my
        DO i=1,mx
 
          IF   (sh(i,j).lt.shmin)                                 THEN
C +...    Constraint 1: Reference Grid Point must be the lowest  One
 
                  ishmin = i
                  jshmin = j
                   shmin = sh(ishmin,jshmin)
                  distmn = (i-iSND)*(i-iSND)+(j-jSND)*(j-jSND)
          ELSE
            IF (sh(i,j).eq.shmin)                                 THEN
C +...      Constraint 2: Reference Grid Point must be the closest One
C +                       from the Sounding Grid Point
 
                  distij = (i-iSND)*(i-iSND)+(j-jSND)*(j-jSND)
              IF (distij.lt.distmn)                               THEN
                  ishmin = i
                  jshmin = j
                   shmin = sh(ishmin,jshmin)
                  distmn = distij
              END IF
            END IF
          END IF
 
C +- Surface Elevation is      MSL
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                   pstDY(i,j) = pstSND
                  TairSL(i,j) = tsurf0 - dtagSL
                  qvapSL(i,j) = qsurf0
 
C +- Surface Elevation is NOT  MSL ==> Integration
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          IF (sh(i,j).ne.zero)                                    THEN
C +
            DO  nSND=1,intpol+1
C +
              ksnd    =  1
C +...        ksnd    =  1 (when pSND(mz) -> pSND(0:mz), etc...)
C +                                          zSND(0) = -500m
C +
C + - - - - - do until
 110          CONTINUE
              if (zSND(ksnd,nSND).gt.sh(i,j))                go to 111
                  ksnd   =  ksnd + 1
              go to 110
 111          CONTINUE
C + - - - - - end do until
C +
              pksh      = pksnd(ksnd-1,nSND)
     .                  + gravit *(zSND(ksnd-1,nSND)-sh(i,j))
     .                           *pcap/(cp*tmsnd(ksnd-1,nSND))
              pint(nSND)= exp(acap*log(pksh))
C +
              gsnd      = (tSND(ksnd,nSND)  - tSND(ksnd-1,nSND))
     .                  / (zSND(ksnd,nSND)  - zSND(ksnd-1,nSND))
              tint(nSND)=  tSND(ksnd-1,nSND)
     .                    +gsnd *(sh(i,j)-zSND(ksnd-1,nSND))
              gsnd      = (qSND(ksnd,nSND)  - qSND(ksnd-1,nSND))
     .                  / (zSND(ksnd,nSND)  - zSND(ksnd-1,nSND))
              qint(nSND)=  qSND(ksnd-1,nSND)
     .                    +gsnd *(sh(i,j)-zSND(ksnd-1,nSND))
C +
            END DO
C +
            pstDY (i,j) =  pint(1)+gradti*(pint(intpol+1)-pint(1))
     .                  -  ptopDY
            TairSL(i,j) =  tint(1)+gradti*(tint(intpol+1)-tint(1))
     .                  -  dtagSL
            qvapSL(i,j) =  qint(1)+gradti*(qint(intpol+1)-qint(1))
C +
          END IF
C +
c _SC     qmax        =  qsat0D(TairSL(i,j),unun,pstDY(i,j),ptopDY,lsf)
c _SC     qvapSL(i,j) =    min (qvapSL(i,j),qmax)
C +...    avoids supersaturation (_SC)
C +
          pstDY1(i,j) = pstDY(i,j)
        END DO
        END DO
C +
C +
C +--Temperature and Specific Humidity Vertical Profiles Initialisation
C +  ==================================================================
C +
        DO j=1,my
        DO i=1,mx
C +
          DO nSND=1,intpol+1
C +
C +         **************
            call INIsnd_th(pstDY(i,j),ptopDY,sigmid,sigma,ttij,qvij)
C +         **************
C +
          END DO
C +
        DO k=mz,1,-1
         tairDY(i,j,k) =  ttij(k,1)+gradti*(ttij(k,intpol+1)-ttij(k,1))
         ta_inv        =   min(tairDY(i,j,k),    tairDY(i,j,mz)-dtagSL)
           qvDY(i,j,k) = (qvij(k,1)+gradti*(qvij(k,intpol+1)-qvij(k,1)))
     .                 *  qsat0D(ta_inv       ,sigma(k),
     .                            pstDY(i,j),ptopDY,lsf)
     .                 /  qsat0D(tairDY(i,j,k),sigma(k),
     .                            pstDY(i,j),ptopDY,lsf)
C +...     Last two Lines: Correction for possible Surface Inversion
C +
c _SC      qmax        =  qsat0D(tairDY(i,j,k),sigma(k),
c _SC.                            pstDY(i,j),ptopDY,lsf)
c _SC      qvDY(i,j,k) =  min (qvDY(i,j,k),qmax)
c #OM    tairDY(i,j,k) =  tSND(1,1)+gradti*(tSND(intpol+1,1)-tSND(1,1))
c #OM      qvDY(i,j,k) =  zero
        END DO
        END DO
        END DO
C +
C +
C +--Reduced Potential Temperature     Vertical Profiles Initialisation
C +  ==================================================================
C +
        DO j=1,my
        DO i=1,mx
          pktaDY(i,j,mzz) = TairSL(i,j)
     .                   /((pstDY1(i,j)         +ptopDY)**cap)
        END DO
        END DO
C +
        DO k=1,mz
        DO j=1,my
        DO i=1,mx
          pktaDY(i,j,k)   = tairDY(i,j,k)
     .                   /((pstDY1(i,j)*sigma(k)+ptopDY)**cap)
        END DO
        END DO
        END DO
C +
C +
C +--Geostrophic Wind Vertical Profile 1st Initialisation
C +  ====================================================
C +
        DO j=1,my
        DO i=1,mx
C +
          DO nSND=1,intpol+1
C +
C +
C +--Rotation from x in the West-East Direction to x in Direction GEddxx
C +  -------------------------------------------------------------------
C +
            DO ksnd=0,40
              ddnew       = (GEddxx - dSND(ksnd,nSND))  * degrad
              uuSND(ksnd) =           fSND(ksnd,nSND)   * cos(ddnew)
              vvSND(ksnd) =           fSND(ksnd,nSND)   * sin(ddnew)
            END DO
C +
C +
C +--Vertical Interpolation
C +  ----------------------
C +
C +         **************
            call INIsnd_vl(pstDY(i,j),ptopDY,sigmid,sigma,ulij,vlij)
C +         **************
C +
          END DO
C +
C +
C +--Time Interpolation
C +  ------------------
C +
          DO k=1,mz
            ugeoDY(i,j,k)= ulij(k,1)+gradti*(ulij(k,intpol+1)-ulij(k,1))
            vgeoDY(i,j,k)= vlij(k,1)+gradti*(vlij(k,intpol+1)-vlij(k,1))
          END DO
        END DO
        END DO
C +
            zetaD        =  zeSND(1)+gradti*( zeSND(intpol+1)- zeSND(1))
C +...      zetaD: Large Scale Local Vorticity (CAUTION: Time Independant)
C +
C +
C +--Large Scale Wind Vertical Profile 1st Initialisation
C +  ====================================================
C +
C +
C +--Auxiliary Variable for Mass Flux Computation
C +  --------------------------------------------
C +
        shmin = 100000.0
C +...  shmin : minimum surface elevation (for mass flux computation)
C +
        DO j=1,my
        DO i=1,mx
          IF (sh(i,j).lt.shmin)                                   THEN
            i0snd   = i
            j0snd   = j
            shmin = sh(i,j)
          END IF
        END DO
        END DO
C +
C +
C +--Wind Initialisation, under Constant Potential Vorticity Constraint
C +  ------------------------------------------------------------------
C +
c #PV   IF (potvor.and.mmx.gt.1.and.mmy.eq.1)                     THEN
C +
c #PV     DO k=1,mz
c #PV       ulscPV(k) = ugeoDY(iSND,jSND,k)
c #PV       vlscPV(k) = vgeoDY(iSND,jSND,k)
c #PV       if (abs(ulscPV(k)).gt.zero.or.abs(vlscPV(k)).gt.zero) lvg= 1
c #PV     END DO
C +
C +                                  **************
c #PV     IF (mmx.gt.1.and.lvg.eq.1) call INIsnd_PV(zetaD)
C +                                  **************
C +
C +...  Initialisation based on Potential Vorticity Conservation
C +     Based on the Direct Integration of the Relative Vorticity
C +
C +
C +--Wind Initialisation under Constant Mass Flux Constraint
C +  -------------------------------------------------------
C +
c #PV   ELSE
C +
          IF (conmas)                                             THEN
              DO k=1,mz
              DO j=1,my
              DO i=1,mx
                WKxyz1(i,j,k) = pstDY(i0snd,j0snd) * ugeoDY(i,j,k)
                WKxyz2(i,j,k) = pstDY(i0snd,j0snd) * vgeoDY(i,j,k)
                uairDY(i,j,k) = WKxyz1(i,j,k)      /  pstDY(i,j)
                vairDY(i,j,k) = WKxyz2(i,j,k)      /  pstDY(i,j)
C +...          Geostrophic Wind only used in Large Scale Press.Grad.Force
C +             Real        Wind takes into Account Mass Conservation
C +
              END DO
              END DO
              END DO
C +
            IF (mmy.eq.1)                                         THEN
              DO k=1,mz
              DO j=1,my
              DO i=1,mx
                vairDY(i,j,k) =                      vgeoDY(i,j,k)
              END DO
              END DO
              END DO
            END IF
C +
          ELSE
C +
              DO k=1,mz
              DO j=1,my
              DO i=1,mx
                uairDY(i,j,k) =                      ugeoDY(i,j,k)
                vairDY(i,j,k) =                      vgeoDY(i,j,k)
              END DO
              END DO
              END DO
          END IF
c #PV   END IF
C +
C +
C +--Output
C +  ------
C +
c #PV   write(21,182)(vgeoDY(i,1,mz),i=1,mx)
 182    format(/,  '  Vg(i,1,mz)      :',/,(15f8.2))
c #PV   write(21,183)(vairDY(i,1,mz),i=1,mx)
 183    format(/,  '  V (i,1,mz)      :',/,(15f8.2))
c #PV   write(21,184)(ugeoDY(i,1,mz),i=1,mx)
 184    format(/,  '  Ug(i,1,mz)      :',/,(15f8.2))
c #PV   write(21,185)(uairDY(i,1,mz),i=1,mx)
 185    format(/,  '  U (i,1,mz)      :',/,(15f8.2))
C +
        IF (IO_loc.ge.2)                                          THEN
            write(21,1860)
 1860       format(1x)
            write(21,1861) (ugeoDY(iSND,jSND,k),k=1,mz)
 1861       format('  ug     =',/,(15f7.1,' m/sec'))
C +
c #PV     IF (potvor.and.mmx.gt.1.and.mmy.eq.1)                   THEN
c #PV       write(21,1860)
c #PV       write(21,1862)  adugPV
 1862       format('  adugPV =',  f7.3,' m/sec')
c #PV     END IF
C +
            write(21,1860)
            write(21,1863)  (vgeoDY(iSND,jSND,k),k=1,mz)
 1863       format('  vg     =',/,(15f7.1,' m/sec'))
C +
c #PV     IF (potvor.and.mmx.gt.1.and.mmy.eq.1)                   THEN
c #PV       write(21,1860)
c #PV       write(21,1864) (1.d3*advgPV(iSND, k),k=1,mz)
 1864       format('  advgPV =',/,(15f7.3,' mm/sec'))
c #PV     END IF
C +
            write(21,1860)
        END IF
C +
C +
C +   ++++++
      END IF
C +   ++++++
 
 
C +  +++++++++++++++++++++++++++++++++++++
C +--INITIALIZATION of BOUNDARY CONDITIONS
C +  +++++++++++++++++++++++++++++++++++++
 
C +   ++++++++++++++++++
      IF   (itexpe.eq.0)                                          THEN
C +   ++++++++++++++++++
 
 
C +     ================
        IF (log_1D.eq.0)                                          THEN
C +     ================
 
C +--Upper
C +  -----
            DO     k=1,mzabso
              DO   j=1,my
                DO i=1,mx
                  uairUB(i,j,k)   = uairDY(i,j,k)
                  vairUB(i,j,k)   = vairDY(i,j,k)
                  pktaUB(i,j,k)   = pktaDY(i,j,k)
                END DO
              END DO
            END DO
 
C +--x Axis
C +  ------
          IF (mmx.gt.1)                                           THEN
 
            DO     k=1,mz
 
              DO   j=1,my
                DO i=1,n7mxLB
                  vaxgLB(i,j,k,1) = uairDY(i,j,k)
                  vaxgLB(i,j,k,2) = vairDY(i,j,k)
                  vaxgLB(i,j,k,3) =   qvDY(i,j,k)
                  vaxgLB(i,j,k,4) = pktaDY(i,j,k)
                  vaxgLB(i,j,1,5) =  pstDY(i,j)
                END DO
                DO i=mx-n6mxLB,mx
                  vaxdLB(i,j,k,1) = uairDY(i,j,k)
                  vaxdLB(i,j,k,2) = vairDY(i,j,k)
                  vaxdLB(i,j,k,3) =   qvDY(i,j,k)
                  vaxdLB(i,j,k,4) = pktaDY(i,j,k)
                  vaxdLB(i,j,1,5) =  pstDY(i,j)
                END DO
              END DO
 
            END DO
 
          END IF
 
C +- y Axis
C +  ------
          IF (mmy.gt.1)                                           THEN
 
            DO     k=1,mz
 
              DO   i=1,mx
                DO j=1,n7myLB
                  vayiLB(i,j,k,1) = uairDY(i,j,k)
                  vayiLB(i,j,k,2) = vairDY(i,j,k)
                  vayiLB(i,j,k,3) =   qvDY(i,j,k)
                  vayiLB(i,j,k,4) = pktaDY(i,j,k)
                  vayiLB(i,j,1,5) =  pstDY(i,j)
                END DO
                DO j=my-n6myLB,my
                  vaysLB(i,j,k,1) = uairDY(i,j,k)
                  vaysLB(i,j,k,2) = vairDY(i,j,k)
                  vaysLB(i,j,k,3) =   qvDY(i,j,k)
                  vaysLB(i,j,k,4) = pktaDY(i,j,k)
                  vaysLB(i,j,1,5) =  pstDY(i,j)
                END DO
              END DO
 
            END DO
 
          END IF
 
 
C +     ====
        ELSE
C +     ====
 
          IF (tequil.gt.0.0)                                      THEN
 
C +--Upper
C +  -----
            DO     k=1,mzabso
              DO   j=1,my
                DO i=1,mx
                  uairUB(i,j,k)   = uairDY(i,j,k)
                  vairUB(i,j,k)   = vairDY(i,j,k)
                  pktaUB(i,j,k)   = pktaDY(i,j,k)
                END DO
              END DO
            END DO
 
C +--x Axis
C +  ------
            IF (mmx.gt.1)                                         THEN
 
              DO       k=1,mz
 
                DO     j=1,my
                  DO   i=2,n7mxLB
                    vaxgLB( i,j,k,1) = uairDY(i,             j,k)
                    vaxgLB( i,j,k,2) = vairDY(i,             j,k)
                    vaxgLB( i,j,1,5) = pstDYn(i,             j)
                  END DO
                    vaxgLB( 1,j,k,1) = uairDY(ip11-lbcfix,j,k)
                    vaxgLB( 1,j,k,2) = vairDY(ip11-lbcfix,j,k)
                    vaxgLB( 1,j,1,5) = pstDYn(ip11-lbcfix,j)
 
                  DO   i=mx-n6mxLB,mx1
                    vaxdLB( i,j,k,1) = uairDY(i,             j,k)
                    vaxdLB( i,j,k,2) = vairDY(i,             j,k)
                    vaxdLB( i,j,1,5) = pstDYn(i,             j)
                  END DO
                    vaxdLB(mx,j,k,1) = uairDY(mx1+lbcfix,j,k)
                    vaxdLB(mx,j,k,2) = vairDY(mx1+lbcfix,j,k)
                    vaxdLB(mx,j,1,5) = pstDYn(mx1+lbcfix,j)
                END DO
 
              END DO
 
            END IF
 
C +- y Axis
C +  ------
            IF (mmy.gt.1)                                         THEN
 
              DO  k=1,mz
 
                DO  i=1,mx
                  DO  j=1,n7myLB
                    vayiLB(i, j,k,1) = uairDY(i,j,             k)
                    vayiLB(i, j,k,2) = vairDY(i,j,             k)
                    vayiLB(i, j,k,5) = pstDYn(i,j)
                  END DO
                    vayiLB(i, 1,k,1) = uairDY(i,jp11-lbcfix,k)
                    vayiLB(i, 1,k,2) = vairDY(i,jp11-lbcfix,k)
                    vayiLB(i, 1,k,5) = pstDYn(i,jp11-lbcfix)
                  DO  j=my-n6myLB,my1
                    vaysLB(i, j,k,1) = uairDY(i,j,             k)
                    vaysLB(i, j,k,2) = vairDY(i,j,             k)
                    vaysLB(i, j,k,5) = pstDYn(i,j)
                  END DO
                    vaysLB(i,my,k,1) = uairDY(i,my1+lbcfix,k)
                    vaysLB(i,my,k,2) = vairDY(i,my1+lbcfix,k)
                    vaysLB(i,my,k,5) = pstDYn(i,my1+lbcfix)
                END DO
 
              END DO
 
            END IF
 
          END IF
 
 
C +     ======
        END IF
C +     ======
 
 
C +  +++++++++++++++++++++++++++++++++++++
C +--UPDATE of LATERAL BOUNDARY CONDITIONS
C +  +++++++++++++++++++++++++++++++++++++
C +
C +   ++++
      ELSE
C +   ++++
C +
C +
C +     ================
        IF (intpol.gt.0)                                          THEN
C +     ================
C +
C +
C +--Temperature and Specific Humidity Vertical Profiles Interpolation
C +  =================================================================
C +
C +
C +--x Axis / x <<
C +  -------------
C +
          IF  (mmx.gt.1)                                          THEN
C +
               i=1
            DO j=1,my
c #pv          i=iSND
C +
C +- Vertical  Interpolation
C +  ~~~~~~~~~~~~~~~~~~~~~~~
              DO nSND=1,2
C +
C +             **************
                call INIsnd_th(pstDY(i,j),ptopDY,sigmid,sigma,ttij,qvij)
C +             **************
C +
              END DO
c #pv          i=1
C +...CAUTION: vaxgLB assumed at i=1 assumed to be that of the Sounding Point
C +            when Potential Temperature is conserved  at the Synoptic Scale
C +
C +- Time  Interpolation
C +  ~~~~~~~~~~~~~~~~~~~
              DO k=1,mz
                dpt(k) = (ttij(k,1) + gradti*(ttij(k,2)-ttij(k,1)))
     .                 /  exp(cap*log(pstDY(i,j)*sigma(k)+ptopDY))
     .                 -vaxgLB(i,j,k,4)
                dqa(k) =  qvij(k,1) + gradti*(qvij(k,2)-qvij(k,1))
     .                 -vaxgLB(i,j,k,3)
              END DO
C +
              IF (openLB)                                         THEN
                DO ii=1,n7mxLB
                DO  k=1,mz
                  vaxgLB(ii,j,k,4) =  vaxgLB(ii,j,k,4) + dpt(k)
                  vaxgLB(ii,j,k,3) =  vaxgLB(ii,j,k,3) + dqa(k)
                END DO
                END DO
              ELSE
                DO ii=1,n7mxLB
                DO  k=1,mz
                  vaxgLB(ii,j,k,4) =  pktaDY( 1,j,k)   + dpt(k)
                  vaxgLB(ii,j,k,3) =    qvDY( 1,j,k)   + dqa(k)
                END DO
                END DO
              END IF
C +
            END DO
C +
C +
C +- x Axis / x >>
C +  -------------
C +
               i=  mx
            DO j=1,my
c #pv          i=iSND
C +
C +- Vertical  Interpolation
C +  ~~~~~~~~~~~~~~~~~~~~~~~
              DO nSND=1,2
C +
C +             **************
                call INIsnd_th(pstDY(i,j),ptopDY,sigmid,sigma,ttij,qvij)
C +             **************
C +
              END DO
C +
c #pv          i=mx
C +
C +- Time  Interpolation
C +  ~~~~~~~~~~~~~~~~~~~
              DO k=1,mz
                dpt(k) = (ttij(k,1) + gradti*(ttij(k,2)-ttij(k,1)))
     .                 /  exp(cap*log(pstDY(i,j)*sigma(k)+ptopDY))
     .                 -vaxdLB(i,j,k,4)
                dqa(k) =  qvij(k,1) + gradti*(qvij(k,2)-qvij(k,1))
     .                 -vaxdLB(i,j,k,3)
              END DO
                ttij_1 = ttij(mz,1)
                ttij_2 = ttij(mz,2)
C +
              IF (openLB)                                         THEN
                DO ii=mx-n6mxLB,mx
                DO  k=1,mz
                  vaxdLB(ii,j,k,4) =  vaxdLB(ii,j,k,4) + dpt(k)
                  vaxdLB(ii,j,k,3) =  vaxdLB(ii,j,k,3) + dqa(k)
                END DO
                END DO
              ELSE
                DO ii=mx-n6mxLB,mx
                DO  k=1,mz
                  vaxdLB(ii,j,k,4) =  pktaDY(mx,j,k)   + dpt(k)
                  vaxdLB(ii,j,k,3) =    qvDY(mx,j,k)   + dqa(k)
                END DO
                END DO
              END IF
C +
            END DO
C +
c #WR       IF (mod(minuGE,30).eq.0.and.jsecGE.eq.0)
c #WR.        write(6,608)mmaSND,jdaSND,jhuSND+itizGE(iSND,jSND),
c #WR.                    mmarGE,jdarGE,jhurGE,
c #WR.                    mmanew,jdanew,jhunew+itizGE(iSND,jSND),
c #WR.                    tiSND1,timmar,tiSND2,
c #WR.                    gradti,vaxdLB(mx,my,mz,4)*pcap
 608          format(3(i6,'/',i2,'/',i2,'LT'),3f13.0,
     .               ' |Time| =',f5.2,5x,' Theta_CLS =',f7.2)
C +
          END IF                                         ! {end mmx > 1} CTR
C +
C +
C +- y Axis / y <<
C +  -------------
C +
          IF  (mmy.gt.1)                                          THEN
C +
               j=1
            DO i=1,mx
C +
C +- Vertical  Interpolation
C +  ~~~~~~~~~~~~~~~~~~~~~~~
            DO nSND=1,2
C +
C +           **************
              call INIsnd_th(pstDY(i,j),ptopDY,sigmid,sigma,ttij,qvij)
C +           **************
C +
            END DO
C +
C +- Time  Interpolation
C +  ~~~~~~~~~~~~~~~~~~~
            DO  k=1,mz
              dpt(k) = (ttij(k,1) + gradti*(ttij(k,2)-ttij(k,1)))
     .               /  exp(cap*log(pstDY(i,j)*sigma(k)+ptopDY))
     .               -vayiLB(i,j,k,4)
              dqa(k) =  qvij(k,1) + gradti*(qvij(k,2)-qvij(k,1))
     .               -vayiLB(i,j,k,3)
            END DO
C +
            IF (openLB)                                           THEN
              DO jj=1,n7myLB
              DO  k=1,mz
                vayiLB(i,jj,k,4) =  vayiLB(i,jj,k,4) + dpt(k)
                vayiLB(i,jj,k,3) =  vayiLB(i,jj,k,3) + dqa(k)
              END DO
              END DO
            ELSE
              DO jj=1,n7myLB
              DO  k=1,mz
                vayiLB(i,jj,k,4) =  pktaDY(i, 1,k)   + dpt(k)
                vayiLB(i,jj,k,3) =    qvDY(i, 1,k)   + dqa(k)
              END DO
              END DO
            END IF
C +
            END DO
C +
C +
C +- y Axis / y >>
C +  -------------
C +
               j=  my
            DO i=1,mx
C +
C +- Vertical  Interpolation
C +  ~~~~~~~~~~~~~~~~~~~~~~~
              DO nSND=1,2
C +
C +             **************
                call INIsnd_th(pstDY(i,j),ptopDY,sigmid,sigma,ttij,qvij)
C +             **************
C +
              END DO
C +
C +- Time  Interpolation
C +  ~~~~~~~~~~~~~~~~~~~
              DO  k=1,mz
                dpt(k) = (ttij(k,1) + gradti*(ttij(k,2)-ttij(k,1)))
     .                 /  exp(cap*log(pstDY(i,j)*sigma(k)+ptopDY))
     .                 -vaysLB(i,j,k,4)
                dqa(k) =  qvij(k,1) + gradti*(qvij(k,2)-qvij(k,1))
     .                 -vaysLB(i,j,k,3)
              END DO
C +
              IF (openLB)                                         THEN
                DO jj=my-n6myLB,my
                DO  k=1,mz
                  vaysLB(i,jj,k,4) =  vaysLB(i,jj,k,4) + dpt(k)
                  vaysLB(i,jj,k,3) =  vaysLB(i,jj,k,3) + dqa(k)
                END DO
                END DO
              ELSE
                DO jj=my-n6myLB,my
                DO  k=1,mz
                  vaysLB(i,jj,k,4) =  pktaDY(i,my,k)   + dpt(k)
                  vaysLB(i,jj,k,3) =    qvDY(i,my,k)   + dqa(k)
                END DO
                END DO
              END IF
C +
            END DO
C +
          END IF                                         ! {end mmy > 1} CTR
C +
C +
C +--Large Scale Wind Vertical Profiles Interpolation / Dynamical Adjustment
C +  =======================================================================
C +
C +
C +--Mass Flux Auxiliary Variable
C +  ----------------------------
C +
          IF  (conmas)                                            THEN
C +
            DO j=1,my
            DO i=1,mx
              WKxy1(i,j) = pstDY1(iSND,jSND)/pstDY1(i,j)
            END DO
            END DO
C +
          ELSE
C +
            DO j=1,my
            DO i=1,mx
              WKxy1(i,j) = 1.0
            END DO
            END DO
C +
          END IF
C +
C +
C +--Large Scale Wind Sounding: Default
C +  ----------------------------------
C +
          DO k=1,mz
            dug(k) = 0.0
            dvg(k) = 0.0
          END DO
C +
C +
C +--Rotation from x in the West-East Direction to x in Direction GEddxx
C +  -------------------------------------------------------------------
C +
          DO nSND=1,intpol+1
            DO ksnd=0,40
              ddnew       = (GEddxx - dSND(ksnd,nSND))  * degrad
              uuSND(ksnd) =           fSND(ksnd,nSND)   * cos(ddnew)
              vvSND(ksnd) =           fSND(ksnd,nSND)   * sin(ddnew)
            END DO
C +
C +
C +--Vertical Interpolation
C +  ----------------------
C +
C +           **************
              call INIsnd_vl(pstDY(iSND,jSND),ptopDY,sigmid,sigma,
     .                       ulij,vlij)
C +           **************
C +
          END DO
C +
C +
C +--Time Interpolation
C +  ------------------
C +
          DO k=1,mz
            dug(k) = graddt*(ulij(k,intpol+1)-ulij(k,1))
            dvg(k) = graddt*(vlij(k,intpol+1)-vlij(k,1))
          END DO
C +
C +
C +--Update of Direct Integration of Wind constrained by PV Conservation
C +  -------------------------------------------------------------------
C +
c #PV     IF (potvor.and.mmx.gt.1.and.mmy.eq.1)                   THEN
C +
c #PV       DO k=1,mz
c #PV         ulscPV(  k) =   ulscPV(k) + dug(k)
c #PV         vlscPV(  k) =   vlscPV(k) + dvg(k)
c #PV       END DO
C +
c #PV       DO k=1,mz
c #PV       DO j=1,my
c #PV       DO i=1,mx
c #PV         advbPV(i,k) = advbPV(i,k) + dvg(k)
c #PV       END DO
c #PV       END DO
c #PV       END DO
c #PV     END IF
C +
C +- PV   Conservation
C +  ~~~~~~~~~~~~~~~~~
c #PV     IF (potvor.and.mmx.gt.1.and.mmy.eq.1)                   THEN
C +
c #PV       DO k=1,mz
c #PV       DO j=1,my
c #PV       DO i=1,mx
c #PV         ugeoDY(i,j,k) =             adugPV       *ulscPV    (k)
c #PV         vgeoDY(i,j,k) = advbPV(i,k)
c #PV.                       +advgPV(i,k)*ugeoDY(i,j,k)*ugeoDY(i,j,k)
C +...        PV Conservation Constraint is included
C +
c #PV         dul           =   dug(k)   *adugPV       *adubPV(i,  k)
C +...        uairDY_Synop := uairDY        +dul
C +           vairDY_Synop := vairDY        +dvg(k)
C +           isallobaric wind contained in (dul,dvl=dvg)
C +
c #PV         ugeoDY(i,j,k) = ugeoDY(i,j,k) +dvg(k)
c #PV.                      /(fcorDY(imez,jmez)*dt)
c #PV         vgeoDY(i,j,k) = vgeoDY(i,j,k) -dul
c #PV.                      /(fcorDY(imez,jmez)*dt)
c #PV       END DO
c #PV       END DO
c #PV       END DO
c #PV     ELSE
C +
            DO k=1,mz
            DO j=1,my
            DO i=1,mx
              ugeoDY(i,j,k) = ugeoDY(i,j,k) +dug(k)
              vgeoDY(i,j,k) = vgeoDY(i,j,k) +dvg(k)
              uairDY(i,j,k) = uairDY(i,j,k) +dug(k) *WKxy1(i,j)
              vairDY(i,j,k) = vairDY(i,j,k) +dvg(k) *WKxy1(i,j)
            END DO
            END DO
            END DO
c #PV     END IF
C +
C +
C +--Lateral Boundaries
C +  ------------------
C +
          IF (openLB)                                             THEN
C +
C +- x Axis / x <<
C +  ~~~~~~~~~~~~~
            DO i=1,n7mxLB
            DO j=1,my
c #PV         IF (potvor.and.mmx.gt.1.and.mmy.eq.1)               THEN
c #PV           DO k=1,mz
c #PV             vaxgLB(i,j,k,1)=
c #PV.            vaxgLB(i,j,k,1)+dug(k)*adubPV(i,k)*adugPV
c #PV             vaxgLB(i,j,k,2)=
c #PV.            vaxgLB(i,j,k,2)+dvg(k)
c #PV           END DO
c #PV         ELSE
                DO k=1,mz
                  vaxgLB(i,j,k,1)=
     .            vaxgLB(i,j,k,1)+dug(k)            *WKxy1(i,j)
                  vaxgLB(i,j,k,2)=
     .            vaxgLB(i,j,k,2)+dvg(k)            *WKxy1(i,j)
                END DO
c #PV         END IF
            END DO
            END DO
C +
C +- x Axis / x >>
C +  ~~~~~~~~~~~~~
            DO i=mx-n6mxLB,mx
            DO j=1,my
c #PV         IF (potvor.and.mmx.gt.1.and.mmy.eq.1)               THEN
c #PV           DO k=1,mz
c #PV             vaxdLB(i,j,k,1)=
c #PV.            vaxdLB(i,j,k,1)+dug(k)*adubPV(i,k)*adugPV
c #PV             vaxdLB(i,j,k,2)=
c #PV.            vaxdLB(i,j,k,2)+dvg(k)
c #PV           END DO
c #PV         ELSE
                DO k=1,mz
                  vaxdLB(i,j,k,1)=
     .            vaxdLB(i,j,k,1)+dug(k)            *WKxy1(i,j)
                  vaxdLB(i,j,k,2)=
     .            vaxdLB(i,j,k,2)+dvg(k)            *WKxy1(i,j)
                END DO
c #PV         END IF
            END DO
            END DO
C +
C +- y Axis / y <<
C +  ~~~~~~~~~~~~~
            IF (mmy.gt.1)                                         THEN
C +
              DO i=1,mx
              DO j=1,n7myLB
              DO k=1,mz
                vayiLB(i,j,k,1)=
     .          vayiLB(i,j,k,1)+dug(k)            *WKxy1(i,j)
                vayiLB(i,j,k,2)=
     .          vayiLB(i,j,k,2)+dvg(k)            *WKxy1(i,j)
              END DO
              END DO
              END DO
C +
C +- y Axis / y >>
C +  ~~~~~~~~~~~~~
              DO i=1,mx
              DO j=my-n6myLB,my
              DO k=1,mz
                vaysLB(i,j,k,1)=
     .          vaysLB(i,j,k,1)+dug(k)            *WKxy1(i,j)
                vaysLB(i,j,k,2)=
     .          vaysLB(i,j,k,2)+dvg(k)            *WKxy1(i,j)
              END DO
              END DO
              END DO
C +
            END IF
C +
          ELSE                       ! {end openLB / begin .NOT. openLB} CTR
C +
C +- x Axis / x <<
C +  ~~~~~~~~~~~~~
            IF (mmx.gt.1)                                         THEN
              DO i=1,n7mxLB
              DO j=1,my
              DO k=1,mz
                vaxgLB(i,j,k,1)= uairDY( 1,j,k)
                vaxgLB(i,j,k,2)= vairDY( 1,j,k)
              END DO
              END DO
              END DO
C +
C +- x Axis / x >>
C +  ~~~~~~~~~~~~~
              DO i=mx-n6mxLB,mx
              DO j=1,my
              DO k=1,mz
                vaxdLB(i,j,k,1)= uairDY(mx,j,k)
                vaxdLB(i,j,k,2)= vairDY(mx,j,k)
              END DO
              END DO
              END DO
            END IF
C +
C +- y Axis / y <<
C +  ~~~~~~~~~~~~~
            IF (mmy.gt.1)                                         THEN
              DO k=1,mz
              DO j=1,n7myLB
              DO i=1,mx
                vayiLB(i,j,k,1)= uairDY(i, 1,k)
                vayiLB(i,j,k,2)= vairDY(i, 1,k)
              END DO
              END DO
              END DO
C +
C +- y Axis / y >>
C +  ~~~~~~~~~~~~~
              DO k=1,mz
              DO j=1,n7myLB
              DO i=1,mx
                vaysLB(i,j,k,1)= uairDY(i,my,k)
                vaysLB(i,j,k,2)= vairDY(i,my,k)
              END DO
              END DO
              END DO
            END IF
          END IF                                    ! {end .NOT. openLB} CTR
C +
C +     ======
        END IF
C +     ======
C +
C +
C +   ++++++
      END IF
C +   ++++++
C +
C +
C +--OUTPUT (Each Hour)
C +  ==================
C +
C +   --------------------------------
      IF (minuGE.eq.0.and.jsecGE.eq.0)                            THEN
C +   --------------------------------
C +
        DO  nSND  = 1,2
              fftt(nSND) = sqrt(ulij(mz,nSND)*ulij(mz,nSND)
     .                         +vlij(mz,nSND)*vlij(mz,nSND))
          IF                   (ulij(mz,nSND).ne.0.0)             THEN
              ddtt(nSND) = atan(vlij(mz,nSND)/ulij(mz,nSND))
            IF                 (ulij(mz,nSND).lt.zero)
     .        ddtt(nSND) = ddtt(nSND) + pi
          ELSE
            IF                 (vlij(mz,nSND).gt.zero)            THEN
              ddtt(nSND) =  0.5 * pi
            ELSE
              ddtt(nSND) = -0.5 * pi
            END IF
          END IF
        END DO
C +
            fftt(   3) = sqrt(ugeoDY(iSND,jSND,mz)*ugeoDY(iSND,jSND,mz)
     .                       +vgeoDY(iSND,jSND,mz)*vgeoDY(iSND,jSND,mz))
        IF                   (ugeoDY(iSND,jSND,mz).ne.zero)       THEN
            ddtt(   3) = atan(vgeoDY(iSND,jSND,mz)/ugeoDY(iSND,jSND,mz))
          IF                 (ugeoDY(iSND,jSND,mz).lt.zero)
     .      ddtt(   3) = ddtt(nSND) + pi
        ELSE
          IF                (vgeoDY(iSND,jSND,mz).gt.zero)        THEN
            ddtt(   3) =  0.5 * pi
          ELSE
            ddtt(   3) = -0.5 * pi
          END IF
        END IF
C +
        DO  nSND  = 1,3
           ddtt(nSND) = ddtt(nSND) * 180.0 / pi
           ddtt(nSND) =-ddtt(nSND) +  90.0
        END DO
C +
        write(4,442)
 442    format(
     .  /,' yyyy-MM-jj-UT-mm |  uL m/s |  vL m/s |  VL m/s |  dd deg |',
     .     ' T(mx,mz) K |',
     .  /,' -----------------+---------+---------+---------+---------+',
     .     '------------+')
C +
        i = iSND
        j = jSND
C +
C +     ***********
        call TIMcor
C +     ***********
C +
        write(4,443)iyrSND,mmaSND,jdaSND,jhuSND,izr,
     .              ulij      (mz,1),vlij      (mz,1),
     .              fftt(1),ddtt(1),ttij_1
        write(4,443)iyrrGE,mmplus,jdplus,jhurGE,minuGE,
     .              ugeoDY(iSND,jSND,mz),vgeoDY(iSND,jSND,mz),
     .              fftt(3),ddtt(3),
     .              vaxdLB(mx,1,mz,4)
     .             *exp(cap*log(pstDY(mx,1)*sigma(mz)+ptopDY))
        if         (mmanew.gt.0)
     .  write(4,443)iyrnew,mmanew,jdanew,jhunew,izr,
     .              ulij      (mz,2),vlij      (mz,2),
     .              fftt(2),ddtt(2),ttij_2
 443    format(i5,4('-',i2),
     .                  ' |',f8.2, ' |',f8.2,' |',f8.2,' |',f8.1,' |',
     .                      f10.3,'  |')
        write(4,444)
 444    format(/,1x)
C +
C +
C +   ------
      END IF
C +   ------
C +
C +
C +--Work Arrays Reset
C +  =================
C +
C +
      DO j=1,my
      DO i=1,mx
        WKxy1(i,j) = 0.0
      END DO
      END DO
C +
      DO k=1,mz
      DO j=1,my
      DO i=1,mx
        WKxyz1(i,j,k) = 0.0
        WKxyz2(i,j,k) = 0.0
      END DO
      END DO
      END DO
C +
      return
C +
      end
