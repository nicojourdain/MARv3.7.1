 
 
      subroutine OUT_nc
 
C +------------------------------------------------------------------------+
C | MAR OUTPUT                                        Fri 20-Jan-2012  MAR |
C |   SubRoutine OUT_nc is used to write the main Model Variables          |
C |                                      on  a NetCDF file                 |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT: ipr_nc: Current time step    number                           |
C |   ^^^^^^         (starting from ipr_nc=1, which => new file creation)  |
C |                                                                        |
C |   OUTPUT: NetCDF File adapted to IDL Graphic Software                  |
C |   ^^^^^^                                                               |
C |                                                                        |
C |   CAUTION:    This Routine requires the usual NetCDF library,          |
C |   ^^^^^^^^    and the complementary access library  'libUN.a'          |
C |                                                                        |
C |   MODIF.  4 Nov 2009   : OUTPUT of Map Scaling Factor SFm_DY           |
C |   ^^^^^                                                                |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
 
      include 'MAR_DY.inc'
c #NH include 'MAR_NH.inc'
c #OL include 'MAR_OL.inc'
 
      include 'MAR_TE.inc'
      include 'MAR_TU.inc'
 
      include 'MAR_CA.inc'
      include 'MAR_HY.inc'
c #WB include 'MAR_WB.inc'
c #BS include 'MAR_BS.inc'
      include 'MAR_RA.inc'
c #TC include 'MAR_TC.inc'
 
      include 'MAR_SL.inc'
      include 'MAR_SV.inc'
      include 'MARdSV.inc'
      include 'MAR_TV.inc'
      include 'MARsSN.inc'
c #PO include 'MAR_PO.inc'
 
      include 'MAR_WK.inc'
c #DY include 'MARvec.inc'
 
      include 'MAR_IO.inc'
 
      real              Soilz(mz)
      common/OUT_nc_rea/Soilz
 
      integer   nsnomz,         kk
      parameter(nsnomz=min(nsno,mz))
 
      integer           kOUTnc,nOUTnc
      integer           ijSNOW(mx,my)
      common/OUT_nc_int/kOUTnc,nOUTnc
     .                 ,ijSNOW
 
 
C +--Local  Variables
C +  ================
 
c #IZ logical           noZOOM
      logical           LastIO
      real              end_YR
 
      integer    Lfnam,     Ltit,     Luni,     Lnam,     Llnam
      PARAMETER (Lfnam= 40, Ltit= 90, Luni= 31, Lnam= 13, Llnam=50)
C +...Length of char strings
 
      CHARACTER*(Lfnam)  fnamNC
      common/OUT_nc_loc/ fnamNC
C +...                   fnamNC: To retain file name.
 
      integer    NdimNC
      PARAMETER (NdimNC = 5)
C +...Number of defined spatial dimensions (exact)
 
      integer    MXdim
      PARAMETER (MXdim = 86401)
C +...Maximum Number of all dims: recorded Time Steps
C +   and also maximum of spatial grid points for each direction.
 
      integer    MX_var
      PARAMETER (MX_var = 120)
C +...Maximum Number of Variables
 
      integer    NattNC
      PARAMETER (NattNC = 2)
C +...Number of real attributes given to all variables
 
      INTEGER           RCODE
 
      integer           njmo
      integer           jourNC(MXdim)
      integer           moisNC(MXdim)
      real              yearNC(MXdim)
      real              dateNC(MXdim)
      common/OUT_nc_r/  yearNC,dateNC
      real              timeNC(MXdim)
      real              VALdim(MXdim,0:NdimNC)
      integer           nDFdim(      0:NdimNC)
      common/OUT_nc_i/  nDFdim
      integer           NvatNC(NattNC)
      CHARACTER*(Lnam)  NAMdim(      0:NdimNC)
      CHARACTER*(Luni)  UNIdim(      0:NdimNC)
      CHARACTER*(Lnam)  SdimNC(4,MX_var)
      CHARACTER*(Luni)  unitNC(MX_var)
      CHARACTER*(Lnam)  nameNC(MX_var)
      CHARACTER*(Llnam) lnamNC(MX_var)
      CHARACTER*(Ltit ) tit_NC
      CHARACTER*(Lnam)  NAMrat(NattNC)
c #TC CHARACTER*9   labelc
      CHARACTER*120 tmpINP
 
      integer  n1000 ,n100a ,n100  ,n10_a ,n10   ,n1    ,m10
      integer  n     ,l     ,              jd10  ,jd1   ,MMXstp
      integer  it    ,mois  ,mill  ,iu    ,itotNC,NtotNC,ID__nc
c #TC integer  itot
      real     starti,starta(1),DayLen,rhodzk
 
      character*1  chn
      character*7  lab__z,lab_dz,lab_ro,lab_wa,lab_g1,lab_g2,lab_Ti
 
      data lab__z/'z_mmWE '/
      data lab_dz/'dzSNOW '/
      data lab_ro/'roSNOW '/
      data lab_wa/'waSNOW '/
      data lab_g1/'g1SNOW '/
      data lab_g2/'g2SNOW '/
      data lab_Ti/'TiSNOW '/
 
c #IZ data     noZOOM/.FALSE./
 
 
C +--NetCDF File Initialization
C +  ==========================
 
      IF (ipr_nc.eq.1)                                              THEN
 
 
C +--Nb of OUTPUT per File (Option #SO for splitting the MAR*.nc file)
C +  ---------------------
 
            nOUTnc =      npr_nc
c #SO       nOUTnc = (2.0 /0.09)*(161.*161.*34.)/(mx*my*mzz)
c #SO       kOUTnc =  86400.1                   /(dt*nboucl)
c #SO       nOUTnc =             nOUTnc         /    kOUTnc
c #SO       nOUTnc =             nOUTnc         *    kOUTnc
c #SO       nOUTnc =  min(       nOUTnc,
c #SO.                           kOUTnc*(365+max(0,1-mod(iyrrGE,4))))
c #SO   IF (npr_nc.EQ.nprint+1)                                     THEN
c #SO       nOUTnc =             nOUTnc         +         1
c #SO   ENDIF
c #SO       nOUTnc =  min(npr_nc,nOUTnc)
            npr_nc =      npr_nc-nOUTnc
 
 
          n1000 = 1 +     iyrrGE/1000
          n100a =     mod(iyrrGE,1000)
          n100  = 1 +     n100a /100
          n10_a =     mod(n100a ,100)
          n10   = 1 +     n10_a /10
          n1    = 1 + mod(n10_a ,10)
          m10   = 1 +     mmarGE/10
          m1    = 1 + mod(mmarGE,10)
          jd10  = 1 +     jdarGE/10
          jd1   = 1 + mod(jdarGE,10)
 
 
C +--Output File Label
C +  -----------------
 
       fnamNC = 'MAR.'
     .        // labnum(n1000) // labnum(n100)
     .        // labnum(  n10) // labnum(  n1)
     .        // labnum(  m10) // labnum(  m1)
     .        // labnum( jd10) // labnum( jd1)
     .        // '.' // explIO
     .        // '.nc    '
 
c #SO     write(6,6000) fnamNC,nOUTnc,npr_nc,kOUTnc
 6000     format(/,'++++++++++++++++++++++++++++++++++++++++++++++',
     .           /,'OUT_nc: Nb of OUTPUT: ',a19,            ':',i4,
     .           /,'                      After  present File:',i4,
     .           /,'                      Per            Day :',i4,
     .           /,'++++++++++++++++++++++++++++++++++++++++++++++')
 
 
C +--Output Title
C +  ------------
 
       tit_NC = 'MAR'
     .        // ' - Exp: ' // explIO
     .        // ' - '
     .        // labnum(n1000) // labnum(n100)
     .        // labnum(  n10) // labnum(  n1)
     .        // labnum(  m10) // labnum(  m1)
     .        // labnum( jd10) // labnum( jd1)
 
 
C +--Create File / Write Constants
C +  -----------------------------
 
       MMXstp = MXdim
C +...To check array bounds... silently
 
C +--Time Variable (hour)
C +  ~~~~~~~~~~~~~~~~~~~~
 
C +...  To define a NetCDF dimension (size, name, unit):
        nDFdim(0)= nOUTnc
        nDFdim(0)= 0
        NAMdim(0)= 'time'
        UNIdim(0)= 'HOURS since 1901-01-15 00:00:00'
 
C +...  Check temporary arrays: large enough ?
        IF (nOUTnc.gt.MMXstp)
     &  STOP '*** OUT_nc - ERROR : MXdim to low ***'
 
              starti = jhurGE + minuGE/60.d0 + jsecGE/3600.d0
C +...        starti : Starting Time (= current time in the day)
 
              starta(1) = (351+(iyrrGE  -1902) *365    ! Nb Days before iyrrGE
     .                        +(iyrrGE  -1901) /  4    ! Nb Leap Years
     .                        + njyrGE(mmarGE)         ! Nb Days before mmarGE
     .                        + njybGE(mmarGE)         ! (including Leap Day)
     .                    *max(0,1-mod(iyrrGE,4))      !
     .                        + jdarGE     -1   )*  24 !
     .                    +jhurGE                      !
     .                  + (minuGE *60 +jsecGE   )/3600.!
 
        DO it = 1,nOUTnc
              timeNC(it)   = starti    + (it-1) * nboucl  *dt / 3600.
C +...                                         nboucl: #iter between output
 
              VALdim(it,0) = starta(1) + (it-1) * nboucl  *dt / 3600.
C +...        VALdim(  ,0) : values of the dimension # 0 (time)
 
C +--Time Variable (date)
C +  ~~~~~~~~~~~~~~~~~~~~
                  dateNC(it) =           timeNC(it)
                  jourNC(it) =  jdarGE + timeNC(it) / 24.
        END DO
                  mois       =  mmarGE
                  mill       =  iyrrGE
        DO it = 1,nOUTnc
          IF     (mois      .eq.2           .AND.
     .        mod(mill,4)   .eq.0           )                     THEN
                  njmo       =  njmoGE(mois) + 1
          ELSE
                  njmo       =  njmoGE(mois)
          END IF
          IF     (jourNC(it).gt.njmo        )                     THEN
            DO iu=it,nOUTnc
                  jourNC(iu) =  jourNC(iu) - njmo
            END DO
                  mois       =  mois + 1
              IF (mois.gt.12)                                     THEN
                  mois       =         1
                  mill       =  mill + 1
              END IF
          END IF
                  moisNC(it) =  mois
                  yearNC(it) =  mill
 
          IF     (dateNC(it).gt.24.-epsi)                         THEN
                  DayLen     =  24.
            DO iu=it,nOUTnc
                  dateNC(iu) = mod(dateNC(iu),DayLen)
            END DO
          END IF
        END DO
 
        DO it = 1,nOUTnc
              dateNC(it) =  dateNC(it)
     .             + 1.d+2 *jourNC(it)
     .             + 1.d+4 *moisNC(it)
        END DO
 
 
C +--Define horizontal spatial dimensions :
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
C +...Check temporary arrays: large enough ?
      IF (    mx .gt.MMXstp.or.my.gt.MMXstp
     &    .or.mzz.gt.MMXstp.or.mw.gt.MMXstp)
     &  STOP '*** OUT_nc - ERROR : MXdim to low ***'
 
C +...To define NetCDF dimensions (size, name, unit):
 
      DO i = 1, mx
        VALdim(i,1) = xxkm(i)
      END DO
      nDFdim(1)= mx
      NAMdim(1)= 'x'
      UNIdim(1)= 'km'
 
      DO j = 1, my
        VALdim(j,2) = yykm(j)
      END DO
      nDFdim(2)= my
      NAMdim(2)= 'y'
      UNIdim(2)= 'km'
 
      do k = 1, mz
        VALdim(k,3) = sigma(k)
      enddo
      nDFdim(3)= mz
      NAMdim(3)= 'level'
      UNIdim(3)= 'sigma_level'
C +... For levels k
 
      do k = 1, mz
        VALdim(k,4) = sigmid(k+1)
      enddo
      nDFdim(4)= mz
      NAMdim(4)= 'level2'
      UNIdim(4)= 'sigma_level'
C +... For levels k+1/2
 
      do k = 1, mw
        VALdim(k,5) = k
      enddo
      nDFdim(5)= mw
      NAMdim(5)= 'sector'
      UNIdim(5)= 'level'
C +... For Surface Sectors
 
C +--Variable's Choice (Table MARvou.dat)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
        OPEN(unit=10,status='unknown',file='MARvou.dat')
 
        itotNC = 0
 980    CONTINUE
          READ (10,'(A120)',end=990) tmpINP
          IF (tmpINP(1:4).eq.'    ')                                THEN
            itotNC = itotNC + 1
            READ (tmpINP,'(4x,5A9,A12,A50)')
     &          nameNC(itotNC)  ,SdimNC(1,itotNC),SdimNC(2,itotNC),
     &          SdimNC(3,itotNC),SdimNC(4,itotNC),
     &          unitNC(itotNC)  ,lnamNC(itotNC)
C +...          nameNC: Name
C +             SdimNC: Names of Selected Dimensions (max.4/variable)
C +             unitNC: Units
C +             lnamNC: Long_name, a description of the variable
 
c #TC       IF (nameNC(itotNC).eq.'qxTC     '.and.nkWri.ge.1)       THEN
c #TC           nameNC(itotNC) =  namTC(1)
c #TC        IF                                  (nkWri.gt.1)       THEN
c #TC           itot = itotNC
c #TC         DO  n=2,nkWri
c #TC           itot = itot    +  1
c #TC           nameNC(itot)   =  namTC(n)
c #TC          DO m=1,4
c #TC           SdimNC(m,itot) = SdimNC(m,itotNC)
c #TC          END DO
c #TC           unitNC(itot)   = unitNC(itotNC)
c #TC           lnamNC(itot)   = lnamNC(itotNC)
c #TC         END DO
c #TC           itotNC         = itot
c #TC        ENDIF
c #TC       ENDIF
 
          ENDIF
        GOTO 980
 990    CONTINUE
 
        CLOSE(unit=10)
 
        NtotNC = itotNC
C +...  NtotNC : Total number of variables writen in NetCDF file.
 
C +--List of NetCDF attributes given to all variables:
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
C +... The "actual_range" is the (min,max)
C +    of all data for each variable:
      NAMrat(1) = 'actual_range'
      NvatNC(1) = 2
 
C +... The "[var]_range" is NOT of attribute type,
C +    it is a true variable containing the (min,max) for
C +    each level, for 4D (space+time) variables only
C +    (automatic handling by UN library;
C +     must be the LAST attribute)
      NAMrat(NattNC) = '[var]_range'
      NvatNC(NattNC) = 2
 
C +--Automatic Generation of the NetCDF File Structure
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
C +     **************
        CALL UNscreate (fnamNC,tit_NC,
     &                  NdimNC, nDFdim, MXdim , NAMdim, UNIdim, VALdim,
     &                  MX_var, NtotNC, nameNC, SdimNC, unitNC, lnamNC,
     &                  NattNC, NAMrat, NvatNC,
     &                  ID__nc)
C +     **************
 
 
C +--Write Time - Constants
C +  ~~~~~~~~~~~~~~~~~~~~~~
        DO j=1,my
        DO i=1,mx
          WKxy1(i,j) =  GElonh(i,j) * 15.
C +...    Conversion: Hour->degrees
 
          WKxy2(i,j) =  GElatr(i,j) / degrad
C +...    Conversion: rad ->degrees
 
          WKxy3(i,j) =  isolSL(i,j)
C +...    Conversion to REAL type (integer not allowed)
 
          WKxy4(i,j) =  isolTV(i,j)
C +...    Conversion to REAL type (integer not allowed)
 
        END DO
        END DO
 
 
        DO l=1,llx
          Soilz(l) =-deptTV(l  )
        END DO
        DO l=  llx+1,mz
          Soilz(l) =-deptTV(llx) -1.e-6*(l-llx)
        END DO
 
 
C +       ************
          CALL UNwrite (ID__nc, 'lon   ', 1  , mx    , my, 1 , WKxy1)
          CALL UNwrite (ID__nc, 'lat   ', 1  , mx    , my, 1 , WKxy2)
          CALL UNwrite (ID__nc, 'MapSC ', 1  , mx    , my, 1 , SFm_DY)
          CALL UNwrite (ID__nc, 'sh    ', 1  , mx    , my, 1 , sh)
          CALL UNwrite (ID__nc, 'isol  ', 1  , mx    , my, 1 , WKxy3)
          CALL UNwrite (ID__nc, 'TEX   ', 1  , mx    , my, 1 , WKxy4)
          CALL UNwrite (ID__nc, 'DepthS ', ipr_nc, mz,  1, 1 , Soilz)
C +       ************
 
 
C +--Snow Mosa?c for OUTPUT
C +  ======================
 
        DO j=1,my
        DO i=1,mx
          ijSNOW(i,j)   =      1 + maskSL(i,j)
        ENDDO
        ENDDO
 
 
C +--Re-Open file if already created.
C +  ================================
 
      ELSE
 
C +     ************
        CALL UNwopen (fnamNC,ID__nc)
C +     ************
 
      END IF
 
 
C +--Write Time-dependent variables:
C +  ===============================
 
C +--UNLIMITED Time Dimension
C +  ------------------------
 
      IF (nDFdim(0).eq.0)                         THEN !
          starta(1) =  (351+(iyrrGE  -1902) *365       ! Nb Days before iyrrGE
     .                     +(iyrrGE  -1901) /  4       ! Nb Leap Years
     .                     + njyrGE(mmarGE)            ! Nb Days before mmarGE
     .                     + njybGE(mmarGE)            ! (including Leap Day)
     .                 *max(0,1-mod(iyrrGE,4))         !
     .                     + jdarGE     -1      )*  24 !
     .                 +jhurGE                         !
     .               + (minuGE *60 +jsecGE      )/3600.!
 
C +     ************
        CALL UNwrite (ID__nc, 'time   ',ipr_nc, 1, 1, 1, starta(1))
C +     ************
 
      END IF
 
 
C +     ************
        CALL UNwrite (ID__nc, 'date   ',ipr_nc, 1, 1, 1, dateNC(ipr_nc))
        CALL UNwrite (ID__nc, 'year   ',ipr_nc, 1, 1, 1, yearNC(ipr_nc))
C +     ************
 
 
C +--Geopotential Height, Saturation Specific Humidity
C +  -------------------------------------------------
 
      do k=1,mz
      do j=1,my
      do i=1,mx
        WKxyz1(i,j,k) = gplvDY(i,j,k)*   grvinv
        WKxyz2(i,j,k) = qvswDY(i,j,k)
c #NH   WKxyz3(i,j,k) = pairNH(i,j,k)*   pstDYn(i,j) * sigma(k)
        WKxyz4(i,j,k) = 0.0
      enddo
      enddo
      enddo
 
 
C +--Surface Humidity and Green Leaf Fraction [%]
C +  --------------------------------------------
 
      DO j=1,my
      DO i=1,mx
            WKxy1(i,j)    = 0.0
            WKxy2(i,j)    = 0.0
            WKxy3(i,j)    = 0.0
      END DO
      END DO
 
      DO n=1,nvx
      DO j=1,my
      DO i=1,mx
            WKxy1(i,j)    =  WKxy1(i,j)
     .                    + eta_TV(i,j,n,1)               *SLsrfl(i,j,n)
            WKxy2(i,j)    =  WKxy2(i,j)
     .                    + glf_TV(i,j,n)   *alaiTV(i,j,n)*SLsrfl(i,j,n)
      END DO
      END DO
      END DO
 
      DO l=1,llx
      DO n=1,nvx
      DO j=1,my
      DO i=1,mx
            WKxyz4(i,j,l) = WKxyz4(i,j,  l)
     .                    + eta_TV(i,j,n,l)               *SLsrfl(i,j,n)
      END DO
      END DO
      END DO
      END DO
 
      DO l=1,llx
      DO j=1,my
      DO i=1,mx
            WKxy3(i,j)    =  WKxy3(i,j)
     .                    + WKxyz4(i,j,l)   *dz_dSV(1-l)
      END DO
      END DO
      END DO
 
C +   ************
      CALL UNwrite (ID__nc, 'uairDY ', ipr_nc, mx, my, mz, uairDY)
      CALL UNwrite (ID__nc, 'vairDY ', ipr_nc, mx, my, mz, vairDY)
      CALL UNwrite (ID__nc, 'wairDY ', ipr_nc, mx, my, mz, wairDY)
      CALL UNwrite (ID__nc, 'tairDY ', ipr_nc, mx, my, mz, tairDY)
c #NH CALL UNwrite (ID__nc, 'wairNH ', ipr_nc, mx, my, mz, wairNH)
c #NH CALL UNwrite (ID__nc, 'pairNH ', ipr_nc, mx, my, mz, WKxyz3)
      CALL UNwrite (ID__nc, 'qvDY   ', ipr_nc, mx, my, mz, qvDY  )
      CALL UNwrite (ID__nc, 'zzDY   ', ipr_nc, mx, my, mz, WKxyz1)
      CALL UNwrite (ID__nc, 'qsatDY ', ipr_nc, mx, my, mz, WKxyz2)
      CALL UNwrite (ID__nc, 'pstar  ', ipr_nc, mx, my, 1 , pstDY )
      CALL UNwrite (ID__nc, 'RadOLR ', ipr_nc, mx, my, 1 , RAdOLR)
      CALL UNwrite (ID__nc, 'RadSol ', ipr_nc, mx, my, 1 , RAdsol)
      CALL UNwrite (ID__nc, 'Rad_IR ', ipr_nc, mx, my, 1 , RAD_ir)
      CALL UNwrite (ID__nc, 'hmelSL ', ipr_nc, mx, my, 1 , hmelSL)
      CALL UNwrite (ID__nc, 'tairSL ', ipr_nc, mx, my, 1 , TairSL)
      CALL UNwrite (ID__nc, 'tsrfSL ', ipr_nc, mx, my, mw, tsrfSL)
c #T2 CALL UNwrite (ID__nc, 'Ta2mSL ', ipr_nc, mx, my, mw, Ta2mSL)
c #T2 CALL UNwrite (ID__nc, 'TminSL ', ipr_nc, mx, my, mw, TminSL)
c #T2 CALL UNwrite (ID__nc, 'TmaxSL ', ipr_nc, mx, my, mw, TmaxSL)
c #T2 CALL UNwrite (ID__nc, 'Ta3mSL ', ipr_nc, mx, my, mw, Ta3mSL)
c #T2 CALL UNwrite (ID__nc, 'V03mSL ', ipr_nc, mx, my, mw, V03mSL)
c #T2 CALL UNwrite (ID__nc, 'V10mSL ', ipr_nc, mx, my, mw, V10mSL)
      CALL UNwrite (ID__nc, 'albxSL ', ipr_nc, mx, my, mw, albxSL)
      CALL UNwrite (ID__nc, 'hsenSL ', ipr_nc, mx, my, 1 , hsenSL)
      CALL UNwrite (ID__nc, 'hlatSL ', ipr_nc, mx, my, 1 , hlatSL)
      CALL UNwrite (ID__nc, 'ect_TE ', ipr_nc, mx, my, mz, ect_TE)
      CALL UNwrite (ID__nc, 'eps_TE ', ipr_nc, mx, my, mz, eps_TE)
      CALL UNwrite (ID__nc, 'TUkvm  ', ipr_nc, mx, my, mz, TUkvm )
      CALL UNwrite (ID__nc, 'TUkvh  ', ipr_nc, mx, my, mz, TUkvh )
      CALL UNwrite (ID__nc, 'SL_z0  ', ipr_nc, mx, my, mw, SL_z0 )
      CALL UNwrite (ID__nc, 'SL_r0  ', ipr_nc, mx, my, mw, SL_r0 )
c #BS CALL UNwrite (ID__nc, 'ustart ', ipr_nc, mx, my, mw, SaltSN)
c #BS CALL UNwrite (ID__nc, 'z0emBS ', ipr_nc, mx, my, mw, Z0emBS)
c #BS CALL UNwrite (ID__nc, 'z0SaBS ', ipr_nc, mx, my, mw, Z0SaBS)
      CALL UNwrite (ID__nc, 'SLsrfl ', ipr_nc, mx, my, mw, SLsrfl)
      CALL UNwrite (ID__nc, 'SLuusl ', ipr_nc, mx, my, mw, SLuusl)
      CALL UNwrite (ID__nc, 'SLutsl ', ipr_nc, mx, my, mw, SLutsl)
      CALL UNwrite (ID__nc, 'SLuqsl ', ipr_nc, mx, my, mw, SLuqsl)
      CALL UNwrite (ID__nc, 'SLussl ', ipr_nc, mx, my, mw, SLussl)
      CALL UNwrite (ID__nc, 'albeSL ', ipr_nc, mx, my, 1 , albeSL)
      CALL UNwrite (ID__nc, 'Clouds ', ipr_nc, mx, my, 1 , cld_SL)
C +   ************
 
C +   ************
      CALL UNwrite (ID__nc, 'HumSol ', ipr_nc, mx, my, 1 , WKxy1)
      CALL UNwrite (ID__nc, 'GreenL ', ipr_nc, mx, my, 1 , WKxy2)
      CALL UNwrite (ID__nc, 'WatSol ', ipr_nc, mx, my, 1 , WKxy3)
      CALL UNwrite (ID__nc, 'EvapoT ', ipr_nc, mx, my, 1 , evapTV)
      CALL UNwrite (ID__nc, 'Draing ', ipr_nc, mx, my, 1 , draiTV)
      CALL UNwrite (ID__nc, 'RunOFF ', ipr_nc, mx, my, 1 , runoTV)
      CALL UNwrite (ID__nc, 'H2OSol ', ipr_nc, mx, my, mz, WKxyz4)
C +   ************
 
 
C +--Dynamical Budget
C +  ----------------
 
c #IZ IF (noZOOM)                                                   THEN
 
c #DY   do k=1,mz
c #DY   do j=1,my
c #DY   do i=1,mx
c #DY     dumy3D(i,j,k) = 0.
c #DY   enddo
c #DY   enddo
c #DY   enddo
 
C +          ******
c #DY   call dynbil(7,1,ipr_nc,ID__nc,0.,0.,0.,0.,dumy3D)
C +          ******
 
c #IZ END IF
 
 
C +--Cloud Microphysics, Mass Flux convective Scheme
C +  -----------------------------------------------
 
      do k=1,mz
      do j=1,my
      do i=1,mx
        WKxyz1(i,j,k) =   qwHY(i,j,k)  +   qiHY(i,j,k)
        WKxyz2(i,j,k) =   qrHY(i,j,k)  +   qsHY(i,j,k)
        WKxyz3(i,j,k) = dqv_CA(i,j,k)*   Lv_H2O/cp
     .             *min(adj_CA(i,j),iun)*86400./dt_Loc
        WKxyz4(i,j,k) =(dpktCA(i,j,k)
     .                *(ptopDY+sigma(k) *pstDY(i,j))**cap)
     .             *min(adj_CA(i,j),iun)*86400./dt_Loc
      enddo
      enddo
      enddo
 
      do j=1,my
      do i=1,mx
             zcd_HY(i,j)  = zcd_HY(i,j) / max(eps9,Hcd_HY(i,j)) / gravit
             Tcd_HY(i,j)  = Tcd_HY(i,j) / max(eps9,Hcd_HY(i,j))
             zsb_HY(i,j)  = zsb_HY(i,j) / max(eps9,Hsb_HY(i,j)) / gravit
             Tsb_HY(i,j)  = Tsb_HY(i,j) / max(eps9,Hsb_HY(i,j))
             Hcd_HY(i,j)  = Hcd_HY(i,j) / max(   1,icntHY     )
             Hsb_HY(i,j)  = Hsb_HY(i,j) / max(   1,icntHY     )
      enddo
      enddo
 
C +   ************
      CALL UNwrite (ID__nc, 'QwQi   ', ipr_nc, mx, my, mz, WKxyz1)
      CALL UNwrite (ID__nc, 'QrQs   ', ipr_nc, mx, my, mz, WKxyz2)
      CALL UNwrite (ID__nc, 'ccniHY ', ipr_nc, mx, my, mz, ccniHY)
      CALL UNwrite (ID__nc, 'qiHY   ', ipr_nc, mx, my, mz, qiHY  )
      CALL UNwrite (ID__nc, 'qwHY   ', ipr_nc, mx, my, mz, qwHY  )
      CALL UNwrite (ID__nc, 'qsHY   ', ipr_nc, mx, my, mz, qsHY  )
      CALL UNwrite (ID__nc, 'qrHY   ', ipr_nc, mx, my, mz, qrHY  )
      CALL UNwrite (ID__nc, 'hlatHY ', ipr_nc, mx, my, mz, hlatHY)
      CALL UNwrite (ID__nc, 'HLCond ', ipr_nc, mx, my, 1 , Hcd_HY)
      CALL UNwrite (ID__nc, 'TaCond ', ipr_nc, mx, my, 1 , Tcd_HY)
      CALL UNwrite (ID__nc, 'z_Cond ', ipr_nc, mx, my, 1 , zcd_HY)
      CALL UNwrite (ID__nc, 'HLSubl ', ipr_nc, mx, my, 1 , Hsb_HY)
      CALL UNwrite (ID__nc, 'TaSubl ', ipr_nc, mx, my, 1 , Tsb_HY)
      CALL UNwrite (ID__nc, 'z_Subl ', ipr_nc, mx, my, 1 , zsb_HY)
      CALL UNwrite (ID__nc, 'rainHY ', ipr_nc, mx, my, 1 , rainHY)
      CALL UNwrite (ID__nc, 'snowHY ', ipr_nc, mx, my, 1 , snowHY)
      CALL UNwrite (ID__nc, 'crysHY ', ipr_nc, mx, my, 1 , crysHY)
C +   ************
 
             icntHY       =     0
      do j=1,my
      do i=1,mx
             Hcd_HY(i,j)  =     0.
             Tcd_HY(i,j)  =     0.
             zcd_HY(i,j)  =     0.
             Hsb_HY(i,j)  =     0.
             Tsb_HY(i,j)  =     0.
             zsb_HY(i,j)  =     0.
      enddo
      enddo
 
C +   ************
      CALL UNwrite (ID__nc, 'CAPE   ', ipr_nc, mx, my, 1 , capeCA)
      CALL UNwrite (ID__nc, 'rainCA ', ipr_nc, mx, my, 1 , rainCA)
      CALL UNwrite (ID__nc, 'snowCA ', ipr_nc, mx, my, 1 , snowCA)
      CALL UNwrite (ID__nc, 'dqv_CA ', ipr_nc, mx, my, mz, WKxyz3)
      CALL UNwrite (ID__nc, 'dpktCA ', ipr_nc, mx, my, mz, WKxyz4)
C +   ************
 
 
C +--Water Budget
C +  ------------
 
C +        ******
c #WB call H2O_WB(-1, 0.,0.,.FALSE.,.TRUE.)
C +        ******
 
c #WB DO j=1,my
c #WB DO i=1,mx
c #WB   WKxy1(i,j) = dq__WB(i,j,1)
c #WB   WKxy2(i,j) = dq__WB(i,j,2)
c #WB   WKxy3(i,j) = dq__WB(i,j,3)
c #WB   WKxy4(i,j) = dq__WB(i,j,4)
c #WB   WKxy5(i,j) = dq__WB(i,j,5)
c #WB   WKxy6(i,j) = dq__WB(i,j,6)
c #WB ENDDO
c #WB ENDDO
 
C +        *******
c #WB CALL UNwrite (ID__nc, 'H2O_ADV', ipr_nc, mx, my,  1 , WKxy1)
c #WB CALL UNwrite (ID__nc, 'H2OdifH', ipr_nc, mx, my,  1 , WKxy2)
c #WB CALL UNwrite (ID__nc, 'H2O_CVA', ipr_nc, mx, my,  1 , WKxy3)
c #WB CALL UNwrite (ID__nc, 'H2OdifV', ipr_nc, mx, my,  1 , WKxy4)
c #WB CALL UNwrite (ID__nc, 'H2O_mic', ipr_nc, mx, my,  1 , WKxy5)
c #WB CALL UNwrite (ID__nc, 'H2Ofltr', ipr_nc, mx, my,  1 , WKxy6)
c #WB CALL UNwrite (ID__nc, 'H2OsrfT', ipr_nc, mx, my,  1 , uq__WB)
c #WB CALL UNwrite (ID__nc, 'H2OsrfA', ipr_nc, mx, my,  1 , wq__WB)
c #WB CALL UNwrite (ID__nc, 'H2Oflux', ipr_nc, mx, my,  1 , upq_WB)
c #WB CALL UNwrite (ID__nc, 'H2Ofluy', ipr_nc, mx, my,  1 , vpq_WB)
c #WB CALL UNwrite (ID__nc, 'H2Omean', ipr_nc, mx, my,  1 , cpq_WB)
c #WB CALL UNwrite (ID__nc, 'Snoflux', ipr_nc, mx, my,  1 , ups_WB)
c #WB CALL UNwrite (ID__nc, 'Snofluy', ipr_nc, mx, my,  1 , vps_WB)
c #WB CALL UNwrite (ID__nc, 'Snomean', ipr_nc, mx, my,  1 , cps_WB)
c #WB CALL UNwrite (ID__nc, 'Vap_Liq', ipr_nc, mx, my, mz , dqwHY)
c #WB CALL UNwrite (ID__nc, 'Vap_Ice', ipr_nc, mx, my, mz , dqiHY)
C +        *******
 
C +        ******
c #WB call H2O_WB(-1, 0.,0.,.TRUE.,.FALSE.)
C +        ******
 
 
C +--Cloud Optical Depth
C +  -------------------
 
c #OD DO j=1,my
c #OD DO i=1,mx
c #OD      WKxy1(i,j)  = 0.
C +...     WKxy1(i,j)  : liquid water path (kg/m2) (droplets)
 
c #OD      WKxy2(i,j)  = 0.
c +...     WKxy2(i,j)  : liquid water path (kg/m2) (crystals)
c #vO ENDDO
c #vO ENDDO
 
c #OD DO k = mzabso+1,mz
c #vO DO j=1,my
c #vO DO i=1,mx
c #OD      rhodzk = (    pstDY(i,j)  * sigma(k)+ptopDY)
c #OD.            / (ra*tairDY(i,j,k)*(1.+.608   *qvDY(i,j,k)))
c #OD.            * (   gpmiDY(i,j,k)-          gpmiDY(i,j,k+1))
C +...     rhodzk : (rho / 1000) * (dz * gravit)
 
c #OD      WKxy1(i,j)  = WKxy1(i,j) + rhodzk * qwHY(i,j,k)
c #OD      WKxy2(i,j)  = WKxy2(i,j) + rhodzk * qiHY(i,j,k)
c #OD END DO
c #vO END DO
c #vO END DO
 
c #vO DO j=1,my
c #vO DO i=1,mx
c #OD      WKxy3(i,j)  = 1.5 * ( WKxy1(i,j) / 20.d-6
c #OD.                         + WKxy2(i,j) / 40.d-6 ) *grvinv
c #OD END DO
c #OD END DO
 
C +   ************
c #OD CALL UNwrite (ID__nc, 'OptDep ', ipr_nc, mx, my, 1 , WKxy3 )
      CALL UNwrite (ID__nc, 'OptDep ', ipr_nc, mx, my, 1 ,RAcdtO )
C +   ************
 
 
C +--Snow Pack
C +  ---------
 
      IF (SnoMod.AND.VSISVAT)                                       THEN
 
        DO k=1,mw
        DO j=1,my
        DO i=1,mx
          WRKxys(i,j,k) = zWE_SN(i,j,k) - zWE0SN(i,j,k)
        END DO
        END DO
        END DO
 
        DO n=1,mw
 
        IF (n.GT.1)                                                 THEN
                     lab__z(5:5) = '_'
                     lab_dz(5:5) = '_'
                     lab_ro(5:5) = '_'
                     lab_wa(5:5) = '_'
                     lab_g1(5:5) = '_'
                     lab_g2(5:5) = '_'
                     lab_Ti(5:5) = '_'
                     write(chn,'(i1)') n
                     lab__z(6:6) = chn
                     lab_dz(6:6) = chn
                     lab_ro(6:6) = chn
                     lab_wa(6:6) = chn
                     lab_g1(6:6) = chn
                     lab_g2(6:6) = chn
                     lab_Ti(6:6) = chn
        ELSE
                     lab__z(5:6) = 'WE'
                     lab_dz(5:6) = 'OW'
                     lab_ro(5:6) = 'OW'
                     lab_wa(5:6) = 'OW'
                     lab_g1(5:6) = 'OW'
                     lab_g2(5:6) = 'OW'
                     lab_Ti(5:6) = 'OW'
        END IF
 
        DO k=1,mz
        DO j=1,my
        DO i=1,mx
          WKxyz1(i,j,k) = 0.
          WKxyz2(i,j,k) = 0.
          WKxyz3(i,j,k) = 0.
          WKxyz4(i,j,k) = 0.
        END DO
        END DO
        END DO
 
        DO k=1,nsnomz
        DO j=1,my
        DO i=1,mx
! #SN     n             =         ijSNOW(i,j)             ! 1: Land  /  2:Ocean
          WKxyz4(i,j,k) = min(max(nssSNo(i,j,n)-k+1,0),1) ! 0: outside SnowPack
        END DO
        END DO
        END DO
 
        DO k=1,nsnomz
        DO j=1,my
        DO i=1,mx
! #vS     n             =         ijSNOW(i,j)             ! 1: Land  /  2:Ocean
          kk            =     max(nssSNo(i,j,n)-k+1,1)    ! 1: 1st lev SnowPack
 
          WKxyz1(i,j,k) = dzsSNo(i,j,n,kk) * WKxyz4(i,j,k)
     .                  +   epsi       * (1.-WKxyz4(i,j,k))
          WKxyz2(i,j,k) = rosSNo(i,j,n,kk) * WKxyz4(i,j,k)
          WKxyz3(i,j,k) = wasSNo(i,j,n,kk) * WKxyz4(i,j,k)
        END DO
        END DO
        END DO
 
C +     ************
! #SN   CALL UNwrite (ID__nc, lab__z , ipr_nc, mx, my, mw, WRKxys)
        CALL UNwrite (ID__nc, lab_dz , ipr_nc, mx, my, mz, WKxyz1)
        CALL UNwrite (ID__nc, lab_ro , ipr_nc, mx, my, mz, WKxyz2)
        CALL UNwrite (ID__nc, lab_wa , ipr_nc, mx, my, mz, WKxyz3)
C +     ************
 
        DO k=1,mz
        DO j=1,my
        DO i=1,mx
          WKxyz1(i,j,k) = 0.
          WKxyz2(i,j,k) = 0.
          WKxyz3(i,j,k) = 0.
          WKxyz4(i,j,k) = 0.
        END DO
        END DO
        END DO
 
        DO k=1,nsnomz
        DO j=1,my
        DO i=1,mx
! #SN     n             =         ijSNOW(i,j)             ! 1: Land  /  2:Ocean
          kk            =     max(nssSNo(i,j,n)-k+1,1)    ! 1: 1st lev SnowPack
 
          WKxyz1(i,j,k) = g1sSNo(i,j,n,kk) * WKxyz4(i,j,k)
          WKxyz2(i,j,k) = g2sSNo(i,j,n,kk) * WKxyz4(i,j,k)
          WKxyz3(i,j,k) = tisSNo(i,j,n,kk) * WKxyz4(i,j,k)
        END DO
        END DO
        END DO
 
C +     ************
        CALL UNwrite (ID__nc, lab_g1 , ipr_nc, mx, my, mz, WKxyz1)
        CALL UNwrite (ID__nc, lab_g2 , ipr_nc, mx, my, mz, WKxyz2)
        CALL UNwrite (ID__nc, lab_Ti , ipr_nc, mx, my, mz, WKxyz3)
C +     ************
 
        END DO
 
      END IF
 
 
C +--Polynya
C +  -------
 
C +   ************
c #PO CALL UNwrite (ID__nc, 'hatmPO ', ipr_nc, mx, my, 1 , hatmPO)
c #PO CALL UNwrite (ID__nc, 'hfraPO ', ipr_nc, mx, my, 1 , hfraPO)
c #PO CALL UNwrite (ID__nc, 'aicePO ', ipr_nc, mx, my, 1 , aicePO)
c #PO CALL UNwrite (ID__nc, 'hicePO ', ipr_nc, mx, my, 1 , hicePO)
c #PO CALL UNwrite (ID__nc, 'hiavPO ', ipr_nc, mx, my, 1 , hiavPO)
C +   ************
 
 
C +--Chemical Species
C +  ----------------
 
c #TC IF      (nkWri.gt.0)                                          THEN
c #TC   DO n=1,nkWri
 
c #TC       labelc       = namTC(      ikTC(n))
c #TC     DO k=1,mz
c #TC     DO j=1,my
c #TC     DO i=1,mx
c #TC       WKxyz1(i,j,k)=  qxTC(i,j,k,ikTC(n))  * 1.000d+09
C +...      Conversion [kg/kg] ------------------> [micro-g/kg]
 
c #CH.                                           * rolvDY(i,j,k)
c #CH.                                           * 0.392D-19 / Unity
C +...      Conversion [mcm]   ------------------> [ppb]        if requested
C +       =>Conversion [kg/kg] ------------------> [micro-g/kg]   eliminated
C +                                            ==> 0.392D-19 = 392D-10/1.d+9
 
c #TC     END DO
c #TC     END DO
c #TC     END DO
 
C +       ************
c #TC     CALL UNwrite (ID__nc,labelc(1:7),ipr_nc, mx, my, mz, WKxyz1)
C +       ************
 
c #TC   END DO
 
c #TC END IF
 
 
C +--That 's all, folks: NetCDF File Closure
C +  =======================================
 
C +   ************
      CALL UNclose (ID__nc)
C +   ************
 
 
C +--Work Arrays Reset
C +  =================
 
      do j=1,my
      do i=1,mx
        WKxy1(i,j)   =0.0
        WKxy2(i,j)   =0.0
        WKxy3(i,j)   =0.0
        WKxy4(i,j)   =0.0
      enddo
      enddo
 
      do k=1,mz
      do j=1,my
      do i=1,mx
        WKxyz1(i,j,k)=0.0
        WKxyz2(i,j,k)=0.0
        WKxyz3(i,j,k)=0.0
        WKxyz4(i,j,k)=0.0
      enddo
      enddo
      enddo
 
      IF   (mmarGE.EQ.12.AND.jdarGE.EQ.31)                          THEN
            end_YR = real(24-jhurGE)*3599.9- dt * nboucl
        IF (end_YR.LT.0.)                                           THEN
                             LastIO =             .TRUE.
        ELSE
                             LastIO =             .FALSE.
        END IF
      ELSE
        IF (mmarGE.EQ. 1.AND.jdarGE.EQ. 1.AND.    jhurGE.EQ.0.AND.
     .      iyrrGE      .GT. iyr0GE      .AND.
     .      iterun.GE.nboucl*nprint)                                THEN
                             LastIO =             .TRUE.
        ELSE
                             LastIO =             .FALSE.
        END IF
      END IF
 
C +   +++++++++++
C +   IF (LastIO) ipr_nc = 0        ! ipr_nc:=0 => NEW MAR*.nc     created
C +   +++++++++++                   !              at the next OUT_nc call
 
      return
      end
