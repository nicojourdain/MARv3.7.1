 
 
      subroutine OUTgks
C +
C +------------------------------------------------------------------------+
C | MAR OUTPUT                                             19-09-2001  MAR |
C |   SubRoutine OUTgks is used to write the main Model Variables          |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   OUTPUT: on asci file si_ddhhmm.LAB (Atmospheric Dynamics, Surface)   |
C |   ^^^^^^               cl_ddhhmm.LAB (Microphysics)                    |
C |                                                                        |
C +------------------------------------------------------------------------+
C +
C +
      IMPLICIT NONE
C +
C +
C +--Global Variables
C +  ================
C +
      include 'MARCTR.inc'
      include 'MARphy.inc'
C +
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
C +
      include 'MARSND.inc'
C +
      include 'MAR_DY.inc'
c #OL include 'MAR_OL.inc'
C +
      include 'MAR_TE.inc'
      include 'MAR_TU.inc'
C +
      include 'MAR_HY.inc'
C +
      include 'MAR_SL.inc'
c #PO include 'MAR_PO.inc'
      include 'MAR_SV.inc'
c #sn include 'MAR_SN.inc'
C +
      include 'MAR_IO.inc'
C +
      external zext
      integer  zext
C +
C +
C +--Local  Variables
C +  ================
C +
      real     fac_sh
      integer  jmmd  ,jm10  ,jh10  ,jh1   ,jd10  ,jd1
C +
C +
C +--Linear Mountain Waves Characteristics
C +  =====================================
C +
c #OL do 10 k=1,mz
c #OL do 10 j=1,my
c #OL do 10 i=1,mx
c #OL  urefOL(i,j,k) = uairDY(i,j,k)
c #OL  uairDY(i,j,k) =(uairDY(i,j,k)-ugeoDY(i,j,k)) *1000.d0
c #OL  trefOL(i,j,k) = tairDY(i,j,k)
c #OL  tairDY(i,j,k) =(tairDY(i,j,k)-tSND  (1,1)  ) *1000.d0
c #OL.                +tSND  (1,1)
c #OL  gplvOL(i,j,k) = gplvDY(i,j,k)
c #OL  gplvDY(i,j,k) =(gplvDY(i,j,k)-gp00OL(i,j,k)) *1000.d0
c #OL.                +gp00OL(i,j,k)
 10   continue
      fac_sh = 1.d0
c #OL fac_sh = 1.d3
C +
C +
C +--Output File Label
C +  =================
C +
      fnam( 1: 3) = 'si_'
      jmmd     = 1 + mod(minuGE,10)
      jm10     = 1 +     minuGE/10
      jh10     = 1 +     jhaMAR/10
      jh1      = 1 + mod(jhaMAR,10)
      jd10     = 1 +     jdaMAR/10
      jd1      = 1 + mod(jdaMAR,10)
      if (jd10.gt.10) then
      fnam( 3: 3) =   '+'
      jd10        =  mod(jd10  ,10)
      end if
      fnam( 4: 4) = labnum(jd10)
      fnam( 5: 5) = labnum(jd1)
      fnam( 6: 6) = labnum(jh10)
      fnam( 7: 7) = labnum(jh1)
      fnam( 8: 8) = labnum(jm10)
      fnam( 9: 9) = labnum(jmmd)
      fnam(10:10) = '.'
      fnam(11:13) = explIO
      fnam(14:16) = '   '
C +
C +
C +--File si_ddhhmm.LAB (Atmospheric Dynamics, Surface)
C +  ==================================================
C +
      open(unit=13,status='unknown',file=fnam)
      rewind    13
C +
      write(13,613)itexpe,iterun,imez,jmez
  613 format(4i12)
      write(13,611) qsolSL
  611 format(l2,'  qsolSL')
      write(13,614)((fac_sh*sh(i,j),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO)
  614 format(10e12.5,' sh')
      write(13,615)GElat0,GElon0,itizGE(imez,jmez),fcorDY(imez,jmez),
     .             0.1d0*pSND(1,1),pstSND,
     .             GEddxx,zs_SL,zn_SL,zl_SL,dt,dx,ptopDY
  615 format(6e12.5)
      write(13,616)( sigma(k),    k=mzw1IO,mzw2IO,izw_IO)
  616 format(8f12.8,'  s')
      write(13,617)(ugeoDY(1,1,k),k=mzw1IO,mzw2IO,izw_IO)
  617 format(8e12.5,'  ug')
      write(13,618)(vgeoDY(1,1,k),k=mzw1IO,mzw2IO,izw_IO)
  618 format(8e12.5,'  vg')
C +
      write(13,619)itexpe*dt,jdaMAR,jhaMAR,
     .             mmarGE,jdarGE,jhurGE,minuGE,jsecGE
  619 format(e12.5,2i12,'  time - jdaMAR - jhaMAR',
     .              i5,'/',i2,i3,'h',i2,'m',i2,'s')
      write(13,620)(((uairDY(i,j,k),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO),
     .                              k=mzw1IO,mzw2IO,izw_IO)
  620 format(10f12.6,'  u')
      write(13,621)(((vairDY(i,j,k),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO),
     .                              k=mzw1IO,mzw2IO,izw_IO)
  621 format(10f12.6,'  v')
      write(13,622)(((wairDY(i,j,k),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO),
     .                              k=mzw1IO,mzw2IO,izw_IO)
  622 format(10e12.5,'  w')
      write(13,623)(((tairDY(i,j,k),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO),
     .                              k=mzw1IO,mzw2IO,izw_IO)
  623 format(10f12.6,'  T')
      write(13,624)(((gplvDY(i,j,k)*grvinv,
     .                              i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO),
     .                              k=mzw1IO,mzw2IO,izw_IO)
  624 format(10f12.4,'  GPlev')
      write(13,625)(((  qvDY(i,j,k),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO),
     .                              k=mzw1IO,mzw2IO,izw_IO)
  625 format(10e12.5,'  qv  ')
      write(13,627) ((hmelSL(i,j  ),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO)
  627 format(10e12.5,'  hmel')
      write(13,628) (( pstDY(i,j  ),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO)
  628 format(10f12.6,'  pstar')
      write(13,629) ((tsrfSL(i,j,1),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO)
  629 format(10f12.6,'  tsrfSL')
      write(13,630)((( TUkvm(i,j,k),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO),
     .                              k=mzw1IO,mzw2IO,izw_IO)
  630 format(10f12.6,'  TUkvm')
      write(13,631)((( TUkvh(i,j,k),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO),
     .                              k=mzw1IO,mzw2IO,izw_IO)
  631 format(10f12.6,'  TUkvh')
      write(13,632) ((   SLuus(i,j),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO)
  632 format(10f12.6,'  SLuus')
      write(13,633) ((   SLuts(i,j),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO)
  633 format(10f12.6,'  SLuts')
      write(13,634) ((   SLuqs(i,j),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO)
  634 format(10f12.9,'  SLuqs')
C +
      write(13,636)(((ect_TE(i,j,k),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO),
     .                              k=mzw1IO,mzw2IO,izw_IO)
  636 format(10e12.5,'  ect_TE')
C +
c #PO write(13,6370)((hatmPO(i,j),  i=mxw1IO,mxw2IO,ixw_IO),
c #PO.                              j=myw1IO,myw2IO,iyw_IO)
 6370 format(10f12.2,' H(atm) ')
c #PO write(13,637) ((hfraPO(i,j),  i=mxw1IO,mxw2IO,ixw_IO),
c #PO.                              j=myw1IO,myw2IO,iyw_IO)
  637 format(10f12.6,' Frazil')
c #PO write(13,6380)((SLsrfl(i,j,2),i=mxw1IO,mxw2IO,ixw_IO),
c #PO.                              j=myw1IO,myw2IO,iyw_IO)
 6380 format(10f12.6,' Srf Wa ')
c #PO write(13,6381)((aicePO(i,j),  i=mxw1IO,mxw2IO,ixw_IO),
c #PO.                              j=myw1IO,myw2IO,iyw_IO)
 6381 format(10f12.6,' A(ice) ')
c #PO write(13,6382)((hicePO(i,j),  i=mxw1IO,mxw2IO,ixw_IO),
c #PO.                              j=myw1IO,myw2IO,iyw_IO)
 6382 format(10f12.6,' h(ice) ')
c #PO write(13,6383)((hiavPO(i,j),  i=mxw1IO,mxw2IO,ixw_IO),
c #PO.                              j=myw1IO,myw2IO,iyw_IO)
 6383 format(10f12.6,' Hi avr.')
C +
      close(unit=13)
C +
C +
C +--File cl_ddhhmm.LAB (Microphysics)
C +  =================================
C +
      fnam( 1: 2) = 'cl'
      open(unit=14,status='unknown',file=fnam)
      rewind    14
C +
      write(14,641)-zext(turnHY),  itexpe,  jdarGE,  jhurGE
  641 format(4i12,    '  turnHY  - itexpe - jdarGE - jhurGE')
      write(14,642)(((  qwHY(i,j,k),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO),
     .                              k=mzw1IO,mzw2IO,izw_IO)
  642 format(10e12.5,'  qw')
      write(14,643)(((  qiHY(i,j,k),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO),
     .                              k=mzw1IO,mzw2IO,izw_IO)
  643 format(10e12.5,'  qi')
      write(14,644)(((  qrHY(i,j,k),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO),
     .                              k=mzw1IO,mzw2IO,izw_IO)
  644 format(10e12.5,'  qr')
      write(14,645)(((  qsHY(i,j,k),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO),
     .                              k=mzw1IO,mzw2IO,izw_IO)
  645 format(10e12.5,'  qs')
      write(14,646) ((rainHY(i,j  ),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO)
  646 format(10e12.5,'  rain')
      write(14,647) ((snowHY(i,j  ),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO)
  647 format(10e12.5,'  snow')
      write(14,648)(((hlatHY(i,j,k),i=mxw1IO,mxw2IO,ixw_IO),
     .                              j=myw1IO,myw2IO,iyw_IO),
     .                              k=mzw1IO,mzw2IO,izw_IO)
  648 format(10e12.5,'  hlat')
C +
      close(unit=14)
C +
C +
C +--RESET
C +  =====
C +
c #OL do 20 k=1,mz
c #OL do 20 j=1,my
c #OL do 20 i=1,mx
c #OL  uairDY(i,j,k) = urefOL(i,j,k)
c #OL  tairDY(i,j,k) = trefOL(i,j,k)
c #OL  gplvDY(i,j,k) = gplvOL(i,j,k)
 20   continue
C +
      return
      end
