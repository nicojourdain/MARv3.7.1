#!/bin/ksh 
#
# v3.1 - 21.06.2006 (12.02.2004)
#                                                                               
# NAME                                                                          
#                                                                               
#   Compile.x - Compile NESTOR, MAR or MAPOST on a machine
#                                                                               
# SYNOPSIS                                                                      
#                                                                               
#   Compile.x  code  [compilor set]
#                                                                              
# OPERANDS                                                                      
#                                                                               
#   code            name of the code to compile                    [NST/MAR/PMP]
#                   (e.g.: './Compile.x NST' compiles NESTOR)
# [ compilor set    integer number refeering to an appropriate machine
#                   listed in the table hereunder. 
#                   (e.g.: './Compile.x MAR 3' compiles MAR for the machine 3) ]
#                                                                               
# DESCRIPTION                                                                   
#                                                                               
#   The Compile.x script compiles NESTOR, MAR or MAPOST. It must be located in
#   the directory with the sources to compile.                  
#   You can set your own preferences in the 'User's paremeters' area (only!).
#
#   The 'compilor set' argument is optional. If specified, it replaces the 
#   compilor set which is used by default by the Compile.x script and which is
#   defined with the 'set_foc' variable hereunder. Especially useful for the
#   distinction between rhodes and uqbar on the IDRIS cluster.
#                                                                               


#*******************************************************************************
# User's parameters                                                             
#                                                                               

#general

. INI.ctr
. FUNCTIONS

#computation precision

MAR_DP=n          #MAR, NESTOR (NST), MAPOST (PMP) in simple precision [n]
NST_DP=n          #                                or double precision [y]
PMP_DP=n          #MAPOST always in simple precision

#compilor set

set_foc=6         #compilor set (foc=FOrtran Compilor)
                  #     (see further for detailed compiling and linking options)

                  # set_foc  machine                           Fortran compilor
                  #                                                           
                  # 2        rhodes     IRIX64 6.5             f90
                  # 2b       uqbar      NEC SX-5 13.1          sxf90
                  # 3        -          SuSE Linux 8.2         f77            
                  # 4        -          SuSE Linux 8.2         ifc (7.1)
                  # 5        -          Mandriva Linux         ifc
                  # 6        eole       Dell Xeon EM64T        ifort
                  # 7        wilfran    Mandrake 10.1          ifort
                  # 8        nic2       Linux SLES9 x86_64     ifort

tmp=`hostname -i | grep 10.`
[ ${#tmp} -gt 0 ] && cluster=nic && set_foc=8

tmp=`hostname -i | grep 139.165.29`
[ ${#tmp} -gt 0 ] && set_foc=6   && cluster=linux

#MAR (only) compilation specifications

    #The next XXvariables are adapted when the script 'CODE' calls 'Compile.x',
    #but you can set them by hand if you want to use manually this script.

runnam=XXrunnam   #run name (MAR executable will be MAR_$runnam.exe) (e.g. a04)
domain=XXdomain   #domain name                                       (e.g. EUa)
CODEdir=XXCODEdir #code directory for the above-specified run
STKcod=XXSTKcod   #directory on the stock where to save the code + executable
                  #(usually: [stock:]MAR/EUa/a04/code)
fsplit=XXfsplit   #[y|n]   y: MAR is fsplitted AND must be compiled so
VE=XXVE           #[T|F]   T: vectorized code,  F: scalar code                 
RT=XXRT           #[L|M|E] LMDz/MAR/ECMWF radiative transfer scheme
                  #        (existing radCEP_*.d*_* => no compilation)
CA=XXCA           #[-|b|E] no/Bechtold/Emanuel convective adjustment           
                  #        (existing cvamnh.o      => no compilation)

case $cluster in (nic2)  fop="P4" ;; #options for compiling radCEP.d
                 (linux) fop="P4" ;;
                 (idris) fop="O3" ;;
                 (*) DAMNED "radCEP optimisation options undefined!" ;;
esac

#
# End of User's parameters
#*******************************************************************************
# DO NOT MODIFY anything beyond this line if you aren't sure of what to do.
# Any further trouble would be your own responsability... :o)
#*******************************************************************************




#===============================================================================
# Compilors set
#===============================================================================

[ ${#2} -ne 0 ] && set_foc=$2

  #foc = Fortran compilor              
  #opt = compilation and linking options 
  #lnk = linking options                 
  #netcdf = NetCDF library               

case $set_foc in
#(1)  #DEC OSF1 V5.1 alpha (decci)
#  inf="DEC OSF1"
#  foc="f90"
#  opt="-O4 -tune ev67 -arch ev67"
#  lnk="/usr/local/netcdf/lib/libnetcdf.a"
#  netcdf="/opt/netcdf/include/netcdf.inc"
#;;
(2)  #IRIX64 6.5 (rhodes)
  inf="IRIX64"
  foc="f90"
  opt="-static -i8 -r8 -OPT:Olimit=6142 -O2"
  lnk="-L/usr/local/pub/lib64/netcdf-3.4 -lnetcdf_i8_r8"
  netcdf="/usr/local/pub/include/netcdf-3.4/netcdf.inc"
;;
(2b) #NEC SX-5 13.1 (uqbar)
  inf="NEC SX-5"
  foc="sxf90"
  opt1="explicitly written with the command"
  opt2="explicitly written with the command"
  opt3="explicitly written with the command"
  lnk="explicitly written with the command"
  #zext.f sbcnew.f MAR___.f
  #opt1='-ew -P static -C vopt -Wf"-init stack=nan -pvctl fullmsg -O infomsg -L fmtlist -Ns -msg nb"'
  #cvamnh.f90
  #opt2='-ew -P static -C vsafe -Wf"-init stack=nan -pvctl fullmsg -O infomsg -L fmtlist -Ns -msg nb"'
  #linking
  #opt3='-ew -P static -C vopt'
  #lnk='-L $HOME/MAR/MARdir/libMAR -lUN $NETCDF_I8R8 -Wl"-f zero"'
  netcdf="not used"
;;
(3)  #SuSE Linux 8.2
  inf="SuSE Linux"
  foc="f77"
  opt="-w -O3 -finit-local-zero"
  lnk="-lnetcdf"
  netcdf="/usr/include/netcdf.inc"
;;
(4)  #SuSE Linux 8.2 (ifc 7.1)
  inf="SuSE Linux"
  foc="ifc"
  opt="-w -zero -O3" # -tpp7 -axW -xW do not work with PRCout
  lnk="-lnetcdfifc -lf2c_ifc"
  netcdf="/usr/include/netcdf.inc"
;;
(5)  #Mandriva Linux (ifc 7.1)
  inf="Mandriva Linux"
  foc="ifc"
  opt="-w -zero -O3" # -tpp7 -axW -xW do not work with PRCout
  lnk="-lnetcdf_ifc"
  netcdf="/usr/include/netcdf.inc"
;;
(6)  #OpenSuse Linux 11.1 (ifort)
  inf="OpenSuse Linux 11.3 (ifort)"
  foc="ifort"
  opt="-w -zero -vec_report0 -static -O3 -axP -xP -mp1 -ipo"         # ifort 9.1
 #opt="-w -zero -vec_report0 -static -O3 -xSSSE3 -axSSSE3 -mp1 -ip"  # ifort 12.0
  lnk="-lnetcdf_ifort"
  netcdf="/usr/include/netcdf.inc"
source /opt/intel/iforte-9.1/bin/ifortvars.sh ; source /opt/intel/icce-9.1/bin/iccvars.sh ; source /opt/intel/idbe-9.1/bin/idbvars.sh
;;
(7)  #Mandrake 10.1 (ifc 8.1)
  inf="Mandrake 10.1"
  foc="ifort"
  opt="-w -zero -O3" # -tpp7 -axW -xW #don't work with PRCout
  lnk="-L/home/moufouma/netcdf-3.6.0-p1/src/libsrc -lnetcdf"
  netcdf="/usr/include/netcdf.inc"
;;
(8)  #NIC2 SLES 9 x86_64
  inf="NIC2 SLES 9 x86_64 (iforte 9.1)"
  foc="ifort"
  opt="-w -zero -vec_report0 -static -O3 -axW -xW -ipo"
  lnk="/u/software/netcdf-3.6.2/intel/lib/libnetcdf.a"
  netcdf="/u/software/netcdf-3.6.2/intel/include/netcdf.inc"
(*)  #UNDEFINED!!! -> exit
  echo '@&?%! '"compilor error: set_foc undefined [$set_foc]" && exit
;;
esac

echo $inf     > Compile.info
echo $foc    >> Compile.info
echo $opt    >> Compile.info
echo $lnk    >> Compile.info
echo $netcdf >> Compile.info

#===============================================================================
# HELP
#===============================================================================

[ ${#1} -eq 0 ] && head -31 $0 && exit 99
[ $1 != "NST" -a $1 != "MAR" -a $1 != "PMP" ] && head -31 $0 && exit 99


#===============================================================================
# 1 - INITIALISATION
#===============================================================================

#script argument                        

usage="\nusage: Compile.x  code  [compilor set]"

[ ${#1} -eq 0 ] && DAMNED "1st argument missing [code]\n$usage"

#main script parameters values          

case $cluster in (nic2|linux|idris) echo ;;
                 (*) echo '@&?%! '"value error: cluster [$cluster]" && exit ;;
esac

Ferror () { echo "  ${1}${blank[35-${#1}]}[KO]" ; }
Fgoon  () { echo "  ${1}${blank[35-${#1}]}[OK]" ; }

#===============================================================================
#  COMPILATION OF NESTOR, MAR OR MAPOST
#===============================================================================

case $1 in

#-------------------------------------------------------------------------------
#NESTOR
#-------------------------------------------------------------------------------
(NST)

tmp=`grep "C |  NESTOR" NESTOR.f | awk '{print($4",",$7,$8,$9)}'`
                                                                 #NESTOR version
echo "NESTOR code version     : $tmp"
echo "Informatic platform     : $inf"
echo "Fortran compilor        : $foc"
echo "Options, compil. & link.: $opt"
echo "Options,           link.: $lnk"
echo

#librairy NetCDF                        

echo "      INCLUDE '$netcdf'" > NetCDF.inc

#simple/double precision                

[ $NST_DP = "y" ] && ln -s MARout.f.DP MARout.f

#COMPILATION                            

rm -f  *.o

for file in *.f ; do
  $foc  -c $opt  $file
  if [ $? -ne 0 ] ; then
    Ferror "$file"
    DAMNED "compilation error"
  else
    Fgoon  "$file"
  fi
done

#LINKING                                

rm -f               ../NESTOR.exe
$foc  $opt  *.o  -o ../NESTOR.exe     $lnk

#END                                    

echo
if [ -f ../NESTOR.exe -o -f ../NESTOR_DP.exe ] ; then
  Fgoon  "NESTOR compilation"
else
  Ferror "NESTOR compilation"
fi

#END OF NESTOR COMPILATION-LINKING
;;


#-------------------------------------------------------------------------------
#MAPOST
#-------------------------------------------------------------------------------
(PMP)

tmp=`grep "C | MAPOST" MAPOST.f | awk '{print($7,$8)}'`
                                                                 #MAPOST version

echo "MAPOST code version     : $tmp"
echo "Informatic platform     : $inf"
echo "Fortran compilor        : $foc"
echo "Options, compil. & link.: $opt"
echo "Options,           link.: $lnk"

#librairy NetCDF                        

echo "      INCLUDE '$netcdf'" > NetCDF.inc

#COMPILATION                            

rm -f  *.o

for file in *.f ; do
  $foc -c $opt $file
  if [ $? -ne 0 ] ; then
    Ferror "$file"
    DAMNED "compilation error"
  else 
    Fgoon  "$file"
  fi
done

#LINKING

rm -f               ../MAPOST.exe
$foc  $opt  *.o  -o ../MAPOST.exe  $lnk

#END

echo
if [ -f ../MAPOST.exe ] ; then
  Fgoon "MAPOST compilation"
else
  Ferror "MAPOST compilation"
fi
echo

#END OF MAPOST COMPILATION-LINKING
;;


#-------------------------------------------------------------------------------
#MAR
#-------------------------------------------------------------------------------
(MAR)

[ ! -f $CODEdir/MAR___.f ] && CODEdir=$PWD

tmp=`grep "C | Version" $CODEdir/MAR___.f |awk '{print($4,$5,$6)}'` #MAR version

echo "MAR code version        : $tmp"
echo "Informatic platform     : $inf"
echo "Fortran compilator      : $foc (`type $foc`)"
if [ $cluster != "idris" ] ; then
echo "Options, compil. & link.: $opt"
else
echo "Options, compil. & link.: $opt1"
echo "Options, compil. & link.: $opt2"
echo "Options, compil. & link.: $opt3"
fi
echo "Options,           link.: $lnk"

#
#compile and link
#

echo "      INCLUDE '$netcdf'" > NetCDF.inc

case $cluster in (nic2|linux)
#
#NIC2, LINUX
#---------------------------------------

echo ; REMARK "compilation" ; echo

#compilation: radCEP.d

if [ $RT = "E" ] ; then

  radmz=`grep "mz=" MARdim.inc | cut -d',' -f1 | cut -d'=' -f2`
  radmz=0`echo $radmz` 

  if [ ! -d radCEP.d/radCEP_${radmz}.d${foc}_$fop ] ; then

    echo "It takes around 2 hours to compile the ECMWF radiative scheme..."

    [ $VE = "T" ] && radvec=256 || radvec=1

    cd $CODEdir/radCEP.d
    time sh radCEP.sh $radvec $radmz $foc $fop  #radCEP_${radmz}.d${foc}_$fop created
                                                #in $CODEdir/radCEP.d/     
    cd $CODEdir

  fi

  radCEPo="radCEP.d/radCEP_${radmz}.d${foc}_$fop/*.o"

  Fgoon  "radCEP.d/radCEP_${radmz}.d${foc}_$fop"

  cp -fp  radCEP.d/radCEP_${radmz}.d${foc}_$fop/radCEP.inc  $CODEdir/

  time $foc  $opt  -c PHYrad_CEP.f
  if [ $? -ne 0 ] ; then
    Ferror "PHYrad_CEP.f"
    DAMNED "compilation error"
  else
    Fgoon "PHYrad_CEP.f"
  fi

fi

#compilation: PHYrad_LMD.f

if [ $RT = "L" ] ; then

  time $foc  $opt  -c  PHYrad_LMD.f
  if [ $? -ne 0 ] ; then
    Ferror "PHYrad_LMD.f"
    DAMNED "compilation error"
  else
    Fgoon "PHYrad_LMD.f"
  fi
    
fi


#compilation: *.f

if [ $fsplit = "y" ] ; then

  for file in *.f ; do
  if [ $file != "PHYrad_LMD.f" -o $file != "PHYrad_CEP.f" ] ; then
    time $foc  $opt  -c $file
    if [ $? -ne 0 ] ; then
      Ferror "$file"
      DAMNED "compilation error"
    else
      Fgoon  "$file"
    fi
  fi
  done

fi

#compilation: zext.f libUN.f MAR___.f SBCnew.f

if [ $fsplit = "n" ] ; then

  for file in zext.f libUN.f MAR___.f SBCnew.f ; do
    time $foc  $opt  -c $file
    if [ $? -ne 0 ] ; then
      Ferror "$file"
      DAMNED "compilation error"
    else
      Fgoon  "$file"
    fi
  done

  if [ -f H2O_WB.f ] ; then

    file="H2O_WB.f"
    time $foc  $opt  -c $file
    if [ $? -ne 0 ] ; then
      Ferror "$file"
      DAMNED "compilation error"
    else
      Fgoon  "$file"
    fi

  fi

fi

#compilation: cvamnh.f90
  
[ $MAR_DP = "T" ] && tmp="-r8" || tmp=""
file="cvamnh.f90"
if [ $CA = "b" ] ; then
  if [ ! -f cvamnh.o ] ; then
    time $foc  $opt  $tmp  -c $file  -o cvamnh.o
    if [ $? -ne 0 ] ; then
      Ferror "$file"
      DAMNED "compilation error"
    else
      Fgoon  "$file"
    fi
  else
    Fgoon  "${file/f90/o} kept"
  fi
fi

#linking                              

echo ; REMARK "linking"

time $foc  $opt  *.o ${radCEPo}  -o MAR_$runnam.exe  $lnk
tmp=$?

;;
(idris)
#
#IDRIS
#---------------------------------------

cd $CODEdir
if [ -f cvamnh.o ] ; then
  mv  cvamnh.o   cvamnh.ok
  rm -f  *.o  *.L
  mv  cvamnh.ok  cvamnh.o
else
  rm -f  *.o  *.L
fi

tmp1=0 ; tmp=0

echo ; REMARK "compilation" ; echo

#compilation: zext.f                          

set -x
sxf90 -c -ew -P static -C vopt -Wf"-init stack=nan -pvctl fullmsg -O infomsg -L fmtlist -Ns -msg nb" zext.f
tmp1=$? ; set +x ; tmp=$(( $tmp1 + $tmp ))

#compilation: H2O_WB.f

if [ -f H2O_WB.f ] ; then

set -x
sxf90 -c -ew -P static -C vopt -Wf"-init stack=nan -pvctl fullmsg -O infomsg -L fmtlist -Ns -msg nb" H2O_WB.f
tmp1=$? ; set +x ; tmp=$(( $tmp1 + $tmp ))

fi

#compilation: SBCnew.f

set -x
sxf90 -c -ew -P static -C vopt -Wf"-init stack=nan -pvctl fullmsg -O infomsg -L fmtlist -Ns -msg nb" SBCnew.f
tmp1=$? ; set +x ; tmp=$(( $tmp1 + $tmp ))

#compilation: cvamnh.f90

set -x
if [ ! -f cvamnh.o ] ; then
sxf90 -c -ew -P static -C vsafe -Wf"-init stack=nan -pvctl fullmsg -O infomsg -L fmtlist -Ns -msg nb" cvamnh.f90
tmp1=$? ; set +x ; tmp=$(( $tmp1 + $tmp ))
else
echo "cvamnh.o kept"
fi

#compilation: PHYrad_LMD.f MAR___.f

if [ $RT = "L" ] ; then

  set -x
  sxf90 -c -ew -P static -C vopt -Wf"-init stack=nan -pvctl fullmsg -O infomsg -L fmtlist -Ns -msg nb" PHYrad_LMD.f
  tmp1=$? ; set +x ; tmp=$(( $tmp1 + $tmp ))

  set -x
  sxf90 -c -ew -P static -C vopt -Wf"-init stack=nan -pvctl fullmsg -O infomsg -L fmtlist -Ns -msg nb" MAR___.f
  tmp1=$? ; set +x ; tmp=$(( $tmp1 + $tmp ))

  #linking                              

  echo ; REMARK "linking"

  [ -f H2O_WB.f ] && h2o = H2O_WB.o
  set -x
  sxf90 -ew -P static -C vopt -o MAR_$runnam.exe MAR___.o zext.o PHYrad_LMD.o cvamnh.o $h2o SBCnew.o -L $HOME/MAR/MARdir/libMAR -lUN $NETCDF_I8R8 -Wl"-f zero"
  tmp1=$? ; set +x ; tmp=$(( $tmp1 + $tmp ))

fi

#compilation: radCEP.d MAR___.f

if [ $RT = "E" ] ; then

  radmz=`grep "mz=" MARdim.inc | cut -d',' -f1 | cut -d'=' -f2`
  radmz=0`echo $radmz` 

  if [ ! -d radCEP.d/radCEP_${radmz}.d${foc}_$fop ] ; then

    echo "It takes around 2 hours to compile the ECMWF radiative scheme..."

    [ $VE = "T" ] && radvec=256 || radvec=1

    cd $CODEdir/radCEP.d
    sh radCEP.sh $radvec $radmz $foc $fop  #radCEP_${radmz}.d${foc}_$fop created
                                           #in $CODEdir/radCEP.d/     
    cd $CODEdir

  fi

  radCEPo="radCEP.d/radCEP_${radmz}.d${foc}_$fop/*.o"

  Fgoon  "radCEP.d/radCEP_${radmz}.d${foc}_$fop"

  cp -fp  radCEP.d/radCEP_${radmz}.d${foc}_$fop/radCEP.inc  $CODEdir/

  set -x
  sxf90 -c -ew -P static -C vsafe -Wf"-init stack=nan -pvctl fullmsg -O infomsg -L fmtlist -Ns -msg nb" PHYrad_CEP.f
  tmp1=$? ; set +x ; tmp=$(( $tmp1 + $tmp ))

  set -x
  sxf90 -c -ew -P static -C vopt -Wf"-init stack=nan -pvctl fullmsg -O infomsg -L fmtlist -Ns -msg nb" MAR___.f
  tmp1=$? ; set +x ; tmp=$(( $tmp1 + $tmp ))


  #linking
  echo ; REMARK "linking"

  [ -f H2O_WB.f ] && h2o=H2O_WB.o
  set -x
  sxf90 -ew -P static -C vopt -o MAR_$runnam.exe MAR___.o zext.o PHYrad_CEP.o radCEP.dO3/*.o cvamnh.o $h2o SBCnew.o -L $HOME/MAR/MARdir/libMAR -lUN $NETCDF_I8R8 -Wl"-f zero"
  tmp1=$? ; set +x ; tmp=$(( $tmp1 + $tmp ))

fi

;;

esac

#
# Backup: code and executable if successful compilation
#

echo

if [ $tmp -eq 0 -a -f MAR_$runnam.exe ] ; then
  Fgoon  "MAR compilation"
  end=yes
else
  Ferror "MAR compilation"
  end=no
fi

echo

if [ $end = "yes" ] ; then

  #put the MAR executable on stock       

  REMARK "MAR_$runnam.exe stored on stock" ; echo

  smput  "$CODEdir"  "MAR_$runnam.exe"  "$STKcod"
  if [ $? -eq 0 ] ; then
    rm -f $CODEdir/MAR_$runnam.exe
    tmp=0
  else
    tmp=1
  fi
  echo

  #save the code directory on stock also 

  REMARK "source code for $runnam stored on stock" ; echo

  cd $CODEdir
  if [ $tmp -eq 0 ] ; then
    rm -f   *.o  *.L
    rm -rf  $CODEdir/src
    tarX   "$CODEdir" "*" "compilink"
    gzip compilink.tar
    smput  "$CODEdir"     "compilink.tar.gz"  "$STKcod"
    cd      $CODEdir
    mv      $CODEdir/Compile.*  ..
    rm -rf  $CODEdir/*
    mv     ../Compile.*  $CODEdir/
  else
    echo "an error occurred, nothing saved on the stock"
  fi
  echo

fi

#END OF MAR COMPILATION-LINKING
;; esac


#-------------------------------------------------------------------------------
#  X - END OF TROUBLES :o)                                                      
#-------------------------------------------------------------------------------
