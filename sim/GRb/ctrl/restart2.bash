DOM=b

MM=01

for dir in ${DOM}* ; do

 cd $dir

  yy=${dir:1:2} ; yy=${yy#0}

  yy2=$(( $yy + 4 )) ; [ $yy2 -gt 99 ] && yy2=$(( $yy2 - 100 )) && yy2="0$yy2"

                     yy3=$(( $yy2 + 1900 ))
  [ $yy2 -le 50 ] && yy3=$(( $yy2 + 2000 ))

  [ $yy2 -le 9  ] && yy2="0${yy2#0}" 

 if [ $yy3 -ge 1958 ] && [ $yy3 -le 1975 ] ; then
   echo $dir $yy3 $MM a
   rm -f /u/xfettwei/MAR/sim/GR$DOM/run/$dir/MAR_GR$dir.*cmd
   MAR -again ../../ctrl/$dir/MAR.ctr.* $dir $yy3 $MM a
   qsub  /u/xfettwei/MAR/sim/GR$DOM/run/$dir/MAR_GR$dir.*.01.01-15.cmd
 fi
 cd ..
done
