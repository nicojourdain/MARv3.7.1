#!/bin/bash

#set -x

. ~/MAR/bin/INI.ctr

dom=j

for yy in `seq 0 8` ; do
                  dir=${dom}0$yy  
 [ $yy -gt 9 ] && dir=${dom}$yy
 if [ -d $dir ] ; then
  cd $dir
  file=`ssh $ustock@$stock ls -1t $STKmar/GR$dom/input/MARsim/$dir/*/*.gz | head -1`
  file=${file##*MARsim_}
  dir2=${file:0:3}
  YY2=${file:4:4}
  MM2=${file:9:2}
  DD2=${file:12:2}
  echo $dir2 $YY2 $MM $DD2

  [ $DD2 -eq "01" ] && DD1=a
  [ $DD2 -eq "11" ] && DD1=c
  [ $DD2 -eq "16" ] && DD1=b
  [ $DD2 -eq "21" ] && DD1=d

  if [ "$dir" == "$dir2" ] ; then
   MAR -again ./MAR.ctr* $dir $YY2 $MM2 $DD1
   
  fi

  cd ..
 fi
done
