for M in `seq 1 12` ; do
 
                 MM=$M
 [ $M -le 9 ] && MM="0$M"

 echo "1979 $MM 01 60 50" > MARdt.ctr
 MAR -again ./MAR.ctr* a76 1979 $MM c
 qsub ~/MAR/sim/GRa/run/a76/MAR_GRa76.79.$MM.01*
 sleep 360

 echo "1979 $MM 11 60 50" > MARdt.ctr
 MAR -again ./MAR.ctr* a76 1979 $MM d
 qsub ~/MAR/sim/GRa/run/a76/MAR_GRa76.79.$MM.11*
 sleep 360

 echo "1979 $MM 21 60 50" > MARdt.ctr
 MAR -again ./MAR.ctr* a76 1979 $MM e
 qsub ~/MAR/sim/GRa/run/a76/MAR_GRa76.79.$MM.21*
 sleep 360
done


