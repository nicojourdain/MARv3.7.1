#!/bin/bash
#PBS -N a79831021e
#PBS -o /climato_tmp1/fettweis/MAR/msg/MAR_GRa79.83.10.21-31.eo
#PBS -l nodes=1:ppn=8
#PBS -m a
#PBS -r y
#PBS -M xavier.fettweis@ulg.ac.be
#PBS -j oe
. /climato_tmp1/fettweis/MAR/bin/INI.ctr
. /climato_tmp1/fettweis/MAR/bin/FUNCTIONS
  export         NBR_CPU=8
  export OMP_NUM_THREADS=$NBR_CPU

#-------------------------------------------------------------------------------
# 1 - Initialisation
#-------------------------------------------------------------------------------
echo ; echo "---- 1 ---- Initialisation" ; echo


#variables definition

host=`hostname -s`

#job messages file in /climato_tmp1/fettweis/MAR/msg

if [ ! -f /climato_tmp1/fettweis/MAR/sim/GRa/run/a79/crash83102131 ] ; then
  rm -f /climato_tmp1/fettweis/MAR/msg/MAR_GRa79.83.10.21-31.${host%${host#???}}*
else
  for fil in `ls /climato_tmp1/fettweis/MAR/msg/MAR_GRa79.83.10.21-31*` ; do
    mv -f $fil ${fil}-crash
  done
fi
msg=/climato_tmp1/fettweis/MAR/msg/MAR_GRa79.83.10.21-31.$host
touch $msg

#-------------------------------------------------------------------------------
# 2 - Job informations
#-------------------------------------------------------------------------------
echo ; echo "---- 2 ---- Job informations" ; echo

[ -d /scratch/fettweis/MARrun ] || mkdir /scratch/fettweis/MARrun

echo "Begin (initial) : 21/10/1983 (01/09/1979)"
echo "End   (final  ) : 31/10/1983 (31/12/1983)"
echo
echo "Domain          : GRa (/climato_tmp1/fettweis/MAR/sim/GRa)"
echo
echo "Date            : `date`"
echo "Host            : $host"
echo "Work directory  : /scratch/fettweis/MARrun"
echo "Messages in     : /climato_tmp1/fettweis/MAR/msg"
echo "$host `date +"%Y-%m-%d %H:%M"` a79 1983 10 21 31 50 $NBR_CPU" >> /climato_tmp1/fettweis/MAR/bin/run/a79

#-------------------------------------------------------------------------------
# 3 - Work directory
#-------------------------------------------------------------------------------
echo ; echo "---- 3 ---- Work directory" ; echo


#work directory (/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31)

[ ! -d /scratch/fettweis/MARrun ] && mkdir /scratch/fettweis/MARrun

if [ ! -d /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31 ] ; then
  mkdir   /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31 ; err=$?
else
  rm -rf  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31
  mkdir   /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31 ; err=$?
  echo "rm: existing /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"
fi
#[ $err -eq 1 ] && qsub /climato_tmp1/fettweis/MAR/sim/GRa/run/a79/MAR_GRa79.83.10.21-31.cmd && sleep 120 && END_EXIT
if [ -d /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31.CRASH ] ; then
  rm -rf  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31.CRASH
  echo "rm: existing /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31.CRASH"
fi
mkdir /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/core

#MAR executable

if [ ! -f "/climato_tmp1/fettweis/MAR/tmp/MAR_a79.exe" ] ; then
 smget "/climato_tmp1/fettweis/MAR/out/GRa/a79/code" "MAR_a79.exe" "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"
else
 cp "/climato_tmp1/fettweis/MAR/tmp/MAR_a79.exe" "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"
fi
if [ ! -f /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR_a79.exe ] ; then
  DAMNED "executable non-existent [/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR_a79.exe]" "MAR_a79"
  END_EXIT
fi
chmod +x  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR_a79.exe

#-------------------------------------------------------------------------------
# 4 - Input data files
#-------------------------------------------------------------------------------
echo ; echo "---- 4 ---- Input data files" ; echo


#datMAR/*.dat

echo ; echo " > datMAR/*.dat" ; echo

cp /climato_tmp1/fettweis/MAR/src/datMAR/*.dat /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31
tmp=0
cp -f  /climato_tmp1/fettweis/MAR/sim/GRa/input/datMAR/AWSvou.dat  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/
tmp=$(( $tmp + $? ))
cp -f  /climato_tmp1/fettweis/MAR/sim/GRa/input/datMAR/MARctr.dat  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/
tmp=$(( $tmp + $? ))
cp -f  /climato_tmp1/fettweis/MAR/sim/GRa/input/datMAR/MARgou.dat  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/
tmp=$(( $tmp + $? ))
cp -f  /climato_tmp1/fettweis/MAR/sim/GRa/input/datMAR/MARinf.dat  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/
tmp=$(( $tmp + $? ))
cp -f  /climato_tmp1/fettweis/MAR/sim/GRa/input/datMAR/MARs01.dat  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/
tmp=$(( $tmp + $? ))
cp -f  /climato_tmp1/fettweis/MAR/sim/GRa/input/datMAR/MARs02.dat  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/
tmp=$(( $tmp + $? ))
[ $tmp -gt 0 ] && END_EXIT
cp -f /climato_tmp1/fettweis/MAR/sim/GRa/input/../usr/*.dat /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/
cp -f /climato_tmp1/fettweis/MAR/sim/GRa/input/../usr/*.nc  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/ 2>/dev/null
cp -f /climato_tmp1/fettweis/MAR/sim/GRa/input/../usr/*.cdf /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/ 2>/dev/null 
cp -f /climato_tmp1/fettweis/MAR/sim/GRa/input/../../../usr/*.dat /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/
cp -f /climato_tmp1/fettweis/MAR/sim/GRa/input/../../../usr/*.nc  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/ 2>/dev/null 
cp -f /climato_tmp1/fettweis/MAR/sim/GRa/input/../../../usr/*.cdf /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/ 2>/dev/null
echo  RCP60 > /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARscenario.ctr

#MARdom.dat

echo ; echo " > MARdom.dat (MARdom_GRa.1979.09.01.dat)" ; echo

cp -f  /climato_tmp1/fettweis/MAR/sim/GRa/input/MARdom/a79/MARdom_GRa.1979.09.01.dat.gz  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/
ucX "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31" "MARdom_GRa.1979.09.01.dat" ".gz"
[ $? -ne 0 ] && DAMNED "MARdom.dat not found" "MARdom" && END_EXIT
sed "s|GRa|a79|g" /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARdom_GRa.1979.09.01.dat > /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARdom.dat
rm -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARdom_GRa.1979.09.01.dat
mv MARdom.dat MARdom_GRa.1979.09.01.dat ; ln -s MARdom_GRa.1979.09.01.dat MARdom.dat

#MAR*.DAT

echo ; echo " > MAR*.DAT (MARsim_a79.1983.10.21.DAT)" ; echo
if [ ! -f "/climato_tmp1/fettweis/MAR/tmp/MARsim_a79.1983.10.21.DAT.tar.gz" ] ; then
 smget "/climato_tmp1/fettweis/MAR/out/GRa/input/MARsim/a79/1983" "MARsim_a79.1983.10.21.DAT.tar.gz" "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"
else
 cp "/climato_tmp1/fettweis/MAR/tmp/MARsim_a79.1983.10.21.DAT.tar.gz" "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"
fi
[ ! -f "MARsim_a79.1983.10.21.DAT.tar.gz" ] && rm -f /climato_tmp1/fettweis/MAR/sim/GRa/run/a79/MAR_GRa79.83.10.21-31.cmd && DAMNED "MARsim.DAT not found" "MARsim" && END_EXIT
ucX "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31" "MARsim_a79.1983.10.21.DAT" ".tar.gz" "MARdyn.DAT"
rm -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARsim_a79.1983.10.21.DAT* ; ln -s MARdyn.DAT MARsim_a79.1983.10.21.DAT

#MARlbc.DAT

echo ; echo " > MARlbc.DAT (MARlbc_GRa.1983.10*)" ; echo

    #look for existing MARlbc.DAT on stock

rm -f  /climato_tmp1/fettweis/.bcls-20536
i=0 ; err=1
while [ $err -ne 0 ] && [ $i -le 30 ] ; do
i=$(( $i + 1 )) ; sleep $(( $i * 2 ))
/usr/bin/ssh -o ConnectTimeout=10 -o ConnectionAttempts=10 -C fettweis@139.165.29.17 " cd /climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1983 ; ls -1 MARlbc_GRa.1983.10* " > /climato_tmp1/fettweis/.bcls-20536 ; err=$?
done
tmp=`cat /climato_tmp1/fettweis/.bcls-20536 | tr '\n' ' '`
tmp=`echo $tmp | sed "s:MARlbc_GRa.1983.10.::g"`
tmp=`echo $tmp | sed "s:.DAT.gz::g"`
j=`echo $tmp | wc -w`
tmp=`echo $tmp | sed "s/-/ /g"`
i=0 ; x=0
while [ $i -lt $(( $j * 2 )) ] ; do
  bcd1[$x]=`echo $tmp | cut -d' ' -f$(( $i + 1 ))`
  bcd2[$x]=`echo $tmp | cut -d' ' -f$(( $i + 2 ))`
  i=$(( $i + 2 ))
  x=$(( $x + 1 ))
done
rm -f  /climato_tmp1/fettweis/.bcls-20536

    #find the more suitable MARlbc.DAT

echo "period: 21-31"
i=0 ; tmp=""
for x1 in ${bcd1[@]} ; do
  if [ $x1 -le 21 ] ; then
    if [ ${bcd2[10#$i]} -ge 31 ] ; then
      tmp=$tmp" $i"  #index of candidates
      echo "  candidate $i: $x1-${bcd2[10#$i]}"
    fi
  fi
  i=$(( $i + 1 ))
done
i=`echo $tmp | wc -w`
i=$(( $i + 0 ))       #number of candidates
case $i in
(0) DAMNED "no MARlbc.DAT found in /climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1983 on stock" "MARlbc"
    END_EXIT ;;
(1) for x in $tmp ; do
      ld1=${bcd1[$x]}
      ld2=${bcd2[$x]}
    done
    echo "chosen: ${ld1}-$ld2" ;;
(*) tmp2=31 ;
    for x in $tmp ; do
      if [ $(( 10#${bcd2[$x]} - 10#${bcd1[$x]} )) -le $tmp2 ] ; then
        tmp2=$(( 10#${bcd2[$x]} - 10#${bcd1[$x]} ))
        ld1=${bcd1[$x]}
        ld2=${bcd2[$x]}
      fi
    done ;
    echo "chosen: ${ld1}-$ld2" ;;
esac
 MARbc=MARlbc_GRa.1983.10.${ld1}-$ld2.DAT
rm -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARlbc.DAT
if [ ! -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" ] ; then
 smget "/climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1983" "$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"
else
 if [ 11 -ne 10 ] ; then
  mv -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"
 else
  cp -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"
 fi
fi
[ ! -f "$MARbc.gz" ] && END_EXIT
ucX "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31" "$MARbc" ".gz"
[ $? -ne 0 ] && END_EXIT
ln -s  $MARbc  MARlbc.DAT

#MARglf.DAT

echo ; echo " > MARglf.DAT (MARglf_GRa.1983.10*)" ; echo

    #look for existing MARglf.DAT on stock

rm -f  /climato_tmp1/fettweis/.bcls-20536
i=0 ; err=1
while [ $err -ne 0 ] && [ $i -le 30 ] ; do
i=$(( $i + 1 )) ; sleep $(( $i * 2 ))
/usr/bin/ssh -o ConnectTimeout=10 -o ConnectionAttempts=10 -C fettweis@139.165.29.17 " cd /climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1983 ; ls -1 MARglf_GRa.1983.10* " > /climato_tmp1/fettweis/.bcls-20536 ; err=$?
done
tmp=`cat /climato_tmp1/fettweis/.bcls-20536 | tr '\n' ' '`
tmp=`echo $tmp | sed "s:MARglf_GRa.1983.10.::g"`
tmp=`echo $tmp | sed "s:.DAT.gz::g"`
j=`echo $tmp | wc -w`
tmp=`echo $tmp | sed "s/-/ /g"`
i=0 ; x=0
while [ $i -lt $(( $j * 2 )) ] ; do
  bcd1[$x]=`echo $tmp | cut -d' ' -f$(( $i + 1 ))`
  bcd2[$x]=`echo $tmp | cut -d' ' -f$(( $i + 2 ))`
  i=$(( $i + 2 ))
  x=$(( $x + 1 ))
done
rm -f  /climato_tmp1/fettweis/.bcls-20536

    #find the more suitable MARglf.DAT

echo "period: 21-31"
i=0 ; tmp=""
for x1 in ${bcd1[@]} ; do
  if [ $x1 -le 21 ] ; then
    if [ ${bcd2[10#$i]} -ge 31 ] ; then
      tmp=$tmp" $i"  #index of candidates
      echo "  candidate $i: $x1-${bcd2[10#$i]}"
    fi
  fi
  i=$(( $i + 1 ))
done
i=`echo $tmp | wc -w`
i=$(( $i + 0 ))       #number of candidates
case $i in
(0) DAMNED "no MARglf.DAT found in /climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1983 on stock" "MARglf"
    END_EXIT ;;
(1) for x in $tmp ; do
      ld1=${bcd1[$x]}
      ld2=${bcd2[$x]}
    done
    echo "chosen: ${ld1}-$ld2" ;;
(*) tmp2=31 ;
    for x in $tmp ; do
      if [ $(( 10#${bcd2[$x]} - 10#${bcd1[$x]} )) -le $tmp2 ] ; then
        tmp2=$(( 10#${bcd2[$x]} - 10#${bcd1[$x]} ))
        ld1=${bcd1[$x]}
        ld2=${bcd2[$x]}
      fi
    done ;
    echo "chosen: ${ld1}-$ld2" ;;
esac
 MARbc=MARglf_GRa.1983.10.${ld1}-$ld2.DAT
rm -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARglf.DAT
if [ ! -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" ] ; then
 smget "/climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1983" "$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"
else
 if [ 11 -ne 10 ] ; then
  mv -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"
 else
  cp -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"
 fi
fi
[ ! -f "$MARbc.gz" ] && END_EXIT
ucX "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31" "$MARbc" ".gz"
[ $? -ne 0 ] && END_EXIT
ln -s  $MARbc  MARglf.DAT

#MARsic.DAT

echo ; echo " > MARsic.DAT (MARsic_GRa.1983.10*)" ; echo

    #look for existing MARsic.DAT on stock

rm -f  /climato_tmp1/fettweis/.bcls-20536
i=0 ; err=1
while [ $err -ne 0 ] && [ $i -le 30 ] ; do
i=$(( $i + 1 )) ; sleep $(( $i * 2 ))
/usr/bin/ssh -o ConnectTimeout=10 -o ConnectionAttempts=10 -C fettweis@139.165.29.17 " cd /climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1983 ; ls -1 MARsic_GRa.1983.10* " > /climato_tmp1/fettweis/.bcls-20536 ; err=$?
done
tmp=`cat /climato_tmp1/fettweis/.bcls-20536 | tr '\n' ' '`
tmp=`echo $tmp | sed "s:MARsic_GRa.1983.10.::g"`
tmp=`echo $tmp | sed "s:.DAT.gz::g"`
j=`echo $tmp | wc -w`
tmp=`echo $tmp | sed "s/-/ /g"`
i=0 ; x=0
while [ $i -lt $(( $j * 2 )) ] ; do
  bcd1[$x]=`echo $tmp | cut -d' ' -f$(( $i + 1 ))`
  bcd2[$x]=`echo $tmp | cut -d' ' -f$(( $i + 2 ))`
  i=$(( $i + 2 ))
  x=$(( $x + 1 ))
done
rm -f  /climato_tmp1/fettweis/.bcls-20536

    #find the more suitable MARsic.DAT

echo "period: 21-31"
i=0 ; tmp=""
for x1 in ${bcd1[@]} ; do
  if [ $x1 -le 21 ] ; then
    if [ ${bcd2[10#$i]} -ge 31 ] ; then
      tmp=$tmp" $i"  #index of candidates
      echo "  candidate $i: $x1-${bcd2[10#$i]}"
    fi
  fi
  i=$(( $i + 1 ))
done
i=`echo $tmp | wc -w`
i=$(( $i + 0 ))       #number of candidates
case $i in
(0) DAMNED "no MARsic.DAT found in /climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1983 on stock" "MARsic"
    END_EXIT ;;
(1) for x in $tmp ; do
      ld1=${bcd1[$x]}
      ld2=${bcd2[$x]}
    done
    echo "chosen: ${ld1}-$ld2" ;;
(*) tmp2=31 ;
    for x in $tmp ; do
      if [ $(( 10#${bcd2[$x]} - 10#${bcd1[$x]} )) -le $tmp2 ] ; then
        tmp2=$(( 10#${bcd2[$x]} - 10#${bcd1[$x]} ))
        ld1=${bcd1[$x]}
        ld2=${bcd2[$x]}
      fi
    done ;
    echo "chosen: ${ld1}-$ld2" ;;
esac
 MARbc=MARsic_GRa.1983.10.${ld1}-$ld2.DAT
rm -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARsic.DAT
if [ ! -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" ] ; then
 smget "/climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1983" "$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"
else
 if [ 11 -ne 10 ] ; then
  mv -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"
 else
  cp -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"
 fi
fi
[ ! -f "$MARbc.gz" ] && END_EXIT
ucX "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31" "$MARbc" ".gz"
[ $? -ne 0 ] && END_EXIT
ln -s  $MARbc  MARsic.DAT

#MARubc.DAT

echo ; echo " > MARubc.DAT (MARubc_GRa.1983.10*)" ; echo

    #look for existing MARubc.DAT on stock

rm -f  /climato_tmp1/fettweis/.bcls-20536
i=0 ; err=1
while [ $err -ne 0 ] && [ $i -le 30 ] ; do
i=$(( $i + 1 )) ; sleep $(( $i * 2 ))
/usr/bin/ssh -o ConnectTimeout=10 -o ConnectionAttempts=10 -C fettweis@139.165.29.17 " cd /climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1983 ; ls -1 MARubc_GRa.1983.10* " > /climato_tmp1/fettweis/.bcls-20536 ; err=$?
done
tmp=`cat /climato_tmp1/fettweis/.bcls-20536 | tr '\n' ' '`
tmp=`echo $tmp | sed "s:MARubc_GRa.1983.10.::g"`
tmp=`echo $tmp | sed "s:.DAT.gz::g"`
j=`echo $tmp | wc -w`
tmp=`echo $tmp | sed "s/-/ /g"`
i=0 ; x=0
while [ $i -lt $(( $j * 2 )) ] ; do
  bcd1[$x]=`echo $tmp | cut -d' ' -f$(( $i + 1 ))`
  bcd2[$x]=`echo $tmp | cut -d' ' -f$(( $i + 2 ))`
  i=$(( $i + 2 ))
  x=$(( $x + 1 ))
done
rm -f  /climato_tmp1/fettweis/.bcls-20536

    #find the more suitable MARubc.DAT

echo "period: 21-31"
i=0 ; tmp=""
for x1 in ${bcd1[@]} ; do
  if [ $x1 -le 21 ] ; then
    if [ ${bcd2[10#$i]} -ge 31 ] ; then
      tmp=$tmp" $i"  #index of candidates
      echo "  candidate $i: $x1-${bcd2[10#$i]}"
    fi
  fi
  i=$(( $i + 1 ))
done
i=`echo $tmp | wc -w`
i=$(( $i + 0 ))       #number of candidates
case $i in
(0) DAMNED "no MARubc.DAT found in /climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1983 on stock" "MARubc"
    END_EXIT ;;
(1) for x in $tmp ; do
      ld1=${bcd1[$x]}
      ld2=${bcd2[$x]}
    done
    echo "chosen: ${ld1}-$ld2" ;;
(*) tmp2=31 ;
    for x in $tmp ; do
      if [ $(( 10#${bcd2[$x]} - 10#${bcd1[$x]} )) -le $tmp2 ] ; then
        tmp2=$(( 10#${bcd2[$x]} - 10#${bcd1[$x]} ))
        ld1=${bcd1[$x]}
        ld2=${bcd2[$x]}
      fi
    done ;
    echo "chosen: ${ld1}-$ld2" ;;
esac
 MARbc=MARubc_GRa.1983.10.${ld1}-$ld2.DAT
rm -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARubc.DAT
if [ ! -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" ] ; then
 smget "/climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1983" "$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"
else
 if [ 11 -ne 10 ] ; then
  mv -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"
 else
  cp -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"
 fi
fi
[ ! -f "$MARbc.gz" ] && END_EXIT
ucX "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31" "$MARbc" ".gz"
[ $? -ne 0 ] && END_EXIT
ln -s  $MARbc  MARubc.DAT

#MARctr.dat

echo ; echo " > MARctr.dat" ; echo
echo "           T  reaVAR=.F. => Input INI: Prev.Dyn.Simulat. (MAR, GCM) |" >  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           T  reaLBC=.F. => Input LBC: Prev.Dyn.Simulat. (MAR, GCM) |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           T  safVAR=.T. => Full Output on Saving Files MARxxx.DAT  |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           F  hamfil=.T. => Initial Filtered Fields (Time, Hamming) |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           T  conmas=.T. => Mass       Conserv. Constraint on Init. |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           F  potvor=.T. => P.-Vortic. Conserv. Constraint on Init. |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           T  brocam=.T. => Brown and Campana Time Scheme (Fast W.) |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           T  center=.T. => Pressure       Spatial Scheme  centered |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           4  nordps= 4  :  Pressure       Spatial Scheme Precision |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           T  staggr=.T. =>                Vertical  Grid staggered |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           T  turhor=.T. => Horizontal Diffusion (Smagorinsky)      |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           F  chimod=.F. => Atmospheric Chemical Model turned OFF   |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           T  convec=.T. => Convective  Adjustment     turned ON    |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           T  micphy=.T. => Cloud       Microphysics   turned ON    |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           T  fracld=.F. => Fractional  Cloudiness     turned OFF   |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "       100.0  rhcrHY     :  Critical Relative Humidity Value        |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "         0.0  tim_HY=18.0:  Cloud Microphys. start after 18h        |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "  0.0001D+00  czmnGE     :  cos(Z) minimal value                    |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           T  physic=.T. => Atmospheric/Surface Physics included    |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           T  vegmod=.F. => Interactive SVAT           turned OFF   |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           T  snomod=.F. => Interactive Snow Model is  included     |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           F  polmod=.F. => Interactive Polynya    is  included     |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "        1.00  hic0       :  Initial Thickness      of Sea Ice       |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "        0.10  fxlead     :  Initial Lead Fraction  in Sea Ice       |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           F  qsolSL=.T. => Soil Humidity is interactive            |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "        50.0  dt         =>    Time Step of Slow Dynamics       (s) |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "        432   nboucl     => Nb Time Steps between  each  Print      |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "         -44  np         => Nb Prints                               |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "           3  ntfast     :  Nb Fast Time Steps over dt  (Lamb)      |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "        50.0  dtDiff     :     Time Step of Diffusion               |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "        50.0  dtPhys     :     Time Step of Surface Physics     (s) |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "      3600.0  dtRadi     :     Time Step of Radiat. Transfert   (s) |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "  5.0000D-03  rxbase     :  Nudging Coefficient (Anthes et al. 1989)|" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "  1.0000D+02  rxfact     :  Lateral Sponge Coefficient         (A89)|" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "+-------------------------------------------------------------------+" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "|                                                                   |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "| Simulation     a79                                                |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "| Periode        21/10/1983 -> 31/10/1983                           |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "| Time Step      50 s                                              |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "| Conv. adjust.  T                                                  |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "| Surface model  SISVAT                                             |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "| Snow    model  T                                                  |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "| Polynya model  F                                                  |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "|                                                                   |" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat
echo "+-------------------------------------------------------------------+" >> /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat

[ ! -d /climato_tmp1/fettweis/MAR/sim/GRa/input/MARctr/a79 ] && mkdir /climato_tmp1/fettweis/MAR/sim/GRa/input/MARctr/a79
cp /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARctr.dat /climato_tmp1/fettweis/MAR/sim/GRa/input/MARctr/a79/MARctr.dat.831021-831031

#-------------------------------------------------------------------------------
# 5 - Stock initialisation
#-------------------------------------------------------------------------------
echo ; echo "---- 5 ---- Stock initialisation" ; echo

 /usr/bin/ssh -o ConnectTimeout=10 -o ConnectionAttempts=10 -C fettweis@139.165.29.17 mkdir -p /climato_tmp1/fettweis/MAR/out/GRa/crash &
 /usr/bin/ssh -o ConnectTimeout=10 -o ConnectionAttempts=10 -C fettweis@139.165.29.17 mkdir -p /climato_tmp1/fettweis/MAR/out/GRa/a79/log &
 /usr/bin/ssh -o ConnectTimeout=10 -o ConnectionAttempts=10 -C fettweis@139.165.29.17 mkdir -p /climato_tmp1/fettweis/MAR/out/GRa/a79/1983 &
 /usr/bin/ssh -o ConnectTimeout=10 -o ConnectionAttempts=10 -C fettweis@139.165.29.17 mkdir -p /climato_tmp1/fettweis/MAR/out/GRa/input/MARsim/a79/1983 &

#-------------------------------------------------------------------------------
# 6 - MAR launch
#-------------------------------------------------------------------------------
echo ; echo "---- 6 ---- MAR launch" ; echo

cd /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31
ulimit -s unlimited
 time  nice -n19 ./MAR_a79.exe > /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR_a79.log
ncdump -v rainHY MAR.*.nc > tmp3.dat
tmp1=`grep "nan, nan, nan," *.log | tail -1`
if [ -f /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR.OK ] && [ ${#tmp1} -eq 0 ] ; then
 echo
 echo "MAR run: OK (`date`)"
 marcrash=0
else
 cp /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR.log /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR.log2
 if [ $? -ne 0 ] ; then
  echo "Not space left on device on `hostname` at `date`"
  #qsub /climato_tmp1/fettweis/MAR/sim/GRa/run/a79/MAR_GRa79.83.10.21-31.cmd
  sleep 120
  rm -rf /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31
  END_EXIT
 else
  DAMNED "MAR error (MAR.OK non-existent) (`date`)" "CRASH"
  date                                      >  mail.txt
  hostname -f                               >> mail.txt
  tail -2  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR.log | head -1        >> mail.txt
  tail -15 /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR_a79.log              >> mail.txt
  end1=`tail -2 MAR.log  | head -1 | awk '{print $2}'`
  end2=`tail -2 MAR.log  | head -1 | awk '{print $3}'`
  end3=`tail -2 MAR.log  | head -1 | awk '{print $4}'`
  mail -s "a79831021e CRASH on $host at $end1 $end2 $end3: 50 => 40 s" xavier.fettweis@ulg.ac.be < mail.txt
  echo "1983 10 21 50 40  $end1 $end2 $end3" >> /climato_tmp1/fettweis/MAR/sim/GRa/ctrl/a79/MARdt.ctr
  MAR -again /climato_tmp1/fettweis/MAR/sim/GRa/ctrl/a79/MAR.ctr.790901-831231  a79  1983  10  21  e
  touch /climato_tmp1/fettweis/CRASH1_a79831021e_on_$host 
  rm -f mail.txt /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR.log2 &>/dev/null
  tail /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR.log /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR_a79.log
  crashtime="`tail -1 /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR_a79.log`"
  mv -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31.$host.CRASH
  tarX  "/scratch/fettweis/MARrun"  "MAR_GRa79.83.10.21-31.$host.CRASH"
  rm -rf  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31.$host.CRASH
  gzipX  "/scratch/fettweis/MARrun"  "MAR_GRa79.83.10.21-31.$host.CRASH.tar"
  [ $? -ne 0 ] && END_EXIT
  smput "/scratch/fettweis/MARrun"  "MAR_GRa79.83.10.21-31.$host.CRASH.tar.gz"  "/climato_tmp1/fettweis/MAR/out/GRa/crash"
  [ $? -eq 0 ] && rm -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31.$host.CRASH.tar.gz
  cd /climato_tmp1/fettweis/MAR/msg ; qsub  /climato_tmp1/fettweis/MAR/sim/GRa/run/a79/MAR_GRa79.83.10.21-31.cmd
  marcrash=1 ; exit
 fi
fi

#test on marcrash (not) to skip part of the script

#marcrash=1: go straight to Informations to summary file
#marcrash=0: just go on as usual

if [ $marcrash -eq 0 ] ; then  ## marcrash if ##


#-------------------------------------------------------------------------------
# 7 - Backup: listing files
#-------------------------------------------------------------------------------
echo ; echo "---- 7 ---- Backup: listing files" ; echo

ls -l /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31

#remove not needed files

echo ; echo "not needed files will be removed" ; echo
rm -f /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARphy.out /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARbil.out
rm -f /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARlbc* /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARglf* /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARsic* /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARubc*
rm -f /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARsim* /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARini* /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/*.dat
rm -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/*.gz

ls -l /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31

#-------------------------------------------------------------------------------
# 8 - Backup: .log files (MAR.log, MAR_a79.log)
#-------------------------------------------------------------------------------
echo ; echo "---- 8 ---- Backup: .log files (MAR.log, MAR_a79.log)" ; echo

mv -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR.log     /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR.log.a79.831021e
mv -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR_a79.log /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR_a79.log.831021e
smput "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"  "MAR.log.a79.831021e"  "/climato_tmp1/fettweis/MAR/out/GRa/a79/log"
[ $? -eq 0 ] && rm -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR.log.a79.831021e
smput "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"  "MAR_a79.log.831021e"  "/climato_tmp1/fettweis/MAR/out/GRa/a79/log"
[ $? -eq 0 ] && rm -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR_a79.log.831021e

#-------------------------------------------------------------------------------
# 9 - Backup: MARsim.DAT
#-------------------------------------------------------------------------------
echo ; echo "---- 9 ---- Backup: MARsim.DAT" ; echo

tarX  "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"  "MAR*.DAT"  "MARsim_a79.1983.11.01.DAT"
if [ $? -eq 0 ] ; then
  rm -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR*.DAT
  Z=".tar"
  gzipX  "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"  "MARsim_a79.1983.11.01.DAT$Z"
  [ $? -eq 0 ] && Z="$Z.gz"
  rm -f "/climato_tmp1/fettweis/MAR/tmp/MARsim_a79."*
  cp -f "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARsim_a79.1983.11.01.DAT$Z" "/climato_tmp1/fettweis/MAR/tmp"
  smput  "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"  "MARsim_a79.1983.11.01.DAT$Z"  "/climato_tmp1/fettweis/MAR/out/GRa/input/MARsim/a79/1983"
  if [ $? -eq 0 ] ; then
    rm -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MARsim_a79.1983.11.01.DAT$Z
    rm -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR*.DAT
  fi
  echo "MARsim_a79.1983.11.01.DAT$Z on stock"
fi

#-------------------------------------------------------------------------------
# 10 - Launch of the next period
#-------------------------------------------------------------------------------
echo ; echo "---- 10 ---- Launch of the next period" ; echo

echo "next simulation is: GRa a79 19831101c"
if [ $? -ne 99 -a -f /climato_tmp1/fettweis/MAR/sim/GRa/run/a79/MAR_GRa79.83.11.01-10.cmd ] ; then
  echo "MAR_GRa79.83.11.01-10.cmd launched in batch on linux"
else
  DAMNED "no job command file for next period simulation [/climato_tmp1/fettweis/MAR/sim/GRa/run/a79/MAR_GRa79.83.11.01-10.cmd]" "fil"
fi

#-------------------------------------------------------------------------------
# 11 - Backup: MAR output files
#-------------------------------------------------------------------------------
echo ; echo "---- 11 ---- Backup: MAR output files" ; echo


#Backup of existing *.nc files

echo ; echo " Backup of existing *.nc files" ; echo
cd /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31
rm -f /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/ONE.*.nc
rm -f /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/MAR.*.nc

  #ICE NetCDF output file

if [ ! -f /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/ICE*nc ] ; then
  echo "ICE NetCDF output file non-existent"
else
  echo ; echo "  > ICE (ICE.a79.1983.10.21-31.nc)" ; echo
  mv -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/ICE*nc  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/ICE.a79.1983.10.21-31.nc
  Z=""
  gzipX  "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"  "ICE.a79.1983.10.21-31.nc"
  [ $? -eq 0 ] && Z=".gz"
smput  "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"  "ICE.a79.1983.10.21-31.nc$Z"  "/climato_tmp1/fettweis/MAR/out/GRa/a79/1983"
[ $? -eq 0 ] && rm -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/ICE.a79.1983.10.21-31.nc$Z
fi

  #SBCnew.AWS output file (WAF only)

if [ -f /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/SBCnew.AWS ] ; then
  echo ; echo "  > SBCnew.AWS (SBCnew.AWS.a79.19831021-31)" ; echo
  mv -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/SBCnew.AWS  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/SBCnew.AWS.a79.19831021-31
  smput  "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"  "SBCnew.AWS.a79.19831021-31"  "/climato_tmp1/fettweis/MAR/out/GRa/a79/1983"
  [ $? -eq 0 ] && rm -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/SBCnew.AWS.a79.19831021-31
fi

  #stations NetCDF output file

if [ ! -f /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/*nc ] ; then
  echo ; echo "  > stations" ; echo
  echo "no other NetCDF output file"
else
  for fil in `ls *.nc` ; do
    [ "${fil:0:3}" != "ANT" ] && mv  $fil  ${fil%%.*}.a79.1983.10.21-31.nc
  done
  for fil in `ls *.nc` ; do
    [ "${fil:0:3}" != "ANT" ] && gzipX  "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"  "$fil"
    [ $? -eq 0 ] && Z=".gz"
  done
  for fil in `ls *.nc$Z` ; do
    smput  "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"  "$fil"  "/climato_tmp1/fettweis/MAR/out/GRa/a79/1983"
    [ $? -eq 0 ] && rm -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/$fil$Z
  done
  for fil in `ls SBCnew.AWS*` ; do
    [ "${fil:0:3}" != "ANT" ] && smput  "/scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31"  "$fil"  "/climato_tmp1/fettweis/MAR/out/GRa/a79/1983"
    [ $? -eq 0 ] && rm -f  /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31/$fil$Z
  done
fi

fi  ## marcrash if ##



#-------------------------------------------------------------------------------
# 12 - Informations to summary file
#-------------------------------------------------------------------------------
echo ; echo "---- 12 ---- Informations to summary file" ; echo

[ 19831110 -gt 19831231 -a ${#crashtime} -eq 0 ] && descrfile=y #RUN     OVER & NO CRASH
[ 19831110 -gt 19831231 -a ${#crashtime} -ne 0 ] && descrfile=y #RUN     OVER $    CRASH
[ 19831110 -le 19831231 -a ${#crashtime} -eq 0 ] && descrfile=n #RUN NOT OVER & NO CRASH
[ 19831110 -le 19831231 -a ${#crashtime} -ne 0 ] && descrfile=y #RUN NOT OVER &    CRASH
if [ $descrfile = "y" ] ; then
  [ ! -f /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa ] && touch /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  [ ${#crashtime} -ne 0 ] && echo "##CRASH##CRASH##CRASH##CRASH##CRASH##CRASH##CRASH## `date`" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  [ ${#crashtime} -eq 0 ] && echo "################################################### `date`" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "name    :  a79" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "period  :  01.09.1979 to 31.12.1983" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  tmp=`sed -n '1,3p' /climato_tmp1/fettweis/MAR/sim/GRa/ctrl/a79/descr.a79`
  echo "$tmp" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "--------------------------------------------------------------------------------" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "dt    :  50 s      ntfast:  3" >>/climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "np    :  -44       nboucl:  432       output:  each 360 min" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "dtDiff:  50 s      dtPhys:  50 s      dtRadi:  3600 s" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  tmp=`sed -n '4,21p' /climato_tmp1/fettweis/MAR/sim/GRa/ctrl/a79/descr.a79`
  echo "$tmp" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "cluster       :  linux (`uname -n`)" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  tmp=`sed -n '22,26p' /climato_tmp1/fettweis/MAR/sim/GRa/ctrl/a79/descr.a79`
  echo "$tmp" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "--------------------------------------------------------------------------------" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "Informations:" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo " WAt => NDVclim=F and border of constant topo. at boundaries=T in NESTOR.
 Opposite tested." >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  if [ ${#crashtime} -ne 0 ] ; then
    echo "---------------------------------------------------------------------------CRASH" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
    echo "CRASH on:" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
    echo "$crashtime" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  fi
  echo "################################################################################" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "informations to summary file /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa"
else
  echo "simulation not yet over"
fi

#-------------------------------------------------------------------------------
# 13 - Final job check
#-------------------------------------------------------------------------------
echo ; echo "---- 13 ---- Final job check" ; echo

  echo "a79831021e job executed successfully on linux"
  mv  $msg  $msg.ok
  rm -rf /scratch/fettweis/MARrun/MAR_GRa79.83.10.21-31
echo `date`
END_EXIT
