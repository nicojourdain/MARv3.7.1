#!/bin/bash
#PBS -N a99990101a
#PBS -o /climato_tmp1/fettweis/MAR/msg/MAR_GRa99.99.01.01-15.eo
#PBS -l nodes=1:ppn=8
#PBS -m a
#PBS -r y
#PBS -M xavier.fettweis@ulg.ac.be
#PBS -j oe
. /climato_tmp1/fettweis/MAR/bin/INI.ctr
. /climato_tmp1/fettweis/MAR/bin/FUNCTIONS
  export         NBR_CPU=8
  export OMP_NUM_THREADS=$NBR_CPU

#-------------------------------------------------------------------------------
# 1 - Initialisation
#-------------------------------------------------------------------------------
echo ; echo "---- 1 ---- Initialisation" ; echo


#variables definition

host=`hostname -s`

#job messages file in /climato_tmp1/fettweis/MAR/msg

if [ ! -f /climato_tmp1/fettweis/MAR/sim/GRa/run/a99/crash99010115 ] ; then
  rm -f /climato_tmp1/fettweis/MAR/msg/MAR_GRa99.99.01.01-15.${host%${host#???}}*
else
  for fil in `ls /climato_tmp1/fettweis/MAR/msg/MAR_GRa99.99.01.01-15*` ; do
    mv -f $fil ${fil}-crash
  done
fi
msg=/climato_tmp1/fettweis/MAR/msg/MAR_GRa99.99.01.01-15.$host
touch $msg

#-------------------------------------------------------------------------------
# 2 - Job informations
#-------------------------------------------------------------------------------
echo ; echo "---- 2 ---- Job informations" ; echo

[ -d /scratch/fettweis/MARrun ] || mkdir /scratch/fettweis/MARrun

echo "Begin (initial) : 01/01/1999 (01/09/1999)"
echo "End   (final  ) : 15/01/1999 (31/12/2002)"
echo
echo "Domain          : GRa (/climato_tmp1/fettweis/MAR/sim/GRa)"
echo
echo "Date            : `date`"
echo "Host            : $host"
echo "Work directory  : /scratch/fettweis/MARrun"
echo "Messages in     : /climato_tmp1/fettweis/MAR/msg"
echo "$host `date +"%Y-%m-%d %H:%M"` a99 1999 01 01 15 60 $NBR_CPU" >> /climato_tmp1/fettweis/MAR/bin/run/a99

#-------------------------------------------------------------------------------
# 3 - Work directory
#-------------------------------------------------------------------------------
echo ; echo "---- 3 ---- Work directory" ; echo


#work directory (/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15)

[ ! -d /scratch/fettweis/MARrun ] && mkdir /scratch/fettweis/MARrun

if [ ! -d /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15 ] ; then
  mkdir   /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15 ; err=$?
else
  rm -rf  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15
  mkdir   /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15 ; err=$?
  echo "rm: existing /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"
fi
#[ $err -eq 1 ] && qsub /climato_tmp1/fettweis/MAR/sim/GRa/run/a99/MAR_GRa99.99.01.01-15.cmd && sleep 120 && END_EXIT
if [ -d /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15.CRASH ] ; then
  rm -rf  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15.CRASH
  echo "rm: existing /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15.CRASH"
fi
mkdir /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/core

#MAR executable

if [ ! -f "/climato_tmp1/fettweis/MAR/tmp/MAR_a99.exe" ] ; then
 smget "/climato_tmp1/fettweis/MAR/out/GRa/a99/code" "MAR_a99.exe" "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"
else
 cp "/climato_tmp1/fettweis/MAR/tmp/MAR_a99.exe" "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"
fi
if [ ! -f /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR_a99.exe ] ; then
  DAMNED "executable non-existent [/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR_a99.exe]" "MAR_a99"
  END_EXIT
fi
chmod +x  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR_a99.exe

#-------------------------------------------------------------------------------
# 4 - Input data files
#-------------------------------------------------------------------------------
echo ; echo "---- 4 ---- Input data files" ; echo


#datMAR/*.dat

echo ; echo " > datMAR/*.dat" ; echo

cp /climato_tmp1/fettweis/MAR/src/datMAR/*.dat /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15
tmp=0
cp -f  /climato_tmp1/fettweis/MAR/sim/GRa/input/datMAR/AWSvou.dat  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/
tmp=$(( $tmp + $? ))
cp -f  /climato_tmp1/fettweis/MAR/sim/GRa/input/datMAR/MARctr.dat  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/
tmp=$(( $tmp + $? ))
cp -f  /climato_tmp1/fettweis/MAR/sim/GRa/input/datMAR/MARgou.dat  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/
tmp=$(( $tmp + $? ))
cp -f  /climato_tmp1/fettweis/MAR/sim/GRa/input/datMAR/MARinf.dat  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/
tmp=$(( $tmp + $? ))
cp -f  /climato_tmp1/fettweis/MAR/sim/GRa/input/datMAR/MARs01.dat  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/
tmp=$(( $tmp + $? ))
cp -f  /climato_tmp1/fettweis/MAR/sim/GRa/input/datMAR/MARs02.dat  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/
tmp=$(( $tmp + $? ))
[ $tmp -gt 0 ] && END_EXIT
cp -f /climato_tmp1/fettweis/MAR/sim/GRa/input/../usr/*.dat /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/
cp -f /climato_tmp1/fettweis/MAR/sim/GRa/input/../usr/*.nc  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/ 2>/dev/null
cp -f /climato_tmp1/fettweis/MAR/sim/GRa/input/../usr/*.cdf /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/ 2>/dev/null 
cp -f /climato_tmp1/fettweis/MAR/sim/GRa/input/../../../usr/*.dat /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/
cp -f /climato_tmp1/fettweis/MAR/sim/GRa/input/../../../usr/*.nc  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/ 2>/dev/null 
cp -f /climato_tmp1/fettweis/MAR/sim/GRa/input/../../../usr/*.cdf /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/ 2>/dev/null
echo  RCP60 > /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARscenario.ctr

#MARdom.dat

echo ; echo " > MARdom.dat (MARdom_GRa.1999.09.01.dat)" ; echo

cp -f  /climato_tmp1/fettweis/MAR/sim/GRa/input/MARdom/a99/MARdom_GRa.1999.09.01.dat.gz  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/
ucX "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15" "MARdom_GRa.1999.09.01.dat" ".gz"
[ $? -ne 0 ] && DAMNED "MARdom.dat not found" "MARdom" && END_EXIT
sed "s|GRa|a99|g" /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARdom_GRa.1999.09.01.dat > /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARdom.dat
rm -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARdom_GRa.1999.09.01.dat
mv MARdom.dat MARdom_GRa.1999.09.01.dat ; ln -s MARdom_GRa.1999.09.01.dat MARdom.dat

#MAR*.DAT

echo ; echo " > MAR*.DAT (MARsim_a99.1999.01.01.DAT)" ; echo
if [ ! -f "/climato_tmp1/fettweis/MAR/tmp/MARsim_a99.1999.01.01.DAT.tar.gz" ] ; then
 smget "/climato_tmp1/fettweis/MAR/out/GRa/input/MARsim/a99/1999" "MARsim_a99.1999.01.01.DAT.tar.gz" "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"
else
 cp "/climato_tmp1/fettweis/MAR/tmp/MARsim_a99.1999.01.01.DAT.tar.gz" "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"
fi
[ ! -f "MARsim_a99.1999.01.01.DAT.tar.gz" ] && rm -f /climato_tmp1/fettweis/MAR/sim/GRa/run/a99/MAR_GRa99.99.01.01-15.cmd && DAMNED "MARsim.DAT not found" "MARsim" && END_EXIT
ucX "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15" "MARsim_a99.1999.01.01.DAT" ".tar.gz" "MARdyn.DAT"
rm -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARsim_a99.1999.01.01.DAT* ; ln -s MARdyn.DAT MARsim_a99.1999.01.01.DAT

#MARlbc.DAT

echo ; echo " > MARlbc.DAT (MARlbc_GRa.1999.01*)" ; echo

    #look for existing MARlbc.DAT on stock

rm -f  /climato_tmp1/fettweis/.bcls-25251
i=0 ; err=1
while [ $err -ne 0 ] && [ $i -le 30 ] ; do
i=$(( $i + 1 )) ; sleep $(( $i * 2 ))
/usr/bin/ssh -o ConnectTimeout=10 -o ConnectionAttempts=10 -C fettweis@139.165.29.17 " cd /climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1999 ; ls -1 MARlbc_GRa.1999.01* " > /climato_tmp1/fettweis/.bcls-25251 ; err=$?
done
tmp=`cat /climato_tmp1/fettweis/.bcls-25251 | tr '\n' ' '`
tmp=`echo $tmp | sed "s:MARlbc_GRa.1999.01.::g"`
tmp=`echo $tmp | sed "s:.DAT.gz::g"`
j=`echo $tmp | wc -w`
tmp=`echo $tmp | sed "s/-/ /g"`
i=0 ; x=0
while [ $i -lt $(( $j * 2 )) ] ; do
  bcd1[$x]=`echo $tmp | cut -d' ' -f$(( $i + 1 ))`
  bcd2[$x]=`echo $tmp | cut -d' ' -f$(( $i + 2 ))`
  i=$(( $i + 2 ))
  x=$(( $x + 1 ))
done
rm -f  /climato_tmp1/fettweis/.bcls-25251

    #find the more suitable MARlbc.DAT

echo "period: 01-15"
i=0 ; tmp=""
for x1 in ${bcd1[@]} ; do
  if [ $x1 -le 01 ] ; then
    if [ ${bcd2[10#$i]} -ge 15 ] ; then
      tmp=$tmp" $i"  #index of candidates
      echo "  candidate $i: $x1-${bcd2[10#$i]}"
    fi
  fi
  i=$(( $i + 1 ))
done
i=`echo $tmp | wc -w`
i=$(( $i + 0 ))       #number of candidates
case $i in
(0) DAMNED "no MARlbc.DAT found in /climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1999 on stock" "MARlbc"
    END_EXIT ;;
(1) for x in $tmp ; do
      ld1=${bcd1[$x]}
      ld2=${bcd2[$x]}
    done
    echo "chosen: ${ld1}-$ld2" ;;
(*) tmp2=31 ;
    for x in $tmp ; do
      if [ $(( 10#${bcd2[$x]} - 10#${bcd1[$x]} )) -le $tmp2 ] ; then
        tmp2=$(( 10#${bcd2[$x]} - 10#${bcd1[$x]} ))
        ld1=${bcd1[$x]}
        ld2=${bcd2[$x]}
      fi
    done ;
    echo "chosen: ${ld1}-$ld2" ;;
esac
 MARbc=MARlbc_GRa.1999.01.${ld1}-$ld2.DAT
rm -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARlbc.DAT
if [ ! -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" ] ; then
 smget "/climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1999" "$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"
else
 if [ 1 -ne 1 ] ; then
  mv -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"
 else
  cp -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"
 fi
fi
[ ! -f "$MARbc.gz" ] && END_EXIT
ucX "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15" "$MARbc" ".gz"
[ $? -ne 0 ] && END_EXIT
ln -s  $MARbc  MARlbc.DAT

#MARglf.DAT

echo ; echo " > MARglf.DAT (MARglf_GRa.1999.01*)" ; echo

    #look for existing MARglf.DAT on stock

rm -f  /climato_tmp1/fettweis/.bcls-25251
i=0 ; err=1
while [ $err -ne 0 ] && [ $i -le 30 ] ; do
i=$(( $i + 1 )) ; sleep $(( $i * 2 ))
/usr/bin/ssh -o ConnectTimeout=10 -o ConnectionAttempts=10 -C fettweis@139.165.29.17 " cd /climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1999 ; ls -1 MARglf_GRa.1999.01* " > /climato_tmp1/fettweis/.bcls-25251 ; err=$?
done
tmp=`cat /climato_tmp1/fettweis/.bcls-25251 | tr '\n' ' '`
tmp=`echo $tmp | sed "s:MARglf_GRa.1999.01.::g"`
tmp=`echo $tmp | sed "s:.DAT.gz::g"`
j=`echo $tmp | wc -w`
tmp=`echo $tmp | sed "s/-/ /g"`
i=0 ; x=0
while [ $i -lt $(( $j * 2 )) ] ; do
  bcd1[$x]=`echo $tmp | cut -d' ' -f$(( $i + 1 ))`
  bcd2[$x]=`echo $tmp | cut -d' ' -f$(( $i + 2 ))`
  i=$(( $i + 2 ))
  x=$(( $x + 1 ))
done
rm -f  /climato_tmp1/fettweis/.bcls-25251

    #find the more suitable MARglf.DAT

echo "period: 01-15"
i=0 ; tmp=""
for x1 in ${bcd1[@]} ; do
  if [ $x1 -le 01 ] ; then
    if [ ${bcd2[10#$i]} -ge 15 ] ; then
      tmp=$tmp" $i"  #index of candidates
      echo "  candidate $i: $x1-${bcd2[10#$i]}"
    fi
  fi
  i=$(( $i + 1 ))
done
i=`echo $tmp | wc -w`
i=$(( $i + 0 ))       #number of candidates
case $i in
(0) DAMNED "no MARglf.DAT found in /climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1999 on stock" "MARglf"
    END_EXIT ;;
(1) for x in $tmp ; do
      ld1=${bcd1[$x]}
      ld2=${bcd2[$x]}
    done
    echo "chosen: ${ld1}-$ld2" ;;
(*) tmp2=31 ;
    for x in $tmp ; do
      if [ $(( 10#${bcd2[$x]} - 10#${bcd1[$x]} )) -le $tmp2 ] ; then
        tmp2=$(( 10#${bcd2[$x]} - 10#${bcd1[$x]} ))
        ld1=${bcd1[$x]}
        ld2=${bcd2[$x]}
      fi
    done ;
    echo "chosen: ${ld1}-$ld2" ;;
esac
 MARbc=MARglf_GRa.1999.01.${ld1}-$ld2.DAT
rm -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARglf.DAT
if [ ! -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" ] ; then
 smget "/climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1999" "$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"
else
 if [ 1 -ne 1 ] ; then
  mv -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"
 else
  cp -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"
 fi
fi
[ ! -f "$MARbc.gz" ] && END_EXIT
ucX "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15" "$MARbc" ".gz"
[ $? -ne 0 ] && END_EXIT
ln -s  $MARbc  MARglf.DAT

#MARsic.DAT

echo ; echo " > MARsic.DAT (MARsic_GRa.1999.01*)" ; echo

    #look for existing MARsic.DAT on stock

rm -f  /climato_tmp1/fettweis/.bcls-25251
i=0 ; err=1
while [ $err -ne 0 ] && [ $i -le 30 ] ; do
i=$(( $i + 1 )) ; sleep $(( $i * 2 ))
/usr/bin/ssh -o ConnectTimeout=10 -o ConnectionAttempts=10 -C fettweis@139.165.29.17 " cd /climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1999 ; ls -1 MARsic_GRa.1999.01* " > /climato_tmp1/fettweis/.bcls-25251 ; err=$?
done
tmp=`cat /climato_tmp1/fettweis/.bcls-25251 | tr '\n' ' '`
tmp=`echo $tmp | sed "s:MARsic_GRa.1999.01.::g"`
tmp=`echo $tmp | sed "s:.DAT.gz::g"`
j=`echo $tmp | wc -w`
tmp=`echo $tmp | sed "s/-/ /g"`
i=0 ; x=0
while [ $i -lt $(( $j * 2 )) ] ; do
  bcd1[$x]=`echo $tmp | cut -d' ' -f$(( $i + 1 ))`
  bcd2[$x]=`echo $tmp | cut -d' ' -f$(( $i + 2 ))`
  i=$(( $i + 2 ))
  x=$(( $x + 1 ))
done
rm -f  /climato_tmp1/fettweis/.bcls-25251

    #find the more suitable MARsic.DAT

echo "period: 01-15"
i=0 ; tmp=""
for x1 in ${bcd1[@]} ; do
  if [ $x1 -le 01 ] ; then
    if [ ${bcd2[10#$i]} -ge 15 ] ; then
      tmp=$tmp" $i"  #index of candidates
      echo "  candidate $i: $x1-${bcd2[10#$i]}"
    fi
  fi
  i=$(( $i + 1 ))
done
i=`echo $tmp | wc -w`
i=$(( $i + 0 ))       #number of candidates
case $i in
(0) DAMNED "no MARsic.DAT found in /climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1999 on stock" "MARsic"
    END_EXIT ;;
(1) for x in $tmp ; do
      ld1=${bcd1[$x]}
      ld2=${bcd2[$x]}
    done
    echo "chosen: ${ld1}-$ld2" ;;
(*) tmp2=31 ;
    for x in $tmp ; do
      if [ $(( 10#${bcd2[$x]} - 10#${bcd1[$x]} )) -le $tmp2 ] ; then
        tmp2=$(( 10#${bcd2[$x]} - 10#${bcd1[$x]} ))
        ld1=${bcd1[$x]}
        ld2=${bcd2[$x]}
      fi
    done ;
    echo "chosen: ${ld1}-$ld2" ;;
esac
 MARbc=MARsic_GRa.1999.01.${ld1}-$ld2.DAT
rm -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARsic.DAT
if [ ! -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" ] ; then
 smget "/climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1999" "$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"
else
 if [ 1 -ne 1 ] ; then
  mv -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"
 else
  cp -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"
 fi
fi
[ ! -f "$MARbc.gz" ] && END_EXIT
ucX "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15" "$MARbc" ".gz"
[ $? -ne 0 ] && END_EXIT
ln -s  $MARbc  MARsic.DAT

#MARubc.DAT

echo ; echo " > MARubc.DAT (MARubc_GRa.1999.01*)" ; echo

    #look for existing MARubc.DAT on stock

rm -f  /climato_tmp1/fettweis/.bcls-25251
i=0 ; err=1
while [ $err -ne 0 ] && [ $i -le 30 ] ; do
i=$(( $i + 1 )) ; sleep $(( $i * 2 ))
/usr/bin/ssh -o ConnectTimeout=10 -o ConnectionAttempts=10 -C fettweis@139.165.29.17 " cd /climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1999 ; ls -1 MARubc_GRa.1999.01* " > /climato_tmp1/fettweis/.bcls-25251 ; err=$?
done
tmp=`cat /climato_tmp1/fettweis/.bcls-25251 | tr '\n' ' '`
tmp=`echo $tmp | sed "s:MARubc_GRa.1999.01.::g"`
tmp=`echo $tmp | sed "s:.DAT.gz::g"`
j=`echo $tmp | wc -w`
tmp=`echo $tmp | sed "s/-/ /g"`
i=0 ; x=0
while [ $i -lt $(( $j * 2 )) ] ; do
  bcd1[$x]=`echo $tmp | cut -d' ' -f$(( $i + 1 ))`
  bcd2[$x]=`echo $tmp | cut -d' ' -f$(( $i + 2 ))`
  i=$(( $i + 2 ))
  x=$(( $x + 1 ))
done
rm -f  /climato_tmp1/fettweis/.bcls-25251

    #find the more suitable MARubc.DAT

echo "period: 01-15"
i=0 ; tmp=""
for x1 in ${bcd1[@]} ; do
  if [ $x1 -le 01 ] ; then
    if [ ${bcd2[10#$i]} -ge 15 ] ; then
      tmp=$tmp" $i"  #index of candidates
      echo "  candidate $i: $x1-${bcd2[10#$i]}"
    fi
  fi
  i=$(( $i + 1 ))
done
i=`echo $tmp | wc -w`
i=$(( $i + 0 ))       #number of candidates
case $i in
(0) DAMNED "no MARubc.DAT found in /climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1999 on stock" "MARubc"
    END_EXIT ;;
(1) for x in $tmp ; do
      ld1=${bcd1[$x]}
      ld2=${bcd2[$x]}
    done
    echo "chosen: ${ld1}-$ld2" ;;
(*) tmp2=31 ;
    for x in $tmp ; do
      if [ $(( 10#${bcd2[$x]} - 10#${bcd1[$x]} )) -le $tmp2 ] ; then
        tmp2=$(( 10#${bcd2[$x]} - 10#${bcd1[$x]} ))
        ld1=${bcd1[$x]}
        ld2=${bcd2[$x]}
      fi
    done ;
    echo "chosen: ${ld1}-$ld2" ;;
esac
 MARbc=MARubc_GRa.1999.01.${ld1}-$ld2.DAT
rm -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARubc.DAT
if [ ! -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" ] ; then
 smget "/climato_tmp1/fettweis/MAR/out/GRa/input/NESTOR/1999" "$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"
else
 if [ 1 -ne 1 ] ; then
  mv -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"
 else
  cp -f "/climato_tmp1/fettweis/MAR/tmp/$MARbc.gz" "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"
 fi
fi
[ ! -f "$MARbc.gz" ] && END_EXIT
ucX "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15" "$MARbc" ".gz"
[ $? -ne 0 ] && END_EXIT
ln -s  $MARbc  MARubc.DAT

#MARctr.dat

echo ; echo " > MARctr.dat" ; echo
echo "           T  reaVAR=.F. => Input INI: Prev.Dyn.Simulat. (MAR, GCM) |" >  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           T  reaLBC=.F. => Input LBC: Prev.Dyn.Simulat. (MAR, GCM) |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           T  safVAR=.T. => Full Output on Saving Files MARxxx.DAT  |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           F  hamfil=.T. => Initial Filtered Fields (Time, Hamming) |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           T  conmas=.T. => Mass       Conserv. Constraint on Init. |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           F  potvor=.T. => P.-Vortic. Conserv. Constraint on Init. |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           T  brocam=.T. => Brown and Campana Time Scheme (Fast W.) |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           T  center=.T. => Pressure       Spatial Scheme  centered |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           4  nordps= 4  :  Pressure       Spatial Scheme Precision |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           T  staggr=.T. =>                Vertical  Grid staggered |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           T  turhor=.T. => Horizontal Diffusion (Smagorinsky)      |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           F  chimod=.F. => Atmospheric Chemical Model turned OFF   |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           T  convec=.T. => Convective  Adjustment     turned ON    |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           T  micphy=.T. => Cloud       Microphysics   turned ON    |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           T  fracld=.F. => Fractional  Cloudiness     turned OFF   |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "       100.0  rhcrHY     :  Critical Relative Humidity Value        |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "         0.0  tim_HY=18.0:  Cloud Microphys. start after 18h        |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "  0.0001D+00  czmnGE     :  cos(Z) minimal value                    |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           T  physic=.T. => Atmospheric/Surface Physics included    |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           T  vegmod=.F. => Interactive SVAT           turned OFF   |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           T  snomod=.F. => Interactive Snow Model is  included     |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           F  polmod=.F. => Interactive Polynya    is  included     |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "        1.00  hic0       :  Initial Thickness      of Sea Ice       |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "        0.10  fxlead     :  Initial Lead Fraction  in Sea Ice       |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           F  qsolSL=.T. => Soil Humidity is interactive            |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "        60.0  dt         =>    Time Step of Slow Dynamics       (s) |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "        360   nboucl     => Nb Time Steps between  each  Print      |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "         -60  np         => Nb Prints                               |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "           3  ntfast     :  Nb Fast Time Steps over dt  (Lamb)      |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "        60.0  dtDiff     :     Time Step of Diffusion               |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "        60.0  dtPhys     :     Time Step of Surface Physics     (s) |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "      1800.0  dtRadi     :     Time Step of Radiat. Transfert   (s) |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "  5.0000D-03  rxbase     :  Nudging Coefficient (Anthes et al. 1989)|" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "  1.0000D+02  rxfact     :  Lateral Sponge Coefficient         (A89)|" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "+-------------------------------------------------------------------+" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "|                                                                   |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "| Simulation     a99                                                |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "| Periode        01/01/1999 -> 15/01/1999                           |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "| Time Step      60 s                                              |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "| Conv. adjust.  T                                                  |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "| Surface model  SISVAT                                             |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "| Snow    model  T                                                  |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "| Polynya model  F                                                  |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "|                                                                   |" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat
echo "+-------------------------------------------------------------------+" >> /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat

[ ! -d /climato_tmp1/fettweis/MAR/sim/GRa/input/MARctr/a99 ] && mkdir /climato_tmp1/fettweis/MAR/sim/GRa/input/MARctr/a99
cp /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARctr.dat /climato_tmp1/fettweis/MAR/sim/GRa/input/MARctr/a99/MARctr.dat.990101-990115

#-------------------------------------------------------------------------------
# 5 - Stock initialisation
#-------------------------------------------------------------------------------
echo ; echo "---- 5 ---- Stock initialisation" ; echo

 /usr/bin/ssh -o ConnectTimeout=10 -o ConnectionAttempts=10 -C fettweis@139.165.29.17 mkdir -p /climato_tmp1/fettweis/MAR/out/GRa/crash &
 /usr/bin/ssh -o ConnectTimeout=10 -o ConnectionAttempts=10 -C fettweis@139.165.29.17 mkdir -p /climato_tmp1/fettweis/MAR/out/GRa/a99/log &
 /usr/bin/ssh -o ConnectTimeout=10 -o ConnectionAttempts=10 -C fettweis@139.165.29.17 mkdir -p /climato_tmp1/fettweis/MAR/out/GRa/a99/1999 &
 /usr/bin/ssh -o ConnectTimeout=10 -o ConnectionAttempts=10 -C fettweis@139.165.29.17 mkdir -p /climato_tmp1/fettweis/MAR/out/GRa/input/MARsim/a99/1999 &

#-------------------------------------------------------------------------------
# 6 - MAR launch
#-------------------------------------------------------------------------------
echo ; echo "---- 6 ---- MAR launch" ; echo

cd /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15
ulimit -s unlimited
 time  nice -n19 ./MAR_a99.exe > /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR_a99.log
ncdump -v rainHY MAR.*.nc > tmp3.dat
tmp1=`grep "nan, nan, nan," *.log | tail -1`
if [ -f /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR.OK ] && [ ${#tmp1} -eq 0 ] ; then
 echo
 echo "MAR run: OK (`date`)"
 marcrash=0
else
 cp /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR.log /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR.log2
 if [ $? -ne 0 ] ; then
  echo "Not space left on device on `hostname` at `date`"
  #qsub /climato_tmp1/fettweis/MAR/sim/GRa/run/a99/MAR_GRa99.99.01.01-15.cmd
  sleep 120
  rm -rf /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15
  END_EXIT
 else
  DAMNED "MAR error (MAR.OK non-existent) (`date`)" "CRASH"
  date                                      >  mail.txt
  hostname -f                               >> mail.txt
  tail -2  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR.log | head -1        >> mail.txt
  tail -15 /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR_a99.log              >> mail.txt
  end1=`tail -2 MAR.log  | head -1 | awk '{print $2}'`
  end2=`tail -2 MAR.log  | head -1 | awk '{print $3}'`
  end3=`tail -2 MAR.log  | head -1 | awk '{print $4}'`
  mail -s "a99990101a CRASH on $host at $end1 $end2 $end3: 60 => 50 s" xavier.fettweis@ulg.ac.be < mail.txt
  echo "1999 1 1 60 50  $end1 $end2 $end3" >> /climato_tmp1/fettweis/MAR/sim/GRa/ctrl/a99/MARdt.ctr
  MAR -again /climato_tmp1/fettweis/MAR/sim/GRa/ctrl/a99/MAR.ctr.990901-021231  a99  1999  01  01  a
  touch /climato_tmp1/fettweis/CRASH1_a99990101a_on_$host 
  rm -f mail.txt /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR.log2 &>/dev/null
  tail /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR.log /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR_a99.log
  crashtime="`tail -1 /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR_a99.log`"
  mv -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15.$host.CRASH
  tarX  "/scratch/fettweis/MARrun"  "MAR_GRa99.99.01.01-15.$host.CRASH"
  rm -rf  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15.$host.CRASH
  gzipX  "/scratch/fettweis/MARrun"  "MAR_GRa99.99.01.01-15.$host.CRASH.tar"
  [ $? -ne 0 ] && END_EXIT
  smput "/scratch/fettweis/MARrun"  "MAR_GRa99.99.01.01-15.$host.CRASH.tar.gz"  "/climato_tmp1/fettweis/MAR/out/GRa/crash"
  [ $? -eq 0 ] && rm -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15.$host.CRASH.tar.gz
  cd /climato_tmp1/fettweis/MAR/msg ; qsub  /climato_tmp1/fettweis/MAR/sim/GRa/run/a99/MAR_GRa99.99.01.01-15.cmd
  marcrash=1 ; exit
 fi
fi

#test on marcrash (not) to skip part of the script

#marcrash=1: go straight to Informations to summary file
#marcrash=0: just go on as usual

if [ $marcrash -eq 0 ] ; then  ## marcrash if ##


#-------------------------------------------------------------------------------
# 7 - Backup: listing files
#-------------------------------------------------------------------------------
echo ; echo "---- 7 ---- Backup: listing files" ; echo

ls -l /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15

#remove not needed files

echo ; echo "not needed files will be removed" ; echo
rm -f /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARphy.out /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARbil.out
rm -f /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARlbc* /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARglf* /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARsic* /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARubc*
rm -f /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARsim* /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARini* /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/*.dat
rm -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/*.gz

ls -l /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15

#-------------------------------------------------------------------------------
# 8 - Backup: .log files (MAR.log, MAR_a99.log)
#-------------------------------------------------------------------------------
echo ; echo "---- 8 ---- Backup: .log files (MAR.log, MAR_a99.log)" ; echo

mv -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR.log     /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR.log.a99.990101a
mv -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR_a99.log /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR_a99.log.990101a
smput "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"  "MAR.log.a99.990101a"  "/climato_tmp1/fettweis/MAR/out/GRa/a99/log"
[ $? -eq 0 ] && rm -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR.log.a99.990101a
smput "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"  "MAR_a99.log.990101a"  "/climato_tmp1/fettweis/MAR/out/GRa/a99/log"
[ $? -eq 0 ] && rm -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR_a99.log.990101a

#-------------------------------------------------------------------------------
# 9 - Backup: MARsim.DAT
#-------------------------------------------------------------------------------
echo ; echo "---- 9 ---- Backup: MARsim.DAT" ; echo

tarX  "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"  "MAR*.DAT"  "MARsim_a99.1999.01.16.DAT"
if [ $? -eq 0 ] ; then
  rm -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR*.DAT
  Z=".tar"
  gzipX  "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"  "MARsim_a99.1999.01.16.DAT$Z"
  [ $? -eq 0 ] && Z="$Z.gz"
  rm -f "/climato_tmp1/fettweis/MAR/tmp/MARsim_a99."*
  cp -f "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARsim_a99.1999.01.16.DAT$Z" "/climato_tmp1/fettweis/MAR/tmp"
  smput  "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"  "MARsim_a99.1999.01.16.DAT$Z"  "/climato_tmp1/fettweis/MAR/out/GRa/input/MARsim/a99/1999"
  if [ $? -eq 0 ] ; then
    rm -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MARsim_a99.1999.01.16.DAT$Z
    rm -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR*.DAT
  fi
  echo "MARsim_a99.1999.01.16.DAT$Z on stock"
fi

#-------------------------------------------------------------------------------
# 10 - Launch of the next period
#-------------------------------------------------------------------------------
echo ; echo "---- 10 ---- Launch of the next period" ; echo

echo "next simulation is: GRa a99 19990116b"
MAR -again /climato_tmp1/fettweis/MAR/sim/GRa/ctrl/a99/MAR.ctr.990901-021231  a99  1999  01  16  b  > /dev/null
if [ $? -ne 99 -a -f /climato_tmp1/fettweis/MAR/sim/GRa/run/a99/MAR_GRa99.99.01.16-31.cmd ] ; then
 cd /climato_tmp1/fettweis/MAR/msg ; qsub /climato_tmp1/fettweis/MAR/sim/GRa/run/a99/MAR_GRa99.99.01.16-31.cmd
  echo "MAR_GRa99.99.01.16-31.cmd launched in batch on linux"
else
  DAMNED "no job command file for next period simulation [/climato_tmp1/fettweis/MAR/sim/GRa/run/a99/MAR_GRa99.99.01.16-31.cmd]" "fil"
fi

#-------------------------------------------------------------------------------
# 11 - Backup: MAR output files
#-------------------------------------------------------------------------------
echo ; echo "---- 11 ---- Backup: MAR output files" ; echo


#Backup of existing *.nc files

echo ; echo " Backup of existing *.nc files" ; echo
cd /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15
rm -f /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/ONE.*.nc
rm -f /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/MAR.*.nc

  #ICE NetCDF output file

if [ ! -f /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/ICE*nc ] ; then
  echo "ICE NetCDF output file non-existent"
else
  echo ; echo "  > ICE (ICE.a99.1999.01.01-15.nc)" ; echo
  mv -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/ICE*nc  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/ICE.a99.1999.01.01-15.nc
  Z=""
  gzipX  "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"  "ICE.a99.1999.01.01-15.nc"
  [ $? -eq 0 ] && Z=".gz"
smput  "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"  "ICE.a99.1999.01.01-15.nc$Z"  "/climato_tmp1/fettweis/MAR/out/GRa/a99/1999"
[ $? -eq 0 ] && rm -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/ICE.a99.1999.01.01-15.nc$Z
fi

  #SBCnew.AWS output file (WAF only)

if [ -f /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/SBCnew.AWS ] ; then
  echo ; echo "  > SBCnew.AWS (SBCnew.AWS.a99.19990101-15)" ; echo
  mv -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/SBCnew.AWS  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/SBCnew.AWS.a99.19990101-15
  smput  "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"  "SBCnew.AWS.a99.19990101-15"  "/climato_tmp1/fettweis/MAR/out/GRa/a99/1999"
  [ $? -eq 0 ] && rm -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/SBCnew.AWS.a99.19990101-15
fi

  #stations NetCDF output file

if [ ! -f /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/*nc ] ; then
  echo ; echo "  > stations" ; echo
  echo "no other NetCDF output file"
else
  for fil in `ls *.nc` ; do
    [ "${fil:0:3}" != "ANT" ] && mv  $fil  ${fil%%.*}.a99.1999.01.01-15.nc
  done
  for fil in `ls *.nc` ; do
    [ "${fil:0:3}" != "ANT" ] && gzipX  "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"  "$fil"
    [ $? -eq 0 ] && Z=".gz"
  done
  for fil in `ls *.nc$Z` ; do
    smput  "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"  "$fil"  "/climato_tmp1/fettweis/MAR/out/GRa/a99/1999"
    [ $? -eq 0 ] && rm -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/$fil$Z
  done
  for fil in `ls SBCnew.AWS*` ; do
    [ "${fil:0:3}" != "ANT" ] && smput  "/scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15"  "$fil"  "/climato_tmp1/fettweis/MAR/out/GRa/a99/1999"
    [ $? -eq 0 ] && rm -f  /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15/$fil$Z
  done
fi

fi  ## marcrash if ##



#-------------------------------------------------------------------------------
# 12 - Informations to summary file
#-------------------------------------------------------------------------------
echo ; echo "---- 12 ---- Informations to summary file" ; echo

[ 19990131 -gt 20021231 -a ${#crashtime} -eq 0 ] && descrfile=y #RUN     OVER & NO CRASH
[ 19990131 -gt 20021231 -a ${#crashtime} -ne 0 ] && descrfile=y #RUN     OVER $    CRASH
[ 19990131 -le 20021231 -a ${#crashtime} -eq 0 ] && descrfile=n #RUN NOT OVER & NO CRASH
[ 19990131 -le 20021231 -a ${#crashtime} -ne 0 ] && descrfile=y #RUN NOT OVER &    CRASH
if [ $descrfile = "y" ] ; then
  [ ! -f /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa ] && touch /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  [ ${#crashtime} -ne 0 ] && echo "##CRASH##CRASH##CRASH##CRASH##CRASH##CRASH##CRASH## `date`" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  [ ${#crashtime} -eq 0 ] && echo "################################################### `date`" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "name    :  a99" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "period  :  01.09.1999 to 31.12.2002" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  tmp=`sed -n '1,3p' /climato_tmp1/fettweis/MAR/sim/GRa/ctrl/a99/descr.a99`
  echo "$tmp" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "--------------------------------------------------------------------------------" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "dt    :  60 s      ntfast:  3" >>/climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "np    :  -60       nboucl:  360       output:  each 360 min" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "dtDiff:  60 s      dtPhys:  60 s      dtRadi:  1800 s" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  tmp=`sed -n '4,21p' /climato_tmp1/fettweis/MAR/sim/GRa/ctrl/a99/descr.a99`
  echo "$tmp" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "cluster       :  linux (`uname -n`)" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  tmp=`sed -n '22,26p' /climato_tmp1/fettweis/MAR/sim/GRa/ctrl/a99/descr.a99`
  echo "$tmp" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "--------------------------------------------------------------------------------" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "Informations:" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo " WAt => NDVclim=F and border of constant topo. at boundaries=T in NESTOR.
 Opposite tested." >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  if [ ${#crashtime} -ne 0 ] ; then
    echo "---------------------------------------------------------------------------CRASH" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
    echo "CRASH on:" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
    echo "$crashtime" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  fi
  echo "################################################################################" >> /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa
  echo "informations to summary file /climato_tmp1/fettweis/MAR/sim/GRadescr.GRa"
else
  echo "simulation not yet over"
fi

#-------------------------------------------------------------------------------
# 13 - Final job check
#-------------------------------------------------------------------------------
echo ; echo "---- 13 ---- Final job check" ; echo

  echo "a99990101a job executed successfully on linux"
  mv  $msg  $msg.ok
  rm -rf /scratch/fettweis/MARrun/MAR_GRa99.99.01.01-15
echo `date`
END_EXIT
