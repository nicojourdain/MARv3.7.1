#!/bin/bash
#PBS -N GRa63701o
#PBS -o /climato_tmp1/fettweis/MAR/msg/NST_GRa.63.7.01-31.eo
#PBS -j eo
. /climato_tmp1/fettweis/MAR/bin/INI.ctr
. /climato_tmp1/fettweis/MAR/bin/FUNCTIONS

#-------------------------------------------------------------------------------
# 1 - Initialisation
#-------------------------------------------------------------------------------
echo ; echo "---- 1 ---- Initialisation" ; echo


#variables definition

host=`hostname -s`

#job messages file in /climato_tmp1/fettweis/MAR/msg

msg=/climato_tmp1/fettweis/MAR/msg/NST_GRa.63.7.01-31.$host
rm -f /climato_tmp1/fettweis/MAR/msg/NST_GRa.63.7.01-31.${host%${host#???}}*
touch $msg

#-------------------------------------------------------------------------------
# 2 - Job informations
#-------------------------------------------------------------------------------
echo ; echo "---- 2 ---- Job informations" ; echo

[ -d /scratch/fettweis/MARrun ] || mkdir /scratch/fettweis/MARrun
echo "GRa63701o /scratch/fettweis/MARrun" >> /climato_tmp1/fettweis/MAR/msg/locations

echo "Begin time    : 01/7/1963"
echo "End   time    : 31/07/1963"
echo "Domain        : GRa (/climato_tmp1/fettweis/MAR/sim/GRa)"
echo
echo "Date          : `date`"
echo "Host          : $host"
echo "Work directory: /scratch/fettweis/MARrun"
echo "Messages in   : /climato_tmp1/fettweis/MAR/msg"

#-------------------------------------------------------------------------------
# 3 - Work directory
#-------------------------------------------------------------------------------
echo ; echo "---- 3 ---- Work directory" ; echo


#work directory (/scratch/fettweis/MARrun/NST_GRa.63.7.01-31)

[ ! -d /scratch/fettweis/MARrun ] && mkdir /scratch/fettweis/MARrun

if [ ! -d /scratch/fettweis/MARrun/NST_GRa.63.7.01-31 ] ; then
  mkdir   /scratch/fettweis/MARrun/NST_GRa.63.7.01-31
else
  rm -rf  /scratch/fettweis/MARrun/NST_GRa.63.7.01-31
  mkdir   /scratch/fettweis/MARrun/NST_GRa.63.7.01-31
  echo ; echo "rm: existing /scratch/fettweis/MARrun/NST_GRa.63.7.01-31"
fi
if [ -d /scratch/fettweis/MARrun/NST_GRa.63.7.01-31.CRASH ] ; then
  rm -rf  /scratch/fettweis/MARrun/NST_GRa.63.7.01-31.CRASH
  echo ; echo "rm: existing /scratch/fettweis/MARrun/NST_GRa.63.7.01-31.CRASH"
fi


cp -rfp  /climato_tmp1/fettweis/MAR/sim/GRa/input/NESTOR/*  /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/
rm -rf  /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/run  /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/ctrl

[ ! -d /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input ] && mkdir /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input
[ -d /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/output ] && rm -rf /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/output
mkdir /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/output
[ ! -d /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/core ] && mkdir /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/core

#NESTOR executable

if [ ! -f /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NESTOR.exe ] ; then
  DAMNED "executable nonexistent [/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NESTOR.exe]" "fil"
  END_EXIT
fi
touch     /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/*
touch     /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/*/*
touch     /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/*/*/*
chmod +x  /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NESTOR.exe

#-------------------------------------------------------------------------------
# 4 - input data files
#-------------------------------------------------------------------------------
echo ; echo "---- 4 ---- input data files" ; echo


#LSC files

[ -f /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/LSCfil.dat ] && rm -f /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/LSCfil.dat

  #ECM.1963.07.01-15.GRD.nc

echo ; echo " > ECM.1963.07.01-15.GRD.nc" ; echo
cd /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input
smget "/climato_tmp1/fettweis/MAR/in/ECMWF/GRD/1963" "ECM.1963.07.01-15.GRD.nc.gz" "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input"
ucX "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input" "ECM.1963.07.01-15.GRD.nc" ".gz"
echo "input/ECM.1963.07.01-15.GRD.nc" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/LSCfil.dat

  #ECM.1963.07.16-31.GRD.nc

echo ; echo " > ECM.1963.07.16-31.GRD.nc" ; echo
cd /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input
smget "/climato_tmp1/fettweis/MAR/in/ECMWF/GRD/1963" "ECM.1963.07.16-31.GRD.nc.gz" "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input"
ucX "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input" "ECM.1963.07.16-31.GRD.nc" ".gz"
echo "input/ECM.1963.07.16-31.GRD.nc" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/LSCfil.dat

  #ECM.1963.08.01-15.GRD.nc

echo ; echo " > ECM.1963.08.01-15.GRD.nc" ; echo
cd /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input
smget "/climato_tmp1/fettweis/MAR/in/ECMWF/GRD/1963" "ECM.1963.08.01-15.GRD.nc.gz" "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input"
ucX "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input" "ECM.1963.08.01-15.GRD.nc" ".gz"
echo "input/ECM.1963.08.01-15.GRD.nc" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/LSCfil.dat


#surface files


  #ETOPO

echo ; echo "  > ETOPO" ; echo
mkdir /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/ETOPO 2>/dev/null
cd /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/ETOPO
smget "/climato_tmp1/fettweis/MAR/in/ETOPO" "etopo.nc.gz" "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/ETOPO"
ucX "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/ETOPO" "etopo.nc" ".gz"

  #ETOPO1

echo ; echo "  > ETOPO1" ; echo
mkdir /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/ETOPO1 2>/dev/null
cd /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/ETOPO1
smget "/climato_tmp1/fettweis/MAR/in/ETOPO1" "etopo1.nc.gz" "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/ETOPO1"
ucX "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/ETOPO1" "etopo1.nc" ".gz"

  #ICEmask

echo ; echo "  > ICEmask" ; echo
mkdir /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/ICEmask 2>/dev/null
cd /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/ICEmask
smget "/climato_tmp1/fettweis/MAR/in/ICEmask" "ICEmask.nc.gz" "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/ICEmask"
ucX "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/ICEmask" "ICEmask.nc" ".gz"

  #FAO

echo ; echo "  > FAO" ; echo
mkdir /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/FAO 2>/dev/null
cd /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/FAO
smget "/climato_tmp1/fettweis/MAR/in/FAO" "FAO_SOIL.nc.gz" "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/FAO"
ucX "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/FAO" "FAO_SOIL.nc" ".gz"
smget "/climato_tmp1/fettweis/MAR/in/FAO" "TEXUNIT.ASC.gz" "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/FAO"
ucX "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/FAO" "TEXUNIT.ASC" ".gz"
smget "/climato_tmp1/fettweis/MAR/in/FAO" "SOILPARAMETER.nc.gz" "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/FAO"
ucX "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/FAO" "SOILPARAMETER.nc" ".gz"
smget "/climato_tmp1/fettweis/MAR/in/FAO" "AFRmax-alb.nc.gz" "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/FAO"
ucX "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/FAO" "AFRmax-alb.nc" ".gz"

  #SOIL

echo ; echo "  > SOIL" ; echo
mkdir /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/SOIL 2>/dev/null
cd /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/SOIL
smget "/climato_tmp1/fettweis/MAR/in/SOIL" "GSWP-SOIL.nc.gz" "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/SOIL"
ucX "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/SOIL" "GSWP-SOIL.nc" ".gz"
smget "/climato_tmp1/fettweis/MAR/in/SOIL" "HWSDglob.nc.gz" "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/SOIL"
ucX "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/SOIL" "HWSDglob.nc" ".gz"

  #VEGE

echo ; echo "  > VEGE" ; echo
mkdir /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/VEGE 2>/dev/null
cd /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/VEGE
smget "/climato_tmp1/fettweis/MAR/in/VEGE" "AFRveg_IGBP.nc.gz" "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/VEGE"
ucX "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/VEGE" "AFRveg_IGBP.nc" ".gz"
smget "/climato_tmp1/fettweis/MAR/in/VEGE" "BELveg_IRM.asc.gz" "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/VEGE"
ucX "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/VEGE" "BELveg_IRM.asc" ".gz"
smget "/climato_tmp1/fettweis/MAR/in/VEGE" "EURveg_IGBP.nc.gz" "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/VEGE"
ucX "/scratch/fettweis/MARrun/NST_GRa.63.7.01-31/input/VEGE" "EURveg_IGBP.nc" ".gz"

#NSTing.ctr file

echo "*************************************************************************" > /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "*****************  CONTROL FILE FOR NESTOR PROGRAM  *********************" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "*************************************************************************" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "                                                                         " >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "-------------------------------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "------------------------| NESTOR CONFIGURATION                          |" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "1${blank[24-1]}| - 1 = Nesting field computation               |" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "                        | - 2 = Rain disagregation                      |" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "                        | - 3 = Wind gust estimate                      |" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "-------------------------------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "GRa                     | Label experience                           (a3)" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "------------------------+-----------------------------------+------------" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "output/                                                     | output path" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "------------------------+-----------------------------------+------------" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "ECM${blank[24-3]}| Large-scale model, e.g. E15, E40, MAR, TVM (a3)" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "MAR${blank[24-3]}| Nested model, e.g. MAR, TVM, EUR           (a3)" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "GRD                     | Region .e.g. GRD,ANT,EUR,GRD               (a3)" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "1963,7,01,00           | DATE of RUN START                 (YY,mm,dd,hh)" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "${blank[10-2]}31,00           | RUN LENGHT                              (dd,hh)" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "           06           | Time interval between two forcings         (hh)" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "------------------------| OUTPUT :                                       " >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "T${blank[24-1]}| - initial/forcing files  (*.DAT)          (F/T)" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "F${blank[24-1]}| - ASCII format init./for. files (MAR only)(F/T)" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "F${blank[24-1]}| - graphic check file     (NST*.nc)        (F/T)" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "T${blank[24-1]}| Spherical     coordinates for LSC grid    (F/T)" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "1${blank[24-1]}| Horizontal interpol. type    (1=bilin, 3=bicub)" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "1${blank[24-1]}| Vertical   interpol. type    (1=linear,3=cubic)" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
echo "------------------------| TOPOGRAPHY SOURCE :                            " >> /scratch/fettweis/MARrun/NST_GRa.63.7.01-31/NSTing.ctr
