#!/bin/bash

set -x

dir2=50

for dir in `seq 21 28` ; do

 [ $dir == 21 ] && Y1=1979 && Y2=1981
 [ $dir == 22 ] && Y1=1982 && Y2=1986
 [ $dir == 23 ] && Y1=1987 && Y2=1991
 [ $dir == 24 ] && Y1=1992 && Y2=1996
 [ $dir == 25 ] && Y1=1997 && Y2=2001
 [ $dir == 26 ] && Y1=2002 && Y2=2006
 [ $dir == 27 ] && Y1=2007 && Y2=2011
 [ $dir == 28 ] && Y1=2012 && Y2=2015


 for YYYY in `seq $Y1 $Y2` ; do

  cp -r c$dir c$dir2

  dir2=$(( $dir2 + 1 ))

 done

done
