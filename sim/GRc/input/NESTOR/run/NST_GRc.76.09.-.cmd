#!/bin/bash
#PBS -N GRc7609
#PBS -o /climato_tmp1/fettweis/MAR/msg/NST_GRc.76.09.-.eo
#PBS -j eo
. /climato_tmp1/fettweis/MAR/bin/INI.ctr
. /climato_tmp1/fettweis/MAR/bin/FUNCTIONS

#-------------------------------------------------------------------------------
# 1 - Initialisation
#-------------------------------------------------------------------------------
echo ; echo "---- 1 ---- Initialisation" ; echo


#variables definition

host=`hostname -s`

#job messages file in /climato_tmp1/fettweis/MAR/msg

msg=/climato_tmp1/fettweis/MAR/msg/NST_GRc.76.09.-.$host
rm -f /climato_tmp1/fettweis/MAR/msg/NST_GRc.76.09.-.${host%${host#???}}*
touch $msg

#-------------------------------------------------------------------------------
# 2 - Job informations
#-------------------------------------------------------------------------------
echo ; echo "---- 2 ---- Job informations" ; echo

[ -d /scratch/fettweis/MARrun ] || mkdir /scratch/fettweis/MARrun
echo "GRc7609 /scratch/fettweis/MARrun" >> /climato_tmp1/fettweis/MAR/msg/locations

echo "Begin time    : /09/1976"
echo "End   time    : /09/1976"
echo "Domain        : GRc (/climato_tmp1/fettweis/MAR/sim/GRc)"
echo
echo "Date          : `date`"
echo "Host          : $host"
echo "Work directory: /scratch/fettweis/MARrun"
echo "Messages in   : /climato_tmp1/fettweis/MAR/msg"

#-------------------------------------------------------------------------------
