#!/bin/bash
#PBS -N GRc580601o
#PBS -o /climato_tmp1/fettweis/MAR/msg/NST_GRc.58.06.01-30.eo
#PBS -j eo
. /climato_tmp1/fettweis/MAR/bin/INI.ctr
. /climato_tmp1/fettweis/MAR/bin/FUNCTIONS

#-------------------------------------------------------------------------------
# 1 - Initialisation
#-------------------------------------------------------------------------------
echo ; echo "---- 1 ---- Initialisation" ; echo


#variables definition

host=`hostname -s`

#job messages file in /climato_tmp1/fettweis/MAR/msg

msg=/climato_tmp1/fettweis/MAR/msg/NST_GRc.58.06.01-30.$host
rm -f /climato_tmp1/fettweis/MAR/msg/NST_GRc.58.06.01-30.${host%${host#???}}*
touch $msg

#-------------------------------------------------------------------------------
# 2 - Job informations
#-------------------------------------------------------------------------------
echo ; echo "---- 2 ---- Job informations" ; echo

[ -d /scratch/fettweis/MARrun ] || mkdir /scratch/fettweis/MARrun
echo "GRc580601o /scratch/fettweis/MARrun" >> /climato_tmp1/fettweis/MAR/msg/locations

echo "Begin time    : 01/06/1958"
echo "End   time    : 30/06/1958"
echo "Domain        : GRc (/climato_tmp1/fettweis/MAR/sim/GRc)"
echo
echo "Date          : `date`"
echo "Host          : $host"
echo "Work directory: /scratch/fettweis/MARrun"
echo "Messages in   : /climato_tmp1/fettweis/MAR/msg"

#-------------------------------------------------------------------------------
# 3 - Work directory
#-------------------------------------------------------------------------------
echo ; echo "---- 3 ---- Work directory" ; echo


#work directory (/scratch/fettweis/MARrun/NST_GRc.58.06.01-30)

[ ! -d /scratch/fettweis/MARrun ] && mkdir /scratch/fettweis/MARrun

if [ ! -d /scratch/fettweis/MARrun/NST_GRc.58.06.01-30 ] ; then
  mkdir   /scratch/fettweis/MARrun/NST_GRc.58.06.01-30
else
  rm -rf  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30
  mkdir   /scratch/fettweis/MARrun/NST_GRc.58.06.01-30
  echo ; echo "rm: existing /scratch/fettweis/MARrun/NST_GRc.58.06.01-30"
fi
if [ -d /scratch/fettweis/MARrun/NST_GRc.58.06.01-30.CRASH ] ; then
  rm -rf  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30.CRASH
  echo ; echo "rm: existing /scratch/fettweis/MARrun/NST_GRc.58.06.01-30.CRASH"
fi


cp -rfp  /climato_tmp1/fettweis/MAR/sim/GRc/input/NESTOR/*  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/
rm -rf  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/run  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/ctrl

[ ! -d /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input ] && mkdir /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input
[ -d /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output ] && rm -rf /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output
mkdir /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output
[ ! -d /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/core ] && mkdir /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/core

#NESTOR executable

if [ ! -f /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NESTOR.exe ] ; then
  DAMNED "executable nonexistent [/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NESTOR.exe]" "fil"
  END_EXIT
fi
touch     /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/*
touch     /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/*/*
touch     /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/*/*/*
chmod +x  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NESTOR.exe

#-------------------------------------------------------------------------------
# 4 - input data files
#-------------------------------------------------------------------------------
echo ; echo "---- 4 ---- input data files" ; echo


#LSC files

[ -f /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/LSCfil.dat ] && rm -f /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/LSCfil.dat

  #ECM.1958.06.01-15.GRD.nc

echo ; echo " > ECM.1958.06.01-15.GRD.nc" ; echo
cd /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input
smget "/climato_tmp1/fettweis/MAR/in/ECMWF/GRD/1958" "ECM.1958.06.01-15.GRD.nc.gz" "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input"
ucX "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input" "ECM.1958.06.01-15.GRD.nc" ".gz"
echo "input/ECM.1958.06.01-15.GRD.nc" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/LSCfil.dat

  #ECM.1958.06.16-30.GRD.nc

echo ; echo " > ECM.1958.06.16-30.GRD.nc" ; echo
cd /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input
smget "/climato_tmp1/fettweis/MAR/in/ECMWF/GRD/1958" "ECM.1958.06.16-30.GRD.nc.gz" "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input"
ucX "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input" "ECM.1958.06.16-30.GRD.nc" ".gz"
echo "input/ECM.1958.06.16-30.GRD.nc" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/LSCfil.dat

  #ECM.1958.07.01-15.GRD.nc

echo ; echo " > ECM.1958.07.01-15.GRD.nc" ; echo
cd /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input
smget "/climato_tmp1/fettweis/MAR/in/ECMWF/GRD/1958" "ECM.1958.07.01-15.GRD.nc.gz" "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input"
ucX "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input" "ECM.1958.07.01-15.GRD.nc" ".gz"
echo "input/ECM.1958.07.01-15.GRD.nc" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/LSCfil.dat


#surface files


  #ETOPO

echo ; echo "  > ETOPO" ; echo
mkdir /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/ETOPO 2>/dev/null
cd /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/ETOPO
smget "/climato_tmp1/fettweis/MAR/in/ETOPO" "etopo.nc.gz" "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/ETOPO"
ucX "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/ETOPO" "etopo.nc" ".gz"

  #ETOPO1

echo ; echo "  > ETOPO1" ; echo
mkdir /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/ETOPO1 2>/dev/null
cd /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/ETOPO1
smget "/climato_tmp1/fettweis/MAR/in/ETOPO1" "etopo1.nc.gz" "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/ETOPO1"
ucX "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/ETOPO1" "etopo1.nc" ".gz"

  #ICEmask

echo ; echo "  > ICEmask" ; echo
mkdir /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/ICEmask 2>/dev/null
cd /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/ICEmask
smget "/climato_tmp1/fettweis/MAR/in/ICEmask" "ICEmask.nc.gz" "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/ICEmask"
ucX "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/ICEmask" "ICEmask.nc" ".gz"

  #FAO

echo ; echo "  > FAO" ; echo
mkdir /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/FAO 2>/dev/null
cd /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/FAO
smget "/climato_tmp1/fettweis/MAR/in/FAO" "FAO_SOIL.nc.gz" "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/FAO"
ucX "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/FAO" "FAO_SOIL.nc" ".gz"
smget "/climato_tmp1/fettweis/MAR/in/FAO" "TEXUNIT.ASC.gz" "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/FAO"
ucX "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/FAO" "TEXUNIT.ASC" ".gz"
smget "/climato_tmp1/fettweis/MAR/in/FAO" "SOILPARAMETER.nc.gz" "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/FAO"
ucX "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/FAO" "SOILPARAMETER.nc" ".gz"
smget "/climato_tmp1/fettweis/MAR/in/FAO" "AFRmax-alb.nc.gz" "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/FAO"
ucX "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/FAO" "AFRmax-alb.nc" ".gz"

  #SOIL

echo ; echo "  > SOIL" ; echo
mkdir /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/SOIL 2>/dev/null
cd /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/SOIL
smget "/climato_tmp1/fettweis/MAR/in/SOIL" "GSWP-SOIL.nc.gz" "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/SOIL"
ucX "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/SOIL" "GSWP-SOIL.nc" ".gz"
smget "/climato_tmp1/fettweis/MAR/in/SOIL" "HWSDglob.nc.gz" "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/SOIL"
ucX "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/SOIL" "HWSDglob.nc" ".gz"

  #VEGE

echo ; echo "  > VEGE" ; echo
mkdir /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/VEGE 2>/dev/null
cd /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/VEGE
smget "/climato_tmp1/fettweis/MAR/in/VEGE" "AFRveg_IGBP.nc.gz" "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/VEGE"
ucX "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/VEGE" "AFRveg_IGBP.nc" ".gz"
smget "/climato_tmp1/fettweis/MAR/in/VEGE" "BELveg_IRM.asc.gz" "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/VEGE"
ucX "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/VEGE" "BELveg_IRM.asc" ".gz"
smget "/climato_tmp1/fettweis/MAR/in/VEGE" "EURveg_IGBP.nc.gz" "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/VEGE"
ucX "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/input/VEGE" "EURveg_IGBP.nc" ".gz"

#NSTing.ctr file

echo "*************************************************************************" > /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "*****************  CONTROL FILE FOR NESTOR PROGRAM  *********************" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "*************************************************************************" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "                                                                         " >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "-------------------------------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------| NESTOR CONFIGURATION                          |" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "1${blank[24-1]}| - 1 = Nesting field computation               |" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "                        | - 2 = Rain disagregation                      |" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "                        | - 3 = Wind gust estimate                      |" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "-------------------------------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "GRc                     | Label experience                           (a3)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------+-----------------------------------+------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "output/                                                     | output path" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------+-----------------------------------+------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "ECM${blank[24-3]}| Large-scale model, e.g. E15, E40, MAR, TVM (a3)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "MAR${blank[24-3]}| Nested model, e.g. MAR, TVM, EUR           (a3)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "GRD                     | Region .e.g. GRD,ANT,EUR,GRD               (a3)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "1958,06,01,00           | DATE of RUN START                 (YY,mm,dd,hh)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "${blank[10-2]}30,00           | RUN LENGHT                              (dd,hh)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "           06           | Time interval between two forcings         (hh)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------| OUTPUT :                                       " >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "T${blank[24-1]}| - initial/forcing files  (*.DAT)          (F/T)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "F${blank[24-1]}| - ASCII format init./for. files (MAR only)(F/T)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "F${blank[24-1]}| - graphic check file     (NST*.nc)        (F/T)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "T${blank[24-1]}| Spherical     coordinates for LSC grid    (F/T)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "1${blank[24-1]}| Horizontal interpol. type    (1=bilin, 3=bicub)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "1${blank[24-1]}| Vertical   interpol. type    (1=linear,3=cubic)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------| TOPOGRAPHY SOURCE :                            " >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "T${blank[24-1]}| - ETOPO data set  (resol. : 5 minutes  )  (F/T)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "F${blank[24-1]}| - GTOPO data set  (resol. : 30 secondes)  (F/T)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------| TOPOGRAPHY TREATMENT :                        |" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "F${blank[24-1]}| - border of constant topography at boundaries |" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "F${blank[24-1]}| - imposed LSC topography in the const. border |" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "F${blank[24-1]}| - imposed LSC topography in the whole domain  |" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "F${blank[24-1]}| - zero topography        in the const. border |" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "T${blank[24-1]}| - filtering of topography                     |" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------| CORRECTION APPLIED TO METEO. FIELDS :          " >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "F${blank[24-1]}| - 600-hPa geopotential height             (F/T)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "T${blank[24-1]}| - mixed surface layer                     (F/T)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------| ROUGHNESS LENGHT :                             " >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "F${blank[24-1]}| Computed from land use datasets           (T/F)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------| VEGETATION COVER :                             " >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "T${blank[24-1]}| - Global land cover (IGBP)                (T/F)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "F${blank[24-1]}| - European land cover (Corine) : Europe   (T/F)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "F${blank[24-1]}| - GlobCover V.2.2 Land Cover              (T/F)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------| VEGETATION FRACTION (select max. one option) : " >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "F${blank[24-1]}| - Correction with NDVI index (res. 1 km)  (T/F)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "F${blank[24-1]}| - Correction with NDVI index (res. 8 km)  (T/F)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------| SOIL MODEL :                                   " >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "T${blank[24-1]}| De Ridder and Schayes (1997) soil model   (T/F)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "40.${blank[24-3]}| Imposed soil wetness in all layers (0 to 100 %)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "F${blank[24-1]}| Soil wetness from ECMWF fields            (T/F)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------| SEA SURFACE TEMPERATURE :                      " >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "F${blank[24-1]}| Imposed Reynolds sea surface temperature  (T/F)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------| SOUNDING                                       " >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "F${blank[24-1]}| Simplified initialisation with sounding   (T/F)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------+---------------------------------+--------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "/input                                                    | Sounding file" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------+---------------------------------+--------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------| CLOUD MICROPHYSICS                             " >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "F${blank[24-1]}| Include cloud content in spec. humidity   (T/F)" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------|------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------| RAIN DISAGGREGATION                            " >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "0${blank[24-1]}| 0 = no rain disaggregation (only model fields) " >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "                        | 1 = disaggregation model of Sinclair (1994)    " >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "                        | 2 = disaggregation model of Alpert   (1989)    " >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "-------------------------------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "------------------------| WIND GUST ESTIMATE METHOD                      " >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "1${blank[24-1]}| 1 = Standard WGE method of Brasseur (2001)     " >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "                        | 2 = BRN method (without TKE) of Ramel (2001)   " >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "                        | 3 = Ratio method                               " >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr
echo "-------------------------------------------------------------------------" >> /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr

cp -fp  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NSTing.ctr  /climato_tmp1/fettweis/MAR/sim/GRc/input/NESTOR/NSTing.ctr

#-------------------------------------------------------------------------------
# 5 - Stock initialisation
#-------------------------------------------------------------------------------
echo ; echo "---- 5 ---- Stock initialisation" ; echo

 /usr/bin/ssh -o ConnectTimeout=10 -o ConnectionAttempts=10 -C fettweis@139.165.29.17 mkdir -p /climato_tmp1/fettweis/MAR/out/GRc/input/NESTOR/1958
 /usr/bin/ssh -o ConnectTimeout=10 -o ConnectionAttempts=10 -C fettweis@139.165.29.17 mkdir -p /climato_tmp1/fettweis/MAR/out/GRc/input/NESTOR/1958

#-------------------------------------------------------------------------------
# 6 - NESTOR launch
#-------------------------------------------------------------------------------
echo ; echo "---- 6 ---- NESTOR launch" ; echo

cd /scratch/fettweis/MARrun/NST_GRc.58.06.01-30
nice -n19 time  ./NESTOR.exe > /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NESTOR.log
echo
cp -fp  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NESTOR.log  /climato_tmp1/fettweis/MAR/sim/GRc/input/NESTOR/run/GRc580601o.log

if [ -f /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/NST.OK ] ; then
  echo "NESTOR run: OK (`date`)"
else
  DAMNED "NESTOR error (NST.OK nonexistent) (`date`)" "exe"
  mv -f  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30.$host.CRASH
  tarX  "/scratch/fettweis/MARrun"  "NST_GRc.58.06.01-30.$host.CRASH"
  [ $? -ne 0 ] && END_EXIT
  rm -rf  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30.$host.CRASH
  gzipX  "/scratch/fettweis/MARrun"  "NST_GRc.58.06.01-30.$host.CRASH.tar"
  [ $? -ne 0 ] && END_EXIT
  Z=.gz
  smput "/scratch/fettweis/MARrun"  "NST_GRc.58.06.01-30.$host.CRASH.tar$Z"  "/climato_tmp1/fettweis/MAR/out/GRc/crash"
  [ $? -eq 0 ] && rm -f  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30.$host.CRASH.tar$Z
  END_EXIT
fi

#-------------------------------------------------------------------------------
# 7 - Backup: NESTOR output files
#-------------------------------------------------------------------------------
echo ; echo "---- 7 ---- Backup: NESTOR output files" ; echo


#output directory content

echo ; echo "> output directory content" ; echo
ls -l /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output

#MAR*.inc* (MARdcl_GRc.1958.06.01.inc)

echo ; echo "> MAR*.inc* (MARdcl_GRc.1958.06.01.inc)" ; echo
tarX  "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output"  "MAR*.inc*"  "MARdcl_GRc.1958.06.01.inc"
if [ $? -eq 0 ] ; then
  MARdcl=MARdcl_GRc.1958.06.01.inc.tar
  smput  "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output"  "$MARdcl"  "/climato_tmp1/fettweis/MAR/out/GRc/input/NESTOR/1958"
  [ $? -eq 0 ] && rm -f  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output/$MARdcl  MAR*.inc*
fi

#MARlbc (MARlbc_GRc.1958.06.01-30.DAT)

echo ; echo "> MARlbc (MARlbc_GRc.1958.06.01-30.DAT)" ; echo
mv  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output/MARlbc.DAT  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output/MARlbc_GRc.1958.06.01-30.DAT
MARlbc=MARlbc_GRc.1958.06.01-30.DAT
  gzipX  "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output"  "MARlbc_GRc.1958.06.01-30.DAT"
  if [ $? -eq 0 ] ; then
    MARlbc=MARlbc_GRc.1958.06.01-30.DAT.gz
    smput  "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output"  "$MARlbc"  "/climato_tmp1/fettweis/MAR/out/GRc/input/NESTOR/1958"
    [ $? -eq 0 ] && rm -f /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output/$MARlbc
  fi
#If any trouble here, MARlbc*.DAT is likely to become part of MARini*DAT
echo "If any trouble here, MARlbc*.DAT is likely to become part of MARini*DAT"

#MARglf (MARglf_GRc.1958.06.01-30.DAT)

echo ; echo "> MARglf (MARglf_GRc.1958.06.01-30.DAT)" ; echo
mv  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output/MARglf.DAT  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output/MARglf_GRc.1958.06.01-30.DAT
MARglf=MARglf_GRc.1958.06.01-30.DAT
  gzipX  "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output"  "MARglf_GRc.1958.06.01-30.DAT"
  if [ $? -eq 0 ] ; then
    MARglf=MARglf_GRc.1958.06.01-30.DAT.gz
    smput  "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output"  "$MARglf"  "/climato_tmp1/fettweis/MAR/out/GRc/input/NESTOR/1958"
    [ $? -eq 0 ] && rm -f /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output/$MARglf
  fi
#If any trouble here, MARglf*.DAT is likely to become part of MARini*DAT
echo "If any trouble here, MARglf*.DAT is likely to become part of MARini*DAT"

#MARsic (MARsic_GRc.1958.06.01-30.DAT)

echo ; echo "> MARsic (MARsic_GRc.1958.06.01-30.DAT)" ; echo
mv  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output/MARsic.DAT  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output/MARsic_GRc.1958.06.01-30.DAT
MARsic=MARsic_GRc.1958.06.01-30.DAT
  gzipX  "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output"  "MARsic_GRc.1958.06.01-30.DAT"
  if [ $? -eq 0 ] ; then
    MARsic=MARsic_GRc.1958.06.01-30.DAT.gz
    smput  "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output"  "$MARsic"  "/climato_tmp1/fettweis/MAR/out/GRc/input/NESTOR/1958"
    [ $? -eq 0 ] && rm -f /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output/$MARsic
  fi
#If any trouble here, MARsic*.DAT is likely to become part of MARini*DAT
echo "If any trouble here, MARsic*.DAT is likely to become part of MARini*DAT"

#MARubc (MARubc_GRc.1958.06.01-30.DAT)

echo ; echo "> MARubc (MARubc_GRc.1958.06.01-30.DAT)" ; echo
mv  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output/MARubc.DAT  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output/MARubc_GRc.1958.06.01-30.DAT
MARubc=MARubc_GRc.1958.06.01-30.DAT
  gzipX  "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output"  "MARubc_GRc.1958.06.01-30.DAT"
  if [ $? -eq 0 ] ; then
    MARubc=MARubc_GRc.1958.06.01-30.DAT.gz
    smput  "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output"  "$MARubc"  "/climato_tmp1/fettweis/MAR/out/GRc/input/NESTOR/1958"
    [ $? -eq 0 ] && rm -f /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output/$MARubc
  fi
#If any trouble here, MARubc*.DAT is likely to become part of MARini*DAT
echo "If any trouble here, MARubc*.DAT is likely to become part of MARini*DAT"

#MARdom (MARdom_GRc.1958.06.01.dat)

echo ; echo "> MARdom (MARdom_GRc.1958.06.01.dat)" ; echo
mv  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output/MARdom.dat  /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output/MARdom_GRc.1958.06.01.dat
MARdom=MARdom_GRc.1958.06.01.dat
  gzipX  "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output"  "MARdom_GRc.1958.06.01.dat"
  if [ $? -eq 0 ] ; then
    MARdom=MARdom_GRc.1958.06.01.dat.gz
    smput  "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output"  "$MARdom"  "/climato_tmp1/fettweis/MAR/out/GRc/input/NESTOR/1958"
    [ $? -eq 0 ] && rm -f /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output/$MARdom
  fi

#MARini (MARini_GRc.1958.06.01.DAT)

echo ; echo "> MARini (MARini_GRc.1958.06.01.DAT)" ; echo
tarX  "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output"  "MAR*.DAT"  "MARini_GRc.1958.06.01.DAT"
if [ $? -eq 0 ] ; then
  MARini=MARini_GRc.1958.06.01.DAT.tar
  gzipX  "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output"  "$MARini"
  if [ $? -eq 0 ] ; then
    MARini=$MARini.gz
    smput  "/scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output"  "$MARini"  "/climato_tmp1/fettweis/MAR/out/GRc/input/NESTOR/1958"
    [ $? -eq 0 ] && rm -f /scratch/fettweis/MARrun/NST_GRc.58.06.01-30/output/$MARini
  fi
fi

#-------------------------------------------------------------------------------
# 8 - Final job check
#-------------------------------------------------------------------------------
echo ; echo "---- 8 ---- Final job check" ; echo

  echo "GRc580601o job executed successfully on linux" ; echo
  rm -rf /scratch/fettweis/MARrun/NST_GRc.58.06.01-30
  mv  $msg  $msg.ok &>/dev/null

echo `date`
echo

END_EXIT

